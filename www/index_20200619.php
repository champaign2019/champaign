<?session_start();?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/logingCheck.php" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/themeHtml.php" ?>
</head>
<body class="main">
	<? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
	<section>
		<div class="home-header main_visual">
			<div class="home-slider owl-carousel owl-theme">
				<div class="item" style="width: 100%;">
					<img src="/images/home/visual01.jpg" class="wow fadeIn " style="min-width: 100%;" alt="little girl feeding a cat">
					<img src="/images/home/m_visual01.jpg" class="wow fadeIn " style="min-width: 100%;" alt="little girl feeding a cat">
					<!--
					<div class="header-text">
						<div class="container-1 wow fadeInUp">
							<h3>Protect your best friend from paralysis ticks, fleas and more* with a long-lasting, single dose.</h3>
						</div>
					</div>
					-->
				</div>
				<div class="item" style="width: 100%;">
					<img src="/images/home/visual02.jpg" class="wow fadeIn " alt="little girl feeding a cat">
					<img src="/images/home/m_visual02.jpg" class="wow fadeIn " style="min-width: 100%;" alt="little girl feeding a cat">
				</div>
				<div class="item" style="width: 100%;">
					<img src="/images/home/visual03.jpg" class="wow fadeIn " alt="little girl feeding a cat">
					<img src="/images/home/m_visual03.jpg" class="wow fadeIn " style="min-width: 100%;" alt="little girl feeding a cat">
				</div>
				<div class="item" style="width: 100%;">
					<img src="/images/home/visual04.jpg" class="wow fadeIn " alt="little girl feeding a cat">
					<img src="/images/home/m_visual04.jpg" class="wow fadeIn " style="min-width: 100%;" alt="little girl feeding a cat">
				</div>
			</div>
		</div>
	</section>

	<section>
	<div class="desktop-only">
		<ul class="nav nav-pills home-product-tabs" id="producttab">
			<li class="bluetab"><a href="#tab-1" data-toggle="tab" class="scroll active" title="Bravecto Chew for Dogs"> <img src="/images/icons/dog_icon.svg" alt="Bravecto Chew for Dogs">강아지용 브라벡토 츄어블</a> </li>
			<!-- 숨김후 추후공개
			<li class="purpaltab"><a href="#tab-2" data-toggle="tab" class="scroll " title="Bravecto Spot-on for Dogs"><img src="/images/icons/dog_icon.svg" alt="Bravecto Spot-on for Dogs">강아지용 브라벡토 스팟온</a> </li>
			-->
			<li class="orangetab"><a href="#tab-3" data-toggle="tab" class="scroll" title="Bravecto Spot-on for Cats"><img src="/images/icons/cat_icon.svg" alt="Bravecto Spot-on for Cats">고양이용 브라벡토 스팟온</a> </li>
			
		</ul>
		<div class="tab-content" id="producttabContent">
			<div class="tab-pane tab-pane-first active" id="tab-1">
				<div class="container-1" style="padding-bottom:60px;">
					<div class="row no-gutters">
						<div class="col-12">
							<h1><span class="hideer"><span class="title-animation1">강아지용</span></span><span class="hideer"><span class="title-animation1"> 브라벡토 츄어블</span></span></h1>
						</div>
						<div class="col-3">
							<ul class="home-product-li">
								<li class="fads s1">맛있는 브라벡토 츄어블정 단 한 알로 12주간 진드기(tick and mite) 및 벼룩(flea)으로부터 안심하고 산책이 가능합니다 </li>
								<li class="fads s2">임신 및 수유중인 개에서 안전하게 사용이 가능하며, 콜리종에서도 안전하게 사용이 가능합니다.</li>
							</ul>
						</div>
						<div class="col-6 text-center">
							<div class="oval oval-blue"><img src="/images/home/oval-chew-for-dogs.png" class="fadi s1" alt="Bravecto"></div>
						</div>
						<div class="col-3">
							<ul class="home-product-li">
								<li class="fads s3">바베시아증, 라임병, 중증열성혈소판감소증(SFTS) 등 다양한 질병을 매개하는 진드기 감염을 예방합니다.</li>
								<li class="fads s4">투여 후 4시간 이내에 작용하기 시작하여, 12시간 이내에 흡혈 진드기가 100% 사멸됩니다.</li>
							</ul>
						</div>
						<div class="col-12">
							<div class="usp-logo pull-left fads chew_logo s5"><img src="/images/home/chew_logo.png" alt="3 Months Paralysis tick & Flea Protection"></div>
							<div class="pull-right fads 6s"><a href="/page/chew-for-dogs.php" class="findoutmore" title="Find out more"><span>더 알아보기</span></a></div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>

				<section class="video_con type01">
					<div class="container-1">
						<div class="video_box">
							<iframe width="820" height="470" src="https://www.youtube.com/embed/JJjjqz6_mwY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
					</div>
				</section>

				<section class="alt-color">
					<div class="container-1">
						<div class="row no-gutters">
							<div class="col-md-12 col-lg-8 moile-last">
								<div class="benefits">
									<div class="benefits-list benefits-list-first wow fadeInUp"> <a href="/page/benefits.php" class="redbg-link" title="BENEFITS">브라벡토 장점</a> </div>
									<div class="benefits-list extrta-mb ">
										<img src="/images/icons/longlasting.png" alt="little girl feeding a cat" class="wow fadeInUp">
										<h3 class="anim"><span class="titleanimation-bottom">호기심 많은 삶의 동반자를 보호하세요</span></h3>
										<p class="wow fadeInUp">도심 속 공원 산책, 깊은 산속 캠핑, 드넓은 해변가에서의 해수욕 등 
										우리의 반려견들은 자연을 만끽할 수 있는 액티비티를 할 때 가장 행복할 것입니다.
										브라벡토로 반려견에게 12주간의 행복을 선물해 주세요.</p>
									</div>
									<div class="benefits-list extrta-mb">
										<img src="/images/icons/simple.png" alt="little girl feeding a cat" class="wow fadeInUp">
										<h3 class="anim"><span class="titleanimation-bottom">맞습니다, 잘먹습니다!</span></h3>
										<p class="wow fadeInUp">몸에 좋은 약이 입에 쓰다지만, 약을 거부하는 강아지의 모습을 보는 것은 참으로 안타까운 일입니다.
										맛있게 먹는 반려견의 행복한 모습을 보는 것 만큼 뿌듯한 일이 있을까요?
										더 이상 약이 피부에 제대로 흡수되었는지, 털에 흘리거나 옷에 묻지 않았는지 고민하지 마세요. </p>
									</div>
									<div class="benefits-list ">
										<img src="/images/icons/world.png" alt="little girl feeding a cat" class="wow fadeInUp">
										<h3 class="anim"><span class="titleanimation-bottom">1억 도스 판매 돌파</span></h3>
										<p class="wow fadeInUp">
										브라벡토는 출시 후 광범위한 임상 연구와 모니터링으로 반려동물이 더욱 안전하고 행복할 수 있도록 노력해 왔습니다. 이러한 노력으로 전 세계 85여개 국가에서 출시 5주년인 2019년, 1억 도스 판매를 돌파하였으며, 그 안전성을 검증 받았습니다.</p>
									</div>
									<div class="benefits-list benefits-list-last mb-0 wow fadeInUp"> <a href="/page/benefits.php" class="findoutmore" title="Find out more"><span>더 알아보기</span></a> </div>
								</div>
							</div>
							<div class="col-md-12 col-lg-4 mobile-first ">
								<div class="where-to-get wow fadeInUp">
									<h2>브라벡토 구매를 원하시나요?</h2>
									<p>당신의 반려견에게 가장 알맞은 제품에 대해 수의사와 상담하세요. 
									구입을 원하신다면 거주지역을 아래 검색창에 입력하세요.</p>
									<form action="/page/stockists.php">
										<input name="zipcode" type="text" placeholder="시/군/구 검색">
										<button class="arror-btn" type="submit"></button>
									</form>
								</div>
							</div>
						</div>
					</div>
				</section>

				<section class="purpal-background top-strip mobile-pedding">
					<div class="container-1 ">
						<div class="row no-gutters">
							<div class="col-md-12 col-lg-6 align-self-center moile-last">
								<div class="text-block ">
									<h2 class="anim"><span class="titleanimation-bottom">반려견의 외부기생충</span></h2>
									<p class="wow fadeInUp">
										참진드기가 반려견 몸에 달라붙어 흡혈하는 과정 중에 <a href="/page/parasites.php">다양한 진드기 매개성 질환</a>[중증열성혈소판감소증(SFTS), 라임병, 바베시아증, 아나플라즈마증, 에를리히증 등] 이 강아지로 전염되어 죽음까지도 유발할 수 있는 치명적인 결과를 초래할 수 있습니다. 참진드기 외에도 <a href="/page/flea.php">벼룩</a>, <a href="/page/mitemite.php">모낭충, 귀진드기, 옴진드기</a> 등 다양한 외부기생충이 우리 반려견의 행복을 위협하죠. 이러한 외부기생충 매개 질환은 브라벡토로 간단하게 예방이 가능합니다¹². 

									</p>
								</div>
							</div>
							<div class="col-md-12 col-lg-6 mobile-first"><img src="/images/home/photo1_2.jpg" class="w-100 wow fadeIn" alt="Meet the bad guys"></div>
						</div>
						<div class="row no-gutters">
							<div class="col-md-12 col-lg-6"><img src="/images/home/photo2.jpg" class="w-100 wow fadeIn" alt="Bravecto for Vets"></div>
							<div class="col-md-12 col-lg-6 align-self-center">
								<div class="text-block">
									<h2 class="anim"><span class="titleanimation-bottom">자신 있게 권하세요</span></h2>
									<p class="wow fadeInUp">
										브라벡토는 동물병원에 방문하는 반려견 및 보호자의 일상을 바꿔줄 수 있습니다.
										MSD동물약품의 광범위한 임상 연구와 모니터링을 바탕으로 반려견 및 보호자에게 
										더욱 안전한 동물용의약품을 제공하세요. 
										세계적인 동물용의약품인 브라벡토만의 독보적인 안전성과 효능을 더 전문적으로 알아보세요.
									</p>
									<a href="/page/vets-resources.php" class="findoutmore wow fadeInUp" title="Find out more"><span>학술정보 찾아보기</span></a>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
			<div class="tab-pane" id="tab-2">
				<div class="container-1">
					<div class="row no-gutters">
						<div class="col-12">
							<h1><span class="hideer "><span class="title-animation1">강아지용</span></span><span class="hideer"><span class="title-animation1">브라벡토 스팟온</span></span></h1>
						</div>
						<div class="col-3">
							<ul class="home-product-li">
								<li class="fads s1">1회 사용만으로 12주간 외부기생충으로부터 안심하고 산책할 수 있습니다</li>
								<li class="fads s2">TWIST’N’USE 기술로 사용이 더욱 간단하고 편리합니다</li>
								<li class="fads s3">4시간 이전에 작용하기 시작하여, 12시간 이내에 진드기의 100%가 사멸합니다</li>
								<li class="border-0 fads s4">외부기생충으로 인한 각종 질병을 예방합니다</li>
							</ul>
						</div>
						<div class="col-6 text-center">
							<div class="oval oval-purpal"><img src="/images/home/oval-spot-on-dogs.png" class="fadi s1" alt="Bravecto"></div>
						</div>
						<div class="col-3">
							<ul class="icon-li">
								<li class="flees fads s5"> <a href="#">벼룩<strong>12주</strong></a> </li>
								<li class="paral fads s6"> <a href="#">참진드기<strong>12주</strong></a> </li>
								<li class="brown fads s7"> <a href="#">귀진드기<strong>12주</strong></a> </li>
								<li class="brown fads s8"> <a href="#">옴진드기<strong>12주</strong></a> </li>
								<li class="brown fads s9"> <a href="#">모낭충<strong>12주</strong></a> </li>
							</ul>
							<p class="small-text fads s10">
								진드기(tick and mite) 및 벼룩 구제(12주 지속)
								임신, 분만 및 수유중인 개에서 안전하게 사용이 가능합니다
								콜리종에서도 안전하게 사용이 가능합니다
							</p>
						</div>
						<div class="col-12">
							<div class="usp-logo pull-left fads s11"><img src="/images/home/logo-6-months.png" alt="6 Months Paralysis tick & Flea Protection"></div>
							<div class="pull-right fads s12"><a href="/page/spot-on-dogs.php" class="findoutmore" title="Find out more"><span>Find out more</span></a></div>
						</div>
					</div>
				</div>
			</div>
			<div class="tab-pane" id="tab-3">
				<div class="container-1" style="padding-bottom:60px;">
					<div class="row no-gutters">
						<div class="col-12">
							<h1><span class="hideer "><span class="title-animation1">고양이용</span></span><span class="hideer"><span class="title-animation1">브라벡토 스팟온</span></span></h1>
						</div>
						<div class="col-3">
							<ul class="home-product-li">
								<li class="fads s1">1회 사용만으로 12주간 고양이를 각종 외부기생충으로부터 보호합니다</li>
								<li class="fads s2">TWIST’N’USE 기술로 사용이 더욱 간단하고 편리합니다</li>
								
							</ul>
						</div>
						<div class="col-6 text-center">
							<div class="oval oval-orange"><img src="/images/home/oval-spot-on-cats.png" class=" fadi s1" alt="little girl feeding a cat"></div>
						</div>
						<div class="col-3">
							<ul class="home-product-li">
								<li class="fads s3">외부기생충 매개 질환을 예방합니다.</li>
								<li class="fads s4">9주령 및 1.2kg가 조금 넘는 새끼고양이에서도 안전하게 사용이 가능합니다.</li>
								<li class="fads s5">2020년 출시 예정</li>
							</ul>
						</div>
						<div class="col-12">
							<div class="usp-logo pull-left fads chew_logo s5"><img src="/images/home/chew_logo.png" alt="3 Months Paralysis tick & Flea Protection"></div>
							<div class="pull-right fads s7"><a href="/page/spot-on-cats.php" class="findoutmore" title="Find out more"><span>더 알아보기</span></a></div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
				<section class="alt-color">
					<div class="container-1">
						<div class="row no-gutters">
							<div class="col-md-12 col-lg-8 moile-last">
								<div class="benefits">
									<div class="benefits-list benefits-list-first wow fadeInUp"> <a href="/page/benefits.php" class="redbg-link" title="BENEFITS">브라벡토 장점</a> </div>
									<div class="benefits-list extrta-mb ">
										<img src="/images/icons/longlasting02.png" alt="little girl feeding a cat" class="wow fadeInUp">
										<h3 class="anim"><span class="titleanimation-bottom">호기심 많은 삶의 동반자를 보호하세요</span></h3>
										<p class="wow fadeInUp">매일 매일 몸단장 하기 바쁜 우리집 고양이. 벼룩, 귀진드기, 옴진드기 등 외부기생충으로 인해 괴로워 하고 있나요? 브라벡토로 12주간 당신의 반려묘를 보호해 주세요. </p>
									</div>
									<div class="benefits-list extrta-mb">
										<img src="/images/icons/simple02.png" alt="little girl feeding a cat" class="wow fadeInUp">
										<h3 class="anim"><span class="titleanimation-bottom">까다로운 입맛, 바르면 됩니다.</span></h3>
										<p class="wow fadeInUp">고양이에게 억지로 약을 먹인다는 것은 고양이에게도, 주인에게도 여간 힘든 일이 아니죠. 브라벡토 스팟온의 <a href="/page/spot-on-cats.php">TWIST’N’USE 기술</a>로 더 이상 스트레스 받지 않고 편하게 바르기만 하면 됩니다.</p>
									</div>
									<div class="benefits-list ">
										<img src="/images/icons/world02.png" alt="little girl feeding a cat" class="wow fadeInUp">
										<h3 class="anim"><span class="titleanimation-bottom">길고양이에게 발라주세요</span></h3>
										<p class="wow fadeInUp">
										도심 속 구석구석 숨어 사는 길고양이들은 특히나 외부기생충 노출에 취약하죠. 고양이용 브라벡토 스팟온은 12주 동안 외부기생충으로부터 길고양이를 자유롭게 할 뿐만 아니라, 길고양이를 집에 데려올 경우 외부기생충이 집안에 퍼지는 것 또한 예방합니다.</p>
									</div>
									<div class="benefits-list benefits-list-last mb-0 wow fadeInUp"> <a href="/page/benefits.php" class="findoutmore" title="Find out more"><span>더 알아보기</span></a> </div>
								</div>
							</div>
							<div class="col-md-12 col-lg-4 mobile-first ">
								<div class="where-to-get wow fadeInUp">
									<h2>브라벡토 구매를 원하시나요?</h2>
									<p>당신의 반려묘에게 가장 알맞은 제품에 대해 수의사와 상담하세요. 
									구입을 원하신다면 거주지역을 아래 검색창에 입력하세요.</p>
									<form action="/page/stockists.php">
										<input name="zipcode" type="text" placeholder="시/군/구 검색">
										<button class="arror-btn" type="submit"></button>
									</form>
								</div>
							</div>
						</div>
					</div>
				</section>

				<section class="purpal-background top-strip mobile-pedding">
					<div class="container-1 ">
						<div class="row no-gutters">
							<div class="col-md-12 col-lg-6 align-self-center moile-last">
								<div class="text-block ">
									<h2 class="anim"><span class="titleanimation-bottom">반려묘의 외부기생충</span></h2>
									<p class="wow fadeInUp">
										고양이가 외출을 하지 않는다고 외부기생충으로 부터 안전한 것은 아닙니다. 피부에 존재하는 옴진드기, 귀진드기 등 <a href="/page/mitemite.php">눈에 보이지 않는 진드기</a>로 온종일 긁는 모습을 보면 마음이 너무 아프죠. 또한 어쩌다 감염된 <a href="/page/flea.php">벼룩</a>으로 인해 걷잡을 수 없이 힘들어 질 수도 있습니다. 고양이용 브라벡토로 12주간 외부기생충으로 부터 고양이를 보호해 주세요.
									</p>
									<a href="/page/parasites.php" class="findoutmore wow fadeInUp" title="Find out more"><span>더 알아보기</span></a>
								</div>
							</div>
							<div class="col-md-12 col-lg-6 mobile-first"><img src="/images/home/photo1.jpg" class="w-100 wow fadeIn" alt="Meet the bad guys"></div>
						</div>
						<div class="row no-gutters">
							<div class="col-md-12 col-lg-6"><img src="/images/home/photo2.jpg" class="w-100 wow fadeIn" alt="Bravecto for Vets"></div>
							<div class="col-md-12 col-lg-6 align-self-center">
								<div class="text-block">
									<h2 class="anim"><span class="titleanimation-bottom">자신 있게 권하세요</span></h2>
									<p class="wow fadeInUp">
										브라벡토는 동물병원에 방문하는 반려묘 및 보호자의 일상을 바꿔줄 수 있습니다. MSD동물약품의 광범위한 임상 연구와 모니터링을 바탕으로 반려견 및 보호자에게 더욱 안전한 동물용의약품을 제공하세요. 세계적인 동물용의약품인 브라벡토만의 뛰어난 안전성과 효능을 더 전문적으로 알아보세요.
									</p>
									<a href="page/vets-resources.php" class="findoutmore wow fadeInUp" title="Find out more"><span>학술정보 찾아보기</span></a>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</div>
	<div class="mobile-only">
		<div class="prodduct-accordion accordion" id="product-collaps">
			<div class="card product-1">
				<div id="headingOne">
					<a class="product-link-mobile collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne" href="#">
						<img src="/images/icons/dog_icon.svg" width="39" alt="Bravecto Chew for Dogs">
						강아지용 브라벡토 츄어블
					</a>
				</div>
				<div id="collapseOne" class="collapse " aria-labelledby="headingOne" data-parent="#product-collaps">
					<div class="collapes-wraper">
						<div class="mobile-photo"> <img src="/images/home/mobile-chew-for-dogs.png" width="270" alt="Bravecto"> </div>
						<div class="mobile-slider">
							<div class="product-sdlier-mobile owl-carousel owl-theme ">
								<div class="slider-item">맛있는 브라벡토 츄어블정 단 한 알로 12주간 진드기(tick and mite) 및 벼룩(flea)으로부터 안심하고 산책이 가능합니다 </div>
								<div class="slider-item">임신 및 수유중인 개에서 안전하게 사용이 가능하며, 콜리종에서도 안전하게 사용이 가능합니다.</div>
								<div class="slider-item">바베시아증, 라임병, 중증열성혈소판감소증(SFTS) 등 다양한 질병을 매개하는 진드기 감염을 예방합니다.</div>
								<div class="slider-item">투여 후 4시간 이내에 작용하기 시작하여, 12시간 이내에 흡혈 진드기가 100% 사멸됩니다.</div>
<!-- 								<div class="slider-item">콜리종에서도 안전하게 사용이 가능합니다</div> -->
							</div>
						</div>
						<div class="usp-logo pull-left"><img src="/images/home/chew_logo.png" width="149" alt="3 Months Paralysis tick & Flea Protection"></div>
						<div class="pull-right "><a href="/page/chew-for-dogs.php" class="findoutmore bravecto_vets3" title="Find out more"><span>더알아보기</span></a></div>
						<div class="clearfix"></div>
					</div>
					<section class="video_con">
					<div class="container-1">
						<div class="video_box mobile">
							<iframe width="320" height="180" src="https://www.youtube.com/embed/JJjjqz6_mwY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
					</div>
				</section>
					
					<section class="alt-color" >
						<div class="container-1">
							<div class="row no-gutters">
								<div class="col-md-12 col-lg-8 moile-last">
									<div class="benefits">
										<div class="benefits-list benefits-list-first wow fadeInUp"> <a href="/page/benefits.php" class="redbg-link" title="BENEFITS">브라벡토 장점</a> </div>
										<div class="benefits-list extrta-mb ">
											<img src="/images/icons/longlasting.png" alt="little girl feeding a cat" class="wow fadeInUp">
											<h3 class="anim"><span class="titleanimation-bottom">호기심 많은 삶의 동반자를 보호하세요</span></h3>
											<p class="wow fadeInUp">도심 속 공원 산책, 깊은 산속 캠핑, 드넓은 해변가에서의 해수욕 등 
											우리의 반려견들은 자연을 만끽할 수 있는 액티비티를 할 때 가장 행복할 것입니다.
											브라벡토로 반려견에게 12주간의 행복을 선물해 주세요.</p>
										</div>
										<div class="benefits-list extrta-mb">
											<img src="/images/icons/simple.png" alt="little girl feeding a cat" class="wow fadeInUp">
											<h3 class="anim"><span class="titleanimation-bottom">맞습니다, 잘먹습니다!</span></h3>
											<p class="wow fadeInUp">몸에 좋은 약이 입에 쓰다지만, 약을 거부하는 강아지의 모습을 보는 것은 참으로 안타까운 일입니다.
											맛있게 먹는 반려견의 행복한 모습을 보는 것 만큼 뿌듯한 일이 있을까요?
											더 이상 약이 피부에 제대로 흡수되었는지, 털에 흘리거나 옷에 묻지 않았는지 고민하지 마세요. </p>
										</div>
										<div class="benefits-list ">
											<img src="/images/icons/world.png" alt="little girl feeding a cat" class="wow fadeInUp">
											<h3 class="anim"><span class="titleanimation-bottom">1억 도스 판매 돌파</span></h3>
											<p class="wow fadeInUp">
											브라벡토는 출시 후 광범위한 임상 연구와 모니터링으로 반려동물이 더욱 안전하고 행복할 수 있도록 노력해 왔습니다. 이러한 노력으로 전 세계 85여개 국가에서 출시 5주년인 2019년, 1억 도스 판매를 돌파하였으며, 그 안전성을 검증 받았습니다.</p>
										</div>
										<div class="benefits-list benefits-list-last mb-0 wow fadeInUp"> <a href="/page/benefits.php" class="findoutmore bravecto_vets2" title="Find out more"><span>더 알아보기</span></a> </div>
									</div>
								</div>
								<div class="col-md-12 col-lg-4 mobile-first ">
									<div class="where-to-get wow fadeInUp">
										<h2>브라벡토 구매를 원하시나요?</h2>
										<p>당신의 반려견에게 가장 알맞은 제품에 대해 수의사와 상담하세요. 
										구입을 원하신다면 거주지역을 아래 검색창에 입력하세요.</p>
										<form action="/page/stockists.php">
											<input name="zipcode" type="text" placeholder="시/군/구 검색">
											<button class="arror-btn" type="submit"></button>
										</form>
									</div>
								</div>
							</div>
						</div>
					</section>

					<section class="purpal-background top-strip mobile-pedding">
						<div class="container-1 ">
							<div class="row no-gutters">
								<div class="col-md-12 col-lg-6 align-self-center moile-last">
									<div class="text-block ">
										<h2 class="anim"><span class="titleanimation-bottom">반려견의 외부기생충</span></h2>
										<p class="wow fadeInUp">
											참진드기가 반려견 몸에 달라붙어 흡혈하는 과정 중에 <a href="/page/parasites.php">다양한 진드기 매개성 질환</a>[중증열성혈소판감소증(SFTS), 라임병, 바베시아증, 아나플라즈마증, 에를리히증 등] 이 강아지로 전염되어 죽음까지도 유발할 수 있는 치명적인 결과를 초래할 수 있습니다. 참진드기 외에도 <a href="/page/flea.php">벼룩</a>, <a href="/page/mitemite.php">모낭충, 귀진드기, 옴진드기</a> 등 다양한 외부기생충이 우리 반려견의 행복을 위협하죠. 이러한 외부기생충 매개 질환은 브라벡토로 간단하게 예방이 가능합니다. 

										</p>
									</div>
								</div>
								<div class="col-md-12 col-lg-6 mobile-first"><img src="/images/home/photo1_2.jpg" class="w-100 wow fadeIn" alt="Meet the bad guys"></div>
							</div>
							<div class="row no-gutters">
								<div class="col-md-12 col-lg-6"><img src="/images/home/photo2.jpg" class="w-100 wow fadeIn" alt="Bravecto for Vets"></div>
								<div class="col-md-12 col-lg-6 align-self-center">
									<div class="text-block">
										<h2 class="anim"><span class="titleanimation-bottom">자신 있게 권하세요</span></h2>
										<p class="wow fadeInUp">
											브라벡토는 동물병원에 방문하는 반려견 및 보호자의 일상을 바꿔줄 수 있습니다.
											MSD동물약품의 광범위한 임상 연구와 모니터링을 바탕으로 반려견 및 보호자에게 
											더욱 안전한 동물용의약품을 제공하세요. 
											세계적인 동물용의약품인 브라벡토만의 독보적인 안전성과 효능을 더 전문적으로 알아보세요.
										</p>
										<a href="/page/vets-resources.php" class="findoutmore wow fadeInUp bravecto_vets" title="Find out more"><span class="">학술정보 찾아보기</span></a>
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
			</div>
			<!--
			<div class="card product-2">
				<div id="headingTwo">
					<a class="product-link-mobile collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" href="#">
						<img src="/images/icons/dog_icon.svg" width="39" alt="Bravecto Spot-on for Dogs">
						강아지용 브라벡토 스팟온
					</a>
				</div>
				<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#product-collaps">
					<div class="collapes-wraper">
						<div class="mobile-photo"> <img src="/images/home/mobile-spot-on-dogs.jpg" width="270" alt="Bravecto"> </div>
						<div class="mobile-slider">
							<div class="product-sdlier-mobile owl-carousel owl-theme ">
								<div class="slider-item">1회 사용만으로 12주간 외부기생충으로부터 안심하고 산책할 수 있습니다</div>
								<div class="slider-item">TWIST’N’USE 기술로 사용이 더욱 간단하고 편리합니다</div>
								<div class="slider-item">4시간 이전에 작용하기 시작하여, 12시간 이내에 진드기의 100%가 사멸합니다</div>
								<div class="slider-item">외부기생충으로 인한 각종 질병을 예방합니다</div>
								<div class="slider-item">
									<ul class="icon-li">
										<li class="flees"><a href="#">벼룩<strong>12주</strong></a> </li>
										<li class="paral"><a href="#">참진드기<strong>12주</strong></a> </li>
										<li class="brown"><a href="#">귀진드기<strong>12주</strong></a> </li>
										<li class="brown"><a href="#">옴진드기<strong>12주</strong></a> </li>
										<li class="brown"><a href="#">모낭충<strong>12주</strong></a> </li>
										<li><br /><br /></li>
									</ul>
									<p class="small-text">진드기(tick and mite) 및 벼룩 구제(12주 지속)
									임신, 분만 및 수유중인 개에서 안전하게 사용이 가능합니다
									콜리종에서도 안전하게 사용이 가능합니다</p>
								</div>
							</div>
						</div>
						<div class="usp-logo pull-left"><img src="/images/home/logo-6-months.png" alt="6 Months Paralysis tick & Flea Protection" width="149"></div>
						<div class="pull-right "><a href="/page/spot-on-dogs.php" class="findoutmore" title="Find out more"><span>&nbsp;</span></a></div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			-->
			<div class="card product-3">
				<div id="headingThree">
					<a class="product-link-mobile collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree" href="#">
						<img src="/images/icons/cat_icon.svg" width="35" alt="Bravecto Spot-on for Cats">
						고양이용 브라벡토 스팟온
					</a>
				</div>
				<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#product-collaps">
					<div class="collapes-wraper">
						<div class="mobile-photo"> 
						<img src="/images/home/mobile-spot-on-cats.png" width="270" alt="Bravecto"> </div>
						<div class="mobile-slider">
							<div class="product-sdlier-mobile owl-carousel owl-theme ">
								<div class="slider-item">1회 사용만으로 12주간 고양이를 각종 외부기생충으로부터 보호합니다.</div>
								<div class="slider-item">TWIST’N’USE 기술로 사용이 더욱 간단하고 편리합니다</div>
								<div class="slider-item">외부기생충 매개 질환을 예방합니다.</div>
								<div class="slider-item">9주령 및 1.2kg가 조금 넘는 새끼고양이에서도 안전하게 사용이 가능합니다.</div>
								<div class="slider-item">2020년 출시 예정</div>
							</div>
						</div>
						<div class="usp-logo pull-left"><img src="/images/home/chew_logo.png" alt="Up to 3 Months Paralysis tick & Flea Protection" width="149"></div>
						<div class="pull-right "><a href="/page/spot-on-cats.php" class="findoutmore bravecto_vets3" title="Find out more"><span>더알아보기</span></a></div>
						<div class="clearfix"></div>
					</div>
					
						
					<section class="alt-color">
						<div class="container-1">
							<div class="row no-gutters">
								<div class="col-md-12 col-lg-8 moile-last">
									<div class="benefits">
										<div class="benefits-list benefits-list-first wow fadeInUp"> <a href="/page/benefits.php" class="redbg-link" title="BENEFITS">브라벡토 장점</a> </div>
										<div class="benefits-list extrta-mb ">
											<img src="/images/icons/longlasting02.png" alt="little girl feeding a cat" class="wow fadeInUp">
											<h3 class="anim"><span class="titleanimation-bottom">호기심 많은 삶의 동반자를 보호하세요</span></h3>
											<p class="wow fadeInUp">매일 매일 몸단장 하기 바쁜 우리집 고양이. 벼룩, 귀진드기, 옴진드기 등 외부기생충으로 인해 괴로워 하고 있나요? 브라벡토로 12주간 당신의 반려묘를 보호해 주세요. </p>
										</div>
										<div class="benefits-list extrta-mb">
											<img src="/images/icons/simple02.png" alt="little girl feeding a cat" class="wow fadeInUp">
											<h3 class="anim"><span class="titleanimation-bottom">까다로운 입맛, 바르면 됩니다.</span></h3>
											<p class="wow fadeInUp">고양이에게 억지로 약을 먹인다는 것은 고양이에게도, 주인에게도 여간 힘든 일이 아니죠. 브라벡토 스팟온의 <a href="/page/spot-on-cats.php">TWIST’N’USE 기술</a>로 더 이상 스트레스 받지 않고 편하게 바르기만 하면 됩니다.</p>
										</div>
										<div class="benefits-list ">
											<img src="/images/icons/world02.png" alt="little girl feeding a cat" class="wow fadeInUp">
											<h3 class="anim"><span class="titleanimation-bottom">길고양이에게 발라주세요</span></h3>
											<p class="wow fadeInUp">
											도심 속 구석구석 숨어 사는 길고양이들은 특히나 외부기생충 노출에 취약하죠. 고양이용 브라벡토 스팟온은 12주 동안 외부기생충으로부터 길고양이를 자유롭게 할 뿐만 아니라, 길고양이를 집에 데려올 경우 외부기생충이 집안에 퍼지는 것 또한 예방합니다.</p>
										</div>
										<div class="benefits-list benefits-list-last mb-0 wow fadeInUp"> <a href="/page/benefits.php" class="findoutmore bravecto_vets2" title="Find out more"><span>더 알아보기</span></a> </div>
									</div>
								</div>
								<div class="col-md-12 col-lg-4 mobile-first ">
									<div class="where-to-get wow fadeInUp">
										<h2>브라벡토 구매를 원하시나요?</h2>
										<p>당신의 반려묘에게 가장 알맞은 제품에 대해 수의사와 상담하세요. 
										구입을 원하신다면 거주지역을 아래 검색창에 입력하세요.</p>
										<form action="/page/stockists.php">
											<input name="zipcode" type="text" placeholder="시/군/구 검색">
											<button class="arror-btn" type="submit"></button>
										</form>
									</div>
								</div>
							</div>
						</div>
					</section>

					<section class="purpal-background top-strip mobile-pedding">
						<div class="container-1 ">
							<div class="row no-gutters">
								<div class="col-md-12 col-lg-6 align-self-center moile-last">
									<div class="text-block ">
										<h2 class="anim"><span class="titleanimation-bottom">반려묘의 외부기생충</span></h2>
										<p class="wow fadeInUp">
											고양이가 외출을 하지 않는다고 외부기생충으로 부터 안전한 것은 아닙니다. 피부에 존재하는 옴진드기, 귀진드기 등 <a href="/page/mitemite.php">눈에 보이지 않는 진드기</a>로 온종일 긁는 모습을 보면 마음이 너무 아프죠. 또한 어쩌다 감염된 <a href="/page/flea.php">벼룩</a>으로 인해 걷잡을 수 없이 힘들어 질 수도 있습니다. 고양이용 브라벡토로 12주간 외부기생충으로 부터 고양이를 보호해 주세요.
										</p>
										<a href="/page/mitemite.php" class="findoutmore wow fadeInUp bravecto_vets2" title="Find out more"><span>더 알아보기</span></a>
									</div>
								</div>
								<div class="col-md-12 col-lg-6 mobile-first"><img src="/images/home/photo1.jpg" class="w-100 wow fadeIn" alt="Meet the bad guys"></div>
							</div>
							<div class="row no-gutters">
								<div class="col-md-12 col-lg-6"><img src="/images/home/photo2.jpg" class="w-100 wow fadeIn" alt="Bravecto for Vets"></div>
								<div class="col-md-12 col-lg-6 align-self-center">
									<div class="text-block">
										<h2 class="anim"><span class="titleanimation-bottom">자신 있게 권하세요</span></h2>
										<p class="wow fadeInUp">
											브라벡토는 동물병원에 방문하는 반려묘 및 보호자의 일상을 바꿔줄 수 있습니다. MSD동물약품의 광범위한 임상 연구와 모니터링을 바탕으로 반려견 및 보호자에게 더욱 안전한 동물용의약품을 제공하세요. 세계적인 동물용의약품인 브라벡토만의 뛰어난 안전성과 효능을 더 전문적으로 알아보세요.
										</p>
										<a href="/page/vets-resources.php" class="findoutmore wow fadeInUp bravecto_vets" title="Find out more"><span class="">학술정보 찾아보기</span></a>
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
			</div>
		</div>
	</div>
	</section>
	<div class="footer_top_txt_box">
		<div class="container-1">
			<p>
				1. Kidd L, Breitschwerdt EB. Transmission times and prevention of tick-borne diseases in dogs. Compend Contin Educ Pract Vet. 2003;(25)10:742-751.<br/>
				2. Taenzler J, Liebenberg J, Roepke RKA, Heckeroth AR. Prevention of transmission of Babesia canis by Dermacentor reticulatus ticks to dogs treated orally with fluralaner chewable tablets (Bravecto™).<br/>
				Parasites & Vectors. 2015;8:305.
			</p>
		</div>
	</div>

	
	<? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>

	<script>
		$(window).resize(function(){
			var winW = $(this).width();
			if(winW > 991){
				var link = $('.desktop-only .video_con .video_box iframe').attr('src');
				var header_h = $('header').height();
				var nav_h = $('.nav').height();
				var divOffset = $('.desktop-only .video_con').offset().top + header_h + nav_h;
				var oneStart = 'true';
				$(window).scroll(function(){
					var position = $(window).scrollTop();
					
					if (oneStart === 'true'){
						if(position - divOffset > 0){
							$('.desktop-only .video_box iframe').attr('src', link + '?autoplay=1');
							oneStart = 'false';
						}
					}
				});
			}else{
				var link = $('.mobile-only .video_con .video_box iframe').attr('src');
				var header_h = $('header').height();
				var nav_h = $('.nav').height();
				var divOffset = $('.mobile-only .video_con').offset().top + header_h + nav_h;
				var oneStart = 'true';
				$(window).scroll(function(){
					var position = $(window).scrollTop();
					
					if (oneStart === 'true'){
						if(position - divOffset > 0){
							$('.mobile-only .video_box iframe').attr('src', link + '?autoplay=1');
							oneStart = 'false';
						}
					}
				});
			}
		}).resize();

		/*

		var link = $('.video_con .video_box iframe').attr('src');
		var header_h = $('header').height();
		var nav_h = $('.nav').height();
		var divOffset = $('.video_con').offset().top + header_h + nav_h;
		var oneStart = 'true';
		$(window).scroll(function(){
			var position = $(window).scrollTop();
			
			if (oneStart === 'true'){
				if(position - divOffset > 0){
					$('.video_con .video_box iframe').attr('src', link + '?autoplay=1');
					oneStart = 'false';
				}
			}
		});
		*/
	</script>
</body>
</html>
		
