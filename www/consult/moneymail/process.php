<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Moneymail.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/email/SendMail.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/log/Log.class.php";


include "config.php";

if ($boardgrade == 3 || $boardgrade == 2) { // 게시판접근 권한처리
	include $_SERVER['DOCUMENT_ROOT']."/include/gradeCheck.php";
}

$mm = new Moneymail($pageRows, $tablename, $listtablename, $_REQUEST);


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/include/headHtml.php" ?>
</head>
<body>
<?
if (checkReferer($_SERVER["HTTP_REFERER"])) {
	
	if ($_REQUEST['cmd'] == 'send') {

		$_REQUEST['no'] = $_REQUEST['emailhistory_fk'];
		if($multiSend){
		$_REQUEST['sending_count'] = 1;
		$data = $mm->getData($_REQUEST['no']);
		
		
		$filename = array();
		$filename_org = array();
		if ($data['filename']) {
			$filename[0] = $data['filename'];
			$filename_org[0] = $data['filename_org'];
		}
		if ($data['filename2']) {
			$filename[1] = $data['filename2'];
			$filename_org[1] = $data['filename2_org'];
		}
		if ($data['filename3']) {
			$filename[2] = $data['filename3'];
			$filename3_org[2] = $data['filename3_org'];
		}
		$sendmail = new SendMail();

		// 메일발송
		$sendmail->sendFile(COMPANY_EMAIL, "", $_REQUEST['receiveemail'], $data['title'], str_replace("\\", "", $data['contents']), $uploadPath, $filename, $filename_org);
		}else{
			$_REQUEST['sending'] = 1;
			
		}
		$r = $mm->insertList($_REQUEST);

		// 방문자 통계 서비스가 유료일때 통계데이터 update
		if (IS_LOG && LOG_TYPE == 1) {
			$log = new Log($tablename);
			$log->setLog($_COOKIE["CONNECTID_".WEBLOG_NUMBER], $_REQUEST[no]);
		}

		if ($r > 0) {
			echo returnURLMsg($mm->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), '정상적으로 발송되었습니다.');
		} else {
			echo returnURLMsg($mm->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), '요청처리중 장애가 발생하였습니다.');
		}
	} 


} else {
	echo returnURLMsg($mm->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), '요청처리중 장애가 발생하였습니다.1');
}
?>
</body>
</html>