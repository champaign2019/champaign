<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Moneymail.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Hospital.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/environment/Stipulation.class.php";

include "config.php";

if ($boardgrade == 3 || $boardgrade == 2) { // 게시판접근 권한처리
	include $_SERVER['DOCUMENT_ROOT']."/include/gradeCheck.php";
}

$mm = new Moneymail($pageRows, $tablename, $listtablename, $_REQUEST);

$rowPageCount = $mm->getCountList($_REQUEST);
$result = $mm->getListList($_REQUEST);
if($_REQUEST['view_loc_type'] != 1){ //제거 하기 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/include/headHtml.php" ?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/boardConfig/config.php" ?>
<script>

function goSave(obj) {
	
	
	if(validation(obj)){
		<?if(!$isCellSum){?>
		if($("[name=cell1]").val() != "" && $("input[name=cell2]").val() != "" && $("input[name=cell3]").val() != ""){
			$("input[name=cell]").val($("[name=cell1]").val()+"-"+$("input[name=cell2]").val()+"-"+$("input[name=cell3]").val())
		}
		<?}?>
	
		var form = $("#frm");

		fn_formSpanCheck(form,key);	
		return true;	
	}else{
		return false;
	}
}

var rowCount = '<?=$rowPageCount[0]?>';
$(window).load(function(){
	
	
	if($(".program_table tbody tr").size() == rowCount){
		$(".mo_programPage").hide();	
	}
	
});

function more(){
	
	//다음페이지 계산
	var reqPageNo = (Math.ceil($(".program_table tbody tr").size()/<?=$pageRows?>)+1);
	
	$.post("index.php",
	{
		reqPageNo : reqPageNo,
		stype : $("#stype").val(),
		view_loc_type : 1,
		sval : $("#sval").val()
		
	},function(data){
		
		$(".program_table tbody").append(data);
		if($(".program_table tbody tr").size() == rowCount){
			$(".mo_programPage").hide();
		}
		
		
	}).fail(function(error){
		
	})
	
}
</script>
</head>

<body>

<div id="wrap">
              <? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
              <? include "sub_visual/sub_visual.php" ?>
              <!-- 프로그램시작 -->
<div class="programCon">
<form name="frm" id="frm" action="<?=getSslCheckUrl($_SERVER['REQUEST_URI'], 'process.php')?>" method="post" enctype="multipart/form-data" onsubmit="return goSave(this);" >
		<?
			if (!$loginCheck) { 
			
			$st = new Stipulation();
			$sv = $st->getData();
			
		?>
		<div class="join" data-aos="fade-down" data-aos-delay="400">
			<div class="joinTop">
				<div class="ScrollLy">
					<?=str_replace("#company_name#", COMPANY_NAME, $sv['privacy_mini_text'])?>	
					<!-- //policy -->
				</div>
				<div class="jcheck">
					<input type="checkbox" data-value="<?=getMsg('alt.checkbox.agree') ?>" id="agree02" name="agree02" value="" title="<?=getMsg('message.tbody.agree_ch') ?>" checked="checekd"/>
					<label for="agree02"> <?=getMsg('message.tbody.agree') ?></label>
				</div>
				<!-- //jcheck -->
			</div>
		</div>
		<?}?>
		<!-- //top -->
		<? 
		if($branch) { 
		$hospital = new Hospital(999, $_REQUEST);
		$hResult = $hospital->branchSelect();
		?>
		<div class="writeForm writeForm_top" data-aos="fade-down" data-aos-delay="400">
			<table>
				<colgroup>
					<col class="writeForm_col01" />
					<col width="*" />
				</colgroup>			
				
				<tr>
					<th><?=getMsg('th.branch')?></th>
					<td>
						
						<select name="hospital_fk" id="hospital_fk" onchange="" data-value="<?=getMsg('alert.text.shospital_fk') ?>">
							<option value=""><?=getMsg('lable.option.branch') ?></option>
							<? while ($row=mysql_fetch_assoc($hResult)) { ?>				
							<option value="<?=$row['no']?>"><?=$row['name']?></option>
							<? } ?>
						</select>	
					</td>
				</tr>
				
			</table>
		</div>
		<?
		}
		?>
		
		<div class="writeForm" data-aos="fade-down" data-aos-delay="400">
			<table>
				<colgroup>
					<col class="writeForm_col01" />
					<col width="*" />
				</colgroup>
				<tr>
					<th>이름</th>
					<td>
						<input data-value="<?=getMsg('alert.text.name2') ?>" id="name" name="name" class="inputTxt inputName" type="text" value="<?=$_SESSION['member_name']?>" />
					</td>
				</tr>
				
				<tr>
					<th><?=getMsg('th.cell') ?></th>
					<td>
						<?
						if(!$isCellSum){
						?>
						<select class="selectNum" name="cell1" id="cell1">
							<?=getOptsCell(getSplitIdx($_SESSION['member_cell'], "", 0))?>
						</select>
						<input data-value="<?=getMsg('alert.text.cell') ?>" name="cell2" id="cell2" class="inputNum" type="text" value="<?=getSplitIdx($_SESSION['member_cell'], "-", 1)?>" maxlength="4" onkeyup="isNumberOrHyphen2(this);" />
						<input data-value="<?=getMsg('alert.text.cell') ?>" name="cell3" id="cell3" class="inputNum" type="text" value="<?=getSplitIdx($_SESSION['member_cell'], "-", 2)?>" maxlength="4" onkeyup="isNumberOrHyphen2(this);" />
						<input type="hidden" name="cell" id="cell" value=""/>
						<?}else{ ?>
						<input type="text" name="cell" id="cell" value="<?=$_SESSION['member_cell'] ?>" onkeyup="isNumberOrHyphen(this);" onblur="cvtUserPhoneNumber(this)" data-value="<?=getMsg('alert.text.cell') ?>" />
						<?}?>
						<?
						if($useSmsChk){
						?>
						<span class="label_wrap"><input type="checkbox" id="Num_check" name="iscall" value="1" /><label for="Num_check"><?=getMsg('th.telconsult') ?></label></span>
						<?
						}
						?>
					</td>
				</tr>
				
				<tr>
					<th><?=getMsg('th.email') ?></th>
					<td colspan="3">
					<input type="text" id="receiveemail" name="receiveemail" value="<?=$_SESSION['member_email'] ?>" class="inputLong"  title="이메일주소를 입력해주세요." data-value="이메일 주소를 입력하세요." />
					</td>
				</tr>
				<tr>
					<th><?=getMsg('th.program') ?></th>
					<td>
						<?
							$oresult = $mm->selectOptionList($_REQUEST);
						?>
						<select name="emailhistory_fk" id="emailhistory_fk">
						<? while ($row=mysql_fetch_assoc($oresult)) { ?>
							<option value="<?=$row['no']?>" <?=getSelected($row['no'], $_REQUEST['semailhistory_fk'])?>><?=$row['gubun']?></option>
						<? } ?>
						</select>
						
					</td>
				</tr>
			</table>
		</div>
	<!-- //bwrite -->
		<div class="writeForm_btn" data-aos="fade-down" data-aos-delay="400">
			<a href="javascript:;" id="w_btn" onclick="$('#frm').submit()" class="mcol_type">비용메일 받기</a>
		</div>
	<input type="hidden" name="cmd" id="cmd" value="send"/>
	<input type="hidden" name="tablename" id="tablename" value="<?=$tablename?>"/>
	<? if($loginCheck){ ?>
	<input type="hidden" name="member_fk" id="member_fk" value="<?=$_SESSION['member_no']?>"/>
	<? } ?>
	<input type="hidden" name="listtablename" id="listtablename" value="<?=$listtablename?>"/>
	</form>
	<form name="board" id="board" method="post" action="process.php" onSubmit="return goSave();">
	<div class="program_table titCenter_table mt60" data-aos="fade-down" data-aos-delay="400">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="온라인상담 게시판입니다.">
			<colgroup>
			<col width="8%" />
			<col width="*" />
			<col width="15%" />
			<col width="15%" />
			</colgroup>
			<thead>
				<tr>
					<th scope="col" class="none1000">&nbsp;</th>
					<th scope="col"><?=getMsg('th.contents') ?></th>
					<th scope="col"><?=getMsg('th.email') ?></th>
					<th scope="col">신청일</th>
				</tr>
			</thead>
			<tbody>
<? } ?>
			<? if ($rowPageCount[0] == 0) { ?>
				<tr class="bbsno">
					<td colspan="4">등록된 게시물이 없습니다</td>
				</tr>
			<?
				} else {
						$i = 0;
						while ($row=mysql_fetch_assoc($result)) {
							$name = mb_substr($row['name'], 0, 1, "UTF-8")."**";
							$email = mb_substr($row['receiveemail'], 0, 1, "UTF-8")."****@****.***";
				?>
					<tr>
						<td class="none1000"><?=($rowPageCount[0]-($mm->reqPageNo-1)*$pageRows-$i)?></td>
						<td class="tit"><?=$name?>님의 비용문의가 접수되었습니다.</td>
						<td><?=$email?></td>
						<td><?=getYMD($row['registdate'])?></td>
					</tr>
			<?		$i++;
					}
				}
			?>
			<? if($_REQUEST['view_loc_type'] != 1){ //제거 하기 ?>
			</tbody>
		</table>
		<div class="mo_programPage"><a href="javascript:;" onclick="more()"><?=getMsg('btn.more') ?><span>+</span></a></div>
		<?=pageListUser($mm->reqPageNo, $rowPageCount[1], $mm->getQueryString('index.php', 0, $_REQUEST))?><!-- 페이징 처리 -->
	</div>
	<!-- //blist -->
	</form>
</div>
	<!-- 프로그램끝 -->
        <? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
	<? include "sub_footer/sub_footer.php" ?>
</div>
</body>
</html>

<? } ?>