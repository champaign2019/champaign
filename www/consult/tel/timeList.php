<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import = "java.util.*" %>
<%@ page import="com.vizensoft.config.*" %>
<%@ page import="com.vizensoft.util.*" %>
<%@ page import="com.vizensoft.reservation.*" %>
<%@ page import="com.vizensoft.management.hospital.*" %>
<%@ include file="config.php" %>
<%


ReservationVO param = ReservationVO.getParam(pageRows,"", request);
ReservationDAO mgr = ReservationDAO.getInstance();									// 게시판 메니져의 인스턴스 생성


ArrayList<ReservationVO> data = mgr.resertimeList(param);


%>
<div>
<p>오전</p>
<ul>
<%
	for (int i = 0 ; i < data.size() ; i++) {
		
		if(Function.getIntParameter(data.get(i).getResertime().substring(0,2)) >= 13){continue;}
		
	

		String liClick = "data-value='"+data.get(i).getResertime()+"' style='cursor:pointer;' onclick=\"timeSelectedValue(this);\"";

		if("0".equals(data.get(i).getReseryesno())) {
			if (Function.checkNull(param.getResertime()).equals(data.get(i).getResertime())) {
%>
	<li <%=liClick %>><%=data.get(i).getResertime().substring(0,5)%>(예약가능)</li>
<%
			} else {
%>
	<li <%=liClick %>><%=data.get(i).getResertime().substring(0,5)%>(예약가능)</li>
<%
			}
		} else {
			if (Function.checkNull(param.getResertime()).equals(data.get(i).getResertime())) {
%>
	<li <%=liClick %> ><%=data.get(i).getResertime().substring(0,5)%>(현재예약)</li>
<%
	} else {
		if(SiteProperty.RESER_JUNGBOK){
%>
	<li  <%=liClick%>><%=data.get(i).getResertime().substring(0,5)%>(<%=data.get(i).getReser_cnt()%>명 예약중)</li>	
<%
		} else {
	%>
	<li class="timeNo"><%=data.get(i).getResertime().substring(0,5)%>(예약불가능)</li>
<%
	}
	}
		}

	}
%>
</ul>
</div>
<div>
<p>오후</p>
<ul>
<%
	for (int i = 0 ; i < data.size() ; i++) {
		
		if(Function.getIntParameter(data.get(i).getResertime().substring(0,2)) < 13){continue;}
		
	

		String liClick = "data-value='"+data.get(i).getResertime()+"' style='cursor:pointer;' onclick=\"timeSelectedValue(this);\"";

		if("0".equals(data.get(i).getReseryesno())) {
	if (Function.checkNull(param.getResertime()).equals(data.get(i).getResertime())) {
%>
	<li <%=liClick%>><%=data.get(i).getResertime().substring(0,5)%>(예약가능)</li>
<%
	} else {
%>
	<li <%=liClick%>><%=data.get(i).getResertime().substring(0,5)%>(예약가능)</li>
<%
	}
		} else {
	if (Function.checkNull(param.getResertime()).equals(data.get(i).getResertime())) {
%>
	<li <%=liClick%> ><%=data.get(i).getResertime().substring(0,5)%>(현재예약)</li>
<%
	} else {
		if(SiteProperty.RESER_JUNGBOK){
%>
	<li  <%=liClick %>><%=data.get(i).getResertime().substring(0,5)%>(<%=data.get(i).getReser_cnt()%>명 예약중)</li>	
<%
				} else {
%>
	<li class="timeNo"><%=data.get(i).getResertime().substring(0,5)%>(예약불가능)</li>
<%
				}
			}
		}

	}
%>
</ul>
</div>
