<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Common.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Hospital.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Clinic.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/environment/Stipulation.class.php";

include "config.php";

if ($boardgrade == 3 || $boardgrade == 2) { // 게시판접근 권한처리
	include $_SERVER['DOCUMENT_ROOT']."/include/gradeCheck.php";
}

$_REQUEST['orderby'] = $orderby; //정렬 배열 선언

$objCommon = new Common($pageRows, $tablename, $_REQUEST);

$rowPageCount = $objCommon->getCount($_REQUEST);
$result = $objCommon->getList($_REQUEST);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/include/headHtml.php" ?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/boardConfig/config.php" ?>

<script>

function goSave(obj) {
	
	
	if(validation(obj)){
		<?if(!$isCellSum){?>
		if($("[name=cell1]").val() != "" && $("input[name=cell2]").val() != "" && $("input[name=cell3]").val() != ""){
		$("input[name=cell]").val($("[name=cell1]").val()+"-"+$("input[name=cell2]").val()+"-"+$("input[name=cell3]").val())
		}
		<?}?>
	
		if(!fn_spamCheck()){return false;}
		
		var form = $("#frm");

		fn_formSpanCheck(form,key);	
		return true;	
	}else{
		return false;
	}
}
</script>
</head>

<body>

<div id="wrap">
	<? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
	<? include "sub_visual/sub_visual.php" ?>
	<!-- 프로그램시작 -->
	<form name="frm" id="frm" action="<?=getSslCheckUrl($_SERVER['REQUEST_URI'], 'process.php')?>" method="post" enctype="multipart/form-data" onsubmit="return goSave(this);">
	<div class="programCon">
		
		<?
			if (!$loginCheck) { 
			
			$st = new Stipulation();
			$sv = $st->getData();
			
		?>
		<div class="join" data-aos="fade-down" data-aos-delay="400">
			<div class="joinTop">
				<div class="ScrollLy">
					<?=str_replace("#company_name#", COMPANY_NAME, $sv['privacy_mini_text'])?>	
					<!-- //policy -->
				</div>
				<div class="jcheck">
					<input type="checkbox" data-value="<?=getMsg('alt.checkbox.agree') ?>" id="agree02" name="agree02" value="" title="<?=getMsg('message.tbody.agree_ch') ?>" checked="checekd"/>
					<label for="agree02"> <?=getMsg('message.tbody.agree') ?></label>
				</div>
				<!-- //jcheck -->
			</div>
		</div>
		<?}?>
		<!-- //join -->
		<? 
		if($branch || $clinic) { 
		?>
		<div class="writeForm writeForm_top" data-aos="fade-down" data-aos-delay="400">
			<table>
				<colgroup>
					<?
					if($branch){
					?>
					<col class="writeForm_col01" />
					<col width="*" />
					<?
					}
					?>
					<?
					if($clinic){
					?>
					<col class="writeForm_col01" />
					<col width="*" />
					<?
					}
					?>
				</colgroup>			
				<tr>
					<?
					if($branch){
						$hospital = new Hospital(999, $_REQUEST);
						$hResult = $hospital->branchSelect();
					?>
					<th><?=getMsg('th.branch')?></th>
					<td>
						
						<select name="hospital_fk" id="hospital_fk" onchange="" data-value="<?=getMsg('alert.text.shospital_fk') ?>">
							<option value=""><?=getMsg('lable.option.branch') ?></option>
							<? while ($row=mysql_fetch_assoc($hResult)) { ?>				
							<option value="<?=$row['no']?>"><?=$row['name']?></option>
							<? } ?>
						</select>	
					</td>
					<?
					}
					?>
					<?
					if($clinic){
						$clinicObj = new Clinic(999, $_REQUEST);
						$clinicList = $clinicObj->selectBoxList(0,($branch ? 0 : DEFAULT_BRANCH_NO),$_REQUEST['sclinic_fk']);
						
					?>
					<th><?=getMsg('th.clinic_fk') ?></th>
					<td>
						<select name="clinic_fk" id="clinic_fk" data-value="<?=getMsg('alt.select.sclinic_fk') ?>" >
							<?= $clinicList ?>
						</select>
					</td>
					<?
					}
					?>
				</tr>
			</table>
		</div>
		<?
		}
		?>
		<div class="schedule_wrap" data-aos="fade-down" data-aos-delay="500">			
			<div class="schedule"></div>
			<div class="schedule_time"></div>
			
		</div>
		<div class="writeForm" data-aos="fade-down" data-aos-delay="500">
			<table>
				<colgroup>
					<col class="writeForm_col01" />
					<col width="*" />
				</colgroup>
				<tr>
					<th><?=getMsg('th.name2') ?></th>
					<td>
						<input data-value="<?=getMsg('alert.text.name2') ?>" id="name" name="name" class="inputTxt inputName" type="text" value="<?=$member_name?>" style="width:20%;min-width:140px;" />
					</td>
				</tr>
				<tr>
					<th><?=getMsg('th.cell') ?></th>
					<td>
						<?
						if(!$isCellSum){
						?>
						<select class="selectNum" name="cell1" id="cell1">
							<?=getOptsCell(getSplitIdx($_SESSION['member_cell'], "", 0))?>
						</select>
						<input data-value="<?=getMsg('alert.text.cell') ?>" name="cell2" id="cell2" class="inputNum" type="text" value="<?=getSplitIdx($_SESSION['member_cell'], "-", 1)?>" maxlength="4" onkeyup="isNumberOrHyphen2(this);" />
						<input data-value="<?=getMsg('alert.text.cell') ?>" name="cell3" id="cell3" class="inputNum" type="text" value="<?=getSplitIdx($_SESSION['member_cell'], "-", 2)?>" maxlength="4" onkeyup="isNumberOrHyphen2(this);" />
						<input type="hidden" name="cell" id="cell" value=""/>
						<?}else{ ?>
						<input type="text" name="cell" id="cell" value="<?=$_SESSION['member_cell'] ?>" class="w80" onkeyup="isOnlyNumberNotHypen(this);"  data-value="<?=getMsg('alert.text.cell') ?>" style="width:30%;min-width:200px;" />
						<?}?>
						<!-- <span class="label_wrap"><input type="checkbox" id="Num_check" name="iscall" value="1" /><label for="Num_check"><?=getMsg('th.telconsult') ?></label></span>
						<input type="hidden" name="cell" id="cell" value=""/> -->
					</td>
				</tr>
			</table>
		</div>
		<div class="writeForm_btn" data-aos="fade-down" data-aos-delay="400">
			<a href="javascript:;" id="w_btn" onclick="$('#frm').submit()"><?=getMsg('btn.submit') ?></a>
			<a href="#" onClick="reset();"><?=getMsg('btn.cancel') ?></a>
		</div>
		<div class="program_table titCenter_table mt60" data-aos="fade-down" data-aos-delay="400">
			<table>
				<colgroup>
					<col class="w80 none1000" />
					<col class="*" />
					<col class="w160" />
					<col class="w150" />
				</colgroup>
				<thead>
					<tr>
						<th class="none1000"><?=getMsg('th.no')?></th>
						<th><?=getMsg('th.title') ?></th>
						<th><?=getMsg('th.tel') ?></th>
						<th><?=getMsg('th.registdate') ?></th>
					</tr>
				</thead>
				<tbody class="programBody">
				<?
				$i = 0;
				if($rowPageCount[0] > 0){
					while ($row=mysql_fetch_assoc($result)) {
						
						//(지점 > 진료과목) 체크 로직
						$firstTitle = "";
						if($branch && $row['hospital_name']){
						
							$firstTitle .= $row['hospital_name'];; 
						
						}

						if($clinic && $row['clinic_name']){
							if($row['firstTitle'] != "["){
								$firstTitle.="";
							}	
							$firstTitle.= $row['clinic_name'];
						} 
						
						
						
						$firstTitle+="]";
							
						if("[]" == $firstTitle){
							$firstTitle = "";
						}	

						$name = mb_substr($row['name'], 0, 1, "UTF-8")."**";
						$cell = str_replace( '-', '', $row[cell] );
						if( strpos( $row[cell], '-' ) > -1){
							$cell = $row[cell];
							$cell = substr($cell, 0, 3)."-****-****";
						}else{
							if( preg_match( '/(010)|(011)|(016)|(017)|(018)|(019)|(070)/', substr($cell, 0, 3) ) ){
								if( strlen($cell) == 11){
									$cell = substr($cell, 0, 3)."-****-****";
								}
								else if( strlen($cell) == 10) $cell = substr($cell, 0, 3)."-***-****";
								else $cell= substr($cell, 0, 3)."-***-****";
							}else{
								$cell = "";
								for( $j =0; $j < strlen( $row[cell] ); $j++){
									if($j > 2){
										$cell .= '*';
									}else{
										$cell .= substr($row[cell], $j, 1);
									}
								}
							}
						}
				?>
					<tr>
						<td class="none1000"><?=($rowPageCount[0]-($objCommon->reqPageNo-1)*$pageRows-$i)?></td>
						<td class="tit" style="text-align:left;" >
						<?=$name?><?=getMsg('message.tbody.consult') ?></td>
						<td>
						<?=$cell?>
						</td>
						<td>
						<?
						//날짜만 보여지는지 시간까지 보여지는지에 체크
						if($timeDate){
						?>
						<?= getDateTimeFormat($row['registdate']) ?>
						<?
						}else{
						?>
						<?= getYMD($row['registdate']) ?>
						<?
						}
						?> 
						</td>
					</tr>
					
				<?	
					$i++;
					}
				}else{ 
				?>
				<tr>
					<td colspan="4"><?=getMsg('message.tbody.data') ?></td>
				</tr>
				<?
				}
				
				?>
					
				</tbody>
			</table>
			
		</div>
	</div>
	<input type="hidden" name="cmd" id="cmd" value="write"/>
	<input type="hidden" name="curMonth" id="curMonth" value=""/>
	<? if (!$branch) { ?>
	<input type="hidden" name="hospital_fk" id="hospital_fk" value="<?=DEFAULT_BRANCH_NO?>"/>
	<?}?>
	</form>
	<!-- 프로그램끝 -->
	<? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
	<? include "sub_footer/sub_footer.php" ?>
</div>
</body>
</html>
