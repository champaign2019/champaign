<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Common.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/email/SendMail.class.php";
include "config.php";

$objCommon = new Common($pageRows, $tablename, $_REQUEST);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
<?
if (checkReferer($_SERVER["HTTP_REFERER"])) {

	$_REQUEST['uploadPath'] = $_SERVER['DOCUMENT_ROOT'].$uploadPath;
	
	if ($_REQUEST['cmd'] == 'write') {
		
		if($useWriteAnswer){$_REQUEST['whereConsult'] = 1;}// 온라인상담 답변형
	
		//다중업로드파일
		if(!$useFileDrag){
			$_REQUEST = multifileupload('filenames', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, true, $maxSaveSize);
		}
		//다중업로드파일 END

		$_REQUEST = fileupload('filename', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, true, $maxSaveSize);		// 첨부파일
		$_REQUEST = fileupload('moviename', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 동영상
		$_REQUEST = fileupload('imagename1', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지1
		$_REQUEST = fileupload('imagename2', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지2
		$_REQUEST = fileupload('imagename3', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지3
		$_REQUEST = fileupload('imagename4', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지4
		$_REQUEST = fileupload('imagename5', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지5
		$_REQUEST = fileupload('imagename6', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지6
		$_REQUEST = fileupload('imagename7', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지7
		$_REQUEST = fileupload('imagename8', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지8

		$r = $objCommon->insert($_REQUEST);
		if($useFileDrag){
			$id = session_id();
			$_REQUEST['session']   = $id;
			$_REQUEST['common_fk'] = $r;
			$objCommon->updateUploadFiles($_REQUEST);
		}
		if ($r > 0) {
			// 방문자 통계 서비스가 유료일때 통계데이터 update
			if (IS_LOG && LOG_TYPE == 1) {
				$log = new Log($tablename);
				$log->setLog($_COOKIE["CONNECTID_".WEBLOG_NUMBER], $r);
			}

			$data = $objCommon->getData($_REQUEST, false);
//			$hospital = new Hospital(999, $_REQUEST);
//			$hdata = $hospital->getData($_REQUEST['hospital_fk']);

			// 메일발송 체크, 답변이 된경우 답변메일 발송
			if ($_REQUEST[email_chk] == '1' and $answercheck == true) {
				$title = "[".COMPANY_NAME."]".$_REQUEST['name']."님의 상담답변이 등록되었습니다. 홈페이지에 방문하여 확인하세요";
				$contents = "
					<table border='0' cellpadding='0' cellspacing='0' width='100%' style='font-family:굴림; font-size: 12px; color: #4b4b4b; border:#CCCCCC 1px solid;'> ";
				if ($branch) {
						$content .= "
							<tr height='30'>									
								<td width=90 style='font-weight:bold; padding-left:5px; border-right:#CCCCCC 1px solid; border-bottom:#CCCCCC 1px solid; background-color: #f4f8fd;'>".getMsg('th.hospital_fk')."</td>
								<td style='padding-left:5px; border-bottom:#CCCCCC 1px solid;'>".$data['hospital_name']."</td>
							</tr>	";
					}
					if ($clinic) {
						$content .= "
							<tr height='30'>									
								<td width=90 style='font-weight:bold; padding-left:5px; border-right:#CCCCCC 1px solid; border-bottom:#CCCCCC 1px solid; background-color: #f4f8fd;'>".getMsg('th.clinic_fk')."</td>
								<td style='padding-left:5px; border-bottom:#CCCCCC 1px solid;'>".$data['clinic_name']."</td>
							</tr>	";
					}
				$contents .= "
					<tr height='30'>									
						<td width=90 style='font-weight:bold; padding-left:5px; border-right:#CCCCCC 1px solid; border-bottom:#CCCCCC 1px solid; background-color: #f4f8fd;'>성명</td>
						<td style='padding-left:5px; border-bottom:#CCCCCC 1px solid;'>".$_REQUEST['name']."</td>
					</tr>												
					<tr height='30'>									
						<td style='font-weight:bold; padding-left:5px; border-right:#CCCCCC 1px solid; border-bottom:#CCCCCC 1px solid; background-color: #f4f8fd;'>".getMsg('th.cell')."</td>	
						<td style='padding-left:5px; border-bottom:#CCCCCC 1px solid;'>".$_REQUEST['cell']."</td>	
					</tr>												
					<tr height='30'>									
						<td style='font-weight:bold; padding-left:5px; border-right:#CCCCCC 1px solid; border-bottom:#CCCCCC 1px solid; background-color: #f4f8fd;'>".getMsg('th.email')."</td>	
						<td style='padding-left:5px; border-bottom:#CCCCCC 1px solid;'>".$_REQUEST['email']."</td>		
					</tr>												
					<tr height='30'>									
						<td style='font-weight:bold; padding-left:5px; border-right:#CCCCCC 1px solid; background-color: #f4f8fd;'>상담제목</td>	
						<td style='padding-left:5px;'>".$_REQUEST['title']."</td>
					</tr>												
					</table>";
				
				$mailForm = getURLMakeMailForm(COMPANY_URL, EMAIL_FORM);
				$mailForm = str_replace(":SUBJECT", $title, $mailForm);
				$mailForm = str_replace(":CONTENT", $contents, $mailForm);
				$sendmail = new SendMail();
				$sendmail->send(COMPANY_EMAIL, COMPANY_NAME, $_REQUEST['email'], $title, $mailForm);
			}

			// SMS발송
			if ($_REQUEST['sms_send'] == 1) {
				$sms = new Sms(999, $_REQUEST);
				$_REQUEST['tran_id'] = SMS_KEYNO;
				$_REQUEST['tran_etc1'] = SMS_CNAME;
				$_REQUEST['tran_etc2'] = SMS_USERID;
				$_REQUEST['tran_etc3'] = SMS_USERNAME;
				$_REQUEST['sendtype'] = 'cell';
				$_REQUEST['tran_status'] = '1';
				$_REQUEST['tran_date'] = getFullToday();

				if ($_REQUEST[cell] && $hdata['reser_write_user'] == "1") {
					// 받을 사람(사용자)
					// 핸드폰번호가 있고 상담글 등록시 사용자에게 SMS발송
					$msg = str_replace("#name#", $_REQUEST[name], $hdata['reser_sms_msg']);
					$_REQUEST['tran_callback'] = $hdata['reser_admin_tel'];
					$_REQUEST['receiver'] = $data['cell'];
					$_REQUEST['tran_msg'] = $msg;
					$sms->sendSMS($_REQUEST);
				}
			}

			if( COMPANY_EMAIL == "" ){
				//echo "::".$_REQUEST[answer]."::";
				//echo "++".COMPANY_URL."++";
				//echo "++".EMAIL_FORM."++";
				//echo "++".$mailForm."++";
			}

			echo returnURLMsg($objCommon->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), '정상적으로 저장되었습니다.');
		} else {
			echo returnURLMsg($objCommon->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), '요청처리중 장애가 발생하였습니다.');
		}

	} else if ($_REQUEST['cmd'] == 'edit') {
		//다중업로드파일
		$_FILES['filenames'] = $_FILES['filenames'];
		$_REQUEST['unos'] = array();
		foreach($_FILES as $key => $val){ 
			if(strpos($key, '|filename') && $val['name']){
				$filename = explode("|", $key);
				if($_FILES['filenames']){
					if($_REQUEST['filename_chk'])
					array_push($_REQUEST['unos'], $_REQUEST['filename_chk']);

					array_push($_REQUEST['unos'], $filename[0]);
					array_push($_FILES['filenames']['name'], $_FILES[$key]['name']);
					array_push($_FILES['filenames']['type'], $_FILES[$key]['type']);
					array_push($_FILES['filenames']['tmp_name'], $_FILES[$key]['tmp_name']);
					array_push($_FILES['filenames']['size'], $_FILES[$key]['size']);
				}else{
					if($_REQUEST['filename_chk'])
					array_push($_REQUEST['unos'], $_REQUEST['filename_chk']);

					array_push($_REQUEST['unos'], $filename[0]);
					$_FILES['filenames']['name'][0] = $_FILES[$key]['name'];
					$_FILES['filenames']['type'][0] = $_FILES[$key]['type'];
					$_FILES['filenames']['tmp_name'][0] = $_FILES[$key]['tmp_name'];
					$_FILES['filenames']['size'][0] = $_FILES[$key]['size'];
				}
			
			}
		}

		if($_FILES['filenames'])
		$_REQUEST = multifileupload('filenames', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, true, $maxSaveSize);
		//다중업로드파일 END
		$_REQUEST = fileupload('filename', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, true, $maxSaveSize);		// 첨부파일
		$_REQUEST = fileupload('moviename', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 동영상
		$_REQUEST = fileupload('imagename1', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지1
		$_REQUEST = fileupload('imagename2', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지2
		$_REQUEST = fileupload('imagename3', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지3
		$_REQUEST = fileupload('imagename4', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지4
		$_REQUEST = fileupload('imagename5', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지5
		$_REQUEST = fileupload('imagename6', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지6
		$_REQUEST = fileupload('imagename7', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지7
		$_REQUEST = fileupload('imagename8', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지8
		$r = $objCommon->update($_REQUEST);

		if ($r > 0) {
			
			echo returnURLMsg($objCommon->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), '정상적으로 수정되었습니다.');
		} else {
			echo returnURLMsg($objCommon->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), '요청처리중 장애가 발생하였습니다.');
		}
	}else if ($_REQUEST['cmd'] == 'reply') {
		
		if($useWriteAnswer){$_REQUEST['whereConsult'] = 1;}// 온라인상담 답변형
		
		$_REQUEST = fileupload('filename', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, true, $maxSaveSize);		// 첨부파일
		$_REQUEST = fileupload('moviename', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 동영상
		$_REQUEST = fileupload('imagename1', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지1
		$_REQUEST = fileupload('imagename2', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지2
		$_REQUEST = fileupload('imagename3', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지3
		$_REQUEST = fileupload('imagename4', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지4
		$_REQUEST = fileupload('imagename5', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지5
		$_REQUEST = fileupload('imagename6', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지6
		$_REQUEST = fileupload('imagename7', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지7
		$_REQUEST = fileupload('imagename8', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지8

		$r = $objCommon->insertReply($_REQUEST);
		if ($r > 0) {
			echo returnURLMsg($objCommon->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), '정상적으로 저장되었습니다.');
		} else {
			echo returnURLMsg($objCommon->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), '요청처리중 장애가 발생하였습니다.');
		}

	} else if ($_REQUEST['cmd'] == 'groupDelete') {

		$no = $_REQUEST['no'];
		 
		$r = 0;
		for ($i=0; $i<count($no); $i++) {
			$_REQUEST['no'] = $no[$i];

			$r += $objCommon->delete($_REQUEST);
		}

		if ($r > 0) {
			echo returnURLMsg($objCommon->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), '총 '.$r.'건이 삭제되었습니다.');
		} else {
			echo returnURLMsg($objCommon->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), '요청처리중 장애가 발생하였습니다.');
		}

	} else if ($_REQUEST['cmd'] == 'delete') {
		
		$data = $objCommon->getData($_REQUEST, false);
		
		$view = false;
		//비공개 경우 : 로그인상태 : 본인글 확인, 미로그인상태 : password확인 후 올바르지 않으면 비밀번호 입력으로
		if ($loginCheck) {													// 로그인 상태
			if ($data['member_fk'] == $_SESSION['member_no']) {					// 본인글 확인
				$view = true;
			}
			else {
				if (!$_REQUEST[cur_password]) {						// 전달 받은 비밀번호가 없을 때
					echo returnUrl($objCommon->getQueryStringCmd('password.php', $data[no], $_REQUEST));
				}
				else {
					if ($objCommon->checkPassword($_REQUEST) == 0) {								// 비밀번호가 올바르지 않을 때
						echo returnURLMsg($objCommon->getQueryStringCmd('password.php', $data[no], $_REQUEST)."&cmd=".$_REQUEST[cmd],"비밀번호가 올바르지 않습니다.");
					}
					else{
						$view = true;
					}
				}
			}
		}
		else {													// 미로그인 상태
			if (!$_REQUEST[cur_password]) {						// 전달 받은 비밀번호가 없을 때
				echo returnUrl($objCommon->getQueryStringCmd('password.php', $data[no], $_REQUEST));
			}
			else {
				if ($objCommon->checkPassword($_REQUEST) == 0) {								// 비밀번호가 올바르지 않을 때
					echo returnURLMsg($objCommon->getQueryStringCmd('password.php', $data[no], $_REQUEST)."&cmd=".$_REQUEST[cmd],"비밀번호가 올바르지 않습니다.");
				}
				else{
					$view = true;
				}
			}
		}

		if ( $view ) {
			/*delete START*/
			$r = $objCommon->delete($_REQUEST);
			if ($r > 0) {
				echo returnURLMsg($objCommon->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), '정상적으로 삭제되었습니다.');
			}
			else {
				echo returnURLMsg($objCommon->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), '요청처리중 장애가 발생하였습니다.');
			}
			/*delete END*/
		}
		else {
			echo returnURLMsg($objCommon->getQueryStringCmd('password.php', $data[no], $_REQUEST)."&cmd=".$_REQUEST[cmd],"비밀번호가 올바르지 않습니다.");
		}

	}


} else {
	echo returnURLMsg($objCommon->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), '요청처리중 장애가 발생하였습니다.');
}
?>
</body>
</html>