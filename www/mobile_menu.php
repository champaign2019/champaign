<div class="pro_m_wrap">
	<div class="m_menu">
		<ul class="subleftmenu">
			<li><a href="/manage/" target="_blank" class="submenu" > 관리자모드 </a></li>
			<li><a href="/consult/consult/index.php" target="mainFrame" class="submenu" > 온라인상담 </a></li>
			<li><a href="/consult/newconsult/index.php" target="mainFrame" class="submenu" > 온라인상담 - 답변형</a></li>
			<li><a href="/consult/formMail/write.php" target="mainFrame" class="submenu" > 메일상담 </a></li>
			<li><a href="/consult/tel/index.php" target="mainFrame" class="submenu" > 전화상담 </a></li>
			<li><a href="/consult/moneymail/index.php" target="mainFrame" class="submenu" > 비용메일 </a></li>
			<li><a href="/board/reply/index.php" target="mainFrame" class="submenu" > 치료후기 - 답변형 </a></li>
			<li><a href="/board/reply_comment/index.php" target="mainFrame" class="submenu" > 치료후기 - 답변댓글형 </a></li>
			<li><a href="/reservation/write.php" target="mainFrame" class="submenu" > 온라인예약 </a></li>
			<li><a href="/reservation/confirm.php" target="mainFrame" class="submenu" > 예약확인 </a></li>
			<li><a href="/board/notice/index.php" target="mainFrame" class="submenu" > 공지/보도/동영상 </a></li>
			<li><a href="/board/NoticeListImage/index.php" target="mainFrame" class="submenu" > 목록이미지 공지/보도/동영상 </a></li>
			<li><a href="/board/CureInstance/index.php" target="mainFrame" class="submenu" > 치료사례(이미지목록형) </a></li>
			<li><a href="/board/beafphoto/index.php" target="mainFrame" class="submenu" > 치료사례(갤러리형) </a></li>
			<li><a href="/board/newbna/index.php" target="mainFrame" class="submenu" > 전후사진 갤러리 </a></li>
			<li><a href="/board/gallery/index.php" target="mainFrame" class="submenu" > 갤러리게시판 </a></li>
			<li><a href="/board/cardtype/index.php" target="mainFrame" class="submenu" > 카드형게시판 </a></li>
			<li><a href="/board/faq/index.php" target="mainFrame" class="submenu" > FAQ </a></li>
			<li><a href="/board/faq2/index.php" target="mainFrame" class="submenu" > FAQ - 분류별 </a></li>
			<li><a href="/board/common/index.php" target="mainFrame" class="submenu" > 공통게시판 </a></li>
			<li><a href="/member/write.php" target="mainFrame" class="submenu" > 회원가입 </a></li>
			<li><a href="/member/agree.php" target="mainFrame" class="submenu" > 회원약관 </a></li>
			<li><a href="/member/login.php" target="mainFrame" class="submenu" > 로그인 </a></li>
			<li><a href="/member/idpwFind.php" target="mainFrame" class="submenu" > ID/PW찾기 </a></li>
			<li><a href="/member/edit.php" target="mainFrame" class="submenu" > 회원정보변경 </a></li>
			<li><a href="/member/policy.php" target="mainFrame" class="submenu" > 개인정보취급방침 </a></li>
			<li><a href="/member/secede.php" target="mainFrame" class="submenu" > 회원탈퇴 </a></li>
			<li><a href="/include/logout.php" target="mainFrame" class="submenu" > 로그아웃 </a></li>
			<li><a href="/include/popup/mainPopup.php" target="mainFrame" class="submenu" > 팝업보기 </a></li>
		</ul>
	</div>
	<div class="bg_black"></div>
</div>
