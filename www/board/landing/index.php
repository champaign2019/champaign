<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/filter/RequestWrapper.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Common.class.php";
include "config.php";
$_REQUEST['orderby'] = $orderby; //정렬 배열 선언
$objCommon = new Common($pageRows, $tablename, $_REQUEST);
$rowPageCount = $objCommon->getCount($_REQUEST);
$result = $objCommon->getList($_REQUEST);
if ($rowPageCount[0] == 0) {
	echo returnURLMsg('/', '등록된 랜딩페이지가 없습니다.');
}
else {
	if (empty($_REQUEST['no']) || $_REQUEST['no'] == 0) {
		while($row = mysql_fetch_assoc($result)) {
			$_REQUEST['no'] = $row['no'];
			break;
		}
	}
	$data = $objCommon->getData($_REQUEST, false);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<? include $_SERVER['DOCUMENT_ROOT']."/include/headHtml.php" ?>
		<? include $_SERVER['DOCUMENT_ROOT']."/include/boardConfig/config.php" ?>
    	<script>
    		$(document).ready(function() {
    			var frm = $('#ladingFrm');
    			$(frm).find('.go-save').click(function() {
    				$(frm).submit();
    			});
    			$(frm).submit(function() {
    				if (!validation(this))
    					return false;
    				
    				if (!jQ.phoneNumberPatternValidation('.phone-number-validation'))
    					return false;
    				
    				fn_formSpanCheck(this, key);	
    				
    				return true;
    			});
    			$(frm).find('.is-number-or-hyphen').keyup(function() {isNumberOrHyphen(this);});
    			$(frm).find('.cvt-user-phone-number').blur(function() {cvtUserPhoneNumber(this);});
    		});
    	</script>
	</head>
	<body>
		<div class="content">
			<?
				if ($data['state'] == 1) {
					echo $data['contents'];
				}
				else if ($data['state'] == 2) {
					if ($data['filename']) {
						$filename = split(",", $data['filename']);
						$rel = split(",", $data['relation_url']);
						for ($i=0; $i<count($filename); $i++) {
							if ($rel[$i]) echo '<a href="'.$rel[$i].'" target="_blank" >';
							echo '<img src="'.$uploadPath.$filename[$i].'" />';
							if ($rel[$i]) echo '</a>';
						}
					}
				}
			?>
		</div>
		<div class="input_footer">
		    <div class="w1400">
				<form name="ladingFrm" id="ladingFrm" action="process.php" method="post">
			        <div class="name_box">
			            <div class="name">
			                <p class="tit">이름</p>
			                <input data-value="이름을 입력하세요." name="name" class="input_text" type="text" />
			            </div>
			            <div class="name">
			                <p class="tit">연락처</p>
			                <input type="text" name="cell" class="input_text phone-number-validation is-number-or-hyphen cvt-user-phone-number" data-value="연락처를 입력하세요." />
			            </div>
			        </div>
			        <div class="cont_box">
			            <div class="cont">
			                <p class="tit">상담내용</p>
			                <textarea class="input_text" name="contents" data-value="상담내용을 입력해 주세요." style="resize:none;"></textarea>
			            </div>
			        </div>
			        <div class="click_box">
			            <a style="cursor:pointer;" class="click go-save">상담신청하기</a>
			            <label>
			            <input type="checkbox" class="input_check" name="agree" data-value="개인정보 수집에 동의해주세요." value="1" />
			            <p>개인정보 수집에 동의합니다.</p>
						</label>
			        </div>
			        <input type="hidden" name="category_fk" value="<?=$_REQUEST['no'] ?>" />
			        <input type="hidden" name="cmd" value="write" />
				</form>
		    </div>
		</div>
	</body>
</html>
<?	
	}
?>