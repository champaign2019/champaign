<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Common.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Hospital.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Clinic.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/environment/Stipulation.class.php";


include "config.php";

if($useWriteAnswer){

if ($boardgrade == 3 || $boardgrade == 2) { // 게시판접근 권한처리
	include $_SERVER['DOCUMENT_ROOT']."/include/gradeCheck.php";
}

$objCommon = new Common($pageRows, $tablename, $_REQUEST);
$data = $objCommon->getData($_REQUEST, false);
$category_result = $category_tablename ? $objCommon->getCategoryList($_REQUEST) : null;
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/include/headHtml.php" ?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/boardConfig/config.php" ?>

<script>
$(window).load(function(){
	fn_addDel_object(<?=$useFileCount?>); //파일 function 생성
});
function goSave(obj) {
	
	
	if(validation(obj)){
		<?if(!$isCellSum){?>
			<?if($useTel){?>
			if($("[name=tel1]").val() != "" && $("input[name=tel2]").val() != "" && $("input[name=tel3]").val() != ""){
			$("input[name=tel]").val($("[name=tel1]").val()+"-"+$("input[name=tel2]").val()+"-"+$("input[name=tel3]").val())
			}
			<?}?>
			
			<?if($useCell){?>
			if($("[name=cell1]").val() != "" && $("input[name=cell2]").val() != "" && $("input[name=cell3]").val() != ""){
			$("input[name=cell]").val($("[name=cell1]").val()+"-"+$("input[name=cell2]").val()+"-"+$("input[name=cell3]").val())
			}
			<?}?>
			
		<?}?>
		
		<?if($useEmail){?>
		if($("input[name=email1]").val() != "" && $("[name=email2]").val() != ""){
		$("input[name=email]").val($("input[name=email1]").val().trim()+"@"+$("[name=email2]").val().trim());
		}
		
		<?}?>
		
		if(!fn_spamCheck()){return false;}
		
		var form = $("#frm");

		fn_formSpanCheck(form,key);	
		return true;	
	}else{
		return false;
	}
}
</script>
</head>

<body>

	<div id="wrap">
    <? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
    <? include "sub_visual/sub_visual.php" ?>
    <!-- 프로그램시작 -->
	<form name="frm" id="frm" action="<?=getSslCheckUrl($_SERVER['REQUEST_URI'], 'process.php')?>" method="post" enctype="multipart/form-data" onsubmit="return goSave(this);">
	<div class="programCon">
		<?
			if ($loginCheck) { 
			
			$st = new Stipulation();
			$sv = $st->getData();
			
		?>
		<div class="join">
			<div class="joinTop">
				<div class="ScrollLy">
					<?=str_replace("#company_name#", COMPANY_NAME, $sv['privacy_mini_text'])?>	
					<!-- //policy -->
				</div>
				<div class="jcheck">
					<input type="checkbox" data-value="<?=getMsg('alt.checkbox.agree') ?>" id="agree02" name="agree02" value="" title="<?=getMsg('message.tbody.agree_ch') ?>" checked="checekd"/>
					<label for="agree02"> <?=getMsg('message.tbody.agree') ?></label>
				</div>
				<!-- //jcheck -->
			</div>
		</div>
		<?
		}
			?>
		<!-- //join -->
		<? 
		if($branch || $clinic) { 
		?>
		<div class="writeForm writeForm_top">
			<table>
				<colgroup>
					<?
					if($branch){
					?>
					<col class="writeForm_col01" />
					<col width="*" />
					<?
					}
					?>
					<?
					if($clinic){
					?>
					<col class="writeForm_col01" />
					<col width="*" />
					<?
					}
					?>
				</colgroup>			
				<tr>
					<?
					if($branch){
						$hospital = new Hospital(999, $_REQUEST);
						$hResult = $hospital->branchSelect();
					?>
					<th><?=getMsg('th.branch')?></th>
					<td>
						
						<select name="hospital_fk" id="hospital_fk" onchange="" data-value="<?=getMsg('alert.text.shospital_fk') ?>">
							<option value=""><?=getMsg('lable.option.branch') ?></option>
							<? while ($row=mysql_fetch_assoc($hResult)) { ?>				
							<option value="<?=$row['no']?>" <?=getSelected($row['no'],$data['hospital_fk'])?>><?=$row['name']?></option>
							<? } ?>
						</select>	
					</td>
					<?
					}
					?>
					<?
					if($clinic){
						$clinicObj = new Clinic(999, $_REQUEST);
						$clinicList = $clinicObj->selectBoxList(0,$data['hospital_fk'],$data['sclinic_fk']);
						
					?>
					<th><?=getMsg('th.clinic_fk') ?></th>
					<td>
						<select name="clinic_fk" id="clinic_fk" data-value="<?=getMsg('alt.select.sclinic_fk') ?>" >
							<?= $clinicList ?>
						</select>
					</td>
					<?
					}
					?>
				</tr>
			</table>
		</div>
		<?
		}
		?>
		<?
		if($useCalendar){
		?>
		<div class="schedule_wrap">
			<div class="schedule"></div>
			<div class="schedule_time"></div>
			<input type="hidden" name="curMonth" id="curMonth" value=""/>
		</div>
		<?
		}
		?>
		
		<div class="writeForm">
			<table>
				<colgroup>
					<col class="writeForm_col01" />
					<col width="*" />
				</colgroup>
				<tr>
					<th><?=getMsg('th.name')?></th>
					<td>
						<input data-value="<?=getMsg('alert.text.name')?>." id="name" name="name" class="inputTxt inputName" type="text" value="<?=$_SESSION['member_name']?>" maxlength="25" />
					</td>
				</tr>
				<?
				if($useEmail){
				?>
				<tr>
					<th><?=getMsg('th.email') ?></th>
					<td class="mail_type">
						<input data-value="<?=getMsg('alert.text.email')?>." name="email1" id="email1" class="inputEmail" type="text" value="<?=getSplitIdx($member_email, "@", 0)?>" maxlength="70" /><span class="email_txt">@</span>
						<select class="selecEmail" name="email2" id="email2" >
							<option value=""><?=getMsg('lable.option.select')?></option>
							<?=getOptsEmail(getSplitIdx($member_email, "@", 1))?>
						</select>
						<?
						if($useEmailChk){
						?>
						<span class="label_wrap"><input type="checkbox" id="Email_check" name="ismail" value="1" /><label for="Email_check"><?=getMsg('lable.checkbox.email')?></label></span>
						<?
						}
						?>
					</td>
				</tr>
				<?
				}
				?>
				<?
				if($usePassword){
				?>
				<tr>
					<th><?=getMsg('th.password')?></th>
					<td>
						<input data-value="<?=getMsg('alert.text.password')?>." name="password" id="password" class="inputPass" type="password"  maxlength="100" />
					</td>
				</tr>
				<?
				}
				?>
				<?
				if($useTel){
				?>
				<tr>
					<th><?=getMsg('th.tel') ?></th>
					<td>
						<?
						if(!$isCellSum){
						?>
						<select class="selectNum" name="tel1" id="tel1">
							<?=getOptsTel(getSplitIdx($member_tel, "-", 0))?>
						</select>
						<input data-value="<?=getMsg('alert.text.tel')?>" name="tel2" id="tel2" class="inputNum" type="text" value="<?=getSplitIdx($member_tel, "-", 1)?>" maxlength="4" onkeyup="isNumberOrHyphen2(this);" />
						<input data-value="<?=getMsg('alert.text.tel')?>" name="tel3" id="tel3" class="inputNum" type="text" value="<?=getSplitIdx($member_tel, "-", 2)?>" maxlength="4" onkeyup="isNumberOrHyphen2(this);" />
						<input type="hidden" name="tel" id="tel" value=""/>
						<?}else{?>
						<input type="text" name="tel" id="tel" value="<?=$member_tel?>" onkeyup="isOnlyNumberNotHypen(this);"  data-value="<?=getMsg('alert.text.tel')?>"  />
						
						<?}?>
					</td>
				</tr>
				<?
				}
				?>
				
				<?
				if($useCell){
				?>
				<tr>
					<th><?=getMsg('th.cell') ?></th>
					<td>
						<?
						if(!$isCellSum){
						?>
						<select class="selectNum" name="cell1" id="cell1">
							<?=getOptsCell(getSplitIdx($member_cell, "", 0))?>
						</select>
						<input data-value="<?=getMsg('alert.text.cell') ?>" name="cell2" id="cell2" class="inputNum" type="text" value="<?=getSplitIdx($member_cell, "-", 1)?>" maxlength="4" onkeyup="isNumberOrHyphen2(this);" />
						<input data-value="<?=getMsg('alert.text.cell') ?>" name="cell3" id="cell3" class="inputNum" type="text" value="<?=getSplitIdx($member_cell, "-", 2)?>" maxlength="4" onkeyup="isNumberOrHyphen2(this);" />
						<input type="hidden" name="cell" id="cell" value=""/>
						<?}else{ ?>
						<input type="text" name="cell" id="cell" value="<?=$member_cell?>" onkeyup="isOnlyNumberNotHypen(this);"  data-value="<?=getMsg('alert.text.cell') ?>" />
						<?}?>
						
						<?
						if($useSmsChk){
						?>
						<span class="label_wrap"><input type="checkbox" id="Num_check" name="iscall" value="1" /><label for="Num_check"><?=getMsg('th.telconsult') ?></label></span>
						<?
						}
						?>
					</td>
				</tr>
				<?
				}
				?>
				<tr>
					<th><?=getMsg('th.title') ?></th>
					<td class="addr_td">
						<input data-value="<?=getMsg('alert.text.title')?>" type="text" id="title" class="inputTit" name="title" value="" />
						<?
						if($useSecret){
						//무조건 비공개일경우 type="hidden"
						?>
						<span class="label_wrap"><label for="secret"><input type="checkbox" name="secret" id="secret" value="1" /> <?=getMsg('lable.checkbox.secret')?></label></span>
						<?
						}
						?>
					</td>
				</tr>
				<tr>
					<th><?=getMsg('th.contents') ?></th>
					<td><textarea name="contents" id="contents" data-value="<?=getMsg('alert.text.contents')?>" cols="30" rows="10"></textarea></td>
				</tr>
				<?
				if($gallery){
				?>
				<tr>
					<th><?=getMsg('th.imagename')?></th>
					<td><input type="file" name="imagename1" id="imagename1" /></td>
				</tr>
				<tr>
					<th><?=getMsg('th.image_alt')?></th>
					<td><input type="text" name="image1_alt" id="image1_alt" value="이미지입니다." /></td>
				</tr>
				
				<?
				}
				?>
				
				<?
				if($useRelationurl){
				?>
				<tr>
					<th><?=getMsg('th.relation_url')?></th>
					<td><input type="text" id="relation_url" name="relation_url" title="" /></td>
				</tr>
				<?
				}
				?>
				
				<?
				if($useFile){
				?>
				<tr>
					<th><?=getMsg('th.filename')?></th>
					<td id="useFile">
						<p>
							<input type="file" name="filenames[]" id="" class="input50p" style="display:inline-block;" />
							<a class="btns gr_btn" href="javascript:;" id="addFile"><?=getMsg('btn.add')?></a>
							<a class="btns gr_btn" href="javascript:;" id="delFile"><?=getMsg('btn.remove')?></a>
						</p>
					</td>
				</tr>
				<?
				}
				?>
				<? if ($useMovie) { ?>
				<tr>
					<th scope="row"><label for=""><?=getMsg('th.moviename')?></label></th>
					<td>
						<p><label for=""><?=getMsg('message.tbody.movie')?></label></p>
						<input type="file" id="moviename" name="moviename" class="input92p" title="첨부파일을 업로드 해주세요." />	
					</td>
				</tr>
				<? } ?>
			</table>
		</div>
		<div class="writeForm_btn">
			<a href="javascript:;" id="w_btn" onclick="$('#frm').submit()"><?=getMsg('btn.submit') ?></a>
			<a href="<?=$objCommon->getQueryString('index.php', 0, $_REQUEST)?>" ><?=getMsg('btn.cancel') ?></a>
		</div>
	
		<input type="hidden" name="email" id="email" value=""/>
		<input type="hidden" name="cmd" id="cmd" value="reply"/>
		<input type="hidden" name="top" id="top" value="<?=$data['top']?>"/>
		<input type="hidden" name="gno" id="gno" value="<?=$data['gno']?>"/>
		<input type="hidden" name="ono" id="ono" value="<?=$data['ono']?>"/>
		<input type="hidden" name="nested" id="nested" value="<?=$data['nested']?>"/>
		<? if (!$branch) { ?>
		<input type="hidden" name="hospital_fk" id="hospital_fk" value="<?=$data['hospital_fk']?>"/>
		<?}?>
	</div>
	</form>
	 <!-- 프로그램끝 -->
    <? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
    <? include "sub_footer/sub_footer.php" ?>
</div>
</body>
</html>
<?
}
?>