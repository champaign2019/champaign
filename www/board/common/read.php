<?session_start();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<?
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Consult.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Common.class.php";

include "config.php";

if ($boardgrade == 3 || $boardgrade == 2) { // 게시판접근 권한처리
	include $_SERVER['DOCUMENT_ROOT']."/include/gradeCheck.php";
}

	$objCommon = new Common($pageRows, $tablename, $_REQUEST);
	$_REQUEST['orderby'] = $orderby; //정렬 배열 선언
	$data = $objCommon->getData($_REQUEST, false);

	$noPassword = $usePassword;
	$view = false;
	if($useSecret && $useBtn){
 		
		//비공개 경우 : 로그인상태 : 본인글 확인, 미로그인 상태 : password확인 후 올바르지 않으면 비밀번호 입력으로
	if ($data['secret'] == "1") {
	if ($loginCheck) {													// 로그인 상태
		if ($data['member_fk'] == $_SESSION['member_no']) {				// 본인글 확인
	$noPassword = false;
	$view = true;
		}
	}
		
	if ($noPassword) {													// 미로그인 상태
		if (!$_REQUEST['cur_password']) {						// 전달 받은 비밀번호가 없을 때
		echo returnURL($objCommon->getQueryStringCmd(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'password.php'), $data['no'], $_REQUEST));
		} else {
	if ($objCommon->checkPassword($_REQUEST) == 0) {								// 비밀번호가 올바르지 않을 때
		echo returnURLMsg($objCommon->getQueryStringCmd('password.php', $data[no], $_REQUEST)."&cmd=".$_REQUEST[cmd],"비밀번호가 올바르지 않습니다.");
	}else{
		$view = true;
		$_REQUEST['cmd'] = "edit";	
	}
		}
	}
	
	
		}else{
	$view = true;
	$_REQUEST['cmd'] = "edit";	
		}
	}else{
		
		if($useBtn && ($usePassword || $loginCheck)){
			$_REQUEST['cmd'] = "edit";	
		}
		$view = true;
	}

if($view){
	$data = $objCommon->getData($_REQUEST, $userCon);
?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/include/headHtml.php" ?>
<script type="text/javascript">
	<?if($useBtn && ($usePassword || $loginCheck)){?>
	function goDelete() {
		var del = confirm ("<?=getMsg('confirm.text.delete')?>");
		if (del){ document.location.href = "<?=$objCommon->getQueryStringCmd('password.php', $data[no], $_REQUEST)?>&cmd=delete";}
		else { return false;	}
	}
	
	function goEdit() {
		<?if($useBtn && ($usePassword || $loginCheck)){?>
		document.location.href = "<?=$objCommon->getQueryStringCmd('edit.php', $data['no'], $_REQUEST)?>";
		<?}else if(usePassword){?>
		document.location.href = "<?=$objCommon->getQueryStringCmd('password.php', $data['no'], $_REQUEST)?>";
		<?}?>
	}
	<?}?>
</script>
</head>
<body>

	
<div id="wrap">
    <? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
    <? include "sub_visual/sub_visual.php" ?>
    <!-- 프로그램시작 -->

<div class="programCon">
	<div class="programRead">
		<?
		//(지점 > 진료과목) 체크 로직
		$firstTitle = "";
		if($branch && $data['hospital_name']){
		
			$firstTitle .= "<span class='branch01'>" .$data['hospital_name']. "</span>"; 
		
		}
		if($clinic && $data['clinic_name']){
			if($data['firstTitle'] != "["){
				$firstTitle.="";
			}	
			$firstTitle.= "<span class='branch02'>" .$data['clinic_name']. "</span>";
		} 
		
		$firstTitle.="";
			
		if(!$firstTitle){
			$firstTitle = "";
		}		
		?>
		<div class="readTop">
			<p class="imgTitle"><?=$firstTitle?> <?=$data['title'] ?></p>
			<!--<p class="imgTitle_sub tit"> 작성자</p>-->
			<p class="imgTitle_sub">
				<?=$data['name']?>
			<i></i>
			
			<?
			//날짜만 보여지는지 시간까지 보여지는지에 체크
			if($timeDate){
			?>
			<?= getDateTimeFormat($data['registdate']) ?>
			<?
			}else{
			?>
			<?= getYMD($data['registdate']) ?>
			<?
			}
			?>
			<span class="hit"><img src="/manage/img/ucount_icon.png" /> </span><?=$data['readno']?>
			</p>
			
			
		</div>
		<div class="readEdit">
			<?
			if(is_file($_SERVER['DOCUMENT_ROOT'].$uploadPath.$row['imagename1']) && $isListImage){
			?>
			<p><img src="<?=$uploadPath?><?=$row['imagename1']?>" alt="<?=$row['image1_alt']?>" title="<?=$row['image1_alt']?>" /></p>
			<?
			}
			?>
			<? if ($useMovie){ ?>
			<?if ($data['moviename']) {?>
			<p>
				<script type="text/javascript">
				<!--
				tv_adplay_autosize("<?=$uploadPath?><?=$data['moviename']?>", "MoviePlayer");
				//-->
				</script>
			</p>
			
			<? } ?>
		<? } ?>
			
			<?=stripslashes($data['contents'])?>
		</div>
		<? if ( ($useFile || $useRelationurl) && ($data['filename_org'] || $data['relation_url'])) { ?>
		<div class="urlFile">
		<? if ($useFile) { ?>
			<?
				if ($data['filename_org']) {
					$filename = split(",", $data['filename']);
					$filename_org = split(",", $data['filename_org']);
					$filesize = split(",", $data['filesize']);
			?>
				<?for($i=0;$i<sizeof($filesize);$i++){?>
				<p><img src="/manage/img/file_img.gif" alt="파일첨부" />&nbsp;&nbsp;<a href="/lib/download.php?path=<?=$uploadPath?>&vf=<?=$filename[$i]?>&af=<?=$filename_org[$i]?>" target="_blank"><?=$filename_org[$i]?> [<?=getFileSize($filesize[$i])?>]</a></p>
				<?}?>
			<? } ?>
		<? } ?>
		<? if ($useRelationurl) { ?>
			<? if ($data['relation_url']) { ?>
				<p><img src="/img/url_img.gif" alt="관련URL" /> <a href="<?=$data['relation_url']?>" target="_blank"><?=cvtHttp($data['relation_url'])?></a></p>
			<? } ?>
		<? } ?>
		</div>
		<? } ?>	

		<?if($useAnswer && $data['answer']){ ?>
		<div class="readAnswer">
			<p class="answer_tit"><?=getMsg('th.answer') ?></p>
			<div class="answer_txt"><?=$data['answer']?></div>
		</div>
		<? } ?>
		
		<? if ($isComment) { ?>
			
			<? include $_SERVER['DOCUMENT_ROOT']."/board/comment/comment.php" ?><!-- 댓글 -->
	
		<? } ?>
			
		<div class="readBottom">
			<table>
				<colgroup>
					<col width="22%" />
					<col width="*" />
				</colgroup>
				<tr>
					<th><?=getMsg('th.prev')?><img src="/img/read_arrowUp.png" alt="" /></th>
					<td>
					<? if($data['prev_no'] > 0) { ?>
						<a class="" href="<?=$objCommon->getQueryString('read.php', $data['prev_no'], $_REQUEST)?>"><?=$data['prev_title']?></a>
					<? }else{ ?>
						<a href=""><?=getMsg('message.tbody.prev')?></a>
					<?}?>
					
					</td>
				</tr>
				<tr>
					<th><?=getMsg('th.next')?><img src="/img/read_arrowDown.png" alt="" /></th>
					<td>
					<? if ($data['next_no'] > 0) { ?>
						<a class="" href="<?=$objCommon->getQueryString('read.php', $data['next_no'], $_REQUEST)?>"><?=$data['next_title']?></a>
					<? }else{ ?>
						<a href=""><?=getMsg('message.tbody.next')?></a>
					<?}?>
					</td>
				</tr>
			</table>
			
			<dl class="readBottom_btn">
				<dt>
					<?
					if($useBtn && ($usePassword || $loginCheck)){
						if($usePassword || ($loginCheck && $data['member_fk'] == $_SESSION['member_no'])){
					?>
						<a class="btns" href="javascript:;" onclick="return goEdit();"><strong><?=getMsg('btn.edit')?></strong></a>
						<a class="btns" href="javascript:;" onclick="return goDelete();"><strong><?=getMsg('btn.delete')?></strong></a>
					<?
						}
					}
					?>
					<?
					if($useWriteAnswer){
					?>
					<a class="btns" href="<?=$objCommon->getQueryStringCmd('reply.php', $data['no'], $_REQUEST)?>"><strong><?=getMsg('th.answer') ?></strong></a>
					<?
					}
					?>
				</dt>
				<dd>
				<a class="btns" href="<?=$objCommon->getQueryString('index.php', 0, $_REQUEST)?>"><strong><?=getMsg('btn.list')?></strong></a>
				</dd>
			</dl>
		</div>
	</div>
</div>
 <!-- 프로그램끝 -->
    <? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
    <? include "sub_footer/sub_footer.php" ?>
</div>
</body>
</html>
<?
}
?>