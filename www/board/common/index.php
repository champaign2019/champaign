<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/filter/RequestWrapper.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Common.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Hospital.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Clinic.class.php";


include "config.php";
if ($boardgrade == 3)  { // 게시판 접근 권한 처리
?>

<?
}
$_REQUEST['orderby'] = $orderby; //정렬 배열 선언
$_REQUEST['whereConsult'] = $answer; //답변형 게시판일경우 1을 반환함

$objCommon = new Common($pageRows, $tablename, $_REQUEST);
$rowPageCount = $objCommon->getCount($_REQUEST);
$result = $objCommon->getList($_REQUEST);

$category_result = $category_tablename ? $objCommon->getCategoryList($_REQUEST) : null;

if($_REQUEST['view_loc_type'] == 0){
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<? include $_SERVER['DOCUMENT_ROOT']."/include/headHtml.php" ?>
<script type="text/javascript">
var rowCount = <?=$rowPageCount[0]?>

$(window).load(function(){
	
	
	if($(".program_table tbody tr").size() == rowCount){
		$(".mo_programPage").hide();	
	}
	
})

function goSearch() {
	$("#searchForm").submit();
}

function goLogin() {
	if (confirm("<?=getMsg('alert.head.login') ?>")){
		$("#urlMaintain").submit();
	} else {
		document.location.href = "<?=$objCommon->getQueryString('index.php', 0 ,$_REQUEST)?>";
	}
}

function more(){
	
	//다음페이지 계산
	var reqPageNo = (Math.ceil($(".program_table tbody tr").size()/<?=$pageRows?>)+1);
	
	$.post("index.php",
	{
		reqPageNo : reqPageNo,
		stype : $("#stype").val(),
		sval : $("#sval").val(),
		view_loc_type : 1
		
	},function(data){
		
		$(".program_table tbody").append(data);
		if($(".program_table tbody tr").size() == rowCount){
			$(".mo_programPage").hide();
		}
		
		
	}).fail(function(error){
		
	})
	
}
</script>
</head>
<body>

	<div id="wrap">
    <? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
    <? include "sub_visual/sub_visual.php" ?>
    <!-- 프로그램시작 -->
	<div class="programCon" data-aos="fade-down" data-aos-delay="400">
		<? if($category_tablename) {
			$checkClass = (!$_REQUEST['scategory_fk'] ? "active" : "");
		?>
		
		<ul class="faq_tab">
			
				<li class="<?=$checkClass?>"><a href="index.php"><?=getMsg('th.all')?></a></li>
				<? 
					
				while ($row=mysql_fetch_assoc($category_result)) { 
						$checkClass =  ($_REQUEST['scategory_fk'] == $row['no'] ? "active" : "");
				?>
				<li class="<?=$checkClass?>"><a href="index.php?scategory_fk=<?=$row[no]?>"/><?=$row[name]?></a></li>
				<?
					}
					
				?>
		</ul>
		<?
		}
		?>

		<div class="program_table <?=$listImgClass?>">
			<table class="ig_type">
				<colgroup>
					<col class="w80 none1000" />
					<?
					if($gallery){
					?>
					<col class="wauto" />
					<?
					}
					?>
					<col width="*" />
					<?
					if($useAnswer){
					?>
					<col class="w70" />
					<?
					}
					?>
					<?
					if($useSecret){
					?>
					<col class="w70" />
					<?
					}
					?>
					<col class="w100" />
					<col class="w150" />
					<?
					if($userCon){
					?>
					<col class="w70" />
					<?
					}
					?>
					
				</colgroup>
				<thead>
					<tr>
						<th class="none1000"><?=getMsg('th.no') ?></th>
						<?
						if($gallery){
						?>
						<th></th>
						<?
						}
						?>
						<th><?=getMsg('th.title') ?></th>
						<?
						if($useAnswer){
						?>
						<th><?=getMsg('th.answer') ?></th>
						<?
						}
						?>
						<?
						if($useSecret){
						?>
						<th><?=getMsg('th.secret') ?></th>
						<?
						}
						?>
						<th><?=getMsg('th.name') ?></th>
						<th><?=getMsg('th.registdate') ?></th>
						<?
						if($userCon){
						?>
						<th><?=getMsg('th.userCon') ?></th>
						<?
						}
						?>
						
					</tr>
				</thead>
				<tbody class="programBody">
				<?
				}
				$readUrl = "";
				$i = 0;
				if($rowPageCount[0] > 0){
					while ($row=mysql_fetch_assoc($result)) {
						
						if($useContents || $isComment || $useAnswer){
							
							$readUrl = "style='cursor:pointer;' onclick=\"location.href='".$objCommon->getQueryString('read.php', $row['no'], $_REQUEST)."'\"";
							
							if($useSecret){
								if($row['secret'] == '1'){
									if(!$loginCheck){ // 비로그인 상태
										if($row['secret'] == "1" && $row['member_fk'] > 0){  // 비공개글, 회원작성글
											
											$readUrl="style='cursor:pointer;' onclick=\"goLogin()\"";
										} else if($row['secret'] == '1'){
											if($usePassword){
												$readUrl = "style='cursor:pointer;' onclick=\"location.href='".$objCommon->getQueryString('password.php', $row['no'], $_REQUEST)."'\"";
											}else{
												//$readUrl="style='cursor:pointer;' onclick=\"alert('".getMsg('alert.text.secret_article')."');\"";
												$readUrl = "style='cursor:pointer;' onclick=\"location.href='".$objCommon->getQueryString('password.php', $row['no'], $_REQUEST)."'\"";
											}
										}
									}else{			 // 로그인 상태
										if($row['secret'] == "1" && $row['member_fk'] > 0){  // 비공개글, 회원작성글
											if($row['member_fk'] != $_SESSION['member_no']){					// 비공개글, 본인작성글이 아닐경우
												//$readUrl="style='cursor:pointer;' onclick=\"alert('".getMsg('alert.text.secret_article')."');\"";
												$readUrl = "style='cursor:pointer;' onclick=\"location.href='".$objCommon->getQueryString('password.php', $row['no'], $_REQUEST)."'\"";
											}
										}
									}
								}
							}
						}
						//(지점 > 진료과목) 체크 로직
						$firstTitle = "";
						if($branch && $row['hospital_name']){
						
							$firstTitle .= "<span class='branch01'>" .$row['hospital_name']. "</span>"; 
						
						}
						if($clinic && $row['clinic_name']){
							if($row['firstTitle'] != "["){
								$firstTitle.="";
							}	
							$firstTitle.= "<span class='branch02'>" .$row['clinic_name']. "</span>";
						} 
						
						$firstTitle.="";
							
						if(!$firstTitle){
							$firstTitle = "";
						}	
				?>
					<tr class="<?=$row['nested'] > 0 && $useWriteAnswer ? "answer_left" : ""?>">
						<td class="none1000">
						<?
						if($row['top'] == "1"){ 
						?>
						<span class="noti_icon"><?=getMsg('lable.td.notice') ?></span>
						<?
						}else{
						?>
						<?=$unickNum ? $row['no'] : $rowPageCount[0] - (($_REQUEST['reqPageNo']-1)*$pageRows) - $i?>
						<?
						}
						?>
						</td>
						<?
						$imageNonetype = (!$useNoImg && !$row['imagename1']);						
						if($gallery){
							if(!$imageNonetype){
						?>
						<td class="nt_list">
                            <div class="img_box">
							<? if($listImageResize) {?>
								<?
								if(is_file($_SERVER['DOCUMENT_ROOT'].$uploadPath.$row['listimage'])){
								?>
								<img src="<?=$uploadPath?><?=$row['listimage']?>"/>
								<?
								}else{
								?>
									<?if("/img/no_image.png" == RegexImgIgnore($row['contents'], $editImgUse)  && !$useNoImg ){ ?>
									<? }else { ?>
									<img src="<?=RegexImgIgnore($row['contents'],$editImgUse) ?>" alt=""  />
									<? } ?>
								<?
								}
								?>	
							<? } else { ?>
							<?
							if(is_file($_SERVER['DOCUMENT_ROOT'].$uploadPath.$row['imagename1'])){
							?>
							<!-- <img src="<?=$uploadPath?><?=$row['imagename1']?>" alt="<?=$row['image1_alt']?>" title="<?=$row['image1_alt']?>" width="130" height="130" /> -->
							<img src="<?=$uploadPath?><?=$row['imagename1']?>" alt="<?=$row['image1_alt']?>" title="<?=$row['image1_alt']?>" <?= $listImageType==1?getImgSizeCheck($_SERVER['DOCUMENT_ROOT'].$uploadPath.$row['imagename1']):$listImageHtml ?> />
							<?
							}else{
							?>
								<?if("/img/no_image.png" == RegexImgIgnore($row['contents'], $editImgUse)  && !$useNoImg ){ ?>
								<? }else { ?>
								<img src="<?=RegexImgIgnore($row['contents'],$editImgUse) ?>" alt=""  width="" height=""/ class="no_imgbox"/>
								<? } ?>
							<?
							}
							?>										
							<? } ?>
                            </div>
						</td>
						<?
							}
						}
						?>
						<td class="tit" <?=$readUrl ?> colspan="<?=$imageNonetype ? 2 : 0?>">							
							<p class="<?=$row['nested'] > 0 && $useWriteAnswer ? "answer_icon" : ""?>" <?=$row['nested'] > 0 && $useWriteAnswer ? "style='padding-left:".($row['nested']*10)."px'" : ""?>>
								
								<?
								if($row['nested'] > 0 && $useWriteAnswer) { 
								?>
								<img src="/img/answer_icon.gif" alt="" /><span class="ans_icon"><?=getMsg('th.answer') ?></span>
								<?
								}else if($row['top'] == "1"){ ?>
								
								<?}?>
								<?=$firstTitle?> <?=$row['title'] ?>
								<? if (checkNewIcon($row['registdate'], $row['newicon'], 1)) { ?>
								<span class="new_icon gre">NEW</span>
								<? } ?>
							</p>
							<span class="consult_on">
								<?
								if($useAnswer){
								?>
								<!-- 답변 여부 -->
								<?=getAnwserUserName($row['answer'], true)?>
								<?
								}
								?>
								<?
								if($useSecret){
								?>
								<!-- 공개 or 비공개 -->
								<?=getSecretUserName($row['secret'], true)?>
								<?
								}
								?>
							</span>
							</td>
						<?
						if($useAnswer){
						?>
						<td class="none1000"><?=getAnwserUserName($row['answer'], true)?></td>
						<?
						}
						?>
						<?
						if($useSecret){
						?>
						<td class="none1000"><?=getSecretUserName($row['secret'], true)?></td>
						<?
						}
						?>
						<td><p><?=$row['name']?></p></td>
						<td class="marmar">
						<?
						//날짜만 보여지는지 시간까지 보여지는지에 체크
						
						if($timeDate){
						?>
						<?= getDateTimeFormat($row['registdate']) ?>
						<?
						}else{
						?>
						<?= getYMD($row['registdate']) ?>
						<?
						}
						?> 
						
						
						</td>
						<?
						if($userCon){
						?>
						<td class="hi_td marmar">
						<span class="hit"><img src="/manage/img/ucount_icon.png" /></span><?=$row['readno']?>				
						</td>
						<?
						}
						?>
						
					</tr>
					
				<?
					}
				}else{ 
				?>
				<tr>
					<td>
					<?=getMsg('message.tbody.data') ?>
					<script>
					var p_tObj = $(".program_table table"); 
					$(p_tObj).find(".programBody tr td").attr("colspan",$(p_tObj).find("thead th").size());
					</script>
					</td>
				</tr>
				<?
				}
				
				if($_REQUEST['view_loc_type'] == 0){
				?>
					
				</tbody>
			</table>
		</div>
		<?
		if($rowPageCount[0] > $pageRows){
		?>
		<div class="mo_programPage"><a href="javascript:;" onclick="more()"><?=getMsg('btn.more') ?><span>+</span></a></div>
		<?
		}
		?>
		<?
		if($useBtn){
		?>
		<dl class="write_btn">
			<dd><a href="write.php"><?=getMsg('btn.write') ?></a></dd>
		</dl>
		<?
		}
		?>
		
		<?=pageListUser($objCommon->reqPageNo, $rowPageCount[1], $objCommon->getQueryString('index.php', 0, $_REQUEST))?>
		<form name="searchForm" id="searchForm" action="index.php" method="get">
		<div class="program_search">
			<? 
			if($branch) { 
			$hospital = new Hospital(999, $_REQUEST);
			$hResult = $hospital->branchSelect();
			?>
			<select name="shospital_fk" title="<?=getMsg('alt.select.shospital_fk') ?>" onchange="goSearch();">
				<option value=""><?=getMsg('lable.option.branch') ?></option>
				<? while ($row=mysql_fetch_assoc($hResult)) { ?>
				<option value="<?=$row['no']?>" <?=getSelected($row['no'],$_REQUEST['shospital_fk'])?>><?=$row['name']?></option>
				<? } ?>
			</select>
			<? } ?>
			
			<? 
			if ($clinic) {
				$clinicObj = new Clinic(999, $_REQUEST);
				$clinicList = $clinicObj->selectBoxList(0,($branch ? $_REQUEST['shospital_fk'] : DEFAULT_BRANCH_NO),$_REQUEST['sclinic_fk']);
			?>
			<select name="sclinic_fk" title="<?=getMsg('alt.select.sclinic_fk') ?>" onchange="goSearch();">
				<?=$clinicList?>
			</select>
			<? } ?>
			<select name="stype" title="<?=getMsg('alt.select.stype') ?>">
				<option value="all" <?=getSelected($_REQUEST[stype], "all") ?>><?=getMsg('lable.option.all') ?></option>
				<option value="title" <?=getSelected($_REQUEST[stype], "title") ?>><?=getMsg('th.title') ?></option>
				<option value="name" <?=getSelected($_REQUEST[stype], "name") ?>><?=getMsg('th.name') ?></option>
				<option value="contents" <?=getSelected($_REQUEST[stype], "contents") ?>><?=getMsg('th.contents') ?></option>
			</select>
		
			<span>
				<input type="text" name="sval" value="<?=$_REQUEST[sval]?>" title="<?=getMsg('alt.text.search') ?>" />
				<a href="javascript:;" onclick="$('#searchForm').submit()" ><?=getMsg('btn.search') ?></a>
			</span>
		</div>
		</form>
	</div>
	 <!-- 프로그램끝 -->
    <? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
    <? include "sub_footer/sub_footer.php" ?>
</div>
</body>
</html>
<?
}
?>