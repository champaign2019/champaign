<?session_start();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?

include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Common.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Hospital.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Clinic.class.php";

include "config.php";
if ($boardgrade == 3) { // 게시판접근 권한처리
	include $_SERVER['DOCUMENT_ROOT']."/include/gradeCheck.php";
}

$common = new Common($pageRows, $tablename, $_REQUEST);
$_REQUEST['orderby'] = $orderby; //정렬 배열 선언
$rowPageCount = $common->getCount($_REQUEST);
$result = $common->getList($_REQUEST);
	if($_REQUEST['view_loc_type'] != 1){ //제거 하기 

?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/include/headHtml.php" ?>
<script type="text/javascript" src="/js/masonry.pkgd.min.js"></script>
<script>
var rowCount = '<?=$rowPageCount[0]?>';
var $grid;
	$(window).load(function(){
		$grid = $('.card_type ul').masonry({
			  itemSelector: 'li'
		});
		
		if($(".programCon ul li").size() == rowCount){
			$(".mo_programPage").hide();	
		}
		$(".gal_wrap ul li").mouseenter(function(){
			$(this).addClass("on_e");
		});
		$(".gal_wrap ul li").mouseleave(function(){
			$(this).removeClass("on_e");
		});
	});
	
	function goSearch() {
		$("#searchForm").submit();
	}



	function more(){
		
		//다음페이지 계산
		var reqPageNo = (Math.ceil($(".programCon ul li").size()/<?=$pageRows?>)+1);
		
		$.post("index.php",
		{
			reqPageNo : reqPageNo,
			stype : $("#stype").val(),
			sval : $("#sval").val(),
			view_loc_type : 1
		},function(data){
			var elems = $( data );
			console.log(data);
			$grid.append(elems).masonry( 'appended', elems, true );
			if($(".programCon ul li").size() == rowCount){
				$(".mo_programPage").hide();
			}
			
		}).fail(function(error){
			
		})
		
	}
</script>
</head>
<body>

	<div id="wrap">
    <? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
    <? include "sub_visual/sub_visual.php" ?>
    <!-- 프로그램시작 -->
	<div class="programCon" data-aos="fade-down" data-aos-delay="400">
		<div class="bnas_wrap">
			<div class="card_type">
				<ul>
				<?
				}
				
				$topClass = "";
				$readUrl = "";
				$i = 0;
				 if ($rowPageCount[0] > 0) { 
							while ($row=mysql_fetch_assoc($result)) {
								$readUrl = "style='cursor:pointer;' onclick=\"location.href='".$common->getQueryString('read.php', $row[no], $_REQUEST)."'\"";
								
								//(지점 > 진료과목) 체크 로직
								$firstTitle = "";
								if($branch && $row['hospital_name']){
								
									$firstTitle .= "<span class='branch01'>" .$row['hospital_name']. "</span>"; 
								
								}
								if($clinic && $row['clinic_name']){
									if($row['firstTitle'] != "["){
										$firstTitle.="";
									}	
									$firstTitle.= "<span class='branch02'>" .$row['clinic_name']. "</span>";
								}
								
								$firstTitle.="";
							
								if("[]" == $firstTitle){
									$firstTitle = "";
								}
					?>
					<li>
						<a <?=$readUrl?>>
							<div class="img">
								<?
								if(is_file($_SERVER['DOCUMENT_ROOT'].$uploadPath.$row['imagename1'])){
								?>
								<img src="<?=$uploadPath?><?=$row['imagename1']?>" alt="<?=$row['image1_alt']?>" title="<?=$row['image1_alt']?>" />
								<?
								}else{
								?>
								<img src="/img/no_image.png" alt="noImage" class="noimg_ty" />
								<?
								}
								?>
							</div>
							<div class="txt">
								<div class="ti"><?=$row['title']?></div>
								<div class="um">
									<?=utf8_strcut(strip_tags($row['contents']),50,'...')?>
								</div>
							</div>
						</a>
					</li>
					<?
						$i++;
					}
				}else{ 
				?>
				<?
				$_REQUEST['nodata'] = "해당되는 데이터가 없습니다.";
				include $_SERVER['DOCUMENT_ROOT']."/nodata/index.php" 
				?>
				<?
				}
				
				if($_REQUEST['view_loc_type'] == 0){
				?>
				</ul>
			</div>
			<?
			if($rowPageCount[0] > $pageRows){
			?>
			<div class="card_btn">
				<a href="javascript:;" onclick="more()" class="mo_programPage"><span>+</span> 더보기</a>
			</div>
			<?
			}
			?>
			

			
		</div>
		
	</div>
	 <!-- 프로그램끝 -->
    <? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
    <? include "sub_footer/sub_footer.php" ?>
</div>
</body>
</html>
<? } ?>