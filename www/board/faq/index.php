<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Common.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Hospital.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Clinic.class.php";
include "config.php";

$_REQUEST['orderby'] = $orderby;

$objCommon = new Common($pageRows, $tablename, $_REQUEST);
$rowPageCount = $objCommon->getCount($_REQUEST);
$result = $objCommon->getList($_REQUEST);

$category_result = $category_tablename ? $objCommon->getCategoryList($_REQUEST) : null;
if($_REQUEST['view_loc_type'] == 0){
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/include/headHtml.php" ?>
<script type="text/javascript">
$(window).load(function(){
	
	
	if($(".program_table tbody tr").size() == rowCount){
		$(".mo_programPage").hide();	
	}
	$(".program_table td.tit").mouseenter(function(){
		$(this).parent("tr").addClass("on_e");
	});
	$(".program_table td.tit").mouseleave(function(){
		$(this).parent("tr").removeClass("on_e");
	});
	
})

var rowCount = <?=$rowPageCount[0]?>

$(window).load(function(){
	
	
	if($(".program_table tbody tr").size() == rowCount){
		$(".mo_programPage").hide();	
	}
	
})

function goSearch() {
	$("#searchForm").submit();
}


function more(){
	
	//다음페이지 계산
	var reqPageNo = (Math.ceil($(".program_table tbody tr").size()/<?=$pageRows?>)+1);
	
	$.post("index.php",
	{
		reqPageNo : reqPageNo,
		stype : $("#stype").val(),
		view_loc_type : 1,
		sval : $("#sval").val()
		
	},function(data){
		
		$(".program_table tbody").append(data);
		if($(".program_table tbody tr").size() == rowCount){
			$(".mo_programPage").hide();
		}
		
		
	}).fail(function(error){
		
	})
	
}
</script>
</head>

<body>

	<div id="wrap">
    <? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
    <? include "sub_visual/sub_visual.php" ?>
    <!-- 프로그램시작 -->
	<div class="programCon" data-aos="fade-down" data-aos-delay="400">
		
		<? if(mysql_num_rows($category_result) > 0) {
			$checkClass = (!$_REQUEST['scategory_fk'] ? "active" : "");
		?>
		
		<ul class="faq_tab">
			
				<li class="<?=$checkClass?>"><a href="index.php"><?=getMsg('th.all')?></a></li>
				<? 
					
				while ($row=mysql_fetch_assoc($category_result)) { 
						$checkClass =  ($_REQUEST['scategory_fk'] == $row['no'] ? "active" : "");
				?>
				<li class="<?=$checkClass?>"><a href="index.php?scategory_fk=<?=$row[no]?>"/><?=$row[name]?></a></li>
				<?
					}
					
				?>
		</ul>
		<?
		}
		?>
		
		<div class="program_table">
			<table>
				<colgroup>
					<col class="w80 none1000" />
					<col class="w100 w100w" />
					<col width="*" />
					<col class="w150" />
					<col class="w70" />
				</colgroup>
				<thead>
					<tr>
						<th class="none1000"><?=getMsg('th.no')?></th>
						<th><?=getMsg('th.category')?></th>
						<th><?=getMsg('th.title') ?></th>
						<th><?=getMsg('th.registdate') ?></th>
						<th><?=getMsg('th.userCon')?></th>
					</tr>
				</thead>
				<tbody class="programBody">
				<?
				}
				$readUrl = "";
				$i = 0;
				if($rowPageCount[0] > 0){
					while ($row=mysql_fetch_assoc($result)) {
						$readUrl = "style='cursor:pointer;' onclick=\"location.href='".$objCommon->getQueryString('read.php', $row['no'], $_REQUEST)."'\"";
							
						//(지점 > 진료과목) 체크 로직
						$firstTitle = "";
						if($branch && $row['hospital_name']){
						
							$firstTitle .= "<span class='branch01'>" .$row['hospital_name']. "</span>"; 
						
						}
						if($clinic && $row['clinic_name']){
							if($row['firstTitle'] != "["){
								$firstTitle.="";
							}	
							$firstTitle.= "<span class='branch02'>" .$row['clinic_name']. "</span>";
						} 
						
						$firstTitle.="";
							
						if(!$firstTitle){
							$firstTitle = "";
						}	
				?>
					<tr id="tit">
						<td class="none1000"><?=$rowPageCount[0] - (($objCommon->reqPageNo-1)*$pageRows) - $i?></td>
						<td class="category"><?=$row['category_name'] ?></td>
						<td class="tit" <?=$readUrl ?>>
							<p class="all">

							<? if ($row[top] == "1") { ?>
							<img class="noticeIcon" src="/img/noticeIcon.png" alt="" />	
							<?
							}
							?>
							<?=$firstTitle?> <?=$row['title']?>
							<? if (checkNewIcon($row[registdate], $row[newicon], 1)) { ?>
							<img class="newIcon" src="/img/newIcon.png" alt="" />
							<?
							}
							?>
							</p>
						</td>
						<td>
						<?
						//날짜만 보여지는지 시간까지 보여지는지에 체크
						if($timeDate){
						?>
						<?= getDateTimeFormat($row['registdate']) ?>
						<?
						}else{
						?>
						<?= getYMD($row['registdate']) ?>
						<?
						}
						?> 
						</td>
						<td>
							<span class="hit"><?=getMsg('th.userCon')?> : </span><?=$row[readno]?>		
						</td>
					</tr>
					<?
					}
				}else{ 
				?>
				<tr>
					<td colspan="5"><?=getMsg('message.tbody.data') ?></td>
				</tr>
				<?
				}
				
				if($_REQUEST['view_loc_type'] == 0){
				?>
				</tbody>
			</table>
		</div>
		<?
		if($rowPageCount[0] > $pageRows){
		?>
		<div class="mo_programPage"><a href="javascript:;" onclick="more()"><?=getMsg('btn.more') ?><span>+</span></a></div>
		<?
		}
		?>
		
		<?=pageListUser($objCommon->reqPageNo, $rowPageCount[1], $objCommon->getQueryString('index.php', 0, $_REQUEST))?><!-- 페이징 처리 -->
		
		<form name="searchForm" id="searchForm" action="index.php" method="get">
		<div class="program_search">
			<? 
				if ($branch) {
					$hospitalObj = new Hospital(999, $_REQUEST);
					$hResult = $hospitalObj->branchSelect();
			?>
			<select name="shospital_fk" title="<?=getMsg('alt.select.shospital_fk') ?>" onchange="goSearch();">
				<option value="-1" <?=getSelected(-1, $_REQUEST['shospital_fk'])?>><?=getMsg('lable.option.branch') ?></option>
				<? while ($row=mysql_fetch_assoc($hResult)) { ?>
				<option value="<?=$row['no']?>" <?=getSelected($row['no'], $_REQUEST['shospital_fk'])?>><?=$row['name']?></option>
				<? } ?>
			</select>
			<? } ?>
			<? 
				if ($clinic) {
					$clinicObj = new Clinic(999, $_REQUEST);
					$clinicList = $clinicObj->selectBoxList(0,($branch ? $_REQUEST['shospital_fk'] : DEFAULT_BRANCH_NO),$_REQUEST['sclinic_fk']);
			?>
			<select name="sclinic_fk" title="<?=getMsg('alt.select.sclinic_fk') ?>" onchange="goSearch();">
				<?=$clinicList?>
			</select>
			<? } ?>
			<select name="stype" title="<?=getMsg('alt.select.stype') ?>">
				<option value="all" <?=getSelected($_REQUEST[stype], "all") ?>><?=getMsg('lable.option.all') ?></option>
				<option value="title" <?=getSelected($_REQUEST[stype], "title") ?>><?=getMsg('th.title') ?></option>
				<option value="name" <?=getSelected($_REQUEST[stype], "name") ?>><?=getMsg('th.name')?></option>
				<option value="contents" <?=getSelected($_REQUEST[stype], "contents") ?>><?=getMsg('th.contents') ?></option>
			</select>
		
			<span>
				<input type="text" name="sval" value="<?=$_REQUEST[sval]?>" title="<?=getMsg('alt.text.search') ?>" />
				<a href="javascript:;" onclick="$('#searchForm').submit()" ><?=getMsg('btn.search') ?></a>
			</span>
		</div>
		</form>
	</div>
	 <!-- 프로그램끝 -->
    <? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
    <? include "sub_footer/sub_footer.php" ?>
</div>
</body>
</html>
</html>
<?
}
?>