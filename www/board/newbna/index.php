<?session_start();

include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Common.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Hospital.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Clinic.class.php";


include "config.php";
if ($boardgrade == 3) { // 게시판접근 권한처리
	include $_SERVER['DOCUMENT_ROOT']."/include/gradeCheck.php";
}

$_REQUEST['category_tablename'] = $category_tablename;
$objCommon = new Common($pageRows, $tablename, $_REQUEST);
$rowPageCount = $objCommon->getCount($_REQUEST);
$result = $objCommon->getList($_REQUEST);

$category_result = $category_tablename ? $objCommon->getCategoryList($_REQUEST) : null;


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www10.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<? include $_SERVER['DOCUMENT_ROOT']."/include/headHtml.php" ?>
<script type='text/javascript' src='/js/jquery.bxslider.min.js'></script>
<script type='text/javascript' src='newbnaScript.js'></script> <!-- 전후사진 bx슬라이드 스크립트 -->
<script type="text/javascript">
var rowCount = "<?=$rowPageCount[0]?>";
var listSize = "<?=mysql_num_rows($result)?>";


$(window).load(function(){
	if(listSize == rowCount){
		$(".mo_programPage").hide();	
	}
	$(".bna_wrap ul li").mouseenter(function(){
		$(this).addClass("on_e");
	});
	$(".bna_wrap ul li").mouseleave(function(){
		$(this).removeClass("on_e");
	});
	
	if(listSize > 0){
		slideInner();
		iconRemove(0);
		typeClassSelect($(".icon_type > div:not(:hidden):eq(0)").attr("data-value"));
	}

});



function goSearch() {
	$("#searchForm").submit();
}



function more(){
	
	//다음페이지 계산
	var reqPageNo = (Math.ceil($(".programCon .bnas_pager div#bna").size()/<?=$pageRows?>)+1);
	
	$.post("index.php",
	{
		reqPageNo : reqPageNo,
		stype : $("#stype").val(),
		sval : $("#sval").val(),
		view_loc_type : 1
		
	},function(data){
		
		$(".programCon .bnas_pager").append($(data).find(".bnas_pager").children("div"));
		
		if($(".programCon .bnas_pager > div").size() == rowCount){
			$(".mo_programPage").hide();
		}
		
		$(".bna_slide ul").off();
		//slideInner();
		
		
	}).fail(function(error){
		
	})
	
}
</script>
</head>
<body>

	<div id="wrap">
    <? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
    <? include "sub_visual/sub_visual.php" ?>
    <!-- 프로그램시작 -->
	<div class="programCon" data-aos="fade-down" data-aos-delay="400">
		<!--상단카테고리-->
		<? if(mysql_num_rows($category_result) > 0) {
			$checkClass = (!$_REQUEST['scategory_fk'] ? "active" : "");
		?>
		
		<ul class="faq_tab">
			
				<li class="<?=$checkClass?>"><a href="index.php"><?=getMsg('th.all')?></a></li>
				<? 
					
				while ($row=mysql_fetch_assoc($category_result)) { 
						$checkClass =  ($_REQUEST['scategory_fk'] == $row['no'] ? "active" : "");
				?>
				<li class="<?=$checkClass?>"><a href="index.php?scategory_fk=<?=$row[no]?>"/><?=$row[name]?></a></li>
				<?
					}
					
				?>
		</ul>
		<?
		}
		?>
		
		<!--상단카테고리-->
		<div class="bnas_wrap">
			<?
			if($rowPageCount[0] > 0){
			?>
			<div class="bna_slide">
			<ul></ul>
			<?
			if($isBeafIcon){
			?>
			<p class="bef">Before</p>
			<p class="bef aft">After</p>
			<?
			}
			?>
			</div>
			<div class="under_txt"></div>
			
			<div class="icon_type">
				<div class="front on" data-value="0">
					<img src="/img/bna_front.png" />
					<p><?=getMsg('th.front')?></p>
				</div>
				<div class="ff" data-value="1">
					<img src="/img/bna_ff.png" />
					<p><?=getMsg('th.ff')?></p>
				</div>
				<div class="side" data-value="2">
					<img src="/img/bna_side.png" />
					<p><?=getMsg('th.side')?></p>
				</div>
			</div>
			<?
			}
			?>
		</div>
		<?
		if($rowPageCount[0] > 0){
		?>
		<div class="bnas_pager">
			<?
			
			$topClass = "";
			$readUrl = "";
			$i = 0;
			
			$cCountIndex =  ($pageRows*($_REQUEST['reqPageNo']-1)); 
			while ($row=mysql_fetch_assoc($result)) {
					$readUrl = "style='cursor:pointer;' onclick=\"location.href='".$objCommon->getQueryString('read.php', $row[no], $_REQUEST)."'\"";
							
					//(지점 > 진료과목) 체크 로직
					$firstTitle = "";
					if($branch && $row['hospital_name']){
					
						$firstTitle .= "<span class='branch01'>" .$row['hospital_name']. "</span>"; 
					
					}
					if($clinic && $row['clinic_name']){
						if($row['firstTitle'] != "["){
							$firstTitle.="";
						}	
						$firstTitle.= "<span class='branch02'>" .$row['clinic_name']. "</span>";
					}
					
					$firstTitle.="";
				
					if("[]" == $firstTitle){
						$firstTitle = "";
					}
			?>
			<div id="bna">
				<a data-slide-index="<?=$i++?>" href="">
					<p></p>
					<?
					$befor = "";
					$after = "";
					if(is_file($_SERVER['DOCUMENT_ROOT'].$uploadPath.$row['imagename1'])){
						$befor .= $uploadPath.$row['imagename1']."|";
					}else{
						$befor .= "/img/no_image.png|";
					}
					
					if(is_file($_SERVER['DOCUMENT_ROOT'].$uploadPath.$row['imagename3'])){
						$befor .= $uploadPath.$row['imagename3']."|";
					}else{
						$befor .= "/img/no_image.png|";
					}
					
					if(is_file($_SERVER['DOCUMENT_ROOT'].$uploadPath.$row['imagename5'])){
						$befor .= $uploadPath.$row['imagename5']."|";
					}else{
						$befor .= "/img/no_image.png";
					}
					
					if(is_file($_SERVER['DOCUMENT_ROOT'].$uploadPath.$row['imagename2'])){
						$after .= $uploadPath.$row['imagename2']."|";
					}else{
						$after .= "/img/no_image.png|";
					}
					
					if(is_file($_SERVER['DOCUMENT_ROOT'].$uploadPath.$row['imagename4'])){
						$after .= $uploadPath.$row['imagename4']."|";
					}else{
						$after .= "/img/no_image.png|";
					}
					
					if(is_file($_SERVER['DOCUMENT_ROOT'].$uploadPath.$row['imagename6'])){
						$after .= $uploadPath.$row['imagename6']."|";
					}else{
						$after .= "/img/no_image.png";
					}
					
					if(is_file($_SERVER['DOCUMENT_ROOT'].$uploadPath.$row['imagename1'])){
					?>
					<img src="<?=$uploadPath?><?=$row['imagename1']?>" alt="<?=$row['image1_alt']?>" title="<?=$row['image1_alt']?>"  data-image="<?=$befor?>" data-title="<?=$row['title']?>"/>
					<?
					}else{
					?>
					<img src="/img/no_image.png" alt="noImage" title="noImage" data-image="<?=$befor?>" data-image="<?=$befor?>" data-title="<?=$row['title']?>"/>
					<?
					}
					?>
					<?
					if(is_file($_SERVER['DOCUMENT_ROOT'].$uploadPath.$row['imagename2'])){
						
					?>
					<img src="<?=$uploadPath?><?=$row['imagename2']?>" alt="<?=$row['image2_alt']?>" title="<?=$row['image2_alt']?>"  data-image="<?=$after?>"/>
					<?
					}else{
					?>
					<img src="/img/no_image.png" alt="noImage" title="noImage" data-image="<?=$after?>"/>
					<?
					}
					?> 
				</a>
			</div>
			<!-- <div>
				<a data-slide-index="1" href="">
					<p></p>
					<img src="/img/before_img.jpg" />
					<img src="/img/after_img.jpg" /> 
				</a>
			</div>
			<div>
				<a data-slide-index="2" href="">
					<p></p>
					<img src="/img/before_img.jpg" />
					<img src="/img/after_img.jpg" /> 
				</a>
			</div> -->
			<?
			}
		
			?>
		
		</div>
		<?
		if($rowPageCount[0] > $pageRows){
		?>
		<div class="mo_programPage"><a href="javascript:;" onclick="more()"><?=getMsg('btn.more') ?><span>+</span></a></div>
		<?
		}
		?>
		
		<?=pageListUser($objCommon->reqPageNo, $rowPageCount[1], $objCommon->getQueryString('index.php', 0, $_REQUEST))?><!-- 페이징 처리 -->
		<?
		}else{
		?>
		<?
		$_REQUEST['nodata'] = "해당되는 데이터가 없습니다.";
		include $_SERVER['DOCUMENT_ROOT']."/nodata/index.php" 
		?>
		<?
		}
		?>
		<form name="searchForm" id="searchForm" action="index.php" method="get">
		<div class="program_search">
			<? 
				if ($branch) {
					$hospitalObj = new Hospital(999, $_REQUEST);
					$hResult = $hospitalObj->branchSelect();
			?>
			<select name="shospital_fk" title="<?=getMsg('alt.select.shospital_fk') ?>" onchange="goSearch();">
				<option value="-1" <?=getSelected(-1, $_REQUEST['shospital_fk'])?>><?=getMsg('lable.option.branch') ?></option>
				<? while ($row=mysql_fetch_assoc($hResult)) { ?>
				<option value="<?=$row['no']?>" <?=getSelected($row['no'], $_REQUEST['shospital_fk'])?>><?=$row['name']?></option>
				<? } ?>
			</select>
			<? } ?>
			<? 
				if ($clinic) {
					$clinicObj = new Clinic(999, $_REQUEST);
					$clinicList = $clinicObj->selectBoxList(0,($branch ? $_REQUEST['shospital_fk'] : DEFAULT_BRANCH_NO),$_REQUEST['sclinic_fk']);
			?>
			<select name="sclinic_fk" title="<?=getMsg('alt.select.sclinic_fk') ?>" onchange="goSearch();">
				<?=$clinicList?>
			</select>
			<? } ?>
			<select name="stype" title="<?=getMsg('alt.select.stype') ?>">
				<option value="all" <?=getSelected($_REQUEST[stype], "all") ?>><?=getMsg('lable.option.all') ?></option>
				<option value="title" <?=getSelected($_REQUEST[stype], "title") ?>><?=getMsg('th.title') ?></option>
				<option value="name" <?=getSelected($_REQUEST[stype], "name") ?>><?=getMsg('th.name')?></option>
				<option value="contents" <?=getSelected($_REQUEST[stype], "contents") ?>><?=getMsg('th.contents') ?></option>
			</select>
			<span>
				<input type="text" name="sval" value="<?=$_REQUEST[sval]?>" title="<?=getMsg('alt.text.search') ?>" />
				<a href="javascript:;" onclick="$('#searchForm').submit()" ><?=getMsg('btn.search') ?></a>
			</span>
		</div>
		</form>
	</div>
	 <!-- 프로그램끝 -->
    <? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
    <? include "sub_footer/sub_footer.php" ?>
</div>
</body>
</html>
