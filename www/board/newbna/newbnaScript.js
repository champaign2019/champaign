/**
 * 
 */

var noImage = "/img/no_image.png";
var slideHtml = "";
var typeNo = 0;
var chkidx = 0;
var typeClass = 0;
var clickcount = 1;
var chkstart = 0;
var slideInner = function(){
	$(".bna_slide ul").html("");
	$(".bnas_pager > div").each(function(){
		slideHtml = "<li>";
		slideHtml += "<div class='before'>";
		slideHtml += "<img src='"+$(this).find("img:eq(0)").attr("src")+"' />";
		slideHtml += "</div>";
		slideHtml += "<div class='after'>";
		slideHtml += "<img src='"+$(this).find("img:eq(1)").attr("src")+"' />"; 
		slideHtml += "</div>";
		slideHtml += "</li>";
		$(".bna_slide ul").append(slideHtml);
		$(".under_txt").append("<p>"+$(this).find("img:eq(0)").data("title")+"</p>");
	});
	
	var bna_slide = $(".bna_slide ul").bxSlider({
		auto : false,
		pagerCustom : '.bnas_pager',
		controls : true,
		onSliderLoad : function(){
			$('.under_txt > p').eq(0).show();
		},
		onSlideAfter : function(obj){
			$(".icon_type div").each(function(){
				$(this).show();
			});
			var bnaidx = $(obj).index();
			$('.under_txt > p').hide();
			$('.under_txt > p').eq(bnaidx - 1).show();
	//180626 추가
			
			var removeCheck = false;
			iconRemove(bnaidx-1); //아이콘삭제
			
			//둘다 noimage 일 경우
			
			chkidx = ($(".icon_type > div:not(:hidden)").size()*1)-1;
			
			var chkminus = false;
			var minusidx;
			$(".icon_type > div:not(:hidden)").each(function() {
				if($(this).attr("data-value") == typeNo) {
					chkminus = true;
					chkidx = typeNo;
				}
			});
			if(!chkminus) {
				minusidx = $(".icon_type > div:not(:hidden):first").attr("data-value");
				if(minusidx > typeNo) {
					chkidx = minusidx;
					console.log(minusidx);
					console.log("right");
				} else if(minusidx < typeNo){
					chkidx = ($(".icon_type > div:not(:hidden)").size()*1-1);
				} else {
					chkidx = ($(".icon_type > div:not(:hidden)").size()*1+1);
				}
			}

 			if($(obj).find("img:eq(0)").attr("src") == noImage && $(obj).find("img:eq(1)").attr("src") == noImage) {
 				chgImageValue(chkidx);
 				typeClassSelect(chkidx);
 				console.log(chkidx);
 				console.log("***");
			} else {
				$(".icon_type > div:not(:hidden)").each(function(){
					if($(this).attr("data-value") == typeClass) {
						clickcount = 0; 
					}
				});
				if(clickcount==1) {
					typeClassSelect(chkidx);
				} else {
					typeClassSelect(typeNo);
					clickcount = 1; 
				}
 			}
			
 			if(chkstart == 0) {
 				typeClassSelect($(".icon_type > div:not(:hidden):eq(0)").attr("data-value"));
 			}
		}
	});
	
	$(".icon_type > div").click(function(){
		$(".icon_type > div").removeClass("on");
		$(this).addClass("on");
		typeNo = $(".icon_type > div").index(this);
		typeClass = $(this).attr("data-value");
		var obj = $(this);
		chkstart = 1;
		chgImageValue($(obj).data("value"));
		
	});
}
function typeClassSelect(idx) {
	$(".icon_type > div").removeClass("on");
	$(".icon_type > div:eq("+idx+")").addClass("on");
}
function chgImageValue(idx) {
	
	var firstBefore1 = $(".bnas_pager > div:first").find("img:eq(0)").data("image").split("|")[idx];
	var firstAfter2 =  $(".bnas_pager > div:first").find("img:eq(1)").data("image").split("|")[idx];
	
	var lastBefore1 = $(".bnas_pager > div:last").find("img:eq(0)").data("image").split("|")[idx];
	var lastAfter2 =  $(".bnas_pager > div:last").find("img:eq(1)").data("image").split("|")[idx];
	
	
	$(".bnas_pager > div").each(function(i){
		var image1 = $(this).find("img:eq(0)").data("image").split("|");
		$(".bna_slide ul li:not('.bx-clone')").eq(i).find("img:eq(0)").attr("src",image1[idx]);
		var image2 = $(this).find("img:eq(1)").data("image").split("|");
		$(".bna_slide ul li:not('.bx-clone')").eq(i).find("img:eq(1)").attr("src",image2[idx]);
	});
	
	$(".bna_slide ul li.bx-clone:first").find("img:eq(0)").attr("src",lastBefore1);
	$(".bna_slide ul li.bx-clone:first").find("img:eq(1)").attr("src",lastAfter2);
	
	$(".bna_slide ul li.bx-clone:last").find("img:eq(0)").attr("src",firstBefore1);
	$(".bna_slide ul li.bx-clone:last").find("img:eq(1)").attr("src",firstAfter2);
}

function iconRemove(idx) {
	
	var dataAry = $(".bnas_pager > div:eq("+(idx)+") img:eq(0)").attr("data-image").split("|");
	var dataAry2 = $(".bnas_pager > div:eq("+(idx)+") img:eq(1)").attr("data-image").split("|");
	
	if(noImage == dataAry[0] && noImage == dataAry2[0]) {
		$(".icon_type .front").hide();
	}
	if(noImage == dataAry[1] && noImage == dataAry2[1]) {
		$(".icon_type .ff").hide();
	}
	if(noImage == dataAry[2] && noImage == dataAry2[2]) {
		$(".icon_type .side").hide();
	}

}