<?session_start();
include "config.php";
if ($boardgrade == 3)  { // 게시판 접근 권한 처리
?>

<?
}
	$geturl = "http://asp1.krx.co.kr/servlet/krx.asp.DisList4MainServlet?code=".$stockNumber."&gubun=K";
	$gettime="";
	
	$xmlstr = "";
	$disInfo_lenth = 0;

	$line="";
	$disInfo = array(array());
	
	$xml = "";
	$sti = "";

	try{

		$stockXmlData = file_get_contents($geturl);
		
		$sti = simplexml_load_string(trim($stockXmlData));
		
		$disclosureMain = $sti->disclosureMain;
		
		$gettime = $disclosureMain[0]["querytime"];
		
		
		$disinfo_data = $sti->disInfo;

		$disInfo_lenth = count($disinfo_data);
		for($i=0;$i<$disInfo_lenth;$i++){
			
			$disInfo[$i][0] = $disinfo_data[$i]["distime"];
			$disInfo[$i][1] = $disinfo_data[$i]["disTitle"];
			$disInfo[$i][2] = $disinfo_data[$i]["disAcpt_no"];
			$disInfo[$i][3] = $disinfo_data[$i]["submitOblgNm"];
			
		}

	}
	catch(Exception $e){
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<? include $_SERVER['DOCUMENT_ROOT']."/include/headHtml.php" ?>
</head>
<body>
	<div id="wrap">
		<? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
		<? include "sub_visual/sub_visual.php" ?>		
		<div class="programCon" data-aos="fade-down" data-aos-delay="400">
		<div class="program_table <?=$listImgClass?>">
				<table>
					<colgroup>
						<col width="15%" />
						<col width="15%" />
						<col width="55%" />
						<col width="15%" />
					</colgroup>
					<thead>
						<tr>
							<th>번호
							</th>
							<th>일자</th>
							<th>제목</th>
							<th>제출의무자</th>
						</tr>
					</thead>
					<tbody>
						<?
							if ( $disInfo_lenth > 0 ) {
								for ( $i = 0; $i < $disInfo_lenth ; $i++ ) {
						?>
						<tr>
							<td><?=$disInfo_lenth-$i?></td>
							<td><?=substr($disInfo[$i][0], 0, 4) ?>.<?=substr($disInfo[$i][0], 4, 2) ?>.<?=substr($disInfo[$i][0], 6, 2)?></td>		
							<td><a href="#" onclick="window.open('http://kind.krx.co.kr/common/disclsviewer.do?method=search&acptno=<?=$disInfo[$i][2]?>','공시상세보기','width=1200,height=800,top=100,left=350');"><?=$disInfo[$i][1]?></a></td>
							<td><?=$disInfo[$i][3]?></td>
						</tr>
						<?
								}
							}
							else {
						?>
							<tr>
								<td colspan="4">데이터가 없습니다.</td>
							</tr>
						<?
							}
						?>
					</tbody>
				</table>
		</div>
		</div>
		<? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
		<? include "sub_footer/sub_footer.php" ?>
	</div>
</body>
</html>