<?session_start();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?

include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Common.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Hospital.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Clinic.class.php";


include "config.php";
if ($boardgrade == 3) { // 게시판접근 권한처리
	include $_SERVER['DOCUMENT_ROOT']."/include/gradeCheck.php";
}
$_REQUEST['orderby'] = $orderby;
$objCommon = new Common($pageRows, $tablename, $_REQUEST);
$rowPageCount = $objCommon->getCount($_REQUEST);
$result = $objCommon->getList($_REQUEST);

	if($_REQUEST['view_loc_type'] == 0){ //제거 하기 

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/include/headHtml.php" ?>
<script type="text/javascript">
var rowCount = <?=$rowPageCount[0]?>

$(window).load(function(){
	
	
	if($(".program_table tbody tr").size() == rowCount){
		$(".mo_programPage").hide();	
	}
	$(".program_table td.tit").mouseenter(function(){
		$(this).parent("tr").addClass("on_e");
	});
	$(".program_table td.tit").mouseleave(function(){
		$(this).parent("tr").removeClass("on_e");
	});
	
})

function goSearch() {
	$("#searchForm").submit();
}


function more(){
	
	//다음페이지 계산
	var reqPageNo = (Math.ceil($(".before").size()/<?=$pageRows?>)+1);
	
	$.post("index.php",
	{
		reqPageNo : reqPageNo,
		stype : $("#stype").val(),
		sval : $("#sval").val(),
		view_loc_type : 1
		
	},function(data){
		
		$("#bodyUl").append(data);
		if($(".before").size() == rowCount){
			$(".mo_programPage").hide();
		}
		
		
	}).fail(function(error){
		
	})
	
}
</script>
</head>

<body>

	<div id="wrap">
    <? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
    <? include "sub_visual/sub_visual.php" ?>
    <!-- 프로그램시작 -->
	<div class="programCon" data-aos="fade-down" data-aos-delay="400">
		<div class="bna_wrap">
			<ul id="bodyUl">
				<?
				}
				$topClass = "";
				$readUrl = "";
				$i = 0;
				if($rowPageCount[0] > 0){
				while ($row=mysql_fetch_assoc($result)) {
						$readUrl = "style='cursor:pointer;' onclick=\"location.href='".$objCommon->getQueryString('read.php', $row[no], $_REQUEST)."'\"";
								
						//(지점 > 진료과목) 체크 로직
						$firstTitle = "";
						if($branch && $row['hospital_name']){
						
							$firstTitle .= "<span class='branch01'>" .$row['hospital_name']. "</span>"; 
						
						}
						if($clinic && $row['clinic_name']){
							if($row['firstTitle'] != "["){
								$firstTitle.="";
							}	
							$firstTitle.= "<span class='branch02'>" .$row['clinic_name']. "</span>";
						}
						
						$firstTitle.="";
					
						if("[]" == $firstTitle){
							$firstTitle = "";
						}
				?>
				<li>
					<a <?=$readUrl?>>
						<div class="before">
							<?
							if(is_file($_SERVER['DOCUMENT_ROOT'].$uploadPath.$row['imagename1'])){
							?>
							<img src="<?=$uploadPath?><?=$row['imagename1']?>" alt="<?=$row['image1_alt']?>" title="<?=$row['image1_alt']?>" />
							<?
							if($isBeafIcon){
							?>
							<span>Before</span>
							<?
							}
							?>
							<?
							}else{
							?>
							<? if($useNoImg){?>
							<img src="/img/no_image.png" alt="noImage" title="noImage" />
							<? } ?>
							<?
							}
							?>
						</div>
						<div class="after">
							<?
							if(is_file($_SERVER['DOCUMENT_ROOT'].$uploadPath.$row['imagename2'])){
							?>
							<img src="<?=$uploadPath?><?=$row['imagename2']?>" alt="<?=$row['image2_alt']?>" title="<?=$row['image2_alt']?>" />
							<?
							if($isBeafIcon){
							?>
							<span class="colafer">After</span>
							<?
							}
							?>
							<?
							}else{
							?>
							<? if($useNoImg){?>
							<img src="/img/no_image.png" alt="noImage" title="noImage" />
							<? } ?>
							<?
							}
							?>
						</div>
						<div class="beafphotocc">
							<p class="photoday">
								<span class="btn_wrap">
									<? if ($row['top'] == "1") { ?>
										<span class="noti_icon"><?=getMsg('lable.td.notice') ?></span>
									<?	}  ?>									
								</span>
								<span class="fontn"><?=$firstTitle?> </span>
							</p>
							<a <?=$readUrl?>>
							<dl>
								<dt>
									<?=utf8_strcut(strip_tags($row['title']),40,'...')?>
									<? if ($isComment) { ?>
										<span class="reNum">[<strong><?= $row['comment_count']?></strong>]</span>
									<? } ?>
								</dt>
								<dd><?=utf8_strcut(strip_tags($row['contents']),50,'...')?></dd>
							</dl>
							<p class="under">
								<span>
								<?
								if($timeDate){
								?>
								<?= getDateTimeFormat($row['registdate']) ?>
								<?
								}else{
								?>
								<?= getYMD($row['registdate']) ?>
								<?
								}
								?>
								</span><i></i>
								<span><img src="/manage/img/ucount_icon.png" /><?= $row['readno'] ?></span>
							</p>
						</div>
					</a>
				</li>
				<?
					}
				}else{ 
				?>
				<?
				$_REQUEST['nodata'] = "해당되는 데이터가 없습니다.";
				include $_SERVER['DOCUMENT_ROOT']."/nodata/index.php" 
				?>
				<?
				}
				
				if($_REQUEST['view_loc_type'] == 0){
				?>
			</ul>
		</div>
		<?
		if($rowPageCount[0] > $pageRows){
		?>
		<div class="mo_programPage"><a href="javascript:;" onclick="more()"><?=getMsg('btn.more') ?><span>+</span></a></div>
		<?
		}
		?>
		
		<?=pageListUser($objCommon->reqPageNo, $rowPageCount[1], $objCommon->getQueryString('index.php', 0, $_REQUEST))?><!-- 페이징 처리 -->
		
		<form name="searchForm" id="searchForm" action="index.php" method="get">
		<div class="program_search">
			<? 
				if ($branch) {
					$hospitalObj = new Hospital(999, $_REQUEST);
					$hResult = $hospitalObj->branchSelect();
			?>
			<select name="shospital_fk" title="<?=getMsg('alt.select.shospital_fk') ?>" onchange="goSearch();">
				<option value="-1" <?=getSelected(-1, $_REQUEST['shospital_fk'])?>><?=getMsg('lable.option.branch') ?></option>
				<? while ($row=mysql_fetch_assoc($hResult)) { ?>
				<option value="<?=$row['no']?>" <?=getSelected($row['no'], $_REQUEST['shospital_fk'])?>><?=$row['name']?></option>
				<? } ?>
			</select>
			<? } ?>
			<? 
				if ($clinic) {
					$clinicObj = new Clinic(999, $_REQUEST);
					$clinicList = $clinicObj->selectBoxList(0,($branch ? $_REQUEST['shospital_fk'] : DEFAULT_BRANCH_NO),$_REQUEST['sclinic_fk']);
			?>
			<select name="sclinic_fk" title="<?=getMsg('alt.select.sclinic_fk') ?>" onchange="goSearch();">
				<?=$clinicList?>
			</select>
			<? } ?>
			<select name="stype" title="<?=getMsg('alt.select.stype') ?>">
				<option value="all" <?=getSelected($_REQUEST[stype], "all") ?>><?=getMsg('lable.option.all') ?></option>
				<option value="title" <?=getSelected($_REQUEST[stype], "title") ?>><?=getMsg('th.title') ?></option>
				<option value="name" <?=getSelected($_REQUEST[stype], "name") ?>><?=getMsg('th.name')?></option>
				<option value="contents" <?=getSelected($_REQUEST[stype], "contents") ?>><?=getMsg('th.contents') ?></option>
			</select>
			<span>
				<input type="text" name="sval" value="<?=$_REQUEST[sval]?>" title="<?=getMsg('alt.text.search') ?>" />
				<a href="javascript:;" onclick="$('#searchForm').submit()" ><?=getMsg('btn.search') ?></a>
			</span>
		</div>
		</form>
	</div>
	 <!-- 프로그램끝 -->
    <? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
    <? include "sub_footer/sub_footer.php" ?>
</div>
</body>
</html>
<?
}
?>