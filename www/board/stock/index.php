<?session_start();
include "config.php";
if ($boardgrade == 3)  { // 게시판 접근 권한 처리
?>

<?
}
	$geturl = "http://asp1.krx.co.kr/servlet/krx.asp.XMLSise?code=".$stockNumber;
	$gettime="";
	$janggubun = "";
	$DungRakrate_str = "";
	
	$timeconclude_length = 0;
	$dailystock_length = 0;
	$Askprice_length= 0;
	
	$CurJuka = 0;
	$Debi = 0;
	$StandardPrice = 0;
	$DungRakrate = 0;
	
	$up = "▲";
	$down = "▼";
	$bohab = "─";
	$line="";
	$xml = "";
	
	$Stockinfo_arr= array(array());
	$Timeconclude_arr = array(array());
	$Dailystock_arr = array(array());
	$Askprice_arr = array(array());
	$Hoga_arr= array();
	

	try{
		$stockXmlData = file_get_contents($geturl);

		$sti = simplexml_load_string(trim($stockXmlData));

		/*주가정보*/
		
		$stockInfo = $sti->stockInfo;
		
		$gettime = $stockInfo["myNowTime"];
		$janggubun = $stockinfo["myJangGubun"];
		
		$TBL_StockInfo = $sti->TBL_StockInfo;
		
		$Stockinfo_arr[0] = $TBL_StockInfo["JongName"];		//종목명 
		$Stockinfo_arr[1] = $TBL_StockInfo["CurJuka"];		//현재가 
		$Stockinfo_arr[2] = $TBL_StockInfo["DungRak"];		//전일대비코드
		$Stockinfo_arr[3] = $TBL_StockInfo["Debi"];			//전일대비
		$Stockinfo_arr[4] = $TBL_StockInfo["PrevJuka"];		//전일종가 
		$Stockinfo_arr[5] = $TBL_StockInfo["Volume"];			//거래량
		$Stockinfo_arr[6] = $TBL_StockInfo["Money"];			//거래대금  
		$Stockinfo_arr[7] = $TBL_StockInfo["StartJuka"];		//시가 
		$Stockinfo_arr[8] = $TBL_StockInfo["HighJuka"];		//고가
		$Stockinfo_arr[9] = $TBL_StockInfo["LowJuka"];		//저가 		
		$Stockinfo_arr[10] = $TBL_StockInfo["High52"];		//52주 최고 
		$Stockinfo_arr[11] = $TBL_StockInfo["Low52"];			//52주 최저  
		$Stockinfo_arr[12] = $TBL_StockInfo["UpJuka"];		//상한가 
		$Stockinfo_arr[13] = $TBL_StockInfo["DownJuka"];		//하한가 
		$Stockinfo_arr[14] = $TBL_StockInfo["Per"];			//PER            
		$Stockinfo_arr[15] = $TBL_StockInfo["Amount"];		//상장주식수    
		$Stockinfo_arr[16] = $TBL_StockInfo["FaceJuka"];		//액면가
		
		// 등락율 계산
		$CurJuka = (int)str_replace(",", "", $Stockinfo_arr[1]);
		$Debi = (int)str_replace(",", "", $Stockinfo_arr[3]);
		
		/*등락구분코드*/
		// 1 - 상한, 2 - 상승, 3 - 보합, 4 - 하한, 5 - 하락
		
		if($Stockinfo_arr[2] == "1"||$Stockinfo_arr[2] == "2"||$Stockinfo_arr[2] == "3"){
			$StandardPrice = $CurJuka - $Debi;
		}
		else if($Stockinfo_arr[2] == "4"||$Stockinfo_arr[2] == "5"){
			$StandardPrice = $CurJuka + $Debi;
		}
		
		// 등락률 = (당일종가 - 기준가) / 기준가 * 100
		// 기준가 = 당일종가(현재가) - 전일대비
		$DungRakrate = (($CurJuka - $StandardPrice) / $StandardPrice) * 100;
		$DungRakrate_str = floor($DungRakrate*100) / 100;
		
		/*일자별시세*/
		
		$DailyStock = $sti->TBL_DailyStock->DailyStock;
		
 		$dailystock_length = count($DailyStock);
		
		for($i=0; $i<$dailystock_length; $i++){
			
			$Dailystock_arr[$i][0] = $DailyStock[$i]["day_Date"];		//일자
			$Dailystock_arr[$i][1] = $DailyStock[$i]["day_EndPrice"];	//종가
			$Dailystock_arr[$i][2] = $DailyStock[$i]["day_getDebi"];	//전일대비
			$Dailystock_arr[$i][3] = $DailyStock[$i]["day_Start"];		//시가
			$Dailystock_arr[$i][4] = $DailyStock[$i]["day_High"];		//고가
			$Dailystock_arr[$i][5] = $DailyStock[$i]["day_Low"];		//저가
			$Dailystock_arr[$i][6] = $DailyStock[$i]["day_Volume"];	//거래량
			$Dailystock_arr[$i][7] = $DailyStock[$i]["day_getAmount"];	//거래대금
			$Dailystock_arr[$i][8] = $DailyStock[$i]["day_Dungrak"];	//전일대비코드
			
		}
		
		/*시간대별 체결가*/
		
		$TBL_TimeConclude = $sti->TBL_TimeConclude->TBL_TimeConclude;
		
		$timeconclude_length = count($TBL_TimeConclude);
		for($i=0; $i<$timeconclude_length; $i++){
			
			$Timeconclude_arr[$i][0] = $TBL_TimeConclude[$i]["time"];		//시간
			$Timeconclude_arr[$i][1] = $TBL_TimeConclude[$i]["negoprice"];	//체결가
			$Timeconclude_arr[$i][2] = $TBL_TimeConclude[$i]["Debi"];		//전일대비
			$Timeconclude_arr[$i][3] = $TBL_TimeConclude[$i]["sellprice"];	//매도호가
			$Timeconclude_arr[$i][4] = $TBL_TimeConclude[$i]["buyprice"];	//매수호가
			$Timeconclude_arr[$i][5] = $TBL_TimeConclude[$i]["amount"];	//체결량
			$Timeconclude_arr[$i][6] = $TBL_TimeConclude[$i]["Dungrak"];	//전일대비코드

		}
		
		/*증권사별거래*/
		
		$AskPrice = $sti->TBL_AskPrice->AskPrice;
		
		$Askprice_length = count($AskPrice);
		for($i=0; $i<$Askprice_length; $i++){
			
			$Askprice_arr[$i][0] = $AskPrice[$i]["member_memdoMem"];	//매도증권사
			$Askprice_arr[$i][1] = $AskPrice[$i]["member_memdoVol"];	//매도거래량
			$Askprice_arr[$i][2] = $AskPrice[$i]["member_memsoMem"];	//매수증권사
			$Askprice_arr[$i][3] = $AskPrice[$i]["member_mesuoVol"];	//매수거래량

		}
		
		/*호가*/
		
		$TBL_Hoga = $sti->TBL_Hoga;
		
		$Hoga_arr[0] = $TBL_Hoga[0]["mesuJan0"];		//매수잔량
		$Hoga_arr[1] = $TBL_Hoga[0]["mesuHoka0"];	//매수호가
		$Hoga_arr[2] = $TBL_Hoga[0]["mesuJan1"];		//매수잔량
		$Hoga_arr[3] = $TBL_Hoga[0]["mesuHoka1"];	//매수호가
		$Hoga_arr[4] = $TBL_Hoga[0]["mesuJan2"];		//매수잔량
		$Hoga_arr[5] = $TBL_Hoga[0]["mesuHoka2"];	//매수호가
		$Hoga_arr[6] = $TBL_Hoga[0]["mesuJan3"];		//매수잔량
		$Hoga_arr[7] = $TBL_Hoga[0]["mesuHoka3"];	//매수호가
		$Hoga_arr[8] = $TBL_Hoga[0]["mesuJan4"];		//매수잔량
		$Hoga_arr[9] = $TBL_Hoga[0]["mesuHoka4"];	//매수호가
		$Hoga_arr[10] = $TBL_Hoga[0]["medoHoka0"];	//매도잔량
		$Hoga_arr[11] = $TBL_Hoga[0]["medoJan0"];	//매도호가
		$Hoga_arr[12] = $TBL_Hoga[0]["medoHoka1"];	//매도잔량
		$Hoga_arr[13] = $TBL_Hoga[0]["medoJan1"];	//매도호가
		$Hoga_arr[14] = $TBL_Hoga[0]["medoHoka2"];	//매도잔량
		$Hoga_arr[15] = $TBL_Hoga[0]["medoJan2"];	//매도호가
		$Hoga_arr[16] = $TBL_Hoga[0]["medoHoka3"];	//매도잔량
		$Hoga_arr[17] = $TBL_Hoga[0]["medoJan3"];	//매도호가
		$Hoga_arr[18] = $TBL_Hoga[0]["medoHoka4"];	//매도잔량
		$Hoga_arr[19] = $TBL_Hoga[0]["medoJan4"];	//매도호가	
		
		$Hoga_arr[20] = number_format( (int)( str_replace(",", "", $Hoga_arr[0]) ) + (int)( str_replace(",", "", $Hoga_arr[2]) ) + (int)( str_replace(",", "", $Hoga_arr[4]) ) + (int)( str_replace(",", "", $Hoga_arr[6]) ) + (int)str_replace(",", "", $Hoga_arr[8]) );
		$Hoga_arr[21] = number_format( (int)( str_replace(",", "", $Hoga_arr[11]) ) + (int)( str_replace(",", "", $Hoga_arr[13]) ) + (int)( str_replace(",", "", $Hoga_arr[15]) ) + (int)( str_replace(",", "", $Hoga_arr[17]) ) + (int)str_replace(",", "", $Hoga_arr[19]) );
	
	}
	catch(Exception $e){
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<? include $_SERVER['DOCUMENT_ROOT']."/include/headHtml.php" ?>
<link href="/vendors/bower_components/morris.js/morris.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
$(window).load(function(){
	$(".loading").delay(500).fadeOut("slow");
});
$(document).ready(function() {	
	var now = new Date();
	if(now.getHours() >= 9) {
		setTimeout(function() {
			callData();
		},1000);
	} else {
		nodata();
	}
});
function callData() {
	$.ajax({
		url:'httputil.php',
		type:'POST',
		dataType:'json',
		success:function(data) {
			arraySet3(data, '<?=str_replace(",", "", $Stockinfo_arr[8])?>', '<?=str_replace(",", "", $Stockinfo_arr[9])?>');
		},
		error:function(err){
			console.log(err);
			alert("해당되는 데이터가 없습니다.");
		}
	});
}

function nodata() {
	$("#morris_line_chart").remove();
	$("#morris_bar_chart").remove();
	var html = "<div class=\"programCon\"><div><ul><li class=\"nodata\"><img src=\"/img/sub_list_none_img.png\" alt=\"느낌표 이미지\"><p>해당되는 데이터가 없습니다.</p></li></ul></div></div>";
	$("#nodata").html(html);
}
</script>
</head>
<body>
<div id="wrap">
	<? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
	<? include "sub_visual/sub_visual.php" ?>		
	<!-- 프로그램시작 -->
	<div class="programCon" data-aos="fade-down" data-aos-delay="400">
		<div class="program_table <?=$listImgClass?>">
		<div class="stock01">
			<div class="loading_wrap">
				<div class="loading">
					Loading
					<span></span>
				</div>
				<div id="morris_line_chart" class="morris-chart" style="height:300px;"></div> <!-- 그래프 -->
				<div id="morris_bar_chart" class="morris-chart" style="height:300px;"></div> <!-- 그래프 -->
			</div>
			<div class="stock01_table">
				<table class="pc_tbl">
					<colgroup>
						<col width="20%" />
						<col width="30%" />
						<col width="20%" />
						<col width="30%" />
					</colgroup>
					<tbody>
						<? if ( $Stockinfo_arr[0] != "" ) { ?>
						<tr>
							<td class="left bold">번호</td>
							<td class="left"><?=$stockNumber?></td>
							<td class="bold">현재가</td>
							<td><?=$Stockinfo_arr[1]?></td>
						</tr>
						<tr>
							<td class="left bold">전일대비</td>
							<td class="left">
								<?
									if ( $Stockinfo_arr[2] == "1"||$Stockinfo_arr[2] == "2" ) {
										echo ("<img src=\"/img/stock_arrow.png\">");
									}
									else if ( $Stockinfo_arr[2] == "3" ) {
										echo ($bohab);
									}
									else if ( $Stockinfo_arr[2] == "4" ||$Stockinfo_arr[2] == "5" ) {
										echo ("<img src=\"/img/stock_arrow_down.png\">");
									}
								?>
								<?=$Stockinfo_arr[3]?>(<?=$DungRakrate_str?>%)
							</td>
							<td colspan="2"></td>
						</tr>
						<tr>
							<td class="left bold">거래량</td>
							<td class="left"><?=$Stockinfo_arr[5]?></td>
							<td class="bold">현재가</td>
							<td><?=$Stockinfo_arr[1]?></td>
						</tr>
						<tr>
							<td class="left bold">시가</td>
							<td class="left"><?=$Stockinfo_arr[7]?></td>
							<td colspan="2"></td>
						</tr>
						<tr>
							<td class="left bold">고가</td>
							<td class="left"><?=$Stockinfo_arr[8]?></td>
							<td class="bold">저가</td>
							<td><?=$Stockinfo_arr[9]?></td>
						</tr>
						<tr>
							<td class="left bold">52주최고</td>
							<td class="left"><?=$Stockinfo_arr[10]?></td>
							<td class="bold">52주최저</td>
							<td><?=$Stockinfo_arr[11]?></td>
						</tr>
						<tr>
							<td class="left bold">상한가</td>
							<td class="left"><?=$Stockinfo_arr[12]?></td>
							<td class="bold">하한가</td>
							<td><?=$Stockinfo_arr[13]?></td>
						</tr>
						<tr>
							<td class="left bold">PER</td>
							<td class="left"><?=$Stockinfo_arr[14]?></td>
							<td colspan="2"></td>
						</tr>
						<tr>
							<td class="left bold">상장주식수</td>
							<td class="left"><?=$Stockinfo_arr[15]?></td>
							<td class="bold">액면가</td>
							<td><?=$Stockinfo_arr[16]?></td>
						</tr>
						<? } else { ?>
						<tr>
							<td class="left bold">번호</td>
							<td class="left"></td>
							<td class="bold">현재가</td>
							<td></td>
						</tr>
						<? } ?>
					</tbody>
				</table>
			</div>
		</div>
		<div class="stock02">
			<div class="stock02_tab">
				<a href="javascript:;" class="on"><span>호가</span></a>
				<a href="javascript:;"><span>시간대별 체결가</span></a>
				<a href="javascript:;"><span>회원사별 거래</span></a>
				<a href="javascript:;"><span>일자별 시세</span></a>
				<a href="javascript:;"><span>일자별거래</span></a>
			</div>
			<div class="stock02_cont">
				<div class="stock02_box">
					<table>
						<colgroup>
							<col width="33.3%"/>
							<col width="33.3%"/>
							<col width="*%"/>
						</colgroup>
						<tbody>
							<tr>
								<th>매도잔량</th>
								<th>호가</th>
								<th>매수잔량</th>
							</tr>
							<?if($Hoga_arr[0] != ""){?>
							<tr>
								<td><?=$Hoga_arr[0]?></td>
								<td><?=$Hoga_arr[1]?></td>
								<td>-</td>
							</tr>
							<tr>
								<td><?=$Hoga_arr[2]?></td>
								<td><?=$Hoga_arr[3]?></td>
								<td>-</td>
							</tr>
							<tr>
								<td><?=$Hoga_arr[4]?></td>
								<td><?=$Hoga_arr[5]?></td>
								<td>-</td>
							</tr>
							<tr>
								<td><?=$Hoga_arr[6]?></td>
								<td><?=$Hoga_arr[7]?></td>
								<td>-</td>
							</tr>
							<tr>
								<td><?=$Hoga_arr[8]?></td>
								<td><?=$Hoga_arr[9]?></td>
								<td>-</td>
							</tr>
							<tr>
								<td>-</td>
								<td><?=$Hoga_arr[10]?></td>
								<td><?=$Hoga_arr[11]?></td>
							</tr>
							<tr>
								<td>-</td>
								<td><?=$Hoga_arr[12]?></td>
								<td><?=$Hoga_arr[13]?></td>
							</tr>
							<tr>
								<td>-</td>
								<td><?=$Hoga_arr[14]?></td>
								<td><?=$Hoga_arr[15]?></td>
							</tr>
							<tr>
								<td>-</td>
								<td><?=$Hoga_arr[16]?></td>
								<td><?=$Hoga_arr[17]?></td>
							</tr>
							<tr>
								<td>-</td>
								<td><?=$Hoga_arr[18]?></td>
								<td><?=$Hoga_arr[19]?></td>
							</tr>
							<tr>
								<td><?=$Hoga_arr[20]?></td>
								<td>잔량합계</td>
								<td><?=$Hoga_arr[21]?></td>
							</tr>
							<?} else {?>
							<tr>
								<td colspan="3">데이터가 없습니다.</td>
							</tr>
							<?} ?>
						</tbody>
					</table>
				</div>
				<div class="stock02_box">
					<table class="taC">
						<tbody>
							<tr>
								<th class="taC">시간</th>
								<th class="taC">체결가</th>
								<th class="taC">전일대비</th>
								<th class="taC">매도호가</th>
								<th class="taC">매수호가</th>
								<th class="taC">체결량</th>
							</tr>
							<?if($timeconclude_length > 0){ ?>
								<?for($i=0;$i<$timeconclude_length;$i++){ ?>
							<tr>
								<td><?=$Timeconclude_arr[$i][0]?></td>
								<td><?=$Timeconclude_arr[$i][1]?></td>
								<?
									if ( $Timeconclude_arr[$i][6] == "1" || $Timeconclude_arr[$i][6] == "2" ) {
										echo ("<td class=\"red\">");
										echo ($up);
									}
									else if ( $Timeconclude_arr[$i][6] == "3" ) {
										echo ("<td>");
										echo ($bohab);
									}
									else if ( $Timeconclude_arr[$i][6] == "4" || $Timeconclude_arr[$i][6] == "5" ) {
										echo ("<td class=\"blue\">");
										echo ($down);
									}
									else {
										echo ("<td>");
									}
									echo ($Timeconclude_arr[$i][2]);
								?>
								</td>
								<td><?=$Timeconclude_arr[$i][3]?></td>
								<td><?=$Timeconclude_arr[$i][4]?></td>
								<td><?=$Timeconclude_arr[$i][5]?></td>
							</tr>
								<?} ?>
							<?} else {?>
							<tr>
								<td colspan="6">데이터가 없습니다.</td>
							</tr>
							<?} ?>
						</tbody>
					</table>
				</div>
				<div class="stock02_box">
					<table class="taC">
						<tbody>
							<tr>
								<th class="taC" colspan="2">매도상위</th>
								<th class="taC" colspan="2">매수상위</th>
							</tr>
							<tr>
								<th class="taC">증권사</th>
								<th class="taC">거래량</th>
								<th class="taC">증권사</th>
								<th class="taC">거래량</th>
							</tr>
							<?if($Askprice_length > 0){ ?>
								<?for($i=0;$i<$Askprice_length;$i++){ ?>
							<tr>
								<td><?=$Askprice_arr[$i][0]?></td>
								<td><?=$Askprice_arr[$i][1]?></td>
								<td><?=$Askprice_arr[$i][2]?></td>
								<td><?=$Askprice_arr[$i][3]?></td>
							</tr>
								<?} ?>
							<?} else {?>
							<tr>
								<td colspan="4">데이터가 없습니다.</td>
							</tr>
							<?} ?>
						</tbody>
					</table>
				</div>
				<div class="stock02_box">
					<table class="taC">
						<tbody>
							<tr>
								<th class="taC">일자</th>
								<th class="taC">종가</th>
								<th class="taC">전일대비</th>
								<th class="taC">시가</th>
								<th class="taC">고가</th>
								<th class="taC">저가</th>
							</tr>
							<?if($dailystock_length > 0){ ?>
								<?for($j=0;$j<$dailystock_length;$j++){ ?>
							<tr>
								<td><?=$Dailystock_arr[$j][0]?></td>
								<td><?=$Dailystock_arr[$j][1]?></td>
								<?
									if ( $Dailystock_arr[$j][8] == "1" || $Dailystock_arr[$j][8] == "2" ) {
										echo ("<td class=\"red\">");
										echo ($up);
									}
									else if ( $Dailystock_arr[$j][8] == "3" ) {
										echo ("<td>");
										echo ($bohab);
									}
									else if ( $Dailystock_arr[$j][8] == "4" || $Dailystock_arr[$j][8] == "5" ) {
										echo ("<td class=\"blue\">");
										echo ($down);
									}
									else {
										echo ("<td>");
									}
									echo ($Dailystock_arr[$j][2]);
								?>
								</td>
								<td><?=$Dailystock_arr[$j][3]?></td>
								<td><?=$Dailystock_arr[$j][4]?></td>
								<td><?=$Dailystock_arr[$j][5]?></td>
							</tr>
								<?} ?>
							<?} else {?>
							<tr>
								<td colspan="6">데이터가 없습니다.</td>
							</tr>
							<?} ?>
						</tbody>
					</table>
				</div>
				<div class="stock02_box">
					<table class="taC">
						<tbody>
							<tr>
								<th class="taC">일자</th>
								<th class="taC">종가</th>
								<th class="taC">전일대비</th>
								<th class="taC">거래량</th>
								<th class="taC">거래대금</th>
							</tr>
							<?if($dailystock_length > 0){ ?>
								<?for($j=0;$j<$dailystock_length;$j++){ ?>
							<tr>
								<td><?=$Dailystock_arr[$j][0]?></td>
								<td><?=$Dailystock_arr[$j][1]?></td>
								<?
									if ( $Dailystock_arr[$j][8] == "1" || $Dailystock_arr[$j][8] == "2" ) {
										echo ("<td class=\"red\">");
										echo ($up);
									}
									else if ( $Dailystock_arr[$j][8] == "3" ) {
										echo ("<td>");
										echo ($bohab);
									}
									else if ( $Dailystock_arr[$j][8] == "4" || $Dailystock_arr[$j][8] == "5" ) {
										echo ("<td class=\"blue\">");
										echo ($down);
									}
									else {
										echo ("<td>");
									}
									echo ($Dailystock_arr[$j][2]);
								?>
								</td>
								<td><?=$Dailystock_arr[$j][6]?></td>
								<td><?=$Dailystock_arr[$j][7]?></td>
							</tr>
								<?} ?>
							<?} else {?>
							<tr>
								<td colspan="5">데이터가 없습니다.</td>
							</tr>
							<?} ?>
						</tbody>
					</table>
				</div>
			</div>
			<script>
				$ = jQuery;
				$(".stock02_cont > div").hide();
				$(".stock02_cont > div").eq(0).show();
		
				$(".stock02_tab a").click(function(){
					var sto_idx = $(this).index();
						
					$(".stock02_tab a").removeClass("on");
					$(this).addClass("on");

					$(".stock02_cont > div").hide();
					$(".stock02_cont > div").eq(sto_idx).show();

				});
			</script>
		</div>
		</div>
	</div>
	<!-- 프로그램끝 -->
	<? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
	<? include "sub_footer/sub_footer.php" ?>
	<script src="/vendors/bower_components/jquery/dist/jquery.min.js"></script>
	<script src="/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="/vendors/bower_components/raphael/raphael.min.js"></script>
	<script src="/vendors/bower_components/morris.js/morris.min.js"></script>
	<script src="/include/dist/js/morris-data.js"></script>
</div>
</body>
</html>
		
