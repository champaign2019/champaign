<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Comment.class.php";

$comment = new Comment(9999, $tablename, $_REQUEST);
$result = $comment->getList($_REQUEST);

?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/boardConfig/config.php" ?>
<script>
	function goSave() {
		
		if($("#name").val()=="" |$("#name").val()=="이름") {
			alert("<?=getMsg('alert.text.name2')?>");
			$("#name").focus();
			return false;
		}
		
		if($("#password").length > 0) {
			if($("#password").val()=="") {
				alert("<?=getMsg('alert.text.password')?>")
				$("#password").focus();
				return false;
			}
		}else{
			if($("#password_temp").val()=="<?=getMsg('th.password')?>") {
				alert("<?=getMsg('alert.text.password')?>")
				$("#password_temp").focus();
				return false;
			}
		}
		
		if($("#contents").val()=="") {
			alert("<?=getMsg('alert.text.contents')?>")
			$("#contents").focus();
			return false;
		}

		var form = $("#frm");

		fn_formSpanCheck(form,key);	
		//스팸 끝

		return true;
	}
	
	function goCommentDelete(obj) {
			var str = "<?=getMsg('confirm.text.delete')?>";
		if($("#password_del").val()=="") {
			alert("<?=getMsg('alert.text.password')?>")
			$("#password_del").focus();
			return false;
		}
		if(confirm(str)) {
			$("#delete_frm").append("<input type='hidden' name='password' value='"+$("#password_del").val()+"'/>");
			$("#delete_frm").submit();
		}else{
			return false;
		}
	}
	
	/*function openDiv(obj) {
		$("#no").val($(obj).attr("id"));
		var x = obj.offset().top-($("#bread").offset().top+$("#password_box").height()+50);
		var y = obj.offset().left-($("#bread").offset().left+$("#password_box").width()+30);
		$("#password_box").css({"top":x, "left":y});
		$("#password_box").show();
	}*/

	function openDiv(obj) {
		$("#no").val($(obj).attr("id"));
		var x = obj.offset().top - 5;
		var y = obj.offset().left - ( $("#password_box").width() + 20) ;
		$("#password_box").css({"top":x, "left":y});
		$("#password_box").show();
	}


	function closeDiv() {
		$("#password_box").hide();
		return false;
	}
	
	function openDiv2(obj) {
		$("#e_no").val($(obj).attr("id").replace("e_",""));
		
		var x = obj.offset().top - 5;
		var y = obj.offset().left - ( $("#password_box2").width() + 20) ;
		$("#password_box2").css({"top":x, "left":y});
		$("#password_box2").show();
	}


	function closeDiv2() {
		$("#password_box2").hide();
		return false;
	}
	function editSave(){
		$("#e_name").val($("#e_name2").val());
		$("#e_contents").val($("#e_contents2").val());
		
		var form = $("#edit_frm");
		
		fn_formSpanCheck(form,key);	
		
		form.submit();
		
	}
	function checkPassword(){
		$("#e_password").val($("#edit_password").val());
		
		$.post("/board/comment/password_check_ajax.php", $( "#edit_frm" ).serialize(), function(data){
			var result = data.trim();
			var e_no = $("#e_no").val();
			//alert(result);
			if(result !== "0"){
				$("#password_box2").hide();
				$(".r_delete2").hide();
				$("#e_"+e_no).show();
				$("#e_"+e_no).attr("value","<?=getMsg('btn.save')?>");
				$("#e_"+e_no).attr("class","");
				$("#e_"+e_no).attr("onclick","editSave();");
				
				$("#name_target_"+e_no).html("<input type='text'class='custom_text' name='e_name' id='e_name2' value='"+$("#name_target_"+e_no).html().trim()+"' />");
				$("#contents_target_"+e_no).html("<textarea class='custom_area' name='e_contents' id='e_contents2' title='내용을 입력해주세요'>"+$("#contents_target_"+e_no).html().trim()+"</textarea>");
				
				
				//alert($("#e_"+e_no).parent().html());
			} else {
				alert("<?=getMsg('alert.text.confirm_password')?>");
			}
			
		});
	}
$(window).load(function(){
	
	$(".r_delete").click(function(){
		openDiv($(this));	
	});
	
	$(".r_delete").focus(function(){
		openDiv($(this));	
	});
	
	$(".r_delete2").click(function(){
		openDiv2($(this));	
	});
	
	$(".r_delete2").focus(function(){
		openDiv2($(this));	
	});
	
	$(".del_submit").click(function(){
		goCommentDelete($(this));	
	});
	$(".del_submit2").click(function(){
		checkPassword();	
	});
	$(".del_submit").focus(function(){
		//goCommentDelete($(this));	
	});
	
	$(".focus_zone").focus(function(){
		focusTextRemove($(this));
	});
	
	$(".focus_zone").blur(function(){
		blurTextInsert($(this));
	});
		
});
</script>
<div class="read_reple">
		<form name="edit_frm" id="edit_frm" action="/board/comment/process.php" method="post">
			<input type="hidden" name="no" 		 id="e_no"/>
			<input type="hidden" name="cmd" 	value="edit"/>
			<input type="hidden" name="contents" id="e_contents"/>
			<input type="hidden" name="name" id="e_name"/>
			<input type="hidden" name="password" id="e_password"/>
			<input type="hidden" name="parent_fk" value="<?=$comment->parent_fk?>"/>
			<input type="hidden" name="url"	value="<?=$_SERVER['REQUEST_URI']?>"/>
		</form>
		<form name="delete_frm" id="delete_frm" action="<?=getSslCheckUrl($_SERVER['SERVER_NAME'], '/board/comment/process.php')?>" method="post" >
		<? if (mysql_num_rows($result) == 0) { ?>
		<dl>
			<dd class="bbsno">
				<?=getMsg('message.tbody.reple')?>
			</dd>
		</dl>
		<? } else { ?>
		<? while ($co_row = mysql_fetch_assoc($result)) { ?>
		<dl class="reple_dl">
			<dt><strong id="name_target_<?=$co_row[no]?>"><?=$co_row['name']?></strong> <?=$co_row['registdate']?></dt>
			<dd><p id="contents_target_<?=$co_row[no]?>"><?=$co_row['contents']?></p><span class="reEdit">
			<strong>
				<input type="button" class="r_delete2" id="e_<?=$co_row[no]?>" value="<?=getMsg('btn.edit')?>"/>
				<input type="button" class="r_delete" id="<?=$co_row[no]?>" value="<?=getMsg('btn.delete')?>"/>
			</strong>
			</span></dd>
		</dl>
		<? }
		}
		?>
		<!-- //box --> 
		<input type="hidden" name="cmd" value="r_delete"/>
		<input type="hidden" name="no" id="no"  value=""/>
		<input type="hidden" name="parent_fk" value="<?=$comment->parent_fk?>"/>
		<input type="hidden" name="member_fk"  value="<?=$_SESSION[member_no]?>"/>
		<input type="hidden" name="tablename"  value="<?=$tablename?>"/>
		<input type="hidden" name="url"	value="<?=$_SERVER['REQUEST_URI']?>"/>
		<input type="submit" style="display:none;" />
	</form>

		
	<div class="read_rego">
		<form name="frm" id="frm" action="/board/comment/process.php" method="post" onsubmit="return goSave();">
			<dl>
				<dt>
					<div><input type="text" class="focus_zone" name="name" id="name" title="작성자 이름을 입력해주세요" value="<?=!$_SESSION['member_name'] ? "이름" : $_SESSION['member_name']?>"/></div>
					<div><input type="text" class="focus_zone" name="password_temp" id="password_temp" value="비밀번호" title="비밀번호를 입력해주세요" /></div>
				</dt>
				<dd>
					<textarea class="focus_zone" name="contents" id="contents" title="내용을 입력해주세요"></textarea>
					<input value="<?=getMsg('th.confirm')?>" type="submit" />
				</dd>
			</dl>
			<input type="hidden" name="cmd" value="r_write"/>
			<input type="hidden" name="parent_fk" value="<?=$comment->parent_fk?>"/>
			<input type="hidden" name="member_fk"  value="<?=$_SESSION[member_no]?>"/>
			<input type="hidden" name="tablename"  value="<?=$tablename?>"/>
			<input type="hidden" name="url" value="<?=$_SERVER["REQUEST_URI"]?>"/>
		</form>
	</div>
	<!-- //rego -->
</div>

<div id="password_box" class="password_box">
	<div>
		<dl>
			<dt><?=getMsg('th.password')?></dt>
			<dd>
				<input type="password" name="password_del" id="password_del" size="8" value="" title="비밀번호 확인" onkeyup="javascript:if(event.keyCode == 13){goCommentDelete($(this))}"/>
				<a><input type="button" class="del_submit" value="<?=getMsg('btn.delete')?>" /></a>
				<input type="button" class="password_box_x" onclick="closeDiv();" onfocus="closeDiv()" value="X" />
			</dd>
			
		</dl>
	</div>
	<!-- //in_box -->
</div>
<div id="password_box2" class="password_box">
	<div>
		<dl>
			<dt><?=getMsg('th.password')?></dt>
			<dd>
				<input type="password" name="edit_password" id="edit_password" size="8" value="" title="비밀번호 확인" onkeyup="javascript:if(event.keyCode == 13){checkPassword($(this))}"/>
				<a><input type="button" class="del_submit2" value="<?=getMsg('btn.edit')?>" /></a>
				<input type="button" class="password_box_x" onclick="closeDiv2();" onfocus="closeDiv2()" value="X" />
			</dd>
			
		</dl>
	</div>
	<!-- //in_box -->
</div>