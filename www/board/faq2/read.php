<?session_start();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<?
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Consult.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Common.class.php";

include "config.php";

if ($boardgrade == 3 || $boardgrade == 2) { // 게시판접근 권한처리
	include $_SERVER['DOCUMENT_ROOT']."/include/gradeCheck.php";
}

	$objCommon = new Common($pageRows, $tablename, $_REQUEST);
	$_REQUEST['category_tablename'] = $category_tablename;
	$_REQUEST['orderby'] = $orderby; //정렬 배열 선언
	$data = $objCommon->getData($_REQUEST, $userCon);

?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/include/headHtml.php" ?>
<script type="text/javascript">
	

	
</script>
</head>
<body>

	
<div id="wrap">
    <? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
    <? include "sub_visual/sub_visual.php" ?>
    <!-- 프로그램시작 -->

<div class="programCon">
	<div class="programRead">
		<?
		//(지점 > 진료과목) 체크 로직
		$firstTitle = "";
		if($branch && $data['hospital_name']){
		
			$firstTitle .= "<span class='branch01'>" .$data['hospital_name']. "</span>"; 
		
		}
		if($clinic && $data['clinic_name']){
			if($data['firstTitle'] != "["){
				$firstTitle.="";
			}	
			$firstTitle.= "<span class='branch02'>" .$data['clinic_name']. "</span>";
		} 
		
		$firstTitle.="";
			
		if(!$firstTitle){
			$firstTitle = "";
		}		
		?>
		<div class="readTop">
			<p class="imgTitle"><?=$firstTitle?> <?=$data['title'] ?></p>
			<!--<p class="imgTitle_sub tit"> 작성자</p>-->
			<p class="imgTitle_sub">
				<?=$data['name']?>
			<i></i>
			
			<?
			//날짜만 보여지는지 시간까지 보여지는지에 체크
			if($timeDate){
			?>
			<?= getDateTimeFormat($data['registdate']) ?>
			<?
			}else{
			?>
			<?= getYMD($data['registdate']) ?>
			<?
			}
			?>
			<span class="hit"><img src="/manage/img/ucount_icon.png" /> </span><?=$data['readno']?>
			</p>
			
			
		</div>
		<div class="readEdit">
			

			<? if ($useMovie){ ?>
			<?if ($data['moviename']) {?>
			<p>
				<script type="text/javascript">
				<!--
				tv_adplay_autosize("<?=$uploadPath?><?=$data['moviename']?>", "MoviePlayer");
				//-->
				</script>
			</p>
			
			<? } ?>
		<? } ?>
			
			<?=stripslashes($data['contents'])?>
		</div>
		<? if ( ($useFile || $useRelationurl) && ($data['filename_org'] || $data['relation_url'])) { ?>
		<div class="urlFile">
		<? if ($useFile) { ?>
			<?if ($data['filename_org']) { ?>
				<p><img src="/img/file_img.gif" alt="파일첨부" /> <a href="<?=$uploadPath?>&vf=<?=$data['filename']?>&af=<?=$data['filename_org']?>" target="_blank"><?=$data['filename_org']?> [<?=getFileSize($data['filesize'])?>]</a></p>
			<? } ?>
		<? } ?>
		<? if ($useRelationurl) { ?>
			<? if ($data['relation_url']) { ?>
				<p><img src="/img/url_img.gif" alt="관련URL" /> <a href="<?=$data['relation_url']?>" target="_blank"><?=cvtHttp($data['relation_url'])?></a></p>
			<? } ?>
		<? } ?>
		</div>
		<? } ?>	

		<?if($data['answer']){ ?>
		<div class="readAnswer">
			<p class="answer_tit"><?=getMsg('th.answer') ?></p>
			<div class="answer_txt"><?=$data['answer']?></div>
		</div>
		<? } ?>
		
		<? if ($isComment) { ?>
			
			<? include $_SERVER['DOCUMENT_ROOT']."/board/comment/comment.php" ?><!-- 댓글 -->
	
		<? } ?>
			
		<div class="readBottom">
			<table>
				<colgroup>
					<col width="22%" />
					<col width="*" />
				</colgroup>
				<tr>
					<th><?=getMsg('th.prev')?><img src="/img/read_arrowUp.png" alt="" /></th>
					<td>
					<? if($data['prev_no'] > 0) { ?>
						<a class="" href="<?=$objCommon->getQueryString('read.php', $data['prev_no'], $_REQUEST)?>"><?=$data['prev_title']?></a>
					<? }else{ ?>
						<a href=""><?=getMsg('message.tbody.prev')?></a>
					<?}?>
					
					</td>
				</tr>
				<tr>
					<th><?=getMsg('th.next')?><img src="/img/read_arrowDown.png" alt="" /></th>
					<td>
					<? if ($data['next_no'] > 0) { ?>
						<a class="" href="<?=$objCommon->getQueryString('read.php', $data['next_no'], $_REQUEST)?>"><?=$data['next_title']?></a>
					<? }else{ ?>
						<a href=""><?=getMsg('message.tbody.next')?></a>
					<?}?>
					</td>
				</tr>
			</table>
			
			<dl class="readBottom_btn">
				<dt>
					
				</dt>
				<dd>
				<a class="btns" href="<?=$objCommon->getQueryString('index.php', 0, $_REQUEST)?>"><strong><?=getMsg('btn.list')?></strong></a>
				</dd>
			</dl>
		</div>
	</div>
</div>
 <!-- 프로그램끝 -->
    <? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
    <? include "sub_footer/sub_footer.php" ?>
</div>
</body>
</html>
