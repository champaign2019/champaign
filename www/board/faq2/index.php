<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Common.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Hospital.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Clinic.class.php";

include "config.php";

$_REQUEST['orderby'] = $orderby;

$objCommon = new Common($pageRows, $tablename, $_REQUEST);
$rowPageCount = $objCommon->getCount($_REQUEST);
$result = $objCommon->getList($_REQUEST);

$category_result = $category_tablename ? $objCommon->getCategoryList($_REQUEST) : null;
if($_REQUEST['view_loc_type'] == 0){
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/include/headHtml.php" ?>
<script type="text/javascript">
var rowCount = <?=$rowPageCount[0]?>

$(window).load(function(){
	
	
	if($(".program_table tbody tr").size() == rowCount){
		$(".mo_programPage").hide();	
	}
	$(".program_table td.tit").mouseenter(function(){
		$(this).parent("tr").addClass("on_e");
	});
	$(".program_table td.tit").mouseleave(function(){
		$(this).parent("tr").removeClass("on_e");
	});
	
})

function goSearch() {
	$("#searchForm").submit();
}

function goLogin() {
	if (confirm("로그인 페이지로 이동합니다.")){
		$("#urlMaintain").submit();
	} else {
		document.location.href = "<?=$objCommon->getQueryString('index.php', 0 ,$_REQUEST)?>";
	}
}

function more(){
	
	//다음페이지 계산
	var reqPageNo = (Math.ceil($(".faq_table > tbody > #tit").size()/<?=$pageRows?>)+1);

	$.post("indexJquery.php",
	{
		reqPageNo : reqPageNo,
		stype : $("#stype").val(),
		view_loc_type : 1,
		sval : $("#sval").val()
		
	},function(data){
		
		$(".faq_table tbody").append(data);
		if($(".faq_table > tbody > #tit").size() == rowCount){
			$(".mo_programPage").hide();
		}
		
		
	}).fail(function(error){
		
	})
	
}
</script>
</head>

<body>

	<div id="wrap">
    <? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
    <? include "sub_visual/sub_visual.php" ?>
    <!-- 프로그램시작 -->
	<div class="programCon" data-aos="fade-down" data-aos-delay="400">
		
		<? if(mysql_num_rows($category_result) > 0) {
			$checkClass = (!$_REQUEST['scategory_fk'] ? "active" : "");
		?>
		
		<ul class="faq_tab">
			
				<li class="<?=$checkClass?>"><a href="index.php"><?=getMsg('th.all')?></a></li>
				<? 
					
				while ($row=mysql_fetch_assoc($category_result)) { 
						$checkClass =  ($_REQUEST['scategory_fk'] == $row['no'] ? "active" : "");
				?>
				<li class="<?=$checkClass?>"><a href="index.php?scategory_fk=<?=$row[no]?>"/><?=$row[name]?></a></li>
				<?
					}
					
				?>
		</ul>
		<?
		}
		?>
		
		<table class="faq_table">
			<tbody>
				<?
				}
				$readUrl = "";
				$i = 0;
				if($rowPageCount[0] > 0){
					while ($row=mysql_fetch_assoc($result)) {
						$readUrl = "style='cursor:pointer;' onclick=\"location.href='".$objCommon->getQueryString('read.php', $row['no'], $_REQUEST)."'\"";
							
						//(지점 > 진료과목) 체크 로직
						$firstTitle = "";
						if($branch && $row['hospital_name']){
						
							$firstTitle .= "<span class='branch01'>" .$row['hospital_name']. "</span>"; 
						
						}
						if($clinic && $row['clinic_name']){
							if($row['firstTitle'] != "["){
								$firstTitle.="";
							}	
							$firstTitle.= "<span class='branch02'>" .$row['clinic_name']. "</span>";
						} 
						
						$firstTitle.="";
							
						if(!$firstTitle){
							$firstTitle = "";
						}	
				?>
				<tr id="tit">
					<th onclick="clickFn(this);"><?=$firstTitle?> <?=$row['title']?></th>
				</tr>
				<tr>
					<td>
					<? if ($useMovie){ ?>
						<?if ($row['moviename']) {?>
							<script type="text/javascript">
							tv_adplay_autosize("<?=$uploadPath?><?=$row['moviename']?>", "MoviePlayer");
							</script>
						<? } ?>
					<? } ?>
					<?=$row['contents']?>
					<? if ( ($useFile || $useRelationurl)) { ?>
					<div class="urlFile">
					<?
						$_REQUEST['no'] = $row['no'];
						$data = $objCommon->getData($_REQUEST, false);
						if ($data['filename_org']) {
							$filename = split(",", $data['filename']);
							$filename_org = split(",", $data['filename_org']);
							$filesize = split(",", $data['filesize']);
							for ($i=0;$i<sizeof($filesize);$i++) {
					?>
					<p>
						<img src="/manage/img/file_img.gif" alt="파일첨부" />
						&nbsp;&nbsp;
						<a href="/lib/download.php?path=<?=$uploadPath?>&vf=<?=$filename[$i]?>&af=<?=$filename_org[$i]?>" target="_blank">
							<?=$filename_org[$i]?> [<?=getFileSize($filesize[$i])?>]
						</a>
					</p>
					<?
							}
						}
					?>
					</div>
					<? } ?>	
					</td>
				</tr>
				<?
					}
				}else{ 
				?>
				<tr>
					<th ><?=getMsg('message.tbody.data') ?></th>
				</tr>
				<?
				}
				
				if($_REQUEST['view_loc_type'] == 0){
				?>
				
			</tbody>
		</table>
		<script>
		function clickFn(that){
			if ( $(that).parent().next().attr("class") == "active" ){
				$(".faq_table tr").removeClass("active");
				$(".faq_table td").slideUp(300);
			} else {
				$(".faq_table td").slideUp(300);
				$(".faq_table tr").removeClass("active");
				$(that).parent().next().addClass("active");
				$(that).parent().next().find("td").slideDown(500);
			}
		}
		</script>
		<?
		if($rowPageCount[0] > $pageRows){
		?>
		<div class="mo_programPage"><a href="javascript:;" onclick="more()"><?=getMsg('btn.more') ?><span>+</span></a></div>
		<?
		}
		?>
		
		<?=pageListUser($objCommon->reqPageNo, $rowPageCount[1], $objCommon->getQueryString('index.php', 0, $_REQUEST))?><!-- 페이징 처리 -->
		
		<form name="searchForm" id="searchForm" action="index.php" method="get">
		<div class="program_search">
			<? 
			if($branch) { 
			$hospitalObj = new Hospital(999, $_REQUEST);
			?>
			<select name="shospital_fk" title="<?=getMsg('alt.select.shospital_fk') ?>" onchange="goSearch();">
				<?=$hospitalObj->selectBoxList(0, 0, 0,$_REQUEST['shospital_fk'])?>
			</select>
			<? } ?>
			
			<? 
			if($clinic) { 
			$clinicObj = new Clinic(999, $_REQUEST);
			?>
			<select name="sclinic_fk" title="진료과목을 선택해주세요" onchange="goSearch();">
				<?=$clinicObj->selectBoxList(0, ($branch ? $_REQUEST['shospital_fk'] : DEFAULT_BRANCH_NO), $_REQUEST['sclinic_fk'])?>
			</select>
			<? } ?>
			<select name="stype" title="<?=getMsg('alt.select.stype') ?>">
				<option value="all" <?=getSelected($_REQUEST[stype], "all") ?>><?=getMsg('lable.option.all') ?></option>
				<option value="title" <?=getSelected($_REQUEST[stype], "title") ?>><?=getMsg('th.title') ?></option>
				<option value="name" <?=getSelected($_REQUEST[stype], "name") ?>><?=getMsg('th.name')?></option>
				<option value="contents" <?=getSelected($_REQUEST[stype], "contents") ?>><?=getMsg('th.contents') ?></option>
			</select>
		
			<span>
				<input type="text" name="sval" value="<?=$_REQUEST[sval]?>" title="<?=getMsg('alt.text.search') ?>" />
				<a href="javascript:;" onclick="$('#searchForm').submit()" ><?=getMsg('btn.search') ?></a>
			</span>
		</div>
		</form>
	</div>
	 <!-- 프로그램끝 -->
    <? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
    <? include "sub_footer/sub_footer.php" ?>
</div>
</body>
</html>
</html>
<?
}
?>