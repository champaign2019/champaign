<?
include $_SERVER['DOCUMENT_ROOT']."/include/logingCheck.php";

$pageTitle 	 	= "투자정보";				// 게시판 명
$stockNumber	= "222040";
$tablename  		= "";					// DB 테이블 명
$category_tablename = "";
$uploadPath 		= "/upload//";			// 파일, 동영상 첨부 경로
$maxSaveSize	  		= 50*1024*1024;					// 50Mb
$pageRows	  		= 10;							// 보여질 게시글 수
$boardgrade	  		= 0;							// 사용자 게시판 권한 [0: 미로그인(읽기, 쓰기), 1: 미로그인 - 읽기, 로그인 - 쓰기, 2: 로그인(읽기, 쓰기), 3: 로그인만 접근]
$secretLevel 		= 0;							// 0 : 비회원, 비공개글은 못봄 , 1 비회원 비공개글 패스워드 창으로 이동

/* 화면 필드 생성 여부 */
$unickNum	 	= false;							//  고유번호 타입    
$branch	  		= false;							// 지점 사용유무  
$clinic	  		= false;							// 진료과목 지점별 사용유무	
$doctor	  		= false;							// 의료진 사용유무	

$useCalendar 	= false;							//달력 사용유무

$useMovie  	 	= false;						// 파일 첨부 [사용: true, 사용 안함 : true]
$useRelationurl	= false;						// 관련URL 첨부 [사용: true, 사용 안함 : true]
$useEmail 		= true;							// 이메일 사용유무
$useTel 			= true;							// 연락처 사용유무
$useCell 		= true;							// 핸드폰 사용유무
$useSmsChk 		= true;							// sms 수신 사용유무
$useEmailChk		= true;							// email 수신 사용유무
$usePassword		= true;							// 비밀번호 사용유무
$useTitle 		= true;							//제목 사용유무
$useContents 	= true;							//내용 사용유무
$useBtn 			= true;							// 글쓰기, 수정페이지, 삭제 가능 유무		
$isComment 		= true;							// 댓글		
$isCellSum 		= true;						//연락처 , 핸드폰번호 하나로 합치기



$isListImage		= false;							//상세에 목록이미지 사용유무
$useNoImg		= true;					// noImage 사용 여부
$gallery 		= true; 						//목록이미지
$editImgUse 		= true;							//목록이미지 없으면 에디터이미지 [사용 : true, 사용안함 : false]
		
$listImageType = 1; 						//1:사이즈-비율고정, 2:가로사이즈-비율고정, 3:세로사이즈-비율고정, 4:사이즈고정-비율미고정, 5:가로고정-비율hide
//$listimageSize = 130;								//이미지 사이즈

$timeDate	  	= true;						// 날짜 or 날짜 and 시간	
$userCon	  		= true;							// 조회수 카운트 [관리자 페이지:true, 사용자 페이지:true, 조회수:사용여부]
$useFile  		= false;							// 파일 첨부 [사용: true, 사용 안함 : true]
$useFileCount  	= 10;							// 파일 첨부 가능 갯수
$textAreaEditor	= true;							// 에디터 사용여부
$editorIgnore= "";								//에디터 미사용 배열 구분자 ex : '|0|1|'						

$useSecret	  	= true;							// 비밀글 사용 여부	

$useAnswer		= false;							// 답변필드사용여부
$useWriteAnswer	= true;							// 답변글사용여부
$answer 				= $useWriteAnswer ? 1 : 0;							// 답변글사용여부

$searchPosition1 	= 0; 							//검색 위치 0 : 위 1 아래 -- 아직 기능 미구현
$searchPosition2 	= "center"; 					//검색 위치 center : 가운데 right : 오른쪽  left 왼쪽 

$onlyMember = false; 								//자기글만 보기
$smember_fk = 0;
if($onlyMember){
	
	$smember_fk = $member_no == 0 ? 99999999 : $member_no ;
}
$useFileDrag    = false;
$userAgent = $_SERVER["HTTP_USER_AGENT"];
if($useFileDrag){
    if ( preg_match("/MSIE 6.0[0-9]*/", $userAgent) ) {
        $useFileDrag    = false;
    }else if ( preg_match("/MSIE 7.0*/", $userAgent) ) {
        $useFileDrag    = false;
    }else if ( preg_match("/MSIE 8.0*/", $userAgent) ) {
        $useFileDrag    = false;
    }else if ( preg_match("/MSIE 9.0*/", $userAgent) ) {
        $useFileDrag    = false;
    }else if ( preg_match("/MSIE 10.0*/", $userAgent) ) {
        $useFileDrag    = true;
	}
}

/* 문자메세지/이메일메세지 */
$getUserMsg = "상담요청이 정상적으로 요청되었습니다. 감사합니다.";			//사용자 보내는 메세지 - 빈값이면 문자 안나감
$getAdminMsg = "님의 상담이 요청되었습니다. 확인하시고 답변 부탁 드립니다.";	//관리자 보내는 메세지 - 빈값이면 문자 안나감

$getAnswerUserMsg = "";											//사용자 답변 보내는 메세지 - 빈값이면 문자 안나감
$getAnswerAdminMsg = "";											//관리자 답변 보내는 메세지 - 빈값이면 문자 안나감

$getUserEmail = "상담 요청이 정상적으로 요청되었습니다.";					//사용자 보내는 이메일 - 빈값이면 문자 안나감
$getAnswerUserEmail = "";											//관리자 보내는 이메일 - 빈값이면 문자 안나감

$orderby = Array( "top desc","registdate desc" );					    //order by 배열
if($answer > 0){
	$orderby = Array( "top desc","gno DESC","ono ASC" );					    //order by 배열
}
?>