<?session_start();
include "config.php";
if ($boardgrade == 3)  { // 게시판 접근 권한 처리
?>

<?
}
	/********
	*개별 재무제표
	*********/
	
	$geturl = "http://asp1.krx.co.kr/servlet/krx.asp.XMLJemu?code=".$stockNumber;
	$gettime="";

	$DaeCha_ym = array();
	$DaeCha_data = array(array());
	$SonIk_ym = array();
	$SonIk_data = array(array());
	$CashFlow_ym = array();
	$CashFlow_data = array(array());

	try{
		$stockXmlData = file_get_contents($geturl);

		$sti = simplexml_load_string(trim($stockXmlData));

		/*조회시각*/
		$financialTotal = $sti->financialTotal;
		
		$gettime = $financialTotal[0]["querytime"];
		
		/*대차대조표*/
		
		$TBL_DaeCha = $sti->TBL_DaeCha;
		
		$DaeCha_ym[0] = $TBL_DaeCha[0]["year0"];
		$DaeCha_ym[1] = $TBL_DaeCha[0]["month0"];
		$DaeCha_ym[2] = $TBL_DaeCha[0]["year1"];
		$DaeCha_ym[3] = $TBL_DaeCha[0]["month1"];
		$DaeCha_ym[4] = $TBL_DaeCha[0]["year2"];
		$DaeCha_ym[5] = $TBL_DaeCha[0]["month2"];
		
		$TBL_DaeCha_data = $sti->TBL_DaeCha->TBL_DaeCha_data;

		$daeCha_data_length = count($TBL_DaeCha_data);
		
		for($i=0;$i<$daeCha_data_length;$i++){
			
			$DaeCha_data[$i][0] = $TBL_DaeCha_data[$i]["hangMok".$i];
			$DaeCha_data[$i][1] = $TBL_DaeCha_data[$i]["year1Money".$i];
			$DaeCha_data[$i][2] = $TBL_DaeCha_data[$i]["year1GuSungRate".$i];
			$DaeCha_data[$i][3] = $TBL_DaeCha_data[$i]["year1JungGamRate".$i];
			$DaeCha_data[$i][4] = $TBL_DaeCha_data[$i]["year2Money".$i];
			$DaeCha_data[$i][5] = $TBL_DaeCha_data[$i]["year2GuSungRate".$i];
			$DaeCha_data[$i][6] = $TBL_DaeCha_data[$i]["year2JungGamRate".$i];
			$DaeCha_data[$i][7] = $TBL_DaeCha_data[$i]["year3Money".$i];
			$DaeCha_data[$i][8] = $TBL_DaeCha_data[$i]["year3GuSungRate".$i];
			$DaeCha_data[$i][9] = $TBL_DaeCha_data[$i]["year3JungGamRate".$i];
			
		}
		
		/*손익계산서*/
		
		$TBL_SonIk_ym = $sti->TBL_SonIk;
		
		$SonIk_ym[0] = $TBL_SonIk_ym[0]["year0"];
		$SonIk_ym[1] = $TBL_SonIk_ym[0]["month0"];
		$SonIk_ym[2] = $TBL_SonIk_ym[0]["year1"];
		$SonIk_ym[3] = $TBL_SonIk_ym[0]["month1"];
		$SonIk_ym[4] = $TBL_SonIk_ym[0]["year2"];
		$SonIk_ym[5] = $TBL_SonIk_ym[0]["month2"];

		
		$TBL_SonIk_data = $sti->TBL_SonIk->TBL_SonIk_data;
		
		$sonIk_data_length = count($TBL_SonIk_data);
		
		for($i=0;$i<$sonIk_data_length;$i++){
			
			$SonIk_data[$i][0] = $TBL_SonIk_data[$i]["hangMok".$i];
			$SonIk_data[$i][1] = $TBL_SonIk_data[$i]["year1Money".$i];
			$SonIk_data[$i][2] = $TBL_SonIk_data[$i]["year1GuSungRate".$i];
			$SonIk_data[$i][3] = $TBL_SonIk_data[$i]["year1JungGamRate".$i];
			$SonIk_data[$i][4] = $TBL_SonIk_data[$i]["year2Money".$i];
			$SonIk_data[$i][5] = $TBL_SonIk_data[$i]["year2GuSungRate".$i];
			$SonIk_data[$i][6] = $TBL_SonIk_data[$i]["year2JungGamRate".$i];
			$SonIk_data[$i][7] = $TBL_SonIk_data[$i]["year3Money".$i];
			$SonIk_data[$i][8] = $TBL_SonIk_data[$i]["year3GuSungRate".$i];
			$SonIk_data[$i][9] = $TBL_SonIk_data[$i]["year3JungGamRate".$i];
			
		}
		
		/*현금흐름표*/
		
		$TBL_CashFlow = $sti->TBL_CashFlow;
		
		$CashFlow_ym[0] = $TBL_CashFlow[0]["year0"];
		$CashFlow_ym[1] = $TBL_CashFlow[0]["month0"];
		$CashFlow_ym[2] = $TBL_CashFlow[0]["year1"];
		$CashFlow_ym[3] = $TBL_CashFlow[0]["month1"];
		$CashFlow_ym[4] = $TBL_CashFlow[0]["year2"];
		$CashFlow_ym[5] = $TBL_CashFlow[0]["month2"];
		
		$TBL_CashFlow_data = $sti->TBL_CashFlow->TBL_CashFlow_data;
		
		$cashFlow_data_length = count($TBL_CashFlow_data);
		
		for($i=0;$i<$cashFlow_data_length;$i++){
			
			$CashFlow_data[$i][0] = $TBL_CashFlow_data[$i]["hangMok".$i];
			$CashFlow_data[$i][1] = $TBL_CashFlow_data[$i]["year1Money".$i];
			$CashFlow_data[$i][2] = $TBL_CashFlow_data[$i]["year1JungGamRate".$i];
			$CashFlow_data[$i][3] = $TBL_CashFlow_data[$i]["year2Money".$i];
			$CashFlow_data[$i][4] = $TBL_CashFlow_data[$i]["year2JungGamRate".$i];
			$CashFlow_data[$i][5] = $TBL_CashFlow_data[$i]["year3Money".$i];
			$CashFlow_data[$i][6] = $TBL_CashFlow_data[$i]["year3JungGamRate".$i];
			
		}
	
	}
	catch(Exception $e){
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<? include $_SERVER['DOCUMENT_ROOT']."/include/headHtml.php" ?>
</head>
<body>

	<div id="wrap">
		<? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
		<? include "sub_visual/sub_visual.php" ?>
		<div class="programCon" data-aos="fade-down" data-aos-delay="400">
		<div class="program_table <?=$listImgClass?>">
			<div class="finance_cont">
				<div class="finance_con">
					<div class="finance_box01 fin_bg">
						<div class="inner">
							<div class="tit">
								<p>대차대조표 <span>(단위 : 백만원)</span></p>
							</div>
							<div class="finance_tbl">
								<table>
									<colgroup>
										<col width="25%" />
										<col width="25%" />
										<col width="25%" />
										<col width="25%" />
									</colgroup>
									<tbody>						
										<tr>
											<th rowspan="2">재무항목</th>
											<?if($DaeCha_ym != "") {?>
											<th><?=$DaeCha_ym[0]?>년 <?=$DaeCha_ym[1]?>월</th>
											<th><?=$DaeCha_ym[2]?>년 <?=$DaeCha_ym[3]?>월</th>
											<th><?=$DaeCha_ym[4]?>년 <?=$DaeCha_ym[5]?>월</th>
											<?}else{?>
											<th>YYYY년 MM월</th>
											<th>YYYY년 MM월</th>
											<th>YYYY년 MM월</th>
											<?}?>
										</tr>
										<tr>
											<th>금액</th>
											<th>구성비</th>
											<th>증감율</th>
											<th>금액</th>
											<th>구성비</th>
											<th>증감율</th>
											<th>금액</th>
											<th>구성비</th>
											<th>증감율</th>
										</tr>
										<?if($sonIk_data_length > 0){?>
											<?for($i=0;$i<$sonIk_data_length;$i++){ ?>
										<tr>
											<td><?=$SonIk_data[$i][0]?></td>
											<td><?=$SonIk_data[$i][1]?></td>
											<td><?=$SonIk_data[$i][2]?></td>
											<td><?=$SonIk_data[$i][3]?></td>
											<td><?=$SonIk_data[$i][4]?></td>
											<td><?=$SonIk_data[$i][5]?></td>
											<td><?=$SonIk_data[$i][6]?></td>
											<td><?=$SonIk_data[$i][7]?></td>
											<td><?=$SonIk_data[$i][8]?></td>
											<td><?=$SonIk_data[$i][9]?></td>
										</tr>
											<?}?>
										<?}else{?>
										<tr>
											<td>[재무항목]</td>
											<td>00,000</td>
											<td>00,000</td>
											<td>00,000</td>
											<td>00,000</td>
											<td>00,000</td>
											<td>00,000</td>
											<td>00,000</td>
											<td>00,000</td>
											<td>00,000</td>
											<td>00,000</td>
										</tr>
										<?}?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="finance_box01">
						<div class="inner">
							<div class="tit">
								<p>손익계산서 <span>(단위 : 백만원)</span></p>
							</div>
							<div class="finance_tbl">
								<table>
									<colgroup>
										<col width="25%" />
										<col width="25%" />
										<col width="25%" />
										<col width="25%" />
									</colgroup>
									<tbody>						
										<tr>
											<th rowspan="2">재무항목</th>
											<? if($SonIk_ym[0] != "") {?>
											<th colspan="3"><?=$SonIk_ym[0]?>년 <?=$SonIk_ym[1]?>월</th>
											<th colspan="3"><?=$SonIk_ym[2]?>년 <?=$SonIk_ym[3]?>월</th>
											<th colspan="3"><?=$SonIk_ym[4]?>년 <?=$SonIk_ym[5]?>월</th>
											<? } ?>
										</tr>
										<tr>
											<th>금액</th>
											<th>구성비</th>
											<th>증감율</th>
											<th>금액</th>
											<th>구성비</th>
											<th>증감율</th>
											<th>금액</th>
											<th>구성비</th>
											<th>증감율</th>
										</tr>
										<?if($sonIk_data_length > 0){?>
											<?for($i=0;$i<$sonIk_data_length;$i++){ ?>
										<tr>
											<td><?=$SonIk_data[$i][0]?></td>
											<td><?=$SonIk_data[$i][1]?></td>
											<td><?=$SonIk_data[$i][2]?></td>
											<td><?=$SonIk_data[$i][3]?></td>
											<td><?=$SonIk_data[$i][4]?></td>
											<td><?=$SonIk_data[$i][5]?></td>
											<td><?=$SonIk_data[$i][6]?></td>
											<td><?=$SonIk_data[$i][7]?></td>
											<td><?=$SonIk_data[$i][8]?></td>
											<td><?=$SonIk_data[$i][9]?></td>
										</tr>
											<?}?>
										<?} else {?>
										<tr>
											<td>[재무항목]</td>
											<td>00,000</td>
											<td>00,000</td>
											<td>00,000</td>
											<td>00,000</td>
											<td>00,000</td>
											<td>00,000</td>
											<td>00,000</td>
											<td>00,000</td>
										</tr>
										<?}?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="finance_box01">
						<div class="inner">
							<div class="tit">
								<p>현금흐름표 <span>(단위 : 백만원)</span></p>
							</div>
							<div class="finance_tbl">
								<table>
									<colgroup>
										<col width="25%" />
										<col width="25%" />
										<col width="25%" />
										<col width="25%" />
									</colgroup>
									<tbody>						
										<tr>
											<th rowspan="2">재무항목</th>
											<? if($CashFlow_ym[0] != "") {?>
											<th colspan="3"><?=$CashFlow_ym[0]?>년 <?=$CashFlow_ym[1]?>월</th>
											<th colspan="3"><?=$CashFlow_ym[2]?>년 <?=$CashFlow_ym[3]?>월</th>
											<th colspan="3"><?=$CashFlow_ym[4]?>년 <?=$CashFlow_ym[5]?>월</th>
											<?}?>
										</tr>
										<tr>
											<th>금액</th>
											<th>증감액</th>
											<th>금액</th>
											<th>증감액</th>
											<th>금액</th>
											<th>증감액</th>
										</tr>
										<?if($cashFlow_data_length > 0){?>
											<?for($i=0;$i<$cashFlow_data_length;$i++){ ?>
										<tr>
											<td><?=$CashFlow_data[$i][0]?></td>
											<td><?=$CashFlow_data[$i][1]?></td>
											<td><?=$CashFlow_data[$i][2]?></td>
											<td><?=$CashFlow_data[$i][3]?></td>
											<td><?=$CashFlow_data[$i][4]?></td>
											<td><?=$CashFlow_data[$i][5]?></td>
											<td><?=$CashFlow_data[$i][6]?></td>
										</tr>
											<?}?>
										<?} else {?>
										<tr>
											<td>[재무항목]</td>
											<td>00,000</td>
											<td>00,000</td>
											<td>00,000</td>
											<td>00,000</td>
											<td>00,000</td>
											<td>00,000</td>
										</tr>
										<?}?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
	<?
		/********
		*연결 재무제표
		*********/
		$JongCd = "222040";
		$geturl = "http://asp1.krx.co.kr/servlet/krx.asp.XMLJemu2?code=".$JongCd;
		$gettime="";

		try{
			$stockXmlData = file_get_contents($geturl);

			$sti = simplexml_load_string(trim($stockXmlData));
			
			
			/*조회시각*/
			$financialTotal = $sti->financialTotal;
			
			$gettime = $financialTotal[0]["querytime"];
			
			/*대차대조표*/
			
			$TBL_DaeCha_ym = $sti->TBL_DaeCha;
			
			$DaeCha_ym[0] = $TBL_DaeCha_ym[0]["year0"];
			$DaeCha_ym[1] = $TBL_DaeCha_ym[0]["month0"];
			$DaeCha_ym[2] = $TBL_DaeCha_ym[0]["year1"];
			$DaeCha_ym[3] = $TBL_DaeCha_ym[0]["month1"];
			$DaeCha_ym[4] = $TBL_DaeCha_ym[0]["year2"];
			$DaeCha_ym[5] = $TBL_DaeCha_ym[0]["month2"];
			
			$TBL_DaeCha_data = $sti->TBL_DaeCha->TBL_DaeCha_data;
		
			$daeCha_data_length = count($TBL_DaeCha_data);
			
			for($i=0;$i<$daeCha_data_length;$i++){
				
				$DaeCha_data[$i][0] = $TBL_DaeCha_data[$i]["hangMok".$i];
				$DaeCha_data[$i][1] = $TBL_DaeCha_data[$i]["year1Money".$i];
				$DaeCha_data[$i][2] = $TBL_DaeCha_data[$i]["year1GuSungRate".$i];
				$DaeCha_data[$i][3] = $TBL_DaeCha_data[$i]["year1JungGamRate".$i];
				$DaeCha_data[$i][4] = $TBL_DaeCha_data[$i]["year2Money".$i];
				$DaeCha_data[$i][5] = $TBL_DaeCha_data[$i]["year2GuSungRate".$i];
				$DaeCha_data[$i][6] = $TBL_DaeCha_data[$i]["year2JungGamRate".$i];
				$DaeCha_data[$i][7] = $TBL_DaeCha_data[$i]["year3Money".$i];
				$DaeCha_data[$i][8] = $TBL_DaeCha_data[$i]["year3GuSungRate".$i];
				$DaeCha_data[$i][9] = $TBL_DaeCha_data[$i]["year3JungGamRate".$i];
				
			}
			
			/*손익계산서*/
			
			$TBL_SonIk_ym = $sti->TBL_SonIk;
			
			$SonIk_ym[0] = $TBL_SonIk_ym[0]["year0"];
			$SonIk_ym[1] = $TBL_SonIk_ym[0]["month0"];
			$SonIk_ym[2] = $TBL_SonIk_ym[0]["year1"];
			$SonIk_ym[3] = $TBL_SonIk_ym[0]["month1"];
			$SonIk_ym[4] = $TBL_SonIk_ym[0]["year2"];
			$SonIk_ym[5] = $TBL_SonIk_ym[0]["month2"];
			
			$TBL_SonIk_data = $sti->TBL_SonIk->TBL_SonIk_data;
			
			$sonIk_data_length = count($TBL_SonIk_data);
			
			for($i=0;$i<$sonIk_data_length;$i++){
				
				$SonIk_data[$i][0] = $TBL_SonIk_data[$i]["hangMok".$i];
				$SonIk_data[$i][1] = $TBL_SonIk_data[$i]["year1Money".$i];
				$SonIk_data[$i][2] = $TBL_SonIk_data[$i]["year1GuSungRate".$i];
				$SonIk_data[$i][3] = $TBL_SonIk_data[$i]["year1JungGamRate".$i];
				$SonIk_data[$i][4] = $TBL_SonIk_data[$i]["year2Money".$i];
				$SonIk_data[$i][5] = $TBL_SonIk_data[$i]["year2GuSungRate".$i];
				$SonIk_data[$i][6] = $TBL_SonIk_data[$i]["year2JungGamRate".$i];
				$SonIk_data[$i][7] = $TBL_SonIk_data[$i]["year3Money".$i];
				$SonIk_data[$i][8] = $TBL_SonIk_data[$i]["year3GuSungRate".$i];
				$SonIk_data[$i][9] = $TBL_SonIk_data[$i]["year3JungGamRate".$i];
				
			}
			
			/*현금흐름표*/
			
			$TBL_CashFlow = $sti->TBL_CashFlow;
			
			$CashFlow_ym[0] = $TBL_CashFlow[0]["year0"];
			$CashFlow_ym[1] = $TBL_CashFlow[0]["month0"];
			$CashFlow_ym[2] = $TBL_CashFlow[0]["year1"];
			$CashFlow_ym[3] = $TBL_CashFlow[0]["month1"];
			$CashFlow_ym[4] = $TBL_CashFlow[0]["year2"];
			$CashFlow_ym[5] = $TBL_CashFlow[0]["month2"];
			
			$TBL_CashFlow_data = $sti->TBL_CashFlow->TBL_CashFlow_data;
			
			$cashFlow_data_length = count($TBL_CashFlow_data);
			
			for($i=0;$i<$cashFlow_data_length;$i++){
				
				$CashFlow_data[$i][0] = $TBL_CashFlow_data[$i]["hangMok".$i];
				$CashFlow_data[$i][1] = $TBL_CashFlow_data[$i]["year1Money".$i];
				$CashFlow_data[$i][2] = $TBL_CashFlow_data[$i]["year1JungGamRate".$i];
				$CashFlow_data[$i][3] = $TBL_CashFlow_data[$i]["year2Money".$i];
				$CashFlow_data[$i][4] = $TBL_CashFlow_data[$i]["year2JungGamRate".$i];
				$CashFlow_data[$i][5] = $TBL_CashFlow_data[$i]["year3Money".$i];
				$CashFlow_data[$i][6] = $TBL_CashFlow_data[$i]["year3JungGamRate".$i];
				
			}
		
		}
		catch(Exception $e){
		}
	?>
				<div class="finance_con">
					<div class="finance_box01 fin_bg">
						<div class="inner">
							<div class="tit">
								<p>요약 재무상태표 <span>(단위 : 백만원)</span></p>
							</div>
							<div class="finance_tbl">
								<table class="pc_tbl">
									<colgroup>
										<col width="25%" />
										<col width="25%" />
										<col width="25%" />
										<col width="25%" />
									</colgroup>
									<tbody>						
										<tr>
											<th rowspan="2">재무항목</th>
											<?if($DaeCha_ym != "") {?>
											<th><?=$DaeCha_ym[0]?>년 <?=$DaeCha_ym[1]?>월</th>
											<th><?=$DaeCha_ym[2]?>년 <?=$DaeCha_ym[3]?>월</th>
											<th><?=$DaeCha_ym[4]?>년 <?=$DaeCha_ym[5]?>월</th>
											<?}else{?>
											<th>YYYY년 MM월</th>
											<th>YYYY년 MM월</th>
											<th>YYYY년 MM월</th>
											<?}?>
										</tr>
										<tr>
											<th>금액</th>
											<th>금액</th>
											<th>금액</th>
										</tr>
										<?if($sonIk_data_length > 0){?>
											<?for($i=0;$i<$sonIk_data_length;$i++){ ?>
										<tr>
											<td><?=$SonIk_data[$i][0]?></td>
											<td><?=$SonIk_data[$i][1]?></td>
											<td><?=$SonIk_data[$i][4]?></td>
											<td><?=$SonIk_data[$i][7]?></td>
										</tr>
											<?}?>
										<?}else{?>
										<tr>
											<td>[재무항목]</td>
											<td>00,000</td>
											<td>00,000</td>
											<td>00,000</td>
										</tr>
										<?}?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<script>
				$(".finance_cont > div").hide();
				$(".finance_cont > div").eq(0).show();

				$(".finance_tab a").click(function(){
					
					var tbl_idx = $(this).index();

					$(".finance_tab a").removeClass("on");
					$(this).addClass("on");

					$(".finance_cont > div").hide();
					$(".finance_cont > div").eq(tbl_idx).show();
				});
			</script>
		</div>
		</div>
		<? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
		<? include "sub_footer/sub_footer.php" ?>
	</div>
</body>
</html>		
