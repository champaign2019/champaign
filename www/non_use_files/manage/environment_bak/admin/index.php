<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/environment/Admin.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

$admin = new Admin($pageRows, "admin", $_REQUEST);
$rowPageCount = $admin->getCount($_REQUEST);
$result = $admin->getList($_REQUEST);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
<script>
function groupDelete() {	
	if ( $('[name^=no]:checked').length > 0 ) {
		if (confirm("삭제하시겠습니까?")) {
			document.frm.submit();
		}
	} else {
		alert("삭제할 항목을 하나 이상 선택해 주세요.");
	}
}
</script>
</head>
<body> 
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<div id="wrap">
	<!-- canvas -->
	<div id="canvas">
		<!-- S T A R T :: headerArea-->
		<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/top.php" ?>
		<!-- E N D :: headerArea--> 
		
		<!-- S T A R T :: containerArea-->
		<div id="container">
			<div id="content">
				<div class="con_tit">
					<h2><?=$pageTitle?> - [목록]</h2>
				</div>
				<!-- //con_tit -->
				<div class="con">
					<!-- 내용 : s -->
					<div id="bbs">
						<div id="blist">
							<p><span><strong>총 <?=$rowPageCount[0]?>개</strong>  |  <?=$admin->reqPageNo?>/<?=$rowPageCount[1]?>페이지</span></p>
							<form name="frm" id="frm" action="process.php" method="post">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리목록입니다.">
								<colgroup>
									<col class="w3" />
									<col class="w5" />
									<? if($branch){?>
									<col class="w5" />
									<? } ?>
									<col class="w10" />
									<col class="w10" />
									<col class="" />
									<?
									if($gradeView){
									?>
									<col class="w10" />
									<?
									}
									?>
									<col class="w20" />
								</colgroup>
								<thead>
									<tr>
										<th scope="col" class="first">
											<label class="b_first_c"><input type="checkbox" name="allChk" id="allChk" onClick="check(this, document.frm.no)"/>
											<i></i>
											</label>
										</th>
										<th scope="col">번호</th>
										<? if($branch){?>
										<th scope="col">지점</th>
										<? } ?>
										<th scope="col">아이디</th>
										<th scope="col">이름</th>
										<th scope="col">이메일</th>
										<?
										if($gradeView){
										?>
										<th scope="col">등급</th>
										<?
										}
										?>
										<th scope="col" class="last">등록일</th>
									</tr>
								</thead>
								<tbody>
								<? if ($rowPageCount[0] == 0) { ?>
									<tr>
										<td class="first" colspan="8">등록된 관리자가 없습니다</td>
									</tr>
								<?
									 } else {
										$targetUrl = "";
										while ($row = mysql_fetch_array($result)) {
											$targetUrl = "style='cursor:pointer;' onclick=\"location.href='".$admin->getQueryString('read.php', $row['no'], $_REQUEST)."'\" ";
								?>
									<tr>
										<td class="first">
											<label class="b_nor_c"><input type="checkbox" name="no[]" id="no" value="<?=$row['no']?>"/>
											<i></i>
											</label>
										</td>
										<td <?=$targetUrl?>><?=$rowPageCount[0] - (($objCommon->reqPageNo-1)*$pageRows) - $i?></td>
										<? if($branch){?>
										<td <?=$targetUrl?>><? if ($row['hospital_name'] == '') echo "통합"; else echo $row['hospital_name']; ?></td>
										<? } ?>
										<td <?=$targetUrl?>><?=$row['id']?></td>
										<td <?=$targetUrl?>><?=$row['name']?></td>
										<td <?=$targetUrl?>><?=$row['email']?></td>
										<?
										if($gradeView){
										?>
										<td <?=$targetUrl?>><?=$row['grade_name']?></td>
										<?
										}
										?>
										<td <?=$targetUrl?>><?=$row['registdate']?></td>
									</tr>
								<?
										}
									 }
								?>
								</tbody>
							</table>
							<input type="hidden" name="cmd" id="cmd" value="groupDelete"/>
							<input type="hidden" name="sgrade" id="sgrade" value="<?=$_GET['sgrade']?>>"/>
							<input type="hidden" name="stype" id="stype" value="<?=$_GET['stype']?>"/>
							<input type="hidden" name="sval" id="sval" value="<?=$_GET['sval']?>"/>
							</form>
							<div class="btn">
								<div class="btnLeft">
									<a class="btns" href="#" onclick="groupDelete();"><strong>삭제</strong> </a>
								</div>
								<div class="btnRight">
									<a class="wbtn" href="write.php"><strong>글쓰기</strong> </a>
								</div>
							</div>
							<!--//btn-->
							<!-- 페이징 처리 -->
							<?=pageList($admin->reqPageNo, $rowPageCount[1], $admin->getQueryString('index.php', 0, $_REQUEST))?>
							<!-- //페이징 처리 -->
							<form name="searchForm" id="searchForm" action="index.php" method="get">
								<div class="search">
									<select name="stype" title="검색을 선택해주세요">
										<option value="all" <?=getSelected($_GET['stype'], "all") ?>>제목+내용</option>
										<option value="id" <?=getSelected($_GET['stype'], "id") ?>>아이디</option>
										<option value="name" <?=getSelected($_GET['type'], "name") ?>>이름</option>
										<option value="memo" <?=getSelected($_GET['stype'], "memo") ?>>메모</option>
									</select>
									<input type="text" name="sval" value="<?=$_GET['sval']?>" title="검색할 내용을 입력해주세요" />
									<input type="submit" class="se_btn " value="검색" />
								</div>
							</form>
							<!-- //search --> 
						</div>
						<!-- //blist -->
					</div>
					<!-- //bbs --> 
					<!-- 내용 : e -->
				</div>
				<!--//con -->
			</div>
			<!--//content -->
		</div>
		<!--//container --> 
		<!-- E N D :: containerArea-->
	</div>
	<!--//canvas -->
</div>
<!--//wrap -->

</body>
</html>