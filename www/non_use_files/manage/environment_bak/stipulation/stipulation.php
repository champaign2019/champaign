<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/environment/Stipulation.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";

$st = new Stipulation();
$row = $st->getData();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
<script>
var oEditors_privacy; // 에디터 객체 담을 곳
var oEditors_privacy_mini; // 에디터 객체 담을 곳
var oEditors_join; // 에디터 객체 담을 곳
$(window).load(function() {
	oEditors_privacy = setEditor("privacy_text"); // 에디터 셋팅
	oEditors_privacy_mini = setEditor("privacy_mini_text"); // 에디터 셋팅
	oEditors_join = setEditor("join_text"); // 에디터 셋팅
});
function go() {
	var sHTML = oEditors_privacy.getById["privacy_text"].getIR();
	if (sHTML == "") {
		alert('내용을 입력해 주세요.');
		$("#privacy_text").focus();
		return false;
	} else {
		oEditors_privacy.getById["privacy_text"].exec("UPDATE_CONTENTS_FIELD", []);	// 에디터의 내용이 textarea에 적용됩니다.
	}
	var sHTML1 = oEditors_privacy_mini.getById["privacy_mini_text"].getIR();
	if (sHTML1 == "") {
		alert('내용을 입력해 주세요.');
		$("#privacy_mini_text").focus();
		return false;
	} else {
		oEditors_privacy_mini.getById["privacy_mini_text"].exec("UPDATE_CONTENTS_FIELD", []);	// 에디터의 내용이 textarea에 적용됩니다.
	}
	var sHTML2 = oEditors_join.getById["join_text"].getIR();
	if (sHTML2 == "") {
		alert('내용을 입력해 주세요.');
		$("#join_text").focus();
		return false;
	} else {
		oEditors_join.getById["join_text"].exec("UPDATE_CONTENTS_FIELD", []);	// 에디터의 내용이 textarea에 적용됩니다.
	}
	$("#frm").submit();
}
</script>
</head>
<body> 
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<div id="wrap">
	<!-- canvas -->
	<div id="canvas">
		<!-- S T A R T :: headerArea-->
		<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/top.php" ?>
		<!-- E N D :: headerArea--> 
		
		<!-- S T A R T :: containerArea-->
		<div id="container">
			<div id="content">
				<div class="con_tit">
					<h2>개인정보 취급방침 / 회원약관</h2>* #company_name# 부분은 실제 사용자 페이지에서 해당 업체명으로 표기됩니다.
				</div>
				<!-- //con_tit -->
				<div class="con">
					<!-- 내용 : s -->
					<div id="bbs">
						<div id="bread">
							<form name="frm" id="frm" action="process.php" method="post">
							<h3>개인정보 취급방침</h3>
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리 기본내용입니다.">
								<colgroup>
									<col width="100%" />
								</colgroup>
								<tbody>
									<tr>
										<td>
											<textarea id="privacy_text" name="privacy_text" rows="10" style="width:100%"><?=$row['privacy_text']?></textarea>
										</td>
									</tr>
								</tbody>
							</table>
							<h3>개인정보 취급방침 간소</h3>
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리 기본내용입니다.">
								<colgroup>
									<col width="100%" />
								</colgroup>
								<tbody>
									<tr>
										<td>
											<textarea id="privacy_mini_text" name="privacy_mini_text" rows="10" style="width:100%"><?=$row['privacy_mini_text']?></textarea>
										</td>
									</tr>
								</tbody>
							</table>
							<h3>회원약관</h3>
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리 기본내용입니다.">
								<colgroup>
									<col width="100%" />
								</colgroup>
								<tbody>
									<tr>
										<td>
											<textarea id="join_text" name="join_text" rows="10" style="width:100%"><?=$row['join_text']?></textarea>
										</td>
									</tr>
								</tbody>
							</table>
							</form>
							<div class="btn">
								<div class="btnRight">
									<a class="btns" href="#" onclick="go();"><strong>저장</strong></a>
								</div>
							</div>
							<!--//btn-->
						</div>
						<!-- //bread -->
					</div>
					<!-- //bbs --> 
					<!-- 내용 : e -->
				</div>
				<!--//con -->
			</div>
			<!--//content -->
		</div>
		<!--//container --> 
		<!-- E N D :: containerArea-->
	</div>
	<!--//canvas -->
</div>
<!--//wrap -->

</body>
</html>