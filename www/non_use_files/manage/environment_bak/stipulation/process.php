<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/environment/Stipulation.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
</head>
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<?
$st = new Stipulation();
$r = $st->update($_REQUEST);

if ($r > 0) {
	echo returnURLMsg(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'stipulation.php'), '정상적으로 수정되었습니다.');
} else {
	echo returnURLMsg(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'stipulation.php'), '요청처리중 장애가 발생하였습니다.');
}
?>
</html>