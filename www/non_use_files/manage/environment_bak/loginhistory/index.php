<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/environment/Admin.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

$admin = new Admin($pageRows, "admin", $_REQUEST);
$rowPageCount = $admin->getCountLoginHistory($_REQUEST);
$result = $admin->getLoginHistoryList($_REQUEST);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
</head>
<body> 
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<div id="wrap">
	<!-- canvas -->
	<div id="canvas">
		<!-- S T A R T :: headerArea-->
		<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/top.php" ?>
		<!-- E N D :: headerArea--> 
		
		<!-- S T A R T :: containerArea-->
		<div id="container">
			<div id="content">
				<div class="con_tit">
					<h2><?=$pageTitle?> - [목록]</h2>
				</div>
				<!-- //con_tit -->
				<div class="con">
					<!-- 내용 : s -->
					<div id="bbs">
						<div id="blist">
							<p><span><strong>총 <?=$rowPageCount[0]?>개</strong>  |  <?=$admin->reqPageNo?>/<?=$rowPageCount[1]?>페이지</span></p>
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자접속기록 목록입니다.">
								<colgroup>
									<col class="w10" />
									<col class="" />
									<col class="w20" />
									<col class="w20" />
									<col class="w20" />
								</colgroup>
								<thead>
									<tr>
										<th scope="col" class="first">번호</th>
										<th scope="col">아이디</th> 
										<th scope="col">이름</th> 
										<th scope="col">아이피</th> 
										<th scope="col">접속일</th> 
									</tr>
								</thead>
								<tbody>
								<? if ($rowPageCount[0] == 0) { ?>
									<tr>
										<td class="first" colspan="5"><%=UsingProperties.getMsg("alert.board.nodata")%></td>
									</tr>
								<?
									 } else {
										 $i=0;
										 while ($row = mysql_fetch_array($result)) {
								?>
									<tr>
										<td class="first"><?=$rowPageCount[0]-(($admin->reqPageNo-1)*$pageRows)-$i?></td>
										<td><?=$row['id']?></td>
										<td><?=$row['name']?></td>
										<td><?=$row['ip']?></td>
										<td class="last"><?=$row['logindate']?></td>
									</tr>
								<?
											$i++;
										}
									}
								?>
								</tbody>
							</table>
							<!--//btn-->
							<!-- 페이징 처리 -->
							<?=pageList($admin->reqPageNo, $rowPageCount[1], $admin->getQueryString('index.php', 0, $_REQUEST))?>
							<!-- //페이징 처리 -->
							<form name="searchForm" id="searchForm" action="index.php" method="get">
								<div class="search">
									<select name="stype" title="검색을 선택해주세요">
										<option value="all" <?=getSelected($stype, "all") ?>>전체</option>
										<option value="id" <?=getSelected($stype, "id") ?>>아이디</option>
										<option value="name" <?=getSelected($type, "name") ?>>이름</option>
										<option value="ip" <?=getSelected($stype, "ip") ?>>아이피</option>
									</select>
									<input type="text" name="sval" value="<?=$_GET['sval']?>" title="검색할 내용을 입력해주세요" />
									<img src="/manage/img/btn_search.gif" class="sbtn" alt="검색" style='cursor:pointer;' onclick="$('#searchForm').submit();"/>
								</div>
							</form>
						</div>
						<!-- //blist -->
					</div>
					<!-- //bbs --> 
					<!-- 내용 : e -->
				</div>
				<!--//con -->
			</div>
			<!--//content -->
		</div>
		<!--//container --> 
		<!-- E N D :: containerArea-->
	</div>
	<!--//canvas -->
</div>
<!--//wrap -->

</body>
</html>