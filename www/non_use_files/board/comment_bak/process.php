<?session_start();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Comment.class.php";

include $_SERVER['DOCUMENT_ROOT']."/include/logingCheck.php";

?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

</head>
<body>
<?
if (checkReferer($_SERVER["HTTP_REFERER"])) {
	$comment = new Comment(9999, $_REQUEST);
	$comment_cmd = $_REQUEST['cmd'];
	$_REQUEST['member_fk'] = chkIsset($_REQUEST['member_fk']);


	if ($comment_cmd == 'r_write') {
		$r = $comment->insert($_REQUEST);
		if ($r > 0) {
			echo returnURLMsg($_REQUEST['url'], '정상적으로 저장되었습니다.');
		} else {
			echo returnURLMsg($_REQUEST['url'], '요청처리중 장애가 발생하였습니다.');
		}

	} else if ($comment_cmd == 'r_delete') {
		$cp = $comment->checkPassword($_REQUEST[no], $_REQUEST[password]);
		if ($cp) {
			$no = $_REQUEST['no'];
			$r = $comment->delete($no);
		
			if ($r > 0) {
				echo returnURLMsg($_REQUEST['url'], '정상적으로 삭제되었습니다.');
			} else {
				echo returnURLMsg($_REQUEST['url'], '요청처리중 장애가 발생하였습니다.');
			}
		} else {
			echo returnURLMsg($_REQUEST['url'], '비밀번호가 올바르지 않습니다.');
		}
	} 
} else {
	echo returnURLMsg($_REQUEST['url'], '요청처리중 장애가 발생하였습니다.1');
}


?>
</body>
</html>