<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Comment.class.php";

$comment = new Comment(9999, $tablename, $_REQUEST);
$result = $comment->getList($_REQUEST);

?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/boardConfig/config.php" ?>
<script>
	function goSave() {
		
		if($("#name").val()=="" |$("#name").val()=="이름") {
			alert('이름을 입력해주십시오.');
			$("#name").focus();
			return false;
		}
		
		if($("#password").length > 0) {
			if($("#password").val()=="") {
				alert('비밀번호를 입력해주십시오.');
				$("#password").focus();
				return false;
			}
		}else{
			if($("#password_temp").val()=="비밀번호") {
				alert('비밀번호를 입력해주십시오.');
				$("#password_temp").focus();
				return false;
			}
		}
		
		if($("#contents").val()=="") {
			alert('내용을 입력해주십시오.');
			$("#contents").focus();
			return false;
		}

		var form = $("#frm");

		fn_formSpanCheck(form,key);	
		//스팸 끝

		return true;
	}
	
	function goCommentDelete(obj) {
			var str = "삭제하시겠습니까?";
		if($("#password_del").val()=="") {
			alert('패스워드를 입력해주십시오.');
			$("#password_del").focus();
			return false;
		}
		if(confirm(str)) {
			$("#delete_frm").append("<input type='hidden' name='password' value='"+$("#password_del").val()+"'/>");
			$("#delete_frm").submit();
		}else{
			return false;
		}
	}
	
	/*function openDiv(obj) {
		$("#no").val($(obj).attr("id"));
		var x = obj.offset().top-($("#bread").offset().top+$("#password_box").height()+50);
		var y = obj.offset().left-($("#bread").offset().left+$("#password_box").width()+30);
		$("#password_box").css({"top":x, "left":y});
		$("#password_box").show();
	}*/

	function openDiv(obj) {
		$("#no").val($(obj).attr("id"));
		var x = obj.offset().top - 5;
		var y = obj.offset().left - ( $("#password_box").width() + 20) ;
		$("#password_box").css({"top":x, "left":y});
		$("#password_box").show();
	}


	function closeDiv() {
		$("#password_box").hide();
		return false;
	}
	
	
$(window).load(function(){
	
	$(".r_delete").click(function(){
		openDiv($(this));	
	});
	
	$(".r_delete").focus(function(){
		openDiv($(this));	
	});
	
	$(".del_submit").click(function(){
		goCommentDelete($(this));	
	});
	$(".del_submit").focus(function(){
		//goCommentDelete($(this));	
	});
	
	$(".focus_zone").focus(function(){
		focusTextRemove($(this));
	});
	
	$(".focus_zone").blur(function(){
		blurTextInsert($(this));
	});
		
});
</script>
<div class="read_reple">
	
		<form name="delete_frm" id="delete_frm" action="<?=getSslCheckUrl($_SERVER['SERVER_NAME'], '/board/comment/process.php')?>" method="post" >
		<? if (mysql_num_rows($result) == 0) { ?>
		<dl>
			<dd class="bbsno">
				등록된 댓글이 없습니다.
			</dd>
		</dl>
		<? } else { ?>
		<? while ($co_row = mysql_fetch_assoc($result)) { ?>
		<dl class="reple_dl">
			<dt><strong><?=$co_row['name']?></strong> <?=$co_row['registdate']?></dt>
			<dd><?=$co_row['contents']?><span class="reEdit"><strong><input type="button" class="r_delete" id="<?=$co_row[no]?>" value="삭제"/></strong></span></dd>
		</dl>
		<? }
		}
		?>
		<!-- //box --> 
		<input type="hidden" name="cmd" value="r_delete"/>
		<input type="hidden" name="no" id="no"  value=""/>
		<input type="hidden" name="parent_fk" value="<?=$comment->parent_fk?>"/>
		<input type="hidden" name="member_fk"  value="<?=$_SESSION[member_no]?>"/>
		<input type="hidden" name="tablename"  value="<?=$tablename?>"/>
		<input type="hidden" name="url"	value="<?=$_SERVER['REQUEST_URI']?>"/>
		<input type="submit" style="display:none;" />
	</form>

		
	<div class="read_rego">
		<form name="frm" id="frm" action="/board/comment/process.php" method="post" onsubmit="return goSave();">
			<dl>
				<dt>
					<div><input type="text" class="focus_zone" name="name" id="name" title="작성자 이름을 입력해주세요" value="<?=!$_SESSION['member_name'] ? "이름" : $_SESSION['member_name']?>"/></div>
					<div><input type="text" class="focus_zone" name="password_temp" id="password_temp" value="비밀번호" title="비밀번호를 입력해주세요" /></div>
				</dt>
				<dd>
					<textarea class="focus_zone" name="contents" id="contents" title="내용을 입력해주세요"></textarea>
					<input value="확인" type="submit" />
				</dd>
			</dl>
			<input type="hidden" name="cmd" value="r_write"/>
			<input type="hidden" name="parent_fk" value="<?=$comment->parent_fk?>"/>
			<input type="hidden" name="member_fk"  value="<?=$_SESSION[member_no]?>"/>
			<input type="hidden" name="tablename"  value="<?=$tablename?>"/>
			<input type="hidden" name="url" value="<?=$_SERVER["REQUEST_URI"]?>"/>
		</form>
	</div>
	<!-- //rego -->
</div>

<div id="password_box" class="password_box">
	<div>
		<dl>
			<dt>패스워드</dt>
			<dd>
				<input type="password" name="password_del" id="password_del" size="8" value="" title="비밀번호 확인" onkeyup="javascript:if(event.keyCode == 13){goCommentDelete($(this))}"/>
				<a><input type="button" class="del_submit" value="삭제" /></a>
				<input type="button" class="password_box_x" onclick="closeDiv();" onfocus="closeDiv()" value="X" />
			</dd>
			
		</dl>
	</div>
	<!-- //in_box -->
</div>