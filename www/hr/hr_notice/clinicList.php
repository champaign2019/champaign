<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.vizensoft.management.branch.*" %>
<%@ page import="com.vizensoft.util.*" %>
<%@ page import="java.util.*" %>
<%@ page import="org.apache.log4j.*" %>
<%@ include file="config.jsp" %>
<%
int branch_fk		= Function.getIntParameter(request.getParameter("branch_fk"), 0);		// 지점 PK
int type_fk		= Function.getIntParameter(request.getParameter("type_fk"), 0);		// 선택되어야할 분류
int type		= Function.getIntParameter(request.getParameter("type"), 0);		// 선택되어야할 분류

TypeVO param = TypeVO.getParam(pageRows, request);
TypeDAO dd = TypeDAO.getInstance();
String typeList = dd.selectBoxList(type,branch_fk,type_fk);
%>
<%= typeList %>
