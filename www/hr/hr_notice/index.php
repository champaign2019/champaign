<?session_start();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?

include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Common.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Hospital.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Clinic.class.php";

include $_SERVER['DOCUMENT_ROOT']."/include/logingCheck.php";
include "config.php";
if ($boardgrade == 3) { // 게시판접근 권한처리
	include $_SERVER['DOCUMENT_ROOT']."/include/gradeCheck.php";
}

$common = new Common($pageRows, $tablename, $_REQUEST);
$_REQUEST['orderby'] = $orderby;
$rowPageCount = $common->getCount($_REQUEST);
$result = $common->getList($_REQUEST);
	if($_REQUEST['view_loc_type'] != 1){ //제거 하기 

?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/include/headHtml.php" ?>
<script type="text/javascript">
$(window).load(function(){
	
	
	if($(".program_table tbody tr").size() == rowCount){
		$(".mo_programPage").hide();	
	}
	$(".program_table td.tit").mouseenter(function(){
		$(this).parent("tr").addClass("on_e");
	});
	$(".program_table td.tit").mouseleave(function(){
		$(this).parent("tr").removeClass("on_e");
	});
	
})

var rowCount = <?=$rowPageCount[0]?>

$(window).load(function(){
	
	
	if($(".program_table tbody tr").size() == rowCount){
		$(".mo_programPage").hide();	
	}
	
})

function goSearch() {
	$("#searchForm").submit();
}


function more(){
	
	//다음페이지 계산
	var reqPageNo = (Math.ceil($(".program_table tbody tr").size()/<?=$pageRows?>)+1);
	
	$.post("index.php",
	{
		reqPageNo : reqPageNo,
		stype : $("#stype").val(),
		view_loc_type : 1,
		sval : $("#sval").val()
		
	},function(data){
		
		$(".program_table tbody").append(data);
		if($(".program_table tbody tr").size() == rowCount){
			$(".mo_programPage").hide();
		}
		
		
	}).fail(function(error){
		
	})
	
}
</script>
</head>

<body>

<div id="wrap">
	<? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
	<? include "sub_visual/sub_visual.php" ?>
	<!-- 프로그램시작 -->
	<div class="programCon" data-aos="fade-down" data-aos-delay="400">
		<div class="program_table">
			<table>
				<colgroup>
					<col class="w80 none1000" />
					<col width="*" />
					<col class="w100" />
					<col class="w150" />
					<? if($userCon){?>
					<col class="w70" />
					<? } ?>
				</colgroup>
				<thead>
					<tr>
						<th class="none1000">번호</th>
						<th>제목</th>
						<th>작성자</th>
						<th>작성일</th>
						<th>조회수</th>
					</tr>
				</thead>
				<tbody class="programBody">
<? } ?>
					<? if ($rowPageCount[0] == 0) { ?>
						<tr class="bbsno">
							<td colspan="4">등록된 글이 없습니다.</td>
						</tr>
					<?
						} else {
							$topClass = "";
							$targetUrl = "";
							$i = 0;
							while ($row=mysql_fetch_assoc($result)) {
								$targetUrl = "style='cursor:pointer;' onclick=\"location.href='".$common->getQueryString('read.php', $row[no], $_REQUEST)."'\"";
								
								//(지점 > 분류) 체크 로직
								$firstTitle = "";
								if($branch && $row['branch_name']){
								
									$firstTitle .= "<span class='branch01'>" .$row['branch_name']. "</span>"; 
								
								}
								if($type && $row['type_name']){
									if($row['firstTitle'] != "["){
										$firstTitle.="";
									}	
									$firstTitle.= "<span class='branch02'>" .$row['type_name']. "</span>";
								}
								
								$firstTitle.="";
							
								if("[]" == $firstTitle){
									$firstTitle = "";
								}
					?>
					<tr>
						<td class="none1000">
							<? if ($row['top'] == "1") { ?>
								<span class="noti_icon">공지</span>
							<? } else { ?>
								<?=$unickNum ? $row['no'] : $rowPageCount[0]-($common->reqPageNo-1)*$pageRows-$i?>
							<? } ?>
						</td>
						<td class="tit" <?=$targetUrl?>>
							<p class="mw100">
								
								<?=$firstTitle?> <?=$row['title']?>
								<? if (checkNewIcon($row[registdate], $row[newicon], 1)) { ?>
									<img class="newIcon" src="/img/newIcon.png" alt="">
								<? } ?>
							</p>
							</td>
						<td><?=$row['name']?></td>
						<td>
						<?
						if($timeDate){
						?>
						<?= getDateTimeFormat($row['registdate']) ?>
						<?
						}else{
						?>
						<?= getYMD($row['registdate']) ?>
						<?
						}
						?>
						</td>
						<td><span class="hit"><img src="/manage/img/ucount_icon.png" /> </span><?=$row['readno']?></td>
					</tr>
					<?	$i++;
							}
					?>
					<? } ?>
				<? if($_REQUEST['view_loc_type'] != 1){ //제거 하기  ?>
				</tbody>
			</table>
		</div>
		<div class="mo_programPage"><a href="javascript:;" onclick="more()">더보기<span>+</span></a></div>
		<?=pageListUser($common->reqPageNo, $rowPageCount[1], $common->getQueryString('index.php', 0, $_REQUEST))?><!-- 페이징 처리 -->
		
		<form name="searchForm" id="searchForm" action="index.php" method="get">
		<div class="program_search">
			<? 
				if ($branch) {
					$branchObj = new Branch(999, $_REQUEST);
					$hResult = $branchObj->branchSelect();
			?>
			<select name="sbranch_fk" title="지점을 선택해주세요" onchange="goSearch();">
				<option value="-1" <?=getSelected(-1, $_REQUEST['sbranch_fk'])?>>지점 선택</option>
				<? while ($row=mysql_fetch_assoc($hResult)) { ?>
				<option value="<?=$row['no']?>" <?=getSelected($row['no'], $_REQUEST['sbranch_fk'])?>><?=$row['name']?></option>
				<? } ?>
			</select>
			<? } ?>
			<? 
				if ($type) {
					$typeObj = new Type(999, $_REQUEST);
					$typeList = $typeObj->selectBoxList(0,($branch ? $_REQUEST['sbranch_fk'] : DEFAULT_BRANCH_NO),$_REQUEST['stype_fk']);
			?>
			<select name="stype_fk" title="분류을 선택해 주십시오." onchange="goSearch();">
				<?=$typeList?>
			</select>
			<? } ?>
			<select name="stype" title="검색을 선택해주세요">
				<option value="all" <?=getSelected($_REQUEST[stype], "all") ?>>제목+내용</option>
				<option value="title" <?=getSelected($_REQUEST[stype], "title") ?>>제목</option>
				<option value="name" <?=getSelected($_REQUEST[stype], "name") ?>>작성자</option>
				<option value="contents" <?=getSelected($_REQUEST[stype], "contents") ?>>내용</option>
			</select>
			<span>
				<input type="text" name="sval" value="<?=$_REQUEST[sval]?>" title="검색할 내용을 입력해주세요" />
				<a href="javascript:;" onclick="$('#searchForm').submit()" >검색</a>
			</span>
		</div>
		</form>
	</div>
	<!-- 프로그램끝 -->
	<? include "sub_footer/sub_footer.php" ?>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
</div>
</body>
</html>

<? } ?>