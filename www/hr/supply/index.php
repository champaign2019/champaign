<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Common.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Hospital.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Clinic.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/environment/Stipulation.class.php";

include "config.php";
if ($boardgrade == 3 || $boardgrade == 2)  { // 게시판 접근 권한 처리
	
	include $_SERVER['DOCUMENT_ROOT']."/include/gradeCheck.php";

}

$objCommon = new Common($pageRows, $tablename, $_REQUEST);
$category_result = $category_tablename ? $objCommon->getCategoryList($_REQUEST) : null;
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/include/headHtml.php" ?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/boardConfig/config.php" ?>
<script>

function goSave(obj) {
	
	
	if(validation(obj)){
		
		<?if(!$isCellSum){?>
		if($("[name=tel1]").val() != "" && $("input[name=tel2]").val() != "" && $("input[name=tel3]").val() != ""){
		$("input[name=tel]").val($("[name=tel1]").val()+"-"+$("input[name=tel2]").val()+"-"+$("input[name=tel3]").val())
		}
		
		if($("[name=cell1]").val() != "" && $("input[name=cell2]").val() != "" && $("input[name=cell3]").val() != ""){
		$("input[name=cell]").val($("[name=cell1]").val()+"-"+$("input[name=cell2]").val()+"-"+$("input[name=cell3]").val())
		}
		
		<?}?>
		
		if($("input[name=email1]").val() != "" && $("[name=email2]").val() != ""){
		$("input[name=email]").val($("input[name=email1]").val().trim()+"@"+$("[name=email2]").val().trim());
		}
		
		if($("[name=tm_year]").val().trim() != ""){
			$("[name=title]").val($("[name=tm_year]").val()+"년 ");
		}
		
		if($("[name=tm_month]").val() != ""){
			$("[name=title]").val($("[name=tm_month]").val()+"개월");
		}
		
		//여기부터 스팸
		//if(!fn_spamCheck()){return false;}
		
		var form = $("#frm");

		fn_formSpanCheck(form,key);	
		//스팸 끝
		$(form).submit()	
	}else{
		return;
	}
}

function fn_category(obj){
	
	if($(obj).val() == 1){
		
	}else{
		
	}
	
}
</script>
</head>

<body>
	
	<div id="wrap">
		<? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
		<? include "sub_visual/sub_visual.php" ?>
		<!-- 프로그램 시작 -->
		<div class="programCon" data-aos="fade-down" data-aos-delay="400">
				<form name="frm" id="frm" action="<?=getSslCheckUrl($_SERVER['REQUEST_URI'], 'process.php')?>" method="post" enctype="multipart/form-data" >
				<div class="writeForm check_th">
					<table>
						<colgroup>
							<col class="writeForm_col04" />
							<col width="*" />
						</colgroup>
						<tr>
							<th>지원자 성함</th>
							<td>
								<input type="text" name="name" data-value="지원자 성함을 입력해주십시오." class="inputTxt inputName" />
							</td>
						</tr>
						<tr>
							<th>지원자 연락처</th>
							<td>
								<input type="text" name="cell" data-value="지원자 연락처를 입력해주십시오." class="inputTxt inputName" />
							</td>
						</tr>
						<tr>
							<th>지원자 이메일</th>
							<td>
								<input data-value="이메일을 입력하세요." name="email1" id="email1" class="inputEmail" type="text" value="<?=getSplitIdx($_SESSION['member_email'], "@", 0)?>" maxlength="70"  /><span class="email_txt">@</span>
								<select class="selecEmail" name="email2" id="email2" data-value="이메일을 선택하세요">
								<option value="">선택하세요</option>
								<?=getOptsEmail(getSplitIdx($_SESSION['member_email'], "@", 1))?>
							</select>
								<input type="hidden" name="email" id="email" value=""/>
							</td>
						</tr>
						<tr class="spty">
							<th>지원부서</th>
							<td>
								<?
								if($type){
									$typeObj = new Clinic(999, $_REQUEST);
									$typeList = $typeObj->radioList(0,0,0);
									
								?>
								<?= $typeList ?>
								<?
								}
								?>
								<!-- label><input type="radio" name="hospital_fk" value="1" data-value="지원분야를 선택해주십시오." /> 영업</label>
								<label><input type="radio" name="hospital_fk" value="2" data-value="지원분야를 선택해주십시오." /> 경영지원</label>
								<label><input type="radio" name="hospital_fk" value="3" data-value="지원분야를 선택해주십시오."/> 개발</label>
								<label><input type="radio" name="hospital_fk" value="4" data-value="지원분야를 선택해주십시오."/> 기획</label>
								<label><input type="radio" name="hospital_fk" value="5" data-value="지원분야를 선택해주십시오."/> 마케팅</label>
								<label><input type="radio" name="hospital_fk" value="6" data-value="지원분야를 선택해주십시오."/> 기타</label-->
							</td>
						</tr>
						<tr class="spty">
							<th>구분</th>
							<td>
								<label><input type="radio" name="category_fk" value="1" data-value="경력을 선택해주십시오."/> 신입</label>
								<label><input type="radio" name="category_fk" value="2" data-value="경력을 선택해주십시오."/> 경력</label>
								<input type="text" name="tm_year" class="inputNum02" onkeyup="onlyNumber(this);" maxlength="2" /> <label class="inner"> 년</label>
								<input type="text" name="tm_month" class="inputNum02" onkeyup="onlyNumber(this);" maxlength="2" /> <label class="inner"> 개월</label>
								<div class="hope_c"><label>희망연봉 <input type="text" name="state" class="inputNum02" onkeyup="onlyNumber(this);" maxlength="10" />  만원</label></div>
							</td>
						</tr>
						<tr>
							<th>지원내용</th>
							<td><textarea id="contents" name="contents"  data-value="내용을 입력하세요." ></textarea></td>
						</tr>
						<tr>
							<th>이력서</th>
							<td><input type="file" name="imagename1" data-value="" /></td>
						</tr>			
						<tr>
							<th>자기소개서</th>
							<td><input type="file" name="imagename2" data-value="" /></td>
						</tr>
						<tr>
							<th>포트폴리오</th>
							<td><input type="file" name="imagename3" data-value="" /></td>
						</tr>
					</table>
					<label class="agr_last"><input type="checkbox" name="agree" value="1" data-value="개인정보 취급방침에 동의합니다."/>개인정보 취급방침에 동의합니다.</label>
				</div>
				<div class="writeForm_btn">
					<a href="javascript:void(0);" class="color_type" onclick="goSave(this)" >지원하기</a>
				</div>
				<input type="hidden" name="cmd" value="write" />
				<input type="hidden" name="title" value="" />
				
			</form>
		</div>
		<!-- 프로그램끝 -->
		<? include "sub_footer/sub_footer.php" ?>
		<? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
	</div>
</body>
</html>
		
