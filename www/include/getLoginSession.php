<?
	include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
	/* ssl 사용할 경우에만 처리 */
	if ( SSL_USE ) {
		/*
			memberInfo세션(비젠소프트 공통 사용자 로그인 세션)이 null이면서 (= 비로그인 경우)
		*/
		if ( empty($_SESSION['member_no']) ) {
			/*
				static memberSession이 null이 아니면
				(= 로그인이 되어있지만 도메인이 달라졌거나 하는 경우에 session 유지가 되지 않은 경우)
			*/
			if ( $GLOBALS['memberSession'] ) {
				/*
					session에 다시 set 해준다.
				*/
				$memberSession = $GLOBALS['memberSession'];
//				print_r($memberSession);
//				echo '<br/>';
				$memberSessionKey = array_keys($memberSession);
				for ($i=0; $i<count($memberSession); $i++) {
//					echo $memberSessionKey[$i].' : '.$memberSession[$memberSessionKey[$i]]."<br/>";
					$_SESSION[$memberSessionKey[$i]] = $memberSession[$memberSessionKey[$i]];
				}
			}
		}
	}
?>