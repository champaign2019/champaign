<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/member/Member.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/log/Log.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/sms/Sms.class.php";

include "config.php";

include_once $_SERVER['DOCUMENT_ROOT']."/include/logingCheck.php";
$member = new Member(999, 'member', $_REQUEST);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/include/headHtml.php" ?>
</head>
<body>
<?
if (checkReferer($_SERVER["HTTP_REFERER"])) {
	$subject = "비젠소프트";
	$body = "약도이미지입니다.";

	// SMS발송
	$sms = new Sms(999, $_REQUEST);
	$_REQUEST['tran_id'] = SMS_KEYNO;
	$_REQUEST['tran_etc1'] = SMS_CNAME;
	$_REQUEST['tran_etc2'] = SMS_USERID;
	$_REQUEST['tran_etc3'] = SMS_USERNAME;
	$_REQUEST['sendtype'] = 'cell';
	$_REQUEST['tran_status'] = '1';
	$_REQUEST['tran_date'] = getFullToday();

	if ($_REQUEST[receiver]) {
		$_REQUEST['tran_callback'] = COMPANY_TEL;
		$_REQUEST['receiver'] = $_REQUEST['receiver'];
		$_REQUEST['tran_msg'] = $body;
		$_REQUEST['mms_body'] = $body;
		$_REQUEST['mms_subject'] = $subject;
		$_REQUEST['file_name1'] = "ch_kd_map.jpg";
		$sms->sendMMS($_REQUEST);
	}

	echo returnURLMsg($member->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'minimap.php'), 0, $_REQUEST), '정상적으로 발송되었습니다.');

} else {
	echo returnURLMsg($member->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'minimap.php'), 0, $_REQUEST), '요청처리중 장애가 발생하였습니다.1');
}
?>
</body>
</html>