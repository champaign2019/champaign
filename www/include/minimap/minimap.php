<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";

include $_SERVER['DOCUMENT_ROOT']."/include/logingCheck.php";
include "config.php";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/include/headHtml.php" ?>
<script type="text/javascript">
function goSave(){
	if ($("#receiver").val() == "") {
		alert('휴대폰 번호를 입력해 주세요.');
		$("#receiver").focus();
		return false;
	}
	
	return true;
}
</script>
</head>
<body>
<form name="frm" id="frm" action="<?=getSslCheckUrl($_SERVER['REQUEST_URI'], 'process.php')?>" method="post" onsubmit="return goSave();" >
<div id="bbs">
	<div id="bread">
		<div class="rcon">
			<div class="readCont">
				<p>
				휴대폰 번호
				<input type="text" name="receiver" id="receiver" onKeyUp="isNumberOrHyphen(this);cvtPhoneNumber(this);" title="휴대폰번호를 입력해주세요." />
				<input type="hidden" name="cmd" id="cmd" value="mms" />
				<strong class="btn_in inbtn">
			<input type="submit" class="btns" value="요청"/>
		 </strong>
				</p>
			</div>
		</div>
		<!-- /rcon -->
	</div>
	<!-- //bread -->
</div>
<!-- //bbs --> 	
</form>
</body>
</html>