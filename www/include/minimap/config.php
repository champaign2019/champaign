<?
	$pageTitle 		= "문자메세지";
	$tablename		= "smstemplate";						// 게시판 테이블명
	$uploadPath		= "/upload/sms/";						// 파일, 동영상 첨부 경로
	$branch 		= true;									// 지점 사용유무  
	$pageRows		= 10;
	$saveSmsRows	= 9;
	$gradeno		= 2;									// 관리자 권한 기준 [지점 사용시]
			
	$isreceiver 	= 1;									// 수신여부 기능 (0 = 미사용, 1 = 사용)
	$isbirthday 	= 0;									// 생일여부 기능 (0 = 미사용, 1 = 사용)
	
	$memberPageRows	= 20;									// 리스트에 보여질 로우 수(회원목록)
	$reserPageRows	= 20;									// 리스트에 보여질 로우 수(예약내역)
	$sendPageRows	= 20;									// 리스트에 보여질 로우 수(발송내역)
	
	$maxSaveSize	= 50*1024*1024;							// 50Mb
	$userCon 		= false;

	// castle적용
	include_once($_SERVER['DOCUMENT_ROOT']."/include/castle.php");
?>