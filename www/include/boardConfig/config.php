<script>
var oEditors = new Array();
var ignore = '<?=$editorIgnore?>';
$(window).load(function(){
	
	//inputMaxLength();
	
	//mkAutoDateFormat();
	
	<?
	if($textAreaEditor){
	?>
		fn_textarea_check(ignore);
	<?
	}
	?>
	// 에디터 기본 font 설정
	$(".note-editable.panel-body").find("p font").attr("face",$(".note-current-fontname").css("font-family"));
		
	if($("#hospital_fk").size() > 0){
		$("#hospital_fk").change(function(){
			clinicSelectbox($(this).val(), 0, 0);
			
			if($("#doctor_fk").size() > 0){
				
				doctorSelectbox($(this).val(),0,0,0);
			}
			
		});
		
		if($(".schedule").size() > 0){
			$("#hospital_fk").change(function(){
				calendarNewList($("#hospital_fk").val(), $("#curMonth").val());
			});
		}
	}
	
	if($("#clinic_fk").size() > 0){
		$("#clinic_fk").change(function(){
			var clinickFk = $("#clinic_fk option:selected").data("hospital");
			
			if($("#hospital_fk").size() > 0){
				var obj = $("#hospital_fk option").filter(function(){if($(this).val() == clinickFk){return this}});
				$(obj).prop("selected",true);
			}
			
			
			if($("#doctor_fk").size() > 0){
				<?
				if(CLINIC_DOCTOR_REL){
				?>
				doctorSelectbox($("#hospital_fk").val(),$(this).val(),0,0);
				<?
				}else{
				?>				
				doctorSelectbox($("#hospital_fk").val(),0,0,0);
				<?
				}
				?>
			}
			
			
			if($("#hospital_fk").size() == 0){
				if($("#curMonth").size() > 0){
					calendarNewList(1, $("#curMonth").val());
				}
			}else{
				if($("#curMonth").size() > 0){
					calendarNewList($("#hospital_fk").val() == undefined ? 1 : $("#hospital_fk").val(), $("#curMonth").val());
				}
			}
			
		});
	
	}
	
	

	if($("#curMonth").size() > 0){
		
		calendarNewList($("#hospital_fk").val() == undefined ? 1 : $("#hospital_fk").val(), $("#curMonth").val());
	}
	
	
});

$(window).resize(function(){
	<?
	if($textAreaEditor){
	?>
	//fn_textarea_check(ignore);
	<?
	}
	?>
		
});


function fn_textarea_check(ignore){
		setTimeout(function(){
			
			$(".programCon textarea, #bbs textarea").each(function(i){
					if(ignore.indexOf("|"+i+"|") < 0){
						var tagCheck = $(this).next().prop("tagName");
						if(tagCheck == undefined){
							setSummertnoteEditor($(this).attr("id")); // 에디터 셋팅
						}
					}
			})

		},100)
		
}

function sendFile(file,editor,welEditable) {
	  data = new FormData();
	  data.append("files0", file);
	   $.ajax({
	   url: "/summernote/image_upload.php",
	   data: data,
	   cache: false,
	   contentType: false,
	   processData: false,
	   type: 'POST',
	   success: function(data, msg){  
//		editor.insertImage(welEditable, data);              
		$("#"+editor).summernote('insertImage', data.trim());
		//file 초기화
		$(".form-group .note-v [type=file]").each(function(i){
			$(this).replaceWith($(this).clone(true));				
		})
	},
	   error: function(jqXHR, textStatus, errorThrown) {
	   console.log(textStatus+" "+errorThrown);
	  }
	});
}

function setSummertnoteEditor(editorArea) {
		$("#"+editorArea).summernote({
			height:300,
			minHeight:100,
			lang: 'ko-KR',
			callbacks: {
				onImageUpload: function(files, editor,$editable) {
					if(navigator.userAgent.indexOf('9.0') == -1) {
						for(var i=0;i<files.length;i++){
							sendFile(files[i],editorArea,$editable);
						}
					}
				}
			}
		});
}

function ajaxSummernoteInsertImage(editorArea) {
//비동기 파일 전송
	var frm = $('#'+editorArea).closest("form")[0];
//	alert( frm );
	temp = frm ;
	var action = $(frm).attr("action");
	
	$(frm).attr("action","/summernote/image_upload.php");

	
	//$(frm).append("fileObj", $('#'+editorArea).next().find(".note-v").html());
	$(frm).ajaxForm({
		dataType : 'text' ,
		beforeSubmit : function(){
			
		},
		success : function(json, msg){
//			console.log("msg :: "+json.trim());
			$("#"+editorArea).summernote('insertImage', json.trim());
			//file 초기화
			$(".form-group .note-v [type=file]").each(function(i){
				$(this).replaceWith($(this).clone(true));				
			})
		},
		error : function(xhr, status, exception){
			alert("파일전송에 실패하였습니다. \n"+status);
			return false;
		}
	});
	from_validation = 'editor'; 
	$(frm).submit();
	$(frm).attr("action",action);
	$(frm).off();

	from_validation = ''; 
}

function imageOnchange(obj){
	ajaxSummernoteInsertImage($(obj).parents(".note-editor").prev().attr("id")); 
}

function clinicChangeSelectBox(obj){
	if($("#hospital_fk").size() > 0){
		if($(obj).val() != 0){
			$("#hospital_fk").val($(obj).find("option:selected").attr("id"));
			if($(".schedule").size() > 0){
				calendarNewList($("#hospital_fk").val(), $("#curMonth").val());
			}
		}
	}
}

function timeSelectedValue(obj) {
	//console.log($(obj).data("value"));
	if($(".schedule_time ul li").size() > 0){
		$(".schedule_time ul li").removeClass("choiceTime");
		$(obj).addClass("choiceTime");
		$("#resertime").val($(obj).data("value"));
	}else{
		$("#resertime").val($(obj).val());
	}
	
}

function calendarSelectConDayConfirm(today) {
	$('.calendar_table td').removeAttr("id");
	$('.calendar_table td').filter(function(){
		if( $(this).text().trim() == today.substring(8).trim() ){
			return this;
		}
	}).attr("id", "r_click");
	
	$("#reserdate").val(today);
	$("#resertime").val("");	
	curTime($("#reserdate").val());
}

function timeSelectOnLoadConfirm(today) {
	
	if($("#tmTime").size() > 0){
		if($("#tmTime").val().trim() != ""){
			$("#resertime").val($("#tmTime").val());
			curTime($("#reserdate").val());
		}
		
	}
	
}

function calendarSelectDayConfirm(today) {
	
	<?if($clinic) {?>
	if ($("#clinic_fk").val() == 0) {
		alert('진료과목을 선택해주세요.');
		$("#clinic_fk").focus();
		return false;
	}	
	<?}?>
	<?if(doctor) {?>
	if ($("#doctor_fk").val() == 0) {
		alert('의료진을 선택해주세요.');
		$("#doctor_fk").focus();
		return false;
	}
	<?}?>
	
	$('.calendar_table td').removeAttr("id");
	$('.calendar_table td').filter(function(){
		if( $(this).text().trim() == today.substring(8).trim() ){
			return this;
		}
	}).attr("id", "r_click");
	
	$("#reserdate").val(today);
	$("#resertime").val("");
	reserTimeList($("#hospital_fk").val(),$("#doctor_fk").val(),$("#reserdate").val(),$("#resertime").val(),0);
}


function calendarSelectMonth(month) {
	$("#curMonth").val(month);
}

//진료과목 selectbox 가져오기
function clinicSelectbox(hospital_fk,clinic_fk,type) {
	jQuery("#clinic_fk").load("clinicList.php?hospital_fk="+hospital_fk+"&clinic_fk="+clinic_fk+"&type="+type);
}

// 의료진 selectbox 가져오기
function doctorSelectbox(hospital_fk,clinic_fk,doctor_fk,type) {
	hospital_fk = (hospital_fk == undefined || hospital_fk == "") ? 0 : hospital_fk; 
	clinic_fk = (clinic_fk == undefined || clinic_fk == "") ? 0 : clinic_fk; 
	jQuery("#doctor_fk").load("doctorList.php?hospital_fk="+hospital_fk+"&clinic_fk="+clinic_fk+"&doctor_fk="+doctor_fk+"&type="+type);
}

// 의료진 사진 가져오기
function doctorPhoto(hospital_fk,clinic_fk) {
	jQuery("#doctorPhoto").load("doctorPhoto.php?hospital_fk="+hospital_fk+"&clinic_fk="+clinic_fk);
}

// 예약달력 가져오기
function calendarNewList(shospital_fk, smonth) {
	$.get("calendarList.php?shospital_fk="+shospital_fk+"&smonth="+smonth,function(data){
		$(".schedule").html(data);
		console.log(data);
		
		timeSelectOnLoadConfirm($("#reserdate").val());
		


		if($("[name=p_reserdate]").size() > 0){
			$("#reserdate").val($("[name=p_reserdate]").val())
			$("#resertime").val($("[name=p_resertime]").val())
			reserTimeList($("#hospital_fk").val(),$("#doctor_fk").val(),$("#p_reserdate").val(),$("#p_resertime").val(),0);
		}
		
		
	})
	//jQuery(".schedule").load();
}

// 예약시간 가져오기
function reserTimeList(hospital_fk,doctor_fk,reserdate,resertime,times) {
	jQuery(".schedule_time").load("timeList.php?hospital_fk="+hospital_fk+"&doctor_fk="+doctor_fk+"&reserdate="+reserdate+"&resertime="+resertime+"&times="+times);
}


function telTimeList(selectDay,type) {
	jQuery("#curTimeList").load("curTimeList.php?type="+type+"&selectDay="+selectDay);
}

// 전화 상담 시간 가져오기
function curTime(selectDay){
	$.get("curTimeList.php?selectDay="+selectDay,function(data){
		$(".schedule_time").html(data);
		
		if($("#tmTime").size() > 0){
			timeSelectedValue($("#"+$("#tmTime").val().replace(/:/,"")));
		}
		
	})
	
}

</script>

<script>
$(function () {
	
    var obj = $("#dropzone");
    <?if($useFileDrag){ ?>
 //   FileUploadInit();
    <?}?>
    obj.on('dragenter', function (e) {
         e.stopPropagation();
         e.preventDefault();
         $(this).css('border', '2px solid #5272A0');
    });

    obj.on('dragleave', function (e) {
         e.stopPropagation();
         e.preventDefault();
         $(this).css('border', '2px dotted #8296C2');
    });

    obj.on('dragover', function (e) {
         e.stopPropagation();
         e.preventDefault();
    });

    obj.on('drop', function (e) {
         e.preventDefault();
         $(this).css('border', '2px dotted #8296C2');

         var files = e.originalEvent.dataTransfer.files;

         if(files.length < 1)
			return;
		
         F_FileMultiUpload(files, obj);
    });

});

var fileList = "";     
var fileCnt  = 0;      //파일갯수
var loading  = false;
//파일 지우기
function FileDelete(filename, that){
	
		var Deletedata	 = new FormData();
		$.ajax({
			url 	    : "process_file.php?cmd=delete&filename="+filename,
			method 	    : 'post',
			data        : Deletedata,
			dataType    : 'json',
			processData : false,
			contentType : false,
			success : function(res) {
				
				text = "<label>"+$(that).parent().html()+"</label>";
				var fileList2 = $("#dropzone").html();
				
				fileList2 = fileList2.replace(text, "");
				fileList = fileList2;
				fileCnt--;
				 
				//alert(fileList.indexOf('<label>'));
				if(fileList2.indexOf('<label>') == -1){
					$("#dropzone").html("파일을 올려주세요.");
				} else {
					$("#dropzone").html(fileList2);
				}
			},
			error : function(data) {
				//alert("error");
			}
		}); 
		
		//alert(fileList);
	
}
//파일 업로드
function F_FileMultiUpload(files, obj) {
	
	var data	 = new FormData();
	data.append("cmd", "update");
	//확장자 체크 변수 :[ true -> 사용가능 확장자 false -> 사용 불가능  확장자]
	var chkFile  = false;
	//파일 갯수 체크
	var maxCnt   = <? echo $useFileCount ?>;
	
	if (files.length <= maxCnt && fileCnt + files.length <= maxCnt){
		//확장자 체크
		//var selection = [ ".txt", ".jpg", ".png", ".xls", ".gif"];
		
		
		for (var i = 0; i < files.length; i++) {
			data.append('filenames[]', files[i]);

		}
		
		
		if (!loading) {
			//console.log("data 1  :::"+data.getAll('filenames1'));
			loading = true;
			
			$.ajax({
				url 	    : "process_file.php?cmd=write",
				method 	    : 'post',
				data        : data,
				dataType    : 'json',
				processData : false,
				contentType : false,
				success : function(res) {
					//alert(res);
					//alert(res.filename);
					loading = false;
					var filename2 = res.filename;
				
					var filenameArray = filename2.split(",");
				
				
					fileList += "<font style='color:black; font-weight:bold '>"
					for (var i = 0; i < files.length; i++) {
						//alert(data.get('filenames1').name);
						fileList += "<label>["+files[i].name+"<a onclick='javascript:FileDelete(\""+filenameArray[i]+"\", this)'> <img src='/img/ico_cancel.jpg' width='10px' style='vertical-align:initial;'> </a>], </label>";
						fileCnt++;
					}
					
					fileList += "</font>";
					
					$(obj).html(fileList);
				},
				error : function(data) {
					//alert("error");
				}
			}); 
		}
		else {
			alert("파일 등록중입니다.");
		}
	}
	else {
		alert("최대 "+maxCnt+"개의 파일만 첨부 가능합니다.")
	}
}
function FileUploadInit(){
	var Initdata	 = new FormData();
	$.ajax({
		url 	    : "process_file.php?cmd=init",
		method 	    : 'post',
		data        :  Initdata,
		dataType    : 'json',
		processData : false,
		contentType : false,
		success : function(res) {
			//alert(res);
		},
		error : function(data) {
			//alert("error");
		}
	}); 
}
// 파일 멀티 업로드 Callback
function F_FileMultiUpload_Callback(files) {
	for (var i = 0; i < files.length; i++)
		console.log(files[i].file_nm + " - " + files[i].file_size);
}
window.onbeforeunload = function(){
	//서브밋할 경우 안탐
	if(!isSubmitChk){
		FileUploadInit();
	}
}
</script>
<style>
    #dropzone
    {
        border:2px dotted #3292A2;
        width:90%;
        height:50px;
        text-align:center;
        font-size:14px;
        padding-top:12px;
        margin-top:10px;
    }
</style>