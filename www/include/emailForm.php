<?
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
?>
<html>
<head>
<style>
	#noImg {
		width:194px;
		height:50px;
		text-align: center;
		border:1px solid red;
		font-size: 30px;
		padding-top:30px;
	}
</style>
<title></title>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
</head>
<body leftmargin='0' topmargin='0'>
<table border='0' cellpadding='0' cellspacing='0' width='100%' class="eail_f">
<tr>
	<td height='15'></td>
</tr>
<tr>
	<td>
		<table border='1' cellpadding='0' cellspacing='1' width='100%' bordercolor="#323337" style="border:1px solid #323337; margin:0 auto;">
		<tr>
			<td>
				<table border='0' cellpadding='0' cellspacing='0' width='100%' bgcolor='#FFFFFF' style="margin:0 auto;">
				<tr>
					<td height='110' align="center">
						<Table width='90%' height='110' cellspacing='0' cellpadding='0' border='0' align='center' style="width:90%; height:110px; margin:0 auto;">
						<tr>
							<td height='30'></td>
						</tr>
						<tr hdight="80">
							<td width="282" height='80' align='left' valign='top'>
								<a href="<?=COMPANY_URL?>" target="_brank">
									<img src="<?=COMPANY_URL?>/manage/img/email_logo.jpg" style="max-height:80px;">
								</a>
							</td>
							<td width='45px' height='80'></td>
							<td align='left' height='80' valign='bottom'>
								<table border='0' cellpadding='0' cellspacing='0'>
								<tr>
									<td><font face='Noto Sans KR' style="font-size:14px;" color="#222"><b><?=COMPANY_NAME?></b></font></td>
								</tr>
								<tr>
									<td><font face='Noto Sans KR, sans-serif' color='#444' style="font-size:12px;">URL. <a href="<?=COMPANY_URL?>" target="_brank" color="eail_lcolor"><?=COMPANY_URL?></a> <span style="color:#d7d7d7">|</span>
										TEL. <?=COMPANY_TEL?>  <? if(!COMPANY_FAX){ ?>|  FAX. <?=COMPANY_FAX?><? } ?></font>
									</td>
								</tr>
								<tr>
									<td><font face='Noto Sans KR, sans-serif' color='#444' style="font-size:12px;">ADDR. <?=COMPANY_ADDR?></font></td>
								</tr>
								</table>
							</td>
						</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height='20'></td>
				</tr>
				<tr>
					<td height='1' bgcolor='#d7d7d7' style="font-size:0;"></td>
				</tr>
				<tr>
					<td height='5'></td>
				</tr>
				<tr>
					<td width='90%' align="center">
						<Table width='90%' cellspacing='0' cellpadding='0' border='0' align='center' height='500' style="width:90%; min-height:100px;">
						<tr>
							<td height=20></td>
						</tr>
						<tr>
							<td valign='top' height=20><strong>:SUBJECT</strong></span>
							</td>
						</tr>
						<tr>
							<td height=10></td>
						</tr>
						<tr>
							<td height='100%' valign='top' height=20><font size='2'>:CONTENT</font></td>
						</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height='20'></td>
				</tr>
				<tr>
					<td height="60" align='center' bgcolor="#fcfcfc"><font face='Noto Sans KR, sans-serif' color='#666' style="font-size:16px;">Copyright  <?=CREATE_YEAR?>.  <?=COMPANY_URL?>   All Rights Reserved</font></td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
</body>
</html>