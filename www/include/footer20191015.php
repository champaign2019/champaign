<footer>
    <div class="footer-top mobile-pedding">
        <div class="container-1">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3">
                    <a href="/"><img src="/images/footer/footer-logo.svg" class="img-fluid" alt="Bravecto"></a>
                    <ul class="socali-links">
                        <li><a href="https://www.facebook.com/pages/Bravecto-AU/311090325756499" title="Facebook" target="_blank"><img src="/images/icons/facebook.svg" alt="facebook"></a></li>
                        <li><a href="https://www.instagram.com/bravectoau/" title="Instagram" target="_blank"><img src="/images/icons/instagram.svg" alt="Instagram"></a></li>
                        <li><a href="https://www.youtube.com/channel/UCMZUxH12RqKZjQlqZRDVmUQ/featured" title="Youtube" target="_blank"><img src="/images/icons/youtube.svg" alt="Youtube"></a></li>
                    </ul>
                </div>


                <div class="col-lg-2 col-md-2 col-sm-2">
                    <ul class="footer-links">
                        <li>
                            <h6>
                                <a href="/page/about-us.php" title="About Bravecto">About Bravecto</a>
                            </h6>
                            <a href="/page/parasites.php" title="Beat the Parasites">Meet the bad guys</a>
                            <a href="/page/chew-for-dogs.php" title="Bravecto Chew for Dogs">Bravecto Chew for Dogs</a>
                            <a href="/page/spot-on-dogs.php" title="Bravecto Spot-on for Dogs">Bravecto Spot-on for Dogs</a>
                            <a href="/page/spot-on-cats.php" title="Bravecto Spot-on for Cats">Bravecto Spot-on for Cats</a>
                            <a href="/page/plus-cats.php" title="Bravecto Spot-on Plus for Cats">Bravecto <span class="plus">Plus</span> for Cats</a>
                            <a href="/page/benefits.php" title="Benefits">Benefits</a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <ul class="footer-links">
                        <li>
                            <h6><a href="/page/vets-info.php" title="Bravecto for Vets">Bravecto for Vets</a></h6>
                            <a href="/page/vets-resources.php" title="Vet Resources">Vet Resources</a>
                        </li>
                    </ul>
                </div>
                <div class="co-lg-2 col-md-2 col-sm-2">
                    <ul class="footer-links">
                        <li>
                            <h6><a href="/page/contact-us.php" title="Contact Us">Contact Us</a></h6>
                            <a href="/page/stockists.php" title="Find a Stockists">Find a Stockist</a>
                        </li>
                    </ul>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-3">
                    <ul class="footer-links-2">
                        <li>
                            <a href="http://www.msd-animal-health.com.au/privacy.aspx" target="_blank" title="Privacy Policy">Privacy Policy</a>
                           
                            <a href="/page/terms.php" title="Terms and Conditions">Terms and Conditions</a>
                        </li>
                    </ul>
                </div>


            </div>
        </div>
        <div class="left-logo"><img src="/images/footer/footer-left-logo.png" alt="MSD Animal Health"></div>
    </div>
    <div class="footer-bottom">
        <div class="container-1">
            <div class="row">
                <div class="col-sm-12 col-md-3 col-lg-4"></div>
                <div class="col-sm-12 col-md-9 col-lg-8">
                    <p>
                        <a href="https://portal.apvma.gov.au/pubcris;jsessionid=AjAebiai95var7phRx1L2QBS?p_auth=RBZ2QvsL&p_p_id=pubcrisportlet_WAR_pubcrisportlet&p_p_lifecycle=1&p_p_state=normal&p_p_mode=view&p_p_col_id=column-1&p_p_col_pos=2&p_p_col_count=4&_pubcrisportlet_WAR_pubcrisportlet_javax.portlet.action=search" target="_blank" title="Refer to product label for full claim details.">*Refer to product label for full claim details.</a><br>
                        <br>

                        Copyright &copy; 2018 Intervet International B.V. All rights reserved. <br>
                        BRAVECTO &reg; is a Registered Trademark of Intervet International B.V., The Netherlands. Content intended for Australian audiences only.
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--~///////////////// Mobile Menu start /////////////////-->
<div id="mobilemenu">
    <div class="mobile-header">
        <a href="/" title="Bravecto"><img src="/images/footer/footer-logo.svg" width="120" alt="Bravecto"></a>
        <a href="#" title="close menu" class="closmenu"><img src="/images/icons/menu-close.svg" width="45" alt="close menu"></a>
    </div>
    <div class="menumobile">
        <ul class="nav flex-column">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" title="PRODUCTS" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">브라벡토소개</a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="/page/chew-for-dogs.php" title="Bravecto Chew for Dogs">Bravecto Chew for Dogs</a>
                    <a class="dropdown-item" href="/page/spot-on-dogs.php" title="Bravecto Spot on for Dogs">Bravecto Spot on for Dogs</a>
                    <a class="dropdown-item" href="/page/spot-on-cats.php" title="Bravecto Spot on for Cats">Bravecto Spot on for Cats</a>
                    <a class="dropdown-item" href="/page/plus-for-cats.php" title="Bravecto Plus for Cats">Bravecto Spot on <span class="plus">Plus</span> for Cats</a>
                </div>
            </li>
            <li class="nav-item"> <a class="nav-link " href="/page/benefits.php" title="Benefits">브라벡토 장점</a> </li>
            <li class="nav-item"> <a class="nav-link " href="/page/parasites.php" title="Parasites">외부기생충</a> </li>
            <li class="nav-item dropdown vetlink">
                <a class="nav-link dropdown-toggle" href="/page/vets-info.php" title="Vet Info" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" onclick="location.href='/page/vets-info.php';">수의학적 정보</a>
                <div class="dropdown-menu"> <a class="dropdown-item" href="/page/vets-resources.php" title="Vet resources">Vet resources</a></div>
            </li>
            <li class="nav-item dropdown vetlink">
                <a class="nav-link dropdown-toggle" href="/page/contact-us.php" title="CONTACT US" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" onclick="location.href='/page/contact-us.php';">고객문의</a>
                <div class="dropdown-menu"> <a class="dropdown-item" href="/page/about-us.php" title="ABOUT US">ABOUT US</a></div>
            </li>
        </ul>
    </div>
    <div class="menufooter"> <a href="/page/stockists.php" title="Find a stockists">판매점 찾기</a> </div>
</div>

<!--~///////////////// Mobile Menu End /////////////////-->
<script src="https://code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" crossorigin="anonymous"></script>

<script src="/js/owl.carousel.js"></script>
<script src="/js/easing.min.js"></script>
<script src="/js/sticky-kit.min.js"></script>
<script src="/js/custom.js"></script>
<script src="/js/wow.js"></script>
<script src="/js/viewportchecker.js"></script>
<script src="/js/tracking.js"></script>
<script src="/js/ekko-lightbox.js"></script>

    

    <script>
        wow = new WOW(
            {
                animateClass: 'animated',
                offset: 100
            }
        );
        wow.init();

    </script>

    <!-- Facebook Pixel Code -->
    <script>
        !function (f, b, e, v, n, t, s) {
            if (f.fbq) return; n = f.fbq = function () {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            }; if (!f._fbq) f._fbq = n;
            n.push = n; n.loaded = !0; n.version = '2.0'; n.queue = []; t = b.createElement(e); t.async = !0;
            t.src = v; s = b.getElementsByTagName(e)[0]; s.parentNode.insertBefore(t, s)
        }(window,
            document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1765867277065388'); // Insert your pixel ID here.
        fbq('track', 'PageView');
    </script>
    <noscript>
        <img height="1" width="1" style="display:none"
             src="https://www.facebook.com/tr?id=1765867277065388&ev=PageView&noscript=1" />
    </noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->