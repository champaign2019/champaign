<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Popup.class.php";

include $_SERVER['DOCUMENT_ROOT']."/include/popup/config.php";
include $_SERVER['DOCUMENT_ROOT']."/include/popup/userAgent.php";

$popup = new Popup($pageRows, $tablename, $_REQUEST);
$rowPageCount = $popup->getCount($_REQUEST);
$resultPopup = rstToArray($popup->getMainList());
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<script type="text/javascript" src="/js/function.js"></script>
<script type="text/javascript">
  var ns4=document.layers;
  var ie4=document.all;
  var ns6=document.getElementById&&!document.all;
  var zCount = 99999;
 
</script>

<?		
for($i = 0; $i < count($resultPopup); $i++) {
$rowPopup = $resultPopup[$i];

// add_m : _m이 들어갈 변수
$add_m = "";
/*
device가 mobile이면서 popup_device_type이 mobile전용(1) 혹은 PC&Mobile 각각(3)일 경우에 모바일 전용 컬럼을 가져오기 위해 _m을 붙인다.
*/
if ( ($resultMobile && $rowPopup['popup_device_type'] == 1) || ($resultMobile && $rowPopup['popup_device_type'] == 3) ) {
	$add_m = "_m";
}
?>
<script type="text/javascript">

	var	crossobj<?=$i?>;
	var	dragapproved<?=$i?>;

function drag_drop<?=$i?>(e){
	
	if (ie4&&dragapproved<?=$i?>){
		crossobj<?=$i?>.style.left=tempx+event.clientX-offsetx+"px";
		crossobj<?=$i?>.style.top=tempy+event.clientY-offsety+"px";
		return false;
	} else if (ns6&&dragapproved<?=$i?>){
		crossobj<?=$i?>.style.left=tempx+e.clientX-offsetx+"px";
		crossobj<?=$i?>.style.top=tempy+e.clientY-offsety+"px";
		return false;
	}
}

 function initializedrag<?=$i?>(e){
    crossobj<?=$i?>=ns6? document.getElementById("showimage<?=$i?>") : document.all.showimage<?=$i?>

    var firedobj=ns6? e.target : event.srcElement;
    var topelement=ns6? "HTML" : "BODY";

    while (firedobj.tagName!=topelement&&firedobj.id!="dragbar<?=$i?>"){
      firedobj=ns6? firedobj.parentNode : firedobj.parentElement;
    }

    if (firedobj.id=="dragbar<?=$i?>"){
      offsetx=ie4? event.clientX : e.clientX;
      offsety=ie4? event.clientY : e.clientY;

      tempx=parseInt(crossobj<?=$i?>.style.left);
      tempy=parseInt(crossobj<?=$i?>.style.top);

      dragapproved<?=$i?>=true;
      document.onmousemove=drag_drop<?=$i?>;
    }
  }    

  function initDrags<?=$i?>() {
		
	  zCount++;
	  document.onmousedown=initializedrag<?=$i?>;
	  document.onmouseup=new Function("dragapproved<?=$i?>=false");
	  document.getElementById("showimage<?=$i?>").style.zIndex = zCount;

  }
</script>


<?

	$tempContents = $rowPopup['contents'.$add_m];
	$detailBtn = "";

	if($rowPopup['relation_url'.$add_m]) {
		$detailBtn = "<img src=\"/img/btn_detail.gif\" alt='상세보기' align=\"absmiddle\" style=\"border:0; margin:2 0 0 0;  text-align:right; cursor:hand;\" onclick=\"window.open('".cvtHttp($rowPopup['relation_url'.$add_m])."','_blank','height=600,width=800,top=50,left=50,toolbar=1, directories=1, status=1, menubar=1, scrollbars=1, resizable=1,location=1')\">&nbsp;";
	}

	if ($rowPopup['type'] == "1") {
		$tempContents = "<img src=\""."/upload/popup/".$rowPopup['imagename'.$add_m]."\" alt='".$rowPopup['image_alt'.$add_m]."' style=\"border:0;\" style=\"cursor:hand;\" ";
		if($rowPopup['relation_url'.$add_m]) {
			$tempContents .= "onclick=\"window.open('".cvtHttp($rowPopup['relation_url'.$add_m])."','_blank','height=600,width=800,top=50,left=50,toolbar=1, directories=1, status=1, menubar=1, scrollbars=1, resizable=1,location=1')\"";
		}
		$tempContents .= ">";
		$detailBtn = "";
	}

	if ($rowPopup['type'] == "0" || $rowPopup['type'] == "1") {
?>
	<div id="showimage<?=$i?>" style="position:absolute;left:<?=$rowPopup['area_left'.$add_m]?>px;top:<?=$rowPopup['area_top'.$add_m]?>px;z-index:999999;width:<?=$rowPopup['popup_width'.$add_m]?>px; height:<?=$rowPopup['popup_height'.$add_m]?>px;">
	<? $border = $rowPopup['type'] == "1" ? "" : "border:".$rowPopup['border_color'.$add_m]." 5px solid; ";?>

		<div id="divPop<?=$rowPopup['no']?>">
			<div id="dragbar<?=$i?>" onmouseDown="initDrags<?=$i?>();"  style="<?=$border?> background:#fff; width:auto; height:auto; overflow:hidden;">
				<div id="popMain<?=$rowPopup['no']?>">
				
					<p><?=$tempContents?></p>
					<p><?=$detailBtn?></p>
				</div>
			</div>
			<div style="background:<?=$rowPopup['type'] == "1" ? $rowPopup['bg_color'.$add_m] : $rowPopup['border_color'.$add_m] ?>; color:#fff; vertical-align:middle; text-align:right; padding:3px 10px;">
				<input type="checkbox" id="chkbox<?=$rowPopup['no']?>" onclick="closeLayer('showimage<?=$i?>', this, '1','showimage<?=$i?>');"/>오늘 하루 이 창을 열지 않음
				<a href="javascript:closeLayer('divPop<?=$rowPopup['no']?>', getObject('chkbox<?=$rowPopup['no']?>'), '1','showimage<?=$i?>');" style="color:#fff;">[닫기]</a>
			</div>
		</div>
	</div>
	<SCRIPT type="text/javascript">
	//<!--
	startTime('showimage<?=$i?>', 'popMain<?=$rowPopup["no".$add_m]?>', '<?=$rowPopup["area_top".$add_m]?>', '<?=$rowPopup["area_left".$add_m]?>', '<?=$rowPopup["popup_width".$add_m]?>', '<?=$rowPopup["popup_height".$add_m]?>', '1');
	//-->
	</SCRIPT>
<?
	} else {
?>
	<SCRIPT type="text/javascript">

	//<!--
	if ( !getCookie("divPop<?=$rowPopup['no']?>")) {     
		window.open('/include/popup/popup.php?no=<?=$rowPopup["no"]?>','divPop<?=$rowPopup["no"]?>','width=<?=$rowPopup["popup_width".$add_m]?>, height=<?=$rowPopup["popup_height".$add_m]+20?>, top=<?=$rowPopup["area_top".$add_m]?> , left=<?=$rowPopup["area_left".$add_m]?> ,toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no');
	 } 
	//-->
	</SCRIPT>
<?
	}
 }  ?>

