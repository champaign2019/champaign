<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Popup.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

$popup = new Popup($pageRows, $tablename, $category_tablename, $_REQUEST);
$rowPageCount = $popup->getCount($_REQUEST);
$result = rstToArray($popup->getMainList());
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<script type="text/javascript" src="/js/function.js"></script>
<script type="text/javascript">
  var ns4=document.layers;
  var ie4=document.all;
  var ns6=document.getElementById&&!document.all;
  var zCount = 99999;
 
</script>

<?		
for($i = 0; $i < count($result); $i++) {
$row = $result[$i];
?>
<script type="text/javascript">

	var	crossobj<?=$i?>;
	var	dragapproved<?=$i?>;

function drag_drop<?=$i?>(e){
	
	if (ie4&&dragapproved<?=$i?>){
		crossobj<?=$i?>.style.left=tempx+event.clientX-offsetx+"px";
		crossobj<?=$i?>.style.top=tempy+event.clientY-offsety+"px";
		return false;
	} else if (ns6&&dragapproved<?=$i?>){
		crossobj<?=$i?>.style.left=tempx+e.clientX-offsetx+"px";
		crossobj<?=$i?>.style.top=tempy+e.clientY-offsety+"px";
		return false;
	}
}

 function initializedrag<?=$i?>(e){
    crossobj<?=$i?>=ns6? document.getElementById("showimage<?=$i?>") : document.all.showimage<?=$i?>

    var firedobj=ns6? e.target : event.srcElement;
    var topelement=ns6? "HTML" : "BODY";

    while (firedobj.tagName!=topelement&&firedobj.id!="dragbar<?=$i?>"){
      firedobj=ns6? firedobj.parentNode : firedobj.parentElement;
    }

    if (firedobj.id=="dragbar<?=$i?>"){
      offsetx=ie4? event.clientX : e.clientX;
      offsety=ie4? event.clientY : e.clientY;

      tempx=parseInt(crossobj<?=$i?>.style.left);
      tempy=parseInt(crossobj<?=$i?>.style.top);

      dragapproved<?=$i?>=true;
      document.onmousemove=drag_drop<?=$i?>;
    }
  }    

  function initDrags<?=$i?>() {
		
	  zCount++;
	  document.onmousedown=initializedrag<?=$i?>;
	  document.onmouseup=new Function("dragapproved<?=$i?>=false");
	  document.getElementById("showimage<?=$i?>").style.zIndex = zCount;

  }
</script>


<?

	$tempContents = $row['contents'];
	$detailBtn = "";

	if($row['relation_url']) {
		$detailBtn = "<img src=\"/img/btn_detail.gif\" alt='상세보기' align=\"absmiddle\" style=\"border:0; margin:2 0 0 0;  text-align:right; cursor:hand;\" onclick=\"window.open('".cvtHttp($row['relation_url'])."','_blank','height=600,width=800,top=50,left=50,toolbar=1, directories=1, status=1, menubar=1, scrollbars=1, resizable=1,location=1')\">&nbsp;";
	}

	if ($row['type'] == "1") {
		$tempContents = "<img src=\""."/upload/popup/".$row['imagename']."\" alt='".$row['image_alt']."' style=\"border:0;\" style=\"cursor:hand;\" ";
		if($row['relation_url']) {
			$tempContents .= "onclick=\"window.open('".cvtHttp($row['relation_url'])."','_blank','height=600,width=800,top=50,left=50,toolbar=1, directories=1, status=1, menubar=1, scrollbars=1, resizable=1,location=1')\"";
		}
		$tempContents .= ">";
		$detailBtn = "";
	}

	if ($row['type'] == "0" || $row['type'] == "1") {
?>
	<div id="showimage<?=$i?>" style="position:absolute;left:<?=$row['area_left']?>px;top:<?=$row['area_top']?>px;z-index:999999;width:<?=$row['popup_width']?>px; height:<?=$row['popup_height']?>px;">
	<? $border = $row['type'] == "1" ? "" : "border:".$row['border_color']." 5px solid; ";?>

		<div id="divPop<?=$row['no']?>">
			<div id="dragbar<?=$i?>" onmouseDown="initDrags<?=$i?>();"  style="<?=$border?> background:#fff; width:auto; height:auto; overflow:hidden;">
				<div id="popMain<?=$row['no']?>">
				
					<p><?=$tempContents?></p>
					<p><?=$detailBtn?></p>
				</div>
			</div>
			<div style="background:<?=$row['type'] == "1" ? $row['bg_color'] : $row['border_color'] ?>; color:#fff; vertical-align:middle; text-align:right; padding:3px 10px;">
				<input type="checkbox" id="chkbox<?=$row['no']?>" onclick="closeLayer('showimage<?=$i?>', this, '1','showimage<?=$i?>');"/>오늘 하루 이 창을 열지 않음
				<a href="javascript:closeLayer('divPop<?=$row['no']?>', getObject('chkbox<?=$row['no']?>'), '1','showimage<?=$i?>');" style="color:#fff;">[닫기]</a>
			</div>
		</div>
	</div>
	<SCRIPT type="text/javascript">
	//<!--
	startTime('showimage<?=$i?>', 'popMain<?=$row[no]?>', '<?=$row[area_top]?>', '<?=$row[area_left]?>', '<?=$row[popup_width]?>', '<?=$row[popup_height]?>', '1');
	//-->
	</SCRIPT>
<?
	} else {
?>
	<SCRIPT type="text/javascript">

	//<!--
	if ( !getCookie("divPop<?=$row['no']?>")) {     
		window.open('/include/popup/popup.php?no=<?=$row[no]?>','divPop<?=$row[no]?>','width=<?=$row[popup_width]?>, height=<?=$row[popup_height]+20?>, top=<?=$row[area_top]?> , left=<?=$row[area_left]?> ,toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no');
	 } 
	//-->
	</SCRIPT>
<?
	}
 }  ?>

