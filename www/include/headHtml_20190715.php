<?php include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";?>
<?php include_once $_SERVER['DOCUMENT_ROOT']."/lib/member/Member.class.php";?>
<!-- /** [version:v1.0.3 update:18.10.15] **/ -->
<?
include_once $_SERVER['DOCUMENT_ROOT']."/lib/global/Message_global.php";
?>
<title><?=COMPANY_NAME?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1.0" />
<meta name="format-detection" content="telephone=no">
<meta name="description" content="페이지설명" />
<meta name="keywords" content="검색키워드등록" />
<meta name="classification" content="검색사이트 카테고리 등록" />

<link rel="stylesheet" href="/css/basic.css" type="text/css" media="all" />
<link rel="stylesheet" href="/css/board.css" type="text/css" media="all" />
<link rel="stylesheet" href="/css/layout.css" type="text/css" media="all" />
<link rel="stylesheet" href="/css/notosanskr.css" type="text/css" media="all" />
<link rel="stylesheet" href="/css/content.css" type="text/css" media="all" />
<link rel="stylesheet" href="/css/jquery.datetimepicker.css" type="text/css" media="all" />
<link rel="stylesheet" href="/css/gap.css" type="text/css" media="all" />
<!-- <link rel="stylesheet" href="/css/gap_mobile.css" type="text/css" media="all" /> -->


<!--[if lt IE 9]>
<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<meta http-equiv="X-UA-Compatible" content="IE=9" />
<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
<![endif]-->

<script type="text/javascript" src="/js/function.js?ver=1"></script>
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/calendar_beans_v2.0.js"></script>
<script type="text/javascript" src="/js/jquery-1.8.0.min.js"></script>
<script type="text/javascript" src="/js/dev.js"></script>
<script type="text/javascript" src="/js/vizenweblog.js"></script>
<!-- aos 동적효과 --> 
<script type="text/javascript" src="/js/aos.js"></script>
<link rel="stylesheet" href="/css/aos.css" type="text/css" media="all" />
<script>
		AOS.init({
		});
</script>

<!-- typing 효과 -->
<!-- 사용시 활성화 및 해당 주석 삭제 -->
<!-- 사용법은 textEffectBase.js 참고해주세요 -->
<script type="text/javascript" src="/js/textEffect.js"></script>
<script type="text/javascript" src="/js/textEffectBase.js" charset="utf-8"></script>


<!-- 달력 변경 jquery.js 버젼업, datetimepicker.full.js 달력 js -->
<script src="/js/jquery.js"></script>
<script src="/js/jquery.datetimepicker.full.js"></script>
<!-- 달력 변경 끝 -->
<!--<script src="/js/zoom.js"></script>-->
<!--반응형사이트 아닐경우 활성화-->


<!-- summernote editor-->

<link href="/summernote/bootstrap-3.3.2-dist/css/bootstrap.css" rel="stylesheet">
<link href="/summernote/summernote.css" rel="stylesheet">

<script src="/summernote/bootstrap-3.3.2-dist/js/bootstrap.js"></script> 
<script type="text/javascript" src="/js/jquery.form.js"></script>
<script src="/summernote/summernote.js"></script>
<script src="/summernote/lang/summernote-ko-KR.js"></script>

<!-- include summernote css/js-->

<!-- bx슬라이더 -->
<script type="text/javascript" src="/js/jquery.bxslider.min.js"></script>


<!-- 슬릭슬라이더 -->
<link rel="stylesheet" href="/css/slick.css" type="text/css" media="all" />
<link rel="stylesheet" href="/css/slick-theme.css" type="text/css" media="all" />
<script src="/js/slick.js" type="text/javascript" charset="utf-8"></script>
<script src="/js/slick.min.js" type="text/javascript" charset="utf-8"></script>




<!-- <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script> -->
<script type="text/javascript" src="/js/function_jquery.js"></script>

<script type="text/javascript" src="/smarteditor/js/HuskyEZCreator.js"></script>
<script type="text/javascript" src="/js/jquery.colorPicker.js"></script>
<script type='text/javascript' src='/js/validationBind.js'></script> 



<?
	unset( $_SESSION['SESSIONKEY'] );

	$hour		= date("H");
	$minute		= date("i");
	$second		= date("s");


	$_SESSION['SESSIONKEY'] = time();			// Return current Unix timestamp
?>



<script>
$(window).load(function(){
	
	if($(parent.frames['mainFrame']).size() > 0){
		
		//$("[name=mainFrame]",parent.document).attr("src",$(parent.frames['mainFrame'].document).get(0).URL);	
	}	
})


var commonhour = '<?=$hour?>';
var commonminute = '<?=$minute?>';
var commonsecond = '<?=$second?>';

var key = '<?=$_SESSION["SESSIONKEY"]?>';
</script>


<?
	if ( SSL_USE ) {
		if ( empty($GLOBALS['memberSession']) && $_SESSION['member_no'] ) {
			Member::setMemberSession($_SESSION);
		}
	}
//	include_once $_SERVER['DOCUMENT_ROOT']."/include/getLoginSession.php";
?>
<script type="text/javascript" src="/js/jQ.js"></script>