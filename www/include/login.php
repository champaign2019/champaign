<?session_start();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/member/Member.class.php";
?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<? include $_SERVER['DOCUMENT_ROOT']."/include/logingCheck.php"; ?>
<? $pageTitle = "로그인"; ?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/include/headHtml.php"; ?>
</head>
<body>
<?
$url = $_REQUEST['url'];			// 전달 받은 주소
$param = $_REQUEST['param'];		// 전달 받은 파라메터

// 파라메터가 있을 경우에는 url 뒷에 결합한다.
if ($param) {
	$url = $url."?".$param;
}

if(($_REQUEST['from'] == 'google') || ($_REQUEST['from'] == 'naver') || ($_REQUEST['from'] == 'kakao') || ($_REQUEST['from'] == 'facebook')) {
	
//	$data = $member->loginMember($_REQUEST['id']);
	$_SESSION['member_id'] = $_REQUEST['from']."_".$data['id'];
	$_SESSION['member_password'] = $data['id'];
	$_SESSION['member_name'] = $data['name'];
	$_SESSION['member_email'] = $data['email'];
	$_SESSION['member_sns'] = 'snsLogin';
	$_SESSION['member_snsType'] = '1';
	echo returnURL($url);
} else {
	$member = new Member(999, 'member', $_REQUEST);
	$cnt = $member->checkLogin($_REQUEST['id'], $_REQUEST['password']);
	if ($cnt == 1) {
		$data = $member->loginMember($_REQUEST['id']);

		if(DORMANT_TYPE == 1) {
			if($data['dormant_state'] > 0) {
			//휴면계정일때
			$data = $member->dormantInfo($_REQUEST['id']);
				$_SESSION['human_no'] = $data['no'];
				$_SESSION['human_id'] = $data['id'];
				$_SESSION['human_name'] = $data['name'];
				$_SESSION['human_email'] = $data['email'];
				$_SESSION['human_cell'] = $data['cell'];
				$_SESSION['human_tel'] = $data['tel'];
				echo returnURL("/member/login_human.php");

			} else {
			$member->updateLogin($data);
				$_SESSION['member_no'] = $data['no'];
				$_SESSION['member_id'] = $data['id'];
				$_SESSION['member_name'] = $data['name'];
				$_SESSION['member_email'] = $data['email'];
				$_SESSION['member_cell'] = $data['cell'];
				$_SESSION['member_tel'] = $data['tel'];
				if ( SSL_USE ) {
					Member::setMemberSession($_SESSION);
				}
				echo returnURL(substr(COMPANY_URL, 0, strlen(COMPANY_URL)-1).$url);
			}

		} else {
			$_SESSION['member_no'] = $data['no'];
			$_SESSION['member_id'] = $data['id'];
			$_SESSION['member_name'] = $data['name'];
			$_SESSION['member_email'] = $data['email'];
			$_SESSION['member_cell'] = $data['cell'];
			$_SESSION['member_tel'] = $data['tel'];
			if ( SSL_USE ) {
				Member::setMemberSession($_SESSION);
			}
			echo returnURL(substr(COMPANY_URL, 0, strlen(COMPANY_URL)-1).$url);
		}
	} else if($cnt == 2){
		echo returnHistory("탈퇴한 회원입니다.");
	}else {
		echo returnHistory("아이디 비밀번호를 확인해주세요.");
	}
}
?>
</body>
</html>