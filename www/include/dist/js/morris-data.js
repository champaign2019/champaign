/*Morris Init*/
//$(function() {
//	"use strict";
var lineExConfig = function(arrObj) {
    if($('#morris_area_chart').length > 0)
		// Area Chart
		Morris.Area({
			element: 'morris_area_chart',
			data: [{
				period: '2010 Q1',
				iphone: 2666,
				ipad: null,
				itouch: 2647
			}, {
				period: '2010 Q2',
				iphone: 2778,
				ipad: 2294,
				itouch: 2441
			}, {
				period: '2010 Q3',	
				iphone: 4912,
				ipad: 1969,
				itouch: 2501
			}, {
				period: '2010 Q4',
				iphone: 3767,
				ipad: 3597,
				itouch: 5689
			}, {
				period: '2011 Q1',
				iphone: 6810,
				ipad: 1914,
				itouch: 2293
			}, {
				period: '2011 Q2',
				iphone: 5670,
				ipad: 4293,
				itouch: 1881
			}, {
				period: '2011 Q3',
				iphone: 4820,
				ipad: 3795,
				itouch: 1588
			}, {
				period: '2011 Q4',
				iphone: 15073,
				ipad: 5967,
				itouch: 5175
			}, {
				period: '2012 Q1',
				iphone: 10687,
				ipad: 4460,
				itouch: 2028
			}, {
				period: '2012 Q2',
				iphone: 8432,
				ipad: 5713,
				itouch: 1791
			}],
			xkey: 'period',
			ykeys: ['iphone', 'ipad', 'itouch'],
			labels: ['iPhone', 'iPad', 'iPod Touch'],
			pointSize: 0,
			lineWidth:0,
			fillOpacity: 1,
			pointStrokeColors:['#8BC34A', '#0fc5bb', '#f8b32d'],
			behaveLikeLine: true,
			grid: false,
			hideHover: 'auto',
			lineColors: ['#8BC34A', '#0fc5bb', '#f8b32d'],
			resize: true,
			redraw: true,
			smooth: true,
			gridTextColor:'#878787',
			gridTextFamily:"Open Sans",
		});

    if($('#morris_donut_chart').length > 0) {
		// Donut Chart
		Morris.Donut({
			element: 'morris_donut_chart',
			data: [{
				label: "Download Sales",
				value: 12
			}, {
				label: "In-Store Sales",
				value: 30
			}, {
				label: "Mail-Order Sales",
				value: 20
			}],
			colors: ['rgba(139,195,74,1)', 'rgba(248, 179, 45,1)', 'rgba(243, 57, 35,1)'],
			resize: true,
			labelColor: '#878787',
		});
		$("div svg text").attr("style","font-family: Open Sans").attr("font-weight","400");
	}	

    if($('#morris_line_chart').length > 0)
		// Line Chart
		Morris.Line({
			// ID of the element in which to draw the chart.
			element: 'morris_line_chart',
			// Chart data records -- each entry in this array corresponds to a point on
			// the chart.
			data: arrObj.values,
			// The name of the data record attribute that contains x-visitss.
			xkey: 'T',
			xLabels: 'T',
			// A list of names of data record attributes that contain y-visitss.
			ykeys: ['data'],
//			ymax:arrObj.max,
//			ymin:arrObj.min,
			ymax:'auto',
			ymin:'auto',
			// Labels for the ykeys -- will be displayed when you hover over the
			// chart.
			labels: ['금일주가'],
			// Disables line smoothing
			pointSize: 1.1,
			pointStrokeColors:['#192082'],
//			behaveLikeLine: true,
			grid:true,
			gridTextColor:'#878787',
			lineWidth: 2,
			smooth: true,
//			hideHover: 'auto',
			lineColors: ['#192082'],
			resize: true,
			gridTextFamily:"Open Sans",
			parseTime:false
		});

	if($('#morris_bar_chart').length > 0)
	   // Bar Chart
		Morris.Bar({
			element: 'morris_bar_chart',
			data: arrObj.values,
			xkey: 'T',
			ykeys: ['data2'],
			labels: ['거래량'],
			barRatio: 0.8,
			xLabelAngle: 0,
			pointSize: 2.5,
			barOpacity: 1,
			pointStrokeColors:['#192082'],
			behaveLikeLine: true,
			grid: true,
			gridTextColor:'#878787',
			hideHover: 'auto',
			barColors: ['#192082'],
			resize: true,
			gridTextFamily:"Open Sans"
		});
	
	if($('#morris_extra_line_chart').length > 0)
		moririsLine = Morris.Line({
        element: 'morris_extra_line_chart',
        data: arrObj.values,
        xkey: 'd',
//        xLabelFormat: function(d) {
//            return d.getFullYear() +'년 '+(d.getMonth()+1)+'월 '+d.getDate()+ "일"; 
//            },
//        xLabels:'day',
//        xLabelAngle: 45,
        ykeys: arrObj.xname,
        labels: arrObj.xname,
        parseTime:false,
        labelGap:10,
        pointSize: 2,
        fillOpacity: 0,
		lineWidth:2,
		pointStrokeColors:['#0FC5BB', '#F8B42F', '#0F7FFF', '#E11D8E'],
		behaveLikeLine: true,
		grid: true,
		hideHover: 'auto',
		lineColors: ['#0FC5BB', '#F8B42F', '#0F7FFF', '#E11D8E'],
		resize: true,
		gridTextColor:'#878787',
		gridTextFamily:"Open Sans"
        
    });
	
	if($('#morris_extra_bar_chart').length > 0)
		// Morris bar chart
		Morris.Bar({
			element: 'morris_extra_bar_chart',
			data: [{
				y: '2006',
				a: 100,
				b: 90,
				c: 60
			}, {
				y: '2007',
				a: 75,
				b: 65,
				c: 40
			}, {
				y: '2008',
				a: 50,
				b: 40,
				c: 30
			}, {
				y: '2009',
				a: 75,
				b: 65,
				c: 40
			}, {
				y: '2010',
				a: 50,
				b: 40,
				c: 30
			}, {
				y: '2011',
				a: 75,
				b: 65,
				c: 40
			}, {
				y: '2012',
				a: 100,
				b: 90,
				c: 40
			}],
			xkey: 'y',
			ykeys: ['a', 'b', 'c'],
			labels: ['A', 'B', 'C'],
			barColors:['#8BC34A', '#f8b32d', '#f33923'],
			barOpacity:1,
			hideHover: 'auto',
			grid: false,
			resize: true,
			gridTextColor:'#878787',
			gridTextFamily:"Open Sans"
		});

//});
}
function dianaFunction(test) {
	var diana = new Array();
    for (var i=0 ;i<test.categories.category.length; i++) {
    	
        var dianaObj = new Object();
//	        dianaObj['d'] = dateFommat(test.categories.category[i].label);
	        dianaObj['d'] = test.categories.category[i].label;
	        
	        $(test.dataset).each(function(a){
	        	dianaObj[test.dataset[a].seriesname] = test.dataset[a].data[i].value;
	        });
	        diana.push(dianaObj);
    }
    return diana;
}
function dateFommat(str) {
	if(str.indexOf("시") > -1) { //시간별
		
	} else if(str.indexOf("일") > -1 && str.indexOf("요") == -1) { //일별
		var year = str.substr(0,str.indexOf("년"));
		var month = str.substr(str.indexOf("년")+2,str.indexOf("월")-1-(str.indexOf("년")+1));
		if(month.length < 2) {
			month = "0"+month;
		}
		var day = str.substr(str.indexOf("월")+2,str.indexOf("일")-1-(str.indexOf("월")+1));
		if(day.length < 2) {
			day = "0"+day;
		}
		return year+'-'+month+'-'+day;
	} else if(str.indexOf("주") > -1){ //주별
		
	} else if(str.indexOf("요일") > -1) { //요일별
		var week = new Array('일요일', '월요일', '화요일', '수요일', '목요일', '금요일', '토요일');
		for(var i=0; i<week.length; i++) {
			if(week[i] == str) {
				return i;
			}
		}
	} else { //월별
		
	}
}

var arraySet2 = function(result, num){
	
	var label = new Array();
	var xname = new Array();
	var arr = new Array();
	
	arr = dianaFunction(result);
	$($($(result).get(0).categories).get(0).category).each(function(i){
		label.push($(this).get(0).label);
	});
	
	$($(result).get(0).dataset).each(function(a){
		xname.push($(this).get(0).seriesname);
    });

	var pObj = new Object();
	pObj.values = arr;
	pObj.xname = xname;
	pObj.label = label;
	objectResult2 = pObj;
//	console.log(objectResult2);
//	console.log("objectResult2");
	if(num == 0) {
	lineExConfig(pObj);
	} else {
		moririsLine.options.ykeys = pObj.xname;
		moririsLine.setData(pObj.values);
	}
}

var arraySet3 = function(result, max, min) {
	
	console.log(result);
	var stock_data = [];
	
	var labels_data1 = [];
	var labels_data2 = [];
	var series_data = []; //주가
	var trVl_data = []; //거래량
	
	var obj = new Array();
	var idx = 0;
	var idx2 = 0;
	$.each( result.item, function( key, value ) {
		var dataObj = new Object();
		
		var now = new Date();
		var thistime = value.hms;
		now.setHours(thistime.split(":")[0]);
		now.setMinutes(thistime.split(":")[1]);
		now.setSeconds(thistime.split(":")[2]);

		if(now.getHours() >= 15 && now.getMinutes() >= 0) {
			if(idx2 == 0) {
				idx2++;
				var hms = value.hms.split(":");
				dataObj['T'] = hms[0]+":"+hms[1];
				dataObj['data'] = value.indx;
				dataObj['data2'] = value.trVl;
				obj[idx] = dataObj;
				
				labels_data1.push(hms[0]+":"+hms[1]);
				return false;
			}
		} else {
			if(key%50 == 0) {
				var hms = value.hms.split(":");
				dataObj['T'] = hms[0]+":"+hms[1];
				dataObj['data'] = value.indx;
				dataObj['data2'] = value.trVl;
				obj[idx] = dataObj;
				idx++;
	
				labels_data1.push(hms[0]+":"+hms[1]);
				labels_data2.push(value.hms);
				series_data.push(parseInt(value.indx));
				trVl_data.push(parseInt(value.trVl));
			}
		}
	});
	var pObj = new Object();
	pObj.max = max;
	pObj.min = min;
	pObj.label = labels_data1;
	pObj.values = obj;
	console.log(pObj);
	
	lineExConfig(pObj);
}
                                                                                                                    