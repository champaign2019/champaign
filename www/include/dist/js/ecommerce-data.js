/*Dashboard2 Init*/
"use strict"; 

var chartResult;

/*****Ready function start*****/
$(document).ready(function(){
	if( $('#pie_chart_4').length > 0 ){
		$('#pie_chart_4').easyPieChart({
			barColor : '#0FC5BB',
			lineWidth: 20,
			animate: 3000,
			size:	165,
			lineCap: 'square',
			trackColor: '#f4f4f4',
			scaleColor: false,
			onStep: function(from, to, percent) {
				$(this.el).find('.percent').text(Math.round(percent));
			}
		});
	}
	
	if( $('#datable_1').length > 0 )
		$('#datable_1').DataTable({
			"bFilter": false,
			"bLengthChange": false,
			"bPaginate": false,
			"bInfo": false,
			
		});
});
/*****Ready function end*****/

/*****Load function start*****/
$(window).on("load",function(){
//	window.setTimeout(function(){
//		$.toast({
//			heading: 'Welcome to Goofy',
//			text: 'Use the predefined ones, or specify a custom position object.',
//			position: 'top-left',
//			loaderBg:'#f8b32d',
//			icon: '',
//			hideAfter: 3500, 
//			stack: 6
//		});
//	}, 3000);
});
/*****Load function* end*****/

/*****E-Charts function start*****/
var echartsConfig = function(arrObj) { 
		if( $('#e_chart_1').length > 0 ){
		var eChart_1 = echarts.init(document.getElementById('e_chart_1'));
		var option = {
			color: ['#3390FA', '#FFC102', '#8BD73A', '#FD7602'],		
			tooltip: {
				trigger: 'axis',
				backgroundColor: 'rgba(33,33,33,1)',
				borderRadius:0,
				padding:10,
				axisPointer: {
					type: 'cross',
					label: {
						backgroundColor: 'rgba(33,33,33,1)'
					}
				},
				textStyle: {
					color: '#fff',
					fontStyle: 'normal',
					fontWeight: 'normal',
					fontFamily: "'Open Sans', sans-serif",
					fontSize: 12
				}	
			},
			barMaxWidth : '50',
			barGap : '5%',
			legend: {
				show: true
			},
			toolbox: {
				show: true,
				orient: 'vertical',
				left: 'right',
				top: 'center',
				showTitle: false,
				feature: {
					mark: {show: true},
					magicType: {show: true, type: ['line', 'bar', 'stack', 'tiled']},
					restore: {show: true},
				}
			},
			grid: {
				left: '3%',
				right: '10%',
				bottom: '3%',
				containLabel: true
			},
			xAxis : [
				{
					type : 'category',
					data : arrObj.label,
					axisLine: {
						show:true,
						lineStyle: {
							color:'#878787'
						}
					},
					axisLabel: {
						textStyle: {
							rotate : 90,
							color: '#878787',
							fontFamily: "'Open Sans', sans-serif",
							fontSize: 12
						}
					},
				}
			],
			yAxis : [
				{
					type : 'value',
					axisLine: {
						show:true,
						lineStyle: {
							color:'#878787'
						}
					},
					axisLabel: {
						textStyle: {
							color: '#878787',
							fontFamily: "'Open Sans', sans-serif",
							fontSize: 12
						}
					},
					splitLine: {
						show: false,
					}
				}
			],
			series : arrObj.values
		};
//		console.log(arrObj.label);
		eChart_1.setOption(option);
		eChart_1.resize();
	}
	if( $('#e_chart_2').length > 0 ){
		var eChart_2 = echarts.init(document.getElementById('e_chart_2'));
		var option1 = {
			angleAxis: {
				max: 100,
				axisLabel: {
					textStyle: {
						color: '#878787'
					}
				},
				axisLine: {
					lineStyle: {
						color: 'rgba(33, 33, 33, 0.1)'
					}
				}
			},
			color: ['#0FC5BB', '#92F2EF', '#D0F6F5'],
			polar:{
				radius:'70%',
				axisLabel: {
					textStyle: {
						color: '#878787'
					}
				}
			},
			radiusAxis: {
				type: 'category',
				data: ['Dt1', 'Dt2', 'Dt3'],
				z: 10,
				show:false,
				axisLine: {
					lineStyle: {
						color: 'rgba(33, 33, 33, 0.1)'
					}
				}
			},
			
			series: [{
				type: 'bar',
				data: [70, 0, 0],
				coordinateSystem: 'polar',
				name: 'Dt1',
				radius: [0, '30%'],
				stack: 'a'
			}, {
				type: 'bar',
				data: [0, 40, 0],
				coordinateSystem: 'polar',
				name: 'Dt2',
				stack: 'a'
			},{
				type: 'bar',
				data: [0, 0, 80],
				coordinateSystem: 'polar',
				name: 'Dt3',
				stack: 'a'
			}]
		};

		eChart_2.setOption(option1);
		eChart_2.resize();
	}
	if( $('#e_chart_3').length > 0 ){
		var eChart_3 = echarts.init(document.getElementById('e_chart_3'));
		var data = arrObj;
		var option3 = {
			tooltip: {
				show: true,
				trigger: 'item',
				backgroundColor: 'rgba(33,33,33,1)',
				borderRadius:0,
				padding:10,
				formatter: "{b}: {c} ({d}%)",
				textStyle: {
					color: '#fff',
					fontStyle: 'normal',
					fontWeight: 'normal',
					fontFamily: "'Open Sans', sans-serif",
					fontSize: 12
				}	
			},
			series: [{
				type: 'pie',
				selectedMode: 'single',
//				radius: ['80%', '30%'],
				color: ['#0FC5BB', '#4A33FA', '#3390FA', '#FB8127', '#80C431', '#FFA901'],
				labelLine: {
					normal: {
						show: true
					}
				},
				data: data
			}]
		};
		eChart_3.setOption(option3);
		eChart_3.on('click', function(params){
			var data = params.data;
			var name = data.name;
			var link = data.link;
			if(name.indexOf("기타") < 0) {
				var progress = link.replace('javascript:','').replace('detailData(\'','').replace('\');','');
//				console.log(link.replace('javascript:','').replace('detailData(\'','').replace('\');',''));
				detailData(progress,1);
			}
		});
		eChart_3.resize();
	}
}
/*****E-Charts function end*****/

/*****Sparkline function start*****/
var sparklineLogin = function() { 
		if( $('#sparkline_4').length > 0 ){
			$("#sparkline_4").sparkline([2,4,4,6,8,5,6,4,8,6,6,2 ], {
				type: 'line',
				width: '100%',
				height: '45',
				lineColor: '#0FC5BB',
				fillColor: '#0FC5BB',
				minSpotColor: '#0FC5BB',
				maxSpotColor: '#0FC5BB',
				spotColor: '#0FC5BB',
				highlightLineColor: '#0FC5BB',
				highlightSpotColor: '#0FC5BB'
			});
		}	
		if( $('#sparkline_5').length > 0 ){
			$("#sparkline_5").sparkline([0,2,8,6,8], {
				type: 'bar',
				width: '100%',
				height: '45',
				barWidth: '10',
				resize: true,
				barSpacing: '10',
				barColor: '#0FC5BB',
				highlightSpotColor: '#0FC5BB'
			});
		}	
		if( $('#sparkline_6').length > 0 ){
			$("#sparkline_6").sparkline([0, 23, 43, 35, 44, 45, 56, 37, 40, 45, 56, 7, 10], {
				type: 'line',
				width: '100%',
				height: '50',
				lineColor: '#0FC5BB',
				fillColor: 'transparent',
				minSpotColor: '#0FC5BB',
				maxSpotColor: '#0FC5BB',
				spotColor: '#0FC5BB',
				highlightLineColor: '#0FC5BB',
				highlightSpotColor: '#0FC5BB'
			});
		}
	}
	var sparkResize;
/*****Sparkline function end*****/

/*****Resize function start*****/
var sparkResize,echartResize;
$(window).on("resize", function () {
	/*Sparkline Resize*/
	//clearTimeout(sparkResize);
	//sparkResize = setTimeout(sparklineLogin, 200);
	
	/*E-Chart Resize*/
	
	if(chartResult != undefined){
	clearTimeout(echartResize);
	echartResize = setTimeout("echartsConfig(chartResult)", 200);
	}
}).resize(); 
/*****Resize function end*****/

/*****Function Call start*****/

sparklineLogin();
//echartsConfig();

/*****Function Call end*****/

var arraySet = function(result){
	
	console.log(result);
	
		var label = new Array();

		var arr = new Array();
		
		$($($(result).get(0).categories).get(0).category).each(function(i){
			label[i] =$(this).get(0).label;
		})
		

		$($(result).get(0).dataset).each(function(i){
			
			var obj = new Object();
			
			obj.name = $(this).get(0).seriesname;
			obj.type = ($(this).get(0).renderas == "" ? "bar" : $(this).get(0).renderas.toLowerCase());
			
			var data = new Array();
			$($(this).get(0).data).each(function(a){
				data[a] = $(this).get(0).value;
			})
			
			obj.data = data;
			arr[i] = obj;
			
		})

		var pObj = new Object();
		pObj.values = arr;
		pObj.label = label;
		//obj.tooltext = tooltext; 
		objectResult = pObj;
		
		echartResize = pObj;
		
		console.log($(pObj));
		echartsConfig(pObj)
}

var arraySet1 = function(result){
	
	var label = new Array();
	var arr = new Array();
	
//	console.log("*****++++******");
	console.log($(result));
	
	$($(result).get(0).data).each(function(i){
		var obj = new Object();
		obj.name = $(this).get(0).label;
		obj.value = $(this).get(0).value;
		obj.link = $(this).get(0).link;
		var data = new Array();
		arr[i] = obj;
	});

		objectResult1 = arr;
//		console.log("objectResult1");
//		console.log(objectResult1);
		chartResult = arr;
		echartsConfig(arr);
		
		
		
//	$($($(result).get(0).categories).get(0).category).each(function(i){
//		label[i] =$(this).get(0).label;
//	})
//	
//	
//	$($(result).get(0).dataset).each(function(i){
//		
//		var obj = new Object();
//		
//		obj.name = $(this).get(0).seriesname;
//		obj.type = ($(this).get(0).renderas == "" ? "bar" : $(this).get(0).renderas.toLowerCase());
//		
//		var data = new Array();
//		$($(this).get(0).data).each(function(a){
//			data[a] = $(this).get(0).value;
//		})
//		
//		obj.data = data;
//		arr[i] = obj;
//		
//	})
	
//	var pObj = new Object();
//	pObj.values = arr;
//	pObj.label = label;
//	//obj.tooltext = tooltext; 
//	objectResult1 = pObj;
//	console.log($(pObj));
//	echartsConfig(pObj)
}