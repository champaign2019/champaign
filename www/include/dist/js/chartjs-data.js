/*Chartjs Init*/

//$( document ).ready(function() {
//    "use strict";

var chartResult2;
var chartJSConfig = function(arrObj) { 	
	if( $('#chart_1').length > 0 ){
		var ctx1 = document.getElementById("chart_1").getContext("2d");
		var data1 = {
			labels: ["January", "February", "March", "April", "May", "June", "July"],
			datasets: [
			{
				label: "fir",
				backgroundColor: "rgba(139,195,74,1)",
				borderColor: "rgba(139,195,74,1)",
				pointBorderColor: "rgba(139,195,74,1)",
				pointHighlightStroke: "rgba(139,195,74,1)",
				data: [0, 59, 80, 58, 20, 55, 40]
			},
			{
				label: "sec",
				backgroundColor: "rgba(248, 179, 45,1)",
				borderColor: "rgba(248, 179, 45,1)",
				pointBorderColor: "rgba(248, 179, 45,1)",
				pointBackgroundColor: "rgba(248, 179, 45,1)",
				data: [28, 48, 40, 19, 86, 27, 90],
			}
			
		]
		};
		
		var areaChart = new Chart(ctx1, {
			type:"line",
			data:data1,
			
			options: {
				tooltips: {
					mode:"label"
				},
				elements:{
					point: {
						hitRadius:90
					}
				},
				
				scales: {
					yAxes: [{
						stacked: true,
						gridLines: {
							color: "rgba(33,33,33,0)",
						},
						ticks: {
							fontFamily: "Open Sans",
							fontColor:"#878787"
						}
					}],
					xAxes: [{
						stacked: true,
						gridLines: {
							color: "rgba(33,33,33,0)",
						},
						ticks: {
							fontFamily: "Open Sans",
							fontColor:"#878787"
						}
					}]
				},
				animation: {
					duration:	3000
				},
				responsive: true,
				legend: {
					display: false,
				},
				tooltip: {
					backgroundColor:'rgba(33,33,33,1)',
					cornerRadius:0,
					footerFontFamily:"'Open Sans'"
				}
				
			}
		});
	}
    
	if( $('#chart_2').length > 0 ){
		
		var ctx2 = document.getElementById("chart_2").getContext("2d");
		var data2 = {
			labels: arrObj.label,
			datasets: arrObj.values
		};
		
		var hBar = new Chart(ctx2, {
			type:"horizontalBar",
			data:data2,
			options: {
				onClick: function(evt,itm){
					var idx = itm[0]['_index'];
					var name = data2.datasets[0].name[idx];
					if(name.indexOf("기타") < 0) {
					var progress = data2.datasets[0].link[idx].replace('javascript:','').replace('detailData(\'','').replace('\');','');
					detailData(progress,1);
					}
				},
				tooltips: {
					mode:"label"
				},
				scales: {
					barPercentage:0.2,
					barThickness:0.5,
//					maxBarThickness:0.5,
					yAxes: [{
//						maxBarThickness:10,
						stacked: true,
						gridLines: {
							color: "rgba(33,33,33,0)",
						},
						ticks: {
							fontFamily: "Open Sans",
							fontColor:"#878787",
							beginAtZero:true
						}
					}],
					xAxes: [{
						stacked: true,
						gridLines: {
							color: "rgba(33,33,33,0)",
						},
						ticks: {
							fontFamily: "Open Sans",
							fontColor:"#878787"
						}
					}],
					
				},
				elements:{
					point: {
						hitRadius:40
					}
				},
				animation: {
					duration:	3000
				},
				responsive: true,
				legend: {
					display: false,
				},
				tooltip: {
					backgroundColor:'rgba(33,33,33,1)',
					cornerRadius:0,
					footerFontFamily:"'Open Sans'"
				}
				
			}
		});
	}

	if( $('#chart_3').length > 0 ){
		var ctx3 = document.getElementById("chart_3").getContext("2d");
		var data3 = {
			labels: ["Eating", "Drinking", "Sleeping", "Designing", "Coding", "Cycling", "Running"],
			datasets: [
				{
					label: "My First dataset",
					backgroundColor: "rgba(139,195,74,1)",
					borderColor: "rgba(139,195,74,1)",
					pointBackgroundColor: "rgba(139,195,74,1)",
					pointBorderColor: "#fff",
					pointHoverBackgroundColor: "#fff",
					pointHoverBorderColor: "rgba(139,195,74,1)",
					data: [65, 59, 90, 81, 56, 55, 40]
				},
				{
					label: "My Second dataset",
					backgroundColor: "rgba(248, 179, 45,1)",
					borderColor: "rgba(248, 179, 45,1)",
					pointBackgroundColor: "rgba(248, 179, 45,1)",
					pointBorderColor: "#fff",
					pointHoverBackgroundColor: "#fff",
					pointHoverBorderColor: "rgba(248, 179, 45,1)",
					data: [28, 48, 40, 19, 96, 27, 100]
				}
			]
		};
		var radarChart = new Chart(ctx3, {
			type: "radar",
			data: data3,
			options: {
					scale: {
						ticks: {
							beginAtZero: true,
							fontFamily: "Open Sans",
							
						},
						gridLines: {
							color: "rgba(33,33,33,0)",
						},
						pointLabels:{
							fontFamily: "Open Sans",
							fontColor:"#878787"
						},
					},
					
					animation: {
						duration:	3000
					},
					responsive: true,
					legend: {
							labels: {
							fontFamily: "Open Sans",
							fontColor:"#878787"
							}
						},
						elements: {
							arc: {
								borderWidth: 0
							}
						},
						tooltip: {
						backgroundColor:'rgba(33,33,33,1)',
						cornerRadius:0,
						footerFontFamily:"'Open Sans'"
					}
			}
		});
	}

	if( $('#chart_4').length > 0 ){
		var ctx4 = document.getElementById("chart_4").getContext("2d");
		var data4 = {
			datasets: [{
				data: [
					11,
					16,
					7,
					3,
					14
				],
				backgroundColor: [
					"rgba(139,195,74,1)",
					"rgba(248, 179, 45,1)",
					"rgba(15, 197, 187,1)",
					"rgba(243, 57, 35,1)",
					"rgba(225, 29, 142,1)"
				],
				hoverBackgroundColor: [
					"rgba(139,195,74,1)",
					"rgba(248, 179, 45,1)",
					"rgba(15, 197, 187,1)",
					"rgba(243, 57, 35,1)",
					"rgba(225, 29, 142,1)"
				],
				label: 'My dataset' // for legend
			}],
			labels: [
				"lab 1",
				"lab 2",
				"lab 3",
				"lab 4",
				"lab 5"
			]
		};
		var polarChart = new Chart(ctx4, {
			type: "polarArea",
			data: data4,
			options: {
				elements: {
					arc: {
						borderColor: "#fff",
						borderWidth: 0
					}
				},
				scale: {
					ticks: {
						beginAtZero: true,
						fontFamily: "Open Sans",
						
					},
					gridLines: {
						color: "rgba(33,33,33,0)",
					}
				},
				animation: {
					duration:	3000
				},
				responsive: true,
				legend: {
					labels: {
					fontFamily: "Open Sans",
					fontColor:"#878787"
					}
				},
				tooltip: {
					backgroundColor:'rgba(33,33,33,1)',
					cornerRadius:0,
					footerFontFamily:"'Open Sans'"
				}
			}
		});
	}

	if( $('#chart_5').length > 0 ){
		var ctx5 = document.getElementById("chart_5").getContext("2d");
		var data5 = {
			datasets: [
				{
					label: 'First Dataset',
					data: [
						{
							x: 80,
							y: 60,
							r: 10
						},
						{
							x: 40,
							y: 40,
							r: 10
						},
						{
							x: 30,
							y: 40,
							r: 20
						},
						{
							x: 20,
							y: 10,
							r: 10
						},
						{
							x: 50,
							y: 30,
							r: 10
						}
					],
					backgroundColor:"rgba(225, 29, 142,1)",
					hoverBackgroundColor: "rgba(225, 29, 142,1)",
				},
				{
					label: 'Second Dataset',
					data: [
						{
							x: 40,
							y: 30,
							r: 10
						},
						{
							x: 25,
							y: 20,
							r: 10
						},
						{
							x: 35,
							y: 30,
							r: 10
						}
					],
					backgroundColor:"rgba(139,195,74,1)",
					hoverBackgroundColor: "rgba(139,195,74,1)",
				}]
		};
		
		var bubbleChart = new Chart(ctx5,{
			type:"bubble",
			data:data5,
			options: {
				elements: {
					points: {
						borderWidth: 1,
						borderColor: 'rgb(33, 33, 33)'
					}
				},
				scales: {
					xAxes: [
					{
						ticks: {
							min: -10,
							max: 100,
							fontFamily: "Open Sans",
							fontColor:"#878787"
						},
						gridLines: {
							color: "rgba(33,33,33,0)",
						}
					}],
					yAxes: [
					{
						ticks: {
							fontFamily: "Open Sans",
							fontColor:"#878787"
						},
						gridLines: {
							color: "rgba(33,33,33,0)",
						}
					}]
				},
				animation: {
					duration:	3000
				},
				responsive: true,
				legend: {
					labels: {
					fontFamily: "Open Sans",
					fontColor:"#878787"
					}
				},
				tooltip: {
					backgroundColor:'rgba(33,33,33,1)',
					cornerRadius:0,
					footerFontFamily:"'Open Sans'"
				}
			}
		});
	}

	if( $('#chart_6').length > 0 ){
		var ctx6 = document.getElementById("chart_6").getContext("2d");
		var data6 = {
			 labels: [
			"lab 1",
			"lab 2",
			"lab 3"
		],
		datasets: [
			{
				data: [300, 50, 100],
				backgroundColor: [
					"rgba(139,195,74,1)",
					"rgba(248, 179, 45,1)",
					"rgba(15, 197, 187,1)"
				],
				hoverBackgroundColor: [
					"rgba(139,195,74,1)",
					"rgba(248, 179, 45,1)",
					"rgba(15, 197, 187,1)"
				]
			}]
		};
		
		var pieChart  = new Chart(ctx6,{
			type: 'pie',
			data: data6,
			options: {
				animation: {
					duration:	3000
				},
				responsive: true,
				legend: {
					labels: {
					fontFamily: "Open Sans",
					fontColor:"#878787"
					}
				},
				tooltip: {
					backgroundColor:'rgba(33,33,33,1)',
					cornerRadius:0,
					footerFontFamily:"'Open Sans'"
				},
				elements: {
					arc: {
						borderWidth: 0
					}
				}
			}
		});
	}

	if( $('#chart_7').length > 0 ){
		var ctx7 = document.getElementById("chart_7").getContext("2d");
		var data7 = {
			 labels: [
			"lab 1",
			"lab 2",
			"lab 3"
		],
		datasets: [
			{
				data: [300, 50, 100],
				backgroundColor: [
					"rgba(139,195,74,1)",
					"rgba(248, 179, 45,1)",
					"rgba(15, 197, 187,1)"
				],
				hoverBackgroundColor: [
					"rgba(139,195,74,1)",
					"rgba(248, 179, 45,1)",
					"rgba(15, 197, 187,1)"
				]
			}]
		};
		
		var doughnutChart = new Chart(ctx7, {
			type: 'doughnut',
			data: data7,
			options: {
				animation: {
					duration:	3000
				},
				responsive: true,
				legend: {
					labels: {
					fontFamily: "Open Sans",
					fontColor:"#878787"
					}
				},
				tooltip: {
					backgroundColor:'rgba(33,33,33,1)',
					cornerRadius:0,
					footerFontFamily:"'Open Sans'"
				},
				elements: {
					arc: {
						borderWidth: 0
					}
				}
			}
		});
	}	
//});
}

var chartjsResize;
//$(window).on("resize", function () {
//	
//	if(chartResult2 != undefined){
//	clearTimeout(chartjsResize);
//	chartjsResize = setTimeout("chartJSConfig(chartResult2)", 200);
// 	console.log("resize:chartjs");
//	}
//}).resize(); 

var arraySet3 = function(result){
	
	var label = new Array();
	var labeldetaile = new Array();
	var link = new Array();
	var data = new Array();
	var arr = new Array();
	var backgroundColor = new Array(
						"#3390FA",
						"#F8B32D",
						"#8BC34A",
						"#E11D8E",
						"#0FC5BB",
						"#F33923" );

	$($(result).get(0).data).each(function(i){
		
		label[i] = $(this).get(0).label;
		labeldetaile = label[i].split("{br}");
		var labeldetaile2 = new Array();
		for(var d=0; d<labeldetaile.length; d++){
			labeldetaile2[d] = labeldetaile[d]; 			
		}
//		label[i] = label[i].replace("{br}","\n");
		label[i] = labeldetaile2;
		data[i] = $(this).get(0).value;
		link[i] = $(this).get(0).link;
	});
	
	var obj = new Object();
	obj.data = data;
	obj.name = label;
	obj.link = link;
	obj.backgroundColor = backgroundColor;
	arr[0] = obj;

	var pObj = new Object();
	pObj.values = arr;
	pObj.label = label;
	//obj.tooltext = tooltext; 
	chartResult2 = pObj;
	objectResult3 = pObj;
//	console.log($(pObj));
	chartJSConfig(pObj);
}