<?session_start();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/reservation/Reservation.class.php";
?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<? include $_SERVER['DOCUMENT_ROOT']."/include/logingCheck.php" ?>
<? $pageTitle = "로그인"; ?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/include/headHtml.php"; ?>
</head>
<body>
<?
$url = $_REQUEST['url'];			// 전달 받은 주소
$param = $_REQUEST['param'];		// 전달 받은 파라메터

// 파라메터가 있을 경우에는 url 뒷에 결합한다.
if ($param) {
	$url = $url."?".param;
}

$reser = new Reservation(999, 'reservation', $_REQUEST);
$rowPageCount = $reser->getCountNonLogin($_REQUEST);

if ( !empty($_REQUEST['name']) ) {
	if ($rowPageCount[0] > 0) {

		$_SESSION['nomember_name'] = $_REQUEST['name'];
		$_SESSION['nomember_password'] = $_REQUEST['password'];

		echo returnURL($url);
	} else {
		echo returnURLMsg("/reservation/confirm.php", '예약내역이 없습니다.');

	}
}
else {
	echo returnURL("/");
}
?>
</body>
</html>