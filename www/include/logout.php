<?session_start();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<?
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include $_SERVER['DOCUMENT_ROOT']."/include/logingCheck.php"
?>
<head>
<title><?=COMPANY_NAME?> | 로그아웃</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>

<?
	$url = $_REQUEST[url] == '' ? '/index.php' : $_REQUEST[url];
	
	unset($_SESSION['member_no']);
	unset($_SESSION['member_id']);
	unset($_SESSION['member_name']);
	unset($_SESSION['member_email']);
	unset($_SESSION['member_cell']);
	unset($_SESSION['member_sns']);
	unset($GLOBALS['memberSession']);
	$GLOBALS['memberSession'] = null;

	echo returnURLMsg($url, '로그아웃되었습니다.');
?>
</body>
</html>