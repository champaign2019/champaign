<?
if (!$loginCheck) {
	$url = $_SERVER['REQUEST_URI'];
	$param = $_SERVER['QUERY_STRING'];
	$loginUrl = "/member/login.php";
	$msg = "로그인 하시겠습니까?";

	if ($_SERVER['REQUEST_METHOD'] == 'GET') {
		$_SESSION['loginUrl'] = $loginUrl;
		$_SESSION['url'] = $url;
		$_SESSION['param'] = $param;
		$_SESSION['msg'] = $msg;
		echo "
			<script>
				location.replace('/include/alert.php');
			</script>";
	} else {
		$_SESSION['loginUrl'] = $loginUrl;
		$_SESSION['url'] = substr($url, 0, strrpos($url, '/'));
		$_SESSION['msg'] = $msg;
		echo "
			<script>
				location.replace('/include/alert.php');
			</script>";
	}
	
}
?>

<!--	메세지를 세션에 담지 않아서 위에걸로 교체
<?
if (!$loginCheck) {
	$url = $_SERVER['REQUEST_URI'];
	$param = $_SERVER['QUERY_STRING'];
	$loginUrl = "/member/login.php";
	$msg = "로그인 하시겠습니까?";

	if ($_SERVER['REQUEST_METHOD'] == 'GET') {
		$_SESSION['loginUrl'] = $loginUrl;
		$_SESSION['url'] = $url;
		$_SESSION['param'] = $param;
		echo "
			<script>
				location.replace('/include/alert.php');
			</script>";
	} else {
		$_SESSION['loginUrl'] = $loginUrl;
		$_SESSION['url'] = substr($url, 0, strrpos($url, '/'));
		echo "
			<script>
				location.replace('/include/alert.php');
			</script>";
	}
	
}
?>
-->