<?
	include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Popup.class.php";
	include $_SERVER['DOCUMENT_ROOT']."/include/top_popup/userAgent.php";

	$topPopup = new Popup(9999, 'top_popup', $_REQUEST);
	// PC의 경우 모바일전용 데이터는 가져오지 않음
	if ( !$topPopupResultMobile ) {
		$_REQUEST['snot_popup_device_type'] = 1;
	}
	// 모바일의 경우 pc전용 데이터는 가져오지 않음
	else {
		$_REQUEST['snot_popup_device_type'] = 0;
	}
	// 진행중 상태값만
	$_REQUEST['sstate'] = 1;
	$_REQUEST['sval'] = null;
	$_REQUEST['stype'] = null;
	$topPopupCount = $topPopup->getCount($_REQUEST);
	$topPopupList = $topPopup->getList($_REQUEST);

	if ( $topPopupCount[0] > 0 ) {
?>
<div class="top_pop" style="display:none;" id="id_top">
	<div class="top_banner">
		<ul>
<?
		while ($topPopupRow=mysql_fetch_assoc($topPopupList)) {
			// add_m : _m이 들어갈 변수
			$add_m = "";
			/*
			device가 mobile이면서 popup_device_type이 mobile전용(1) 혹은 PC&Mobile 각각(3)일 경우에 모바일 전용 컬럼을 가져오기 위해 _m을 붙인다.
			*/
			if ( ($topPopupResultMobile && $topPopupRow['popup_device_type'] == 1) || ($topPopupResultMobile && $topPopupRow['popup_device_type'] == 3) ) {
				$add_m = "_m";
			}
			
			$popup_width = $topPopupRow['popup_width'.$add_m];
			$styleElements = '';
			$styleElements += 'style="';
			if ( $popup_width > 0 ) {
				$styleElements += 'width:'+$popup_width+'px;';
				$styleElements += 'height:auto;';
			}
			$styleElements += '"';
?>
			<li>
				<? if ( !$topPopupRow['relation_url'.$add_m] ) { ?>
				<a>
				<? } else { ?>
				<a href="<?=$topPopupRow['relation_url'.$add_m] ?>" target="_blank">
				<? } ?>
					<img 
						src="/upload/popup/<?=$topPopupRow['imagename'.$add_m] ?>" 
						alt="<?=$topPopupRow['image_alt'.$add_m] ?>" 
						title="<?=$topPopupRow['title'] ?>" 
						<?=$styleElements ?>
					/>
				</a>	
			</li>
<?
		}
?>
		</ul>
	</div>			
	<div class="top_close">
		<div style="padding:3px 10px;">
			<label><input type="checkbox" id="dontViewTopPopupToday" />
			<font>오늘 하루 이 창을 열지 않음</font></label>
			<a href="javascript:;">[닫기]</a>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		$('.top_close a').click(function(){
			$('.top_pop').slideUp();
		});
		$('#dontViewTopPopupToday').click(function(){
			setCookie('dontViewTopPopupToday', 'yes');
			$('.top_pop').slideUp();
		});
		if(getCookie('dontViewTopPopupToday') != 'yes') {
			$('.top_pop').css('display', 'block');
		}
		$(".top_banner ul").bxSlider({
		  auto: true,
		  moveSlides: 1,
		  autoHover: true,
		  stopAutoOnClick: true,
		  pager : false,
		});
	});
</script>
<?
	}
?>