<? include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php"; ?>
<? if(IS_LOG) {  // 웹로그 분석	?>
<div id="com1" style="width:0px; height:0px; position:absolute; left:0px; top:0px; z-index:1"> 
<script type='text/javascript' src="http://weblog.vizensoft.com/stat_js.jsp?sitenum=<?=WEBLOG_NUMBER?>"></script>
</div>
<? } ?>

<?
// 로그인 버튼 클릭 시 현재 페이지 유지
$urlMaintain = "http://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
$paramMaintain = $_SERVER['QUERY_STRING'];

if ($method == 'POST') {
	$urlMaintain = substr($urlMaintain, 0, strrpos($urlMaintain, '/'));
}
?>
<div id="com2" style="width:0px; height:0px; position:absolute; left:0px; top:0px; z-index:1"> 
<form name="urlMaintain" id="urlMaintain" action="/member/login.php" method="post">
	<input type="hidden" name="url" id="main_url" value="<?=$urlMaintain?>" />
	<input type="hidden" name="param" id="main_param" value="<?=$paramMaintain?>" />
</form>
</div>
<script type="text/javascript">
	(function () {
		var s = document.createElement("script");
		s.type = "text/javascript";
		s.async = true;
		s.src = '//api.usersnap.com/load/99850f11-7b8a-49f4-a68e-0cd5efcb0135.js';
		var x = document.getElementsByTagName('script')[0];
		x.parentNode.insertBefore(s, x);
	})();
</script>
<header class="topfix">
	<img src="/images/header/header-bg.jpg" class="top-header-img" alt="Bravecto">
	<div class="container-1 desktop-only">
		<div class="fww">
			<div class="fwr">
				<div class="row no-gutters">
					<div class="col-lg-2 col-xl-2"><a href="/" title="Bravecto" class="brand-logo"><img src="/images/logo.svg" alt="Bravecto"></a></div>
					<div class="col-lg-10 col-xl-10">
						<ul class="nav mainnav justify-content-end">
							<li class="nav-item dropdown" id="shoemenu">
								<a class="nav-link dropdown-toggle collapsed active" data-toggle="collapse"
								   href="#product-menu" role="button"
								   aria-expanded="false" aria-controls="product-menu">브라벡토소개</a>
							</li>
							<li class="nav-item"> <a class="nav-link" href="/page/benefits.php" title="BENEFITS">브라벡토 장점</a> </li>
							<li class="nav-item"> <a class="nav-link" href="/page/fluralaner.php" title="FLURALANER">플루랄라너</a> </li>
							<li class="nav-item dropdown vetlink">
								<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="/page/parasites.php" title="PARASITES">외부기생충</a> 
								<div class="dropdown-menu">
									<a href="/page/parasites.php" class="dropdown-item" >참진드기</a>
									<a href="/page/mitemite.php" class="dropdown-item" >응애류 진드기</a>
									<a href="/page/flea.php" class="dropdown-item" >벼룩</a>
								</div>
							</li>
							<li class="nav-item">
								<a class="nav-link" onclick="location.href='/page/vets-resources.php';" title="VET INFO">수의학적 정보</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" onclick="location.href='/page/contact-us.php';" title="CONTACT US">고객문의</a>
							</li>
							<li class="nav-item"><a class="nav-link" href="/faq/index.php" title="PARASITES">FAQ</a> </li>
							<li> 
							<a class="find-stockist" href="/page/stockists.php" title="Find a stockist">판매점 찾기</a> 
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="header-mobile mobile-only">
		<a href="/" title="Bravecto"><img src="/images/logo.svg" class="mobile-logo" alt="Bravecto"></a>
		<a href="#" class="open-menu"><img src="/images/icons/menu.svg" alt="Menu" width="45"></a>
	</div>
	<div class="collapse collapse1" id="product-menu">
		<div class="row no-gutters">
			<div class="col-xl-6 col-lg-6">
				<a href="/page/chew-for-dogs.php" class="bluetab d-flex">
					<div>
						<h6 class="align-self-end">Bravecto Chew for Dogs 
							<span>
							Just 1 tasty chew for 3 months protection against fleas and paralysis ticks
							</span>
						</h6>
						<div class="imgcontainer"><img src="/images/header/packshot_chew_100.png" alt="packshot chews"></div>
					</div>
				</a>
			</div>
			<!--
			<div class="col-xl-3 col-lg-6">
				<a href="/page/spot-on-dogs.php" class="purpaltab d-flex">
					<div>
						<h6 class="align-self-end">Bravecto Spot-on for Dogs <span>A single easy application with our <strong>TWIST&acute;N&acute;USE</strong> tube protects against fleas and paralysis ticks for 6 months </span></h6>
						<div class="imgcontainer"><img src="/images/header/packshot_spoton_dog_100.png" alt="Spot-on for Dogs"></div>
					</div>
				</a>
			</div>
			-->
			<div class="col-xl-6 col-lg-6">
				<a href="/page/spot-on-cats.php" class="orangetab d-flex">
					<div>
						<h6 class="align-self-end">Bravecto Spot-on for Cats <span>The longest lasting spot-on flea and paralysis tick treatment for cats - up to 3 months from a single application</span></h6>
						<div class="imgcontainer"><img src="/images/header/packshot_spoton_cat_100.png" alt="Spot-on for Cats"></div>
					</div>
				</a>
			</div>
			
		</div>
	</div>
</header>

<!--~///////////////// Header End /////////////////-->
<noscript>
	<div class="container alert alert-danger text-center">
		<div class="col">
			<h3>You have JavaScript disabled</h3><p>
				This may result in reduced functionality and affect the graphical presentation of this site.<br />
				However, all content will still be available.
			</p>
		</div>
	</div>
</noscript>
