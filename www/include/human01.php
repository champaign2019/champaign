<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0, user-scalable=no" />
<link rel="stylesheet" type="text/css" href="/css/ygofont.css">
<style>
	a{}
	ul,ol,li{list-style:none;}
	*{margin:0; padding:0;}
</style>
<title></title>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
</head>
<body>
	<div class="human_wrap" style="width:900px; height:854px; margin:0 auto;  background: #e9f2f1; background: linear-gradient(to bottom, #e9f2f1, #f2ede9, #f5ece5); background: -webkit-linear-gradient(to right, #e9f2f1, #f2ede9, #f5ece5);">
		<div class="human" style="padding-top:54px; box-sizing:border-box;">
			<table style="width:785px; height:745px; margin:0 auto; border:8px solid #fff; box-sizing:border-box; text-align:center;">
				<tbody>
					<tr style="display:block; line-height:1;">
						<td style="display:block; width:100%; font-size:24px; font-weight:300; letter-spacing:-0.045em; line-height:1; color:#222; padding-top:55px; font-family: 'Yoon YGO 550_TT',sans-serif;">(업체명)</td>
					</tr>
					<tr style="display:block; line-height:1;">
						<td style="display:block; text-align:center; width:408px; margin:16px auto 0; font-size:46px; font-weight:bold; letter-spacing:-0.040em; line-height:1; color:#222; padding-bottom:25px; border-bottom:1px solid #737373; font-family: 'Yoon YGO 550_TT',sans-serif;">휴먼계정 알림 안내</td>
					</tr>
					<tr style="display:block; line-height:1;">
						<td style="display:block;"><span style="display:inline-block; width:100%; margin-top:22px; font-size:18px; font-weight:bold; letter-spacing:-0.040em; color:#595959; line-height:1; font-family: 'Yoon YGO 550_TT',sans-serif;"><img src="/img/human_img01.png" style="display:inline-block; vertical-align:middle; margin-top:-3px; margin-right:14px; font-family: 'Yoon YGO 550_TT',sans-serif;">아래 내용을 반드시 확인 바랍니다.</span></td>
					</tr>
					<tr style="display:block; line-height:1; padding-top:60px;">
						<td style="display:block; text-align:center; font-size:20px; font-weight:600; letter-spacing:-0.040em; line-height:1; color:#222; padding-bottom:20px; font-family: 'Yoon YGO 550_TT',sans-serif;">(업체명) 서비스는 1년간 접속하지 않으시면 휴먼 상태로 전환 될 예정입니다.</td>
					</tr>
					<tr style="display:block; line-height:1;">
						<td style="display:block; text-align:center; font-size:18px; font-weight:400; letter-spacing:-0.040em; line-height:28px; color:#444; padding-bottom:80px; font-family: 'Yoon YGO 550_TT',sans-serif;">
							휴면 상태로 전환 된 이후로는 사이트 이용이 불가능 하오니, 서비스를<br />
							계속해서 이용하시려면 안내드리는 휴면 전환일 이전에<br />
							(업체명)사이트에 방문하여 로그인 접속 부탁드립니다.
						</td>
					</tr>
					<tr style="display:block; line-height:1;">
						<td style="display:block; text-align:center; font-size:18px; font-weight:300; letter-spacing:-0.040em; line-height:1; color:#222; font-family: 'Yoon YGO 550_TT',sans-serif;">[ 휴면 계정 전환 예정일 ]</td>
					</tr>
					<tr style="display:block; line-height:1;">
						<td style="display:block; padding-top:20px; font-size:22px; font-weight:bold; letter-spacing:-0.05em; color:#222; line-height:28px; font-family: 'Yoon YGO 550_TT',sans-serif;">
							<b>
								2018년 10월 30일 이후<br />
								휴면 계정 전환 예정
							</b>
						</td>
					</tr>
					<tr>
						<td style="display:block; margin:0; padding:0;"><a href="javascript:;" style="display:block; width:236px; height:44px; margin:0 auto; font-size:18px; font-weight:bold; letter-spacing:-0.05em; background:#595959; color:#fff; line-height:44px; text-align:center; text-decoration:none; font-family: 'Yoon YGO 550_TT',sans-serif;">사이트 바로가기<img style="display:inline-block; vertical-align:middle; margin-top:-5px; margin-left:22px;" src="/img/human_arrow_img.png"></a></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>