<? 
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php"; 
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php"; 
include_once $_SERVER['DOCUMENT_ROOT']."/lib/schedule/Schedule.class.php";	
?>
<? include "config.php"; ?>
<?

	
	$_REQUEST["today"] = getReqParameter( $param['today'], substr(getToday(), 0,8)."01");
	$_REQUEST["start_day"] = getMonthDateAdd( -1, $_REQUEST["today"]);
	$_REQUEST["end_day"] = getMonthDateAdd( 2, $_REQUEST["today"]);
	$_REQUEST["today_year"] = substr($_REQUEST["today"], 0, 4);
	$_REQUEST["today_month"] = substr($_REQUEST["today"], 5, 7);

	$_REQUEST['start'] = $param['start'];
	$_REQUEST['end'] = $param['end'];
	$_REQUEST['color0'] = $param['color0'];
	$_REQUEST['color1'] = $param['color1'];

	$_REQUEST['startdate'] = $_REQUEST["start_day"];
	$_REQUEST['enddate'] = $_REQUEST["end_day"];
	$_REQUEST['today_year'] = $_REQUEST["today_year"];
	$_REQUEST['today_month'] = $_REQUEST["today_month"];

	

	$_REQUEST['orderby'] = $orderby; //정렬 배열 선언

	$objCommon = new Schedule($pageRows, $tablename, $_REQUEST);
	$rowPageCount = $objCommon->getCount($_REQUEST);
	$result = $objCommon->getList($_REQUEST);

	$category_result = $category_tablename ? $objCommon->getCategoryList($_REQUEST) : null;
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<? include $_SERVER['DOCUMENT_ROOT']."/include/headHtml.php"; ?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/boardConfig/config.php"; ?>
<link rel="stylesheet" href="/css/schedule.css" type="text/css" media="all" />

</head>
<body>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/common.php"; ?>
	<div id="wrap">
	<? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
	<? include "sub_visual/sub_visual.php" ?>
	<!-- 프로그램시작 -->
	<div class="programCon">
	<div class="commuting_wrap">
	<!-- 달력 폼 시작 -->

	<div class="div_05_calender mb30">
		<div id="cal_title" class="div_month mb22 up_bar"></div>
		<div class="c_table schedule_table">
		<table>
			<colgroup>
				<col width="100px" />
				<col width="100px" />
				<col width="100px" />
				<col width="100px" />
				<col width="100px" />
				<col width="100px" />
				<col width="100px" />
			</colgroup>
				<thead>
					<tr>
						<th>일</th>
						<th>월</th>
						<th>화</th>
						<th>수</th>
						<th>목</th>
						<th>금</th>
						<th>토</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
		</div>
		<div class="c_btn s_type">						
			<a class="" href="/schedule/list.php">리스트</a>
			<a href="/schedule/write.php" class="right">글쓰기</a>
		</div>

	<form name="searchForm" id="searchForm" action="index.jsp" method="get">
		<div class="program_search">
			
			<select name="stype" title="검색을 선택해주세요">
				<option value="all" <?=getSelected($_REQUEST[stype], "all") ?>>제목+내용</option>
				<option value="title" <?=getSelected($_REQUEST[stype], "title") ?>>제목</option>
				<option value="contents" <?=getSelected($_REQUEST[stype], "contents") ?>>내용</option>
				<option value="user_name" <?=getSelected($_REQUEST[stype], "user_name") ?>>작성자</option>
			</select>
		
			<span>
				<input type="text" name="sval" value="<?=$_REQUEST[sval]?>" title="검색할 내용을 입력해주세요" />
				<a href="javascript:;" onclick="$('#searchForm').submit()" >검색</a>
			</span>
		</div>
		<input type="hidden" name="menu_fk" value="<?=$parameter["menu_fk"] ?>" />
		</form>
     <!-- 달력 폼 끝 -->

	</div>
	</div>
	<!-- 프로그램끝 -->
	<? include "sub_footer/sub_footer.php" ?>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
</div>
</body>
</html>
