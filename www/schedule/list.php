<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/schedule/Schedule.class.php";

include "config.php";

$_REQUEST['orderby'] = $orderby;

$b_prm = new Schedule($pageRows, $_REQUEST);
$b_dao = new Schedule();
$rowPageCount = $b_prm->getCount($_REQUEST);
//echo($rowPageCount);
$b_list = $b_prm->getList($_REQUEST);
$b_notice = Array();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/include/headHtml.php" ?>
<link rel="stylesheet" href="/css/schedule.css" type="text/css" media="all" />
</head>
<body>

<div id="wrap">
	<? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
	<? include "sub_visual/sub_visual.php" ?>
	<!-- 프로그램시작 -->
	<div class="programCon">
		<div class="program_table">
			<table>
				<colgroup>
					<col width="8%" />
					<col width="14%" />
					<col width="14%" />
					<col width="14%" />
					<col width="14%" />
					<col width="14%" />
					<col width="14%" />
					<col width="8%" />
				</colgroup>
				<thead>
					<tr>
						<th>NO</th>
						<th>상태</th>
						<th>제목</th>
						<th>시작일</th>
						<th>종료일</th>
						<th>작성자</th>
						<th>작성일</th>
						<th>조회수</th>
					</tr>
				</thead>
				<tbody>
					<?
						if ($rowPageCount[0] > 0) {
							$readUrl 	= "";
							$style 	= "";
							while ($row=mysql_fetch_assoc($b_list)) {
								$style	= "style=\"cursor:pointer;\"";
								$readUrl = "style='cursor:pointer;' onclick=\"location.href='".$b_prm->getQueryString('read.php', $row['no'], $_REQUEST)."'\"";
					?>
					<tr <?=$readUrl?><?=$style ?>>
					
						<td><?=$rowPageCount[0] - (($b_prm->reqPageNo-1)*$pageRows) - $i?></td>
						<td><?=getState($row['state'])?></td>
						<td><a href="javascript:;"><?=$row['title']?></a></td>
						<td><?=$row['startdate'] ?></td>
						<td><?=$row['enddate'] ?></td>
						<td><?=$row['user_name'] ?></td>
						<td><?=getFullToday($row['registdate'])?></td>
						<td><?=$row['readno'] ?></td>
					</tr>
					<? $i++?>
					<?
							}
						} else {
					?>
					<tr><td class="first" colspan="7">등록된 일정이 없습니다.</td></tr>
					<? 	} ?>
				</tbody>
			</table>
		</div>
		<form name="searchForm" id="searchForm" action="/schedule/list.jsp" method="get">
		<div class="program_search">
			<select name="stype" title="검색을 선택해주세요">
				<option value="all" <?=getSelected($_REQUEST[stype], "all") ?>>전체</option>
				<option value="title" <?=getSelected($_REQUEST[stype], "title") ?>>제목</option>
				<option value="contents" <?=getSelected($_REQUEST[stype], "contents") ?>>내용</option>
				<option value="user_name" <?=getSelected($_REQUEST[stype], "user_name") ?>>작성자</option>
			</select>
			<span>
				<input type="text" name="sval" id="sval" value="<?=$_REQUEST[sval]?>" />
				<a href="javascript:;" onclick="$('#searchForm').submit()" >검색</a>
			</span>
		</div>
		<div class="readBottom_btn">
			<dd>
				<a class="" href="/schedule/index.php"><strong>Calendar</strong></a>
			</dd>
		</div>
		<input type="hidden" name="menu_fk" value="<?=$_REQUEST['menu_fk'] ?>" />
		</form>
	</div>
	<!-- 프로그램끝 -->
	<? include "sub_footer/sub_footer.php" ?>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
</div>
</body>
</html>