<?session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/include/headHtml.php"; ?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/boardConfig/config.php"; ?>
<?include "config.php"; ?>

<link rel="stylesheet" href="/css/schedule.css" type="text/css" media="all" />
<script>
// var oEditors; // 에디터 객체 담을 곳
$(document).ready(function(){
// 	oEditors = setEditor("contents"); // 에디터 셋팅
	$('#startdate1').datetimepicker({timepicker:false,format: "Y-m-d"});
	$("#startbutton").click(function(){
		$('#startdate1').datetimepicker('show');
	})		
	$('#enddate1').datetimepicker({timepicker:false,format: "Y-m-d"});
	$("#endbutton").click(function(){
		$('#enddate1').datetimepicker('show');
	})				
	//initCal({id:"startdate1",type:"day",today:"y"});
	//initCal({id:"enddate1",type:"day",today:"y"});

});

function goSave(obj) {
	if(validation(obj)){		
		$('[name=startdate]').val( $('#startdate1').val() +" "+ $('#startdate2').val()+":"+$('#startdate3').val() +":00" );
		$('[name=enddate]').val( $('#enddate1').val() +" "+ $('#enddate2').val()+":"+$('#enddate3').val() +":00" );

		if($('[name=startdate]').val() > $('[name=enddate]').val()){
			alert('시작일이 종료일보다 높습니다. 재설정 해주세요.');
			$('[name=startdate]').focus();
			return false;
		}		
		//여기부터 스팸
		//if(!fn_spamCheck()){return false;}
		
		var form = $("#frm");

		fn_formSpanCheck(form,key);	
		//스팸 끝
		$(form).submit()	
	}else{
		return;
	}
}

function deleteFile(no){
	if(confirm("첨부된 파일을 삭제하시겠습니까?")){
		$.post("filePop.jsp", "cmd=fileDelete&no="+no, function(data){
			data = data.replace(/<[^>]+>/g, '').trim();
			if(data == 'success'){
				$("#file"+no).hide(500).remove();
			} else if (data == 'fail') {
				alert('파일 삭제에 실패했습니다.');
			}
		});
	}
}
</script>
</head>

<body>

<div id="wrap">
	<? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
	<? include "sub_visual/sub_visual.php" ?>
	<!-- 프로그램시작 -->
	<div class="programCon">
		<div class="commuting_wrap">
			<div class="sr_table w_type">
			<form action="process.php" method="post" id="frm" name="frm"  enctype="multipart/form-data" >
			<table>
				<colgroup>
					<col width="20%" />
					<col width="*" />
				</colgroup>
				<tbody>					
					<tr>
						<th><span class="col01"> </span>이름</th>
						<td><input type="text" name="user_name" id="user_name" class="size03" value="<?=$_SESSION['member_name']?>" data-value="이름을 입력해 주세요."/></td>
					</tr>
					
					<tr>
						<th><span class="col01"> </span>시작일</th>
						<td>
							<input type="text" name="startdate1" id="startdate1" value="<?=$_REQUEST["startdate1"]?>" data-value="시작일을 입력해 주세요." class="size02" maxlength="13" onkeyup="isNumberOrHyphen(this);" readonly="readonly"/>
							<span id="Calstartdate1Icon">
							<button type="button" id="startbutton"><img src="/manage/img/calendar_icon.png" id="CalregistdateIconImg" style="cursor:pointer;"/></button>
							</span>
							<select name="startdate2" id="startdate2" data-value="시작일을 입력해 주세요.">
								<?
								$stime = "";
								for($i=1; $i<=23; ++$i){ 
									$stime = ($i < 10) ? ("0".$i) : $i;
								?>
								<option value="<?=$stime?>" ><?if($i<10){ ?>0<?} ?><?=$i?></option>
								<?} ?>
							</select><span>시</span>
							<select name="startdate3" id="startdate3" data-value="시작일을 입력해 주세요." >
								<?
								$stime2 = "";
								for($i=0;$i<=55;$i=$i+5){ 
									$stime2 = $i < 10 ? ("0".$i) : $i;
								?>
								<option value="<?=$stime2?>" ><?=$stime2?></option>
								<?} ?>
							</select><span class="last">분</span>
						</td>
					</tr>
					<tr>
						<th><span class="col01"> </span>종료일</th>
						<td>
							<input type="text" name="enddate1" id="enddate1" class="size02" maxlength="13" onkeyup="isNumberOrHyphen(this);" data-value="종료일을 입력해 주세요." readonly="readonly"/>
							<span id="Calenddate1Icon">
							<button type="button" id="endbutton"><img src="/manage/img/calendar_icon.png" id="CalregistdateIconImg" style="cursor:pointer;"/></button>
							</span>
							<select name="enddate2" id="enddate2" data-value="종료일을 입력해 주세요." >
								<?
								$etime = "";
								for($i=1;$i<=23; ++$i){ 
									$etime = $i < 10 ? ("0".$i) : $i;
								?>
								<option value="<?=$etime?>"  ><?=$etime?></option>
								<?} ?>
							</select><span>시</span>
							<select name="enddate3" id="enddate3" data-value="종료일을 입력해 주세요.">
								<?
								$etime2 = "";
								for($i=0;$i<=55;$i=$i+5){ 
									$etime2 = $i < 10 ? ("0".$i) : $i;
								?>
								<option value="<?=$etime2?>" ><?=$etime2?></option>
								<?} ?>
							</select><span class="last">분</span>
						</td>
					</tr>
					<tr>
						<th><span class="col01"> </span>상태</th>
						<td>
							<label><input type="radio" name="state" value="0" checked/> 상태없음</label>
							<label><input type="radio" name="state" value="1"/> 미진행</label>
							<label><input type="radio" name="state" value="2"/> 진행중</label>
							<label><input type="radio" name="state" value="3"/> 완료</label>
						</td>
					</tr>
					<tr>
						<th><span class="col01"> </span>라인 색상</th>
						<td class="lin_color">
							<label><input type="radio" name="line_color" value="#fce4e4" checked/><i style="background-color:#fce4e4"></i></label>
							<label><input type="radio" name="line_color" value="#cfe4fe"/> <i style="background-color:#cfe4fe"></i></label>
							<label><input type="radio" name="line_color" value="#c5f4b0"/> <i style="background-color:#c5f4b0"></i></label>
							<label><input type="radio" name="line_color" value="#fff0c4"/> <i style="background-color:#fff0c4"></i></label>
							<label><input type="radio" name="line_color" value="#ffd9c4"/> <i style="background-color:#ffd9c4"></i></label>
							<label><input type="radio" name="line_color" value="#c4c8ff"/> <i style="background-color:#c4c8ff"></i></label>
							<label><input type="radio" name="line_color" value="#e6b5fd"/> <i style="background-color:#e6b5fd"></i></label>
							<label><input type="radio" name="line_color" value="#ffc4e4"/> <i style="background-color:#ffc4e4"></i></label>
							<label><input type="radio" name="line_color" value="#fcd56c"/> <i style="background-color:#fcd56c"></i></label>
							<label><input type="radio" name="line_color" value="#a4a0fd"/> <i style="background-color:#a4a0fd"></i></label>
						</td>
					</tr>
					<tr>
						<th><span class="col01"> </span>제목</th>
						<td><input type="text" name="title" id="title" class="size03" data-value="제목을 입력해 주세요."/></td>
					</tr>
					<tr>
						<th class="vt">내용</th>
						<td>
							<textarea name="contents" id="contents"></textarea>
						</td>
					</tr>
					<!-- <tr>
						<th>첨부파일</th>
						<td>
							<a class="btns" onclick="filePop('');"><strong>파일첨부하기</strong></a>
							<ul class="fileList"></ul>
						</td>
					</tr> -->
					<?if($useMovie){?>
					<tr>
						<th>동영상 첨부</th>
						<td>
							<input type="file" name="moviename" class="size03" />									
						</td>
					</tr>
					<?}?>
				</tbody>
			</table>
			<input type="hidden" name="startdate" />
			<input type="hidden" name="enddate" />
			<input type="hidden" name="cmd" value="write" />
			<input type="hidden" name="user_fk" value="<?=$_SESSION['member_no']?>" />
			<input type="hidden" name="menu_fk" value="" />
			<input type="hidden" name="board_type" value="" />
			<input type="hidden" name="temp_no" value="" />
			
			<div class="writeForm_btn">
				<a href="javascript:;" id="w_btn" onclick="goSave(this)">확인</a>
				<a href="" >취소</a>
			</div>
			</form>
			</div>
		</div>
	</div>
	<!-- 프로그램끝 -->
	<? include "sub_footer/sub_footer.php" ?>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
</div>
</body>
</html>