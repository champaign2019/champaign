<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/schedule/Schedule.class.php";

include "config.php";

$objSchedule = new Schedule(0, $_REQUEST);

	$b_prm = new Schedule($pageRows, $_REQUEST, $uploadPath, $maxSaveSize);
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
<?
if (checkReferer($_SERVER["HTTP_REFERER"])) {
	$_REQUEST['uploadPath'] = $_SERVER['DOCUMENT_ROOT'].$uploadPath;

		if ($_REQUEST['cmd'] == 'write') {
			
			$_REQUEST = fileupload('moviename', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 동영상

			$rst = $objSchedule->insert($_REQUEST);

			if($rst > 0){
				$_REQUEST["startdate"] = substr($_REQUEST["startdate"], 0, 7)."-01" ;
				echo returnURLMsg($objSchedule->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"],'index.php'), 0, $_REQUEST), "저장이 완료되었습니다.");
			} else {
				echo returnURLMsg($objSchedule->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"],'index.php'), 0, $_REQUEST), "요청처리중 장애가 발생하였습니다.");
			}
		} else if($_REQUEST['cmd'] == 'reply'){

			$_REQUEST = fileupload('moviename', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 동영상

			$rst = $objSchedule->insertReply($_REQUEST);
			if($rst > 0){
				echo returnURLMsg($objSchedule->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"],'index.php'), 0, $_REQUEST), "저장이 완료되었습니다.");
			} else {
				echo returnURLMsg($objSchedule->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"],'index.php'), 0, $_REQUEST), "요청처리중 장애가 발생하였습니다.");
			}
		} else if($_REQUEST['cmd'] == 'edit') {

			$_REQUEST = fileupload('moviename', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 동영상

			if(b_dao.update(b_prm)){
				$_REQUEST["startdate"] = substr($_REQUEST["startdate"], 0, 7)."-01" ;
				echo returnURLMsg($objSchedule->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"],'index.php'), 0, $_REQUEST), "정상적으로 수정되었습니다.");
			} else {
				echo returnURLMsg($objSchedule->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"],'index.php'), 0, $_REQUEST), "요청처리중 장애가 발생하였습니다.");
			}
		} else if($_REQUEST['cmd'] == 'delete') {
			$rst = $objSchedule->delete($_REQUEST);				
			if($rst > 0){				
				$_REQUEST["startdate"] = substr($_REQUEST["startdate"], 0, 7)."-01" ;

				echo returnURLMsg($objSchedule->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"],'index.php'), 0, $_REQUEST), "삭제했습니다.");
			} else {
				echo returnURLMsg($objSchedule->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"],'index.php'), 0, $_REQUEST), "삭제에 실패했습니다.");
			}
		}
	} else {
		echo returnURLMsg($objSchedule->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"],'index.php'), 0, $_REQUEST), "올바른 접근 방법이 아닙니다.");
	}
?>
</body>
</html>
