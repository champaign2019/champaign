<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<? include $_SERVER['DOCUMENT_ROOT']."/include/headHtml.php" ?>
<style type="text/css">
.subleftmenu {
	list-style:none;
	margin:0;
	padding:0;
}
.subleftmenu li {
	border-bottom:1px solid #515155;
	background:#323337;
}
.subleftmenu a {
	display:block;
	color:#b5b2b2;
	font-size:14px;
	padding:11px 5px 11px 15px;
}
.sub_title{
	font-size:28px !important;
	font-weight:bold;
	color:#fff !important;
	padding:17px 5px 17px 15px !important;
}
.subleftmenu a:hover {
	background:#307dd4;
	color:#fff;
}
.sub_title:hover {
	background:#323337 !important;
	cursor:default;
}
</style>
</head>
<body>
<table style="width:200px; height:100%; padding:0px; border:0px; background-color: #323337">
	<tr>
		<td valign="top" width="199">
			<ul class="subleftmenu">
				<li><a href="#none" target="" class="submenu sub_title" >비젠소프트 </a></li>
				<li><a href="/manage/" target="_blank" class="submenu" > 관리자모드 </a></li>
				<li><a href="/consult/consult/index.php" target="mainFrame" class="submenu" > 온라인상담 </a></li>
				<li><a href="/consult/newconsult/index.php" target="mainFrame" class="submenu" > 온라인상담 - 답변형</a></li>
				<li><a href="/consult/formMail/write.php" target="mainFrame" class="submenu" > 메일상담 </a></li> 
				<li><a href="/consult/tel/index.php" target="mainFrame" class="submenu" > 전화상담 </a></li>
				<li><a href="/consult/moneymail/index.php" target="mainFrame" class="submenu" > 비용메일 </a></li>
				<li><a href="/board/reply/index.php" target="mainFrame" class="submenu" > 치료후기 - 답변형 </a></li>
				<li><a href="/board/reply_comment/index.php" target="mainFrame" class="submenu" > 치료후기 - 답변댓글형 </a></li>
				<li><a href="/reservation/write.php" target="mainFrame" class="submenu" > 온라인예약 </a></li>
				<li><a href="/reservation/confirm.php" target="mainFrame" class="submenu" > 예약확인 </a></li>
				<li><a href="/board/notice/index.php" target="mainFrame" class="submenu" > 공지/보도/동영상 </a></li>
				<li><a href="/board/NoticeListImage/index.php" target="mainFrame" class="submenu" > 목록이미지 공지/보도/동영상 </a></li>
				<li><a href="/board/CureInstance/index.php" target="mainFrame" class="submenu" > 치료사례(이미지목록형) </a></li>
				<li><a href="/board/beafphoto/index.php" target="mainFrame" class="submenu" > 치료사례(갤러리형) </a></li>
				<li><a href="/board/newbna/index.php" target="mainFrame" class="submenu" > 전후사진 갤러리 </a></li>
				<li><a href="/board/gallery/index.php" target="mainFrame" class="submenu" > 갤러리게시판 </a></li>
				<li><a href="/board/cardtype/index.php" target="mainFrame" class="submenu" > 카드형게시판 </a></li>
				<li><a href="/board/faq/index.php" target="mainFrame" class="submenu" > FAQ </a></li>
				<li><a href="/board/faq2/index.php" target="mainFrame" class="submenu" > FAQ - 분류별 </a></li>
				<li><a href="/board/common/index.php" target="mainFrame" class="submenu" > 공통게시판 </a></li>
				<li><a href="/hr/hr_notice/index.php" target="mainFrame" class="submenu" > 채용공고 </a></li>
				<li><a href="/hr/supply/index.php" target="mainFrame" class="submenu" > 입사지원 </a></li>
				<li><a href="/schedule/index.php" target="mainFrame" class="submenu" > 일정표 </a></li>
				<li><a href="/product/product_image/index.php" target="mainFrame" class="submenu" > 제품소개 - 이미지목록형 </a></li>
				<li><a href="/product/product_gallery/index.php" target="mainFrame" class="submenu" > 제품소개 - 갤러리형 </a></li>
				<li><a href="/member/write.php" target="mainFrame" class="submenu" > 회원가입 </a></li>
				<li><a href="/member/agree.php" target="mainFrame" class="submenu" > 회원약관 </a></li>
				<li><a href="/member/login.php" target="mainFrame" class="submenu" > 로그인 </a></li>
				<li><a href="/member/idpwFind.php" target="mainFrame" class="submenu" > ID/PW찾기 </a></li>
				<li><a href="/member/edit.php" target="mainFrame" class="submenu" > 회원정보변경 </a></li>
				<li><a href="/member/login_human.php" target="mainFrame" class="submenu" > 휴면계정 </a></li>
				<li><a href="/member/policy.php" target="mainFrame" class="submenu" > 개인정보취급방침 </a></li>
				<li><a href="/member/secede.php" target="mainFrame" class="submenu" > 회원탈퇴 </a></li>
				<li><a href="/include/logout.php" target="mainFrame" class="submenu" > 로그아웃 </a></li>
				<li><a href="/include/popup/mainPopup.php" target="mainFrame" class="submenu" > 팝업보기 </a></li>
				<li><a href="/board/landing/index.php" target="mainFrame" class="submenu" >  랜딩 페이지 </a></li>
			</ul>
		</td>
	</tr>
</table>
</body>
</html>
