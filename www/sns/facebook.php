<script>
  // This is called with the results from from FB.getLoginStatus().
  function statusChangeCallback(response) {
    if (response.status === 'connected') {
    	successLogin_facebook();
    } else if (response.status === 'not_authorized') {
    	alert('인증정보를 가져오는데 실패하였습니다. 잠시 후 다시 시도해 주세요');
    } else {
    	alert('페이스북에 로그인에 실패하였습니다.');
    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
	    FB.init({
	      appId      : '<?=LOGIN_FACEBOOK?>',
	      xfbml      : true,
	      version    : 'v2.11'
	    });
	    FB.AppEvents.logPageView();
  };
  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/ko_KR/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  function successLogin_facebook() {
    FB.api('/me', function(response) {
    	document.getElementById("id").value = response.id;
    	document.getElementById("password").value = "snspass";
    	if(response.email != undefined){
    		document.getElementById("email").value = response.email;
    	}
    	document.getElementById("name").value = response.name;
    	document.getElementById("from").value = "facebook";
    	
    	var form = document.getElementById("login");
		form.submit();
    });
  }
  
  function facebook_login(){
	  FB.login(function(response){
		  checkLoginState();
	  }, {scope: 'email'});
  }

  function facebook_logout(){
	  if(FB){
		  //FB.logout(function(response){});
	  }
  }
</script>