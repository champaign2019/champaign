<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />

<? 
include $_SERVER['DOCUMENT_ROOT']."/include/headHtml.php"; 
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";

?>
<script type="text/javascript" src="/js/naverLogin-1.0.1.js"></script>
<script type="text/javascript">
	var naver_id_login = new naver_id_login("<?=LOGIN_NAVER?>", "/sns/naver.php");
	//var state = naver_id_login.getUniqState();
	var forState = $("input[name=forState]", opener.document).val();
	naver_id_login.setState(forState);

	naver_id_login.init_naver_id_login_callback();
	naver_id_login.get_naver_userprofile();

	function naverLogin(profileParams){
		$(".initPrm").each(function(){
			var id = $(this).attr("id");
			if(id != 'from'){
				$(this).val(profileParams[id]);
			}
		});

		var html = $("#login", opener.document).html();
		html += $(".initPrm").parent().html();

		//$("#login", opener.document).html(html);
		$("#login", opener.document).attr("action", "/include/login.php");
		$("#login", opener.document).find('input[name=id]').val($('#id').val());
		$("#login", opener.document).find('input[name=name]').val($('#nickname').val());
		$("#login", opener.document).find('input[name=from]').val($('#from').val());
		$("#login", opener.document).find('input[name=password]').val('snspass');
		$("#login", opener.document).submit();
		self.close();
	}
</script>
</head>
<body>
	
	<input type="hidden" class="initPrm" name="id" id="id"/>
	<input type="hidden" class="initPrm" name="nickname" id="nickname"/>
	<input type="hidden" class="initPrm" name="from" id="from" value="naver"/>
	<input type="hidden" class="initPrm" name="password" id="password" value="snspass"/>

	<!--
	<input type="hidden" class="initPrm" name="password" id="password" value="snspass"/>
	<input type="hidden" class="initPrm" name="age" id="age"/>
	<input type="hidden" class="initPrm" name="enc_id" id="enc_id"/>
	<input type="hidden" class="initPrm" name="gender" id="gender"/>
	<input type="hidden" class="initPrm" name="profile_image" id="profile_image"/>
	<input type="hidden" class="initPrm" name="email" id="email"/>
	<input type="hidden" class="initPrm" name="birthday" id="birthday"/>
	-->
</body>
</html>