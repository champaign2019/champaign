<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/member/Member.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

$member = new Member($pageRows, "member", $_REQUEST);
$rowPageCount = $member->getCount($_REQUEST);
$result = $member->getList($_REQUEST);

$receiveEmail = $_REQUEST['receiveEmail'];

$rowPageCount;

if ($_REQUEST['cmd'] == 'group') {
	$_REQUEST['sendtype'] = 'email';
	$rowPageCount = $member->getCountForSend($_REQUEST);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/boardConfig/config.php" ?>
<script>
	function goSave(obj) {
	
	
	if(validation(obj)){
		
		$("#frm").submit();	
		
	}
}
</script>
</head>
<body> 
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<div id="wrap">
	<!-- canvas -->
	<div id="canvas">
		<!-- S T A R T :: headerArea-->
		<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/top.php" ?>
		<!-- E N D :: headerArea--> 
		
		<!-- S T A R T :: containerArea-->
		<div id="container">
			<div id="content">
				<div class="con_tit">
					<h2><?=$pageTitle?> - [E-Mail 보내기]</h2>
				</div>
				<!-- //con_tit -->
				<form name="frm" id="frm" action="<?=getSslCheckUrl($_SERVER['REQUEST_URI'], 'process.php')?>" method="post" enctype="multipart/form-data" >
				<div class="con">
					<!-- 내용 : s -->
					<div id="bbs">
						<div id="bwrite">
							<!-- :: =========================================================== :: -->
							<h3>내용</h3>
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="이메일쓰기입니다.">
								<colgroup>
									<col width="15%" />
									<col width="85%" />
								</colgroup>
								<tbody>
									<tr>
										<th scope="row"><label for="">받는 사람</label></th>
										<td>
											<textarea id="receiveman" class="mainsend w100m50" name="receiveman" cols="50" <?if("group" != $_REQUEST['cmd']) {?> data-value="받는사람 이메일을 입력하세요." <?}?>><?=$receiveEmail?></textarea>
										</td>
									</tr>
									<?
										if($_REQUEST['cmd'] == "group") {		// 회원검색으로 보낼 경우
									?>
									<tr>
										<th scope="row"><label for="">회원 발송</label></th>
										<td>
											총 <?=$rowPageCount[0]?>명
											<span class="color1">
												(메일이 등록되어 있지 않는 회원, 올바르지 않은 메일은 제외되어 회원검색에서 회원수와 회원발송 총수가 차이가 날 수 있습니다.)
											</span>
										</td>
									</tr>
									<?	} ?>
									<tr>
										<th scope="row"><label for="">보내는 사람</label></th>
										<td>
											<input type="text" id="sendman" name="sendman" value="<?=$_SESSION['admin_email']?>" class="input50p" style="ime-mode:inactive;"/>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for="">제목 </label></th>
										<td>
											<input type="text" id="title" name="title" value="" class="input50p" title="제목을 입력해주세요"/>
										</td>
									</tr>
									<tr>
										<td colspan="2">
											<textarea id="contents" name="contents" title="내용을 입력해주세요" style="width:100%" >
												<? include $_SERVER['DOCUMENT_ROOT']."/include/emailForm.php" ?>
											</textarea>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for="">첨부파일1</label></th>
										<td>
											<input type="file" id="filename" name="filename" value="" class="input92p"/>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for="">첨부파일2</label></th>
										<td>
											<input type="file" id="filename2" name="filename2" value="" class="input92p"/>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for="">첨부파일3</label></th>
										<td>
											<input type="file" id="filename3" name="filename3" value="" class="input92p"/>
										</td>
									</tr>
								</tbody>
							</table>
							<? if ($_REQUEST['cmd'] == 'group') { ?>
								<input type="hidden" name="cmd" id="cmd" value="group"/>
								<input type="hidden" name="sissms" id="sissms" value="<?=$_REQUEST['sissms']?>"/>
								<input type="hidden" name="stype" id="stype" value="<?=$_REQUEST['stype']?>"/>
								<input type="hidden" name="sval" id="sval" value="<?=$_REQUEST['sval']?>"/>
								<input type="hidden" name="ssecession" id="ssecession" value="<?=$_REQUEST['ssecession']?>"/>
								<input type="hidden" name="sisemail" id="sisemail" value="<?=$_REQUEST['sisemail']?>"/>
								<input type="hidden" name="sdatetype" id="sdatetype" value="<?=$_REQUEST['sdatetype']?>"/>
								<input type="hidden" name="sstartdate" id="sstartdate" value="<?=$_REQUEST['sstartdate']?>"/>
								<input type="hidden" name="senddate" id="senddate" value="<?=$_REQUEST['senddate']?>"/>
								<? } else { ?>
								<input type="hidden" name="cmd" id="cmd" value="write"/>
								<? } ?>
							<div class="btn">
								<div class="btnLeft">
									<a class="btns" href="index.php"><strong><?=getMsg('btn.list')?></strong> </a>
								</div>
								<div class="btnRight">
									<a class="btns" href="javascript:;" onclick="goSave(this)"><strong><?=getMsg('btn.save')?></strong> </a>
								</div>
							</div>
							<!--//btn-->
						</div>
						<!-- //bwrite -->
					</div>
					<!-- //bbs --> 
					<!-- 내용 : e -->
				</div>
				<!--//con -->
				</form>
			</div>
			<!--//content -->
		</div>
		<!--//container --> 
		<!-- E N D :: containerArea-->
	</div>
	<!--//canvas -->
</div>
<!--//wrap -->

</body>
</html>