<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/email/Email.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

$email = new Email($pageRows, $tablename, $listtablename, $_REQUEST);
$rowPageCount = $email->getCount($_REQUEST);
$result = $email->getList($_REQUEST);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
<script>
function groupDelete() {	
	if (isSeleted(document.frm.no) ){
		document.frm.submit();
	} else {
		alert("<?=getMsg('alert.text.delete')?>");
	}
}

function goSearch() {
	$("#searchForm").submit();
}
</script>
</head>
<body> 
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<div id="wrap">
	<!-- canvas -->
	<div id="canvas">
		<!-- S T A R T :: headerArea-->
		<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/top.php" ?>
		<!-- E N D :: headerArea--> 
		
		<!-- S T A R T :: containerArea-->
		<div id="container">
			<div id="content">
				<div class="con_tit">
					<h2><?=$pageTitle?> - [발송내역]</h2>
				</div>
				<!-- //con_tit -->
				<div class="con">
					<!-- 내용 : s -->
					<div id="bbs">
						<div id="blist">
							<div class="btn">
								<div class="btnLeft">
									<span >총 <?=$rowPageCount[0]?> 명</span><span>&nbsp;&nbsp;|&nbsp;&nbsp;<?=$email->reqPageNo?>/<?=$rowPageCount[1]?> 페이지</span>
								</div>
							</div>
							<!--//btn -->
							<div class="table_wrap">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="지점관리목록입니다.">
								<colgroup>
								<col class="w5" />
								<col  />
								<col class="w8" />
								<col class="w10" />
								
								</colgroup>
								<thead>
									<tr>
										<th class="first" scope="col">번호</th>
										<th scope="col"><?=getMsg("th.title")?></th>
										<th scope="col">발송자수</th>
										<th scope="col">발송일</th>
									</tr>
								</thead>
								<tbody>
								<? if ($rowPageCount[0] == 0) { ?>
									<tr>
										<td colspan="4">등록된 발송내역이 없습니다</td>			
									</tr>
							
								<?
									} else {
										$targetUrl = "";
										while ($row=mysql_fetch_assoc($result)) {
											$targetUrl = "style='cursor:pointer;' onclick=\"location.href='".$email->getQueryString("read.php", $row['no'], $_REQUEST)."'\"";
								?>
											<tr>
												<td class="first" <?=$targetUrl?>><?=$row['no']?></td>
												<td class="title" <?=$targetUrl?>><?=$row['title']?></td>
												<td <?=$targetUrl?>><?=$row['totalcount']?></td>
												<td class="last" <?=$targetUrl?>><?=$row['registdate']?></td>
											</tr>
								<?
										}
									}
								?>	
								</tbody>
							</table>
							</div>
								
							<!-- 페이징 처리 -->
							<?=pageList($email->reqPageNo, $rowPageCount[1], $email->getQueryString('index.php', 0, $_REQUEST))?>
							<!-- //페이징 처리 -->
							<input type="hidden" name="stype" id="stype" value="<?=$_REQUEST['stype']?>"/>
							<input type="hidden" name="sval" id="sval" value="<?=$_REQUEST['sval']?>"/>

							<form name="searchForm" id="searchForm" action="index.php" method="post">
								<div class="search">
									<select name="stype" title="검색을 선택해주세요">
										<option value="all" <?=getSelected($_REQUEST['stype'], "all") ?>><?=getMsg('lable.option.all')?></option>
										<option value="title" <?=getSelected($_REQUEST['stype'], "title") ?>>제목</option>
										<option value="contents" <?=getSelected($_REQUEST['stype'], "contents") ?>><?=getMsg('lable.option.contents')?></option>
										<option value="name" <?=getSelected($_REQUEST['stype'], "name") ?>>작성자</option>
									</select>
									<input type="text" name="sval" value="<?=$_REQUEST['sval']?>" title="검색할 내용을 입력해주세요" />
									<input type="submit" class="se_btn " value="<?=getMsg('btn.search')?>" />
								</div>
								</form>
							</div>
						</div>
						<!-- //blist -->
					</div>
					<!-- //bbs --> 
					<!-- 내용 : e -->
				</div>
				<!--//con -->
			</div>
			<!--//content -->
		</div>
		<!--//container --> 
		<!-- E N D :: containerArea-->
	</div>
	<!--//canvas -->
</div>
<!--//wrap -->

</body>
</html>