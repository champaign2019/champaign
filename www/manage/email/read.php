<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/email/Email.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

$email = new Email($pageRows, $tablename, $listtablename, $_REQUEST);
$_REQUEST['orderby'] = $orderby; //정렬 배열 선언
$data = $email->getData($_REQUEST['no']);

$result = $email->getListMailList($_REQUEST['no']);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
<script type="text/javascript">
	function display(){
		var dp = $("#emailMemberList dd:eq(0)").css("display");
		if( dp == "inline"){
			$("#emailMemberList dd:eq(0)").animate({height:"0px"},1000, function(){
				$("#emailMemberList dd:eq(0)").animate({width:"0px"},700, function(){
					$("#emailMemberList dd:eq(0)").css({display:"none"});
				});
				$("#emailMemberList dd:eq(1) > a >strong").text("이메일 발송 목록 보이기");
			});
		} else if( dp == "none"){
			$("#emailMemberList dd:eq(1) > a >strong").text("이메일 발송 목록 감추기");
			$("#emailMemberList dd:eq(0)").css({height:"0px",width:"1px",display:"inline"});
			$("#emailMemberList dd:eq(0)").animate({width:"130px"},800, function(){
				var a = $("#emailMemberList dd:eq(0) > li").length;
				for(var i = 0; i < a; i++){
					$("#emailMemberList dd:eq(0)").animate({height:12 * (i+1)},1000/a);
				}
			});
		}
	}
</script>
</head>
<body> 
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<div id="wrap">
	<!-- canvas -->
	<div id="canvas">
		<!-- S T A R T :: headerArea-->
		<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/top.php" ?>
		<!-- E N D :: headerArea--> 
		
		<!-- S T A R T :: containerArea-->
		<div id="container">
			<div id="content">
				<div class="con_tit">
					<h2><?=$pageTitle?> - [<?=getMsg('th.detail')?>]</h2>
				</div>
				<!-- //con_tit -->
				<div class="con">
					<!-- 내용 : s -->
					<div id="bbs">
						<div id="bread">
							<h3>발송 정보</h3>
							<div class="table_wrap">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리 기본내용입니다.">
								<colgroup>
									<col width="15%" />
									<col width="35%" />
									<col width="15%" />
									<col width="35%" />
								</colgroup>
								<tbody>
								
									<tr>
										<th scope="row"><label for="">수신인</label></th>
										<td colspan="3">
										<? if($data['totalcount'] > 5) { ?>
											총<?=$data['totalcount']?>명<p/>
										<? } ?>
											<? while ($row=mysql_fetch_assoc($result)) { ?>
											<?=$row['receiveemail']?><br/>
											<? } ?>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for="">발신인</label></th>
										<td><?=$data['sendman']?></td>
										<th scope="row"><label for="">발신일</label></th>
										<td><?=$data['registdate']?></td>
									</tr>
									<tr>
										<th scope="row"><label for="">제목</label></th>
										<td colspan="3"><?=$data['title']?></td>
									</tr>
									<tr>
										<td colspan="4"><div style="all:default;"><?=stripslashes($data['contents'])?></div></td>
									</tr>
									<? if ($data['filename']) { ?>
									<tr>
										<td colspan="4">
											<a href="/lib/download.php?path=<?=$uploadPath?>&vf=<?=$data['filename']?>&af=<?=$data['filename_org']?>" target="_blank"><?=$data['filename_org']?> [<?=getFileSize($data['filesize'])?>]</a>
										</td>
									</tr>
									<? } ?>
									<? if ($data['filename2']) { ?>
									<tr>
										<td colspan="4">
											<a href="/lib/download.php?path=<?=$uploadPath?>&vf=<?=$data['filename2']?>&af=<?=$data['filename2_org']?>" target="_blank"><?=$data['filename2_org']?> [<?=getFileSize($data['filesize2'])?>]</a>
										</td>
									</tr>
									<? } ?>
									<? if ($data['filename3']) { ?>
									<tr>
										<td colspan="4">
											<a href="/lib/download.php?path=<?=$uploadPath?>&vf=<?=$data['filename3']?>&af=<?=$data['filename3_org']?>" target="_blank"><?=$data['filename3_org']?> [<?=getFileSize($data['filesize3'])?>]</a>
										</td>
									</tr>
									<? } ?>
								</tbody>
							</table>
							</div>
							<div class="btn">
								<div class="btnLeft">
									<a class="btns" href="<?=$email->getQueryString('index.php', 0, $_REQUEST)?>"><strong><?=getMsg('btn.list')?></strong></a>
								</div>
							</div>
							<!--//btn-->
						</div>
						<!-- //bread -->
					</div>
					<!-- //bbs --> 
					<!-- 내용 : e -->
				</div>
				<!--//con -->
			</div>
			<!--//content -->
		</div>
		<!--//container --> 
		<!-- E N D :: containerArea-->
	</div>
	<!--//canvas -->
</div>
<!--//wrap -->

</body>
</html>