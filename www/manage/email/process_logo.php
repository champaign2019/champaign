<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/email/Email.class.php";
include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
</head>
<body>
<?
if (checkReferer($_SERVER["HTTP_REFERER"])) {

	$email = new Email($pageRows, "logo", "logo", $_REQUEST);
	$_REQUEST['uploadPath'] = $_SERVER['DOCUMENT_ROOT']."/img/";
	
	if ($_REQUEST['cmd'] == 'editLogo') {

		$_REQUEST = fileupload('logoimage', $_SERVER['DOCUMENT_ROOT']."/img/", $_REQUEST, false, $maxSaveSize);	// 이미지1
		
		$r = $email->editLogo($_REQUEST);

		if ($r == 0) {
			echo returnURLMsg($email->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'logo.php'), 0, $_REQUEST), getMsg('result.text.error'));
		} else {
			echo returnURLMsg($email->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'logo.php'), 0, $_REQUEST), getMsg('result.text.update'));
		}
	}
} else {
	echo returnURLMsg($admin->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.error'));
}
?>


</body>
</html>