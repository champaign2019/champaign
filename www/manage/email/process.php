<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/member/Member.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/email/Email.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/email/SendMail.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
</head>
<body>
<?
if (checkReferer($_SERVER["HTTP_REFERER"])) {

	$member = new Member($pageRows, "member", $_REQUEST);
	$emailO = new Email($pageRows, $tablename, $listtablename, $_REQUEST);
	$sendmail = new SendMail();
	$_REQUEST['sendtype'] = 'email';

	$receive;
	$email;
	$tempEmail;

	if ($_REQUEST['cmd'] == 'group') {
		$receive = $member->getListForSend($_REQUEST);
		while ($row=mysql_fetch_assoc($receive)) {
			$email .= $row['email'].", ";
		}
	}

	if ($_REQUEST['receiveman']) {
		$tempEmail = str_replace(";", ",", $_REQUEST['receiveman']);
		$tempEmail = str_replace("\n", ",", $tempEmail);

		$email .= $tempEmail;

	}

	if ($_REQUEST['cmd'] == 'write' || $_REQUEST['cmd'] == 'group') {
		$filename = array();
		$filename_org = array();
		$_REQUEST['filesize'] = 0;
		$_REQUEST['filesize2'] = 0;
		$_REQUEST['filesize3'] = 0;
		//첨부파일1
		$files = $_FILES['filename'];
		$tmp_name = $files['tmp_name'];
		if ($tmp_name) {
			
			$org_name = $files['name'];
			$file_name = getRandFileName($org_name);
			if (!$files['error']) {
				move_uploaded_file($tmp_name, $_SERVER['DOCUMENT_ROOT'].$uploadPath.$file_name);
			}
			
			$filename[0] = $file_name;
			$filename_org[0] = $org_name;
			// db insert value
			$_REQUEST['filename'] = $file_name;
			$_REQUEST['filename_org'] = $org_name;
			$_REQUEST['filesize']=$files['size'];
		}
		//첨부파일2
		$files = $_FILES['filename2'];
		$tmp_name = $files['tmp_name'];
		if ($tmp_name) {
			
			$org_name = $files['name'];
			$file_name = getRandFileName($org_name);
			if (!$files['error']) {
				move_uploaded_file($tmp_name, $_SERVER['DOCUMENT_ROOT'].$uploadPath.$file_name);
			}
			
			$filename[1] = $file_name;
			$filename_org[1] = $org_name;
			// db insert value
			$_REQUEST['filename2'] = $file_name;
			$_REQUEST['filename2_org'] = $org_name;
			$_REQUEST['filesize2']=$files['size'];
		}
		//첨부파일3
		$files = $_FILES['filename3'];
		$tmp_name = $files['tmp_name'];
		if ($tmp_name) {
			
			$org_name = $files['name'];
			$file_name = getRandFileName($org_name);
			if (!$files['error']) {
				move_uploaded_file($tmp_name, $_SERVER['DOCUMENT_ROOT'].$uploadPath.$file_name);
			}
			
			$filename[2] = $file_name;
			$filename_org[2] = $org_name;
			// db insert value
			$_REQUEST['filename3'] = $file_name;
			$_REQUEST['filename3_org'] = $org_name;
			$_REQUEST['filesize3']=$files['size'];
		}
		
		$r = $emailO->insert($_REQUEST);
		if ($r == 0) {
			echo returnURLMsg($emailO->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.error'));
		} else {
			// 발송목록 insert
			$emailO->insertMailList($r, $email);
			
			// 메일발송
			$sendmail->sendFile($_REQUEST['sendman'], "", $email, $_REQUEST['title'], str_replace("\\", "", $_REQUEST['contents']), $uploadPath, $filename, $filename_org);

			// 총 발송수 update
			$totalcount = sizeof(split(",", $email));
			$emailO->updateTotalcount($totalcount, $r);

			if ($_REQUEST['emailpop'] == 1) {
//				echo popupCloseRefresh('총 '.$totalcount.'건이 발송되었습니다.');
			} else {
//				echo returnURLMsg($emailO->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), '총 '.$totalcount.'건이 발송되었습니다.');
			}
		}

	} else if ($_REQUEST['cmd'] == 'groupDelete') {

		$no = $_REQUEST['no'];
		
		$r = 0;
		for ($i=0; $i<count($no); $i++) {
			$r += $emailO->delete($no[$i]);
		}

		if ($r > 0) {
			echo returnURLMsg($admin->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.countdelete',$r));
		} else {
			echo returnURLMsg($admin->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.error'));
		}

	}

} else {
	echo returnURLMsg($admin->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.error'));
}
?>


</body>
</html>