<?
	$pageTitle 		= "이메일";
	$tablename		= "emailhistory";						// 게시판 테이블명
	$listtablename	= "emaillist";							// 보낸 메일의 발송자 대상명단
	$branch 			= true;										// 지점 사용유무  
	$pageRows		= 10;
	$uploadPath		= "/upload/email/";						// 파일, 동영상 첨부 경로
	$gradeno			= 2;									// 관리자 권한 기준 [지점 사용시]
	$isreceiver 		= 1;									// 수신여부 기능 (0 = 미사용, 1 = 사용)
	$isbirthday 		= 0;									// 생일여부 기능 (0 = 미사용, 1 = 사용)
	
	$maxSaveSize	 	= 50*1024*1024;							// 50Mb
	$userCon 		= false;

	$clinic	  		= true;							// 진료과목 지점별 사용유무
	$doctor			= true;						// 의료진

	$textAreaEditor	= true;							// 에디터 사용여부
	$editorIgnore= "|0|";								//에디터 미사용 배열 구분자 ex : '|0|1|'	
?>