<? session_start(); 
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Doctortime.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Hospital.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

$doctortime = new Doctortime($pageRows, $_REQUEST);
$hospital = new Hospital($pageRows, $_REQUEST);
$rowPageCount = $doctortime->getCount($_REQUEST);
$result = $doctortime->getList($_REQUEST);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
<script>
function groupDelete() {	
	if ( $('[name^=no]:checked').length > 0 ) {
		document.frm.submit();
	} else {
		alert("<?=getMsg('alert.text.delete')?>");
	}
}
</script>
</head>
<body> 
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<div id="wrap">
	<!-- canvas -->
	<div id="canvas">
		<!-- S T A R T :: headerArea-->
		<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/top.php" ?>
		<!-- E N D :: headerArea--> 
		
		<!-- S T A R T :: containerArea-->
		<div id="container">
			<div id="content">
				<div class="con_tit">
					<h2><?=$pageTitle?> - [<?=getMsg("th.list")?>]</h2>
				</div>
				<!-- //con_tit -->
				<div class="con">
					<!-- 내용 : s -->
					<div id="bbs">
						<div id="blist">
							<p><span><strong><?=getMsg('th.total')?> <?=$rowPageCount[0]?><?=getMsg('th.amount')?></strong>  |  <?=$doctortime->reqPageNo?>/<?=$rowPageCount[1]?><?=getMsg('th.page')?></span></p>
							<div class="table_wrap">
							<form name="frm" id="frm" action="process.php" method="post">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리목록입니다.">
								<colgroup>
									<col class="w2" />
									<col class="w3" />
									<col class="w5" />
									<col class="w5" />
									<col class="w7" />
									<col class="w9" />
									<col class="w9" />
									<col class="w9" />
									<col class="w9" />
									<col class="w9" />
									<col class="w9" />
									<col class="w9" />
									<col class="w9" />
									<col class="w6" />
								</colgroup>
								<thead>
									<tr>
										<th scope="col" class="first">
										<label class="b_first_c"><input type="checkbox" name="allChk" id="allChk" onClick="check(this, document.getElementsByName('no[]'))"/>
										<i></i>
										</label>
										</th>
										<th scope="col"><?=getMsg("th.no")?></th>
										<th scope="col">병원명</th>
										<?
										if($doctor){
										?>
										<th scope="col"><?=getMsg('th.doctor_fk')?></th>
										<?
										}
										?>
										<th scope="col">시작일</th>
										<th scope="col" style=" color:#FF4836;">일요일</th>
										<th scope="col">월요일</th>
										<th scope="col">화요일</th>
										<th scope="col">수요일</th>
										<th scope="col">목요일</th>
										<th scope="col">금요일</th>
										<th scope="col" style=" color:#1559AA;">토요일</th>
										<th scope="col" style=" color:#FF4836;">공휴일</th>
										<th scope="col" class="last">예약간격</th>
									</tr>
								</thead>
								<tbody>
								<? if ($rowPageCount[0] == 0) { ?>
									<tr>
										<td class="frist" colspan="14">등록된 의료진이 없습니다.</td>
									</tr>
								<?
									 } else {
										$targetUrl = "";
										while ($row = mysql_fetch_assoc($result)) {
											$targetUrl = "style='cursor:pointer;' onclick=\"location.href='".$doctortime->getQueryString('edit.php', $row[no], $_REQUEST)."'\"";
								?>
									<tr>
										<td class="first">
										<label class="b_nor_c"><input type="checkbox" name="no[]" id="no" value="<?=$row[no]?>"/>
										<i></i>
										</label>
										</td>
										<td <?=$targetUrl?>><?=$row[no]?></td>
										<td <?=$targetUrl?>><?=$row[hospital_name]?></td>
										<?
										if($doctor){
										?>
										<td <?=$targetUrl?>><?=$row[doctor_name]?></td>
										<?
										}
										?>
										<td <?=$targetUrl?>><?=$row[startday]?></td>
										<td style="background-color:#ffefef;" <?=$targetUrl?>><?=$row[sun_start_time]?> ~ <?=$row[sun_end_time]?></td>
										<td <?=$targetUrl?>><?=$row[mon_start_time]?> ~ <?=$row[mon_end_time]?></td>
										<td <?=$targetUrl?>><?=$row[tue_start_time]?> ~ <?=$row[tue_end_time]?></td>
										<td <?=$targetUrl?>><?=$row[wed_start_time]?> ~ <?=$row[wed_end_time]?></td>
										<td <?=$targetUrl?>><?=$row[thu_start_time]?> ~ <?=$row[thu_end_time]?></td>
										<td <?=$targetUrl?>><?=$row[fri_start_time]?> ~ <?=$row[fri_end_time]?></td>
										<td style="background-color:#effaff;" <?=$targetUrl?>><?=$row[sat_start_time]?> ~ <?=$row[sat_end_time]?></td>
										<td style="background-color:#ffefef;" <?=$targetUrl?>><?=$row[spe_start_time]?> ~ <?=$row[spe_end_time]?></td>
										<td class="last" <?=$targetUrl?>><?=getTimeInterval($row[time_interval])?></td>
									</tr>
								<?
										}
									 }
								?>
								</tbody>
							</table>
								<input type="hidden" name="cmd" id="cmd" value="groupDelete"/>
								<input type="hidden" name="stype" id="stype" value="<?=$_REQUEST[stype]?>"/>
								<input type="hidden" name="sval" id="sval" value="<?=$_REQUEST[sval]?>"/>
							</form>
							</div>
							<div class="btn">
								<div class="btnLeft">
									<a class="btns" href="#" onclick="groupDelete();"><strong><?=getMsg('btn.delete')?></strong> </a>
								</div>
								<div class="btnRight">
									<a class="wbtn" href="write.php"><strong><?=getMsg('btn.write')?></strong> </a>
								</div>
							</div>
							<!--//btn-->
							<!-- 페이징 처리 -->
							<?=pageList($doctortime->reqPageNo, $rowPageCount[1], $doctortime->getQueryString('index.php', 0, $_REQUEST))?>
							<!-- //페이징 처리 -->
							<form name="searchForm" id="searchForm" action="index.php" method="post">
								<div class="search">
									<select name="shospital_fk" id="shospital_fk" title="지점을 선택해주세요" onchange="$('#searchForm').submit();">
										<?=$hospital->selectBoxList(0,0,0,$_REQUEST[shospital_fk])?>
									</select>
								</div>
							</form>
							<!-- //search --> 
						</div>
						<!-- //blist -->
					</div>
					<!-- //bbs --> 
					<!-- 내용 : e -->
				</div>
				<!--//con -->
			</div>
			<!--//content -->
		</div>
		<!--//container --> 
		<!-- E N D :: containerArea-->
	</div>
	<!--//canvas -->
</div>
<!--//wrap -->

</body>
</html>