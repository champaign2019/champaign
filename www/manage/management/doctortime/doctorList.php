<? session_start(); 
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Doctor.class.php";

include "config.php";

$doctor = new Doctor($pageRows, $_REQUEST);

$hospital_fk = $_REQUEST['hospital_fk']=='' ? 1 : $_REQUEST['hospital_fk'];	// 지점PK
$clinic_fk = $_REQUEST['clinic_fk']=='' ? 1 : $_REQUEST['clinic_fk'];			// 진료과목 PK
$doctor_fk = $_REQUEST['doctor_fk']=='' ? 1 : $_REQUEST['doctor_fk'];			// 선택되어질 의료진
$type = $_REQUEST['type']=='' ? 0 : $_REQUEST['type'];							// 타입


$result = $doctor->selectBoxList($type, 0, $hospital_fk, $clinic_fk, $doctor_fk);
?>
<?=$result?>