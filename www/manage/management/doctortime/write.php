<? session_start(); 
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Doctortime.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Hospital.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

$doctortime = new Doctortime($pageRows, $_REQUEST);
$hospital = new Hospital($pageRows, $_REQUEST);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/boardConfig/config.php" ?>
<script>

function goSave(obj) {
	
	
	if(validation(obj)){
		
		nowday = new Date();
		var exp = /-/g;
		if ( ((nowday.getYear()*10000)+((nowday.getMonth()+1)*100)+nowday.getDate()) > $("#startday").val().replace(exp,"")) {
			alert("적용시작일이 오늘 날짜 보다 이전입니다. 오늘 이후로 선택하세요."); 
			$("#startday").focus();
	  		return;
		}
		
		
		$("#frm").submit();	
		
	}
}

// 달력부분
$(window).load(function() {
	$('#startday').datetimepicker({timepicker:false,format: "Y-m-d"});
		$("#button").click(function(){
			$('#startday').datetimepicker('show');
		})	
	//initCal({id:"startday",type:"day",today:"y"});			
	
});
</script>
</head>
<body> 
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<div id="wrap">
	<!-- canvas -->
	<div id="canvas">
		<!-- S T A R T :: headerArea-->
		<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/top.php" ?>
		<!-- E N D :: headerArea--> 
		
		<!-- S T A R T :: containerArea-->
		<div id="container">
			<div id="content">
				<div class="con_tit">
					<h2><?=$pageTitle?> - [<?=getMsg('th.write')?>]</h2>
				</div>
				<!-- //con_tit -->
				<div class="con">
					<!-- 내용 : s -->
					<div id="bbs">
						<div id="bread">
							<h3><?=getMsg('th.branch_sel')?></h3>
							<form name="frm" id="frm" action="<?=getSslCheckUrl($_SERVER['REQUEST_URI'], 'process.php')?>" method="post">
							<div class="table_wrap">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리 기본내용입니다.">
								<colgroup>
									<col width="15%" />
									<col width="35%" />
									<col width="15%" />
									<col width="35%" />
								</colgroup>
								<tbody>
									<?
									if($branch || $doctor){
									?>
									<tr>
										<?
										if($branch){
										?>
										<th scope="row"><label for=""><?=getMsg('th.hospital_fk')?></label></th>
										<td colspan="<?=$doctor ? "1" : "3"?>" >
											<select name="hospital_fk" id="hospital_fk" title="지점을 선택해주세요" data-value="<?=getMsg('alert.text.shospital_fk')?>">
												<?=$hospital->selectBoxList(0,0,0,$_REQUEST['shospital_fk'])?>
											</select>
										</td>
										<?
										}
										?>
										<?
										if($doctor){
										?>
										<th scope="row"><label for="">의료진</label></th>
										<td colspan="<?=$branch ? "1" : "3"?>" >
											<select name="doctor_fk" id="doctor_fk" data-value="의료진을 선택해 주세요.">
												<option value="0">의료진선택</option>
											</select>
										</td>
										<?
										}
										?>
									</tr>
									<?
									}
									?>
									<tr>
										<th scope="row"><label for="">적용시작일</label></th>
										<td>
											<input type="text" id="startday" name="startday" value="" readonly style="width:70px"  data-value="적용시작일을 선택해 주세요."/>&nbsp;
											<span id="CalstartdayIcon">
												<button type="button" id="button"><img src="/manage/img/calendar_icon.png" id="CalregistdateIconImg" style="cursor:pointer;"/></button>
											</span>
										</td>
										<th scope="row"><label for="">*예약시간간격</label></th>
										<td>
											<select NAME="time_interval">
												<option value="1">20분</option>
												<option value="2" selected>30분</option>
												<option value="3">60분</option>
												<option value="4">120분</option>
											</select>
										</td>
									</tr>
									<tr>
										<td colspan="4"><span style='color:#ff4400'>* 00:00~00:00으로 지정하면 예약불가로 설정됩니다.(의료진별 휴진시에 활용하세요.)</span></td>
									</tr>
									<tr>
										<th scope="row"><label for="">일요일</label></th>
										<td colspan="3">
											<select NAME="sun_start_time">
												<?=getTimeList("10:00:00")?>
											</select>
											~
											<select NAME="sun_end_time">
												<?=getTimeList("19:00:00")?>
											</select>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for="">월요일</label></th>
										<td colspan="3">
											<select NAME="mon_start_time">
												<?=getTimeList("10:00:00")?>
											</select>
											~
											<select NAME="mon_end_time">
												<?=getTimeList("19:00:00")?>
											</select>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for="">화요일</label></th>
										<td colspan="3">
											<select NAME="tue_start_time">
												<?=getTimeList("10:00:00")?>
											</select>
											~
											<select NAME="tue_end_time">
												<?=getTimeList("19:00:00")?>
											</select>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for="">수요일</label></th>
										<td colspan="3">
											<select NAME="wed_start_time">
												<?=getTimeList("10:00:00")?>
											</select>
											~
											<select NAME="wed_end_time">
												<?=getTimeList("19:00:00")?>
											</select>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for="">목요일</label></th>
										<td colspan="3">
											<select NAME="thu_start_time">
												<?=getTimeList("10:00:00")?>
											</select>
											~
											<select NAME="thu_end_time">
												<?=getTimeList("19:00:00")?>
											</select>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for="">금요일</label></th>
										<td colspan="3">
											<select NAME="fri_start_time">
												<?=getTimeList("10:00:00")?>
											</select>
											~
											<select NAME="fri_end_time">
												<?=getTimeList("19:00:00")?>
											</select>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for="">토요일</label></th>
										<td colspan="3">
											<select NAME="sat_start_time">
												<?=getTimeList("10:00:00")?>
											</select>
											~
											<select NAME="sat_end_time">
												<?=getTimeList("19:00:00")?>
											</select>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for="">공휴일</label></th>
										<td colspan="3">
											<select NAME="spe_start_time">
												<?=getTimeList("10:00:00")?>
											</select>
											~
											<select NAME="spe_end_time">
												<?=getTimeList("19:00:00")?>
											</select>
											<img src="/manage/img/question_btn.gif" class="helpComment" id="Holy" alt="도움말 이미지"/>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for="">점심시간</label></th>
										<td colspan="3">
											<select NAME="lunch_start_time">
												<?=getTimeList("12:00:00")?>
											</select>
											~
											<select NAME="lunch_end_time">
												<?=getTimeList("13:00:00")?>
											</select>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for="">저녁시간</label></th>
										<td colspan="3">
											<select NAME="dinner_start_time">
												<?=getTimeList("19:00:00")?>
											</select>
											~
											<select NAME="dinner_end_time">
												<?=getTimeList("20:00:00")?>
											</select>
										</td>
									</tr>
								</tbody>
							</table>
							<input type="hidden" name="stype" id="stype" value="<?=$_REQUEST[stype]?>"/>
							<input type="hidden" name="sval" id="sval" value="<?=$_REQUEST[sval]?>"/>
							<? if (!$branch) { ?>
							<input type="hidden" name="hospital_fk" id="hospital_fk" value="<?=DEFAULT_BRANCH_NO?>"/>
							<?
							}
							?>
							<? if (!$doctor) { ?>
							<input type="hidden" name="doctor_fk" id="doctor_fk" value="<?=DEFAULT_DOCTOR_NO?>"/>
							<?
							}
							?>
							<input type="hidden" name="cmd" value="write">
							</div>
							<div class="btn">
								<div class="btnLeft">
									<a class="btns" href="<?=$doctortime->getQueryString('index.php', 0, $_REQUEST)?>"><strong><?=getMsg('btn.list')?></strong></a>
								</div>
								<div class="btnRight">
									<a class="btns" href="#" onclick="goSave(this);"><strong><?=getMsg('btn.save')?></strong></a>
								</div>
							</div>
							</form>
							
							<!--//btn-->
						</div>
						<!-- //bread -->
					</div>
					<!-- //bbs --> 
					<!-- 내용 : e -->
				</div>
				<!--//con -->
			</div>
			<!--//content -->
		</div>
		<!--//container --> 
		<!-- E N D :: containerArea-->
	</div>
	<!--//canvas -->
</div>
<!--//wrap -->
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/helpComment.php" ?>
</body>
</html>