<? session_start(); 
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Hospital.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

$hospital = new Hospital($pageRows, $_REQUEST);
$_REQUEST['orderby'] = $orderby; //정렬 배열 선언
$data = $hospital->getData($_REQUEST['no']);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
</head>
<body> 
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<div id="wrap">
	<!-- canvas -->
	<div id="canvas">
		<!-- S T A R T :: headerArea-->
		<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/top.php" ?>
		<!-- E N D :: headerArea--> 
		
		<!-- S T A R T :: containerArea-->
		<div id="container">
			<div id="content">
				<div class="con_tit">
					<h2><?=$pageTitle?> - [<?=getMsg('th.detail')?>]</h2>
				</div>
				<!-- //con_tit -->
				<div class="con">
					<!-- 내용 : s -->
					<div id="bbs">
						<div id="bread">
							<div class="table_wrap">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리 기본내용입니다.">
								<colgroup>
									<col width="250px" />
									<col width="*" />
									<col width="250px" />
									<col width="*" />
								</colgroup>
								<tbody>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.hospitalname')?></label></th>
										<td colspan="3"><?=$data[name]?></td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.consult_write_user')?></label></th>
										<td colspan="3"><?=getSmsName($data[consult_write_user])?></td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.consult_write_admin')?></label></th>
										<td colspan="3"><?=getSmsName($data[consult_write_admin])?></td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.consult_reply_user')?></label></th>
										<td colspan="3"><?=getSmsName($data[consult_reply_user])?></td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.consult_admin_tel')?></label></th>
										<td colspan="3"><?=$data[consult_admin_tel]?></td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.consult_sms_timechk')?></label></th>
										<td colspan="3"><?=getSmsTimeLimitName($data[consult_sms_timechk])?></td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.consult_sms_time')?></label></th>
										<td colspan="3"><?=$data[consult_sms_starttime]." ~ ".$data[consult_sms_endtime]?></td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.consult_sms_msg')?></label></th>
										<td colspan="3"><?=$data[consult_sms_msg]?></td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.reser_write_user')?></label></th>
										<td colspan="3"><?=getSmsName($data[reser_write_user])?></td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.reser_write_admin')?></label></th>
										<td colspan="3"><?=getSmsName($data[reser_write_admin])?></td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.reser_edit_user')?></label></th>
										<td colspan="3"><?=getSmsName($data[reser_edit_user])?></td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.reser_edit_admin')?></label></th>
										<td colspan="3"><?=getSmsName($data[reser_edit_admin])?></td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.reser_cancel_user')?></label></th>
										<td colspan="3"><?=getSmsName($data[reser_cancel_user])?></td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.reser_cancel_admin')?></label></th>
										<td colspan="3"><?=getSmsName($data[reser_cancel_admin])?></td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.reser_end_user')?></label></th>
										<td colspan="3"><?=getSmsName($data[reser_end_user])?></td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.reser_admin_tel')?></label></th>
										<td colspan="3"><?=$data[reser_admin_tel]?></td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.reser_sms_timechk')?></label></th>
										<td colspan="3"><?=getSmsTimeLimitName($data[reser_sms_timechk])?></td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.reser_sms_time')?></label></th>
										<td colspan="3"><?=$data[reser_sms_starttime]." ~ ".$data[reser_sms_endtime]?></td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.reser_sms_msg')?></label></th>
										<td colspan="3"><?=$data[reser_sms_msg]?></td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.reser_sms_editmsg')?></label></th>
										<td colspan="3"><?=$data[reser_sms_editmsg]?></td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.reser_sms_confirmmsg')?></label></th>
										<td colspan="3"><?=$data[reser_sms_confirmmsg]?></td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.reser_sms_cancelmsg')?></label></th>
										<td colspan="3"><?=$data[reser_sms_cancelmsg]?></td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.reser_possible_day')?></label></th>
										<td><?=$data[reser_possible_day]?><?=getMsg('lable.td.day')?></td>
										<th scope="row"><label for=""><?=getMsg('th.reser_possible_term')?></label></th>
										<td><?=$data[reser_possible_term]?><?=getMsg('lable.td.day')?></td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.reser_time')?></label></th>
										<td colspan="3"><?=$data[reser_start_time]." ~ ".$data[reser_end_time]?></td>
									</tr>
								</tbody>
							</table>
							</div>
							<div class="btn">
								<div class="btnLeft">
									<a class="btns" href="<?=$hospital->getQueryString('index.php', 0, $_REQUEST)?>"><strong><?=getMsg('btn.list')?></strong></a>
								</div>
								<div class="btnRight">
									<a class="btns" href="<?=$hospital->getQueryString('edit.php', $data[no], $_REQUEST)?>"><strong><?=getMsg('btn.edit')?></strong></a>
								</div>
							</div>
							<!--//btn-->
						</div>
						<!-- //bread -->
					</div>
					<!-- //bbs --> 
					<!-- 내용 : e -->
				</div>
				<!--//con -->
			</div>
			<!--//content -->
		</div>
		<!--//container --> 
		<!-- E N D :: containerArea-->
	</div>
	<!--//canvas -->
</div>
<!--//wrap -->

</body>
</html>