<? session_start(); 
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Hospital.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

$hospital = new Hospital($pageRows, $_REQUEST);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
</head>
<body>
<?
if (checkReferer($_SERVER["HTTP_REFERER"])) {
	$cmd = $_REQUEST['cmd'];

	if ($cmd == 'write') {
		$r = $hospital->insert($_REQUEST);
		if ($r > 0) {
			echo returnURLMsg($hospital->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.insert'));
		} else {
			echo returnURLMsg($hospital->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.error'));
		}
	} else if ($cmd == 'edit') {
		$r = $hospital->update($_REQUEST);
		if ($r > 0) {
			echo returnURLMsg($hospital->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.update'));
		} else {
			echo returnURLMsg($hospital->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.error'));
		}
	} else if ($cmd == 'delete') {
		$no = $_REQUEST['no'];
		$r = $hospital->delete($no);
	
		if ($r > 0) {
			echo returnURLMsg($hospital->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.delete'));
		} else {
			echo returnURLMsg($hospital->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.error'));
		}		
		
	} else if ($cmd == 'groupDelete') {
		$no = $_REQUEST['no'];
		
		$r = 0;
		for ($i=0; $i<count($no); $i++) {
			$r += $hospital->delete($no[$i]);
		}

		if ($r > 0) {
			echo returnURLMsg($hospital->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.countdelete',$r));
		} else {
			echo returnURLMsg($hospital->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.error'));
		}
	}

} else {
	echo returnURLMsg($hospital->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.error'));
}


?>
</body>
</html>