<? session_start(); 
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Hospital.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

$hospital = new Hospital($pageRows, $_REQUEST);
$rowPageCount = $hospital->getCount($_REQUEST);
$result = $hospital->getList($_REQUEST);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
<script>
function groupDelete() {	
	if ( $('[name^=no]:checked').length > 0 ) {
		document.frm.submit();
	} else {
		alert("<?=getMsg('alert.text.delete')?>");
	}
}
</script>
</head>
<body> 
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<div id="wrap">
	<!-- canvas -->
	<div id="canvas">
		<!-- S T A R T :: headerArea-->
		<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/top.php" ?>
		<!-- E N D :: headerArea--> 
		
		<!-- S T A R T :: containerArea-->
		<div id="container">
			<div id="content">
				<div class="con_tit">
					<h2><?=$pageTitle?> - [<?=getMsg("th.list")?>]</h2>
				</div>
				<!-- //con_tit -->
				<div class="con">
					<!-- 내용 : s -->
					<div id="bbs">
						<div id="blist">
							<p><span><strong><?=getMsg('th.total')?> <?=$rowPageCount[0]?><?=getMsg('th.amount')?></strong>  |  <?=$hospital->reqPageNo?>/<?=$rowPageCount[1]?><?=getMsg('th.page')?></span></p>
							<div class="table_wrap">
							<form name="frm" id="frm" action="process.php" method="post">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리목록입니다.">
								<colgroup>
									<col style="width:50px !important;" />
									<col style="width:50px !important;"/>
									<col class="" />
									<col class="w10" />
									<col class="w12" />
									<col class="w8" />
									<col class="w10" />
									<col class="w12" />
									<col class="w8" />
									<col class="w5" />
									<col class="w5" />
									<col class="w10" />
								</colgroup>
								<thead>
									<tr>
										<th scope="col" class="first"><label class="b_first_c"><input type="checkbox" name="allChk" id="allChk" onClick="check(this, document.getElementsByName('no[]'))"/>
										<i></i>
										</label>
										</th>
										<th scope="col"><?=getMsg("th.no")?></th>
										<th scope="col"><?=getMsg('th.hospitalname')?></th>
										<th scope="col"><?=getMsg('th.consult_admin_tel2')?></th>
										<th scope="col"><?=getMsg('th.consult_req_sms')?></th>
										<th scope="col"><?=getMsg('th.consult_sms_timechk2')?></th>
										<th scope="col"><?=getMsg('th.reser_admin_tel2')?></th>
										<th scope="col"><?=getMsg('th.reser_req_sms')?></th>
										<th scope="col"><?=getMsg('th.reser_sms_timechk2')?></th>
										<th scope="col"><?=getMSg('th.reser_possible_day')?></th>
										<th scope="col"><?=getMSg('th.reser_possible_term')?></th>
										<th scope="col"><?=getMsg('th.registdate')?></th>
									</tr>
								</thead>
								<tbody>
								<? if ($rowPageCount[0] == 0) { ?>
									<tr>
										<td class="first" colspan="12"><?=getMsg('message.tbody.data')?></td>
									</tr>
								<?
									 } else {
										$targetUrl = "";
										while ($row = mysql_fetch_assoc($result)) {
											$targetUrl = "style='cursor:pointer;' onclick=\"location.href='".$hospital->getQueryString('read.php', $row[no], $_REQUEST)."'\"";
								?>
									<tr>
										<td class="first">
										<?if($row['no'] != DEFAULT_BRANCH_NO){?>
										<label class="b_nor_c"><input type="checkbox" name="no[]" id="no" value="<?=$row[no]?>"/>
										<i></i>
										</label>
										<?
										}
										?>
										</td>
										<td <?=$targetUrl?>><?=$row[no]?></td>
										<td <?=$targetUrl?>><?=$row[name]?></td>
										<td <?=$targetUrl?>><?=$row[consult_admin_tel]?></td>
										<td <?=$targetUrl?>><?=getMsg('lable.td.user')?> <?=getSmsName($row[consult_write_user])?> <?=getMsg('lable.td.admin')?> <?=getSmsName($row[consult_write_admin])?></td>
										<td <?=$targetUrl?>><?=getSmsTimeLimitName($row[consult_sms_timechk])?></td>
										<td <?=$targetUrl?>><?=$row[reser_admin_tel]?></td>
										<td <?=$targetUrl?>><?=getMsg('lable.td.user')?> <?=getSmsName($row[reser_write_user])?> <?=getMsg('lable.td.admin')?> <?=getSmsName($row[reser_write_admin])?></td>
										<td <?=$targetUrl?>><?=getSmsTimeLimitName($row[reser_sms_timechk])?></td>
										<td <?=$targetUrl?>><?=getMsg('lable.td.afterday',$row[reser_possible_day])?></td>
										<td <?=$targetUrl?>><?=getMsg('lable.td.beforeday',$row[reser_possible_term])?></td>
										<td class="last" <?=$targetUrl?>><?=$row[registdate]?></td>
									</tr>
								<?
										}
									 }
								?>
								</tbody>
							</table>
								<input type="hidden" name="cmd" id="cmd" value="groupDelete"/>
								<input type="hidden" name="stype" id="stype" value="<?=$_REQUEST[stype]?>"/>
								<input type="hidden" name="sval" id="sval" value="<?=$_REQUEST[sval]?>"/>
							</form>
							</div>
							<div class="btn">
								<div class="btnLeft">
									<a class="btns" href="#" onclick="groupDelete();"><strong><?=getMsg('btn.delete')?></strong> </a>
								</div>
								<div class="btnRight">
									<a class="wbtn" href="write.php"><strong><?=getMsg('btn.write')?></strong> </a>
								</div>
							</div>
							<!--//btn-->
							<!-- 페이징 처리 -->
							<?=pageList($hospital->reqPageNo, $rowPageCount[1], $hospital->getQueryString('index.php', 0, $_REQUEST))?>
							<!-- //페이징 처리 -->
							<form name="searchForm" id="searchForm" action="index.php" method="post">
								<div class="search">
									<select name="stype" title="검색을 선택해주세요">
										<option value="name" <?=getSelected($_REQUEST[stype], "name") ?>><?=getMsg('lable.option.name2')?></option>
									</select>
									<input type="text" name="sval" value="<?=$_REQUEST[sval]?>" title="검색할 내용을 입력해주세요" />
									<input type="submit" class="se_btn " value="<?=getMsg('btn.search')?>" />
								</div>
							</form>
							<!-- //search --> 
						</div>
						<!-- //blist -->
					</div>
					<!-- //bbs --> 
					<!-- 내용 : e -->
				</div>
				<!--//con -->
			</div>
			<!--//content -->
		</div>
		<!--//container --> 
		<!-- E N D :: containerArea-->
	</div>
	<!--//canvas -->
</div>
<!--//wrap -->

</body>
</html>