<? session_start(); 
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Hospital.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

$hospital = new Hospital($pageRows, $_REQUEST);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/boardConfig/config.php" ?>
<script>
function goSave(obj) {
	
	
	if(validation(obj)){
		
		$("#frm").submit();	
		
	}
}


</script>
</head>
<body> 
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<div id="wrap">
	<!-- canvas -->
	<div id="canvas">
		<!-- S T A R T :: headerArea-->
		<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/top.php" ?>
		<!-- E N D :: headerArea--> 
		
		<!-- S T A R T :: containerArea-->
		<div id="container">
			<div id="content">
				<div class="con_tit">
					<h2><?=$pageTitle?> - [<?=getMsg('th.write')?>]</h2>
				</div>
				<!-- //con_tit -->
				<div class="con">
					<!-- 내용 : s -->
					<div id="bbs">
						<div id="bread">
							<h3><?=getMsg('th.branch_sel')?></h3>
							<form method="post" name="frm" id="frm" action="<?=getSslCheckUrl($_SERVER['REQUEST_URI'], 'process.php')?>">
							<div class="table_wrap">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리 기본내용입니다.">
								<colgroup>
									<col width="250px" />
									<col width="*" />
									<col width="250px" />
									<col width="*" />
								</colgroup>
								<tbody>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.hospitalname')?></label></th>
										<td colspan="3">
											<input type="text" id="name" name="name" value=""  class="input20p" data-value="<?=getMsg('alert.text.hospitalname')?>" />
										</td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.consult_write_user')?></label></th>
										<td colspan="3">
											<input type="radio" id="consult_write_user" name="consult_write_user" value="1" checked/><?=getMsg('lable.checkbox.send')?>
											<input type="radio" id="consult_write_user" name="consult_write_user" value="0"/><?=getMsg('lable.checkbox.nosend')?>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.consult_write_admin')?></label></th>
										<td colspan="3">
											<input type="radio" id="consult_write_admin" name="consult_write_admin" value="1" checked/><?=getMsg('lable.checkbox.send')?>
											<input type="radio" id="consult_write_admin" name="consult_write_admin" value="0"/><?=getMsg('lable.checkbox.nosend')?>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.consult_reply_user')?></label></th>
										<td colspan="3">
											<input type="radio" id="consult_reply_user" name="consult_reply_user" value="1" checked/><?=getMsg('lable.checkbox.send')?>
											<input type="radio" id="consult_reply_user" name="consult_reply_user" value="0"/><?=getMsg('lable.checkbox.nosend')?>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.consult_admin_tel')?></label></th>
										<td colspan="3">
											<input type="text" id="consult_admin_tel" name="consult_admin_tel" value=""  class="input20p" onKeyUp="isNumberOrHyphen(this);cvtPhoneNumber(this);"/>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.consult_sms_timechk')?></label></th>
										<td colspan="3">
											<input type="radio" id="consult_sms_timechk" name="consult_sms_timechk" value="1" checked/><?=getMsg('lable.checkbox.limit')?>
											<input type="radio" id="consult_sms_timechk" name="consult_sms_timechk" value="0"/><?=getMsg('lable.checkbox.nolimit')?>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.consult_sms_time')?></label></th>
										<td colspan="3">
											<select name="consult_sms_starttime">
												<?=getTimeList2("10:00:00")?>
											</select>
											~
											<select name="consult_sms_endtime">
												<?=getTimeList2("19:00:00")?>
											</select>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.consult_sms_msg')?></label></th>
										<td colspan="3">
											<textarea name="consult_sms_msg" id="consult_sms_msg" cols="40" ></textarea>
											<span style='color:#ff4400;vertical-align:top;'>#name#은 고객 성명으로 대체됩니다.</span>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.reser_write_user')?></label></th>
										<td colspan="3">
											<input type="radio" id="reser_write_user" name="reser_write_user" value="1" checked/><?=getMsg('lable.checkbox.send')?>
											<input type="radio" id="reser_write_user" name="reser_write_user" value="0"/><?=getMsg('lable.checkbox.nosend')?>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.reser_write_admin')?></label></th>
										<td colspan="3">
											<input type="radio" id="reser_write_admin" name="reser_write_admin" value="1" checked/><?=getMsg('lable.checkbox.send')?>
											<input type="radio" id="reser_write_admin" name="reser_write_admin" value="0"/><?=getMsg('lable.checkbox.nosend')?>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.reser_edit_user')?></label></th>
										<td colspan="3">
											<input type="radio" id="reser_edit_user" name="reser_edit_user" value="1" checked/><?=getMsg('lable.checkbox.send')?>
											<input type="radio" id="reser_edit_user" name="reser_edit_user" value="0"/><?=getMsg('lable.checkbox.nosend')?>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.reser_edit_admin')?></label></th>
										<td colspan="3">
											<input type="radio" id="reser_edit_admin" name="reser_edit_admin" value="1" checked/><?=getMsg('lable.checkbox.send')?>
											<input type="radio" id="reser_edit_admin" name="reser_edit_admin" value="0"/><?=getMsg('lable.checkbox.nosend')?>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.reser_cancel_user')?></label></th>
										<td colspan="3">
											<input type="radio" id="reser_cancel_user" name="reser_cancel_user" value="1" checked/><?=getMsg('lable.checkbox.send')?>
											<input type="radio" id="reser_cancel_user" name="reser_cancel_user" value="0"/><?=getMsg('lable.checkbox.nosend')?>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.reser_cancel_admin')?></label></th>
										<td colspan="3">
											<input type="radio" id="reser_cancel_admin" name="reser_cancel_admin" value="1" checked/><?=getMsg('lable.checkbox.send')?>
											<input type="radio" id="reser_cancel_admin" name="reser_cancel_admin" value="0"/><?=getMsg('lable.checkbox.nosend')?>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.reser_end_user')?></label></th>
										<td colspan="3">
											<input type="radio" id="reser_end_user" name="reser_end_user" value="1" checked/><?=getMsg('lable.checkbox.send')?>
											<input type="radio" id="reser_end_user" name="reser_end_user" value="0"/><?=getMsg('lable.checkbox.nosend')?>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.reser_admin_tel')?></label></th>
										<td colspan="3">
											<input type="text" id="reser_admin_tel" name="reser_admin_tel" value=""  class="input20p" onKeyUp="isNumberOrHyphen(this);cvtPhoneNumber(this);"/>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.reser_sms_timechk')?></label></th>
										<td colspan="3">
											<input type="radio" id="reser_sms_timechk" name="reser_sms_timechk" value="1" checked/><?=getMsg('lable.checkbox.limit')?>
											<input type="radio" id="reser_sms_timechk" name="reser_sms_timechk" value="0"/><?=getMsg('lable.checkbox.nolimit')?>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.reser_sms_time')?></label></th>
										<td colspan="3">
											<select name="reser_sms_starttime">
												<?=getTimeList2("10:00:00")?>
											</select>
											~
											<select name="reser_sms_endtime">
												<?=getTimeList2("19:00:00")?>
											</select>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.reser_sms_msg')?></label></th>
										<td colspan="3">
											<textarea name="reser_sms_msg" id="reser_sms_msg" cols="40" ></textarea>
											<span style='color:#ff4400;vertical-align:top;'><?=getMsg('message.tbody.name_comment')?></span>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.reser_sms_editmsg')?></label></th>
										<td colspan="3">
											<textarea name="reser_sms_editmsg" id="reser_sms_editmsg" cols="40" ></textarea>
											<span style='color:#ff4400;vertical-align:top;'><?=getMsg('message.tbody.name_comment')?></span>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.reser_sms_confirmmsg')?></label></th>
										<td colspan="3">
											<textarea name="reser_sms_confirmmsg" id="reser_sms_confirmmsg" cols="40" ></textarea>
											<span style='color:#ff4400;vertical-align:top;'><?=getMsg('message.tbody.name_comment')?></span>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.reser_sms_cancelmsg')?></label></th>
										<td colspan="3">
											<textarea name="reser_sms_cancelmsg" id="reser_sms_cancelmsg" cols="40" ></textarea>
											<span style='color:#ff4400;vertical-align:top;'><?=getMsg('message.tbody.name_comment')?></span>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.reser_possible_day')?></label></th>
										<td>
											<input type="text" id="reser_possible_day" name="reser_possible_day" value=""  style="width:30px;" maxlength="3" /><?=getMsg('lable.td.day')?><br/>
											<span style='color:#ff4400'><?=getMsg('message.tbody.reserpossbleday_comment')?></span>
										</td>
										<th scope="row"><label for=""><?=getMsg('th.reser_possible_term')?></label></th>
										<td>
											<input type="text" id="reser_possible_term" name="reser_possible_term" value=""  style="width:30px;" maxlength="3" /><?=getMsg('lable.td.day')?><br/>
											<span style='color:#ff4400'><?=getMsg('message.tbody.reserpossbleterm_comment')?></span>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.reser_time')?></label></th>
										<td colspan="3">
											<select name="reser_start_time">
												<?=getTimeList2("10:00:00")?>
											</select>
											~
											<select name="reser_end_time">
												<?=getTimeList2("19:00:00")?>
											</select>
										</td>
									</tr>
								</tbody>
							</table>
							<input type="hidden" name="stype" id="stype" value="<?=$_REQUEST[stype]?>"/>
							<input type="hidden" name="sval" id="sval" value="<?=$_REQUEST[sval]?>"/>
							<input type="hidden" name="cmd" value="write">
							</div>
							<div class="btn">
								<div class="btnLeft">
									<a class="btns" href="<?=$hospital->getQueryString('index.php', 0, $_REQUEST)?>"><strong><?=getMsg('btn.list')?></strong></a>
								</div>
								<div class="btnRight">
									<a class="btns" href="javascript:;" onclick="goSave(this);"><strong><?=getMsg('btn.save')?></strong></a>
								</div>
							</div>
							</form>
							
							<!--//btn-->
						</div>
						<!-- //bread -->
					</div>
					<!-- //bbs --> 
					<!-- 내용 : e -->
				</div>
				<!--//con -->
			</div>
			<!--//content -->
		</div>
		<!--//container --> 
		<!-- E N D :: containerArea-->
	</div>
	<!--//canvas -->
</div>
<!--//wrap -->

</body>
</html>