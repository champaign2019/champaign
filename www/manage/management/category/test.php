<?
	function getKey($length = 10) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyz';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return date('Ymd').$randomString;
	}

	echo getKey();
	//620708560
	//097bf05fdc
?>