<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/filter/RequestWrapper.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/category/Category.class.php";

include "config.php";

$list = Category::getList();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
$(window).load(function(){
	
	var firstParent = "";
	<?
		$i = 0;
		while ($data = mysql_fetch_assoc($list)) {
	?>
		var parent_index = '<?=$data["code"] ?>';	
		var parent = '<?=$data["rel_code"] ?>';
		console.log(parent);
		
		var orgname = '<?=$data["orgname"] ?>';
		if(parent == 0){
			firstParent = parent_index;
			var index = '<?=$i?>';
			if(index == 0){
				$("#organ_jstree").append("<ul><li id='"+parent_index+"' name='"+parent_index+"' ><a href='javascript:;' name='"+firstParent+"' name='"+firstParent+"' >"+orgname+"</a></li></ul>")
			}else{
				$("#organ_jstree > ul").append("<li id='"+parent_index+"' name='"+parent_index+"' ><a href='javascript:;' name='"+firstParent+"' >"+orgname+"</a></li>")	
			}
		}else{
			$("li[name="+parent+"]").append("<ul><li id='"+parent_index+"' name='"+parent_index+"' ><a href='javascript:;' data-fff name='"+firstParent+"' >"+orgname+"</a></li></ul>")	
		}		
	<?
			$i++;
		}
	?>
	
	$("#organ_jstree").jstree({
		"core" : {
	      "check_callback" : true
	    },
	    "plugins" : [ "dnd" ]
	}).bind("move_node.jstree",function(event,data){
		
		var ref = $("#organ_jstree").jstree(true);
		var sel_node = ref.get_selected();
		ref.deselect_node(sel_node);
		ref.select_node($(data).get(0).node.id);
		 
		var stdObj = $("#"+$(data).get(0).node.id);
		
		var currentParent = $(stdObj).parent().parent().attr("id")
		if(currentParent == "organ_jstree"){currentParent = 0;}
		var firstParent = $(stdObj).parents("li:last").attr("id");
		
		var frsParents = "";
		
		$(stdObj).closest("ul").children("li").each(function(i){
			if(i == 0){
				frsParents = $(this).attr("id");
			}else{
				frsParents += ","+$(this).attr("id");
			}
		});
		
		
		var children = "";
		var cObj = $(data).get(0).node.children_d;
		for(var i=0; i < cObj.length; i++){
			if(i == 0){
				children = $(cObj)[i];
			}else{
				children += ","+$(cObj)[i];
			}
		}
		
		if(firstParent == undefined){firstParent = "";}
		
		var pcsorts = $(stdObj).parents("li").length+1;
		
		$.post("changeDeptUpdate.php",
		{
			firstParent : firstParent,
			currentParent : currentParent,
			frsParents : frsParents,
			children : children,
			pcsorts : pcsorts,
			code : $(data).get(0).node.id
			
		},function(success){
			
		}).fail(function(){
			alert('<?=getMsg("alert.text.error")?>')
		})
		
    }).bind("rename_node.jstree",function(event,data){
			
    		var code = $(data).get(0).node.id;
    		var orgname = $(data).get(0).text;
    		var orgname_eng = "";
    		var contents = "";
    		
    		$.post("updtDeptManage.php",
    		{
    			code : code,	
    			orgname : orgname,	
    			orgname_eng : orgname_eng,	
    			contents : contents	
    		},function(data){
    			data = decodeURIComponent( data );
    			var result = jQuery.parseJSON(data);
    			if(result.resultCode == 'OK'){
    				
    				$("#orgname_right").val(orgname);
    				return;
    			}else{
    				alert(result.resultCode);
    				return;
    			}
    			
    		}).fail(function(){
    			alert('<?=getMsg("alert.text.error")?>')
    		})
    		
    }).bind("select_node.jstree", function (event, data) {
    	//fn_click_dir

    	if(typeof($(data).get(0).event) == "object"){
    		
    	}else{
    		return;
    	}
		var obj = $(data).get(0).event.currentTarget;
    	
    	$("#orgname_left").parent().remove();
    	var id = $(obj).parent().attr("id");
    	
    	var firstId = $(obj).parents("li:last").attr("id");
    	
    	$("#rel_code_left").val(id);
    	
    	$("#rel_first_code_left").val(firstId);
    	
    	$.post("readDeptManage.php",
    	{
    		code : id	
    	},function(data){
    		data = decodeURIComponent( data );
    		var result = jQuery.parseJSON(data);
			test = result;
    		
    		$("#code").val(result.code);
    		$("#orgname_right").val(result.orgname);
    		$("#orgname_eng").val(result.orgname_eng);
    		$("#contents").val(result.contents);
    		$("#pcsorts_left").val(result.pcsorts*1+1);
    		
    	}).fail(function(){
    		alert('<?=getMsg("alert.text.error")?>')
    	})
    	
    }).bind("delete_node.jstree", function (event, data) {
    	
    	var code = $(data).get(0).node.id;
    	var frsParents = "";
		var cObj = $(data).get(0).node.children_d;
		for(var i=0; i < cObj.length; i++){
			frsParents += $(cObj)[i]+",";
		}
		frsParents += code;
    	
    	if(confirm('<?=getMsg("confirm.text.categorydelete")?>')){
    		
    		$.post("removeDeptManage.php",
    		{
    			frsParents : frsParents,
    			code : code 
    		},function(data){
    			data = decodeURIComponent( data );
    			var result = jQuery.parseJSON(data);
    			if(result.resultCode == 'OK'){
    				$("#orgname_right").val("");
    				return;
    			}
    			
    		}).fail(function(){
    			alert('<?=getMsg("alert.text.error")?>')
    		})
    		
    	}
    });
	
	

})

function fncDeptManageInsert() {
	var varFrom = document.getElementById("organFrm");
	if(varFrom.orgname.value == ''){
		alert("<?=getMsg("alert.text.menu")?>");
		varFrom.orgname.focus();
		return;
	}
	
	if (confirm('<?=getMsg("confirm.text.insert")?>')) {
		
		
		var orgname = $("#orgname_right").val();
		var orgname_eng = $("#orgname_eng").val();
		var contents = $("#contents").val();
		var rel_code = $("#rel_code_right").val();
		var rel_first_code = $("#rel_first_code_right").val();
		var pcsorts = $("#pcsorts_right").val();
		
		$.post("addDeptManage.php",
		{
			orgname : orgname,	
			orgname_eng : orgname_eng,	
			contents : contents,	
			rel_code : rel_code,	
			pcsorts : pcsorts,	
			rel_first_code : rel_first_code	
		},function(data){
			data = decodeURIComponent( data );
			var result = jQuery.parseJSON(data);
			if(result.resultCode == 'OK'){
				alert('<?=getMsg("result.text.insert")?>');
				location.href = "index.php";
				return;
			}else{
				alert(result.resultCode);
				return;
			}
			
		}).fail(function(){
			alert('<?=getMsg("alert.text.error")?>')
		})
	}
}

function fn_update(){
	
	var code = $("#code").val();
	var orgname = $("#orgname_right").val();
	var orgname_eng = $("#orgname_eng").val();
	var contents = $("#contents").val();
	
	$.post("updtDeptManage.php",
	{
		code : code,	
		orgname : orgname,	
		orgname_eng : orgname_eng,	
		contents : contents	
	},function(data){
		data = decodeURIComponent( data );
		var result = jQuery.parseJSON(data);
		if(result.resultCode == 'OK'){
			
			var ref = $("#organ_jstree").jstree(true);
			var sel_node = ref.get_selected();
			ref.edit(sel_node,orgname);
			return;
		}else{
			alert(result.resultCode);
			return;
		}
		
	}).fail(function(){
		alert('<?=getMsg("alert.text.error")?>')
	})

}

function doDelete() {
 	var ref = $("#organ_jstree").jstree(true),
 	sel = ref.get_selected();
 	if(!sel.length) { return false; }
 	ref.delete_node(sel);
 }
 
function doCreate() {
	 var ref = $("#organ_jstree").jstree(true);
	 var sel_node = ref.get_selected();
	 var new_sel_node = "";
	 var first_text = "New";
	 if(!sel_node.length) { 
	    alert('<?=getMsg("alert.text.menu")?>');
	     return;
	 }
	 
	 var orgname = first_text;
	 var rel_code = $("#rel_code_left").val();
	 var rel_first_code = $("#rel_first_code_left").val();
	 var pcsorts = $("#pcsorts_left").val();
	
	$.post("addDeptManage.php",
	{
		orgname : orgname,
		rel_code : rel_code,
		rel_first_code : rel_first_code,
		pcsorts : pcsorts
	},function(data){
		data = decodeURIComponent( data );
		var result = jQuery.parseJSON(data);
		if(result.resultCode == 'OK'){
			//location.href = "index.php";
			 ref.deselect_node(sel_node); 
			 sel_node = sel_node[0];
			 new_sel_node = ref.create_node(sel_node,{"id" : result.code,"text":orgname});
			 ref.select_node(new_sel_node);
			 if(new_sel_node) {
			 	 ref.edit(new_sel_node);
			 }
			 $("#code").val(result.code);
	    	 $("#orgname_right").val(orgname);
			 $("#rel_code_left").val(result.code);
			 $("#pcsorts_left").val(parseInt($("#pcsorts_left").val())+1);
			return;
		}else{
			alert(result.resultCode);
			return;
		}
		
	}).fail(function(){
		alert('<?=getMsg("alert.text.error")?>')
	})
	 
}


</script>
</head>
<body> 
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<div id="wrap">
	<!-- canvas -->
	<div id="canvas">
		<!-- S T A R T :: headerArea-->
		<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/top.php" ?>
		<!-- E N D :: headerArea--> 
		
		<!-- S T A R T :: containerArea-->
		<div id="container">
			<div id="content">
				<div class="con_tit">
					<h2><?=pageTitle?> - [<?=getMsg("th.list")?>]</h2>
				</div>
				<!-- //con_tit -->
				<div class="con">
					<!-- 내용 : s -->
					<div id="bbs">
						<div id="bread">
							<div class="btn">
								<div class="btnLeft">
									<a class="btns" href="#" onclick="doCreate();"><strong><?=getMsg("btn.categoryadd")?></strong> </a>
									<a class="btns" href="#" onclick="doDelete();"><strong><?=getMsg("btn.categoryremove")?></strong> </a>
								</div>
							</div>
							<!--//btn-->
							<form action="" method="post" id="organSimpleFrm" name="organSimpleFrm">
							<div id="organ_jstree" style="width:25%; height:550px; overflow-y:scroll; border:1px solid gray; float:left;">
								
							</div>
							
							<input type="hidden" name="rel_code" id="rel_code_left" value="0" />								
							<input type="hidden" name="rel_first_code" id="rel_first_code_left" value="0"/>	
							<input type="hidden" name="pcsorts" id="pcsorts_left" value="1"/>	
							</form>
							
							<div style="margin-left:20px; width:65%; float:left;">
								<form action="" method="post" id="organFrm" name="organFrm">
								<input name="orgname_eng" id="orgname_eng" type="hidden" maxlength="10" size="50" />
								<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리목록입니다.">
									<colgroup>
										<col style="width:25%;" />
										<col style="width:75%;" />
									</colgroup>
									<tbody>
										<!-- <tr>
											<th>* 코드</th>
											<td><input name="orgnztId" id="orgnztId" type="text" size="30" class="readOnlyClass" readonly title="부서ID"></td>
										</tr> -->
										<tr>
											<th><?=getMsg("th.categoryname")?></th>
											<td><input name="orgname" id="orgname_right" type="text" maxlength="50" size="30" /></td>
										</tr>
										<tr>
											<th><?=getMsg("th.engcategoryname")?></th>
											<td><input name="orgname_eng" id="orgname_eng" type="text" maxLength="10" size="50" /></td>
										</tr>
										<tr>
											<th><?=getMsg("th.contents")?></th>
											<td><textarea name="contents" id="contents" type="text" maxLength="255" style="width:310px; height:100px;"></textarea></td>
										</tr>
									</tbody>								
								</table>
								<div class="btn">
									<div class="btnRight">
										<a class="btns" href="#" onclick="fn_update();"><strong>수정</strong> </a>
										<a class="btns" href="#" onclick="fncDeptManageInsert();" id="insertID"><strong>등록</strong> </a>
									</div>
								</div>
								<input type="hidden" name="rel_code" id="rel_code_right" value="0" />								
								<input type="hidden" name="rel_first_code" id="rel_first_code_right" value="0"/>								
								<input type="hidden" name="code" id="code" value="0"/>								
								<input type="hidden" name="pcsorts" id="pcsorts_right" value="1"/>								
								</form>
								
							</div>
						</div>
						<!-- //blist -->
					</div>
					<!-- //bbs --> 
					<!-- 내용 : e -->
				</div>
				<!--//con -->
			</div>
			<!--//content -->
		</div>
		<!--//container --> 
		<!-- E N D :: containerArea-->
	</div>
	<!--//canvas -->
</div>
<!--//wrap -->

</body>
</html>