<? session_start(); 
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
<script>
function goSave(obj) {
	if(validation(obj)){
		var max = $("#editor_maxsize").val();
		max = max*1024*1024;
		$("#editor_maxsize").val(max);
		$("#frm").submit();	
	}
}	
</script>
</head>
<body> 
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<div id="wrap">
	<!-- canvas -->
	<div id="canvas">
		<!-- S T A R T :: headerArea-->
		<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/top.php" ?>
		<!-- E N D :: headerArea--> 
		
		<!-- S T A R T :: containerArea-->
		<div id="container">
			<div id="content">
				<div class="con_tit">
					<h2><?=getMsg('th.config')?></h2>
				</div>
				<!-- //con_tit -->
				<div class="con">
					<!-- 내용 : s -->
					<div id="bbs">
						<div class="entered_into up_type">
							<span class="colR01">* 사이트에 반영되는 중요한 설정 값입니다. 비용발생 부분을 신중히 검토하고 변경하여 주세요. </span>				
						</div>
							<!--//entered_into-->
						<div id="bread">
						<form name="frm" id="frm" action="process.php" method="post">
							<div class="table_wrap">
							<h3><?=getMsg('th.company_info')?> <span class="colR01 under_f">이메일폼에 적용되는 부분입니다.</span></h3>
							<!--div class="table_wrap"-->
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<colgroup>
									<col width="15%" />
									<col width="35%" />
									<col width="15%" />
									<col width="35%" />
								</colgroup>
								<tbody>
									<tr>
										<th><span><?=getMsg('th.company_name')?></span></th>
										<td>
											<input type="text" class="input50p" id="company_name" name="company_name" value="<?=COMPANY_NAME?>" data-value="<?=getMsg('th.company_name')?>"/>
											<br/>
											<label class="jsp">SiteProperty.COMPANY_NAME</label>									
										</td>
										<th><span><?=getMsg('th.company_url')?></span></th>
										<td>
											<input type="text" class="input80p" id="company_url" name="company_url" value="<?=COMPANY_URL?>" data-value="<?=getMsg('th.company_url')?>" />
										</td>
									</tr>
									<tr>
										<th><span><?=getMsg('th.company_email')?></span></th>
										<td>
											<input type="text" class="input50p" id="company_email" name="company_email" value="<?=COMPANY_EMAIL?>" />
											<br/>
											<label class="jsp">SiteProperty.COMPANY_EMAIL</label>	
										</td>
										<th><span><?=getMsg('th.company_tel')?></span></th>
										<td>
											<input type="text" class="input50p" id="company_tel" name="company_tel" value="<?=COMPANY_TEL?>" />
											<br/>
											<label class="jsp">SiteProperty.COMPANY_TEL</label>	
										</td>
									</tr>
									<tr>
										<th><span><?=getMsg('th.company_fax')?></span></th>
										<td>
											<input type="text" class="input50p" id="company_fax" name="company_fax" value="<?=COMPANY_FAX?>" />
											<br/>
											<label class="jsp">SiteProperty.COMPANY_FAX</label>	
										</td>
										<th><span><?=getMsg('th.company_addr')?></span></th>
										<td>
											<input type="text" class="input80p" id="company_addr" name="company_addr" value="<?=COMPANY_ADDR?>" />
											<br/>
											<label class="jsp">SiteProperty.COMPANY_ADDR</label>	
										</td>
									</tr>
									<tr>
										<th><span><?=getMsg('th.create_year')?></span></th>
										<td>
											<input type="text" class="input15p" id="create_year" name="create_year" value="<?=CREATE_YEAR?>" maxlength="4" onkeyup="isOnlyNumber(this)"/>년
											<br/>
											<label class="jsp">SiteProperty.CREATE_YEAR</label>	
										</td>
										<th><span><?=getMsg('th.create_month')?></span></th>
										<td>
											<select id="create_month" name="create_month">
											<? for($i=1; $i<=12; $i++) {?>
												<option value="<?=$i?>" <?=getSelected($i, CREATE_MONTH)?>><?=$i?><?=getMsg('th.month')?></option>
											<? } ?>
											</select>
										</td>
									</tr>
									<tr>
										<th><span><?=getMsg('th.check_referer')?></span></th>
										<td>
											<label class="b_snor_r"><input type="radio" class="" id="check_referer0" name="check_referer" value="0" <?=getChecked(CHECK_REFERER,"0")?>/><i></i><span class="red_col vaM"><?=getMsg('lable.radio.nouse')?></span></label>
											<label class="b_snor_r"><input type="radio" class="" id="check_referer1" name="check_referer" value="1" <?=getChecked(CHECK_REFERER,"1")?>/><i></i><span class="blue_col vaM"><?=getMsg('lable.radio.use')?></span> <?=getMsg('lable.radio.default')?></label>
										</td>
										<th><span><?=getMsg('th.referer_url')?></span></th>
										<td>
											<input type="text" class="input25p" id="referer_url" name="referer_url" value="<?=REFERER_URL?>" />
										</td>
									</tr>
								</tbody>
							</table>
							<!--/div-->
							
							<h3><?=getMsg('th.site_info')?></h3>
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="<?=getMsg('alt.text.offholiday')?>" class="">
								<colgroup>
									<col width="15%" />
									<col width="35%" />
									<col width="15%" />
									<col width="35%" />
								</colgroup>
								<tbody>
									<tr>
										<th><span><?=getMsg('th.site_ver')?></span></th>
										<td>
											<input type="text" class="input25p" id="version" name="version" value="<?=VERSION?>" />
											<br/>
											<label class="etc">최신버전으로 셋팅되어 있는지 확인 할 수 있는 값 (개발자/퍼블리셔 사용영역)</label>
										</td>
										<th><span><?=getMsg('th.site_type')?></span></th>
										<td>
											<label class="b_snor_r"><input type="radio" id="company_type1" name="company_type" value="1" <?=getChecked(COMPANY_TYPE,"1")?>/><i></i><span class="vaM"><?=getMsg('lable.radio.hospital')?></span> <?=getMsg('lable.radio.default')?></label>
											<label class="b_snor_r"><input type="radio" id="company_type0" name="company_type" value="0" <?=getChecked(COMPANY_TYPE,"0")?>/><i></i><span class="vaM"><?=getMsg('lable.radio.company')?></span></label>
											<br/>
											<label class="etc">추후 통합예정</label>
										</td>
									</tr>
									<tr>
										<th><span><?=getMsg('th.ssl_use')?></span></th>
										<td>
											<label class="b_snor_r"><input type="radio" class="" id="ssl_use0" name="ssl_use" value="0" <?=getChecked(SSL_USE,"0")?>/><i></i><span class="red_col vaM"><?=getMsg('lable.radio.nouse')?></span></label>
											<label class="b_snor_r"><input type="radio" class="" id="ssl_use1" name="ssl_use" value="1" <?=getChecked(SSL_USE,"1")?>/><i></i><span class="blue_col vaM"><?=getMsg('lable.radio.use')?></span></label>
										</td>
										<th><span>
											<?=getMsg('th.ssl_port')?>
										</span></th>
										<td>
											<input type="text" class="input50p" id="ssl_port" name="ssl_port" value="<?=SSL_PORT?>" />
											<br/>
											<label class="etc">포트번호 기입 ex) :443 </label>
										</td>
									</tr>
									<tr>
										<th><span><?=getMsg('th.start_page')?></span></th>
										<td colspan="3">
											<input type="text" class="input80p" id="start_page" name="start_page" value="<?=START_PAGE?>" />
											<br/>
											<label class="etc">관리자 로그인시 처음으로 가는 페이지 입력</label>
										</td>
									</tr>
									<tr>
										<th><span><?=getMsg('th.editor_upload_path')?></span></th>
										<td>
											<input type="text" class="input25p" id="editor_upload_path" name="editor_upload_path" value="<?=EDITOR_UPLOAD_PATH?>" />
											<br/>
											<label class="etc">초기 셋팅시에만 변경가능</label>
										</td>
										<th><span><?=getMsg('th.editor_maxsize')?></span></th>
										<td>
											<input type="text" class="input25p" id="editor_maxsize" name="editor_maxsize" value="<?=EDITOR_MAXSIZE/1024/1024?>" onkeyup="isOnlyNumber(this)"/>MB
											<br/>
											<label class="etc">(<?=EDITOR_MAXSIZE?> byte)</label>
										</td>
									</tr>
									<tr>
										<th><span><?=getMsg('th.default_lang')?></span></th>
										<td>
											<input type="text" class="input25p" id="default_lang" name="default_lang" value="<?=DEFAULT_LANG?>" maxlength="3" />
											<br/>
											<label class="etc">국문:kor / 영문:eng / 중문:chi </label>
										</td>
										<th><span><?=getMsg('th.keep_user')?></span></th>
										<td>
											<input type="text" class="input25p" id="keep_user" name="keep_user" value="<?=KEEP_USER?>" />
											<br/>
											<label class="etc">관리자 페이지 로그인세션 유지할 아이디</label>
										</td>
									</tr>
									<tr>
										<th><span><?=getMsg('th.dormant_type')?></span></th>
										<td>
											<label class="b_snor_r"><input type="radio" id="dormant_type0" name="dormant_type" value="0" <?=getChecked(DORMANT_TYPE,"0")?>/><i></i><span class="red_col vaM"><?=getMsg('lable.radio.nouse')?></span> <?=getMsg('lable.radio.default')?></label>
											<label class="b_snor_r"><input type="radio" id="dormant_type1" name="dormant_type" value="1" <?=getChecked(DORMANT_TYPE,"1")?>/><i></i><span class="blue_col vaM"><?=getMsg('lable.radio.use')?></span></label>
											<br/>
											<label class="etc">초기 셋팅시에만 변경가능 (이후 변경건에 대해서는 개발자에게 문의하세요.)</label>
										</td>
										<th>
											<span><?=getMsg('th.dormant_date')?></span>
											<br/>
											<span><?=getMsg('th.dormant_notice_date')?></span>
										</th>
										<td>
											<input type="text" class="input25p" id="dormant_date" name="dormant_date" value="<?=DORMANT_DATE?>" onkeyup="isOnlyNumber(this)"/>
											<br/>
											<input type="text" class="input25p" id="dormant_notice_date" name="dormant_notice_date" value="<?=DORMANT_NOTICE_DATE?>" onkeyup="isOnlyNumber(this)"/>
										</td>
									</tr>
									<tr>
										<th><span><?=getMsg('th.customer_fk')?></span></th>
										<td>
											<input type="text" class="input25p" id="customer_fk" name="customer_fk" value="<?=CUSTOMER_FK?>" onkeyup="isOnlyNumber(this)"/>
											<br/>
											<label class="etc">관리자 유지보수게시판 연동 거래처 번호</label>	
										</td>
										<th><span><?=getMsg('th.project_fk')?></span></th>
										<td>
											<input type="text" class="input25p" id="project_fk" name="project_fk" value="<?=PROJECT_FK?>" onkeyup="isOnlyNumber(this)"/>
											<br/>
											<label class="etc">관리자 유지보수게시판 연동 프로젝트 번호</label>
										</td>
									</tr>
								</tbody>
							</table>
							
							<h3><?=getMsg('th.sns_info')?></h3>
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="<?=getMsg('alt.text.offholiday')?>" class="">
								<colgroup>
									<col width="15%" />
									<col width="35%" />
									<col width="15%" />
									<col width="35%" />
								</colgroup>
								<tbody>
									<tr>
										<th><span><?=getMsg('th.login_naver')?></span></th>
										<td>
											<input type="text" class="input50p" id="login_naver" name="login_naver" value="<?=LOGIN_NAVER?>" />
											<label class="b_snor_c"><input type="checkbox" id="login_naver_use" name="login_naver_use" value="1" <?=getChecked(LOGIN_NAVER_USE, 1)?> /><i></i> <?=getMsg('th.check_use')?></label>
											<br/>
											<label class="etc">ex) 네이버 Client ID : cDshBRSZOiZb3mmcUZv5</label>
										</td>
										<th><span><?=getMsg('th.login_kakao')?></span></th>
										<td>
											<input type="text" class="input50p" id="login_kakao" name="login_kakao" value="<?=LOGIN_KAKAO?>" />
											<label class="b_snor_c"><input type="checkbox" id="login_kakao_use" name="login_kakao_use" value="1" <?=getChecked(LOGIN_KAKAO_USE, 1)?> /><i></i> <?=getMsg('th.check_use')?></label>
											<br/>
											<label class="etc">ex) 카카오 Javascript 키 : 56b7041cad96452d885c5fd22a22f765 </label>
										</td>
									</tr>
									<tr>
										<th><span><?=getMsg('th.login_google')?></span></th>
										<td>
											<input type="text" class="input50p" id="login_google" name="login_google" value="<?=LOGIN_GOOGLE?>" />
											<label class="b_snor_c"><input type="checkbox" id="login_google_use" name="login_google_use" value="1" <?=getChecked(LOGIN_GOOGLE_USE, 1)?> /><i></i> <?=getMsg('th.check_use')?></label>
											<br/>
											<label class="etc">ex) 구글 Client ID : 1022166514691-k4b3adic2ipffcgtlrgodg0he0gnukml.apps.googleusercontent.com</label>
										</td>
										<th><span><?=getMsg('th.login_facebook')?></span></th>
										<td>
											<input type="text" class="input50p" id="login_facebook" name="login_facebook" value="<?=LOGIN_FACEBOOK?>" />
											<label class="b_snor_c"><input type="checkbox" id="login_facebook_use" name="login_facebook_use" value="1" <?=getChecked(LOGIN_FACEBOOK_USE, 1)?> /><i></i> <?=getMsg('th.check_use')?></label>
											<br/>
											<label class="etc">ex) 페이스북 Client ID : 134205310693401</label>
										</td>
									</tr>
								</tbody>
							</table>
							
							<h3><?=getMsg('th.hospital_info')?></h3>
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="<?=getMsg('alt.text.offholiday')?>" class="">
								<colgroup>
									<col width="15%" />
									<col width="35%" />
									<col width="15%" />
									<col width="35%" />
								</colgroup>
								<tbody>
									<tr>
										<th><span><?=getMsg('th.default_branch_no')?></span></th>
										<td>
											<input type="text" class="input25p" id="default_branch_no" name="default_branch_no" value="<?=DEFAULT_BRANCH_NO?>" readonly onkeyup="isOnlyNumber(this)"/> <?=getMsg('lable.radio.default')?>
											<br/>
											<label class="etc">지점 여러개일 경우 대표 지점 설정 (hospital_fk 입력)</label>
										</td>
										<th><span><?=getMsg('th.default_doctor_no')?></span></th>
										<td>
											<input type="text" class="input25p" id="default_doctor_no" name="default_doctor_no" value="<?=DEFAULT_DOCTOR_NO?>" readonly onkeyup="isOnlyNumber(this)"/> <?=getMsg('lable.radio.default')?>
											<br/>
											<label class="etc">의료진 여러명일 경우 대표 의료진 설정 (doctor_fk 입력)</label>
										</td>
									</tr>
									<!--tr>
										<th><span><?=getMsg('th.manage_open')?></span></th>
										<td>
											<label class="b_snor_r"><input type="radio" id="manager_open0" name="manager_open" value="0" <?=getChecked(MANAGER_OPEN,"0")?>/><i></i><span class="red_col vaM"><?=getMsg('lable.radio.nouse')?></span> <?=getMsg('lable.radio.default')?></label>
											<label class="b_snor_r"><input type="radio" id="manager_open1" name="manager_open" value="1" <?=getChecked(MANAGER_OPEN,"1")?>/><i></i><span class="blue_col vaM"><?=getMsg('lable.radio.use')?></span></label>
										</td>
										<th><span><?=getMsg('th.clinic_doctor_rel')?></span></th>
										<td>
											<label class="b_snor_r"><input type="radio" id="clinic_doctor_rel0" name="clinic_doctor_rel" value="0" <?=getChecked(CLINIC_DOCTOR_REL,"0")?>/><i></i><span class="red_col vaM"><?=getMsg('lable.radio.nouse')?></span> <?=getMsg('lable.radio.default')?></label>
											<label class="b_snor_r"><input type="radio" id="clinic_doctor_rel1" name="clinic_doctor_rel" value="1" <?=getChecked(CLINIC_DOCTOR_REL,"1")?>/><i></i><span class="blue_col vaM"><?=getMsg('lable.radio.use')?></span></label>
										</td>
									</tr-->
									<tr>
										<th><span><?=getMsg('th.reser_jungbok')?></span></th>
										<td colspan="3">
											<label class="b_snor_r"><input type="radio" id="reser_jungbok0" name="reser_jungbok" value="0" <?=getChecked(RESER_JUNGBOK,"0")?>/><i></i><span class="red_col vaM"><?=getMsg('lable.radio.nouse')?></span> <?=getMsg('lable.radio.default')?></label>
											<label class="b_snor_r"><input type="radio" id="reser_jungbok1" name="reser_jungbok" value="1" <?=getChecked(RESER_JUNGBOK,"1")?>/><i></i><span class="blue_col vaM"><?=getMsg('lable.radio.use')?></span></label>
										</td>
										<!--th><span><?=getMsg('th.reser_bundle')?></span></th>
										<td>
											<label class="b_snor_r"><input type="radio" id="reser_bundle0" name="reser_bundle" value="0" <?=getChecked(RESER_BUNDLE,"0")?>/><i></i><span class="red_col vaM"><?=getMsg('lable.radio.nouse')?></span> <?=getMsg('lable.radio.default')?></label>
											<label class="b_snor_r"><input type="radio" id="reser_bundle1" name="reser_bundle" value="1" <?=getChecked(RESER_BUNDLE,"1")?>/><i></i><span class="blue_col vaM"><?=getMsg('lable.radio.use')?></span></label>
										</td-->
									</tr>
								</tbody>
							</table>
							
							<h3><?=getMsg('th.sms_info')?> <span class="colR01 under_f">비젠소프트 호스팅 이용시 적용되는 부분입니다. 서버담당자에게 문의하세요.</span></h3>
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="<?=getMsg('alt.text.offholiday')?>" class="">
								<colgroup>
									<col width="15%" />
									<col width="35%" />
									<col width="15%" />
									<col width="35%" />
								</colgroup>
								<tbody>
									<tr>
										<th><span><?=getMsg('th.sms_transmitter')?></span></th>
										<td>
											<input type="text" class="input25p" id="sms_transmitter" name="sms_transmitter" value="<?=SMS_TRANSMITTER?>" />
										</td>
										<th><span><?=getMsg('th.sms_keyno')?></span></th>
										<td>
											<input type="text" class="input25p" id="sms_keyno" name="sms_keyno" value="<?=SMS_KEYNO?>" />
										</td>
									</tr>
									<tr>
										<th><span><?=getMsg('th.sms_username')?></span></th>
										<td>
											<input type="text" class="input25p" id="sms_username" name="sms_username" value="<?=SMS_USERNAME?>" />
											<br/>
											<label class="etc">업체의 대표자명</label>
										</td>
										<th><span><?=getMsg('th.sms_cname')?></span></th>
										<td>
											<input type="text" class="input25p" id="sms_cname" name="sms_cname" value="<?=SMS_CNAME?>" />
											<br/>
											<label class="etc">문자 발송시의 업체이름</label>
										</td>
									</tr>
								</tbody>
							</table>
							
							<h3><?=getMsg('th.email_info')?> <span class="colR01 under_f">비젠소프트 호스팅 이용시 적용되는 부분입니다. 서버담당자에게 문의하세요.</span></h3>
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="<?=getMsg('alt.text.offholiday')?>" class="">
								<colgroup>
									<col width="15%" />
									<col width="35%" />
									<col width="15%" />
									<col width="35%" />
								</colgroup>
								<tbody>
									<tr>
										<th><span><?=getMsg('th.smtp_host')?></span></th>
										<td>
											<input type="text" class="input25p" id="smtp_host" name="smtp_host" value="<?=SMTP_HOST?>" />
										</td>
										<th><span><?=getMsg('th.smtp_user')?></span></th>
										<td>
											<input type="text" class="input25p" id="smtp_user" name="smtp_user" value="<?=SMTP_USER?>" />
										</td>
									</tr>
									<tr>
										<th><span><?=getMsg('th.smtp_password')?></span></th>
										<td>
											<input type="text" class="input25p" id="smtp_password" name="smtp_password" value="<?=SMTP_PASSWORD?>" />
										</td>
										<th><span><?=getMsg('th.email_form')?></span></th>
										<td>
											<input type="text" class="input50p" id="email_form" name="email_form" value="<?=EMAIL_FORM?>" />
											<br/>
											<label class="etc">기본으로 사용할 이메일 폼의 경로</label>
										</td>
									</tr>
								</tbody>
							</table>
							
							
							<h3><?=getMsg('th.weblog_info')?> <span class="colR01 under_f">비젠소프트 호스팅 이용시 적용되는 부분입니다. 서버담당자에게 문의하세요.</span></h3>
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="<?=getMsg('alt.text.offholiday')?>" class="">
								<colgroup>
									<col width="15%" />
									<col width="35%" />
									<col width="15%" />
									<col width="35%" />
								</colgroup>
								<tbody>
									<tr>
										<th><span><?=getMsg('th.is_log')?></span></th>
										<td>
											<label class="b_snor_r"><input type="radio" class="" id="is_log0" name="is_log" value="0" <?=getChecked(IS_LOG,"0")?>/><i></i><span class="red_col vaM"><?=getMsg('lable.radio.nouse')?></span></label>
											<label class="b_snor_r"><input type="radio" class="" id="is_log1" name="is_log" value="1" <?=getChecked(IS_LOG,"1")?>/><i></i><span class="blue_col vaM"><?=getMsg('lable.radio.use')?></span> <?=getMsg('lable.radio.default')?></label>
										</td>
										<th><span><?=getMsg('th.log_ver')?></span></th>
										<td>
											<label class="b_snor_r"><input type="radio" id="log_ver1" name="log_ver" value="1" <?=getChecked(LOG_VER,"1")?>/><i></i><span class="vaM"><?=getMsg('lable.radio.original')?></span> <?=getMsg('lable.radio.default')?></label>
											<label class="b_snor_r"><input type="radio" id="log_ver2" name="log_ver" value="2" <?=getChecked(LOG_VER,2)?>/><i></i><span class="vaM"><?=getMsg('lable.radio.renew')?></span></label>
										</td>
									</tr>
									<tr>
										<th><span><?=getMsg('th.log_type')?></span></th>
										<td>
											<label class="b_snor_r"><input type="radio" id="log_type0" name="log_type" value="0" <?=getChecked(LOG_TYPE,"0")?>/><i></i><span class="vaM"><?=getMsg('lable.radio.free')?></span> <?=getMsg('lable.radio.default')?></label>
											<label class="b_snor_r"><input type="radio" id="log_type1" name="log_type" value="1" <?=getChecked(LOG_TYPE,"1")?>/><i></i><span class="vaM"><?=getMsg('lable.radio.nofree')?></span></label>
										</td>
										<th><span>
											<?=getMsg('th.weblog_number')?>
											<br/>	
											<?=getMsg('th.weblog_number_mo')?>
											<br/>	
											<?=getMsg('th.weblog_number_new')?>
										</span></th>
										<td>
											<input type="text" class="input25p" id="weblog_number" name="weblog_number" value="<?=WEBLOG_NUMBER?>" onkeyup="isOnlyNumber(this)"/>
											<br/>
											<input type="text" class="input25p" id="weblog_number_mo" name="weblog_number_mo" value="<?=WEBLOG_NUMBER_MO?>" onkeyup="isOnlyNumber(this)"/>
											<br/>
											<input type="text" class="input25p" id="weblog_number_new" name="weblog_number_new" value="<?=WEBLOG_NUMBER_NEW?>" onkeyup="isOnlyNumber(this)"/>
										</td>
									</tr>
								</tbody>
							</table>

							<div class="btn">
								<div class="btnLeft">
									<p class="re_bttm"><span class="col01">*</span>저장 후 반영이 안될 시, 서버담당자 또는 개발자에게 문의하세요.</p>
								</div>
								<div class="btnRight">
									<!-- property:저장 -->
									<a class="btns" href="#" onclick="goSave()"><strong><?=getMsg('btn.save')?></strong> </a>
								</div>
							</div>
							<!--//btn-->
						<input type="hidden" name="cmd" value="update">
						</div>
						</form>
						</div>
						<!-- //bread -->	
					</div>
					<!-- //bbs --> 
					<!-- 내용 : e -->
				</div>
				<!--//con -->
			</div>
			<!--//content -->
		</div>
		<!--//container --> 
		<!-- E N D :: containerArea-->
	</div>
	<!--//canvas -->
</div>
<!--//wrap -->

</body>
</html>