<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/environment/Admin.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

$admin = new Admin($pageRows, "admin", $_REQUEST);
$result = $admin->getGradeList();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/boardConfig/config.php" ?>
<script>

function goSave(obj) {
	
	
	if(validation(obj)){
		
		$("#grade").submit();	
		
	}
}

function groupDelete() {	
	if ( $('[name^=no]:checked').length > 0 ) {
		document.frm.submit();
	} else {
		alert("<?=getMsg('alert.text.delete')?>");
	}
}

function goEdit(no, grade_name) {
	
	if ($("#frm #"+grade_name+"").val().length == 0) {
		alert("권한등급이름을 입력하세요.");
		$("#frm #"+grade_name+"").focus();
		return false;
	}
	var tName = $("#frm #"+grade_name+"").val();
	location.href="process.php?cmd=gradeEdit&no="+no+"&grade_name="+tName;
	
}


</script>
</head>
<body> 
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<div id="wrap">
	<!-- canvas -->
	<div id="canvas">
		<!-- S T A R T :: headerArea-->
		<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/top.php" ?>
		<!-- E N D :: headerArea--> 
		
		<!-- S T A R T :: containerArea-->
		<div id="container">
			<div id="content">
				<div class="con_tit">
					<h2><?=$pageTitle?></h2>
				</div>
				<!-- //con_tit -->
				<div class="con">
					<!-- 내용 : s -->
					<div id="bbs">
						<div id="blist">
							<div class="table_wrap">
							<div class="top_search">
								<form name="grade" id="grade" action="process.php" method="post">
								<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리 목록입니다.">
									<colgroup>
										<col class="w20" />
										<col class="w80" />
									</colgroup>
									<tbody>
										<tr>
											<th scope="row"><label for="">등급이름</label></th>
											<td>
												<input type="text" id="grade_name" name="grade_name" value=""  class="input60p" data-value="등급이름을 입력해 주세요."/>
												<a class="btns h30" href="#" onClick="goSave(this)"><strong><?=getMsg('btn.save')?></strong></a>
											</td>
										</tr>
									</tbody>
								</table>
								<input type="hidden" name="cmd" value="grade"/>
								</form>
							</div>
							<!--//top_search-->
							<form name="frm" id="frm" action="<?=getSslCheckUrl($_SERVER['REQUEST_URI'], 'process.php')?>" method="post">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리목록입니다.">
								<colgroup>
									<col class="w5" />
									<col class="w5" />
									<col class="w10" />
									<col class="" />
									<col class="w20" />
									<col class="w10" />
								</colgroup>
								<thead>
									<tr>
										<th scope="col" class="first">
										<label class="b_first_c"><input type="checkbox" name="allChk" id="allChk" onClick="check(this, document.getElementsByName('no[]'))"/>
										<i></i>
										</label>
										</th>
										<th scope="col"><?=getMsg("th.no")?></th>
										<th scope="col">등급이름</th> 
										<th scope="col"><?=getMsg("th.registdate")?></th> 
										<th scope="col"><?=getMsg('btn.edit')?></th> 
										<th scope="col" class="last"><?=getMsg('btn.delete')?></th>
									</tr>
								</thead>
								<tbody>
								<? if (mysql_num_rows($result)==0) { ?>
									<tr>
										<td class="first" colspan="6">등록된 진료과목이 없습니다.</td>
									</tr>
								<?
									 } else {
										 $i = 0;
										 while ($row=mysql_fetch_assoc($result)) {
								?>
									<tr>
										<td class="first">
										<label class="b_nor_c"><input type="checkbox" name="no[]" id="no" value="<?=$row[no]?>"/>
										<i></i>
										</label>
										</td>
										<td><?=$row['no']?></td>
										<td><input type="text" name="grade_name<?=$i?>" id="grade_name<?=$i?>" size="70" value="<?=$row['grade_name']?>"/></td>
										<td><?=$row['registdate']?></td>
										<td><a class="blue_btnre" href="javascript:;" onclick="goEdit('<?=$row['no']?>', 'grade_name<?=$i?>');"><strong><?=getMsg('btn.edit')?></strong> </a></td>
										<td class="last"><a class="red_btnre" href="javascript:;" onclick="delConfirm('<?=getMsg('confirm.text.delete')?>','process.php?cmd=delete&no=<?=$row['no']?>')"><strong><?=getMsg('btn.delete')?></strong> </a></td>
									</tr>
								<?
										}
									 }
								?>
								</tbody>
							</table>
								<input type="hidden" name="cmd" id="cmd" value="gradeGroupDelete"/>
							</div>
							</form>
							<div class="btn">
								<div class="btnLeft">
									<a class="btns" href="#" onclick="groupDelete();"><strong><?=getMsg('btn.delete')?></strong> </a>
								</div>
							</div
							<!--//btn-->
						</div>
						<!-- //blist -->
					</div>
					<!-- //bbs --> 
					<!-- 내용 : e -->
				</div>
				<!--//con -->
			</div>
			<!--//content -->
		</div>
		<!--//container --> 
		<!-- E N D :: containerArea-->
	</div>
	<!--//canvas -->
</div>
<!--//wrap -->

</body>
</html>