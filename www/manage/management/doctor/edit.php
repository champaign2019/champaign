<? session_start(); 
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Hospital.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Clinic.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Doctor.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

$hospital = new Hospital($pageRows, $_REQUEST);
$doctor = new Doctor($pageRows, $_REQUEST);
$rowPageCount = $doctor->getCount($_REQUEST);
$data = $doctor->getData($_REQUEST['no']);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/boardConfig/config.php" ?>

<script type="text/javascript">
$(document).ready(function() {

	$(".img_alt").focus(function(){
		focusAltRemove($(this));
	});
	
	$(".img_alt").blur(function(){
		blurAltInsert($(this));
	});
});

function goSave(obj) {
	
	
	if(validation(obj)){
		
		$("#frm").submit();	
		
	}
}

</script>
</head>
<body> 
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<div id="wrap">
	<!-- canvas -->
	<div id="canvas">
		<!-- S T A R T :: headerArea-->
		<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/top.php" ?>
		<!-- E N D :: headerArea--> 
		
		<!-- S T A R T :: containerArea-->
		<div id="container">
			<div id="content">
				<div class="con_tit">
					<h2><?=$pageTitle?> - [<?=getMsg('th.write')?>]</h2>
				</div>
				<!-- //con_tit -->
				<div class="con">
					<!-- 내용 : s -->
					<div id="bbs">
						<div id="bread">
							<h3><?=getMsg('th.branch_sel')?></h3>
							<div class="table_wrap">
							<form method="post" name="frm" id="frm" action="<?=getSslCheckUrl($_SERVER['REQUEST_URI'], 'process.php')?>" enctype="multipart/form-data">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리 기본내용입니다.">
								<colgroup>
									<col width="15%" />
									<col width="35%" />
									<col width="15%" />
									<col width="35%" />
								</colgroup>
								<tbody>
									<? if ($branch) { ?>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.hospital_fk')?></label></th>
										<td colspan="3">
											<?
												$hospital = new Hospital(999, $_REQUEST);
												$hResult = $hospital->branchSelect();
											?>
											<select name="hospital_fk" id="hospital_fk" data-value="<?=getMsg('alert.text.shospital_fk')?>">
												<option value=""><?=getMsg('th.branch_sel')?></option>
												<? while ($row=mysql_fetch_assoc($hResult)) { ?>
												<option value="<?=$row['no']?>" <?=getSelected($row['no'],$data['hospital_fk'])?>><?=$row['name']?></option>
												<? } ?>
											</select>
										</td>
									</tr>
									<? } ?>
									
									<?
									if($clinic){
										$clinicObj = new Clinic(999, $_REQUEST);
										$clinicList = $clinicObj->selectBoxList(3,($branch ? 0 : DEFAULT_BRANCH_NO),$data['clinic_fk']);
										
									?>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.clinic_fk')?></label></th>
										<td colspan="3">
											<select name="clinic_fk" id="clinic_fk" data-value="<?=getMsg('alert.text.sclinic_fk')?>" >
											<?= $clinicList ?>
										</select>
										</td>
									</tr>
									<?
									}
									?>
									
									<tr>
										<th scope="row"><label for="">의료진명</label></th>
										<td>
											<input type="text" id="name" name="name" value="<?=$data['name']?>" class="input80p" data-value="의료진을 입력해 주세요."/>
										</td>
										<th scope="row"><label for=""><?=getMsg('th.email')?></label></th>
										<td>
											<input type="text" id="email" name="email" value="<?=$data['email']?>" class="input80p"  />
										</td>
									</tr>
									<tr>
										<td colspan="4">
											<textarea id="memo" name="memo" title="내용을 입력해주세요" style="width:100%;"><?=$data[name]?></textarea>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for="">의료진 사진</label></th>
										<td>
										<? if ($data[imagename]) { ?>
											<p><?=getMsg('lable.image_org')?> : <?=$data[imagename]?><br />
												<input type="checkbox" id="listimage_chk" name="listimage_chk" value="1" title="기존파일삭제를 하시려면 체크해주세요" />
												<label for="listimage_chk"><?=getMsg('lable.checkbox.image_del')?></label>
											</p>
										<? } ?>
											<input type="file" id="imagename" name="imagename" value="" class="input92p"/>
										
										</td>
										<th scope="row"><label for="">의료진 사진 설명</label></th>
										<td>
											<input type="text" id="image_alt" name="image_alt" value="<?=$data[image_alt]?>" class="input80p"  />
										</td>
									</tr>
								</tbody>
							</table>
							<input type="hidden" name="stype" id="stype" value="<?=$_REQUEST[stype]?>"/>
							<input type="hidden" name="sval" id="sval" value="<?=$_REQUEST[sval]?>"/>
							<input type="hidden" name="cmd" id="cmd" value="edit"/>
							<input type="hidden" name="no" id="no" value="<?=$data[no]?>"/>
							</form>
							</div>
							<div class="btn">
								<div class="btnLeft">
									<a class="btns" href="<?=$doctor->getQueryString('index.php', 0, $_REQUEST)?>"><strong><?=getMsg('btn.list')?></strong></a>
								</div>
								<div class="btnRight">
									<a class="btns" href="#" onclick="goSave();"><strong><?=getMsg('btn.save')?></strong></a>
								</div>
							</div>
							<!--//btn-->
						</div>
						<!-- //bread -->
					</div>
					<!-- //bbs --> 
					<!-- 내용 : e -->
				</div>
				<!--//con -->
			</div>
			<!--//content -->
		</div>
		<!--//container --> 
		<!-- E N D :: containerArea-->
	</div>
	<!--//canvas -->
</div>
<!--//wrap -->

</body>
</html>