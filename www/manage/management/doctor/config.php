<?
	$pageRows		= 10;
	$pageTitle		= "의료진 관리";
	$uploadPath		= "/upload/doctor/";								// 파일, 동영상 첨부 경로
	$maxSaveSize	= 50*1024*1024;										// 50Mb
	
	$branch	  		= false;							// 지점 사용유무  
	$clinic	  		= false;							// 진료과목 지점별 사용유무	
	$doctor	  		= false;							// 진료과목 지점별 사용유무	

	$textAreaEditor	= true;							// 에디터 사용여부
	$editorIgnore= "";								//에디터 미사용 배열 구분자 ex : '|0|1|'

	// castle적용
	//include_once($_SERVER['DOCUMENT_ROOT']."/include/castle.php");
?>