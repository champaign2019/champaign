<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/member/Member.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/sms/Sms.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/sms/SaveSms.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

$member = new Member($pageRows, "member", $_REQUEST);
$rowPageCount = $member->getCount($_REQUEST);

$savesms = new SaveSms($saveSmsRows, $_REQUEST);
$rowSaveCount = $savesms->getCount($_REQUEST);
$result = $savesms->getList($_REQUEST);

$receiver = $_REQUEST['receiver'];

$rowPageCount;

if ($_REQUEST['cmd'] == 'group') {
	$_REQUEST['sendtype'] = 'cell';
	$rowPageCount = $member->getCountForSend($_REQUEST);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
<style>
	#reserdate {display:none;}
</style>
<script>
function tranType0(){
	$('#reserdate').hide();
	//$("#Caltran_ymdIcon").hide();
}

function tranType1(){
	$('#reserdate').show();
}

	function goSave() {
		// 핸드폰번호 정규식
		//var regex=/^\d{3}-\d{3,4}-\d{4}$/;

		if ($("#tran_msg").val() == "") {
			alert('발송 메세지를 입력하세요.');
			$("#tran_msg").focus();
			return false;
		}
		//$("#tran_msg").val($("#msg").val());
		
		<? if ($_REQUEST['cmd'] != 'group') { ?>
		if ($("#receiver").val() == "") {
			alert('수신자 번호를 입력하세요.');
			$("#receiver").focus();
			return false;
		}
		<? } ?>

		
		if ( !$(":input:radio[name=tran_type]")[0].checked ) {

			if ($("#tran_ymd").val().length < 1) {
				alert('달력버튼을 눌러 예약발송일자을 선택해주세요.');
				$("#tran_ymd").focus();
				return false;
			}

			if ($("#tran_hour").val().length < 2) {
				alert('예약발송일자 시간을 두자리로 적어주세요.');
				$("#tran_hour").focus();
				return false;

			}

			if ($("#tran_minute").val().length < 2) {
				alert('예약발송일자 분을 두자리로 적어주세요.');
				$("#tran_minute").focus();
				return false;
			}
			
			var reserYear = $("#tran_ymd").val().substring(0,4);
			var reserMonth = $("#tran_ymd").val().substring(5,7);
			var reserDay = $("#tran_ymd").val().substring(8,10);

			var now = new Date();
			var reser = new Date(reserYear, reserMonth-1, reserDay);			

			if(reser <= now)  {
				alert('당일이후부터 예약이 가능합니다.');
				$("#tran_ymd").focus();
				return false;
			}

		}
		
		var byteCnt = calculate_byte($("#tran_msg").val());
		if(byteCnt > 90) {
			document.getElementById("cmd").value = "lms";
		} else if((byteCnt <= 90) && ("group" != "<?=$_REQUEST['cmd']?>")){
			document.getElementById("cmd").value = "sms";
		}
		/* 
		else if($(":input:radio[name=tran_type]:checked").val() == 1) {
			document.getElementById("cmd").value = "reser";
		}
		 */
		document.frm.submit();
	}
	
	function msgApply(msg) {
		document.getElementById("tran_msg").value = document.getElementById(msg).value;
		selfchkLength(document.frm);
	}
	
	function msgEdit(msg, no) {
		document.getElementById("cmd").value = "edit";
		document.getElementById("no").value = no;
		document.getElementById("tran_msg").value = document.getElementById(msg).value;
		document.frm.submit();
	}

	function msgDelete(no) {
		document.getElementById("cmd").value = "del";
		document.getElementById("no").value = no;
		document.frm.submit();
	}
	
	function msgSave() {
		var fr = document.frm;
		//$("#tran_msg").val($("#msg").val());
		var sure = confirm('메시지를 저장하시겠습니까?');

		if(sure) {
			document.getElementById("cmd").value = "save";
			fr.submit();
		}
	}

	// 마우스 좌표값 구하기
	function mouseCoords(ev) {	
		if(ev.pageX || ev.pageY){		
			return {x:ev.pageX, y:ev.pageY};	
		}	
		return {
			x:ev.clientX + document.body.scrollLeft - document.body.clientLeft,
			y:ev.clientY + document.body.scrollTop  - document.body.clientTop	
		};
	}
	 
	// 달력부분
	$(window).load(function() {
		
		initCal({id:"tran_ymd",type:"day",today:"y"});
		
		$("#helpCmt").hover(
				function(){
					var ev = window.event;	
					var pos = mouseCoords(ev);
					
					$("#layerSms").css({display:"block",top:pos.y,left:pos.x});
				},
				function(){
					$("#layerSms").css({display:"none"});
				}
		);
		
	});
	
	// 메세지 SMS & MMS 아이콘 변경
	function ShowBlend() {
		var smsImage = document.getElementById("smsImage");
		smsImage.src = "/manage/img/ico_mms.gif";
	}
	function ReturnBlend() {
		var smsImage = document.getElementById("smsImage");
		smsImage.src = "/manage/img/ico_sms.gif";
	}
	
	// 메세지 내용 Byte 체크
	function selfchkLength(frm) {
		var f = frm;
		var tmpStr, nStrLen, reserve;

		sInputStr = f.tran_msg.value;
		nStrLen = calculate_byte(sInputStr);

		if ( nStrLen > 90) {
			ShowBlend();
		} else {
			ReturnBlend();
		}

		if ( nStrLen > 2000 ) {
			tmpStr = Cut_Str(sInputStr,2000);
			reserve = nStrLen - 2000;

			alert("작성하신 내용은 " + reserve + "바이트가 초과되었습니다.(최대 2000Bytes)"); 

			// 2000byte에 맞게 입력내용 수정
			f.tran_msg.value = tmpStr;
			nStrLen = calculate_byte(tmpStr);
			document.getElementById("msgLength").value = nStrLen;
		} else {
			document.getElementById("msgLength").value = nStrLen;
		}
		return;
	}
</script>
</head>
<body> 
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<div id="wrap">
	<!-- canvas -->
	<div id="canvas">
		<!-- S T A R T :: headerArea-->
		<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/top.php" ?>
		<!-- E N D :: headerArea--> 
		
		<!-- S T A R T :: containerArea-->
		<div id="container">
			<div id="content">
				<div class="con_tit">
					<h2>문자메세지</h2>
				</div>
				<!-- //con_tit -->
				<div class="con">
					<!-- 내용 : s -->
					<div id="bbs">
						<form name="frm" id="frm" action="process.php" method="post" >
						
						<div class="entered_into up_type">
							<span class="colR01">* SMS 1건 발송시 27.5원 ⁄ LMS 1건 발송시 66원의 비용이 발생됩니다.</span>
						</div>
						<? if ($_REQUEST['cmd'] == 'group') { ?>
						<div class="entered_into up_type">
							<span class="colR01">총 <?=$rowPageCount[0]?>명</b>&nbsp;&nbsp;<span class=color1>(휴대폰이 등록되어 있지 않는 회원, 휴대폰 형식에 맞지않는 회원은 제외되어 회원검색에서 회원수와 회원발송 총수가 차이가 날 수 있습니다.)</span>
						</div>
						<? } ?>
						<!--//entered_into-->
						<fieldset>
						<div id="sms">
							<div class="sms_top">
								<div class="phone">
									<fieldset>
										<legend>문자보내기</legend>
										<dl class="smsbox">
											<dt><textarea id="tran_msg" name="tran_msg" rows="11" cols="32" onKeyUp="selfchkLength(document.frm);"></textarea></dt>
											<dd id="helpCmt"><input type="text" id="msgLength" name="msgLength" readonly="readonly" size="2" value="0" class="bytes"/> bytes
											<img id="smsImage" src="/manage/img/ico_sms.gif" alt="sms icon" style="margin-top: 8px;"/>
											</dd>
											<dd class="ddright"><a href="javascript:msgSave();" class="blue_btn"><?=getMsg('btn.save')?></a></dd>

										</dl>
										<div class="smstel">
											<dl>
												<dt><label>보내는사람</label></dt>
												<dd><input type="text" id="tran_callback" name="tran_callback" value="<?=COMPANY_TEL?>" onKeyUp="isNumberOrHyphen(this);cvtPhoneNumber(this)"/></dd>
											</dl>
											<dl>
												<dt><label>받는사람</label></dt>
												<dd><textarea name="receiver" rows="4" COLS="20" id="receiver" name="receiver" onKeyUp="isNumberOrHyphenOrEnter(this);cvtPhoneNumberForTextArea(this)"><?=$receiver?></textarea></dd>
											</dl>
											<ul>
												<li><input type="radio" id="tran_type0" name="tran_type" value="0" onclick="tranType0();" checked/><label for="tran_type0">즉시전송</label></li>
												<li><input type="radio" id="tran_type1" name="tran_type" value="1" onclick="tranType1()"/><label for="tran_type1">예약전송</label>
													<div id="reserdate" class="smsreser">
														<dl>
															<dd>
																<input type="text" id="tran_ymd" name="tran_ymd"  style="width:80px;" readonly />
																<span id="Caltran_ymdIcon">
																	<img src="/manage/img/calendar_icon.png" id="Caltran_ymdIconImg" style="cursor:pointer; vertical-align: sub;"/>
																</span>
																<input type="text" id="tran_hour" name="tran_hour" maxLength="2" onKeyUp="isOnlyNumber(this)"/> 시 
																<input type="text" id="tran_minute" name="tran_minute" maxLength="2" onKeyUp="isOnlyNumber(this)"/> 분
															</dd>
														</dl>
													</div>
												</li>
											</ul>
										</div>
									</fieldset>
									<p alt="send" id="send" onclick="goSave();" style="cursor:pointer;"/>보내기</p>
								</div>
								<!-- //phone -->
								<div class="special">
									<!--<p>특수문자</p>-->
									<ul>
										<li><a onClick="addChar('☆');" style="cursor:pointer;">☆</a></li>
										<li><a onClick="addChar('○');" style="cursor:pointer;">○</a></li>
										<li><a onClick="addChar('□');" style="cursor:pointer;">□</a></li>
										<li><a onClick="addChar('◎');" style="cursor:pointer;">◎</a></li>
										<li><a onClick="addChar('★');" style="cursor:pointer;">★</a></li>
										<li><a onClick="addChar('●');" style="cursor:pointer;">●</a></li>
										<li><a onClick="addChar('■');" style="cursor:pointer;">■</a></li>
										<li><a onClick="addChar('⊙');" style="cursor:pointer;">⊙</a></li>
										<li><a onClick="addChar('☏');" style="cursor:pointer;">☏</a></li>
										<li><a onClick="addChar('☎');" style="cursor:pointer;">☎</a></li>
										<li><a onClick="addChar('◈');" style="cursor:pointer;">◈</a></li>
										<li><a onClick="addChar('▣');" style="cursor:pointer;">▣</a></li>
										<li><a onClick="addChar('◐');" style="cursor:pointer;">◐</a></li>
										<li><a onClick="addChar('◑');" style="cursor:pointer;">◑</a></li>
										<li><a onClick="addChar('☜');" style="cursor:pointer;">☜</a></li>
										<li><a onClick="addChar('☞');" style="cursor:pointer;">☞</a></li>
										<li><a onClick="addChar('◀');" style="cursor:pointer;">◀</a></li>
										<li><a onClick="addChar('▶');" style="cursor:pointer;">▶</a></li>
										<li><a onClick="addChar('▲');" style="cursor:pointer;">▲</a></li>
										<li><a onClick="addChar('▼');" style="cursor:pointer;">▼</a></li>
										<li><a onClick="addChar('♠');" style="cursor:pointer;">♠</a></li>
										<li><a onClick="addChar('♣');" style="cursor:pointer;">♣</a></li>
										<li><a onClick="addChar('♥');" style="cursor:pointer;">♥</a></li>
										<li><a onClick="addChar('◆');" style="cursor:pointer;">◆</a></li>
										<li><a onClick="addChar('◁');" style="cursor:pointer;">◁</a></li>
										<li><a onClick="addChar('▷');" style="cursor:pointer;">▷</a></li>
										<li><a onClick="addChar('△');" style="cursor:pointer;">△</a></li>
										<li><a onClick="addChar('▽');" style="cursor:pointer;">▽</a></li>
										<li><a onClick="addChar('♤');" style="cursor:pointer;">♤</a></li>
										<li><a onClick="addChar('♧');" style="cursor:pointer;">♧</a></li>
										<li><a onClick="addChar('♡');" style="cursor:pointer;">♡</a></li>
										<li><a onClick="addChar('◇');" style="cursor:pointer;">◇</a></li>
										<li><a onClick="addChar('※');" style="cursor:pointer;">※</a></li>
										<li><a onClick="addChar('♨');" style="cursor:pointer;">♨</a></li>
										<li><a onClick="addChar('♪');" style="cursor:pointer;">♪</a></li>
										<li><a onClick="addChar('♭');" style="cursor:pointer;">♭</a></li>
										<li><a onClick="addChar('♩');" style="cursor:pointer;">♩</a></li>
										<li><a onClick="addChar('♬');" style="cursor:pointer;">♬</a></li>
										<li><a onClick="addChar('㉿');" style="cursor:pointer;">㉿</a></li>
										<li><a onClick="addChar('㈜');" style="cursor:pointer;">㈜</a></li>
										
										<li class="t"><a onClick="addChar('(~.^)/');" style="cursor:pointer;">(~.^)/</a></li>
										<li class="t"><a onClick="addChar('づ^0^)づ');" style="cursor:pointer;">づ^0^)づ</a></li>
										<li class="t"><a onClick="addChar('&(☎ ☎)&');" style="cursor:pointer;">&(☎ ☎)&</a></li>
										<li class="t"><a onClick="addChar('(*^.^)♂');" style="cursor:pointer;">(*^.^)♂</a></li>
										<li class="t"><a onClick="addChar('(o^^)o');" style="cursor:pointer;">(o^^)o</a></li>
										<li class="t"><a onClick="addChar('o(^^o)');" style="cursor:pointer;">o(^^o)</a></li>
										<li class="t"><a onClick="addChar('=◑.◐=');" style="cursor:pointer;">=◑.◐=</a></li>
										<li class="t"><a onClick="addChar('_(≥∇≤)ノ');" style="cursor:pointer;">_(≥∇≤)ノ</a></li>
										<li class="t"><a onClick="addChar('q⊙.⊙p');" style="cursor:pointer;">q⊙.⊙p</a></li>
										<li class="t"><a onClick="addChar('o(>_<)o');" style="cursor:pointer;">o(>_<)o</a></li>
										<li class="t"><a onClick="addChar('^.^');" style="cursor:pointer;">^.^</a></li>
										<li class="t"><a onClick="addChar('(^.^)V');" style="cursor:pointer;">(^.^)V</a></li>
										<li class="t"><a onClick="addChar('*^^*');" style="cursor:pointer;">*^^*</a></li>
										<li class="t"><a onClick="addChar('^o^~~♬');" style="cursor:pointer;">^o^~~♬</a></li>
										<li class="t"><a onClick="addChar('^.~');" style="cursor:pointer;">^.~</a></li>
										<li class="t"><a onClick="addChar('＼(*^▽^*)ノ');" style="cursor:pointer;">＼(*^▽^*)</a></li>
										<li class="t"><a onClick="addChar('S(*^___^*)S');" style="cursor:pointer;">S(*^___^*)S</a></li>
										<li class="t"><a onClick="addChar('^Δ^');" style="cursor:pointer;">^Δ^</a></li>
										<li class="t"><a onClick="addChar('^L^');" style="cursor:pointer;">^L^</a></li>
										<li class="t"><a onClick="addChar('^ε^');" style="cursor:pointer;">^ε^</a></li>
										<li class="t"><a onClick="addChar('^_^');" style="cursor:pointer;">^_^</a></li>
										<li class="t"><a onClick="addChar('(ノ^O^)ノ');" style="cursor:pointer;">(ノ^O^)ノ</a></li>
										<li class="t"><a onClick="addChar('^0^');" style="cursor:pointer;">^0^</a></li>
										<li class="t"><a onClick="addChar('^^ㆀ');" style="cursor:pointer;">^^ㆀ</a></li>
										<li class="t"><a onClick="addChar('^-^ㆀ');" style="cursor:pointer;">^-^ㆀ</a></li>
										<li class="t"><a onClick="addChar('(^^)ㆀ');" style="cursor:pointer;">(^^)ㆀ</a></li>
										<li class="t"><a onClick="addChar('^～ㆀ');" style="cursor:pointer;">^～ㆀ</a></li>
										<li class="t"><a onClick="addChar('*^^*ㆀ');" style="cursor:pointer;">*^^*ㆀ</a></li>
										<li class="t"><a onClick="addChar('(-_-メ)');" style="cursor:pointer;">(-_-メ)</a></li>
										<li class="t"><a onClick="addChar('o(T^T)o');" style="cursor:pointer;">o(T^T)o</a></li>
										<li class="t"><a onClick="addChar('Θ_Θ');" style="cursor:pointer;">Θ_Θ</a></li>
										<li class="t"><a onClick="addChar('(∏.∏)');" style="cursor:pointer;">(∏.∏)</a></li>
										<li class="t"><a onClick="addChar('(づ_ど)');" style="cursor:pointer;">(づ_ど)</a></li>
										                                   	                             	
								</ul>
									
								</div>
								<!-- //special -->
							</div>
							<!-- //sms_top --> 
							
							<div class="save">
							<div id="blist">
							<h3><?=getMsg('th.msg')?></h3>
							</div>
								<ul>
									<? if (mysql_num_rows($result) == 0) { ?>
											<?=getMsg('message.tbody.msg')?>
									<? } else { ?>
									<? 		while ($row = mysql_fetch_assoc($result)) { ?>
									<li class="box">
										<div class="savesms"><textarea id="tran_msg<?=$row['no']?>" rows="11" cols="28" wrap="hard" style="overflow-x:hidden;"><?=$row['tran_msg']?></textarea></div>
										<div class="savebtn">
											<ul>
											<li><a href="javascript:msgApply('tran_msg<?=$row['no']?>');" class="black_btn tye02"><?=getMsg('btn.apply')?></a></li>
											<li><a href="javascript:msgEdit('tran_msg<?=$row['no']?>', '<?=$row['no']?>')" class="blue_btn"><?=getMsg('btn.edit')?></a></li>
											<li><a href="javascript:msgDelete('<?=$row['no']?>')" class="red_btn"><?=getMsg('btn.delete')?></a></li>
											</ul>
										</div>
										<!-- //savebtn --> 
									</li>
									<?}
									}?>
								</ul>
								
							</div>
							<!-- //save --> 
							

						</div>
						<!-- //sms --> 
					</fieldset>
						<? if ($_REQUEST['cmd'] == 'group') { ?>  
						<input type="hidden" name="cmd" id="cmd" value="group" />
						<? } else { ?>
						<input type="hidden" name="cmd" id="cmd" value="" />
						<? } ?>
						<input type="hidden" name="no" id="no" value="" />
						<input type="hidden" name="sissms" id="sissms" value="<?=$_REQUEST['sissms']?>" />
						<input type="hidden" name="stype" id="stype" value="<?=$_REQUEST['stype']?>" />
						<input type="hidden" name="sval" id="sval" value="<?=$_REQUEST['sval']?>" />
						<input type="hidden" name="ssecession" id="ssecession" value="<?=$_REQUEST['ssecession']?>" />
						<input type="hidden" name="sgender" id="sgender" value="<?=$_REQUEST['gender']?>" />
						<input type="hidden" name="sisemail" id="sisemail" value="<?=$_REQUEST['sisemail']?>" />
						<input type="hidden" name="sdatetype" id="sdatetype" value="<?=$_REQUEST['sdatetype']?>" />
					</form>
					<?=pageList($savesms->reqPageNo, $rowSaveCount[1], $savesms->getQueryString('write.php', 0, $_REQUEST))?><!-- 페이징 처리 -->
					</div>
					<!-- //bbs --> 
					<!-- 내용 : e -->
				</div>
				<!--//con -->
			</div>
			<!--//content -->
		</div>
		<!--//container --> 
		<!-- E N D :: containerArea-->
	</div>
	<!--//canvas -->
</div>
<!--//wrap -->

</body>
</html>