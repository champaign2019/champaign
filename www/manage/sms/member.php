<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/member/Member.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Hospital.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

$member = new Member($pageRows, "member", $_REQUEST);
$rowPageCount = $member->getCount($_REQUEST);
$result = $member->getList($_REQUEST);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
<script>
	
	// 달력부분
	$(document).ready(function() {
		//initCal({id:"sstartdate",type:"day",today:"y"});			
		//initCal({id:"senddate",type:"day",today:"y"});
		$('#sstartdate').datetimepicker({timepicker:false,format: "Y-m-d"});
		$("#start_button").click(function(){
			$('#sstartdate').datetimepicker('show');
		})	
		$('#senddate').datetimepicker({timepicker:false,format: "Y-m-d"});
		$("#end_button").click(function(){
			$('#senddate').datetimepicker('show');
		})	
	});
	
	function group_send(){
		$("input[name=cmd]").val("group");
		$("#searchForm").attr("action","write.php").submit();
	}
	
	// 목록 'search' form 정렬 
	function goOrderBy(by, type) {
		var f = document.searchForm;
		f.ordertype.value = type;
		f.orderby.value = by;
		f.submit();
	}
</script>
</head>
<body> 
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<div id="wrap">
	<!-- canvas -->
	<div id="canvas">
		<!-- S T A R T :: headerArea-->
		<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/top.php" ?>
		<!-- E N D :: headerArea--> 
		
		<!-- S T A R T :: containerArea-->
		<div id="container">
			<div id="content">
				<div class="con_tit">
					<h2><?=$pageTitle?> - [회원검색]</h2>
				</div>
				<!-- //con_tit -->
				<div class="con">
					<!-- 내용 : s -->
					<div id="bbs">
						<div id="blist">
							<div class="top_search table_wrap">
								<form name="searchForm" id="searchForm" action="member.php" method="post">
								<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리 목록입니다.">
									<colgroup>
									<? if ($branch) { ?>
										<col class="w7" />
										<col class="w7" />
									<? } ?>
										<col class="w7" />
										<col class="w7" />
										<col class="w7" />
										<col class="w7" />
										<col class="w7" />
										<col style='width:250px;'/>
										<col class="w7" />
										<col class="" />
									</colgroup>
									<tbody>
										<tr>
											<th scope="row"><label for="">상담횟수</label></th>
											<td>
												<select id="sconsultnum" class="w100m2" name="sconsultnum" title="상담횟수">
													<option value="-1"><?=getMsg('th.all')?></option>
													
												</select>
											</td>
											<th scope="row"><label for="">예약횟수</label></th>
											<td>
												<select id="sresernum" class="w100m2" name="sresernum" title="예약횟수">
													<option value="-1"><?=getMsg('th.all')?></option>
													
												</select>
											</td>
											<th><label for="">SMS 수신</label></th>
											<td>
												<select id="sissms" class="w100m2" name="sissms" title="SMS 수신">
													<option value="-1"><?=getMsg('th.all')?></option>
													<?=getReceiveSelectType($_REQUEST['sissms'])?>
												</select>
											</td>
										</tr>
										<tr>
											<th scope="row"><label for="">회원상태</label></th>
											<td >
												<select id="ssecession" class="w100m2" name="ssecession" title="회원상태">
													<option value="-1"><?=getMsg('th.all')?></option>
													<?=getMemberStateType($_REQUEST['ssecession'])?>
												</select>
											</td>
											<th scope="row"><label for="">날짜검색</label></th>
											<td >
												<select name="sdatetype" id="sdatetype" title="날짜검색를 선택해주세요">
													<option value="registdate" <?=getSelected($_REQUEST['sdatetype'], 'registdate')?>>등록일</option>
													<option value="birthday" <?=getSelected($_REQUEST['sdatetype'], 'birthday')?>>생일</option>
												</select>
												<input type="text" id="sstartdate" name="sstartdate" value="<?=$_REQUEST['sstartdate']?>" style="width:65px"/>
												<span id="CalsstartdateIcon">
													<button type="button" id="start_button"><img src="/manage/img/calendar_icon.png" id="CalregistdateIconImg" style="cursor:pointer;"/></button>
												</span>&nbsp;&nbsp;~&nbsp;&nbsp;
												<input type="text" id="senddate" name="senddate" value="<?=$_REQUEST['senddate']?>" style="width:65px"/>
												<span id="CalsenddateIcon">
													<button type="button" id="end_button"><img src="/manage/img/calendar_icon.png" id="CalregistdateIconImg" style="cursor:pointer;"/></button>
												</span>
												&nbsp;&nbsp;
												
											</td>
											<th scope="row"><label for="">검색어 검색</label></th>
											<td>
												<select name="stype" id="stype" title="검색어를 선택해주세요" onchange="$('#searchForm').submit();">
													<option value="all" <?=getSelected($_REQUEST['stype'], 'all')?>><?=getMsg('th.all')?></option>
													<option value="id" <?=getSelected($_REQUEST['stype'], 'id')?>><?=getMsg('th.id')?></option>
													<option value="name" <?=getSelected($_REQUEST['stype'], 'name')?>><?=getMsg('lable.option.name2')?></option>
													<option value="cell" <?=getSelected($_REQUEST['stype'], 'cell')?>>휴대폰</option>
													<option value="email" <?=getSelected($_REQUEST['stype'], 'email')?>>E-mail</option>
													<option value="addr0" <?=getSelected($_REQUEST['stype'], 'addr0')?>>주소</option>
												</select>
												<input type="text" id="sval" name="sval" value="<?=$_REQUEST['sval']?>" class="upserch04" onkeyup="entKeyEventListener(event.keyCode, 'searchForm');"/>
												<a class="blue_btn" href="#" onclick="$('#searchForm').submit();">검색</a>
												<? if($rowPageCount[0] > 0) { ?>
												<a class="black_btn" href="javascript:group_send();" name="group_send" id="group_send">검색된 목록 문자 보내기</a> 
												<? } ?>
												
											</td>
										</tr>
									</tbody>
								</table>
								<input type="hidden" name="ordertype" id="ordertype" value=""/>
								<input type="hidden" name="orderby" id="orderby" value=""/>
								<input type="hidden" name="cmd" value=""/>
								</form>
							</div>
							<!--//top_search-->
							<p><span><strong><?=getMsg('th.total')?> <?=$rowPageCount[0]?><?=getMsg('th.amount')?></strong>  |  <?=$member->reqPageNo?>/<?=$rowPageCount[1]?><?=getMsg('th.page')?></span></p>
							<div class="table_wrap">
							<form name="frm" id="frm" action="process.php" method="post">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리목록입니다.">
								<colgroup>
									<col class="w5" />
									<col class="w10" />
									<col class="w10"/>
									<col class="" />
									<col class="" />
									<col class="w8" />
									<col class="w8" />
									<col class="w8" />
									<col class="w8" />
								</colgroup>
								<thead>
									<tr>
										<th scope="col" class="first">번호</th>
										<th scope="col">회원명 <img src="/manage/img/menu_icon_on.gif" onclick="goOrderBy('name','ASC');" style="cursor:pointer;"/> <img src="/manage/img/menu_icon.gif" onclick="goOrderBy('name','DESC');" style="cursor:pointer; "/> </th>
										<th scope="col">아이디 <img src="/manage/img/menu_icon_on.gif" onclick="goOrderBy('id','ASC');" style="cursor:pointer;"/> <img src="/manage/img/menu_icon.gif" onclick="goOrderBy('id','DESC');" style="cursor:pointer; "/> </th>
										<th scope="col">휴대폰</th>
										<th scope="col">이메일</th>
										<th scope="col">회원상태</th>
										<th scope="col">상담횟수 <img src="/manage/img/menu_icon_on.gif" onclick="goOrderBy('resernum','ASC');" style="cursor:pointer;"/> <img src="/manage/img/menu_icon.gif" onclick="goOrderBy('resernum','DESC');" style="cursor:pointer; "/> </th>
										<th scope="col">예약횟수 <img src="/manage/img/menu_icon_on.gif" onclick="goOrderBy('consultnum','ASC');" style="cursor:pointer;"/> <img src="/manage/img/menu_icon.gif" onclick="goOrderBy('consultnum','DESC');" style="cursor:pointer; "/> </th>
										<th scope="col" class="last">가입일 <img src="/manage/img/menu_icon_on.gif" onclick="goOrderBy('registdate','ASC');" style="cursor:pointer;"/> <img src="/manage/img/menu_icon.gif" onclick="goOrderBy('registdate','DESC');" style="cursor:pointer; "/> </th>
									</tr>
								</thead>
								<tbody>
								<? if ($rowPageCount[0] == 0) { ?>
									<tr>
										<td class="frist" colspan="8">등록된 회원이 없습니다.</td>
									</tr>
								<?
									 } else {
										 $i = 0;
										while ($row = mysql_fetch_assoc($result)) {
								?>
									<tr>
										<td class="first"><?= $rowPageCount[0] - (($member->reqPageNo-1)*$pageRows) - $i?></td>
										<td><?=$row['name']?></td>
										<td><?=$row['id']?></td>
										<td> <a href="write.php?receiver=<?=$row['cell']?>"> <?=$row['cell']?> </a> </td>
										<td><?=$row['email']?></td>
										<td><?=getMemberStateTypeName($row['secession'])?></td>
										<td <?=$targetUrl?>><?=$row['consultnum']?>&nbsp;회</td>
										<td <?=$targetUrl?>><?=$row['resernum']?>&nbsp;회</td>
										<td <?=$targetUrl?> class="last"><?=$row['registdate']?></td>
									</tr>
								<?
										$i++;
										}
									 }
								?>
								</tbody>
							</table>
								<input type="hidden" name="cmd" id="cmd" value="groupDelete"/>
								<input type="hidden" name="stype" id="stype" value="<?=$_REQUEST['stype']?>"/>
								<input type="hidden" name="sval" id="sval" value="<?=$_REQUEST['sval']?>"/>
							</form>
							</div>
							<!-- 페이징 처리 -->
							<?=pageList($member->reqPageNo, $rowPageCount[1], $member->getQueryString('member.php', 0, $_REQUEST))?>
							<!-- //페이징 처리 -->
						</div>
						<!-- //blist -->
					</div>
					<!-- //bbs --> 
					<!-- 내용 : e -->
				</div>
				<!--//con -->
			</div>
			<!--//content -->
		</div>
		<!--//container --> 
		<!-- E N D :: containerArea-->
	</div>
	<!--//canvas -->
</div>
<!--//wrap -->

</body>
</html>