<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/sms/Sms.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

$sms = new Sms($pageRows, $_REQUEST);
$_REQUEST['stran_id'] = SMS_KEYNO;
$_REQUEST['isreser'] = 1;
$rowPageCount = $sms->getCount($_REQUEST);
$result = $sms->getList($_REQUEST);;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
<script>
	function groupDelete() {	
		if (isSeleted(document.frm.no) ){
			document.frm.submit();
		} else {
			alert("<?=getMsg('alert.text.delete')?>");
		}
	}
	
	// 달력부분
	$(window).load(function() {
		//initCal({id:"sday",type:"day",today:"y"});			
		//initCal({id:"eday",type:"day",today:"y"});
		$('#sday').datetimepicker({timepicker:false,format: "Y-m-d"});
		$("#start_button").click(function(){
			$('#sday').datetimepicker('show');
		})	
		$('#eday').datetimepicker({timepicker:false,format: "Y-m-d"});
		$("#end_button").click(function(){
			$('#eday').datetimepicker('show');
		})	
	});
</script>
</head>
<body> 
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<div id="wrap">
	<!-- canvas -->
	<div id="canvas">
		<!-- S T A R T :: headerArea-->
		<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/top.php" ?>
		<!-- E N D :: headerArea--> 
		
		<!-- S T A R T :: containerArea-->
		<div id="container">
			<div id="content">
				<div class="con_tit">
					<h2><?=$pageTitle?> - [예약내역]</h2>
				</div>
				<!-- //con_tit -->
				<div class="con">
					<!-- 내용 : s -->
					<div id="bbs">
						<div id="blist">
							<div class="top_search table_wrap">
								<form name="searchForm" id="searchForm" action="reservation.php" method="post">
								<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="회원검색목록">
									<colgroup>
										<col class="w13" />
										<col class="w30" />
										<col class="w13" />
										<col class="w44" />
									</colgroup>
									<tbody>
										<tr>
											
											<th scope="row"><label for="">검색어 검색</label></th>
											<td><select id="stype" name="stype" title="검색을 선택해주세요">
												<option value="all" <?=getSelected($_REQUEST['stype'], "all") ?>><?=getMsg('th.all')?></option>
												<option value="tran_msg" <?=getSelected($_REQUEST['stype'], "tran_msg") ?>>문자내용</option>
												<option value="tran_callback" <?=getSelected($_REQUEST['stype'], "tran_callback") ?>>발신번호</option>
												<option value="tran_phone" <?=getSelected($_REQUEST['stype'], "tran_phone") ?>>수신번호</option>
											</select>
											<input type="text" name="sval" value="<?=$_REQUEST['sval']?>" title="검색할 내용을 입력해주세요" />
											</td>
											<th scope="row"><label for="">날짜 검색</label></th>
											<td><input type="text" id="sday" name="sday" value="<?=$_REQUEST['sday']?>" maxlength="10"/>
											<span id="CalsdayIcon">
												<button type="button" id="start_button"><img src="/manage/img/calendar_icon.png" id="CalregistdateIconImg" style="cursor:pointer;"/></button>
											</span>
											&nbsp;&nbsp;~&nbsp;&nbsp;
											<input type="text" id="eday" name="eday" value="<?=$_REQUEST['eday']?>" maxlength="10"/>
											<span id="CaledayIcon">
												<button type="button" id="end_button"><img src="/manage/img/calendar_icon.png" id="CalregistdateIconImg" style="cursor:pointer;"/></button>
											</span>
											&nbsp;&nbsp;<a class="blue_btn" href="#" onclick="document.searchForm.submit()">검색</a>
											</td>
										</tr>
									</tbody>
								</table>
								</form>
							</div>
							<!--//top_search-->
							<p><span><strong><?=getMsg('th.total')?> <?=$rowPageCount[0]?><?=getMsg('th.amount')?></strong>  |  <?=$sms->reqPageNo?>/<?=$rowPageCount[1]?><?=getMsg('th.page')?></span></p>
							<div class="table_wrap">
							<form name="frm" id="frm" action="process.php" method="post">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리목록입니다.">
								<colgroup>
									<col class="w3" />
									<col class="w5" />
									<col class="w10" />
									<col class="w10" />
									<col class="w10" />
									<col class="" />
								</colgroup>
								<thead>
									<tr>
										<th scope="col" class="first">
											<input type="checkbox" name="allChk" id="allChk" onClick="check(this, document.frm.no)"/>
										</th>
										<th scope="col"><?=getMsg("th.no")?></th>
										<th scope="col">발신예정일자</th>
										<th scope="col">발신번호</th>
										<th scope="col">수신번호</th>
										<th scope="col" class="last">문자 내용</th>
									</tr>
								</thead>
								<tbody>
								<? if ($rowPageCount[0] == 0) { ?>
									<tr>
										<td class="frist" colspan="6">예약된 SMS가 없습니다.</td>
									</tr>
								<?
									 } else {
										while ($row = mysql_fetch_assoc($result)) {
								?>
									<tr>
										<td class="first">
											<input type="checkbox" name="no[]" id="no" value="<?=$row['tran_pr']?>"/>
										</td>
										<td><?=$row['tran_pr']?></td>
										<td><?=$row['tran_date']?></td>
										<td><?=$row['tran_callback']?></td>
										<td><?=$row['tran_phone']?></td>
										<td class="title last" ><?=$row['tran_msg']?></td>
									</tr>
								<?
										}
									 }
								?>
								</tbody>
							</table>
								<input type="hidden" name="cmd" id="cmd" value="groupDelete"/>
								<input type="hidden" name="stype" id="stype" value="<?=$_REQUEST['stype']?>"/>
								<input type="hidden" name="sval" id="sval" value="<?=$_REQUEST['sval']?>"/>
							</form>
							</div>
							<!--//btn-->
							<div class="btn">
								<div class="btnLeft"> <a class="btns" href="#" onclick="groupDelete();"><strong><?=getMsg('btn.delete')?></strong> </a> </div>
							</div>
							<!-- 페이징 처리 -->
							<?=pageList($sms->reqPageNo, $rowPageCount[1], $sms->getQueryString('reservation.php', 0, $_REQUEST))?>
							<!-- //페이징 처리 -->
						</div>
						<!-- //blist -->
					</div>
					<!-- //bbs --> 
					<!-- 내용 : e -->
				</div>
				<!--//con -->
			</div>
			<!--//content -->
		</div>
		<!--//container --> 
		<!-- E N D :: containerArea-->
	</div>
	<!--//canvas -->
</div>
<!--//wrap -->

</body>
</html>