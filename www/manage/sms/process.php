<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/member/Member.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/sms/Sms.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/sms/SaveSms.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
</head>
<body>
<?
if (checkReferer($_SERVER["HTTP_REFERER"])) {
	$member = new Member($pageRows, "member", $_REQUEST);
	$sms = new Sms($pageRows, $_REQUEST);
	$savesms = new SaveSms($saveSmsRows, $_REQUEST);

	$_REQUEST['sendtype'] = 'cell';
	$_REQUEST['tran_etc1'] = SMS_CNAME;
	$_REQUEST['tran_etc2'] = SMS_USERID;
	$_REQUEST['tran_etc3'] = SMS_USERNAME;
	$_REQUEST['tran_id'] = SMS_KEYNO;
	$_REQUEST['tran_status'] = '1';

	if ($_REQUEST['cmd'] != 'save') {
		if ($_REQUEST['tran_type'] == 0) {
			$_REQUEST['tran_date'] = date('Y-m-d H:i:s');
		} else {
			$_REQUEST['tran_date'] = $_REQUEST['tran_ymd']." ".$_REQUEST['tran_hour'].":".$_REQUEST['tran_minute'].":00";
		}
	}

	// 메세지 저장
	if ($_REQUEST['cmd'] == 'save') {
		$r = $savesms->insert($_REQUEST);
		if ($r > 0) {
			echo returnURLMsg($sms->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'write.php'), 0, $_REQUEST), getMsg('result.text.insert'));
		}
		else {
			echo returnURLMsg($sms->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'write.php'), 0, $_REQUEST), getMsg('result.text.error'));
		}


	}
	else if ($_REQUEST['cmd'] == 'sms' || $_REQUEST['cmd'] == 'lms') {
		if ($_REQUEST['cmd'] == 'lms') {
			$_REQUEST['mms_subject'] = COMAPNY_NAME;
			$_REQUEST['mms_body'] = $_REQUEST['tran_msg'];
		}
		$sucess = $sms->send($_REQUEST);
		if ($sucess) {
			if ($_REQUEST['smspop'] == 1) {
				if ($_REQUEST['tran_type'] == 0) {
					echo popupCloseRefresh('문자 발송이 정상적으로 처리되었습니다.');
				}
				else {
					echo popupCloseRefresh('문자 발송 예약이 정상적으로 처리 되었습니다.');
				}
			}
			else {
				if ($_REQUEST['tran_type'] == 0) {
					echo returnURLMsg($sms->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'write.php'), 0, $_REQUEST), '문자 발송이 정상적으로 처리되었습니다.');
				}
				else {
					echo returnURLMsg($sms->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'reservation.php'), 0, $_REQUEST), '문자 발송 예약이 정상적으로 처리 되었습니다.');
				}
			}
		}
		else {
			echo returnURLMsg($sms->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'write.php'), 0, $_REQUEST), '문자 발송 중 문제가 발생하였습니다.');
		}
	}
	else if ($_REQUEST['cmd'] == 'group') {
		$tempCell;
		$receive = $member->getListForSend($_REQUEST);
		while ($row=mysql_fetch_assoc($receive)) {
			if ($row['cell']) {
				$tempCell .= $row['cell'].'\r\n';
			}
		}

		if ($_REQUEST['receiver']) {
			$_REQUEST['receiver'] .= '\r\n'.$tempCell;
		}
		else {
			$_REQUEST['receiver'] = $tempCell;
		}

		$sucess;
		$sucess = $sms->send($_REQUEST);

		if ($sucess) {
			if ($_REQUEST['smspop'] == 1) {
				if ($_REQUEST['tran_type'] == 0) {
					echo popupCloseRefresh('문자 발송이 정상적으로 처리되었습니다.');
				}
				else {
					echo popupCloseRefresh('문자 발송 예약이 정상적으로 처리 되었습니다.');
				}
			}
			else {
				if ($_REQUEST['tran_type'] == 0) {
					echo returnURLMsg($sms->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'write.php'), 0, $_REQUEST), '문자 발송이 정상적으로 처리되었습니다.');
				}
				else {
					echo returnURLMsg($sms->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'reservation.php'), 0, $_REQUEST), '문자 발송 예약이 정상적으로 처리 되었습니다.');
				}
			}
		}
		else {
			echo returnURLMsg($sms->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'write.php'), 0, $_REQUEST), '문자 발송 중 문제가 발생하였습니다.');
		}

	}
	else if ($_REQUEST['cmd'] == 'edit') {
		$savesms->update($_REQUEST);
		echo returnURLMsg($sms->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'write.php'), 0, $_REQUEST), '메세지가 수정되었습니다.');
	}
	else if ($_REQUEST['cmd'] == 'del') {
		$savesms->delete($_REQUEST['no']);
		echo returnURLMsg($sms->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'write.php'), 0, $_REQUEST), '메세지가 삭제되었습니다.');
	}
	else if ($_REQUEST['cmd'] == 'groupDelete') {
		$no = $_REQUEST['no'];
		
		$r = 0;
		for ($i=0; $i<count($no); $i++) {
			$r += $sms->delete($no[$i]);
			echo $no[$i];
		}

		if ($r > 0) {
			echo returnURLMsg($sms->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'reservation.php'), 0, $_REQUEST), getMsg('result.text.countdelete',$r));
		}
		else {
			echo returnURLMsg($sms->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'reservation.php'), 0, $_REQUEST), getMsg('result.text.error'));
		}
	}

}
else {
	echo returnURLMsg($admin->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.error'));
}
?>

</body>
</html>