<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Common.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

$objCommon = new Common($pageRows, $tablename, $_REQUEST);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
</head>
<body>
<?
if (checkReferer($_SERVER["HTTP_REFERER"])) {

	$_REQUEST['uploadPath'] = $_SERVER['DOCUMENT_ROOT'].$uploadPath;
	
	if ($_REQUEST['cmd'] == 'write') {
		
		if($useWriteAnswer){
			$_REQUEST['whereConsult'] = 1;// 온라인상담 답변형
		}
		
		$_REQUEST = fileupload('filename', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, true, $maxSaveSize);		// 첨부파일
		$_REQUEST = fileupload('moviename', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 동영상
		$_REQUEST = fileupload('imagename1', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지1
		$_REQUEST = fileupload('imagename2', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지2
		$_REQUEST = fileupload('imagename3', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지3
		$_REQUEST = fileupload('imagename4', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지4
		$_REQUEST = fileupload('imagename5', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지5
		$_REQUEST = fileupload('imagename6', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지6
		$_REQUEST = fileupload('imagename7', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지7
		$_REQUEST = fileupload('imagename8', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지8

		$r = $objCommon->insert($_REQUEST);
		if ($r > 0) {
			echo returnURLMsg($objCommon->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.insert'));
		} else {
			echo returnURLMsg($objCommon->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.error'));
		}

	} else if ($_REQUEST['cmd'] == 'edit') {

		$_REQUEST = fileupload('filename', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, true, $maxSaveSize);		// 첨부파일
		$_REQUEST = fileupload('moviename', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 동영상
		$_REQUEST = fileupload('imagename1', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지1
		$_REQUEST = fileupload('imagename2', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지2
		$_REQUEST = fileupload('imagename3', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지3
		$_REQUEST = fileupload('imagename4', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지4
		$_REQUEST = fileupload('imagename5', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지5
		$_REQUEST = fileupload('imagename6', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지6
		$_REQUEST = fileupload('imagename7', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지7
		$_REQUEST = fileupload('imagename8', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지8
		$r = $objCommon->update($_REQUEST);

		if ($r > 0) {
			$data = $objCommon->getData($_REQUEST, false);
//			$hospital = new Hospital(999, $_REQUEST);
//			$hdata = $hospital->getData($_REQUEST['hospital_fk']);

			// 답변여부 체크
			$answercheck = false;
			if ($_REQUEST[answer] != '' && $_REQUEST[answer] != '<p>&nbsp;</p>') {
				$answercheck = true;
			}

			// 메일발송 체크, 답변이 된경우 답변메일 발송
			if ($_REQUEST[email_chk] == '1' and $answercheck == true) {
				$title = "[".COMPANY_NAME."]".$_REQUEST['name']."님의 상담답변이 등록되었습니다. 홈페이지에 방문하여 확인하세요";
				$contents = "
					<table border='0' cellpadding='0' cellspacing='0' width='100%' style='font-family:굴림; font-size: 12px; color: #4b4b4b; border:#CCCCCC 1px solid;'> ";
				if ($branch) {
						$content .= "
							<tr height='30'>									
								<td width=90 style='font-weight:bold; padding-left:5px; border-right:#CCCCCC 1px solid; border-bottom:#CCCCCC 1px solid; background-color: #f4f8fd;'>".getMsg('th.hospital_fk')."</td>
								<td style='padding-left:5px; border-bottom:#CCCCCC 1px solid;'>".$data['hospital_name']."</td>
							</tr>	";
					}
					if ($clinic) {
						$content .= "
							<tr height='30'>									
								<td width=90 style='font-weight:bold; padding-left:5px; border-right:#CCCCCC 1px solid; border-bottom:#CCCCCC 1px solid; background-color: #f4f8fd;'>".getMsg('th.clinic_fk')."</td>
								<td style='padding-left:5px; border-bottom:#CCCCCC 1px solid;'>".$data['clinic_name']."</td>
							</tr>	";
					}
				$contents .= "
					<tr height='30'>									
						<td width=90 style='font-weight:bold; padding-left:5px; border-right:#CCCCCC 1px solid; border-bottom:#CCCCCC 1px solid; background-color: #f4f8fd;'>성명</td>
						<td style='padding-left:5px; border-bottom:#CCCCCC 1px solid;'>".$_REQUEST['name']."</td>
					</tr>												
					<tr height='30'>									
						<td style='font-weight:bold; padding-left:5px; border-right:#CCCCCC 1px solid; border-bottom:#CCCCCC 1px solid; background-color: #f4f8fd;'>".getMsg('th.cell')."</td>	
						<td style='padding-left:5px; border-bottom:#CCCCCC 1px solid;'>".$_REQUEST['cell']."</td>	
					</tr>												
					<tr height='30'>									
						<td style='font-weight:bold; padding-left:5px; border-right:#CCCCCC 1px solid; border-bottom:#CCCCCC 1px solid; background-color: #f4f8fd;'>".getMsg('th.email')."</td>	
						<td style='padding-left:5px; border-bottom:#CCCCCC 1px solid;'>".$_REQUEST['email']."</td>		
					</tr>												
					<tr height='30'>									
						<td style='font-weight:bold; padding-left:5px; border-right:#CCCCCC 1px solid; background-color: #f4f8fd;'>상담제목</td>	
						<td style='padding-left:5px;'>".$_REQUEST['title']."</td>
					</tr>												
					</table>";
				
				$mailForm = getURLMakeMailForm(COMPANY_URL, EMAIL_FORM);
				$mailForm = str_replace(":SUBJECT", $title, $mailForm);
				$mailForm = str_replace(":CONTENT", $contents, $mailForm);
				$sendmail = new SendMail();
				$sendmail->send(COMPANY_EMAIL, COMPANY_NAME, $_REQUEST['email'], $title, $mailForm);
			}

			// SMS발송
			if ($_REQUEST['sms_send'] == 1) {
				$sms = new Sms(999, $_REQUEST);
				$_REQUEST['tran_id'] = SMS_KEYNO;
				$_REQUEST['tran_etc1'] = SMS_CNAME;
				$_REQUEST['tran_etc2'] = SMS_USERID;
				$_REQUEST['tran_etc3'] = SMS_USERNAME;
				$_REQUEST['sendtype'] = 'cell';
				$_REQUEST['tran_status'] = '1';
				$_REQUEST['tran_date'] = getFullToday();

				if ($_REQUEST[cell] && $hdata['reser_write_user'] == "1") {
					// 받을 사람(사용자)
					// 핸드폰번호가 있고 상담글 등록시 사용자에게 SMS발송
					$msg = str_replace("#name#", $_REQUEST[name], $hdata['reser_sms_msg']);
					$_REQUEST['tran_callback'] = $hdata['reser_admin_tel'];
					$_REQUEST['receiver'] = $data['cell'];
					$_REQUEST['tran_msg'] = $msg;
					$sms->sendSMS($_REQUEST);
				}
			}

			if( COMPANY_EMAIL == "" ){
				//echo "::".$_REQUEST[answer]."::";
				//echo "++".COMPANY_URL."++";
				//echo "++".EMAIL_FORM."++";
				//echo "++".$mailForm."++";
			}


			echo returnURLMsg($objCommon->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.update'));
		} else {
			echo returnURLMsg($objCommon->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.error'));
		}
	}else if ($_REQUEST['cmd'] == 'reply') {
		
		
		$_REQUEST = fileupload('filename', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, true, $maxSaveSize);		// 첨부파일
		$_REQUEST = fileupload('moviename', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 동영상
		$_REQUEST = fileupload('imagename1', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지1
		$_REQUEST = fileupload('imagename2', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지2
		$_REQUEST = fileupload('imagename3', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지3
		$_REQUEST = fileupload('imagename4', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지4
		$_REQUEST = fileupload('imagename5', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지5
		$_REQUEST = fileupload('imagename6', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지6
		$_REQUEST = fileupload('imagename7', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지7
		$_REQUEST = fileupload('imagename8', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지8

		$r = $objCommon->insertReply($_REQUEST);
		if ($r > 0) {
			echo returnURLMsg($objCommon->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.insert'));
		} else {
			echo returnURLMsg($objCommon->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.error'));
		}

	} else if ($_REQUEST['cmd'] == 'groupDelete') {

		$no = $_REQUEST['no'];
		 
		$r = 0;
		for ($i=0; $i<count($no); $i++) {
			$_REQUEST['no'] = $no[$i];

			$r += $objCommon->delete($_REQUEST);
		}

		if ($r > 0) {
			echo returnURLMsg($objCommon->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.countdelete',$r));
		} else {
			echo returnURLMsg($objCommon->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.error'));
		}

	} else if ($_REQUEST['cmd'] == 'delete') {
		
		$r = $objCommon->delete($_REQUEST);

		if ($r > 0) {
			echo returnURLMsg($objCommon->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.delete'));
		} else {
			echo returnURLMsg($objCommon->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.error'));
		}
	}


} else {
	echo returnURLMsg($objCommon->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.error'));
}
?>
</body>
</html>