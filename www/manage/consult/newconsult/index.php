<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Common.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Hospital.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

$_REQUEST['orderby'] = $orderby; //정렬 배열 선언
$_REQUEST['whereConsult'] = $answer; //답변형 게시판일경우 1을 반환함

$objCommon = new Common($pageRows, $tablename, $_REQUEST);
$rowPageCount = $objCommon->getCount($_REQUEST);
$result = $objCommon->getList($_REQUEST);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
<script>

function groupDelete() {	
	if ( $('[name^=no]:checked').length > 0 ) {
		document.frm.submit();
	} else {
		alert("<?=getMsg('alert.text.delete')?>");
	}
}

function goSearch() {
	$("#searchForm").submit();
}


</script>
</head>
<body> 
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<div id="wrap">
	<!-- canvas -->
	<div id="canvas">
		<!-- S T A R T :: headerArea-->
		<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/top.php" ?>
		<!-- E N D :: headerArea--> 
		
		<!-- S T A R T :: containerArea-->
		<div id="container">
			<div id="content">
				<div class="con_tit">
					<h2><?=$pageTitle?> - [<?=getMsg("th.list")?>]</h2>
				</div>
				<!-- //con_tit -->
				<div class="con">
					<!-- 내용 : s -->
					<div id="bbs">
						<div id="blist">
							<p><span><strong><?=getMsg('th.total')?> <?=$rowPageCount[0]?><?=getMsg('th.amount')?></strong>  |  <?=$objCommon->reqPageNo?>/<?=$rowPageCount[1]?><?=getMsg('th.page')?></span></p>
							<form name="frm" id="frm" action="process.php" method="post">
							<div class="table_wrap">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리목록입니다.">
								<colgroup>
									<col class="w3" />
									<col class="w4" />
									<? if ($branch) { ?>
									<col class="w7" />
									<? } ?>
									<? if ($clinic) { ?>
									<col class="w7" />
									<? } ?>
									
									<col class="" />
									<?
									if($useAnswer){
									?>
									<col class="w6" />
									<?
									}
									?>
									<?
									if($useSecret){
									?>
									<col class="w6" />
									<?
									}
									?>
									<?
									if($ismain){
									?>
									<col class="w6" />
									<?
									}
									?>
									<col class="w6" />
									<col class="w10" />
									<?
									if($userCon){
									?>
									<col class="w3" />
									<?
									}
									?>
								</colgroup>
								<thead>
									<tr>
										<th scope="col" class="first">
											<label class="b_first_c"><input type="checkbox" name="allChk" id="allChk" onClick="check(this, document.getElementsByName('no[]'))"/>
											<i></i>
											</label>
										</th>
										<th scope="col"><?=getMsg("th.no")?></th>
										<? 
										if ($branch) { 
										?>
										<th scope="col"><?=getMsg("th.hospital_fk")?></th>
										<? 
										} 
										?>
										<? 
										if ($clinic) { 
										?> 
										<th scope="col"><?=getMsg("th.clinic_fk")?></th>
										<?
										} 
										?>
										
										<th scope="col"><?=getMsg("th.title")?></th> 
										<?
										if($useAnswer){
										?>
										<th scope="col"><?=getMsg("th.answer")?></th> 
										<?
										}
										?>
										<?
										if($useSecret){
										?>
										<th scope="col"><?=getMsg("th.secret")?></th> 
										<?
										}
										?>
										<?
										if($ismain){
										?>
										<th scope="col"><?=getMsg("th.main")?></th> 
										<?
										}
										?>
										<th scope="col"><?=getMsg("th.name")?></th> 
										<th scope="col"><?=getMsg("th.registdate")?></th> 
										<?
										if($userCon){
										?>
										<th scope="col"><?=getMsg("th.userCon")?></th>
										<?
										}
										?>
										
									</tr>
								</thead>
								<tbody>
								<? if ($rowPageCount[0] == 0) { ?>
									<tr>
										<td class="first" colspan="12"><?=getMsg("message.tbody.data")?></td>
									</tr>
								<?
									 } else {
										$targetUrl = "";
										$i = 0;
										while ($row=mysql_fetch_assoc($result)) {
											$targetUrl = "style='cursor:pointer;' onclick=\"location.href='".$objCommon->getQueryString('read.php', $row['no'], $_REQUEST)."'\"";
								?>
									<tr>
										<td class="first">
											<label class="b_nor_c"><input type="checkbox" name="no[]" value="<?=$row[no]?>"/>
											<i></i>
											</label>
										</td>
										<? if ($row['top'] == "1") { ?>
										<td <?=$targetUrl?>>
											<span class="noti_icon">공지</span>
										</td>
										<? } else { ?>
										<td <?=$targetUrl?>><?=$unickNum ? $row['no'] : $rowPageCount[0] - (($objCommon->reqPageNo-1)*$pageRows) - $i?>
										</td>
										<? } ?>
										<? if ($branch) { ?>
										<td <?=$targetUrl?>><?=$row['hospital_name']?></td>
										<? } ?>
										<? if ($clinic) { ?>
										<td <?=$targetUrl?>><?=$row['clinic_name']?></td>
										<? } ?>
										
										<td <?=$targetUrl?> class="title">
											<? if($row['nested'] > 0 && $useWriteAnswer) { ?>
											<span style="margin-left:<?=$row['nested']*10?>px"><img src="/manage/img/ans_licon.png" /> <span class="ans_icon">답변 </span></span>
											<? } ?>
											<?=$row['title']?>
											<? if (checkNewIcon($row['registdate'], $row['newicon'], 1)) { ?>
											<span class="new_icon gre">NEW</span>
											<? } ?>
										</td>
										<?
										if($useAnswer){
										?>
										<td <?=$targetUrl?>>
											<span class="<?=(!$row['answer'] ? "re_ing" : "re_ok")?>">
											<?=(!$row['answer'] || $row['answer'] == "<br/>" ? "대기" : "답변")?>
											</span>

										</td>
										<?
										}
										?>
										<?
										if($useSecret){
										?>
										<td <?=$targetUrl?>><?=getSecretName($row['secret'])?></td>
										<?
										}
										?>
										<?
										if($ismain){
										?>
										<td <?=$targetUrl?>><?=getMain($row['main'])?></td>
										<?
										}
										?>
										<td <?=$targetUrl?>><?=$row[name]?></td>
										<td <?=$targetUrl?> class="<?=!$userCon ? "last" : ""?>">
										
										<?
										if($timeDate){
										?>
										<?= getDateTimeFormat($row['registdate']) ?>
										<?
										}else{
										?>
										<?= getYMD($row['registdate']) ?>
										<?
										}
										?> 
										
										</td>
										<?
										if($userCon){
										?>
										<td class="last" <?=$targetUrl?>><?=$row[readno]?></td>
										<?
										}
										?>
									</tr>
								<?
										}
									 }
								?>
								</tbody>
							</table>
								<input type="hidden" name="cmd" id="cmd" value="groupDelete"/>
								<input type="hidden" name="stype" id="stype" value="<?=$_REQUEST[stype]?>"/>
								<input type="hidden" name="sval" id="sval" value="<?=$_REQUEST[sval]?>"/>
							</div>
							</form>
							<div class="btn">
								<div class="btnLeft">
									<a class="btns gr_btn" href="#" onclick="groupDelete();"><?=getMsg("btn.delete")?></a>
								</div>
								<div class="btnRight">
									<a class="wbtn" href="write.php"><?=getMsg("btn.write")?></a>
								</div>
							</div>
							<!--//btn-->
							<!-- 페이징 처리 -->
							<?=pageList($objCommon->reqPageNo, $rowPageCount[1], $objCommon->getQueryString('index.php', 0, $_REQUEST))?>
							<!-- //페이징 처리 -->
							<form name="searchForm" id="searchForm" action="index.php" method="post">
								<div class="search">
									<? if($branch) { ?>
									<?
										$hospital = new Hospital(999, $_REQUEST);
										$hResult = $hospital->branchSelect();
									?>
									<select name="shospital_fk" id="shospital_fk" title="지점검색" onchange="$('#searchForm').submit();">
										<option value="0"><?=getMsg('th.branch_sel')?></option>
										<? while ($row=mysql_fetch_assoc($hResult)) { ?>
										<option value="<?=$row['no']?>" <?=getSelected($row['no'], $_REQUEST['shospital_fk'])?>><?=$row['name']?></option>
										<? } ?>
									</select>

									<? } ?>
									<? if ($clinic) { ?>
										<?
										include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Clinic.class.php";
										$clinicObj = new Clinic(999, $_REQUEST);
										$clinicList = $clinicObj->selectBoxList(0,($branch ? $_REQUEST['shospital_fk'] : DEFAULT_BRANCH_NO),$_REQUEST['sclinic_fk']);
										?>
									<select name="sclinic_fk" title="<?=getMsg('alt.select.sclinic_fk')?>" onchange="$('#searchForm').submit();">
										<?= $clinicList ?>
									</select>

									<? } ?>
									<?
									if($ismain){
									?>
									<select name="smain" id="smain" title="<?=getMsg('alt.text.main')?>" onchange="$('#searchForm').submit();">
										<option value="" <?=getSelected($_REQUEST[smain], "") ?>><?=getMsg("lable.option.main")?></option>
										<option value="1" <?=getSelected($_REQUEST[smain], "1") ?>><?=getMsg("lable.option.main_y")?></option>
										<option value="2" <?=getSelected($_REQUEST[smain], "2") ?>><?=getMsg("lable.option.main_n")?></option>
									</select>
									<?
									}
									?>
									<select name="stype" title="<?=getMsg('alt.select.stype')?>">
										<option value="all" <?=getSelected($_REQUEST[stype], "all") ?>><?=getMsg("lable.option.all")?></option>
										<option value="name" <?=getSelected($_REQUEST[stype], "name") ?>><?=getMsg("lable.option.name")?></option>
										<option value="email" <?=getSelected($_REQUEST[stype], "email") ?>><?=getMsg("lable.option.email")?></option>
										<option value="contents" <?=getSelected($_REQUEST[stype], "contents") ?>><?=getMsg("lable.option.contents")?></option>
									</select>
									<input type="text" name="sval" value="<?=$_REQUEST[sval]?>" title="<?=getMsg('alt.text.search')?>" />
									<input type="submit" class="se_btn " value="<?=getMsg('btn.search')?>" />
								</div>
							</form>
							<!-- //search --> 
						</div>
						<!-- //blist -->
					</div>
					<!-- //bbs --> 
					<!-- 내용 : e -->
				</div>
				<!--//con -->
			</div>
			<!--//content -->
		</div>
		<!--//container --> 
		<!-- E N D :: containerArea-->
	</div>
	<!--//canvas -->
</div>
<!--//wrap -->

</body>
</html>