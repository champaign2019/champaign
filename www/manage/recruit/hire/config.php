<?
$pageTitle 	 	= "입사지원";				// 게시판 명
$tablename  		= "hire";					// DB 테이블 명
$uploadPath 		= "/upload/hire/";			// 파일, 동영상 첨부 경로
$maxSaveSize	  		= 50*1024*1024;					// 50Mb
$pageRows	  		= 10;							// 보여질 게시글 수
$gradeno		  		= 2;							// 관리자 권한 기준 [지점 사용시]
$islog 			= false; 						//유입(방문자통계 사용여부)
$usePerfect 		= false; 						//조회수
		
/* 화면 필드 생성 여부 */
$unickNum	  	= false;							// 고유번호 사용유무  
$branch	  		= false;							// 지점 사용유무  
$type	  		= false;							// 분류 지점별 사용유무	
$doctor	  		= false;							// 의료진 사용유무	
$useMovie  	 	= false;						// 파일 첨부 [사용: true, 사용 안함 : true]
$useRelationurl	= false;						// 관련URL 첨부 [사용: true, 사용 안함 : true]
$useEmail 		= true;						// 이메일 사용유무
$isComment 		= false;							// 댓글		

$istop 			= false;
$ismain 			= false;
$isnew 			= false;

$timeDate	  	= true;						// 날짜 or 날짜 and 시간	
$userCon	  		= true;							// 조회수 카운트 [관리자 페이지:true, 사용자 페이지:true, 조회수:사용여부]
$registdateGubun = true;							//날짜 수정
$useFile  		= false;							// 파일 첨부 [사용: true, 사용 안함 : true]
$textAreaEditor	= true;							// 에디터 사용여부
$editorIgnore= "";								//에디터 미사용 배열 구분자 ex : '|0|1|'						

/* 문자메세지/이메일메세지 */
$getUserMsg = "상담요청이 정상적으로 요청되었습니다. 감사합니다.";			//사용자 보내는 메세지 - 빈값이면 문자 안나감
$getAdminMsg = "님의 상담이 요청되었습니다. 확인하시고 답변 부탁 드립니다.";	//관리자 보내는 메세지 - 빈값이면 문자 안나감

$getAnswerUserMsg = "";											//사용자 답변 보내는 메세지 - 빈값이면 문자 안나감
$getAnswerAdminMsg = "";											//관리자 답변 보내는 메세지 - 빈값이면 문자 안나감

$getUserEmail = "상담 요청이 정상적으로 요청되었습니다.";					//사용자 보내는 이메일 - 빈값이면 문자 안나감
$getAnswerUserEmail = "";											//관리자 보내는 이메일 - 빈값이면 문자 안나감

$orderby = Array( "top desc","registdate desc" );					    //order by 배열
?>
