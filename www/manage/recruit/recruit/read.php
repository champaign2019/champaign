<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Common.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

$objCommon = new Common($pageRows, $tablename, $_REQUEST);
$_REQUEST['orderby'] = $orderby; //정렬 배열 선언
$data = $objCommon->getData($_REQUEST, $userCon);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
<script type="text/javascript">
	function goDelete() {
		var del = confirm ('삭제하시겠습니까?');
		if (del){
			document.location.href = "process.php?no=<?=$data[no]?>&cmd=delete";
		} else {
			return false;
		}
	}
</script>
</head>
<body> 
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<div id="wrap">
	<!-- canvas -->
	<div id="canvas">
		<!-- S T A R T :: headerArea-->
		<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/top.php" ?>
		<!-- E N D :: headerArea--> 
		
		<!-- S T A R T :: containerArea-->
		<div id="container">
			<div id="content">
				<div class="con_tit">
					<h2><?=$pageTitle?> - [상세]</h2>
				</div>
				<!-- //con_tit -->
				<div class="con">
					<!-- 내용 : s -->
					<div id="bbs">
						<div id="bread">
							
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리 기본내용입니다.">
								<colgroup>
									<col width="15%" />
									<col width="35%" />
									<col width="15%" />
									<col width="35%" />
								</colgroup>
								<tbody>
								<?
									if ($branch) {
								?>
									<tr>
										<th scope="row"><label for="">지점</label></th>
										<td colspan="3"><?=$data['hospital_name']?></td>
									</tr>
								<?
									}
								?>
								<?
									if ($type) {
								?>
									<tr>
										<th scope="row"><label for="">분류</label></th>
										<td colspan="3"><?=$data['clinic_name']?></td>
									</tr>
								<?
									}
								?>
									<tr>
										<th scope="row"><label for="">작성자</label></th>
										<td colspan="<?=$useEmail ? 1 : 3?>"><?=$data['name']?></td>
										<?
											if($useEmail){
										?>
										<th scope="row"><label for="">이메일</label></th>
										<td><a href="javascript:openPop('<?=$objCommon->getQueryStringAddParam('/manage/email/write_pop.php', 0, $_REQUEST, "receiveEmail". $data['email'])?>','메일보내기',2)"><?=$data['email']?></a></td>
										<?
											}
										?>
									</tr>
									<tr>
										<?
											if($userCon){
										?>
										<th scope="row"><label for="">조회수</label></th>
										<td><?=$data['readno']?></td>
										<?
											}
										?>
										<th scope="row"><label for="">등록일</label></th>
										<td colspan="<?=$userCon ? 1 : 3?>" ><?=getDateTimeFormat($data['registdate'])?></td>
									</tr>
									
									<?
										if($istop || $isnew){
									?>
									<tr>
										<?
											if($istop){
										?>
										<th scope="row"><label for="">TOP공지</label></th>
										<td colspan="<?=$isnew ? 1 : 3?>"><?=getTop($data['top'])?></td>
										<?
											}
										?>
										<?
											if($isnew){
										?>
										<th scope="row"><label for="">NEW아이콘</label></th>
										<td colspan="<?=istop ? 1 : 3?>"><?=getNewIcon($data['newicon'])?></td>
										<?
											}
										?>
									</tr>
									<?
										}
									?>
									<?
										if($ismain){
									?>
									<tr>
										<th scope="row"><label for="">메인게시물</label></th>
										<td colspan="3"><?=getMain($data['main'])?></td>
									</tr>
									<?
										}
									?>
								</tbody>
							</table>
							
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리 기본내용입니다.">
								<colgroup>
									<col width="15%" />
									<col width="85%" />
								</colgroup>
								<tbody>
									<tr>
										<th scope="row"><label for="">제목</label></th>
										<td><?=$data['title']?></td>
									</tr>
									<tr>
										<td colspan="2"><?=stripslashes($data[contents])?></td>
									</tr>
									<?
										if ($useFile) {
									?>
										<?
											if ($data['filename_org']) {
												$filename = split(",", $data['filename']);
												$filename_org = split(",", $data['filename_org']);
												$filesize = split(",", $data['filesize']);
										?>
									<tr>
										<td colspan="2">
											<?
											for($i=0;$i<sizeof($filesize);$i++){?>
											<p><img src="/manage/img/file_img.gif" alt="파일첨부" />&nbsp;&nbsp;<a href="/lib/download.php?path=<?=$uploadPath?>&vf=<?=$filename[$i]?>&af=<?=$filename_org[$i]?>" target="_blank"><?=$filename_org[$i]?> [<?=getFileSize($filesize[$i])?>]</a></p>
											<?}?>
										</td>
									</tr>
										<?
											}
										?>
									<?
										}
									?>
									<?
										if ($useRelationurl) {
									?>

											<? if ($data[relation_url]) { ?>

									<tr>
										<th scope="row"><label for="">관련링크</label></th>
										<td>
											<img src="/manage/img/url_img.gif" alt="파일첨부" />&nbsp;&nbsp;<a href="<?=$data['relation_url']?>" target="_blank"><?=cvtHttp($data['relation_url'])?></a>
										</td>
									</tr>
										<?
											}
										?>
									<?
										}
									?>
									<?
										if ($useMovie){
									?>
										
											<? if ($data[moviename]) { ?>
										
									<tr>
										<th scope="row"><label for="">동영상</label></th>
										<td>
											<script type="text/javascript">
											<!--
											tv_adplay_autosize("<?=$uploadPath?><?=$data['moviename']?>", "MoviePlayer");
											//-->
											</script>
										</td>
									</tr>
										<?
											}
										?>
									<?
										}
									?>
									
								</tbody>
							</table>
							<?
								if($islog){
							?>
								<!--웹로그-->
							<?
							}
							?>
							<div class="btn">
								<div class="btnLeft">
									<a class="btns gr_btn" href="<?=$objCommon->getQueryString('index.php', 0, $_REQUEST)?>">목록</a>
								</div>
								<div class="btnRight">
									<a class="btns gr_btn" href="<?=$objCommon->getQueryString('edit.php', $data[no], $_REQUEST)?>">수정</a>
									<a class="btns gr_btn" href="javascript:;" onClick="goDelete();">삭제</a>
								</div>
							</div>
							<!--//btn-->
							<?
								if ($isComment) {
							?>
								<? include $_SERVER['DOCUMENT_ROOT']."/manage/board/comment/comment.php" ?><!-- 댓글 
							<?
								}
							?>	
						</div>
						<!-- //bread -->
					</div>
					<!-- //bbs --> 
					<!-- 내용 : e -->
				</div>
				<!--//con -->
			</div>
			<!--//content -->
		</div>
		<!--//container --> 
		<!-- E N D :: containerArea-->
	</div>
	<!--//canvas -->
</div>
<!--//wrap -->

</body>
</html>