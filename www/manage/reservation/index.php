<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/member/Member.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/reservation/Reservation.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Hospital.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Clinic.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Doctor.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

	$reser = new Reservation($pageRows, $tablename, $_REQUEST);


	
	$today 	  = getToday();													// 오늘날짜
	$pDay = getDayDateAdd(-1, $_REQUEST['startday']);	// 어제
	$nDay = getDayDateAdd(1, $_REQUEST['endday']);		// 내일
	$oneWeek 	  = getDayDateAdd(-7, $today);										// 일주일
	$pWeek = getDayDateAdd(-6, $pDay);			// 전주
	$tWeeks = getThisWeek($today);				// 금주
	$nWeek = getDayDateAdd(6, $nDay);				// 차주
	$oneMonth	  = getMonthDateAdd(-1, $today);									// 한달
	$fDay   = preg_replace('/([0-9]{4}-[0-9]{2})-[0-9]{2}/im', '$1-01', getDayDateAdd(1, $pDay));// 해당월 초일
	$pMonth = getMonthDateAdd(-1, $fDay);													// 전월 첫날
	$tMonth = preg_replace("/([0-9]{4}-[0-9]{2})-[0-9]{2}/im", "$1-01",getToday());			// 해당월 초일
	$nMonth = getMonthDateAdd(1, $fDay);													// 익월 첫날
	$pLastDay = lastDay($pMonth);		// 전월 마지막 날
	$tLastDay = lastDay($tMonth);		// 금월 마지막 날
	$nLastDay = lastDay($nMonth);		// 익월 마지막 날
	$threeMonth = getMonthDateAdd(-3, $today);									// 세달
	
	if ($branch && !$_REQUEST['shospital_fk']) {
	$_REQUEST['shospital_fk'] = $_SESSION['admin_hospital_fk'];
	}
	
	$rowPageCount = $reser->getCount($_REQUEST);
	$result = $reser->getList($_REQUEST);

	$hospital = new Hospital(999, $_REQUEST);
	$clinicObj = new Clinic(999, $_REQUEST);
	$doctorObj = new Doctor(999, $_REQUEST);
	$member = new Member(999, "member", $_REQUEST);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
<script>
function groupDelete() {	
	if ( $('[name^=no]:checked').length > 0 ) {
		document.frm.submit();
	} else {
		alert("<?=getMsg('alert.text.delete')?>");
	}
}
//달력부분
$(window).load(function() {
	//initCal({id:"startday",type:"day",today:"y"});			
	//initCal({id:"endday",type:"day",today:"y"});
	$('#startday').datetimepicker({timepicker:false,format: "Y-m-d"});
		$("#start_button").click(function(){
			$('#startday').datetimepicker('show');
		})	
	$('#endday').datetimepicker({timepicker:false,format: "Y-m-d"});
		$("#end_button").click(function(){
			$('#endday').datetimepicker('show');
		})	
	
	$("#sval").keydown(function(e){
		$("#searchForm")[0].action = "index.php";
		entKeyEventListener(e.keyCode, "searchForm");
	});
});


// 목록 'search' form 정렬 
function goOrderBy(by, type) {
	var f = document.searchForm;
	f.ordertype.value = type;
	f.orderby.value = by;
	f.submit();
}

$(document).ready(function(){
	$("#excel").click(function(){
		$("#searchForm")[0].action = "excel.php";
		$("#searchForm").submit();
	});
});

function goSearch() {
	$("#searchForm")[0].action = "index.php";
	$("#searchForm").submit();
}

function goExcel() {
	$("#searchForm")[0].action = "excel.php";
	$("#searchForm").submit();
}

function searchDate(startDay, endDay) {
	$("#searchForm [name=startday]").val(startDay);
	$("#searchForm [name=endday]").val(endDay);
	$("#searchForm").submit();
}

</script>
</head>
<body> 
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<div id="wrap">
	<!-- canvas -->
	<div id="canvas">
		<!-- S T A R T :: headerArea-->
		<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/top.php" ?>
		<!-- E N D :: headerArea--> 
		
		<!-- S T A R T :: containerArea-->
		<div id="container">
			<div id="content">
				<div class="con_tit">
					<h2><?=$pageTitle?> - [<?=getMsg("th.list")?>]</h2>
				</div>
				<!-- //con_tit -->
				<div class="con">
					<!-- 내용 : s -->
					<div id="bbs">
						<div id="blist">
							<div class="top_search table_wrap">
								<form name="searchForm" id="searchForm" action="index.php" method="post">
								<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리 목록입니다.">
									<colgroup>
										<col width="5%" />
										<col width="" />
										<col width="5%" />
										<col width="" />
										<col width="5%" />
										<col width="" />
										<col width="5%" />
										<col width="" />
									</colgroup>
									<tbody>
										<tr>
											<?
											$colspan = ($branch ? 0 : 3);
											if($branch){
												$hospital = new Hospital(999, $_REQUEST);
												$hResult = $hospital->branchSelect();
											?>
											<th scope="row"><label for=""><?=getMsg('th.hospital_fk')?></label></th>
											<td colspan="2">
												<select name="shospital_fk" id="shospital_fk" class="w100m2" title="지점을 선택해주세요" onchange="$('#searchForm').submit();">
													<option value=""><?=getMsg('th.all')?></option>
													<? while ($row=mysql_fetch_assoc($hResult)) { ?>
													<option value="<?=$row['no']?>" <?=getSelected($row['no'],$_REQUEST['shospital_fk'])?>><?=$row['name']?></option>
													<? } ?>
												</select>
											</td>
											<?
											}
											?>
											<?
											$colspan += ($clinic ? 0 : 2);
											if($clinic){
												$clinicObj = new Clinic(999, $_REQUEST);
												$clinicList = $clinicObj->selectBoxList(3,($branch ? $_REQUEST['shospital_fk'] : DEFAULT_BRANCH_NO),$_REQUEST['sclinic_fk']);
											?>
											<th scope="row"><label for=""><?=getMsg('th.clinic_fk')?></label></th>
											<td colspan="2">
												<select name="sclinic_fk" class="w100m2" id="sclinic_fk" onchange="$('#searchForm').submit();">
													<?=$clinicList?>
												</select>
											</td>
											<?
											}
											?>
											<?
											$colspan += ($doctor ? 0 : 2);
											if($doctor){
											?>
											<th scope="row"><label for=""><?=getMsg('th.doctor_fk')?></label></th>
											<td >
												<select name="sdoctor_fk" class="w100m2" id="sdoctor_fk" onchange="$('#searchForm').submit();">
													<?=$doctorObj->selectBoxList(3,0,($branch ? $_REQUEST['shospital_fk'] : DEFAULT_BRANCH_NO),0,$_REQUEST['sdoctor_fk'])?>
												</select>
											</td>
											<?
											}
											?>
											<th scope="row"><label for=""><?=getMsg('th.newold')?></label></th>
											<td colspan="<?=$colspan?>">
												<select id="snewold" class="w100m2" name="snewold" title="초/재진을 선택하세요" onchange="$('#searchForm').submit();">
													<option value="0" <?=getSelected($_REQUEST['snewold'], 0)?>><?=getMsg('th.all')?></option>
													<?=getIncipientMeetType($_REQUEST['snewold'])?>
											</select>
											</td>
										</tr>
										<tr style="height:35px">
											<th scope="row"><label for="">예약상태</label></th>
											<td>
												<select id="sstate" class="w100m2" name="sstate" title="예약상태를 선택하세요" onchange="$('#searchForm').submit();">
													<?=getReserStateNameType($_REQUEST['sstate'])?>
												</select>
											</td>
											<th scope="row">
											<label for="">
												<select name="datetype" id="datetype" title="날짜검색를 선택해주세요" onchange="$('#searchForm').submit();">
													<option value="all"><?=getMsg('th.all')?></option>
													<option value="registdate" <?=getSelected($_REQUEST['datetype'], "registdate") ?>><?=getMsg('th.registdate')?></option>
													<option value="reserdate" <?=getSelected($_REQUEST['datetype'], "reserdate") ?>>예약일</option>
												</select>
											</label>
											</th>
											<td class="date_wrap" colspan="3">
												<input type="text" class="w10m100" id="startday" name="startday" value="<?=$_REQUEST['startday']?>" onkeyup="isOnlyNumberNotHypen(this);" onblur="cvtUserDateNumber(this)" onfocus="cvtUserHipenReset(this)"/>
												<span id="CalstartdayIcon">
													<button type="button" id="start_button"><img src="/manage/img/calendar_icon.png" id="CalregistdateIconImg" style="cursor:pointer;"/></button>
												</span>
												<input type="text" id="endday" class="w10m100" name="endday" value="<?=$_REQUEST['endday']?>" onkeyup="isOnlyNumberNotHypen(this);" onblur="cvtUserDateNumber(this)" onfocus="cvtUserHipenReset(this)"/>
												<span id="CalenddayIcon">
													<button type="button" id="end_button"><img src="/manage/img/calendar_icon.png" id="CalregistdateIconImg" style="cursor:pointer;"/></button>
												</span>
												<span class="inb">
<!-- 													<br/> -->
<!-- 													<br/> -->
													<a class="black_btn" href="#" onclick="searchDate('<?=$pDay?>','<?=$pDay?>');"><strong>전일</strong></a>
													<a class="black_btn" href="#" onclick="searchDate('<?=$today?>','<?=$today?>');"><strong>오늘</strong></a>
													<a class="black_btn" href="#" onclick="searchDate('<?=$nDay?>','<?=$nDay?>');"><strong>후일</strong></a>
													<a class="black_btn" href="#" onclick="searchDate('<?=$oneWeek?>','<?=$today?>');"><strong>1주</strong></a>
													<a class="black_btn" href="#" onclick="searchDate('<?=$pWeek?>','<?=$pDay?>');"><strong>전주</strong></a>
													<a class="black_btn" href="#" onclick="searchDate('<?=$tWeeks[0]?>','<?=$tWeeks[1]?>');"><strong>금주</strong></a>
													<a class="black_btn" href="#" onclick="searchDate('<?=$nDay?>','<?=$nWeek?>');"><strong>차주</strong></a>
													<a class="black_btn" href="#" onclick="searchDate('<?=$oneMonth?>','<?=$today?>');"><strong>1개월</strong></a>
													<a class="black_btn" href="#" onclick="searchDate('<?=$pMonth?>','<?=$pLastDay?>');"><strong>전월</strong></a>
													<a class="black_btn" href="#" onclick="searchDate('<?=$tMonth?>','<?=$tLastDay?>');"><strong>금월</strong></a>
													<a class="black_btn" href="#" onclick="searchDate('<?=$nMonth?>','<?=$nLastDay?>');"><strong>익월</strong></a>
												</span>
											</td>
											<th><label for="">검색어검색</label></th>
											<td>
												<select id="stype" name="stype" title="검색을 선택해주세요">
													<option value="all" <?= getSelected($_REQUEST['stype'], "all") ?>><?=getMsg('th.all')?></option>
													<option value="name" <?= getSelected($_REQUEST['stype'], "name") ?>>예약자명</option>
													<option value="offchgname" <?= getSelected($_REQUEST['stype'], "offchgname") ?>>변경자명</option>
													<option value="cell" <?= getSelected($_REQUEST['stype'], "cell") ?>><?=getMsg('th.cell')?></option>
													<option value="email" <?= getSelected($_REQUEST['stype'], "email") ?>><?=getMsg('th.email')?></option>
													<option value="etc" <?= getSelected($_REQUEST['stype'], "etc") ?>>비고</option>
												</select>
												<input type="text" id="sval" name="sval" value="<?= $_REQUEST['sval'] ?>"  class="input60p upserch06"/>
												
												<a class="blue_btn" href="javascript:;" onclick="goSearch();">검색</a>
												<a class="black_btn size02" href="javascript:;" onclick="goExcel();"><img src="/manage/img/down_icon.png" class="downl_icon" /><?=getMsg('btn.exceldown')?></a>
											</td>
										</tr>
									</tbody>
								</table>
								<input type="hidden" name="ordertype" id="ordertype" value="<?=$_REQUEST['ordertype']?>"/>
								<input type="hidden" name="orderby" id="orderby" value="<?=$_REQUEST['orderby']?>"/>
								</form>
							</div>
							<!--//top_search-->
							<p><span><strong><?=getMsg('th.total')?> <?=$rowPageCount[0]?><?=getMsg('th.amount')?></strong>  |  <?=$reser->reqPageNo?>/<?=$rowPageCount[1]?><?=getMsg('th.page')?></span></p>
							<div class="table_wrap">
							<form name="frm" id="frm" action="process.php" method="post">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리목록입니다.">
								<colgroup>
									<col class="w2" /> <!-- checkbox -->
									<col class="w2" /> <!-- 번호 -->
									<? if ($branch) { ?>
									<col class="w3" /> <!-- 지점 -->
									<? } ?>
									<? if ($doctor) { ?>
									<col class="w5" /> <!-- 의료진 -->
									<? } ?>
									<? if ($clinic) { ?>
									<col class="w5" /> <!-- 진료과목 -->
									<? } ?>
									<col class="w5" /> <!-- 진료구분 -->
									<col class="w5" /> <!-- 초/재진구분 -->
									<col class="w5" /> <!-- 온오프 구분 -->
									<col class="w4" /> <!-- 예약자 -->
									<col class="w8" /> <!-- 핸드폰 -->
									<col class="w8" /> <!-- 예약일시 -->
									<col class="w6" /> <!-- 신청자 -->
									<col class="w8" /> <!-- 등록일 -->
									<col class="w6" /> <!-- 변경자 -->
									<col class="w8" /> <!-- 변경일 -->
									<col class="w8" /> <!-- 취소일 -->
									<col class="w4" /> <!-- 예약상태 -->
									<col class="w4" /> <!-- 예약확인 -->
									<col class="w4" /> <!-- 예약취소 -->
								</colgroup>
								<thead>
									<tr>
										<th scope="col" class="first">
											<label class="b_first_c"><input type="checkbox" name="allChk" id="allChk" onClick="check(this, document.getElementsByName('no[]'))"/>
												<i></i>
											</label></th>
										<th scope="col"><?=getMsg("th.no")?></th>
										<? if ($branch) { ?>
										<th scope="col"><?=getMsg("th.hospital_fk")?></th>
										<? } ?>
										<? if ($doctor) { ?> 
										<th scope="col"><?=getMsg('th.doctor_fk')?></th>
										<? } ?>
										<? if ($clinic) { ?> 
										<th scope="col"><?=getMsg("th.clinic_fk")?></th>
										<? } ?> 
										<th scope="col">진료구분</th> 
										<th scope="col">초/재진구분</th> 
										<th scope="col">온오프구분</th> 
										<th scope="col">예약자</th> 
										<th scope="col">핸드폰</th> 
										<th scope="col">예약일시</th> 
										<th scope="col">신청자</th> 
										<th scope="col"><?=getMsg('th.registdate')?></th> 
										<th scope="col">변경자</th> 
										<th scope="col">변경일</th> 
										<th scope="col">취소일</th> 
										<th scope="col">예약상태</th> 
										<th scope="col">예약확인</th> 
										<th scope="col" class="last">예약취소</th>
									</tr>
								</thead>
								<tbody>
								<? if ($rowPageCount[0] == 0) { ?>
									<tr>
										<td class="first" colspan="19">등록된 예약이 없습니다.</td>
									</tr>
								<?
									 } else {
										$targetUrl = "";
										$smsPop = "";
										
										$i = 0;
										while ($row=mysql_fetch_assoc($result)) {
											$targetUrl = "style='cursor:pointer;' onclick=\"location.href='".$reser->getQueryString('read.php', $row['no'], $_REQUEST)."'\"";
											$smsPop = "style='cursor:pointer;' onclick=\"javascript:openPop('".$member->getQueryStringAddParam('/manage/sms/write_pop.php', $row['no'], $_REQUEST, "receiver", $row['cell'])."','문자보내기',1)\"";
								?>
									<tr <?=getReserClass($row['state'], "reser")?>>
										<td class="first"><label class="b_nor_c"><input type="checkbox" name="no[]" id="no" value="<?=$row['no']?>"/><i></i></label></td>
										<td <?=$targetUrl?>><?=$row['no']?></td>
										<? if ($branch) { ?>
										<td <?=$targetUrl?>><?=$row['hospital_name']?></td>
										<? } ?>
										<? if ($doctor) { ?>
										<td <?=$targetUrl?>><?=$row['doctor_name']?></td>
										<? } ?>
										<? if ($clinic) { ?>
										<td <?=$targetUrl?>><?=$row['clinic_name']?></td>
										<? } ?>
										<td <?=$targetUrl?>><?=getReserTypeName($row['resertype'])?></td>
										<td <?=$targetUrl?>><?=getIncipientMeetName($row['newold'])?></td>
										<td <?=$targetUrl?>><?=getOnoffTypeName($row['onoff'])?></td>
										<td <?=$targetUrl?>><?=$row['name']?></td>
										<td <?=$smsPop?>><?=$row['cell']?></td>
										<td <?=$targetUrl?>><?=$row['reserdate']?> <?=$row['resertime']?></td>
										<td <?=$targetUrl?>><?=$row['offname']?></td>
										<td <?=$targetUrl?>><?=$row['registdate']?></td>
										<td <?=$targetUrl?>><?=$row['offchgname']?></td>
										<td <?=$targetUrl?>>
											<? if ($row['editdate']) { ?>
												<?=$row['editdate']?>
											<? } ?>
										</td>
										<td <?=$targetUrl?>>
											<? if ($row['canceldate']) { ?>
												<?=$row['canceldate']?>
											<? } ?>
										</td>
										<td <?=$targetUrl?>><?=getReserState($row['state'])?></td>
										<td>
											<? if ($row['state'] != 3) { ?>
											<div class="btnList"> <a class="blue_btn" style="cursor: pointer;" onclick="window.open('reserConfirm.php?no=<?=$row['no']?>&hospital_fk=<?=$row['hospital_fk']?>&name=<?=$row['name']?>&pop=1', 'reserConfirm' , 'scrollbars=0, resizable=0, top=300, left=700, width=500, height=300');"> 확인</a> </div>
											<? } ?>
										</td>
										<td class="last">
											<? if ($row['state'] != 4) { ?>
											<div class="btnList"> <a class="red_btn" style="cursor: pointer;" onclick="window.open('reserCancel.php?no=<?=$row['no']?>&hospital_fk=<?=$row['hospital_fk']?>&name=<?=$row['name']?>&pop=1', 'reserCancel' , 'scrollbars=0, resizable=0, top=300, left=800, width=500, height=300');"> 취소 </a> </div>
											<? } ?>
										</td>
									</tr>
								<?
										}
									 }
								?>
								</tbody>
							</table>
								<input type="hidden" name="cmd" id="cmd" value="groupDelete"/>
								<input type="hidden" name="stype" id="stype" value="<?=$_REQUEST['stype']?>"/>
								<input type="hidden" name="sval" id="sval" value="<?=$_REQUEST['sval']?>"/>
							</form>
							</div>
							<div class="btn">
								<div class="btnLeft">
									<a class="btns" href="#" onclick="groupDelete();"><strong><?=getMsg('btn.delete')?></strong> </a>
								</div>
								<div class="btnRight">
									<a class="wbtn" href="write.php"><strong>예약하기</strong> </a>
								</div>
							</div>
							<!--//btn-->
							<!-- 페이징 처리 -->
							<?=pageList($reser->reqPageNo, $rowPageCount[1], $reser->getQueryString('index.php', 0 ,$_REQUEST))?>
							<!-- //페이징 처리 -->
						</div>
						<!-- //blist -->
					</div>
					<!-- //bbs --> 
					<!-- 내용 : e -->
				</div>
				<!--//con -->
			</div>
			<!--//content -->
		</div>
		<!--//container --> 
		<!-- E N D :: containerArea-->
	</div>
	<!--//canvas -->
</div>
<!--//wrap -->

</body>
</html>