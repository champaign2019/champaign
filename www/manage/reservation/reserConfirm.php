<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/member/Member.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/log/Log.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/reservation/Reservation.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Hospital.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Clinic.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Doctor.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

$reser = new Reservation($pageRows, $tablename, $_REQUEST);
$data = $reser->getData($_REQUEST['no']);

$member = new Member(999, 'member', $_REQUEST);
$hospital = new Hospital(999, $_REQUEST);
$hdata = $hospital->getData($_REQUEST['hospital_fk']);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">

<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body>
<form name="board" action="<?=getSslCheckUrl($_SERVER['REQUEST_URI'], 'process.php')?>" method="post">
	<table width=350 cellspacing=0 cellpadding=0 border=0 style="margin:0 auto;" class="reserv_table">
	<tr>
		<td height=20></td>
	</tr>
	<tr>
		<td align=center>
			<table width=340 cellspacing=0 cellpadding=0 border=0>
				<tr>
					<td width=100% bgcolor=dddddd height=2></td>
				</tr>
				<tr>
					<td align=center height=50 class="tit">예약 확인 하시겠습니까?</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align=center>
		<table width=340 cellspacing=0 cellpadding=0 border=0 class="check_t">
		<tr>
			<td align="center">
				<label class="b_first_c marR10"><input type="checkbox" name="email_send" id="email_send" value="1" checked="checked" /><i></i> 이메일발송</label>
				<label class="b_first_c"><input type="checkbox" name="sms_send" id="sms_send" value="1" <?=getChecked("1", $hdata['reser_end_user'])?> /><i></i> SMS발송</label>
			</td>
		</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td align=center>
		<table width=340 cellspacing=0 cellpadding=0 border=0>
		<tr>
			<td>
				<div class="btnAll">
					<a class="blue_btn" href="#" onClick="document.board.submit();"><strong>예약확인</strong></a>
					<a class="red_btn" href="#" onClick="window.close();"><strong>예약취소</strong></a>
				</div>
			</td>
		</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td height=20></td>
	</tr>
	</table>
	<input type="hidden" name="name" value="<?=$_REQUEST['name']?>"/>
	<input type="hidden" name="no" value="<?=$_REQUEST['no']?>"/>
	<input type="hidden" name="hospital_fk" value="<?=$_REQUEST['hospital_fk']?>"/>
	<input type="hidden" name="lastchange" value="예약확인"/>
	<input type="hidden" name="cmd" value="confirm"/>
	<input type="hidden" name="pop" value="<?=$_REQUEST['pop']?>" />
</form>
</body>
</html>