<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/member/Member.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/log/Log.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/reservation/Reservation.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Hospital.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Clinic.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Doctor.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/email/SendMail.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/sms/Sms.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

$reser = new Reservation($pageRows, $tablename, $_REQUEST);

$member = new Member(999, 'member', $_REQUEST);
$hospital = new Hospital(999, $_REQUEST);
$clinic = new Clinic(999, $_REQUEST);
$doctor = new Doctor(999, $_REQUEST);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php"?>
</head>
<body>
<?
if (checkReferer($_SERVER["HTTP_REFERER"])) {

	if ($_REQUEST['cmd'] == "write") {	// 저장시
		$r = $reser->insert($_REQUEST);
		if ($r > 0) {
			$data = $reser->getData($r);
			$hdata = $hospital->getData($_REQUEST['hospital_fk']);
			
			if ($_REQUEST['email_send'] == 1) {
				if ($_REQUEST['email']) {
					// 이메일 보내기
					$title = "[".COMPANY_NAME."]".$data['name']."님의 예약이 완료 되었습니다.";	// 메일 제목
					
					$contents = "
							<table border='0' cellpadding='0' cellspacing='0' width='100%' style='font-family:굴림; font-size: 12px; color: #4b4b4b; border:#CCCCCC 1px solid;'> ";
					if ($branch) {
						$content .= "
							<tr height='30'>									
								<td width=90 style='font-weight:bold; padding-left:5px; border-right:#CCCCCC 1px solid; border-bottom:#CCCCCC 1px solid; background-color: #f4f8fd;'>".getMsg('th.hospital_fk')."</td>
								<td style='padding-left:5px; border-bottom:#CCCCCC 1px solid;'>".$data['hospital_name']."</td>
							</tr>	";
					}
					if ($clinic) {
						$content .= "
							<tr height='30'>									
								<td width=90 style='font-weight:bold; padding-left:5px; border-right:#CCCCCC 1px solid; border-bottom:#CCCCCC 1px solid; background-color: #f4f8fd;'>".getMsg('th.clinic_fk')."</td>
								<td style='padding-left:5px; border-bottom:#CCCCCC 1px solid;'>".$data['clinic_name']."</td>
							</tr>	";
					}
					$content .= "
							<tr height='30'>
								<td style='font-weight:bold; padding-left:5px; border-right:#CCCCCC 1px solid; border-bottom:#CCCCCC 1px solid; background-color: #f4f8fd;'>예약일시</td>
								<td style='padding-left:5px; border-bottom:#CCCCCC 1px solid;'>".$data['reserdate']." ".$data['resertime']."</td>
							</tr>
							<tr height='30'>
								<td style='font-weight:bold; padding-left:5px; border-right:#CCCCCC 1px solid; border-bottom:#CCCCCC 1px solid; background-color: #f4f8fd;'>성명</td>
								<td style='padding-left:5px; border-bottom:#CCCCCC 1px solid;'>".$data['name']."</td>
							</tr>
							<tr height='30'>
								<td style='font-weight:bold; padding-left:5px; border-right:#CCCCCC 1px solid; border-bottom:#CCCCCC 1px solid; background-color: #f4f8fd;'><?=getMsg('th.cell')?></td>
								<td style='padding-left:5px; border-bottom:#CCCCCC 1px solid;'>".$data['cell']."</td>
							</tr>
							<tr height='30'>
								<td style='font-weight:bold; padding-left:5px; border-right:#CCCCCC 1px solid; border-bottom:#CCCCCC 1px solid; background-color: #f4f8fd;'>".getMsg('th.email')."</td>
								<td style='padding-left:5px; border-bottom:#CCCCCC 1px solid;'>".$data['email']."</td>
							</tr>
							</table>";

					$mailForm = getURLMakeMailForm(COMPANY_URL, EMAIL_FORM);
					$mailForm = str_replace(":SUBJECT", $title, $mailForm);
					$mailForm = str_replace(":CONTENT", $contents, $mailForm);

					$sendmail = new SendMail();
					$sendmail->send(COMPANY_EMAIL, COMPANY_NAME, $_REQUEST['email'], $title, $mailForm);

				}
			}

			// SMS발송
			if ($_REQUEST['sms_send'] == 1) {
				$sms = new Sms(999, $_REQUEST);
				$_REQUEST['tran_id'] = SMS_KEYNO;
				$_REQUEST['tran_etc1'] = SMS_CNAME;
				$_REQUEST['tran_etc2'] = SMS_USERID;
				$_REQUEST['tran_etc3'] = SMS_USERNAME;
				$_REQUEST['sendtype'] = 'cell';
				$_REQUEST['tran_status'] = '1';
				$_REQUEST['tran_date'] = getFullToday();

				if ($_REQUEST[cell] && $hdata['reser_write_user'] == "1") {
					// 받을 사람(사용자)
					// 핸드폰번호가 있고 상담글 등록시 사용자에게 SMS발송
					$msg = str_replace("#name#", $_REQUEST[name], $hdata['reser_sms_msg']);
					$_REQUEST['tran_callback'] = $hdata['reser_admin_tel'];
					$_REQUEST['receiver'] = $data['cell'];
					$_REQUEST['tran_msg'] = $msg;
					$sms->sendSMS($_REQUEST);
				}
			}
			
			if ($_REQUEST['pop'] == "1") {
				echo popupCloseRefresh("정상적으로 저장되었습니다.");
			} else {
				echo returnURLMsg($reser->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.insert'));
			}
		} else {
			if ($_REQUEST['pop'] == "1") {
				echo popupCloseRefresh("요청처리중 장애가 발생하였습니다.");
			} else {
				echo returnURLMsg($reser->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.error'));
			}
		}
	} else if ($_REQUEST['cmd'] == "edit") {
		$r = $reser->update($_REQUEST, $userCon);
		$returnUrl = $_REQUEST['pop'] == "1" ? "read_mini.php" : "read.php";
		if ($r > 0) {
			echo returnURLMsg($reser->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], $returnUrl), $_REQUEST['no'], $_REQUEST), getMsg('result.text.update'));
		} else {
			if ($r == -2) {
				echo returnURLMsg($reser->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], $returnUrl), $_REQUEST['no'], $_REQUEST), '해당 예약날짜와 일시에 동일한 예약이 있습니다.');
			} else {
				echo returnURLMsg($reser->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], $returnUrl), $_REQUEST['no'], $_REQUEST), getMsg('result.text.error'));
			}
		}
	} else if ($_REQUEST['cmd'] == "confirm") {
		$r = $reser->confirmReservation($_REQUEST);
		if ($r > 0) {
			// 이메일 및 sms 발송 준비
			$data = $reser->getData($_REQUEST['no']);
			$hdata = $hospital->getData($_REQUEST['hospital_fk']);
		
			if ($_REQUEST['email_send'] == 1) {
				if ($_REQUEST['email']) {
				// 이메일 보내기
				$title = "[".COMPANY_NAME."]".$data['name']."님의 예약이 확인 완료 되었습니다.";	// 메일 제목
				
				$contents = "
						<table border='0' cellpadding='0' cellspacing='0' width='100%' style='font-family:굴림; font-size: 12px; color: #4b4b4b; border:#CCCCCC 1px solid;'> ";
				if ($branch) {
					$content .= "
						<tr height='30'>									
							<td width=90 style='font-weight:bold; padding-left:5px; border-right:#CCCCCC 1px solid; border-bottom:#CCCCCC 1px solid; background-color: #f4f8fd;'>".getMsg('th.hospital_fk')."</td>
							<td style='padding-left:5px; border-bottom:#CCCCCC 1px solid;'>".$data['hospital_name']."</td>
						</tr>	";
				}
				if ($clinic) {
					$content .= "
						<tr height='30'>									
							<td width=90 style='font-weight:bold; padding-left:5px; border-right:#CCCCCC 1px solid; border-bottom:#CCCCCC 1px solid; background-color: #f4f8fd;'>".getMsg('th.clinic_fk')."</td>
							<td style='padding-left:5px; border-bottom:#CCCCCC 1px solid;'>".$data['clinic_name']."</td>
						</tr>	";
				}
				$content .= "
						<tr height='30'>
							<td style='font-weight:bold; padding-left:5px; border-right:#CCCCCC 1px solid; border-bottom:#CCCCCC 1px solid; background-color: #f4f8fd;'>예약일시</td>
							<td style='padding-left:5px; border-bottom:#CCCCCC 1px solid;'>".$data['reserdate']." ".$data['resertime']."</td>
						</tr>
						<tr height='30'>
							<td style='font-weight:bold; padding-left:5px; border-right:#CCCCCC 1px solid; border-bottom:#CCCCCC 1px solid; background-color: #f4f8fd;'>성명</td>
							<td style='padding-left:5px; border-bottom:#CCCCCC 1px solid;'>".$data['name']."</td>
						</tr>
						<tr height='30'>
							<td style='font-weight:bold; padding-left:5px; border-right:#CCCCCC 1px solid; border-bottom:#CCCCCC 1px solid; background-color: #f4f8fd;'><?=getMsg('th.cell')?></td>
							<td style='padding-left:5px; border-bottom:#CCCCCC 1px solid;'>".$data['cell']."</td>
						</tr>
						<tr height='30'>
							<td style='font-weight:bold; padding-left:5px; border-right:#CCCCCC 1px solid; border-bottom:#CCCCCC 1px solid; background-color: #f4f8fd;'>".getMsg('th.email')."</td>
							<td style='padding-left:5px; border-bottom:#CCCCCC 1px solid;'>".$data['email']."</td>
						</tr>
						</table>";

					$mailForm = getURLMakeMailForm(COMPANY_URL, EMAIL_FORM);
					$mailForm = str_replace(":SUBJECT", $title, $mailForm);
					$mailForm = str_replace(":CONTENT", $contents, $mailForm);

					$sendmail = new SendMail();
					$sendmail->send(COMPANY_EMAIL, COMPANY_NAME, $_REQUEST['email'], $title, $mailForm);
				}
			}

			// SMS발송
			if ($_REQUEST['sms_send'] == 1) {
				$sms = new Sms(999, $_REQUEST);
				$_REQUEST['tran_id'] = SMS_KEYNO;
				$_REQUEST['tran_etc1'] = SMS_CNAME;
				$_REQUEST['tran_etc2'] = SMS_USERID;
				$_REQUEST['tran_etc3'] = SMS_USERNAME;
				$_REQUEST['sendtype'] = 'cell';
				$_REQUEST['tran_status'] = '1';
				$_REQUEST['tran_date'] = getFullToday();

				if (getTelWithHypen( $data[cell] ) && $hdata['reser_end_user'] == "1") {
					// 받을 사람(사용자)
					// 핸드폰번호가 있고 상담글 등록시 사용자에게 SMS발송
					$msg = str_replace("#name#", $_REQUEST[name], $hdata['reser_sms_confirmmsg']);
					$_REQUEST['tran_callback'] = $hdata['reser_admin_tel'];
					$_REQUEST['receiver'] = $data['cell'];
					$_REQUEST['tran_msg'] = $msg;
					$sms->sendSMS($_REQUEST);
				}
			}
			
			if ($_REQUEST['pop'] == "1") {
				echo popupCloseRefresh("정상적으로 완료처리 되었습니다.");
			} else {
				echo returnURLMsg($reser->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), '정상적으로 완료처리 되었습니다.');
			}
		} else {
			if ($_REQUEST['pop'] == "1") {
				echo popupCloseRefresh("요청처리중 장애가 발생하였습니다.");
			} else {
				echo returnURLMsg($reser->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.error'));
			}
		}
	} else if ($_REQUEST['cmd'] == "cancel") {
		$r = $reser->cancelReservation($_REQUEST, $userCon, $_SESSION['admin_id']);
		if ($r > 0) {
			// 이메일 및 sms 발송 준비
			$sms = new Sms(999, $_REQUEST);
			$data = $reser->getData($_REQUEST['no']);
			$hdata = $hospital->getData($_REQUEST['hospital_fk']);
			
			if($data['sms_no'] != 0){
				$sms->deleteNo($data['sms_no']);
			}

			if ($_REQUEST['email_send'] == 1) {
				if ($_REQUEST['email']) {
				// 이메일 보내기
				$title = "[".COMPANY_NAME."]".$data['name']."님의 예약이 취소 처리 되었습니다.";	// 메일 제목
				
				$contents = "
						<table border='0' cellpadding='0' cellspacing='0' width='100%' style='font-family:굴림; font-size: 12px; color: #4b4b4b; border:#CCCCCC 1px solid;'> ";
				if ($branch) {
					$content .= "
						<tr height='30'>									
							<td width=90 style='font-weight:bold; padding-left:5px; border-right:#CCCCCC 1px solid; border-bottom:#CCCCCC 1px solid; background-color: #f4f8fd;'>".getMsg('th.hospital_fk')."</td>
							<td style='padding-left:5px; border-bottom:#CCCCCC 1px solid;'>".$data['hospital_name']."</td>
						</tr>	";
				}
				if ($clinic) {
					$content .= "
						<tr height='30'>									
							<td width=90 style='font-weight:bold; padding-left:5px; border-right:#CCCCCC 1px solid; border-bottom:#CCCCCC 1px solid; background-color: #f4f8fd;'>".getMsg('th.clinic_fk')."</td>
							<td style='padding-left:5px; border-bottom:#CCCCCC 1px solid;'>".$data['clinic_name']."</td>
						</tr>	";
				}
				$content .= "
						<tr height='30'>
							<td style='font-weight:bold; padding-left:5px; border-right:#CCCCCC 1px solid; border-bottom:#CCCCCC 1px solid; background-color: #f4f8fd;'>예약일시</td>
							<td style='padding-left:5px; border-bottom:#CCCCCC 1px solid;'>".$data['reserdate']." ".$data['resertime']."</td>
						</tr>
						<tr height='30'>
							<td style='font-weight:bold; padding-left:5px; border-right:#CCCCCC 1px solid; border-bottom:#CCCCCC 1px solid; background-color: #f4f8fd;'>성명</td>
							<td style='padding-left:5px; border-bottom:#CCCCCC 1px solid;'>".$data['name']."</td>
						</tr>
						<tr height='30'>
							<td style='font-weight:bold; padding-left:5px; border-right:#CCCCCC 1px solid; border-bottom:#CCCCCC 1px solid; background-color: #f4f8fd;'><?=getMsg('th.cell')?></td>
							<td style='padding-left:5px; border-bottom:#CCCCCC 1px solid;'>".$data['cell']."</td>
						</tr>
						<tr height='30'>
							<td style='font-weight:bold; padding-left:5px; border-right:#CCCCCC 1px solid; border-bottom:#CCCCCC 1px solid; background-color: #f4f8fd;'>".getMsg('th.email')."</td>
							<td style='padding-left:5px; border-bottom:#CCCCCC 1px solid;'>".$data['email']."</td>
						</tr>
						</table>";

					$mailForm = getURLMakeMailForm(COMPANY_URL, EMAIL_FORM);
					$mailForm = str_replace(":SUBJECT", $title, $mailForm);
					$mailForm = str_replace(":CONTENT", $contents, $mailForm);

					$sendmail = new SendMail();
					$sendmail->send(COMPANY_EMAIL, COMPANY_NAME, $_REQUEST['email'], $title, $mailForm);

				}
			}

			// SMS발송
			if ($_REQUEST['sms_send'] == 1) {
				
				$_REQUEST['tran_id'] = SMS_KEYNO;
				$_REQUEST['tran_etc1'] = SMS_CNAME;
				$_REQUEST['tran_etc2'] = SMS_USERID;
				$_REQUEST['tran_etc3'] = SMS_USERNAME;
				$_REQUEST['sendtype'] = 'cell';
				$_REQUEST['tran_status'] = '1';
				$_REQUEST['tran_date'] = getFullToday();
			
				if (getTelWithHypen( $data[cell] ) && $hdata['reser_cancel_user'] == "1") {
					//echo $date['cell'].$msg. $hdata['reser_admin_tel'].$data['cell'];
					// 받을 사람(사용자)
					// 핸드폰번호가 있고 상담글 등록시 사용자에게 SMS발송
					$msg = str_replace("#name#", $_REQUEST[name], $hdata['reser_sms_cancelmsg']);
					$_REQUEST['tran_callback'] = $hdata['reser_admin_tel'];
					$_REQUEST['receiver'] = $data['cell'];
					$_REQUEST['tran_msg'] = $msg;
					$sms->sendSMS($_REQUEST);
				}
			}

			if ($_REQUEST['pop']=='1') {
				echo popupCloseRefresh("정상적으로 취소처리 되었습니다.");
			} else {
				echo returnURLMsg($reser->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), '정상적으로 취소처리 되었습니다.');
			}
		} else {
			if ($_REQUEST['pop'] == "1") {
				echo popupCloseRefresh("요청처리중 장애가 발생하였습니다.");
			} else {
				echo returnURLMsg($reser->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.error'));
			}
		}
	} else if ($_REQUEST['cmd'] == 'groupDelete') {

		$no = $_REQUEST['no'];
		
		$r = 0;
		for ($i=0; $i<count($no); $i++) {
			$r += $reser->delete($no[$i]);
		}

		if ($r > 0) {
			echo returnURLMsg($reser->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.countdelete',$r));
		} else {
			echo returnURLMsg($consult->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.error'));
		}

	} else if ($_REQUEST['cmd'] == 'delete') {

		$no = $_REQUEST['no'];

		$r = $reser->delete($no);

		if ($r > 0) {
			if ($_REQUEST['pop'] == '1') {
				echo popupCloseRefresh("정상적으로 삭제되었습니다.");
			} else {
				echo returnURLMsg($reser->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.delete'));
			}
		} else {
			echo returnURLMsg($reser->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.error'));
		}
	}

} else {
	echo returnURLMsg($consult->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.error'));
}
?>
</body>
</html>