<?session_start();
Header("Content-type: text/html; charset=utf-8");

include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/member/Member.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/reservation/Reservation.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Hospital.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Clinic.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Doctor.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

$reser = new Reservation(999999, $tablename, $_REQUEST);

$today = getToday();
$oneWeek = getDayDateAdd(-7, $today);
$oneMonth = getMonthDateAdd(-1, $today);
$threeMonth = getMonthDateAdd(-3, $today);

if ($branch && !$_REQUEST['shospital_fk']) {
	$_REQUEST['shospital_fk'] = $_SESSION['admin_hospital_fk'];
}
$rowPageCount = $reser->getCount($_REQUEST);
$result = $reser->getList($_REQUEST);

$hospital = new Hospital(999, $_REQUEST);
$clinic = new Clinic(999, $_REQUEST);
$doctor = new Doctor(999, $_REQUEST);
$member = new Member(999, "member", $_REQUEST);

header( "Content-type: application/vnd.ms-excel" );   
header( "Content-type: application/vnd.ms-excel; charset=utf-8");  
header( "Content-Disposition: attachment; filename = reservation.xls" );   
header( "Content-Description: PHP4 Generated Data" ); 
?>
<html>
<body topmargin=0 leftmargin=0>
<table cellspacing=0 cellpadding=0 border=1>
	<tr height=30>
		<td align="center" width=40  bgcolor="#C0C0C0">번호</td>
		<? if ($branch) { ?>
		<td align="center" bgcolor="#C0C0C0">지점명</td>
		<? } ?>
		<? if ($doctor) { ?>
		<td align="center" bgcolor="#C0C0C0">의료진</td>
		<? } ?>
		<? if ($clinic) { ?>
		<td align="center" bgcolor="#C0C0C0">진료과목</td>
		<? } ?>
		<td align="center" bgcolor="#C0C0C0">진료구분</td>
		<td align="center" bgcolor="#C0C0C0">초/재진구분</td>
		<td align="center" bgcolor="#C0C0C0">온오프구분</td>
		<td align="center" bgcolor="#C0C0C0">내원경로</td>
		<td align="center" bgcolor="#C0C0C0">전화상담요청</td>
		<td align="center" bgcolor="#C0C0C0">회원명</td>
		<td align="center" bgcolor="#C0C0C0">휴대폰</td>
		<td align="center" bgcolor="#C0C0C0">전화</td>
		<td align="center" bgcolor="#C0C0C0">이메일</td>
		<td align="center" bgcolor="#C0C0C0">예약일시</td>
		<td align="center" bgcolor="#C0C0C0">신청자</td>
		<td align="center" bgcolor="#C0C0C0">변경자</td>
		<td align="center" bgcolor="#C0C0C0">등록일</td>
		<td align="center" bgcolor="#C0C0C0">변경일</td>
		<td align="center" bgcolor="#C0C0C0">취소일</td>
		<td align="center" bgcolor="#C0C0C0">완료일</td>
		<td align="center" bgcolor="#C0C0C0">예약상태</td>
	</tr>
<?
	if ($rowPageCount[0] == 0) { 
?>
	<tr>
		<td align="center">등록된 예약내역이 없습니다.</td>
	</tr>		
<?
	} else {
		$i=0;
		while ($row=mysql_fetch_assoc($result)) {
			$state = $row['state'];
?>
	<tr height=28>
		<td align="center"><?=($rowPageCount[0]-($reser->reqPageNo-1)*$pageRows-$i)?></td>
		<? if ($branch) { ?>
		<td align="center"><?=$row['hospital_name']?></td>
		<? } ?>
		<? if ($doctor) { ?>
		<td align="center"><?=$row['doctor_name']?></td>
		<? } ?>
		<? if ($clinic) { ?>
		<td align="center"><?=$row['clinic_name']?></td>
		<? } ?>
		<td align="center"><?=getReserTypeName($row['resertype'])?></td>
		<td align="center"><?=getIncipientMeetName($row['newold'])?></td>
		<td align="center"><?=getOnoffTypeName($row['onoff'])?></td>
		<td align="center"><?=getRouteTypeName($row['route'])?></td>
		<td align="center"><? if($row['telconsult']==1) {?><font color="blue">요청함</font><?}else{?><font color="red">요청안함</font><?}?></td>		
		<td align="center"><?=$row['name']?></td>
		<td align="center"><?=$row['cell']?></td>
		<td align="center"><?=$row['tel']?></td>
		<td align="center"><?=$row['email']?></td>
		<td align="center"><?=$row['reserdate']?></td>
		<td align="center"><?=$row['offname']?></td>
		<td align="center"><?=$row['offchgname']?></td>
		<td align="center"><?=$row['registdate']?></td>
		<td align="center">
			<? if ($row['editdate']) { ?>
			<?=$row['editdate']?>
			<? } ?>
		</td>
		<td align="center">
			<? if ($row['canceldate']) { ?>
			<?=$row['canceldate']?>
			<? } ?>
		</td>
		<td align="center"><?=$row['confirmdate']?></td>
		<td align="center"><?=getReserStateName($state)?></td>
	</tr>
<?
		$i++;
		}
	}	
?>
</table>
</body>
</html>