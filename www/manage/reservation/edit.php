<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/member/Member.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/log/Log.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/reservation/Reservation.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Hospital.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Clinic.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Doctor.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

$reser = new Reservation($pageRows, $tablename, $_REQUEST);
$data = $reser->getData($_REQUEST['no']);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/boardConfig/config.php" ?>
<script>
function memberSearch(){
	window.open('membersearch.php?sval='+$("#name").val(),'searchMember', 'width=500, height=400,scrollbars=yes');
}

function goSave(obj) {
	
	if(validation(obj)){
		
		$("#board").submit();	
		
	}
}
</script>
</head>
<body> 
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<div id="wrap">
	<!-- canvas -->
	<div id="canvas">
		<!-- S T A R T :: headerArea-->
		<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/top.php" ?>
		<!-- E N D :: headerArea--> 
		
		<!-- S T A R T :: containerArea-->
		<div id="container">
			<div id="content">
				<div class="con_tit">
					<h2><?=$pageTitle?> - [수정]</h2>
				</div>
				<!-- //con_tit -->
				<div class="con">
					<!-- 내용 : s -->
					<div id="bbs">
						<div id="bwrite">
							<h3 class="minTitle"><?=getMsg('th.branch_sel')?></h3>
							<form method="post" name="board" id="board" action="<?=getSslCheckUrl($_SERVER['REQUEST_URI'], 'process.php')?>" >
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리 기본내용입니다.">
								<colgroup>
									<col width="15%" />
									<col width="85%" />
								</colgroup>
								<tbody>
									<? if ($branch) { ?>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.hospital_fk')?></label></th>
										<td colspan="3">
											<?
												$hospital = new Hospital(999, $_REQUEST);
												$hResult = $hospital->branchSelect();
											?>
											<select name="hospital_fk" id="hospital_fk" data-value="<?=getMsg('alert.text.shospital_fk')?>">
												<option value=""><?=getMsg('th.branch_sel')?></option>
												<? while ($row=mysql_fetch_assoc($hResult)) { ?>
												<option value="<?=$row['no']?>" <?=getSelected($row['no'],$data['hospital_fk'])?>><?=$row['name']?></option>
												<? } ?>
											</select>
										</td>
									</tr>
									<? } ?>
									
									<?
									if($clinic){
										$clinicObj = new Clinic(999, $_REQUEST);
										$clinicList = $clinicObj->selectBoxList(0,$data['hospital_fk'],$data['clinic_fk']);
										
									?>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.clinic_fk')?></label></th>
										<td colspan="3">
											<select name="clinic_fk" id="clinic_fk" data-value="<?=getMsg('alert.text.sclinic_fk')?>" >
											<?= $clinicList ?>
										</select>
										</td>
									</tr>
									<?
									}
									?>
									
									<?
									if($doctor){
										$doctorObj = new Doctor(999, $_REQUEST);
										$DoctorList = $doctorObj->selectBoxList(3,$data['hospital_fk'],($branch ? 0 : DEFAULT_BRANCH_NO),0,$data['doctor_fk'])
									?>
									<tr>
										<th scope="row"><label for="">의료진</label></th>
										<td colspan="3">
											<select name="doctor_fk" id="doctor_fk" data-value="의료진을 선택해 주세요.">
											<?= $DoctorList ?>
											</select>
										</td>
									</tr>
									<?
									}
									?>
									<?
									if($data['offname']){
									?>
									<tr>
										<th scope="row"><label for="">신청자</label></th>
										<td colspan="3">
											<?=$data['offname']?>
										</td>
									</tr>
									<?
									}
									?>
								
									
								</tbody>
							</table>
							
							<div class="reserveChoice">
								<div class="r_day">
									<h3 class="minTitle">예약일자</h3>
									<div class="calender_wrap">
										<div class="schedule"></div>
										<div class="under_wrap">
											<p><span class="possibleBox"></span>&nbsp;예약 가능날짜</p>
											<div class="sr_wrap">
												<p>예약선택일 <input type="text" id="reserdate" name="reserdate" value="<?=$data['reserdate']?>" readonly="readonly" title="예약선택일"/> 일</p>
												<i></i>
												<p>예약선택시간 <input type="text" name="resertime" id="resertime" size="13"  value="<?=$data['resertime']?>" readonly="readonly" /> 시</p>
											</div>
										</div>
									</div>
								</div>
								<div class="r_Time">
									<h3 class="minTitle">예약시간선택</h3>
									<div class="schedule_time"></div>					
									<p>시간 목록이 보이지 않으면 선택된 의료진이 <span class="colR01">휴진</span>입니다.</p>
								</div>
								<input type="hidden" name="p_reserdate" id="p_reserdate" value="<?=$data['reserdate']?>" />
								<input type="hidden" name="p_resertime" id="p_resertime" value="<?=$data['resertime']?>" />
							</div>
							<h3 class="minTitle">예약자 정보</h3>
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리 기본내용입니다.">
								<colgroup>
									<col width="15%" />
									<col width="35%" />
									<col width="15%" />
									<col width="35%" />
								</colgroup>
								<tbody>
									<tr>
										<th scope="row"><label for="">이름</label></th>
										<td>
											<input type="text" id="name" name="name" value="<?= $data['name'] ?>" data-value="이름을 입력해주세요." />
											<a class="blue_btn" href="javascript:;" id="member" onclick="memberSearch();">검색</a>
										</td>
										<th scope="row"><label for=""><?=getMsg('th.password')?></label></th>
										<td><input type="password" id="password" name="password" value="" /></td>
									</tr>
									<tr>
										<th scope="row"><label for="">연락처</label></th>
										<td><input type="text" id="tel" name="tel" value="<?= $data['tel'] ?>" data-value="연락처를 입력해주세요."  onkeyup="isOnlyNumberNotHypen(this);"/></td>
										<th scope="row"><label for="">핸드폰</label></th>
										<td>
											<input type="text" id="cell" name="cell" value="<?= $data['cell'] ?>"  onkeyup="isOnlyNumberNotHypen(this);"/>
											<?
											if($useSmsChk){
											?>
											<label for="telconsult" class="b_nor_c size02"><input type="checkbox" name="telconsult" id="telconsult" value="1" title="전화상담여부를 선택해주세요." <?= getChecked(1, $data['telconsult']) ?>/><i></i>
											전화상담여부</label>
											<?
											}
											?>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.email')?></label></th>
										<td colspan="3">
											<input type="text" id="email" name="email" value="<?= $data['email'] ?>" title="이메일주소를 입력해주세요." class="w100m25" />
										</td>
									</tr>
									<tr>
										<th scope="row"><label for="">온/오프 구분</label></th>
										<td>
											<?
											if($data['onoff'] == 0){$data['onoff'] = 1;}
											?>
											<label for="onoff1" data-value="온/오프라인를 선택해주세요." class="b_snor_r"><input type="radio" id="onoff1" name="onoff" value="1" <?= getChecked(1, $data['onoff']) ?> data-value="온/오프 구분을 선택해 주세요."/><i></i><span class="yel_col vaM">
											온라인</label>
											<label for="onoff2" data-value="온/오프라인를 선택해주세요." class="b_snor_r marl15"><input type="radio" id="onoff2" name="onoff" value="2" <?= getChecked(2, $data['onoff']) ?> data-value="온/오프 구분을 선택해 주세요."/><i></i><span class="gre_col vaM">
											오프라인</span></label>
										</td>
										<th scope="row"><label for="">초/재진 구분</label></th>
										<td>
											<label for="newold1" class="b_snor_r"><input type="radio" id="newold1" name="newold" value="1" <?= getChecked(1, $data['newold']) ?> data-value="초진/재진 구분을 선택해 주세요."/><i></i><span class="gre_col vaM">
											초진</span></label>
											<label for="newold2" class="b_snor_r marl15"><input type="radio" id="newold2" name="newold" value="2" <?= getChecked(2, $data['newold']) ?> data-value="초진/재진 구분을 선택해 주세요."/>
											<i></i><span class="blue_col vaM">재진</span></label>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for="">내원경로</label></th>
										<td>
											<select name="route" id="route" data-value="내원경로를 선택해주세요.">
												<?=getRouteNameType($data['route'])?>
											</select>
										</td>
										<th scope="row"><label for="">진료구분</label></th>
										<td>
											<select name="resertype" id="resertype" data-value="진료구분을 선택해주세요.">
												<?=getReserType($data['resertype'])?>
											</select>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for="">내용</label></th>
										<td colspan="3"><textarea name="etc" id="etc" data-value="예약내용을 입력해주세요." rows="5" style="width:100%;"><?=$data['etc']?></textarea></td>
									</tr>
								</tbody>
							</table>
							
							<input type="hidden" name="state" id="state" value="3"/><!-- 관리자 예약 시 상태는 완료 상태로 -->
							<input type="hidden" name="offname" id="offname" value="<?=$data['offname']?>" />
							<input type="hidden" name="cmd" value="edit"/>
							<input type="hidden" name="lastchange" value="수정"/>
							<input type="hidden" name="offchgname" value="<?=$_SESSION['admin_name']?>"/>
							<input type="hidden" name="no" id="no" value="<?=$data['no']?>"/>
							<? if (!$branch) { ?>
							<input type="hidden" name="hospital_fk" id="hospital_fk" value="<?=$data['hospital_fk']?>"/>
							<?}?>
							<? if (!$doctor) { ?>
							<input type="hidden" name="doctor_fk" id="doctor_fk" value="<?=DEFAULT_DOCTOR_NO?>"/>
							<?
							}
							?>
							<?
							$reserData = explode("-",$data['reserdate']);
							?>
							<input type="hidden" name="curMonth" id="curMonth" value="<?=($data['reserdate'] ? "" : $reserData[0]."-".$reserData[1])?>"/>

							<div class="btn">
								<div class="btnLeft">
									<a class="btns" href="<?=$reser->getQueryString('index.php', 0, $_REQUEST)?>"><strong><?=getMsg('btn.list')?></strong></a>
								</div>
								<div class="btnRight">
									<a class="btns" href="#" onclick="goSave(this);"><strong><?=getMsg('btn.save')?></strong></a>
								</div>
							</div>
							
							</form>
							
							<!--//btn-->
						</div>
						<!-- //bread -->
					</div>
					<!-- //bbs --> 
					<!-- 내용 : e -->
				</div>
				<!--//con -->
			</div>
			<!--//content -->
		</div>
		<!--//container --> 
		<!-- E N D :: containerArea-->
	</div>
	<!--//canvas -->
</div>
<!--//wrap -->

</body>
</html>