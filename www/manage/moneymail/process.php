<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Moneymail.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/email/SendMail.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

$mm = new Moneymail($pageRows, $tablename, $listtablename, $_REQUEST);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
</head>
<body>
<?
if (checkReferer($_SERVER["HTTP_REFERER"])) {
	
	if ($_REQUEST['cmd'] == 'write') {

		$_REQUEST = fileupload('filename', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, true, $maxSaveSize);		// 첨부파일
		$_REQUEST = fileupload('filename2', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, true, $maxSaveSize);		// 첨부파일
		$_REQUEST = fileupload('filename3', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, true, $maxSaveSize);		// 첨부파일


		$r = $mm->insert($_REQUEST);
		if ($r > 0) {
			echo returnURLMsg($mm->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.insert'));
		} else {
			echo returnURLMsg($mm->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.error'));
		}

	} else if ($_REQUEST['cmd'] == 'edit') {

		$_REQUEST = fileupload('filename', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, true, $maxSaveSize);		// 첨부파일
		$_REQUEST = fileupload('filename2', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, true, $maxSaveSize);		// 첨부파일
		$_REQUEST = fileupload('filename3', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, true, $maxSaveSize);		// 첨부파일

		$r = $mm->update($_REQUEST);

		if ($r > 0) {
			echo returnURLMsg($mm->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.update'));
		} else {
			echo returnURLMsg($mm->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.error'));
		}
	} else if ($_REQUEST['cmd'] == 'confirm') {

		$r = $mm->confirm($_REQUEST);

		if ($r > 0) {
			echo popupCloseRefresh(getMsg('result.text.update'));
		} else {
			echo returnURLMsg($mm->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'list.php'), 0, $_REQUEST), getMsg('result.text.error'));
		}
	} else if ($_REQUEST['cmd'] == 'cancel') {

		$r = $mm->cancel($_REQUEST);

		if ($r > 0) {
			echo popupCloseRefresh(getMsg('result.text.update'));
		} else {
			echo returnURLMsg($mm->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'list.php'), 0, $_REQUEST), getMsg('result.text.error'));
		}
	} else if ($_REQUEST['cmd'] == 'groupDelete') {

		$no = $_REQUEST['no'];
		
		$r = 0;
		for ($i=0; $i<count($no); $i++) {
			$r += $mm->delete($no[$i]);
		}

		if ($r > 0) {
			echo returnURLMsg($mm->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.countdelete',$r));
		} else {
			echo returnURLMsg($mm->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.error'));
		}

	} else if ($_REQUEST['cmd'] == 'delete') {

		$no = $_REQUEST['no'];
		
		$r = $mm->delete($no);

		if ($r > 0) {
			echo returnURLMsg($mm->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.delete'));
		} else {
			echo returnURLMsg($mm->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.error'));
		}
	} else if ($_REQUEST['cmd'] == 'send') {

		$_REQUEST['no'] = $_REQUEST['emailhistory_fk'];
		$data = $mm->getData($_REQUEST['no']);

		$filename = array();
		$filename_org = array();
		if ($data['filename']) {
			$filename[0] = $data['filename'];
			$filename_org[0] = $data['filename_org'];
		}
		if ($data['filename2']) {
			$filename[1] = $data['filename2'];
			$filename_org[1] = $data['filename2_org'];
		}
		if ($data['filename3']) {
			$filename[2] = $data['filename3'];
			$filename3_org[2] = $data['filename3_org'];
		}
		$sendmail = new SendMail();

		// 메일발송
		$sendmail->sendFile(COMPANY_EMAIL, "", $_REQUEST['receiveemail'], $data['title'], str_replace("\\", "", $data['contents']), $uploadPath, $filename, $filename_org);
		$_REQUEST['sending_count'] = 1;
		$r = $mm->insertList($_REQUEST);
		if ($r > 0) {
		echo returnURLMsg($mm->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'list.php'), 0, $_REQUEST), '정상적으로 발송되었습니다.');
		} else {
			echo returnURLMsg($mm->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'list.php'), 0, $_REQUEST), getMsg('result.text.error'));
		}
	} 


} else {
	echo returnURLMsg($mm->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.error'));
}
?>
</body>
</html>