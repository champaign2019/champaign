<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Moneymail.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Hospital.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

$mm = new Moneymail($pageRows, $tablename, $listtablename, $_REQUEST);

$data = $mm->getData($_REQUEST['no']);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/boardConfig/config.php" ?>
<script>
function goSave(obj) {
	
	
	if(validation(obj)){
		
		$("#frm").submit();	
		
	}
}
</script>
</head>
<body> 
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<div id="wrap">
	<!-- canvas -->
	<div id="canvas">
		<!-- S T A R T :: headerArea-->
		<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/top.php" ?>
		<!-- E N D :: headerArea--> 
		
		<!-- S T A R T :: containerArea-->
		<div id="container">
			<div id="content">
				<div class="con_tit">
					<h2><?=$pageTitle?> - [수정]</h2>
				</div>
				<!-- //con_tit -->
				<div class="con">
					<!-- 내용 : s -->
					<div id="bbs">
						<div id="bread">
							<form method="post" name="frm" id="frm" action="<?=getSslCheckUrl($_SERVER['REQUEST_URI'], 'process.php')?>" enctype="multipart/form-data">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리 기본내용입니다.">
								<colgroup>
									<col width="15%" />
									<col width="85%" />
								</colgroup>
								<tbody>
									<tr>
										<th scope="row"><label for="">프로그램명</label></th>
										<td><input type="text" id="gubun" name="gubun" value="<?=$data['gubun']?>" data-value="프로그램명을 입력해 주세요." class="w80"/></td>
									</tr>
									<tr>
										<th scope="row"><label for="">*제목</label></th>
										<td><input type="text" id="title" name="title" value="<?=$data['title']?>" data-value="제목을 입력해 주세요." class="w80"/></td>
									</tr>
									<tr>
										<td colspan="2">
											<textarea id="contents" name="contents" title="내용을 입력해주세요" style="width:100%">
												<?=stripslashes($data['contents'])?>
											</textarea>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for="">첨부파일1</label></th>
										<td>
										<? if (!$data['filename']) { ?>
											<input type="file" id="filename" name="filename" title="첨부파일을 입력해주세요" class="w80"/>
										<? } else { ?>
											<p>기존파일 : <?=$data['filename_org']?><br/>
											<input type="checkbox" id="filename_chk" name="filename_chk" value="1" title="첨부파일을 삭제하시려면 체크해주세요" />
											<label for="filename_chk"><?=getMsg('lable.checkbox.image_del')?></label>
											<input type="file" name="filename" id="filename" title="첨부파일을 업로드 해주세요." />
										<? } ?>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for="">첨부파일2</label></th>
										<td>
										<? if (!$data['filename2']) { ?>
											<input type="file" id="filename2" name="filename2" title="첨부파일을 입력해주세요" class="w80"/>
										<? } else { ?>
											<p>기존파일 : <?=$data['filename2_org']?><br/>
											<input type="checkbox" id="filename2_chk" name="filename2_chk" value="1" title="첨부파일을 삭제하시려면 체크해주세요" />
											<label for="filename_chk"><?=getMsg('lable.checkbox.image_del')?></label>
											<input type="file" name="filename2" id="filename2" title="첨부파일을 업로드 해주세요." />
										<? } ?>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for="">첨부파일1</label></th>
										<td>
										<? if (!$data['filename3']) { ?>
											<input type="file" id="filename3" name="filename3" title="첨부파일을 입력해주세요" class="w80"/>
										<? } else { ?>
											<p>기존파일 : <?=$data['filename3_org']?><br/>
											<input type="checkbox" id="filename3_chk" name="filename3_chk" value="1" title="첨부파일을 삭제하시려면 체크해주세요" />
											<label for="filename_chk"><?=getMsg('lable.checkbox.image_del')?></label>
											<input type="file" name="filename3" id="filename3" title="첨부파일을 업로드 해주세요." />
										<? } ?>
										</td>
									</tr>
								</tbody>
							</table>
							<input type="hidden" name="cmd" id="cmd" value="edit"/>
							<input type="hidden" name="no" id="no" value="<?=$data['no']?>"/>
							<input type="hidden" name="name" id="name" value="<?=$_SESSION['admin_name']?>"/>
							<input type="hidden" name="tablename" id="tablename" value="<?=$tablename?>"/>
							<div class="btn">
								<div class="btnLeft">
									<a class="btns" href="index.php"><strong><?=getMsg('btn.list')?></strong></a>
								</div>
								<div class="btnRight">
									<a class="btns" href="javascript:;" onclick="goSave(this);"><strong><?=getMsg('btn.save')?></strong></a>
								</div>
							</div>
							</form>
							
							<!--//btn-->
						</div>
						<!-- //bread -->
					</div>
					<!-- //bbs --> 
					<!-- 내용 : e -->
				</div>
				<!--//con -->
			</div>
			<!--//content -->
		</div>
		<!--//container --> 
		<!-- E N D :: containerArea-->
	</div>
	<!--//canvas -->
</div>
<!--//wrap -->

</body>
</html>