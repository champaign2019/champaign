<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Moneymail.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/email/SendMail.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Hospital.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/member/Member.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";
	
	$member = new Member($pageRows, "member", $_REQUEST);

	$mm = new Moneymail($pageRows, $tablename, $listtablename, $_REQUEST);
	
	$data = $mm->listRead($_REQUEST);
	
	$temp = $_REQUEST['no'];

	$_REQUEST['no'] = $data['emailhistory_fk'];

	$pdata = $mm->getData($_REQUEST['no']);

	$filename = array();
	$filename_org = array();
	if ($pdata['filename']) {
		$filename[0] = $pdata['filename'];
		$filename_org[0] = $pdata['filename_org'];
	}
	if ($pdata['filename2']) {
		$filename[1] = $pdata['filename2'];
		$filename_org[1] = $pdata['filename2_org'];
	}
	if ($pdata['filename3']) {
		$filename[2] = $pdata['filename3'];
		$filename3_org[2] = $pdata['filename3_org'];
	}
	$sendmail = new SendMail();

	// 메일발송
	//$sendmail->sendFile(COMPANY_EMAIL, "", $data['receiveemail'], $pdata['title'], str_replace("\\", "", $pdata['contents']), $uploadPath, $filename, $filename_org);
	
	$_REQUEST['sending_count'] = 1;
	$_REQUEST['no'] = $temp;


	$mm->updateSendingCount($_REQUEST); //비용메일 업데이트
	
	$data = $mm->listRead($_REQUEST);

	
	$viewPop = "";
	$smsPop = "";
	$emailPop = "";

		$viewPop = "style='cursor:pointer;' onclick=\"window.open('view.php?no=".$data['no']."', 'view', 'width=1000,height=480,scrollbars=no');\"";
		$smsPop = "style='cursor:pointer;' onclick=\"location.href='".$member->getQueryStringAddParam('/manage/sms/write_pop.php', $data['no'], $_REQUEST, 'receiver'. $data['cell'])."'\"";
		$emailPop = "style='cursor:pointer;' onclick=\"location.href='".$member->getQueryStringAddParam('/manage/email/write_pop.php', $data['no'], $_REQUEST, 'receiveEmail'. $data['email'])."'\"";


?>

	<td class="first" <?=$viewPop?>><?=$data['no']?></td>
	<?
	if($branch){
	?>
	<td <?=$viewPop?>><?=$data['hospital_name']?></td>
	<?
	}
	?>
	<td class="title" <?=$viewPop?>><?=$data['gubun']?></td>
	<td <?=$viewPop?>><?=$data['name']?></td>
	<td <?=$smsPop?>><?=$data['cell']?></td>
	<td <?=$emailPop?>><?=$data['receiveemail']?></td>
	<td <?=$viewPop?>><?=getYMD($data['registdate'])?></td>
	<td <?=$viewPop?>><?=getSendingStateName($data['sending'])?>  (<?=$data['sending_count']?>)</td>
	<td>
		<? if ($data['state'] == 2) { ?>
			<p class="blue_btn consl" onClick="window.open('moneyCancel.php?no=<?=$data['no']?>', 'reserCancel' , 'scrollbars=0, resizable=0, top=500, left=700, width=265, height=191')">상담완료</p>
		<?}?>
		<? if ($data['state'] == 1) { ?>
			<p class="red_btn endc" onClick="window.open('moneyConfirm.php?no=<?=$data['no']?>', 'reserConfirm' , 'scrollbars=0, resizable=0, top=500, left=700, width=270, height=210')">상담전</p>
		<?}?>	
	</td>
	<td class="last">
		<div class="btnList"> <a href="javascript:;" onclick="sending(<?=$data['no']?>)" class="btns"> <strong>발송</strong> </a> </div>
	</td>

