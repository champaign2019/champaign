<?
$pageRows = 10;
$pageTitle = "비용메일 관리";
$tablename = "moneymailhistory";
$listtablename = "moneymaillist";
$uploadPath		= "/upload/email/";						// 파일, 동영상 첨부 경로
$maxSaveSize	 	= 50*1024*1024;							// 50Mb
$gradeno		  		 = 2;								// 관리자 권한 기준 [지점 사용시]

$branch	  		= false;							// 지점 사용유무  
$clinic	  		= false;							// 진료과목 지점별 사용유무	
$doctor	  		= false;							// 진료과목 지점별 사용유무	

$textAreaEditor	= true;							// 에디터 사용여부
$editorIgnore= "";								//에디터 미사용 배열 구분자 ex : '|0|1|'
?>