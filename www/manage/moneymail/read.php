<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Moneymail.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Hospital.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

$mm = new Moneymail($pageRows, $tablename, $listtablename, $_REQUEST);
$_REQUEST['orderby'] = $orderby; //정렬 배열 선언
$data = $mm->getData($_REQUEST['no']);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
</head>
<body> 
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<div id="wrap">
	<!-- canvas -->
	<div id="canvas">
		<!-- S T A R T :: headerArea-->
		<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/top.php" ?>
		<!-- E N D :: headerArea--> 
		
		<!-- S T A R T :: containerArea-->
		<div id="container">
			<div id="content">
				<div class="con_tit">
					<h2><?=$pageTitle?> - [<?=getMsg('th.detail')?>]</h2>
				</div>
				<!-- //con_tit -->
				<div class="con">
					<!-- 내용 : s -->
					<div id="bbs">
						<div id="bread">
							<h3><?=getMsg('th.branch_sel')?></h3>
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리 기본내용입니다.">
								<colgroup>
									<col width="15%" />
									<col width="35%" />
									<col width="15%" />
									<col width="35%" />
								</colgroup>
								<tbody>
									<tr>
										<th scope="row"><label for="">프로그램명</label></th>
										<td><?=$data['gubun']?></td>
										<th scope="row"><label for="">작성자</label></th>
										<td><?=$data['name']?></td>
									</tr>
									<tr>
										<th scope="row"><label for="">제목</label></th>
										<td colspan="3"><?=$data['title']?></td>
									</tr>
									<tr>
										<td colspan="4"><?=stripslashes($data['contents'])?></td>
									</tr>
									<? if ($data['filename']) { ?>
									<tr>
										<td colspan="4">
											<img src="/img/file_img.gif" alt="파일첨부" />
											<a href="/lib/download.php?path=<?=$uploadPath?>&vf=<?=$data['filename']?>&af=<?=$data['filename_org']?>" target="_blank"><?=$data['filename_org']?> [<?=getFileSize($data['filesize'])?>]</a>
										</td>
									</tr>
									<? } ?>
									<? if ($data['filename2']) { ?>
									<tr>
										<td colspan="4">
											<img src="/img/file_img.gif" alt="파일첨부" />
											<a href="/lib/download.php?path=<?=$uploadPath?>&vf=<?=$data['filename2']?>&af=<?=$data['filename2_org']?>" target="_blank"><?=$data['filename2_org']?> [<?=getFileSize($data['filesize2'])?>]</a>
										</td>
									</tr>
									<? } ?>
									<? if ($data['filename3']) { ?>
									<tr>
										<td colspan="4">
											<img src="/img/file_img.gif" alt="파일첨부" />
											<a href="/lib/download.php?path=<?=$uploadPath?>&vf=<?=$data['filename2']?>&af=<?=$data['filename2_org']?>" target="_blank"><?=$data['filename3_org']?> [<?=getFileSize($data['filesize3'])?>]</a>
										</td>
									</tr>
									<? } ?>
								</tbody>
							</table>
							<div class="btn">
								<div class="btnLeft">
									<a class="btns" href="<?=$mm->getQueryString('index.php', 0, $_REQUEST)?>"><strong><?=getMsg('btn.list')?></strong></a>
								</div>
								<div class="btnRight">
									<a class="btns" href="<?=$mm->getQueryString('edit.php', $data['no'], $_REQUEST)?>"><strong><?=getMsg('btn.edit')?></strong></a>
									<a class="btns" href="#" onClick="delConfirm('<?=getMsg('confirm.text.delete')?>','process.php?cmd=delete&no=<?=$data['no']?>');"><strong><?=getMsg('btn.delete')?></strong></a>
								</div>
							</div>
							<!--//btn-->
						</div>
						<!-- //bread -->
					</div>
					<!-- //bbs --> 
					<!-- 내용 : e -->
				</div>
				<!--//con -->
			</div>
			<!--//content -->
		</div>
		<!--//container --> 
		<!-- E N D :: containerArea-->
	</div>
	<!--//canvas -->
</div>
<!--//wrap -->

</body>
</html>