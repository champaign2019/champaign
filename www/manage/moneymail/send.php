<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Moneymail.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Hospital.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

$mm = new Moneymail($pageRows, $tablename, $listtablename, $_REQUEST);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/boardConfig/config.php" ?>
<script>

function goSave(obj) {
	
	
	if(validation(obj)){
		
		$("#frm").submit();	
		
	}
}

</script>
</head>
<body> 
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<div id="wrap">
	<!-- canvas -->
	<div id="canvas">
		<!-- S T A R T :: headerArea-->
		<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/top.php" ?>
		<!-- E N D :: headerArea--> 
		
		<!-- S T A R T :: containerArea-->
		<div id="container">
			<div id="content">
				<div class="con_tit">
					<h2><?=$pageTitle?> - [비용메일 보내기]</h2>
				</div>
				<!-- //con_tit -->
				<div class="con">
					<!-- 내용 : s -->
					<div id="bbs">
						<div id="bread">
							<form method="post" name="frm" id="frm" action="<?=getSslCheckUrl($_SERVER['REQUEST_URI'], 'process.php')?>" enctype="multipart/form-data">
							<div class="table_wrap">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리 기본내용입니다.">
								<colgroup>
									<col width="15%" />
									<col width="85%" />
								</colgroup>
								<tbody>
									<?
									if($branch){
									?>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.hospital_fk')?></label></th>
										<td>
											<?
												$hospital = new Hospital(999, $_REQUEST);
												$hResult = $hospital->branchSelect();
											?>
											<select name="hospital_fk" id="hospital_fk" data-value="<?=getMsg('alert.text.shospital_fk')?>">
												<option value=""><?=getMsg('th.branch_sel')?></option>
												<? while ($hrow=mysql_fetch_assoc($hResult)) { ?>
												<option value="<?=$hrow['no']?>" <?=getSelected($hrow['no'], $_REQUEST['shospital_fk'])?>><?=$hrow['name']?></option>
												<? } ?>
											</select>
										</td>
									</tr>
									<?
									}
									?>
									
								
									<tr>
										<th scope="row"><label for="">프로그램</label></th>
										<td>
											<?
											$result = $mm->selectOptionList($_REQUEST);
											?>
											<select name="emailhistory_fk" id="emailhistory_fk" data-value="프로그램을 선택해 주세요.">
												<? while ($row=mysql_fetch_assoc($result)) { ?>
													<option value="<?=$row['no']?>" <?=getSelected($row['no'], $row['emailhistory_fk'])?>><?=$row['gubun']?></option>
												<? } ?>
											</select>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for="">이름</label></th>
										<td><input type="text" id="name" name="name" value="<?=$_REQUEST['name']?>"  class="w40" data-value="이름을 입력해 주세요."/></td>
									</tr>
									<tr>
										<th scope="row"><label for="">연락처</label></th>
										<td><input type="text" id="cell" name="cell" value="<?=$_REQUEST['cell']?>"  class="w40" data-value="연락처를 입력해 주세요."/></td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.email')?></label></th>
										<td><input type="text" id="receiveemail" name="receiveemail" value="<?=$_REQUEST['receiveemail']?>"  class="w40" data-value="이메일을 입력해 주세요."/></td>
									</tr>
								</tbody>
							</table>
							<input type="hidden" name="cmd" id="cmd" value="send"/>
							<input type="hidden" name="tablename" id="tablename" value="<?=$tablename?>"/>
							<input type="hidden" name="listtablename" id="listtablename" value="<?=$listtablename?>"/>
							</div>
							<div class="btn">
								<div class="btnLeft">
									<a class="btns" href="list.php"><strong><?=getMsg('btn.list')?></strong></a>
								</div>
								<div class="btnRight">
									<a class="btns" href="#" onclick="goSave(this);"><strong>비용메일 보내기</strong></a>
								</div>
							</div>
							<!--//btn-->
							</form>
							
						</div>
						<!-- //bread -->
					</div>
					<!-- //bbs --> 
					<!-- 내용 : e -->
				</div>
				<!--//con -->
			</div>
			<!--//content -->
		</div>
		<!--//container --> 
		<!-- E N D :: containerArea-->
	</div>
	<!--//canvas -->
</div>
<!--//wrap -->

</body>
</html>