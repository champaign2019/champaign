<? session_start(); 
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/branch/Branch.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/branch/Type.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

$branch = new Branch($pageRows, $_REQUEST);
$type = new Type($pageRows, $_REQUEST);
$rowPageCount = $type->getCount($_REQUEST);
$result = $type->getList($_REQUEST);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
<script>
function groupDelete() {	
	if ( $('[name^=no]:checked').length > 0 ) {
		document.frm.submit();
	} else {
		alert("<?=getMsg('alert.text.delete')?>");
	}
}

function goSave() {
	if ($("#insertForm #name").val().length == 0) {
		alert("분류을 입력하세요.");
		$("#insertForm #name").focus();
		return false;
	}
	$("#insertForm").submit();
}

function goEdit(no, name, branch_fk) {
	
	if ($("#frm #"+name+"").val().length == 0) {
		alert("분류을 입력하세요.");
		$("#frm #"+name+"").focus();
		return false;
	}
	var tName = $("#frm #"+name+"").val();
	var tBranch_fk = $("#frm #"+branch_fk+"").val();
	location.href="process.php?cmd=edit&no="+no+"&name="+tName+"&branch_fk="+tBranch_fk;
	
}

</script>
</head>
<body> 
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<div id="wrap">
	<!-- canvas -->
	<div id="canvas">
		<!-- S T A R T :: headerArea-->
		<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/top.php" ?>
		<!-- E N D :: headerArea--> 
		
		<!-- S T A R T :: containerArea-->
		<div id="container">
			<div id="content">
				<div class="con_tit">
					<h2><?=$pageTitle?> - [<?=getMsg("th.list")?>]</h2>
				</div>
				<!-- //con_tit -->
				<div class="con">
					<!-- 내용 : s -->
					<div id="bbs">
						<div id="blist">
							<div class="top_search">
								<form name="insertForm" id="insertForm" action="process.php" method="post">
								<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리 목록입니다.">
									<colgroup>
										<col class="w10" />
										<col class="w40" />
										<col class="w10" />
										<col class="w40" />
									</colgroup>
									<tbody>
										<tr>
											<th scope="row"><label for="">분류명</label></th>
											<td>
												<select name="branch_fk" id="branch_fk">
													<?=$branch->selectBoxList(0,0,$_SESSION['branch_fk'], $_REQUEST['sbranch_fk'])?>
												</select>
											</td>
											<th scope="row"><label for="">분류명</label></th>
											<td>
												<input type="text" id="name" name="name" value=""  class="input60p"/>
												<a class="btns" href="javascript:;" onClick="goSave()"><strong><?=getMsg('btn.save')?></strong></a>
											</td>
										</tr>
									</tbody>
								</table>
								<input type="hidden" name="cmd" value="write"/>
								</form>
							</div>
							<!--//top_search-->
							<p><span><strong><?=getMsg('th.total')?> <?=$rowPageCount[0]?><?=getMsg('th.amount')?></strong>  |  <?=$type->reqPageNo?>/<?=$rowPageCount[1]?><?=getMsg('th.page')?></span></p>
							<form name="frm" id="frm" action="process.php" method="post">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리목록입니다.">
								<colgroup>
									<col class="w5" />
									<col class="w5" />
									<col class="w10" />
									<col class="" />
									<col class="w20" />
									<col class="w10" />
									<col class="w10" />
								</colgroup>
								<thead>
									<tr>
										<th scope="col" class="first">
										<label class="b_first_c"><input type="checkbox" name="allChk" id="allChk" onClick="check(this, document.getElementsByName('no[]'))"/>
										<i></i>
										</label>
										</th>
										<th scope="col"><?=getMsg("th.no")?></th>
										<th scope="col"><?=getMsg("th.hospital_fk")?></th> 
										<th scope="col">분류</th> 
										<th scope="col"><?=getMsg("th.registdate")?></th> 
										<th scope="col"><?=getMsg('btn.edit')?></th> 
										<th scope="col" class="last"><?=getMsg('btn.delete')?></th>
									</tr>
								</thead>
								<tbody>
								<? if ($rowPageCount[0] == 0) { ?>
									<tr>
										<td class="first" colspan="7">등록된 분류이 없습니다.</td>
									</tr>
								<?
									 } else {
										 $i=0;
										 while ($row=mysql_fetch_assoc($result)) {
								?>
									<tr>
										<td class="first">
										<label class="b_nor_c"><input type="checkbox" name="no[]" id="no" value="<?=$row[no]?>"/>
										<i></i>
										</label>
										</td>
										<td><?=$row[no]?></td>
										<td>
											<select name="branch_fk" id="branch_fk<?=$i?>" title="지점을 선택해주세요" >
												<?=$branch->selectBoxList(2,0,$_SESSION['branch_fk'], $row[branch_fk])?>
											</select>
										</td>
										<td><input type="text" name="name<?=$i?>" id="name<?=$i?>" size="70" value="<?=$row[name]?>"/></td>
										<td><?=$row[registdate]?></td>
										<td><a class="blue_btnre" href="javascript:;" onclick="goEdit('<?=$row[no]?>', 'name<?=$i?>', 'branch_fk<?=$i?>');"><strong><?=getMsg('btn.edit')?></strong> </a></td>
										<td class="last"><a class="red_btnre" href="javascript:;" onclick="delConfirm('<?=getMsg('confirm.text.delete')?>','process.php?cmd=delete&no=<?=$row[no]?>')"><strong><?=getMsg('btn.delete')?></strong> </a></td>
									</tr>
								<?
										$i++;
										}
									 }
								?>
								</tbody>
							</table>
								<input type="hidden" name="cmd" id="cmd" value="groupDelete"/>
							</form>
							<div class="btn">
								<div class="btnLeft">
									<a class="btns" href="javascript:;" onclick="groupDelete();"><strong><?=getMsg('btn.delete')?></strong> </a>
								</div>
							</div>
							<!--//btn-->
							<form name="searchForm" id="searchForm" action="index.php" method="post">
								<div class="search">
									<select name="sbranch_fk" id="sbranch_fk" title="지점을 선택해주세요" onchange="$('#searchForm').submit();">
									<?=$branch->selectBoxList(3,0,$_SESSION['branch_fk'], $_REQUEST['sbranch_fk'])?>
								</select>
								</div>
							</form>
							<!-- //search --> 
						</div>
						<!-- //blist -->
					</div>
					<!-- //bbs --> 
					<!-- 내용 : e -->
				</div>
				<!--//con -->
			</div>
			<!--//content -->
		</div>
		<!--//container --> 
		<!-- E N D :: containerArea-->
	</div>
	<!--//canvas -->
</div>
<!--//wrap -->

</body>
</html>