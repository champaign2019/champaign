<?
	$pageRows = 10;
	$pageTitle = "관리자 관리";
	$uploadPath = "";
	$maxSaveSize = 0;

	$branch = false;
	$type = false;
	$doctor = false;
	$gradeView = false;
	$textAreaEditor	= true;							// 에디터 사용여부
	$editorIgnore= "";								//에디터 미사용 배열 구분자 ex : '|0|1|'	
	$useFileCount  	= 10;							// 파일 첨부 가능 갯수
	$useFileDrag    = false;
	/*
		20180605 - 변경내용	
		
		1. 이전 enviroment -> enviroment_bak 으로 백업 ( process, read, write 전부 jsp 여서 company-php enviroment 로 대체)
		2. write edit 그 외에 hospital_fk 있는곳 수정 필요.  관리자 계성 생성시 hospital_fk 0으로 설정됨.
	*/
?>