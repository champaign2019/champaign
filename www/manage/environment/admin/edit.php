<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/environment/Admin.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

$admin = new Admin($pageRows, "admin", $_REQUEST);
$data = $admin->getData($_REQUEST);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/boardConfig/config.php" ?>
<script>
function goSave() {
	if ($("#grade").val() == "0") {
		alert("등급을 선택하세요");
		$("#grade").focus();
		return false;
	}
	if ($("#branch_fk").val() == "") {
		alert("지점을 선택하세요");
		$("#branch_fk").focus();
		return false;
	}
	if ($("#name").val() == "") {
		alert("이름을 입력하세요");
		$("#name").focus();
		return false;
	}
	if ($("#id").val() == "") {
		alert("아이디를 입력하세요");
		$("#id").focus();
		return false;
	}

	
	$("#frm").submit();
}


</script>
</head>
<body> 
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<div id="wrap">
	<!-- canvas -->
	<div id="canvas">
		<!-- S T A R T :: headerArea-->
		<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/top.php" ?>
		<!-- E N D :: headerArea--> 
		
		<!-- S T A R T :: containerArea-->
		<div id="container">
			<div id="content">
				<div class="con_tit">
					<h2><?=$pageTitle?> - [수정]</h2>
				</div>
				<!-- //con_tit -->
				<div class="con">
					<!-- 내용 : s -->
					<div id="bbs">
						<div id="bread">
							<h3>관리자 기본 정보</h3>
							
							<form name="frm" id="frm" action="<?=getSslCheckUrl($_SERVER['REQUEST_URI'], 'process.php')?>" method="post">
							<div class="table_wrap">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리 기본내용입니다.">
								<colgroup>
									<col width="15%" />
									<col width="35%" />
									<col width="15%" />
									<col width="35%" />
								</colgroup>
								<tbody>
									<tr>
										<th scope="row"><label for="">*등급</label></th>
										<td colspan="3">
											<?
												$adminResult = $admin->getGradeList($_REQUEST);
											?>
											<select name="grade" id="grade">
											<? while ($row = mysql_fetch_assoc($adminResult)) { ?>
												<option value="<?=$row['no']?>" <?=getSelected($row['no'], $data['grade'])?>><?=$row['grade_name']?></option>
											<? } ?>
											</select>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for="">이름</label></th>
										<td>
											<input type="text" id="name" name="name" value="<?=$data['name']?>" data-value="관리자 이름을 입력해주세요." />
										</td>
										<th scope="row"><label for="">이메일</label></th>
										<td>
											<input type="text" id="email" name="email" value="<?=$data['email']?>" title="관리자 이메일을 입력해주세요." />
										</td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.id')?></label></th>
										<td><?=$data['id']?></td>
										<th scope="row"><label for=""><?=getMsg('th.password')?></label></th>
										<td>
											<input type="password" id="password" name="password" value="" />
										</td>
									</tr>
									<tr>
										<td colspan="4">
											<textarea id="memo" name="memo" title="내용을 입력해주세요" style="width:100%" ><?=$data['memo']?></textarea>
										</td>
									</tr>
								</tbody>
							</table>
							<input type="hidden" name="hospital_fk" value="0" />
							<input type="hidden" name="cmd" value="edit">
							<input type="hidden" name="stype" id="stype" value="<?=$_GET['stype']?>"/>
							<input type="hidden" name="sval" id="sval" value="<?=$_GET['sval']?>"/>
							<input type="hidden" name="sgrade" id="sgrade" value="<?=$_GET['sgrade']?>"/>
							<input type="hidden" name="no" id="no" value="<?=$_GET['no']?>"/>
							</div>
							</form>
							
							<div class="btn">
								<div class="btnLeft">
									<a class="btns" href="<?=$admin->getQueryString('index.php', 0, $_REQUEST)?>"><strong><?=getMsg('btn.list')?></strong></a>
								</div>
								<div class="btnRight">
									<a class="btns" href="#" onclick="goSave();"><strong><?=getMsg('btn.save')?></strong></a>
								</div>
							</div>
							<!--//btn-->
						</div>
						<!-- //bread -->
					</div>
					<!-- //bbs --> 
					<!-- 내용 : e -->
				</div>
				<!--//con -->
			</div>
			<!--//content -->
		</div>
		<!--//container --> 
		<!-- E N D :: containerArea-->
	</div>
	<!--//canvas -->
</div>
<!--//wrap -->

</body>
</html>