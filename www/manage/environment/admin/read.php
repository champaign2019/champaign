<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/environment/Admin.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

$admin = new Admin($pageRows, "admin", $_REQUEST);
$data = $admin->getData($_REQUEST);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
<script type="text/javascript">
	function goDelete() {
		var del = confirm ('<?=getMsg('confirm.text.delete')?>');
		if (del){
			document.location.href = "process.php?no=<?=$data['no']?>&cmd=delete";
		} else {
			return false;
		}
	}
</script>
</head>
<body> 
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<div id="wrap">
	<!-- canvas -->
	<div id="canvas">
		<!-- S T A R T :: headerArea-->
		<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/top.php" ?>
		<!-- E N D :: headerArea--> 
		
		<!-- S T A R T :: containerArea-->
		<div id="container">
			<div id="content">
				<div class="con_tit">
					<h2><?=$pageTitle?> - [<?=getMsg('th.detail')?>]</h2>
				</div>
				<!-- //con_tit -->
				<div class="con">
					<!-- 내용 : s -->
					<div id="bbs">
						<div id="bread">
							<h3><?=getMsg('th.branch_sel')?></h3>
							<div class="table_wrap">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리 기본내용입니다.">
								<colgroup>
									<col width="15%" />
									<col width="35%" />
									<col width="15%" />
									<col width="35%" />
								</colgroup>
								<tbody>
									<tr>
										<th scope="row"><label for="">등급</label></th>
										<td colspan="3"><?=$data['grade_name']?></td>
									</tr>
									<tr>
										<th scope="row"><label for="">이름</label></th>
										<td><?=$data['name']?></td>
										<th scope="row"><label for="">이메일</label></th>
										<td><?=$data['email']?></td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.id')?></label></th>
										<td colspan="3"><?=$data['id']?></td>
									</tr>
									<tr>
										<td colspan="4">
											<?=$data['memo']?>
										</td>
									</tr>
								</tbody>
							</table>
							</div>
							<div class="btn">
								<div class="btnLeft">
									<a class="btns" href="<?=$admin->getQueryString('index.php', 0, $_REQUEST)?>"><strong><?=getMsg('btn.list')?></strong></a>
								</div>
								<div class="btnRight">
									<a class="btns" href="<?=$admin->getQueryString('edit.php', $data['no'], $_REQUEST)?>"><strong><?=getMsg('btn.edit')?></strong></a>
									<a class="btns" href="#" onClick="goDelete();"><strong><?=getMsg('btn.delete')?></strong></a>
								</div>
							</div>
							<!--//btn-->
						</div>
						<!-- //bread -->
					</div>
					<!-- //bbs --> 
					<!-- 내용 : e -->
				</div>
				<!--//con -->
			</div>
			<!--//content -->
		</div>
		<!--//container --> 
		<!-- E N D :: containerArea-->
	</div>
	<!--//canvas -->
</div>
<!--//wrap -->

</body>
</html>