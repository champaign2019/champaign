<?session_start();
unset($_SESSION['admin_no']);
unset($_SESSION['admin_id']);
unset($_SESSION['admin_name']);
unset($_SESSION['admin_grade']);
unset($_SESSION['admin_email']);
unset($_SESSION['admin_hospital_fk']);

//쿠키삭제
unset($_COOKIE['SESSID']); setcookie('SESSID', '', time() - 3600, '/');

include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? $pageTitle = "로그아웃"; ?>
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
</head>
<body>
<script>
alert("로그아웃 되었습니다.");
document.location.href="/manage/index.php";

</script>

</body>
</html>