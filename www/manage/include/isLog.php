<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/log/Log.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

if (IS_LOG && LOG_TYPE == 1) {
	$lmgr = new Log($tablename);
	$ldata = $lmgr->getData($_REQUEST['no']);
?>
<h3>유입경로</h3>
<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리 기본내용입니다.">
	<colgroup>
		<col width="15%" />
		<col width="35%" />
		<col width="15%" />
		<col width="35%" />
	</colgroup>
	<tbody>
		<tr>
			<th scope="row"><label for="">유입경로</label></th>
			<td>
				<?=$ldata['refname']?> 
				<?
 					if( $ldata['refname'] != "북마크" ) {
 				?>
				<a href="http://<?=$ldata['refhost']?><?=$ldata['refpage']?><?=$ldata['refparam']?>" target="_brank"><?=$ldata['refhost']?></a>
				<?
					}
				?>
			</td>
			<th scope="row"><label for="">검색어</label></th>
			<td><?=$ldata['refsearch']?></td>
		</tr>
		<tr>
			<th scope="row"><label for="">광고유형</label></th>
			<td><?=$ldata['campaigntext']?></td>
			<th scope="row"><label for="">아이피</label></th>
			<td>
				<?
					if($ldata['ip']){
				?>
					<?=$ldata['ip']?>
					<span class="btnAll" style="top:-5px;">
					<?
						if($ldata['sessionid']) {
					?>
					<strong class="btn_in inbtn">
					<input type="button" value="과거접속이력" class="btns" onclick="window.open('http://weblog2.vizensoft.com/site/pop_sessionpage.jsp?sitenum=<?=WEBLOG_NUMBER?>&url=<?=COMPANY_URL?>&sval=<?=$ldata['sessionid']?>', 'past', 'width=800,height=600,scrollbars=yes');" />
					</strong>
					<?
						} else {
					?>
					<strong class="btn_in inbtn">
					<input type="button" value="과거접속이력" class="btns" onclick="window.open('http://weblog2.vizensoft.com/site/pop_sessionpage.jsp?sitenum=<?=WEBLOG_NUMBER?>&url=<?=COMPANY_URL?>&sval=<?=$ldata['ip']?>', 'past', 'width=800,height=600,scrollbars=yes');" />
					</strong>
					<?}			?>
					</span>
				<?}?>
			</td>
		</tr>
	</tbody>
</table>
<?
}
?>