	<title><?=COMPANY_NAME?></title>
<?
include_once $_SERVER['DOCUMENT_ROOT']."/lib/global/Message_global.php";
?>
<meta http-equiv="X-UA-compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1.0" />
<meta name="description" content="페이지설명">
<meta name="keywords" content="검색키워드등록">
<meta name="classification" content="검색사이트 카테고리 등록">
<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" />

<link href="/manage/css/nanumgothic.css" rel="stylesheet" type="text/css" />
<link href="/manage/css/notosanskr.css" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='//cdn.jsdelivr.net/font-kopub/1.0/kopubdotum.css'>
<link href='https://cdn.rawgit.com/openhiun/hangul/14c0f6faa2941116bb53001d6a7dcd5e82300c3f/nanumbarungothic.css' rel='stylesheet' type='text/css'>
<link href="/manage/css/basic.css" rel="stylesheet" type="text/css" />
<link href="/manage/css/layout.css" rel="stylesheet" type="text/css" />
<link href="/manage/css/board.css" rel="stylesheet" type="text/css" />
<link href="/manage/css/manage.css" rel="stylesheet" type="text/css" />
<link href="/css/colorPicker.css" rel="stylesheet" type="text/css" />
<link href="/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="/js/jstree_/css/style.min.css" />
<link rel="stylesheet" href="/css/jquery.datetimepicker.css" type="text/css" media="all" />
<!-- 반응형 모바일 메뉴 활성화 시킬 경우 주석을 풀면 됩니다. -->
<!-- <link href="/manage/css/mobile_menu.css" rel="stylesheet" type="text/css" /> -->

<script type="text/javascript" src="/js/function.js"></script>
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/calendar_beans_v2.0.js"></script>
<script type="text/javascript" src="/js/jquery-1.8.0.min.js"></script>
<!-- 달력 변경 jquery.js 버젼업, datetimepicker.full.js 달력 js -->
<script src="/js/jquery.js"></script>
<script src="/js/jquery.datetimepicker.full.js"></script>
<!-- 달력 변경 끝 -->
<script src="/js/dev.js"></script>

<script src="/js/jstree_/js/jstree.min.js"></script>
<!-- 반응형 모바일 메뉴 효과를 활성화 시킬 경우 주석을 풀면 됩니다 -->
<!-- <script src="/js/mobile_menu.js"></script> -->

<!-- summernote editor-->

<link href="/summernote_20190715/bootstrap-3.3.2-dist/css/bootstrap.css" rel="stylesheet">
<link href="/summernote_20190715/summernote.css" rel="stylesheet">

<script src="/summernote_20190715/bootstrap-3.3.2-dist/js/bootstrap.js"></script> 
<script type="text/javascript" src="/js/jquery.form.js"></script>
<script type="text/javascript" src="/js/jquery-ui.js"></script>
<script src="/summernote_20190715/summernote.js"></script>
<script src="/summernote_20190715/lang/summernote-ko-KR.js"></script>

<!-- include summernote css/js-->




<!-- <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script> -->
<script type="text/javascript" src="/js/function_jquery.js"></script>

<script type="text/javascript" src="/smarteditor/js/HuskyEZCreator.js"></script>
<script type="text/javascript" src="/js/jquery.colorPicker.js"></script>
<script type='text/javascript' src='/js/validationBind.js'></script> 


<link rel="stylesheet/less" type="text/css" href="/manage/css/color.less">
<script type="text/javascript" src="/manage/include/less.min.js"></script>

<script type="text/javascript">
$(window).load(function() {
	var mobileKeyWords = new Array('iPhone', 'iPod', 'BlackBerry', 'Android', 'Windows CE', 'LG', 'MOT', 'SAMSUNG', 'SonyEricsson'); 
	for (var word in mobileKeyWords){ 
		if (navigator.userAgent.match(mobileKeyWords[word]) != null){ 
			// 모바일
			$("div#menu > ul > li > a").attr("href","javascript:;");
		} 
	} 	
});
</script>

<?
include_once $_SERVER['DOCUMENT_ROOT']."/lib/xssFilter.php";
$filter    = new xssFilter();
$_REQUEST  = $filter->checkParameter($_REQUEST);
?>
<script type="text/javascript" src="/js/jQ.js"></script>