<script>
function popRecharge() {
	var str,width,height;
	str="'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,copyhistory=no,";
	str=str+"width=1024";
	str=str+",height=768',top=100,left=100";

	window.open("https://hosting.vizensoft.com/customer/indexPayment.jsp?projectfk=<?=PROJECT_FK?>&costomerfk=<?=CUSTOMER_FK?>&tran_id=<?=SMS_KEYNO?>",'recharge',str);
}

$(function(){
	$.ajax({
		url: "https://hosting.vizensoft.com/customer/rechargeJquery.jsp?projectfk=<?=PROJECT_FK?>&costomerfk=<?=CUSTOMER_FK?>&tran_id=<?=SMS_KEYNO?>", // 클라이언트가 요청을 보낼 서버의 URL 주소
		type: "GET",                             // HTTP 요청 방식(GET, POST)
		dataType: "jsonp",                         // 서버에서 보내줄 데이터의 타입
		success: function(data){
			$("#recharge").text(data);
		},
		error : function(error){
			console.log(error);
		}
	});
});

function popMaintenance() {
	var str,width,height;
	str="'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,copyhistory=no,";
	str=str+"width=1024";
	str=str+",height=768',top=100,left=100";

	window.open("http://vizen.vizensoft.com/site/maintenance/customer/index.jsp?customerfk=<?=CUSTOMER_FK?>&projectfk=<?=PROJECT_FK?>",'maintenance',str);
}

function OpenWinCount(URL,width,height) {

	var str;
	str="'toolbar=yes, location=yes, directories=yes, status=no, menubar=yes, scrollbars=yes,resizable=yes,copyhistory=no,";
	str=str+"width="+width;
	str=str+",height="+height+"',top=100,left=100";

	window.open(URL,'_blank');
}

var parent = $(".parent");
$(document).ready(function(){
	if(isMobileTablet()) $(".parent").attr("href", "javascript: void(0); ");

	// 1depth 메뉴들
	
	//현재 주소
	// 주소중 manage이후 폴더 경로 자르기 ex) /manage/board/index.php -> board
	var curUrl = document.location.pathname;
	curUrl = location.pathname.substring(0,  location.pathname.lastIndexOf("/") ).substring( location.pathname.substring(0,  location.pathname.lastIndexOf("/") ).lastIndexOf("/"));
	
	$(".parent").parent().find("a").each(function(){
		$(this).find("span").removeClass("bg");

		// 1depth 메뉴 href 담기
		href = $(this)[0].href;

		// 가장 가까운 부모 디렉토리명
		href = href.substring(0,  href.lastIndexOf("/") ).substring( href.substring(0,  href.lastIndexOf("/") ).lastIndexOf("/"));
		
		// 현재 주소에서 href가 포함 되어 있으면 그 메뉴에 배경색 지정
		if(curUrl == href){
			$(this).parents('li:eq(1)').find(".parent span").addClass("bg");
		}
	});
	
	// 방문자 통계 클릭시 메뉴 유지
	parent.click(function(){
		// 1depth 메뉴 href 담기
		href = $(this)[0].href;
		
		if(href.indexOf("OpenWinCount") >= 0){
			parent.find("span").removeClass("bg");
			$(this).find("span").addClass("bg");
		}
	});

	// 메뉴 수에 맞게 width 값 수정.
	$("ul.menu > li").css("width", (100 / $("ul.menu > li").length) + "%");
});

</script>
<div id="header">
	<div class="header_inner">
		<div class="up_type">
			<div class="row cf">
				<div class="three col">
			        <div class="hamburger" id="hamburger-9">
			          <span class="line"></span>
			          <span class="line"></span>
			          <span class="line"></span>
			        </div>
			    </div>
			    <script type>
		          $(document).ready(function(){
		              $(".hamburger").click(function(){
		                $(this).toggleClass("is-active");
		                $("div#menu").toggleClass("on");
		              });
		            });
		      </script>
	        </div>
			<h1><?=COMPANY_NAME?><!--<span>
		<a href="#" onclick="window.open('<%=com.vizensoft.config.SiteProperty.COMPANY_URL%>');"> 1</a></span> -->
			<?
			if(strpos($_SERVER["HTTP_HOST"], "vizensoft.com") !== false) {
			?>
			PHP <?=VERSION?>
			<?
			}
			?>
			</h1>
			<p class="login_name"></p>
	<!-- 		<div class='hidnBoard' onclick='location.href="/manage/funcTest/notice/index.php"'></div> -->
			<!--<div class="site_go">
				<a style="cursor:pointer;" onclick="popMaintenance();">
					<ul>
						<li>
							<span>유지보수요청</span>
						</li>
					</ul>
				</a>
			</div>-->
			<div class="util">
				<ul>
					<? if( "1" != SMS_KEYNO) { ?>
					<li class="frist"><a href="javascript:;" style="cursor:default;">SMS현재잔액 <span id="recharge"></span>원</a><i></i></li>
					<li class="line frist"><a href="javascript:;" onclick="popRecharge();">충전하기 <i></i></a></li>
					<? } ?>
					<li class="line frist"><a href="javascript:;" onclick="popMaintenance();"><?=getMsg('btn.customer')?><i></i></a></li>
					<li class="line"><a href="http://www.vizensoft.com" target="_blank"><?=getMsg('btn.vizen')?><i></i></a></li>
					<li ><a href="javascript:;" onclick="window.open('<?=COMPANY_URL?>');"><?=getMsg('btn.company')?></a></li>
					<li><a href="/manage/include/logout.php"><?=getMsg('btn.logout')?></a></li>
				</ul>
			</div>
			<div class="util_toggle">
				<a href="javascript:;"><i class="fa fa-ellipsis-h"></i></a>
			</div>
			<script>
			  $(document).ready(function(){
				  $(".util_toggle a").click(function(){
					$("#header .header_inner .util").toggleClass("on");
				  });
				});
			</script>
		</div>
		
		<div id="menu">
  			<ul class="menu">
				<li><a href="/manage/board/notice/index.php" class="parent"><span><?=getMsg('menu.board')?></span></a>
					<div class="standard_left">
					<ul>
						<li><a href="/manage/board/faq/index.php"><span><?=getMsg('menu.faq')?></span></a></li>
<!-- 						<li><a href="/manage/board/faq/FaqCatagory/index.php"><span><?=getMsg('menu.faqcatagory')?></span></a></li> -->
						<li><a href="/manage/board/common/index.php"><span>지점관리</span></a></li>
						<li><a href="/manage/board/popup/index.php"><span><?=getMsg('menu.popup')?></span></a></li>
					</ul>
					</div>
				</li>

				<li><a href="javascript:OpenWinCount('http://weblog2.vizensoft.com/login.jsp?id=program&pw=program09&sitenum=<%=com.vizensoft.config.SiteProperty.getWeblogNumber()%>&url=index.jsp','_blank');" class="parent"><span><?=getMsg('menu.openwincount')?></span></a>
					<div>
					<ul>
						<li><a href="javascript:OpenWinCount('http://weblog2.vizensoft.com/login.jsp?id=program&pw=program09&sitenum=<%=com.vizensoft.config.SiteProperty.getWeblogNumber()%>&url=index.jsp','_blank');"><span><?=getMsg('menu.openwincount')?></span></a></li>
					</ul>
					</div>
				</li>
<!-- 				<li><a href="javascript:OpenWinCount('http://nweblog.vizensoft.com/side_login.php?id=vizensoft&password=vizen&sitenum=<%=com.vizensoft.config.SiteProperty.getWeblogNumberNew()%>&url=index.php',1024,768);" class="parent"><span>신 방문자통계</span></a> -->
<!-- 					<div> -->
<!-- 					<ul> -->
<!-- 						<li><a href="javascript:OpenWinCount('http://nweblog.vizensoft.com/side_login.php?id=vizensoft&password=vizen&sitenum=<%=com.vizensoft.config.SiteProperty.getWeblogNumberNew()%>&url=index.php',1024,768);"><span>신 방문자통계</span></a></li> -->
<!-- 					</ul> -->
<!-- 					</div> -->
<!-- 				</li> -->
				<li><a href="/manage/environment/admin/index.php" class="parent"><span><?=getMsg('menu.admin')?></span></a>
					<div>
					<ul>
						<li><a href="/manage/environment/admin/index.php"><span><?=getMsg('menu.admin_index')?></span></a></li>
						<li><a href="/manage/environment/loginhistory/index.php"><span><?=getMsg('menu.loginhistory')?></span></a></li>
						<li><a href="/manage/environment/stipulation/stipulation.php"><span><?=getMsg('menu.stipulation')?></span></a></li>
						<!-- <li><a href="/manage/environment/intranet/category/index.php"><span>인트라넷 관리</span></a></li>
						<li><a href="/manage/environment/intranet/permission/index.php"><span>인트라넷 권한설정</span></a></li> -->
					</ul>
					</div>
				</li>
				<? if ($_SESSION['admin_no'] == 1) { ?>
				<li><a href="/manage/management/grade/grade.php" class="parent"><span>비젠소프트</span></a>
					<div>
					<ul>
						<li style="display:none;" ><a href="/manage/management/boardConfig/index.php"><span><?=getMsg('menu.board')?></span></a></li>
						<!-- property:분류관리 -->
						<li><a href="/manage/management/grade/grade.php"><span><?=getMsg('menu.grade')?></span></a></li>
						<li><a href="/manage/management/siteConfig/index.php"><span>기본설정</span></a></li>
					</ul>
					</div>
				</li>
				<? } ?>
			</ul>
		</div>
		<!--//gnb-->
	</div>
	<!-- //header_inner -->
</div>
<!-- //header -->

<script type="text/javascript">
	$(window).load(function(){
		$("#blist table tr td").mouseenter(function(){
		$(this).parent("tr").addClass("on");
		});
		$("#blist table tr td").mouseleave(function(){
			$("#blist table tr").removeClass("on");
		});		
	});	
</script>

