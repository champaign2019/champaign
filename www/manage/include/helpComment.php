<div id="layerTop" class="helper">
	<div>
		<p>
			<strong>공지함 : </strong>현재 게시판 목록 페이지에서 최상에 노출됩니다.<br />
			<strong>공지안함 : </strong>현재 게시판 목록에 TOP공지 되지 않습니다.
		</p>
		<p class="point">* TOP공지를 선택하였을 경우 현재 게시판이 메인 페이지에 노출되어 있어도 노출되지 않습니다.</p>
	</div>
</div>

<div id="layerMain" class="helper">
	<div>
		<p>
			<strong>노출함 : </strong>현재 게시판의 내용이 메인 페이지에 있을 경우 메인 페이지에 노출되게 됩니다.<br />
			<strong>노출안함 : </strong>현재 게시판의 내용이 메인 페이지에 노출되지 않습니다.
		</p>
		<p class="point">* 현재 게시판의 내용이 메인 페이지에 있으면서 메인노출 선택을 하나도 하지 않을 경우 최근에 등록된 게시물이 메인 페이지에 노출 됩니다.</p>
	</div>
</div>

<div id="layerNew" class="helper"> 
	<div>
		<p>
			<strong>항상 : </strong>현재 게시판 목록페이지에서 해당 글 앞에 NEW아이콘이 표기됩니다.<br />
			<strong>하루만 : </strong>현재 게시판 목록페이지에서 해당 글 앞에 하루동안만 NEW아이콘이 표기됩니다.<br />
			<strong>표기안함 : </strong> NEW아이콘이 표기 되지 않습니다.
		</p>
	</div>
</div>

<div id="layer13" class="helper">
	<div>
		<p>
			<strong>HTML : </strong>순수한 HTML로 작성될 경우<br />
			<strong>TEXT : </strong> TEXT 또는 부분적인 HTML이 포함될 수 있습니다.
		</p>
		<p class="point">* TEXT 선택 시 읽기페이지에서 문단 단락 마다 &ltbr&gt태그를 삽입합니다.</p>
	</div>
</div>


<div id="layerReadno" class="helper">
	<div>
		<p>
			<strong>조회 수 : </strong>입력하지 않으시면 0부터 시작합니다.
		</p>
	</div>
</div>


<div id="layerRegistdate" class="helper"> 
	<div>
		<p>
			<strong>작성일 : </strong>입력하지 않으시면 현재 날짜로 등록됩니다.<br />
			ex) 2007-02-01 09:00:00
		</p>
	</div>
</div>

<div id="layerHoly" class="helper"> 
	<div>
		<p>
			추석연휴와 설날연휴를 제외한 공휴일 진료시 진료시간 설정입니다.
		</p>
	</div>
</div>

<div id="layerSecret" class="helper"> 
	<div>
		<p>
			<strong>공개 : </strong>사용자페이지에 노출됩니다.<br />
			<strong>비공개 : </strong>사용자페이지에 노출 되지 않습니다.
		</p>
	</div>
</div>
<div id="layerSmsh" class="helper size01"> 
	<div>
		<p>
			클릭 시 문자발송이 가능합니다.
		</p>
	</div>
</div>
<div id="layerEmailh" class="helper size01"> 
	<div>
		<p>
			클릭 시 메일발송이 가능합니다.
		</p>
	</div>
</div>
<div id="layerOrderExplain" class="helper"> 
	<div>
		<p>
			랜딩 목록 노출 순서를 설정할 수 있습니다.
		</p>
		<p class="point">* 0인 경우 순서에 영향을 미치지 않습니다.</p>
		<p class="point">* 순서가 같은 경우 최신 등록일 순서대로 나옵니다.</p>
	</div>
</div>

<script type="text/javaScript">
function helpComment(type, input) {
	var ev = window.event;	
	var mousePos = mouseCoords(ev);	
	
	if($("#"+input).width()+mousePos.x > $(window).width()){
		mousePos.x = mousePos.x -$("#"+input).width(); 
	}

	document.getElementById(input).style.top = mousePos.y+"px";
    document.getElementById(input).style.left = mousePos.x+"px";
	document.getElementById(input).style.visibility = type;

}

function mouseCoords(ev) {	
	if(ev.pageX || ev.pageY){		
		return {x:ev.pageX, y:ev.pageY};	
	}	
	return {
		x:ev.clientX + document.body.scrollLeft - document.body.clientLeft,
		y:ev.clientY + document.body.scrollTop  - document.body.clientTop	
	};
}

jQuery(document).ready(function(){
	jQuery(".helpComment").filter("#Registdate").css("margin-left","15px");
	jQuery(".helpComment").hover(
		function(){
			var id = jQuery(this).attr("id");
			helpComment("visible","layer"+id);
		},function(){
			var id = jQuery(this).attr("id");
			$("#layer"+id).css("visibility","hidden");
		}
	);
	
	jQuery(".helpComment").focusin(function(){
		var id = jQuery(this).attr("id");
		helpComment("visible","layer"+id);
	});
			
	jQuery(".helpComment").focusout(function(){
		var id = jQuery(this).attr("id");
		$("#layer"+id).css("visibility","hidden");
	});
});
</script>