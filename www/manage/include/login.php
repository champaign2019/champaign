<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/environment/Admin.class.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/loginCheck.php" ?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
<?
$url = $_REQUEST['url'];
$param = $_REQUEST['param'];

// 파라메터가 있을 경우에는 url 뒷에 결합한다.
if ($param) {
	$url = $url."?".$param;
} else {
	$url = START_PAGE;
}
$admin = new Admin(0, "admin", $_REQUEST);
$result = $admin->loginCheck($_REQUEST);
$row = mysql_fetch_array($result);
$loginCheck = $row['cnt'];
$allowedIp = true;
// vizensoft 계정으로 로그인 시도시에만 ip 체크
if ("vizensoft" == $_REQUEST['id']) {
	$allowedIp = ipMatches(ALLOWED_IP, $_SERVER['REMOTE_ADDR']);
}
if ($allowedIp) {
	if ($loginCheck>0) {
		//쿠키삭제
		unset($_COOKIE['SESSID']); setcookie('SESSID', '', time() - 3600, '/');

		$result = $admin->getLoginSessionInfo($_REQUEST);
		$row = mysql_fetch_array($result);

		$_SESSION['admin_no'] = $row['no'];
		$_SESSION['admin_id'] = $row['id'];
		$_SESSION['admin_name'] = $row['name'];
		$_SESSION['admin_grade'] = $row['grade'];
		$_SESSION['admin_email'] = $row['email'];
		$_SESSION['admin_hospital_fk'] = $row['hospital_fk'];

		$admin->insertLoginHistory($row['id'], $row['name'], $_SERVER['REMOTE_ADDR']);

		if(strcmp(KEEP_USER,$row['id']) == 0) {
			setcookie('SESSID',KEEP_USER,0,'/');
		}

		echo returnURL($url);
	} else {
		echo returnHistory("아이디 비밀번호를 확인해주세요.");
	}
}
else {
?>
<script>
	alert("접속 허용된 IP가 아닙니다.");
	history.back();
</script>
<?
}

?>
</body>
</html>