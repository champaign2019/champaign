<?session_start();

include_once $_SERVER['DOCUMENT_ROOT']."/lib/global/Message_global.php";

include $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";


// http://www.vizensoft , http://vizensoft  둘의 세션 공유가 안되어 관리자 진입 주소 통일.
if(str_replace('http://', '', COMPANY_URL ) != $_SERVER['SERVER_NAME'] && !strpos($_SERVER['SERVER_NAME'], 'vizensoft')){
	Header("Location: ".COMPANY_URL."/manage");
}

$loginCheck = false;

if (isset($_SESSION['admin_id'])) {
	$loginCheck = true;
}

if (!$loginCheck) {
	$url = $_SESSION['url'];
	if (!$url) $url = START_PAGE;
	$param = $_SESSION['param'];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
<script>
var tem;
$(window).load(function(){


function loginCheck(){
	if ( getObject("id").value.length < 1 ) {
		alert("<?=getMsg('alert.text.id')?>");
		getObject("id").focus();
		return false;
	}
	if ( getObject("password").value.length < 1 ) {
		alert("<?=getMsg('alert.text.password')?>");
		getObject("password").focus();
		return false;
	}
	var f = document.board;
	if (f.reg.checked==true) {
	   document.cookie = "cookie_userid=" + f.id.value + ";path=/;expires=Sat, 31 Dec 2050 23:59:59 GMT;";
	} else {
	   var now = new Date();	
	   document.cookie = "cookie_userid=" + f.id.value + ";path=/;expires="+now.getTime();
	}
	return true;
}

function userid_chk() {
	var f=document.board;
	var useridname = CookieVal("cookie_userid");
	
	if (useridname=="null"){	
		f.id.focus();
		f.id.value="";
	} else {
		f.password.focus();
		f.id.value=useridname;
		f.reg.checked=true;
	}
}

function CookieVal(cookieName) {
	thisCookie = document.cookie.split("; ");
	for (var i = 0; i < thisCookie.length;i++) {
		if (cookieName == thisCookie[i].split("=")[0]) {
			return thisCookie[i].split("=")[1];
		}
	}
	return "null" ;
}
//-->

</script>
</head>
<body onload="getObject('id').focus();userid_chk();">
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<div id="login">
	<div class="title">
		<h1><?=COMPANY_NAME?> <span><?=getMsg('th.adminmode')?></span></h1>
		<p><?=getMsg('message.tbody.adminlogin')?></p>
	</div>
	<div class="login">
	<form name="board" id="board" method="post" action="/manage/include/login.php" onsubmit="return loginCheck();">
		<fieldset>
			<legend><?=getMsg('th.adminmode_login')?></legend>
			<div class="bgBox">
				<div class="infoBox">
					<dl>
						<dt>
							<label for="id"><img src="/img/id_icon.png" alt=""></label>
						</dt>
						<dd>
							<input type="text" id="id" name="id" value="" title="아이디를 입력해주세요." style="ime-mode:inactive"/>
						</dd>
					</dl>
					<dl>
						<dt>
							<label for="password"><img class="password_icon" src="/img/password_icon.png" alt=""></label>
						</dt>
						<dd>
							<input type="password" id="password" name="password" value="" title="비밀번호를 입력해주세요." />
						</dd>
					</dl>
				</div>
				<!-- //infoBox -->
				<input type="submit" alt="로그인" class="loginBtn" title="" value="<?=getMsg('btn.login')?>" />
			</div>
			<!-- //bgBox -->
			<div class="joinList">
				<label for="reg" class="b_nor_c"><input type="checkbox" name="reg" id="reg"/> <i></i> <?=getMsg('lable.checkbox.idinsert')?></label>
			</div>
			<!-- //joinList -->
			<input type="hidden" name="url" id="url" value="<?=$url?>"/>
			<input type="hidden" name="param" id="param" value="<?=$param?>"/>
		</fieldset>
	</form>
	</div>
	<div class="footer">
		Copyrights (c) 2013 <a href="http://www.vizensoft.com" target="_blank">VIZENSOFT</a>. All Rights Reserved.  
	</div>
</div>
</body>
<?
} else {
	echo "<script>location.href='".START_PAGE."';</script>";
}
?>
</html>