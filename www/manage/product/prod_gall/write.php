<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Common.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/category/Category.class.php";
include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

$common = new Common($pageRows, $tablename, $_REQUEST);
	$_REQUEST['pcsorts'] = 1;
	$ctlist = Category::categoryList();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/boardConfig/config.php" ?>

<script>
	var isSubmitChk = false;
	$(window).load(function(){
		$('#registdate').datetimepicker({timepicker:false,format: "Y-m-d"});
		$("#button").click(function(){
			$('#registdate').datetimepicker('show');
		})				
		<?
		if($registdateGubun){
		?>
		// 달력
		//initCal({id:"registdate",type:"day",today:"y",timeYN:"y"});
		<?
		}
		?>
		fn_addDel_object(<?=$useFileCount?>); //파일 function 생성
	});
	
	function goSave(obj) {
		
		if(validation(obj)){
			isSubmitChk = true;
			$("#frm").submit();	
			
		}
			
	}
	
</script>
</head>
<body> 
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<div id="wrap">
	<!-- canvas -->
	<div id="canvas">
		<!-- S T A R T :: headerArea-->
		<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/top.php" ?>
		<!-- E N D :: headerArea--> 
		
		<!-- S T A R T :: containerArea-->
		<div id="container">
			<div id="content">
				<div class="con_tit">
					<h2><?=$pageTitle?> - [쓰기]</h2>
				</div>
				<!-- //con_tit -->
				<div class="con">
					<!-- 내용 : s -->
					<div id="bbs">
						<div id="bread">
							
							<form method="post" name="frm" id="frm" action="<?=getSslCheckUrl($_SERVER['REQUEST_URI'], 'process.php')?>" enctype="multipart/form-data" >
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리 기본내용입니다.">
								<colgroup>
									<col width="15%" />
									<col width="35%" />
									<col width="15%" />
									<col width="35%" />
								</colgroup>
								<tbody>
									<tr>
										<th scope="row"><label for="">카테고리</label></th>
										<td colspan="3" id="groupCategory">
											<span>
												<select name="categoryFks" onchange="fn_select_category(this)" data-value="카테고리를 선택해 주세요.">
													<option value="">선택하세요.</option>
													<?
														while($row = mysql_fetch_assoc($ctlist)) {
													?>
													<option value="<?=$row['code'] ?>"><?=$row['orgname'] ?></option>
													<?
														}
													?>
												</select>
											</span>
										</td>
									</tr>
									
									
									<tr>
										<th scope="row"><label for="">작성자</label></th>
										<td colspan="<?=$useEmail ? 1 : 3?>">
											<input type="text" name="name" id="name" class="w100m25" value="<?=$_SESSION['admin_name']?>" data-value="작성자를 입력하세요."/>
										</td>
										<?
										if($useEmail){
										?>
										<th scope="row"><label for="">이메일</label></th>
										<td colspan="<?=useCell ? 1 : 3?>">
											<input type="text" id="email" name="email" value="" title="이메일주소를 입력해주세요." onkeyup="isValidEmailing(this);" class="w100m25"/>
										</td>
										<?
										}
										?>
									</tr>
									
									<? if($usePerfect || $registdateGubun) { ?>
									<tr>
										<?
										if($usePerfect){
										?>
										<th scope="row"><label for="">조회수</label></th>
										<td colspan="<?=$registdateGubun ? 1 : 3?>">
											<input type="text" id="readno" name="readno" title="조회수를 입력해주세요" style="width:50px;" onkeyup="isOnlyNumber(this);"/>
										</td>
										<?
										}
										?>
										<?
										if($registdateGubun){
										?>
										<th scope="row"><label for="">등록일</label></th>
										<td colspan="<?=$usePerfect ? 1 : 3?>">
											<input type="hidden" name="registdateGubun" id="registdateGubun" value="1" />
											<input type="text" id="registdate" name="registdate" class="w100m25" value="<?=getFullToday()?>" title="등록일을 입력해주세요" />&nbsp;
											<span id="CalregistdateIcon">
												<button type="button" id="button"><img src="/manage/img/calendar_icon.png" id="CalregistdateIconImg" style="cursor:pointer;"/></button>
											</span>
										</td>
										<?
										}
										?>
									</tr>
									<? } ?>
									
									<?
									if($istop || $isnew){
									?>
									<tr>
										<?
										if($istop){
										?>
										<th scope="row"><label for="">TOP공지</label></th>
										<td colspan="<?=$istop ? 1 : 3?>">
											<label for="top1" class="b_snor_r"><input type="radio" id="top1" name="top" value="1" data-value="TOP공지를 선택해 주세요."/>
											<i></i>
											<span class="blue_col vaM">공지함</span></label>
											<label for="top0" class="b_snor_r marl15"><input type="radio" id="top0" name="top" value="0" checked data-value="TOP공지를 선택해 주세요."/>
											<i></i><span class="red_col vaM">
											공지안함</span></label>
											<img src="/manage/img/question_btn.gif" class="helpComment" id="Top" alt="도움말 이미지"/>
										</td>
										<?
										}
										?>
										<?
										if($isnew){
										?>
										<th scope="row"><label for="">NEW아이콘</label></th>
										<td colspan="<?=$istop ? 1 : 3?>">
											<label for="newicon1" class="b_snor_r"><input type="radio" id="newicon1" name="newicon" value="1" data-value="NEW아이콘을 선택해 주세요."/>
											<i></i><span class="blue_col vaM">
											항상</span></label>
											<label for="newicon2" class="b_snor_r marl15"><input type="radio" id="newicon2" name="newicon" value="2" data-value="NEW아이콘을 선택해 주세요."/>
											<i></i><span class="gre_col vaM">
											하루만</span></label>
											<label for="newicon0" class="b_snor_r marl15"><input type="radio" id="newicon0" name="newicon" value="0" checked data-value="NEW아이콘을 선택해 주세요."/>
											<i></i><span class="red_col vaM">
											표기안함</span></label>
											<img src="/manage/img/question_btn.gif" class="helpComment" id="New" alt="도움말 이미지"/>
										</td>
										<?
										}
										?>
									</tr>
									<?
									}
									?>
									<?
									if($ismain){
									?>
									<tr>
										
										<th scope="row"><label for="">메인게시물</label></th>
										<td colspan="3">
											<label for="main1" class="b_snor_r"><input type="radio" id="main1" name="main" value="1" style="width:14px" data-value="메인게시물을 선택해 주세요."/>
											<i></i><span class="blue_col vaM">
											노출</span></label>
											<label for="main0" class="b_snor_r marl15"><input type="radio" id="main0" name="main" value="0" checked style="width:14px" data-value="메인게시물을 선택해 주세요."/>
											<i></i><span class="red_col vaM">
											노출안함</span></label>
											<img src="/manage/img/question_btn.gif" class="helpComment" id="Main" alt="도움말 이미지"/>		
										</td>
																				
									</tr>
									<?
									}
									?>
									<tr>
										<td colspan="4" height="0" class="bline"></td>
									</tr>
									<tr>
										<th scope="row"><label for="">제목</label></th>
										<td colspan="3">
											<input type="text" id="title" name="title" class="input92p" title="제목을 입력해주세요" data-value="제목을 입력해주세요." />	
										</td>
									</tr>
									<tr>
										<td colspan="4">
											
											<textarea id="contents" name="contents" title="내용을 입력해주세요" style="width:100%;" data-value="내용을 입력해주세요."></textarea>	
										</td>
									</tr>
									
									<tr>
										<th scope="row"><label for="">목록이미지</label></th>
										<td colspan="3">
											<input type="file" id="imagename1" name="imagename1" class="input92p" title="목록이미지를 업로드 해주세요."/>	
										</td>
									</tr>
									<tr>
										<th scope="row"><label for="">목록이미지 설명</label></th>
										<td colspan="3">
											<input type="text" id="image1_alt" name="image1_alt" class="img_alt input92p" value="이미지입니다." title="목록이미지의 설명을 입력해주세요." />	
										</td>
									</tr>
									
									<?if ($useFile) {?>
										<?if ($useFileDrag) {?>
										<tr>
											<th scope="row"><label for="">첨부파일</label></th>
											<td colspan="3" id="useFile"><input type="file" id="" name=""
												class="input50p" style="display: inline-block;"
												title="첨부파일을 업로드 해주세요." multiple="multiple"
												onchange="javascript:F_FileMultiUpload(this.files, this);" />
												<div id="dropzone">파일을 올려주세요.</div></td>
										</tr>
										<?
											} else {
										?>
										<tr>
											<th scope="row"><label for="">첨부파일</label></th>
											<td colspan="3" id="useFile">
												<p>
													<input type="file" id="filenames1" name="filenames[]"
														class="input50p" style="display: inline-block;"
														title="첨부파일을 업로드 해주세요." /> <a class="btns gr_btn"
														href="javascript:;" id="addFile">추가</a> <a class="btns gr_btn"
														href="javascript:;" id="delFile">제거</a>
												</p>
											</td>
										</tr>
										<?}?>
									<?}?>
									<? if ($useRelationurl) { ?>
									<tr>
										<th scope="row"><label for="">관련링크</label></th>
										<td colspan="3">
											<input type="text" id="relation_url" name="relation_url" class="input92p" title="" />
										</td>
									</tr>
									<? } ?>
									<? if ($useMovie) { ?>
									<tr>
										<th scope="row"><label for="">동영상</label></th>
										<td colspan="3">
											<p class="he_txt">확장명이 .MP4가 아닌 경우 재생이 안될 수도 있습니다.</p>
											<input type="file" id="moviename" name="moviename" class="input92p" title="첨부파일을 업로드 해주세요." />	
										</td>
									</tr>
									<? } ?>
								</tbody>
							</table>
							
							<input type="hidden" name="cmd" value="write" />
							<input type="hidden" name="category" value="" />
							<? if (!$branch) { ?>
							<input type="hidden" name="branch_fk" id="branch_fk" value="<?=DEFAULT_BRANCH_NO?>"/>
							<?}?>
							<div class="btn">
								<div class="btnLeft">
									<a class="btns gr_btn" href="<?=$common->getQueryString('index.php', 0, $_REQUEST)?>">목록</a>
								</div>
								<div class="btnRight">
									<a class="btns gr_btn" href="javascript:;" onclick="goSave(this);">저장</a>
								</div>
							</div>
							<!--//btn-->
							</form>
						</div>
						<!-- //bread -->
					</div>
					<!-- //bbs --> 
					<!-- 내용 : e -->
				</div>
				<!--//con -->
			</div>
			<!--//content -->
		</div>
		<!--//container --> 
		<!-- E N D :: containerArea-->
	</div>
	<!--//canvas -->
</div>
<!--//wrap -->

</body>
</html>