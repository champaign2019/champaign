<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Common.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/category/Category.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

$_REQUEST['orderby'] = $orderby; //정렬 배열 선언


$objCommon = new Common($pageRows, $tablename, $_REQUEST);
$rowPageCount = $objCommon->getCount($_REQUEST);
$result = $objCommon->getList($_REQUEST);
	$_REQUEST['pcsorts'] = 1;
	$ctlist = Category::categoryList();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
<script>

function groupDelete() {	
	if ( $('[name^=no]:checked').length > 0 ) {
		document.frm.submit();
	} else {
		alert("삭제할 항목을 하나 이상 선택해 주세요.");
	}
}

function goSearch() {
	$("#searchForm").submit();
}


</script>
</head>
<body> 
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<div id="wrap">
	<!-- canvas -->
	<div id="canvas">
		<!-- S T A R T :: headerArea-->
		<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/top.php" ?>
		<!-- E N D :: headerArea--> 
		
		<!-- S T A R T :: containerArea-->
		<div id="container">
			<div id="content">
				<div class="con_tit">
					<h2><?=$pageTitle?> - [목록]</h2>
				</div>
				<!-- //con_tit -->
				<div class="con">
					<!-- 내용 : s -->
					<div id="bbs">
						<div id="blist">
							<p><span><strong>총 <?=$rowPageCount[0]?>개</strong>  |  <?=$objCommon->reqPageNo?>/<?=$rowPageCount[1]?>페이지</span></p>
							<form name="frm" id="frm" action="process.php" method="post">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리목록입니다.">
								<colgroup>
									<col class="w3" />
									<col class="w4" />
									<col class="w8" />
									
									<col class="w10" />
									
									<col class="" />
									
									<?
									if($ismain){
									?>
									<col class="w6" />
									<?
									}
									?>
									<col class="w6" />
									<col class="w10" />
									<?
									if($userCon){
									?>
									<col class="w3" />
									<?
									}
									?>
								</colgroup>
								<thead>
									<tr>
										<th scope="col" class="first">
											<label class="b_first_c"><input type="checkbox" name="allChk" id="allChk" onClick="check(this, document.getElementsByName('no[]'))"/>
											<i></i>
											</label>
										</th>
										<th scope="col">번호</th>
										<th scope="col">분류</th>
										
										<th scope="col">이미지</th>
										 
										<th scope="col">제목</th> 
										
										<?
										if($ismain){
										?>
										<th scope="col">메인노출</th> 
										<?
										}
										?>
										<th scope="col">작성자</th> 
										<th scope="col" class="<?=!$userCon ? "last" : ""?>">작성일</th> 
										<?
										if($userCon){
										?>
										<th scope="col" class="last">조회</th>
										<?
										}
										?>
										
									</tr>
								</thead>
								<tbody>
								<? if ($rowPageCount[0] == 0) { ?>
									<tr>
										<td class="first" colspan="12">등록된 데이터가 없습니다.</td>
									</tr>
								<?
									 } else {
										$targetUrl = "";
										$i = 0;
										while ($row=mysql_fetch_assoc($result)) {
											$targetUrl = "style='cursor:pointer;' onclick=\"location.href='".$objCommon->getQueryString('read.php', $row['no'], $_REQUEST)."'\"";
								?>
									<tr>
										<td class="first">
											<label class="b_nor_c"><input type="checkbox" name="no[]" value="<?=$row[no]?>"/>
											<i></i>
											</label>
										</td>
										<? if ($row['top'] == "1") { ?>
										<td <?=$targetUrl?>>
											<span class="noti_icon">공지</span>
										</td>
										<? } else { ?>
										<td <?=$targetUrl?>><?=$unickNum ? $row['no'] : $rowPageCount[0] - (($objCommon->reqPageNo-1)*$pageRows) - $i?>
										</td>
										<? } ?>
										<td <?=targetUrl?>><?=Category::getCategoryGroupData($row['category'],",") ?></td>
										
										<td <?=$targetUrl?>>
											<? if($listImageResize) {?>
												<?
												if(is_file($_SERVER['DOCUMENT_ROOT'].$uploadPath.$row['listimage'])){
												?>
												<img src="<?=$uploadPath?><?=$row['listimage']?>"/>
												<?
												}else{
												?>
												<img src="/img/no_image.png" alt="이미지입니다." title="이미지입니다." width="130" height="130" />
												<?
												}
												?>	
											<? } else { ?>
											<?
											if(is_file($_SERVER['DOCUMENT_ROOT'].$uploadPath.$row['imagename1'])){
											?>
											<img src="<?=$uploadPath?><?=$row['imagename1']?>" alt="<?=$row['image1_alt']?>" title="<?=$row['image1_alt']?>" width="130" height="130" />
											<?
											}else{
											?>
											<img src="/img/no_image.png" alt="이미지입니다." title="이미지입니다." width="130" height="130" />
											<?
											}
											?>										
											<? } ?>										
										</td>
										
										<td <?=$targetUrl?> class="title">
											
											<?=$row['title']?>
											<? if (checkNewIcon($row['registdate'], $row['newicon'], 1)) { ?>
											<span class="new_icon gre">NEW</span>
											<? } ?>
										</td>
										
										<?
										if($ismain){
										?>
										<td <?=$targetUrl?>><?=getMain($row['main'])?></td>
										<?
										}
										?>
										<td <?=$targetUrl?>><?=$row[name]?></td>
										<td <?=$targetUrl?> class="<?=!$userCon ? "last" : ""?>">
										
										<?
										if($timeDate){
										?>
										<?= getDateTimeFormat($row['registdate']) ?>
										<?
										}else{
										?>
										<?= getYMD($row['registdate']) ?>
										<?
										}
										?> 
										
										</td>
										<?
										if($userCon){
										?>
										<td class="last" <?=$targetUrl?>><?=$row[readno]?></td>
										<?
										}
										?>
									</tr>
								<? $i++;
										}
									 }
								?>
								</tbody>
							</table>
								<input type="hidden" name="cmd" id="cmd" value="groupDelete"/>
								<input type="hidden" name="stype" id="stype" value="<?=$_REQUEST[stype]?>"/>
								<input type="hidden" name="sval" id="sval" value="<?=$_REQUEST[sval]?>"/>
							</form>
							<div class="btn">
								<div class="btnLeft">
									<a class="btns gr_btn" href="javascript:;" onclick="groupDelete();">삭제</a>
								</div>
								<div class="btnRight">
									<a class="wbtn" href="write.php">글쓰기</a>
								</div>
							</div>
							<!--//btn-->
							<!-- 페이징 처리 -->
							<?=pageList($objCommon->reqPageNo, $rowPageCount[1], $objCommon->getQueryString('index.php', 0, $_REQUEST))?>
							<!-- //페이징 처리 -->
							<form name="searchForm" id="searchForm" action="index.php" method="post">
								<div class="search">
									<span id="groupCategory">
										<span>
											<select name="categoryFks" onchange="fn_select_category(this)" data-value="카테고리를 선택해 주세요.">
												<option value="">선택하세요.</option>
												<?
													$categorys = explode(',', $_REQUEST['category']);
													while($row = mysql_fetch_assoc($ctlist)) {
												?>
												<option value="<?=$row['code']?>" <?=getSelected($row['code'], $categorys[0]) ?>><?=$row['orgname']?></option>
												<?
													}
												?>
											</select>
										</span>
									</span>
									<input type="hidden" name="category" value="<?=$_REQUEST["category"] ?>" />
									<script>
										var paramCategoryFk = '<?=$_REQUEST["category"] ?>';
										
										if(paramCategoryFk != ""){
											fn_select_category_value(paramCategoryFk);
										}
									</script>
									<?
									if($ismain){
									?>
									<select name="smain" id="smain" title="메인노출 여부를 선택해주세요" onchange="$('#searchForm').submit();">
										<option value="" <?=getSelected($_REQUEST[smain], "") ?>>메인노출 여부</option>
										<option value="1" <?=getSelected($_REQUEST[smain], "1") ?>>메인노출함</option>
										<option value="2" <?=getSelected($_REQUEST[smain], "2") ?>>메인노출 안 함</option>
									</select>
									<?
									}
									?>
									<select name="stype" title="검색을 선택해주세요">
										<option value="all" <?=getSelected($_REQUEST[stype], "all") ?>>제목+내용</option>
										<option value="name" <?=getSelected($_REQUEST[stype], "name") ?>>작성자</option>
										<option value="email" <?=getSelected($_REQUEST[stype], "email") ?>>이메일</option>
										<option value="contents" <?=getSelected($_REQUEST[stype], "contents") ?>>내용</option>
									</select>
									<input type="text" name="sval" value="<?=$_REQUEST[sval]?>" title="검색할 내용을 입력해주세요" />
									<input type="submit" class="se_btn " value="검색" />
								</div>
							</form>
							<!-- //search --> 
						</div>
						<!-- //blist -->
					</div>
					<!-- //bbs --> 
					<!-- 내용 : e -->
				</div>
				<!--//con -->
			</div>
			<!--//content -->
		</div>
		<!--//container --> 
		<!-- E N D :: containerArea-->
	</div>
	<!--//canvas -->
</div>
<!--//wrap -->

</body>
</html>