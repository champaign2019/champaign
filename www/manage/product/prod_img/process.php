<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Common.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

$objCommon = new Common($pageRows, $tablename, $_REQUEST);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
</head>
<body>
<?
if (checkReferer($_SERVER["HTTP_REFERER"])) {

	$_REQUEST['uploadPath'] = $_SERVER['DOCUMENT_ROOT'].$uploadPath;
	
	if ($_REQUEST['cmd'] == 'write') {
		//다중업로드파일
		if(!$useFileDrag){
			$_REQUEST = multifileupload('filenames', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, true, $maxSaveSize);
		}
		//다중업로드파일 END			
		$_REQUEST = fileupload('filename', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, true, $maxSaveSize);		// 첨부파일
		$_REQUEST = fileupload('moviename', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 동영상
		$_REQUEST = fileupload('imagename1', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지1
		if(listImageResize && $_REQUEST['imagename1'] != null ) {
			$_REQUEST = fileresizeupload($_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST);	// 목록이미지
		}
		$_REQUEST = fileupload('imagename2', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지2
		$_REQUEST = fileupload('imagename3', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지3
		$_REQUEST = fileupload('imagename4', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지4
		$_REQUEST = fileupload('imagename5', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지5
		$_REQUEST = fileupload('imagename6', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지6
		$_REQUEST = fileupload('imagename7', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지7
		$_REQUEST = fileupload('imagename8', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지8

		$r = $objCommon->insert($_REQUEST);
		if($useFileDrag){
			$id = session_id();
			$_REQUEST['session']   = $id;
			$_REQUEST['common_fk'] = $r;
			$objCommon->updateUploadFiles($_REQUEST);
		}
		if ($r > 0) {
			echo returnURLMsg($objCommon->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), '정상적으로 저장되었습니다.');
		} else {
			echo returnURLMsg($objCommon->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), '요청처리중 장애가 발생하였습니다.');
		}

	} else if ($_REQUEST['cmd'] == 'edit') {
		//다중업로드파일
		$_FILES['filenames'] = $_FILES['filenames'];
		$_REQUEST['unos'] = array();
		foreach($_FILES as $key => $val){ 
			if(strpos($key, '|filename') && $val['name']){
				$filename = explode("|", $key);
				if($_FILES['filenames']){
					if($_REQUEST['filename_chk'])
					array_push($_REQUEST['unos'], $_REQUEST['filename_chk']);

					array_push($_REQUEST['unos'], $filename[0]);
					array_push($_FILES['filenames']['name'], $_FILES[$key]['name']);
					array_push($_FILES['filenames']['type'], $_FILES[$key]['type']);
					array_push($_FILES['filenames']['tmp_name'], $_FILES[$key]['tmp_name']);
					array_push($_FILES['filenames']['size'], $_FILES[$key]['size']);
				}else{
					if($_REQUEST['filename_chk'])
					array_push($_REQUEST['unos'], $_REQUEST['filename_chk']);

					array_push($_REQUEST['unos'], $filename[0]);
					$_FILES['filenames']['name'][0] = $_FILES[$key]['name'];
					$_FILES['filenames']['type'][0] = $_FILES[$key]['type'];
					$_FILES['filenames']['tmp_name'][0] = $_FILES[$key]['tmp_name'];
					$_FILES['filenames']['size'][0] = $_FILES[$key]['size'];
				}
			
			}
		}

		if($_FILES['filenames'])
		$_REQUEST = multifileupload('filenames', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, true, $maxSaveSize);
		//다중업로드파일 END
		$_REQUEST = fileupload('filename', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, true, $maxSaveSize);		// 첨부파일
		$_REQUEST = fileupload('moviename', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 동영상
		$_REQUEST = fileupload('imagename1', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지1
		if(listImageResize) {
			$_REQUEST = fileresizeupload($_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST);	// 목록이미지
		}
		$_REQUEST = fileupload('imagename2', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지2
		$_REQUEST = fileupload('imagename3', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지3
		$_REQUEST = fileupload('imagename4', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지4
		$_REQUEST = fileupload('imagename5', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지5
		$_REQUEST = fileupload('imagename6', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지6
		$_REQUEST = fileupload('imagename7', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지7
		$_REQUEST = fileupload('imagename8', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지8
		$r = $objCommon->update($_REQUEST);

		if ($r > 0) {
		
			echo returnURLMsg($objCommon->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), '정상적으로 수정되었습니다.');
		} else {
			echo returnURLMsg($objCommon->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), '요청처리중 장애가 발생하였습니다.');
		}
	}else if ($_REQUEST['cmd'] == 'reply') {
		//다중업로드파일
		$_REQUEST = multifileupload('filenames', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, true, $maxSaveSize);
		//다중업로드파일 END		
		$_REQUEST = fileupload('filename', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, true, $maxSaveSize);		// 첨부파일
		$_REQUEST = fileupload('moviename', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 동영상
		$_REQUEST = fileupload('imagename1', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지1
		$_REQUEST = fileupload('imagename2', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지2
		$_REQUEST = fileupload('imagename3', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지3
		$_REQUEST = fileupload('imagename4', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지4
		$_REQUEST = fileupload('imagename5', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지5
		$_REQUEST = fileupload('imagename6', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지6
		$_REQUEST = fileupload('imagename7', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지7
		$_REQUEST = fileupload('imagename8', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 이미지8

		$r = $objCommon->insertReply($_REQUEST);
		if ($r > 0) {
			echo returnURLMsg($objCommon->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), '정상적으로 저장되었습니다.');
		} else {
			echo returnURLMsg($objCommon->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), '요청처리중 장애가 발생하였습니다.');
		}

	} else if ($_REQUEST['cmd'] == 'groupDelete') {

		$no = $_REQUEST['no'];
		 
		$r = 0;
		for ($i=0; $i<count($no); $i++) {
			$_REQUEST['no'] = $no[$i];

			$r += $objCommon->delete($_REQUEST);
		}

		if ($r > 0) {
			echo returnURLMsg($objCommon->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), '총 '.$r.'건이 삭제되었습니다.');
		} else {
			echo returnURLMsg($objCommon->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), '요청처리중 장애가 발생하였습니다.');
		}

	} else if ($_REQUEST['cmd'] == 'delete') {
		
		$r = $objCommon->delete($_REQUEST);

		if ($r > 0) {
			echo returnURLMsg($objCommon->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), '정상적으로 삭제되었습니다.');
		} else {
			echo returnURLMsg($objCommon->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), '요청처리중 장애가 발생하였습니다.');
		}
	}


} else {
	echo returnURLMsg($objCommon->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), '요청처리중 장애가 발생하였습니다.1');
}
?>
</body>
</html>