<? session_start(); 
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/branch/Branch.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/branch/Type.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

$branch = new Branch($pageRows, $_REQUEST);
$type = new Type($pageRows, $_REQUEST);

$_REQUEST['gubun'] = $gubun;

$rowPageCount = $type->getCount($_REQUEST);
$result = $type->getList($_REQUEST);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
<script>
function groupDelete() {	
	if ( isSeleted(document.frm.no) ){
		document.frm.submit();
	} else {
		alert("삭제할 항목을 하나 이상 선택해 주세요.");
	}
}

function goSave(type) {
	if(type == 'sml'){
		if ($("#insertForm [name=branch_fk]").val()== 0) {
			alert("대분류를 선택해 주십시오.");
			$("#insertForm [name=branch_fk]").focus();
			return false;
		}
		if ($("#insertForm #name").val().length == 0) {
			alert("소분류명을 입력해주십시오.");
			$("#insertForm #name").focus();
			return false;
		}
		$('#insertForm [name=name]:eq(0)').prop("disabled", true);
		$("#insertForm").submit();


	}else if( type ==  'big'){
		if ($("#insertForm #name_big").val().length == 0) {
			alert("대분류명을 입력해주십시오.");
			$("#insertForm #name_big").focus();
			return false;
		}
		$('#insertForm [name=name]:eq(1)').prop("disabled", true);
		$('#insertForm [name=branch_fk]:eq(0)').prop("disabled", true);
		$("#insertForm").submit();

	}else if( type == 'editBig'){
		if ($("#editBig [name=no]").val() == 0) {
			alert("수정 될 대분류명을 선택해 주십시오.");
			$("#editBig [name=no]").focus();
			return false;
		}
		if ($("#editBig [name=name]").val().length == 0) {
			alert("수정 할 대분류명을 입력해주십시오.");
			$("#editBig [name=name]").focus();
			return false;
		}

		$("#editBig").submit();
	}else if( type == 'delBig'){
		if ($("#editBig [name=no]").val() == 0) {
			alert("수정 될 대분류명을 선택해 주십시오.");
			$("#editBig [name=no]").focus();
			return false;
		}
		$("#editBig [name=cmd]").val("deleteBig");
		$("#editBig").submit();
	}
}

function goEdit(no, name, branch_fk) {
	
	if ($("#frm #"+name+"").val().length == 0) {
		alert("대분류명을 선택해주세요.");
		$("#frm #"+name+"").focus();
		return false;
	}
	var tName = $("#frm #"+name+"").val();
	var tHospital_fk = $("#frm #"+branch_fk+"").val();
	location.href="process.php?cmd=edit&no="+no+"&name="+encodeURI(tName)+"&branch_fk="+tHospital_fk;
	
}

</script>
</head>
<body> 
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<div id="wrap">
	<!-- canvas -->
	<div id="canvas">
		<!-- S T A R T :: headerArea-->
		<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/top.php" ?>
		<!-- E N D :: headerArea--> 
		
		<!-- S T A R T :: containerArea-->
		<div id="container">
			<div id="content">
				<div class="con_tit">
					<h2><?=$pageTitle?> - [목록]</h2>
				</div>
				<!-- //con_tit -->
				<div class="con">
					<!-- 내용 : s -->
					<div id="bbs">
						<div id="blist">
							<div class="top_search">
								<h3>대, 소분류 저장</h3>
								<form name="insertForm" id="insertForm" action="process.php" method="post">
								<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리 목록입니다.">
									<colgroup>
										<col class="w10" />
										<col class="w40" />
										<col class="w10" />
										<col class="w40" />
									</colgroup>
									<tbody>
										<tr>
											<th scope="row"><label for="">대분류명</label></th>
											<td colspan=3>
												<input type="text" id="name_big" name="name" value=""  class="input60p"/>
												<a class="blue_btn size02" href="javascript:;" onClick="goSave('big')"><strong>대분류 저장</strong></a>
											</td>
										</tr>
<!-- 										<tr><td colspan=4 style="border:0;"></td></tr> -->
										<tr>
											<th scope="row"><label for="">대분류명</label></th>
											<td>
												<?
												$branch = new Branch(999, $_REQUEST);
												$hResult = $branch->branchSelectProduct($gubun);
												?>
												<select name="branch_fk" id="branch_fk">
													<option value="">대분류선택</option>
												<? while ($row=mysql_fetch_assoc($hResult)) { ?>
												
												<option value="<?=$row['no']?>"><?=$row['name']?></option>
												<? } ?>
												</select>
											</td>
											<th scope="row"><label for="">소분류명</label></th>
											<td>
												<input type="text" id="name" name="name" value=""  class="input60p"/>
												<a class="blue_btn size02" href="javascript:;" onClick="goSave('sml')"><strong>소분류 저장</strong></a>
											</td>
										</tr>
									</tbody>
								</table>
									<input type="hidden" name="cmd" value="write"/>
								</form>
							</div>
							<!--//top_search-->

							<div class="top_search">
								<h3>대분류 수정</h3>
								<form id="editBig" action="process.php" method="post" >
									<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리 목록입니다.">
										<colgroup>
											<col class="w10" />
											<col class="w40" />
											<col class="w10" />
											<col class="w40" />
										</colgroup>
										<tbody>
											<tr>
												<th scope="row"><label for="">수정 될 대분류</label></th>
												<td>
													<?
													$hResult = $branch->branchSelectProduct($gubun);
													?>
													<select name="no" >
														<option value="">대분류선택</option>
														<? while ($row=mysql_fetch_assoc($hResult)) { ?>
												
														<option value="<?=$row['no']?>"><?=$row['name']?></option>
														<? } ?>
													</select>
													<a class="blue_btn size02" href="javascript:;" onClick="goSave('delBig')" style="float:right;" ><strong>대분류 삭제</strong></a>
												</td>
												<th scope="row"><label for="">수정 할 대분류명</label></th>
												<td>
													<input type="text"  name="name" value=""  class="input60p"/>
													<a class="blue_btn size02" href="javascript:;" onClick="goSave('editBig')"><strong>대분류 수정</strong></a>
												</td>
											</tr>
										</tbody>
									</table>
									<input type="hidden" name="cmd" value="editBig" value="대분류 수정"/>
								</form>
							</div>


							<h3>소분류 수정</h3>
							<p><span><strong>총 <?=$rowPageCount[0]?>개</strong>  |  <?=$branch->reqPageNo?>/<?=$rowPageCount[1]?>페이지</span></p>
							
							<form name="frm" id="frm" action="process.php" method="post">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리목록입니다.">
								<colgroup>
									<col class="w5" />
									<col class="w5" />
									<col class="w10" />
									<col class="" />
									<col class="w20" />
									<col class="w10" />
									<col class="w10" />
								</colgroup>
								<thead>
									<tr>
										<th scope="col" class="first">
											<label class="b_first_c"><input type="checkbox" name="allChk" id="allChk" onClick="check(this, document.frm.no)"/><i></i>
											</label>
										</th>
										<th scope="col">번호</th>
										<th scope="col">대분류</th> 
										<th scope="col">소분류</th> 
										<th scope="col">작성일</th> 
										<th scope="col">수정</th> 
										<th scope="col" class="last">삭제</th>
									</tr>
								</thead>
								<tbody>
								<? if ($rowPageCount[0] == 0) { ?>
									<tr>
										<td class="first" colspan="7">등록된 분류이 없습니다.</td>
									</tr>
								<?
									 } else {
										while ($row=mysql_fetch_assoc($result)) {
								?>
									<tr>
										<td class="first">
											<label class="b_nor_c"><input type="checkbox" name="no[]" value="<?=$row[no]?>"/>
											<i></i>
											</label>
										</td>
										<td><?=$row['no']?></td>
										<td>
											<select name="branch_fk" id="branch_fk<?=i?>" title="지점을 선택해주세요" >
												<? 

												$hResult = $branch->branchSelectProduct($gubun);
													
												while ($hrow=mysql_fetch_assoc($hResult)) { ?>
												
												<option value="<?=$hrow['no']?>" <?=getSelected($hrow['no'],$row['branch_fk'])?>><?=$hrow['name']?></option>
												<? } ?>
											</select>
										</td>
										<td><input type="text" name="name<?=$i?>" id="name<?=$i?>" size="70" value="<?=$row['name']?>"/></td>
										<td><?=$row['registdate']?></td>
										<td><a class="blue_btn" href="javascript:;" onclick="goEdit('<?=$row['no']?>', 'name<?=$i?>', 'branch_fk<?=$i?>');">수정</a></td>
										<td class="last"><a class="red_btn" href="javascript:;" onclick="delConfirm('삭제하겠습니까?','process.php?cmd=delete&no=<?=$row['no']?>')">삭제</a></td>
									</tr>
								<?
										}
									 }
								?>
								</tbody>
							</table>
								<input type="hidden" name="cmd" id="cmd" value="groupDelete"/>
							</form>
							<div class="btn">
								<div class="btnLeft">
									<a class="btns" href="javascript:;" onclick="groupGo();">삭제</a>
								</div>
							</div>
							<!--//btn-->
							<form name="searchForm" id="searchForm" action="index.php" method="post">
								<div class="search">
									<select name="sbranch_fk" id="sbranch_fk" title="지점을 선택해주세요" onchange="$('#searchForm').submit();">
									
									<? while ($row=mysql_fetch_assoc($hResult)) { ?>
											
										<option value="<?=$row['no']?>" <?=getSelected($hrow['no'],$_REQUEST['sbranch_fk'])?> ><?=$row['name']?></option>
									<? } ?>
								</select>
								</div>
							</form>
							<!-- //search --> 
						</div>
						<!-- //blist -->
					</div>
					<!-- //bbs --> 
					<!-- 내용 : e -->
				</div>
				<!--//con -->
			</div>
			<!--//content -->
		</div>
		<!--//container --> 
		<!-- E N D :: containerArea-->
	</div>
	<!--//canvas -->
</div>
<!--//wrap -->

</body>
</html>