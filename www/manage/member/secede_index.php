<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/member/Member.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

$member = new Member($pageRows, "member", $_REQUEST);
$rowPageCount = $member->getSecedeCount($_REQUEST);
$result = $member->getSecedeList($_REQUEST);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
<script>
function groupDelete() {	
	if ( $('[name^=no]:checked').length > 0 ) {
		document.frm.submit();
	} else {
		alert("<?=getMsg('alert.text.delete')?>");
	}
}
</script>
</head>
<body> 
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<div id="wrap">
	<!-- canvas -->
	<div id="canvas">
		<!-- S T A R T :: headerArea-->
		<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/top.php" ?>
		<!-- E N D :: headerArea--> 
		
		<!-- S T A R T :: containerArea-->
		<div id="container">
			<div id="content">
				<div class="con_tit">
					<h2><?=$pageTitle?> - [<?=getMsg("th.list")?>]</h2>
				</div>
				<!-- //con_tit -->
				<div class="con">
					<!-- 내용 : s -->
					<div id="bbs">
						<div id="blist">
							<p><span><strong><?=getMsg('th.total')?> <?=$rowPageCount[0]?><?=getMsg('th.amount')?></strong>  |  <?=$member->reqPageNo?>/<?=$rowPageCount[1]?><?=getMsg('th.page')?></span></p>
							<div class="table_wrap">
							<form name="frm" id="frm" action="process.php" method="post">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리목록입니다.">
								<colgroup>
									<col class="w5" />
									<col class="w5" />
									<col class="w20" />
									<col class="w20" />
									<col class="w25" />
									<col class="w25" />
								</colgroup>
								<thead>
									<tr>
										<th scope="col" class="first">
										<label class="b_first_c"><input type="checkbox" name="allChk" id="allChk" onClick="check(this, document.getElementsByName('no[]'))"/>
										<i></i>
										</label>
										</th>
										<th scope="col"><?=getMsg("th.no")?></th>
										<th scope="col">회원명</th> 
										<th scope="col"><?=getMsg('th.id')?></th> 
										<th scope="col">가입일</th> 
										<th scope="col" class="last">탈퇴일</th>
									</tr>
								</thead>
								<tbody>
								<? if ($rowPageCount[0] == 0) { ?>
									<tr>
										<td class="first" colspan="6">탈퇴한 회원이 없습니다.</td>
									</tr>
								<?
									 } else {
										while ($row=mysql_fetch_assoc($result)) {
								?>
									<tr>
										<td class="first">
										<label class="b_nor_c"><input type="checkbox" name="no[]" id="no" value="<?=$row['no']?>"/>
										<i></i>
										</label>
										</td>
										<td><?=$row[no]?></td>
										<td><?=$row[name]?></td>
										<td><?=$row[id]?></td>
										<td><?=$row[registdate]?></td>
										<td class="last"><?=$row[secededate]?></td>
									</tr>
								<?
										}
									 }
								?>
								</tbody>
							</table>
								<input type="hidden" name="cmd" id="cmd" value="groupDeleteSecede"/>
								<input type="hidden" name="stype" id="stype" value="<?=$row['stype']?>"/>
								<input type="hidden" name="sval" id="sval" value="<?=$row['sval']?>"/>
							</form>
							</div>
							<div class="btn">
								<div class="btnLeft">
									<a class="btns" href="#" onclick="groupDelete();"><strong><?=getMsg('btn.delete')?></strong> </a>
								</div>
							</div>
							<!--//btn-->
							<!-- 페이징 처리 -->
							<?=pageList($member->reqPageNo, $rowPageCount[1], $member->getQueryString('secede_index.php', 0, $_REQUEST))?>
							<!-- //페이징 처리 -->
							
							<form name="searchForm" id="searchForm" action="secede_index.php" method="post">
								<div class="search">
									<select name="stype" title="검색을 선택해주세요">
										<option value="all" <?=getSelected($_REQUEST['stype'], "all") ?>><?=getMsg('th.all')?></option>
										<option value="id" <?=getSelected($_REQUEST['stype'], "id") ?>><?=getMsg('th.id')?></option>
										<option value="name" <?=getSelected($_REQUEST['stype'], "name") ?>><?=getMsg('lable.option.name2')?></option>
									</select>
									<input type="text" name="sval" value="<?=$_REQUEST['sval']?>" title="검색할 내용을 입력해주세요" />
									<input type="submit" class="se_btn " value="<?=getMsg('btn.search')?>" />
								</div>
							</form>
							<!-- //search --> 
						</div>
						<!-- //blist -->
					</div>
					<!-- //bbs --> 
					<!-- 내용 : e -->
				</div>
				<!--//con -->
			</div>
			<!--//content -->
		</div>
		<!--//container --> 
		<!-- E N D :: containerArea-->
	</div>
	<!--//canvas -->
</div>
<!--//wrap -->

</body>
</html>