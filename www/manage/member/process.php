<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/member/Member.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/email/SendMail.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

$member = new Member($pageRows, "member", $_REQUEST);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php" ?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
</head>
<body>
<?
if (checkReferer($_SERVER["HTTP_REFERER"])) {
	
	if ($_REQUEST['cmd'] == 'write') {

		$r = $member->insert($_REQUEST);
		if ($r > 0) {
			echo returnURLMsg($member->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.insert'));
		} else {
			echo returnURLMsg($member->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.error'));
		}

	} else if ($_REQUEST['cmd'] == 'edit') {
		$r = $member->update($_REQUEST);
		if ($r > 0) {
			echo returnURLMsg($member->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.update'));
		} else {
			echo returnURLMsg($member->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.error'));
		}

	} else if ($_REQUEST['cmd'] == 'secession') {	// 탈퇴처리
		$delNo = $_REQUEST['no'];
		$data = $member->getData($_REQUEST);
		$no = $member->insertSecede($data);
		if ($no) {
			$r = $member->delete($delNo);
			if ($r > 0) {
				echo returnURLMsg($member->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), '정상적으로 탈퇴되었습니다.');
			} else {
				echo returnURLMsg($member->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), '탈퇴처리를 실패하였습니다.');
			}
		} else {
			echo returnURLMsg($member->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.error'));
		}

	} else if ($_REQUEST['cmd'] == 'groupDelete') {

		$no = $_REQUEST['no'];
		
		$r = 0;
		for ($i=0; $i<count($no); $i++) {
			$r += $member->delete($no[$i]);
		}

		if ($r > 0) {
			echo returnURLMsg($member->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.countdelete',$r));
		} else {
			echo returnURLMsg($member->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.error'));
		}

	} else if ($_REQUEST['cmd'] == 'groupDeleteSecede') {

		$no = $_REQUEST['no'];
		
		$r = 0;
		for ($i=0; $i<count($no); $i++) {
			$r += $member->deleteSecede($no[$i]);
		}

		if ($r > 0) {
			echo returnURLMsg($member->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.countdelete',$r));
		} else {
			echo returnURLMsg($member->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.error'));
		}
	} else if ($_REQUEST['cmd'] == 'dormant') {

		$no = $_REQUEST['no'];
		
		$r = 0;
		for ($i=0; $i<count($no); $i++) {
			//멤버->휴면테이블이동(insert)
			$new_no = $member->changeDormantState($no[$i]);
			
			if($new_no > 0) {
				$_REQUEST['hospital_fk'] = 0;
				$_REQUEST['name'] = '';
				$_REQUEST['tel'] = '';
				$_REQUEST['cell'] = '';
				$_REQUEST['email'] = '';
				$_REQUEST['secession'] = 0;
				$_REQUEST['gender'] = '';
				$_REQUEST['zipcode'] = '';
				$_REQUEST['addr0'] = '';
				$_REQUEST['addr1'] = '';
				$_REQUEST['issms'] = 0;
				$_REQUEST['ismail'] = 0;
				$_REQUEST['birthday'] = '';
				$_REQUEST['birthtype'] = 0;
				$_REQUEST['logindate'] = '';
				$_REQUEST['dormant_email'] = 0;
				$_REQUEST['dormant_state'] = 1;
				$_REQUEST['no'] = $no[$i];
				//멤버테이블 리셋
				$r += $member->resetDormantState($_REQUEST);
			}
		}

		if ($r == count($no)) {
			echo returnURLMsg($member->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), '총 '.$r.'건이 수정되었습니다.');
		} else {
			echo returnURLMsg($member->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.error'));
		}
	} else if ($_REQUEST['cmd'] == 'dormantEmail') {

		$no = $_REQUEST['no'];
		
		$r = 0;
		for ($i=0; $i<count($no); $i++) {
			//1-1. 이메일 주소 가져오기
				$_REQUEST['no'] = $no[$i];
				$data = $member->getData($_REQUEST);

			//1-2. 이메일 보내기
				$title = "[".COMPANY_NAME."] ".$data['name']."님의 휴면계정 알림 안내";
				$mailForm = getURLMakeMailForm(COMPANY_URL, "/include/human_emailForm.php");
				$mailForm = str_replace(":DATE", $data['dormant_date'], $mailForm);
				$sendmail = new SendMail();
				//$sendmail->send(COMPANY_EMAIL, COMPANY_NAME, $data['email'], $title, $mailForm);

			//1-3. 이메일 완료시 상태 업데이트
			if($sendmail->send(COMPANY_EMAIL, COMPANY_NAME, $data['email'], $title, $mailForm)) {
				$r += $member->changeDormantEmail($no[$i]);
			}
		}

		if ($r == count($no)) {
			echo returnURLMsg($member->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), '총 '.$r.'건이 발송되었습니다.');
		} else {
			echo returnURLMsg($member->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.error'));
		}
	} else if ($_REQUEST['cmd'] == 'groupReset') {

		$no = $_REQUEST['no'];
		
		$r = 0;
		for ($i=0; $i<count($no); $i++) {
			$_REQUEST['no'] = $no[$i];
			$data = $member->dormantRead($_REQUEST);
			$data['dormant_state'] = 0;
			$mr = $member->resetDormantStateId($data);

			if($mr > 0) {
				$r += $member->deleteDormant($no[$i]);
			}
		}

		if ($r == count($no)) {
			echo returnURLMsg($member->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'dormant_index.php'), 0, $_REQUEST), '총 '.$r.'건이 복구되었습니다.');
		} else {
			echo returnURLMsg($member->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'dormant_index.php'), 0, $_REQUEST), getMsg('result.text.error'));
		}
	}


} else {
	echo returnURLMsg($admin->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.error'));
}
?>
</body>
</html>