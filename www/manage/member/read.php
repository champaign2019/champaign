<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/member/Member.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/log/Log.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

$member = new Member($pageRows, "member", $_REQUEST);
$_REQUEST['sdormanttype'] = DORMANT_TYPE;
$_REQUEST['orderby'] = $orderby; //정렬 배열 선언
$data = $member->getData($_REQUEST);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
<script>
function del(){
	sure = confirm("삭제하시겠습니까?");
	if (sure) { $("#frm").submit(); }
	else { return false; }
}

function secession(){
	sure = confirm("회원을 탈퇴시키시겠습니까?");
	if (sure) { 
		$("#cmd").val("secession");
		$("#frm").submit();
	}
	else { return false; }
}
</script>
</head>
<body> 
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<div id="wrap">
	<!-- canvas -->
	<div id="canvas">
		<!-- S T A R T :: headerArea-->
		<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/top.php" ?>
		<!-- E N D :: headerArea--> 
		
		<!-- S T A R T :: containerArea-->
		<div id="container">
			<div id="content">
				<div class="con_tit">
					<h2><?=$pageTitle?> - [<?=getMsg('th.detail')?>]</h2>
				</div>
				<!-- //con_tit -->
				<div class="con">
					<!-- 내용 : s -->
					<div id="bbs">
						<div id="bread">
							<div class="table_wrap">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리 기본내용입니다.">
								<colgroup>
									<col width="15%" />
									<col width="35%" />
									<col width="15%" />
									<col width="35%" />
								</colgroup>
								<tbody>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.id')?></label></th>
										<td colspan="3"><?=$data['id']?></td>
									</tr>
									<tr>
										<th scope="row"><label for="">이름</label></th>
										<td colspan="<?=$gender ? 1 : 3?>"><?=$data['name']?></td>
										<?
										if($gender){
										?>
										<th scope="row"><label for="">성별</label></th>
										<td><?=getSexTypeName($data['gender'])?></td>
										<?
										}
										?>
									</tr>
									<tr>
										<th scope="row"><label for="">휴대폰</label></th>
										<td><a href="javascript:openPop('<?=$member->getQueryStringAddParam('/manage/sms/write_pop.php', 0, $_REQUEST, "receiver". $data['cell'])?>','문자보내기',1)"><?=$data['cell']?></a></td>
										<th scope="row"><label for="">SMS 수신</label></th>
										<td><?=getReceiveTypeName($data['issms'])?></td>
									</tr>
									<tr>
										<th scope="row"><label for="">이메일</label></th>
										<td><a href="javascript:openPop('<?=$member->getQueryStringAddParam('/manage/email/write_pop.php', 0, $_REQUEST, "receiveEmail". $data['email'])?>','메일보내기',2)"><?=$data['email']?></a></td>
										<th scope="row"><label for="">이메일 수신</label></th>
										<td><?=getReceiveTypeName($data['ismail'])?></td>
									</tr>
										<?
											if( $useTel ) {
										?>
										<th scope="row"><label for="">연락처</label></th>
										<td colspan="<?=$birsthday ? 1 : 3?>"><?=$data['tel']?></td>
										<?
											}
										?>
										<?
											if($birsthday){
										?>
										<th scope="row"><label for="">생일</label></th>
										<td colspan="<?=$useTel ? 1 : 3?>"><?=$data['birthday']?> (<?=getBirthTypeName($data['birthtype'])?>)</td>
										<?
											}
										?>
	
									<tr>
										<th scope="row"><label for="">우편번호</label></th>
										<td colspan="3"><?=$data['zipcode']?></td>
									</tr>
									<tr>
										<th scope="row"><label for="">주소</label></th>
										<td colspan="3">
											<?=$data['addr0']?><br />
											<?=$data['addr1']?>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for="">회원상태</label></th>
										<td><?=getMemberStateTypeName($data['secession'])?></td>
										<th scope="row"><label for="">가입일</label></th>
										<td ><?=$data['registdate']?></td>
									</tr>
									<? if(DORMANT_TYPE > 0) {?>
									<tr>
										<th scope="row"><label for="">마지막로그인</label></th>
										<td><?=$data['logindate']?></td>
									</tr>
									<tr>
										<th scope="row"><label for="">로그인지난일수</label></th>
										<td><?=$data['dormant_days']." 일"?></td>
										<th scope="row"><label for="">휴면메일발송여부</label></th>
										<td><?=getDormantEmailStateName($data['dormant_email'])?></td>
									</tr>
									<?}?>
								</tbody>
							</table>
							</div>
							<?
								if (IS_LOG && LOG_TYPE == 1) {
									
									$log = new Log('member');				// 방문자 로그 객체
									$ldata = $log->getData($data['no']);	// 해당 데이터의 방문자 로그를 읽어온다.
						 	?>
							<h3>유입경로</h3>
							<div class="table_wrap">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리 기본내용입니다.">
								<colgroup>
									<col width="15%" />
									<col width="35%" />
									<col width="15%" />
									<col width="35%" />
								</colgroup>
								<tbody>
									<tr>
										<th scope="row"><label for="">유입경로</label></th>
										<td>
											<?=$ldata['refname']?>
											<? if ($ldata['refname'] != '북마크') { ?>
											<a href="http://<?=$ldata['refhost']?><?=$ldata['refpage']?><?=$ldata['refparam']?>" target="_brank"><?=$ldata['refhost']?></a>
											<? } ?>
										</td>
										<th scope="row"><label for="">검색어</label></th>
										<td><?=$ldata['refsearch']?></td>
									</tr>
									<tr>
										<th scope="row"><label for="">광고유형</label></th>
										<td><?=$ldata['campaigntext']?></td>
										<th scope="row"><label for="">아이피</label></th>
										<td>
										<? if ($ldata['ip']) { ?>
											<?=$ldata['ip']?> <span class="btnAll" style="top:-5px;">
											<? if ($ldata['sessionid']) { ?>
											<strong class="btn_in inbtn">
											<input type="button" value="과거접속이력" class="btns" onclick="window.open('http://weblog2.vizensoft.com/site/pop_sessionpage.php?sitenum=<?=WEBLOG_NUMBER?>&url=<?=COMPANY_URL?>&sval=<?=$ldata['session']?>', 'past', 'width=800,height=600,scrollbars=yes');" />
											</strong>
											<? } else { ?>
											<strong class="btn_in inbtn">
											<input type="button" value="과거접속이력" class="btns" onclick="window.open('http://weblog2.vizensoft.com/site/pop_sessionpage.php?sitenum=<?=WEBLOG_NUMBER?>&url=<?=COMPANY_URL?>&sval=<?=$ldata['ip']?>', 'past', 'width=800,height=600,scrollbars=yes');" />
											</strong>
											<? } ?>
											</span>
										<? } ?>
										</td>
									</tr>
								</tbody>
							</table>
							</div>
							<? } ?>
							<div class="btn">
								<div class="btnLeft">
									<a class="btns" href="<?=$member->getQueryString('index.php', 0, $_REQUEST)?>"><strong><?=getMsg('btn.list')?></strong></a>
								</div>
								<div class="btnRight">
									<? if($data['secession'] == 1) { ?>
									<a class="btns" href="#" onclick="return secession();" >
										<strong>탈퇴</strong>
									</a>
									<? } ?>
									<a class="btns" href="<?=$member->getQueryString('edit.php', $data['no'], $_REQUEST)?>"><strong><?=getMsg('btn.edit')?></strong></a>
									<a class="btns" href="#" onClick="return del();"><strong><?=getMsg('btn.delete')?></strong></a>
									<form id="frm" name="frm" method="post" action="<?=getSslCheckUrl($_SERVER['REQUEST_URI'], 'process.php')?>" onsubmit="return">
									<fieldset>
									<input type="hidden" name="stype" id="stype" value="<?=$_REQUEST['stype']?>"/>
									<input type="hidden" name="sval" id="sval" value="<?=$_REQUEST['sval']?>"/>
									<input type="hidden" name="sgender" id="sgender" value="<?=$_REQUEST['sgender']?>"/>
									<input type="hidden" name="ssecession" id="ssecession" value="<?=$_REQUEST['ssecession']?>"/>
									<input type="hidden" name="sissms" id="sissms" value="<?=$_REQUEST['sissms']?>"/>
									<input type="hidden" name="sisemail" id="sisemail" value="<?=$_REQUEST['sisemail']?>"/>
									<input type="hidden" name="sdatetype" id="sdatetype" value="<?=$_REQUEST['sdatetype']?>"/>
									<input type="hidden" name="sstartdate" id="sstartdate" value="<?=$_REQUEST['sstartdate']?>"/>
									<input type="hidden" name="senddate" id="senddate" value="<?=$_REQUEST['senddate']?>"/>
									<input type="hidden" name="sendtype" id="sendtype" value="<?=$_REQUEST['sendtype']?>"/>
									<input type="hidden" name="no" id="no" value="<?=$data['no']?>"/>
									<input type="hidden" name="registdate" id="registdate" value="<?=$data['registdate']?>"/>
									<input type="hidden" name="name" id="name" value="<?=$data['name']?>"/>
									<input type="hidden" name="id" id="id" value="<?=$data['id']?>"/>
									<input type="hidden" name="cmd" id="cmd" value="groupDelete"/>
									</fieldset>
								</form>
								</div>
							</div>
							<!--//btn-->
						</div>
						<!-- //bread -->
					</div>
					<!-- //bbs --> 
					<!-- 내용 : e -->
				</div>
				<!--//con -->
			</div>
			<!--//content -->
		</div>
		<!--//container --> 
		<!-- E N D :: containerArea-->
	</div>
	<!--//canvas -->
</div>
<!--//wrap -->

</body>
</html>