<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/member/Member.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

$member = new Member($pageRows, "member", $_REQUEST);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
<script>
function goSave(obj) {
	if(validation(obj)){
		if($("input[name=email1]").val() != "" && $("[name=email2]").val() != ""){
			$("input[name=email]").val($("input[name=email1]").val().trim()+"@"+$("[name=email2]").val().trim());
		}
			
		
		$("#wform").submit();
	}
}

function checkId(){
	var id = $("#id").val();
	window.open ('/member/id_check.php?id='+id, 'newwin' , 'toolbar=0, location=0, status=0, menubar=0, scrollbars=0, resizable=0, top=0, left=0, width=504, height=317');		
}

function searchzipcode(){
	var urlname = "/zipcode/search.php";
	window.open(urlname,"browse_org","height=369,width=506,menubar=no,directories=no,resizable=no,status=no,scrollbars=no");
}
</script>
<?
if(SSL_USE){ 
?>
<!-- 다음우편번호API -->
<script src="https://spi.maps.daum.net/imap/map_js_init/postcode.v2.js"></script>
<?}else{?>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<?}?>
<script>
    function zipcodeapi() {
        new daum.Postcode({
            oncomplete: function(data) {
                // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

                // 각 주소의 노출 규칙에 따라 주소를 조합한다.
                // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
                var fullAddr = ''; // 최종 주소 변수
                var extraAddr = ''; // 조합형 주소 변수

                // 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
                if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
                    fullAddr = data.roadAddress;

                } else { // 사용자가 지번 주소를 선택했을 경우(J)
                    fullAddr = data.jibunAddress;
                }

                // 사용자가 선택한 주소가 도로명 타입일때 조합한다.
                if(data.userSelectedType === 'R'){
                    //법정동명이 있을 경우 추가한다.
                    if(data.bname !== ''){
                        extraAddr += data.bname;
                    }
                    // 건물명이 있을 경우 추가한다.
                    if(data.buildingName !== ''){
                        extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                    }
                    // 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
                    fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
                }

                // 우편번호와 주소 정보를 해당 필드에 넣는다.
                $('#zipcode').val(data.zonecode); //5자리 새우편번호 사용
                $('#addr0').val(fullAddr);

                // 커서를 상세주소 필드로 이동한다.
                $('#addr1').focus();
            }
        }).open();
    }
</script>
</head>
<body> 
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<div id="wrap">
	<!-- canvas -->
	<div id="canvas">
		<!-- S T A R T :: headerArea-->
		<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/top.php" ?>
		<!-- E N D :: headerArea--> 
		
		<!-- S T A R T :: containerArea-->
		<div id="container">
			<div id="content">
				<div class="con_tit">
					<h2><?=$pageTitle?> - [<?=getMsg('th.write')?>]</h2>
				</div>
				<!-- //con_tit -->
				<div class="con">
					<!-- 내용 : s -->
					<div id="bbs">
						<div id="bread">
							<h3>회원가입</h3>
							<form method="post" name="wform" id="wform" action="<?=getSslCheckUrl($_SERVER['REQUEST_URI'], 'process.php')?>">
							<div class="table_wrap">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리 기본내용입니다.">
								<colgroup>
									<col width="15%" />
									<col width="35%" />
									<col width="15%" />
									<col width="35%" />
								</colgroup>
								<tbody>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.id')?></label></th>
										<td colspan="3">
											<input type="text" id="id" name="id" value="" data-value="사용하실 ID를 입력해 주세요." readonly maxlength="10" />
											<a class="blue_btn size02" href="javascript:;" onclick="checkId();">ID 중복확인</a>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.password')?></label></th>
										<td>
											<input type="password" id="password" name="password" value="" data-value="비밀번호를 입력해주세요." />
										</td>
										<th scope="row"><label for="">비밀번호 확인</label></th>
										<td>
											<input type="password" id="password2" name="password2" value="" data-value="비밀번호를 정확히 입력하세요."/>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for="">이름</label></th>
										<td colspan="<?=$gender ? 1 : 3?>">
											<input type="text" id="name" name="name" value="" data-value="이름을 입력하세요." />
										</td>
										<?
										if($gender){
										?>
										<th scope="row"><label for="">성별</label></th>
										<td>
											<input type="radio" id="gender1" name="gender" value="1" title="남자" checked="checked" />
											<label for="gender1">남자</label>&nbsp;
											<input type="radio" id="gender2" name="gender" value="2" title="여자" />
											<label for="gender2">여자</label>
										</td>
										<?
										}
										?>
									</tr>
									<tr>
										<th scope="row"><label for="">휴대폰</label></th>
										<td>
											<input type="text" id="cell" name="cell" value="" maxlength="13" data-value="휴대폰 번호를 입력하세요." onkeyup="isNumberOrHyphen(this);" onblur="cvtUserPhoneNumber(this)" />
											<label class="b_nor_c"><input type="checkbox" id="jphoneok" name="jphoneok" value="1" checked="checked"/><i></i> 수신</label>
										</td>
										<th scope="row"><label for="">이메일</label></th>
										<td>
											<input type="hidden" name="email" id="email" value=""/>
											<input data-value="이메일을 입력하세요." name="email1" id="email1" class="inputEmail" type="text" value="<?=getSplitIdx("", "@", 0)?>" maxlength="70"/><span class="email_txt">@</span>
											<select class="selecEmail" name="email2" id="email2" >
												<option value="">선택하세요</option>
												<?=getOptsEmail(getSplitIdx("", "@", 1))?>
											</select>
											<label class="b_nor_c"><input type="checkbox" id="jemailok" name="jemailok" value="1" checked="checked"/><i></i> 수신
											</label>
										</td>
									</tr>
									<tr>
										<? if( $useTel ) { ?>
										<th scope="row"><label for="">연락처</label></th>
										<td colspan="<?=$birsthday ? 1 : 3?>">
											<input type="text" id="tel" name="tel" value="" maxlength="13" onkeyup="isNumberOrHyphen(this);" onblur="cvtUserPhoneNumber(this)" />
										</td>
										<? } ?>
										<?
										if($birsthday){
										?>
										<th scope="row"><label for="">생일</label></th>
										<td colspan="<?=$useTel ? 1 : 3?>">
											<input type="text" id="birthday" name="birthday" value="" maxlength="13" onkeyup="isNumberOrHyphen(this);cvtDate(this);"/>
											<input type="radio" id="solar" name="birthtype" value="0" checked="checked"/> 양력
											<input type="radio" id="lunar" name="birthtype" value="1"/> 양력
											<p>ex)910823</p>
										</td>
										<?
										}
										?>
									</tr>
									<tr>
										<th scope="row"><label for="">우편번호</label></th>
										<td colspan="3">
											<input type="text" id="zipcode" name="zipcode" value="" data-value="우편번호를 선택하세요." maxlength="13" readonly onclick="zipcodeapi();" />
											<a class="blue_btn size02" href="javascript:;" onclick="zipcodeapi();">우편번호검색</a>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for="">주소</label></th>
										<td colspan="3">
											<input type="text" id="addr0" name="addr0" value="" data-value="주소를 입력하세요." maxlength="13" readonly class="w50"/><br/>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for="">상세주소</label></th>
										<td colspan="3">
											<input type="text" id="addr1" name="addr1" value="" maxlength="13" class="w50"/>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for="">탈퇴신청</label></th>
										<td colspan="3">
											<label class="b_snor_r"><input type="radio" id="secession1" name="secession" value="1" title="예" /><i></i> 예</label>
											<label class="b_snor_r"><input type="radio" id="secession0" name="secession" value="0" title="예" checked/><i></i>  아니오</label>
										</td>
									</tr>
								</tbody>
							</table>
		
								<input type="hidden" name="stype" id="stype" value="<?=$_REQUEST['stype']?>"/>
								<input type="hidden" name="sval" id="sval" value="<?=$_REQUEST['sval']?>"/>
								<input type="hidden" name="sgender" id="sgender" value="<?=$_REQUEST['sgender']?>"/>
								<input type="hidden" name="ssecession" id="ssecession" value="<?=$_REQUEST['ssecession']?>"/>
								<input type="hidden" name="sissms" id="sissms" value="<?=$_REQUEST['sissms']?>"/>
								<input type="hidden" name="sisemail" id="sisemail" value="<?=$_REQUEST['sisemail']?>"/>
								<input type="hidden" name="sdatetype" id="sdatetype" value="<?=$_REQUEST['sdatetype']?>"/>
								<input type="hidden" name="sstartdate" id="sstartdate" value="<?=$_REQUEST['sstartdate']?>"/>
								<input type="hidden" name="senddate" id="senddate" value="<?=$_REQUEST['senddate']?>"/>
								<input type="hidden" name="sendtype" id="sendtype" value="<?=$_REQUEST['sendtype']?>"/>
								<input type="hidden" name="sendtype" id="sendtype" value="<?=$_REQUEST['sendtype']?>"/>

								<input type="hidden" name="cmd" id="cmd" value="write"/>
							</div>
							<div class="btn">
								<div class="btnLeft">
									<a class="btns" href="<?=$member->getQueryString('index.php', 0, $_REQUEST)?>"><strong><?=getMsg('btn.list')?></strong></a>
								</div>
								<div class="btnRight">
									<a class="btns" href="#" onclick="goSave(this);"><strong><?=getMsg('btn.save')?></strong></a>
								</div>
							</div>
							</form>
							<!--//btn-->
						</div>
						<!-- //bread -->
					</div>
					<!-- //bbs --> 
					<!-- 내용 : e -->
				</div>
				<!--//con -->
			</div>
			<!--//content -->
		</div>
		<!--//container --> 
		<!-- E N D :: containerArea-->
	</div>
	<!--//canvas -->
</div>
<!--//wrap -->

</body>
</html>