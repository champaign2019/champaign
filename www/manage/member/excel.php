<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/member/Member.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/branch/Branch.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

$member = new Member(99999, "member", $_REQUEST);
$rowPageCount = $member->getCount($_REQUEST);
$result = $member->getList($_REQUEST);

$today 	  = getToday();													// 오늘날짜
$pDay = getDayDateAdd(-1, $_REQUEST['sstartdate']);	// 어제
$nDay = getDayDateAdd(1, $_REQUEST['senddate']);		// 내일
$oneWeek 	  = getDayDateAdd(-7, $today);										// 일주일
$pWeek = getDayDateAdd(-6, $pDay);			// 전주
$tWeeks = getThisWeek($today);				// 금주
$nWeek = getDayDateAdd(6, $nDay);				// 차주
$oneMonth	  = getMonthDateAdd(-1, $today);									// 한달
$fDay   = preg_replace('/([0-9]{4}-[0-9]{2})-[0-9]{2}/im', '$1-01', getDayDateAdd(1, $pDay));// 해당월 초일
$pMonth = getMonthDateAdd(-1, $fDay);													// 전월 첫날
$tMonth = preg_replace("/([0-9]{4}-[0-9]{2})-[0-9]{2}/im", "$1-01",getToday());			// 해당월 초일
$nMonth = getMonthDateAdd(1, $fDay);													// 익월 첫날
$pLastDay = lastDay($pMonth);		// 전월 마지막 날
$tLastDay = lastDay($tMonth);		// 금월 마지막 날
$nLastDay = lastDay($nMonth);		// 익월 마지막 날


// 엑셀
header( "Content-type: application/vnd.ms-excel; charset=utf-8");
header( "Content-Disposition: attachment; filename = excel.xls" );
header( "Content-Description: PHP4 Generated Data" );

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
<style type="text/css">
.w5{width:5%;}
.w8{width:8%;}
.w10{width:10%;}
th, td{border:1px solid black; text-align: center;}
th{background-color: #C0C0C0; height: 30px;}
tr{height: 28px;}
</style>
</head>
<body> 
<div id="wrap">
	<!-- canvas -->
	<div id="canvas">
		
		<!-- S T A R T :: containerArea-->
		<div id="container">
			<div id="content">
				<!-- //con_tit -->
				<div class="con">
					<!-- 내용 : s -->
					<div id="bbs">
						<div id="blist">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리목록입니다.">
								<colgroup>
									<col class="w4" />
									<col class="w7" />
									<col class="w7" />
									<col class="w10" />
									<col class="w10" />
									<col class="w6" />
									<col class="w6" />
									<col class="w6" />
									<col class="w6" />
									<col class="w6" />
									<col class="w6" />
									<col class="w4" />
								</colgroup>
								<thead>
									<tr>
										<th scope="col">번호</th>
										<th scope="col">회원명</th> 
										<th scope="col">아이디</th> 
										<th scope="col">휴대폰</th> 
										<th scope="col">이메일</th>
										<th scope="col">회원상태</th> 
										<th scope="col">sms수신</th> 
										<th scope="col">email수신</th> 
										<!--<th scope="col">상담횟수</th> 
										<th scope="col">예약횟수</th>--> 
										<th scope="col" class="last">가입일</th>
									</tr>
								</thead>
								<tbody>
								<? if ($rowPageCount[0] == 0) { ?>
									<tr>
										<td class="first" colspan="10">등록된 회원이 없습니다.</td>
									</tr>
								<?
									 } else {
										$targetUrl = "";
										$smsPop = "";
										$emailPop = "";
										while ($row=mysql_fetch_assoc($result)) {
											$targetUrl = "style='cursor:pointer;' onclick=\"location.href='".$member->getQueryString('read.php', $row['no'], $_REQUEST)."'\"";
											$smsPop = "style='cursor:pointer;' onclick=\"location.href='".$member->getQueryStringAddParam('/manage/sms/write.php', $row['no'], $_REQUEST, "receiver", $row['cell'])."'\"";
											$emailPop = "style='cursor:pointer;' onclick=\"location.href='".$member->getQueryStringAddParam('/manage/email/write.php', $row['no'], $_REQUEST, "receiveEmail", $row['email'])."'\"";
								?>
									
									<tr>
										<td <?=$targetUrl?>><?=$row['no']?></td>
										<td <?=$targetUrl?>><?=$row['name']?></td>
										<td <?=$targetUrl?>><?=$row['id']?></td>
										<td class="helpComment" id="Smsh" <?=$smsPop?>><?=$row['cell']?></td>
										<td class="helpComment" id="Emailh" <?=$emailPop?>><?=$row['email']?></td>
										<td <?=$targetUrl?>><?=getMemberStateTypeName($row['secession'])?></td>
										<td <?=$targetUrl?>><?=getReceiveTypeName($row['issms'])?></td>
										<td <?=$targetUrl?>><?=getReceiveTypeName($row['ismail'])?></td>
										<!--<td <?=$targetUrl?>><?=$row['consultnum']?>&nbsp;회</td>
										<td <?=$targetUrl?>><?=$row['resernum']?>&nbsp;회</td>-->
										<td <?=$targetUrl?> class="last"><?=$row['registdate']?></td>
									</tr>
								<?
										}
									 }
								?>
								</tbody>
							</table>
						</div>
						<!-- //blist -->
					</div>
					<!-- //bbs --> 
					<!-- 내용 : e -->
				</div>
				<!--//con -->
			</div>
			<!--//content -->
		</div>
		<!--//container --> 
		<!-- E N D :: containerArea-->
	</div>
	<!--//canvas -->
</div>
<!--//wrap -->

</body>
</html>