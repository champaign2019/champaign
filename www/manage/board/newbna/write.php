<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Common.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Hospital.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Clinic.class.php";
include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

$_REQUEST['category_tablename'] = $category_tablename;
$common = new Common($pageRows, $tablename, $_REQUEST);

if($category_tablename){ 
	
	$category_result = $common->getCategoryList($_REQUEST);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/boardConfig/config.php" ?>

<script>
	var isSubmitChk = false;
	$(window).load(function(){
		$('#registdate').datetimepicker({timepicker:false,format: "Y-m-d"});
		$("#button").click(function(){
			$('#registdate').datetimepicker('show');
		})			
		<?
		if($registdateGubun){
		?>
		// 달력
		//initCal({id:"registdate",type:"day",today:"y",timeYN:"y"});
		<?
		}
		?>
		fn_addDel_object(<?=$useFileCount?>); //파일 function 생성
	});
	
	function goSave(obj) {
		
		if(validation(obj)){
			isSubmitChk = true;
			$("#frm").submit();	
			
		}
			
	}
	
</script>
</head>
<body> 
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<div id="wrap">
	<!-- canvas -->
	<div id="canvas">
		<!-- S T A R T :: headerArea-->
		<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/top.php" ?>
		<!-- E N D :: headerArea--> 
		
		<!-- S T A R T :: containerArea-->
		<div id="container">
			<div id="content">
				<div class="con_tit">
					<h2><?=$pageTitle?> - [<?=getMsg('th.write')?>]</h2>
				</div>
				<!-- //con_tit -->
				<div class="con">
					<!-- 내용 : s -->
					<div id="bbs">
						<div id="bread">
							
							<form method="post" name="frm" id="frm" action="<?=getSslCheckUrl($_SERVER['REQUEST_URI'], 'process.php')?>" enctype="multipart/form-data" >
							<div class="table_wrap">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리 기본내용입니다.">
								<colgroup>
									<col width="15%" />
									<col width="35%" />
									<col width="15%" />
									<col width="35%" />
								</colgroup>
								<tbody>
									<? if($category_tablename){?>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.category')?></label></th>
										<td colspan="3">
											<select name="category_fk" id="category_fk" data-value="<?=getMsg('alert.text.category')?>">
												<? while ($row=mysql_fetch_assoc($category_result)) { ?>
												<option value="<?=$row[no]?>"/><?=$row[name]?></option>
												<? } ?>
											</select>
										</td>
									</tr>
									<? } ?>

									<? 
									if ($branch) { 
										
										$hospital = new Hospital(999, $_REQUEST);
										$hResult = $hospital->branchSelect();
									?>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.hospital_fk')?></label></th>
										<td colspan="3">
											<select name="hospital_fk" id="hospital_fk" title="지점 선택" data-value="<?=getMsg('alert.text.shospital_fk')?>">
												<option value=""><?=getMsg('th.branch_sel')?></option>
												<? while ($row=mysql_fetch_assoc($hResult)) { ?>
												
												<option value="<?=$row['no']?>"><?=$row['name']?></option>
												<? } ?>
											</select>
										</td>
									</tr>
									<? 
									}
									?>
									
									<?
									if($clinic){
										
										$clinicObj = new Clinic(999, $_REQUEST);
										$clinicList = $clinicObj->selectBoxList(0,($branch ? 0 : DEFAULT_BRANCH_NO),$_REQUEST['sclinic_fk']);
									?>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.clinic_fk')?></label></th>
										<td colspan="3">
											<select name="clinic_fk" id="clinic_fk" title="진료과목 선택" data-value="<?=getMsg('alert.text.sclinic_fk')?>">
												<?= $clinicList ?>
											</select>
										</td>
									</tr>
									<?
									}
									?>
									
									
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.name')?></label></th>
										<td colspan="<?=$useEmail ? 1 : 3?>">
											<input type="text" name="name" id="name" class="w100m25" value="<?=$_SESSION['admin_name']?>" data-value="<?=getMsg('alert.text.name')?>"/>
										</td>
										<?
										if($usePassword){
										?>
										<th scope="row"><label for=""><?=getMsg('th.password')?></label></th>
										<td>
											<input type="password" id="password" name="password" class="w100m25" title="비밀번호를 입력해주세요" data-value="<?=getMsg('alert.text.password')?>" />
										</td>
										<?
										}
										?>
									</tr>
									<?
									if($useCell || $useEmail){
									?>
									<tr>
										<?
										if($useCell){
										?>
										<th scope="row"><label for=""><?=getMsg('th.cell')?></label></th>
										<td colspan="<?=$useEmail ? 1 : 3?>">
											<input type="text" id="cell" name="cell" value="" class="w100m25" title="휴대폰 번호를 입력해주세요." onkeyup="isOnlyNumberNotHypen(this);"/>
										</td>
										<?
										}
										?>
										<?
										if($useEmail){
										?>
										<th scope="row"><label for=""><?=getMsg('th.email')?></label></th>
										<td colspan="<?=useCell ? 1 : 3?>">
											<input type="text" id="email" name="email" value="" title="이메일주소를 입력해주세요." onkeyup="isValidEmailing(this);" class="w100m25"/>
										</td>
										<?
										}
										?>
									</tr>
									<?
									}
									?>
									<? if($usePerfect || $registdateGubun) { ?>
									<tr>
										<?
										if($usePerfect){
										?>
										<th scope="row"><label for=""><?=getMsg('th.userCon')?></label></th>
										<td colspan="<?=$registdateGubun ? 1 : 3?>">
											<input type="text" id="readno" name="readno" title="조회수를 입력해주세요" style="width:50px;" onkeyup="isOnlyNumber(this);"/>
										</td>
										<?
										}
										?>
										<?
										if($registdateGubun){
										?>
										<th scope="row"><label for=""><?=getMsg('th.registdate')?></label></th>
										<td colspan="<?=$usePerfect ? 1 : 3?>">
											<input type="text" name="registdate" id="registdate" value=""/>
											<span id="CalregistdateIcon">
											<button type="button" id="button"><img src="/manage/img/calendar_icon.png" id="CalregistdateIconImg" style="cursor:pointer;"/></button>
											</span>
										</td>
										<?
										}
										?>
									</tr>
									<? } ?>
									
									<?
									if($istop || $isnew){
									?>
									<tr>
										<?
										if($istop){
										?>
										<th scope="row"><label for=""><?=getMsg('th.topnotice')?></label></th>
										<td colspan="<?=$istop ? 1 : 3?>">
											<label for="top1" class="b_snor_r"><input type="radio" id="top1" name="top" value="1" data-value="<?=getMsg('alert.text.topnotice')?>"/>
											<i></i>
											<span class="blue_col vaM"><?=getMsg('lable.radio.notice_y')?></span></label>
											<label for="top0" class="b_snor_r marl15"><input type="radio" id="top0" name="top" value="0" checked data-value="<?=getMsg('alert.text.topnotice')?>"/>
											<i></i><span class="red_col vaM">
											<?=getMsg('lable.radio.notice_n')?></span></label>
											<img src="/manage/img/question_btn.gif" class="helpComment" id="Top" alt="도움말 이미지"/>
										</td>
										<?
										}
										?>
										<?
										if($isnew){
										?>
										<th scope="row"><label for=""><?=getMsg('th.newicon')?></label></th>
										<td colspan="<?=$istop ? 1 : 3?>">
											<label for="newicon1" class="b_snor_r"><input type="radio" id="newicon1" name="newicon" value="1" data-value="NEW아이콘을 선택해 주세요."/>
											<i></i><span class="blue_col vaM">
											<?=getMsg('lable.radio.always')?></span></label>
											<label for="newicon2" class="b_snor_r marl15"><input type="radio" id="newicon2" name="newicon" value="2" data-value="NEW아이콘을 선택해 주세요."/>
											<i></i><span class="gre_col vaM">
											<?=getMsg('lable.radio.oneday')?></span></label>
											<label for="newicon0" class="b_snor_r marl15"><input type="radio" id="newicon0" name="newicon" value="0" checked data-value="NEW아이콘을 선택해 주세요."/>
											<i></i><span class="red_col vaM">
											<?=getMsg('lable.radio.noicon')?></span></label>
											<img src="/manage/img/question_btn.gif" class="helpComment" id="New" alt="도움말 이미지"/>
										</td>
										<?
										}
										?>
									</tr>
									<?
									}
									?>
									<?
									if($ismain){
									?>
									<tr>
										
										<th scope="row"><label for=""><?=getMsg('th.maincontents')?></label></th>
										<td colspan="3">
											<label for="main1" class="b_snor_r"><input type="radio" id="main1" name="main" value="1" style="width:14px" data-value="메인게시물을 선택해 주세요."/>
											<i></i><span class="blue_col vaM">
											<?=getMsg('lable.radio.main_y')?></span></label>
											<label for="main0" class="b_snor_r marl15"><input type="radio" id="main0" name="main" value="0" checked style="width:14px" data-value="메인게시물을 선택해 주세요."/>
											<i></i><span class="red_col vaM">
											<?=getMsg('lable.radio.main_n')?></span></label>
											<img src="/manage/img/question_btn.gif" class="helpComment" id="Main" alt="도움말 이미지"/>		
										</td>
																				
									</tr>
									<?
									}
									?>
									<tr>
										<td colspan="4" height="0" class="bline"></td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.title')?></label></th>
										<td colspan="3">
											<input type="text" id="title" name="title" class="input92p" title="제목을 입력해주세요" data-value="<?=getMsg('alert.text.title')?>" />	
										</td>
									</tr>
									<tr>
										<td colspan="4">
											
											<textarea id="contents" name="contents" title="내용을 입력해주세요" style="width:100%;" data-value="<?=getMsg('alert.text.contents')?>"></textarea>	
										</td>
									</tr>
									
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.before_img')?></label></th>
										<td colspan="3">
											<input type="file" id="imagename1" name="imagename1" class="input92p" title="치료전이미지를 업로드 해주세요." data-value="치료전 이미지를 선택해 주세요."/>	
										</td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.before_alt')?></label></th>
										<td colspan="3">
											<input type="text" id="image1_alt" name="image1_alt" class="img_alt input92p" value="치료 전 사진입니다." title="치료전이미지의 설명을 입력해주세요." />	
										</td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.after_img')?></label></th>
										<td colspan="3">
											<input type="file" id="imagename2" name="imagename2" class="input92p" title="치료후이미지를 업로드 해주세요."  data-value="치료후 이미지를 선택해 주세요."/>	
										</td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.after_alt')?></label></th>
										<td colspan="3">
											<input type="text" id="image2_alt" name="image2_alt" class="img_alt input92p" value="치료 후 사진입니다." title="치료후이미지의 설명을 입력해주세요." />	
										</td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.after_img2')?></label></th>
										<td colspan="3">
											<input type="file" id="imagename3" name="imagename3" class="input92p" title="치료전이미지를 업로드 해주세요."/>	
										</td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.before_alt2')?></label></th>
										<td colspan="3">
											<input type="text" id="image3_alt" name="image3_alt" class="img_alt input92p" value="치료 전 사진입니다." title="치료전이미지의 설명을 입력해주세요." />	
										</td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.after_img2')?></label></th>
										<td colspan="3">
											<input type="file" id="imagename4" name="imagename4" class="input92p" title="치료후이미지를 업로드 해주세요."/>	
										</td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.after_alt2')?></label></th>
										<td colspan="3">
											<input type="text" id="image4_alt" name="image4_alt" class="img_alt input92p" value="치료 후 사진입니다." title="치료후이미지의 설명을 입력해주세요." />	
										</td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.before_img3')?></label></th>
										<td colspan="3">
											<input type="file" id="imagename5" name="imagename5" class="input92p" title="치료전이미지를 업로드 해주세요."/>	
										</td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.before_alt3')?></label></th>
										<td colspan="3">
											<input type="text" id="image5_alt" name="image5_alt" class="img_alt input92p" value="치료 전 사진입니다." title="치료전이미지의 설명을 입력해주세요." />	
										</td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.after_img3')?></label></th>
										<td colspan="3">
											<input type="file" id="imagename6" name="imagename6" class="input92p" title="치료후이미지를 업로드 해주세요."/>	
										</td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.after_alt3')?></label></th>
										<td colspan="3">
											<input type="text" id="image6_alt" name="image6_alt" class="img_alt input92p" value="치료 후 사진입니다." title="치료후이미지의 설명을 입력해주세요." />	
										</td>
									</tr>

									<?if ($useFile) {?>
										<?if ($useFileDrag) {?>
										<tr>
											<th scope="row"><label for=""><?=getMsg('th.filename')?></label></th>
											<td colspan="3" id="useFile"><input type="file" id="" name=""
												class="input50p" style="display: inline-block;"
												title="첨부파일을 업로드 해주세요." multiple="multiple"
												onchange="javascript:F_FileMultiUpload(this.files, this);" />
												<div id="dropzone"><?=getMsg('message.tbody.file')?></div></td>
										</tr>
										<?
											} else {
										?>
										<tr>
											<th scope="row"><label for=""><?=getMsg('th.filename')?></label></th>
											<td colspan="3" id="useFile">
												<p>
													<input type="file" id="filenames1" name="filenames[]"
														class="input50p" style="display: inline-block;"
														title="첨부파일을 업로드 해주세요." /> <a class="btns gr_btn"
														href="javascript:;" id="addFile"><?=getMsg('btn.add')?></a> <a class="btns gr_btn"
														href="javascript:;" id="delFile"><?=getMsg('btn.remove')?></a>
												</p>
											</td>
										</tr>
										<?}?>
									<?}?>
									<? if ($useRelationurl) { ?>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.relation_url')?></label></th>
										<td colspan="3">
											<input type="text" id="relation_url" name="relation_url" class="input92p" title="" />
										</td>
									</tr>
									<? } ?>
									<? if ($useMovie) { ?>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.moviename')?></label></th>
										<td colspan="3">
											<p class="he_txt"><?=getMsg('message.tbody.movie')?></p>
											<input type="file" id="moviename" name="moviename" class="input92p" title="첨부파일을 업로드 해주세요." />	
										</td>
									</tr>
									<? } ?>
								</tbody>
							</table>
							</div>
							<?
							if($useAnswer){
							?>
							<h3 class="minTitle"><?=getMsg('th.answer')?></h3>
							<div class="table_wrap">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리 기본내용입니다.">
								<colgroup>
									<col width="15%" />
									<col width="35%" />
									<col width="15%" />
									<col width="35%" />
								</colgroup>
								<tbody>
									<tr>
										<td colspan="4">
											<textarea id="answer" name="answer" title="내용을 입력해주세요" style="width:100%;"></textarea>	
										</td>
									</tr>
								</tbody>
							</table>
							</div>
							<?
							}
							?>
							<input type="hidden" name="cmd" value="write" />
							<? if (!$branch) { ?>
							<input type="hidden" name="hospital_fk" id="hospital_fk" value="<?=DEFAULT_BRANCH_NO?>"/>
							<?}?>
							<div class="btn">
								<div class="btnLeft">
									<a class="btns gr_btn" href="<?=$common->getQueryString('index.php', 0, $_REQUEST)?>"><?=getMsg('btn.list')?></a>
								</div>
								<div class="btnRight">
									<a class="btns gr_btn" href="#" onclick="goSave(this);"><?=getMsg('btn.save')?></a>
								</div>
							</div>
							<!--//btn-->
							</form>
						</div>
						<!-- //bread -->
					</div>
					<!-- //bbs --> 
					<!-- 내용 : e -->
				</div>
				<!--//con -->
			</div>
			<!--//content -->
		</div>
		<!--//container --> 
		<!-- E N D :: containerArea-->
	</div>
	<!--//canvas -->
</div>
<!--//wrap -->

</body>
</html>