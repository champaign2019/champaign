<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Common.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Hospital.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Clinic.class.php";
include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

$objCommon = new Common($pageRows, $tablename, $_REQUEST);
$category_result = $category_tablename ? $objCommon->getCategoryList($_REQUEST) : null;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/boardConfig/config.php" ?>
<? include 'script.php' ?>

<script>
	var isSubmitChk = false;
	$(window).load(function(){
				
		<?
		if($registdateGubun){
		?>
		// 달력
		initCal({id:"registdate",type:"day",today:"y",timeYN:"y"});
		<?
		}
		?>

		fn_addDel_object(<?=$useFileCount?>); //파일 function 생성
	});
	
	function goSave(obj) {
		
		if(validation(obj)){
			isSubmitChk = true;
			$("#frm").submit();	
			
		}
			
	}
	
</script>
</head>
<body> 
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<div id="wrap">
	<!-- canvas -->
	<div id="canvas">
		<!-- S T A R T :: headerArea-->
		<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/top.php" ?>
		<!-- E N D :: headerArea--> 
		
		<!-- S T A R T :: containerArea-->
		<div id="container">
			<div id="content">
				<div class="con_tit">
					<h2><?=$pageTitle?> - [<?=getMsg('th.write')?>]</h2>
				</div>
				<!-- //con_tit -->
				<div class="con">
					<!-- 내용 : s -->
					<div id="bbs">
						<div id="bread">
							<form method="post" name="frm" id="frm" action="<?=getSslCheckUrl($_SERVER['REQUEST_URI'], 'process.php')?>" enctype="multipart/form-data" >
								<div class="table_wrap">
									<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리 기본내용입니다.">
										<colgroup>
											<col width="15%" />
											<col width="35%" />
											<col width="15%" />
											<col width="35%" />
										</colgroup>
										<tbody>
											<tr>
												<th scope="row"><label for=""><?=getMsg('th.name')?></label></th>
												<td colspan="3">
													<input type="text" name="name" id="name" class="w100m25" value="<?=$_SESSION['admin_name']?>" data-value="<?=getMsg('alert.text.name')?>"/>
												</td>
											</tr>
											<tr>
												<td colspan="4" height="0" class="bline"></td>
											</tr>
											<tr>
												<th scope="row"><label for=""><?=getMsg("th.landing_type")?></label></th>
												<td colspan="3">
													<label class="b_snor_r">
														<input type="radio" name="state" value="1" checked/>
														<i></i>
														<?=getMsg("th.normal_landing")?>
													</label>
													<label class="b_snor_r">
														<input type="radio" name="state" value="2" />
														<i></i>
														<?=getMsg("th.image_landing")?>
													</label>
												</td>
											</tr>
											<tr>
												<th scope="row"><label for=""><?=getMsg("th.order")?></label></th>
												<td colspan="3">
													<input type="text" name="ono" onkeyup="onlyNumber(this);" value="0" />
													<img src="/manage/img/question_btn.gif" class="helpComment" id="OrderExplain"/>
												</td>
											</tr>
											<tr>
												<th scope="row"><label for=""><?=getMsg('th.title')?></label></th>
												<td colspan="3">
													<label class="b_snor_r">
														<input type="radio" name="secret" value="0" checked/>
														<i></i>
														텍스트형
													</label>
													<label class="b_snor_r">
														<input type="radio" name="secret" value="1" />
														<i></i>
														이미지형
													</label><br/>
													<input type="text" id="title" name="title" class="input92p" title="제목을 입력해주세요" />	
													<input type="file" id="imagename1" name="imagename1" class="input92p" title="목록이미지를 업로드 해주세요."/>	
												</td>
											</tr>
											<tr>
												<td colspan="4">
													
													<textarea id="contents" name="contents" title="내용을 입력해주세요" style="width:100%;" ></textarea>	
												</td>
											</tr>
											<tr>
												<th scope="row"><label for=""><?=getMsg('th.filename')?></label></th>
												<td colspan="3" id="useFile">
													<p>
														<input type="file" id="filenames1" name="filenames[]"
															class="input50p" style="display: inline-block;"
															title="첨부파일을 업로드 해주세요." /> 
															<input type="text" class="link w50" placeholder="이미지 링크를 입력해주세요." onkeyup="linkBind();" style="vertical-align:top;" />
															<a class="btns gr_btn"
															href="javascript:;" id="addFileLink"><?=getMsg('btn.add')?></a> <a class="btns gr_btn"
															href="javascript:;" id="delFileLink"><?=getMsg('btn.remove')?></a>
													</p>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
								<input type="hidden" name="relation_url" />
								<input type="hidden" name="cmd" value="write" />
								<? if (!$branch) { ?>
								<input type="hidden" name="hospital_fk" id="hospital_fk" value="<?=DEFAULT_BRANCH_NO?>"/>
								<?}?>
								<div class="btn">
									<div class="btnLeft">
										<a class="btns gr_btn" href="<?=$objCommon->getQueryString('index.php', 0, $_REQUEST)?>"><?=getMsg('btn.list')?></a>
									</div>
									<div class="btnRight">
										<a class="btns gr_btn" href="#" onclick="goSave(this);"><?=getMsg('btn.save')?></a>
									</div>
								</div>
								<!--//btn-->
							</form>
						</div>
						<!-- //bread -->
					</div>
					<!-- //bbs --> 
					<!-- 내용 : e -->
				</div>
				<!--//con -->
			</div>
			<!--//content -->
		</div>
		<!--//container --> 
		<!-- E N D :: containerArea-->
	</div>
	<!--//canvas -->
</div>
<!--//wrap -->

</body>
</html>