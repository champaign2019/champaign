<script>
	var linkBind;
	$(document).ready(function() {
		var stateChange;
		(stateChange = function() {
			if ($('[name=state]:checked').val() == 1) {
				$('[name=contents]').closest('tr').show();
				$('#useFile').closest('tr').hide();
			}
			else {
				$('[name=contents]').closest('tr').hide();
				$('#useFile').closest('tr').show();
			}
		})();
		
		$('[name=state]').change(function() {
			stateChange();
		});
		
		var secretChange;
		(secretChange = function() {
			if ($('[name=secret]:checked').val() == 0) {
				$('[name=title]').show();
				$('[name=imagename1]').hide();
				$('[name=imagename1]').closest('.weidtFile').hide();
			}
			else {
				$('[name=title]').hide();
				$('[name=imagename1]').show();
				$('[name=imagename1]').closest('.weidtFile').show();
			}
		})();
		
		$('[name=secret]').change(function() {
			secretChange();
		});
		
		$("#addFileLink").click(function(){
		    var html = '<p><input type="file" id="filenames'+($('#useFile input[type=file]').size()+1)+'" name="filenames'+($('#useFile input[type=file]').size()+1)+'" class="input50p" style="display:inline-block;" title="첨부파일을 업로드 해주세요." /><input type="text" class="link w50" placeholder="이미지 링크를 입력해주세요." onkeyup="linkBind();" style="vertical-align:top;"/></p>';
		    $("#useFile").append(html);
		    linkBind();
		});

		$("#delFileLink").click(function(){

		    if($("#useFile input[type=file]").size() > 1){
		        if($("#useFile > p:last").attr("class") == 'pre_file'){
		            //var uno = $("#useFile > p:last input[name=filenames_chk]").val();

		        }else{
		            $("#useFile > p:last").remove();
		        }
		    }
		});

		(linkBind = function() {
			var result = '';
			$('.link').each(function(i) {
				if (i > 0) result += ',';
				result += this.value.replace(/,/gi, '&#44;');
			});
			$('[name=relation_url]').val(result);
		})();
	});
</script>