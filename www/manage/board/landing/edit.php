<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Hospital.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Clinic.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Common.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

$objCommon = new Common($pageRows, $tablename, $_REQUEST);
$data = $objCommon->getData($_REQUEST, false);
$category_result = $category_tablename ? $objCommon->getCategoryList($_REQUEST) : null;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/boardConfig/config.php" ?>
<? include 'script.php' ?>
<script>
	$(window).load(function(){
				
		<?
		if($registdateGubun){
		?>
		// 달력
		initCal({id:"registdate",type:"day",today:"y",timeYN:"y"});
		<?
		}
		?>
		fn_addDel_object(<?=$useFileCount?>); //파일 function 생성
	});
	
	function goSave(obj) {
		
		if(validation(obj)){
			
			$("#frm").submit();	
			
		}
			
	}
	
</script>
</head>
<body> 
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<div id="wrap">
	<!-- canvas -->
	<div id="canvas">
		<!-- S T A R T :: headerArea-->
		<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/top.php" ?>
		<!-- E N D :: headerArea--> 
		
		<!-- S T A R T :: containerArea-->
		<div id="container">
			<div id="content">
				<div class="con_tit">
					<h2><?=$pageTitle?> - [<? if($useAnswer){?> 답변&middot; <? } ?>수정]</h2>
				</div>
				<!-- //con_tit -->
				<div class="con">
					<!-- 내용 : s -->
					<div id="bbs">
						<div id="bwrite">
							
							<form method="post" name="frm" id="frm" action="<?=getSslCheckUrl($_SERVER['REQUEST_URI'], 'process.php')?>" enctype="multipart/form-data" >
								<div class="table_wrap">
									<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리 기본내용입니다.">
										<colgroup>
											<col width="15%" />
											<col width="35%" />
											<col width="15%" />
											<col width="35%" />
										</colgroup>
										<tbody>
												<tr>
													<th scope="row"><label for=""><?=getMsg('th.name')?></label></th>
													<td colspan="3">
														<input type="text" name="name" id="name" class="w100m25" value="<?=$_SESSION['admin_name']?>" data-value="<?=getMsg('alert.text.name')?>" value="<?=$data['name'] ?>"/>
													</td>
												</tr>
												<tr>
													<td colspan="4" height="0" class="bline"></td>
												</tr>
												<tr>
													<th scope="row"><label for=""><?=getMsg("th.landing_type")?></label></th>
													<td colspan="3">
														<label class="b_snor_r">
															<input type="radio" name="state" value="1" <?=getChecked($data[state], 1) ?>/>
															<i></i>
															<?=getMsg("th.normal_landing")?>
														</label>
														<label class="b_snor_r">
															<input type="radio" name="state" value="2" <?=getChecked($data[state], 2) ?>/>
															<i></i>
															<?=getMsg("th.image_landing")?>
														</label>
													</td>
												</tr>
												<tr>
													<th scope="row"><label for=""><?=getMsg("th.order")?></label></th>
													<td colspan="3">
														<input type="text" name="ono" onkeyup="onlyNumber(this);" value="<?=$data['ono'] ?>" />
														<img src="/manage/img/question_btn.gif" class="helpComment" id="OrderExplain"/>
													</td>
												</tr>
												<tr>
													<th scope="row"><label for=""><?=getMsg('th.title')?></label></th>
													<td colspan="3">
														<label class="b_snor_r">
															<input type="radio" name="secret" value="0" <?=getChecked($data['secret'], '0') ?>/>
															<i></i>
															텍스트형
														</label>
														<label class="b_snor_r">
															<input type="radio" name="secret" value="1" <?=getChecked($data['secret'], '1') ?>/>
															<i></i>
															이미지형
														</label><br/>
														<input type="text" id="title" name="title" class="input92p" title="제목을 입력해주세요" value="<?=$data['title'] ?>"/>	
														<? if (!$data['imagename1']) { ?>
														<input type="file" name="imagename1" id="imagename1" title="목록이미지를 업로드 해주세요."  class="input92p"/>
														<? } else { ?>
															<div class="weidtFile">
																<p class="pre_file"><img src="/manage/img/file_img.gif" /> <span class="blue_col vaM"><?=$data['imagename1_org']?></span>
																	<label for="imagename1_chk" class="b_nor_c size02 marl15"><input type="checkbox" id="imagename1_chk" name="imagename1_chk" value="1" title="기존이미지를 삭제하시려면 체크해주세요" /><i></i>
																	<?=getMsg('lable.checkbox.image_del')?></label>
																</p>
																<input type="file" name="imagename1" id="imagename1" title="목록이미지를 업로드 해주세요."  class="input92p"/>
															</div>
														<? } ?>		
													</td>
												</tr>
												<tr>
													<td colspan="4">
														
														<textarea id="contents" name="contents" title="내용을 입력해주세요" style="width:100%;" ><?=stripslashes($data['contents']) ?></textarea>	
													</td>
												</tr>
												<tr>
													<th scope="row"><label for=""><?=getMsg('th.filename')?></label></th>
													<td colspan="3" id="useFile">
														<? if (!$data['filename']) {				
														?>
															<input type="file" name="filenames[]" class="input50p" title="첨부파일" style="display:inline-block;" />
															<input type="text" class="link w50" placeholder="이미지 링크를 입력해주세요." onkeyup="linkBind();" style="vertical-align:top;" />
															<a class="btns gr_btn" href="javascript:;" id="addFileLink"><?=getMsg('btn.add')?></a>
															<a class="btns gr_btn" href="javascript:;" id="delFileLink"><?=getMsg('btn.remove')?></a>
															<? } else { 
																$unos = split(",", $data['unos']);
																$filename = split(",", $data['filename']);
																$filename_org = split(",", $data['filename_org']);
																$filesize = split(",", $data['filesize']);
																$rel = split(",", $data['relation_url']);
															?>
																<div class="weidtFile">
																	<?for($i=0;$i<sizeof($filesize);$i++){?>
																	<p><?=getMsg('lable.image_org')?> : <?=$filename_org[$i]?><br />
																		<input type="checkbox" id="" name="filename_chk[]"  value="<?=$unos[$i]?>" title="첨부파일을 삭제하시려면 체크해주세요"  />
																		<label for="filename_chk"><?=getMsg('lable.checkbox.image_del')?></label>
																	</p>
																	<input type="file" name="<?=$unos[$i]?>|filename" class="filename input50p" id="" title="첨부파일을 업로드 해주세요." style="display:inline-block;" />
																	<input type="text" class="link w50" placeholder="이미지 링크를 입력해주세요." onkeyup="linkBind();" style="vertical-align:top;" value="<?=$rel[$i] ?>" />
																	<? if ($i==0) { ?>
																	<a class="btns gr_btn" href="javascript:;" id="addFileLink"><?=getMsg('btn.add')?></a>
																	<a class="btns gr_btn" href="javascript:;" id="delFileLink"><?=getMsg('btn.remove')?></a>
																	<? } ?>
																	<?}?>
																</div>
															<? } ?>											
														</td>
													</td>
												</tr>
										</tbody>
									</table>
								</div>
								<?
								if(!$registdateGubun){
								?>
								<input type="hidden" name="registdate" id="registdate" value="<?=$data['registdate']?>" />
								<?
								}
								?>
								<?
								if(!$userCon){
								?>
								<input type="hidden" name="readno" id="readno" value="<?=$data['readno']?>" />
								<?
								}
								?>
								<input type="hidden" name="relation_url" value="<?=$data['relation_url'] ?>" />
								<input type="hidden" name="cmd" id="cmd" value="edit"/>
								<input type="hidden" name="stype" id="stype" value="<?=$_REQUEST['stype']?>"/>
								<input type="hidden" name="sval" id="sval" value="<?=$_REQUEST['sval']?>"/>
								<input type="hidden" name="no" id="no" value="<?=$data['no']?>"/>
								<input type="hidden" name="reqPageNo" id="reqPageNo" value="<?=$data['reqPageNo']?>"/>
								<? if (!$branch) { ?>
								<input type="hidden" name="hospital_fk" id="hospital_fk" value="<?=$data['hospital_fk']?>"/>
								<?}?>
								<div class="btn">
									<div class="btnLeft">
										<a class="btns" href="<?=$objCommon->getQueryString('index.php', 0, $_REQUEST)?>"><strong><?=getMsg('btn.list')?></strong></a>
									</div>
									<div class="btnRight">
										<a class="btns gr_btn" href="#" onclick="goSave(this);"><?=getMsg('btn.save')?></a>
									</div>
								</div>
								<!--//btn-->
							</form>
						</div>
						<!-- //bread -->
					</div>
					<!-- //bbs --> 
					<!-- 내용 : e -->
				</div>
				<!--//con -->
			</div>
			<!--//content -->
		</div>
		<!--//container --> 
		<!-- E N D :: containerArea-->
	</div>
	<!--//canvas -->
</div>
<!--//wrap -->

</body>
</html>