<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Common.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

$objCommon = new Common($pageRows, $tablename, $_REQUEST);
$_REQUEST['orderby'] = $orderby; //정렬 배열 선언
$data = $objCommon->getData($_REQUEST, $userCon);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
<script type="text/javascript">
	function goDelete() {
		var del = confirm ('<?=getMsg('confirm.text.delete')?>');
		if (del){
			document.location.href = "process.php?no=<?=$data[no]?>&cmd=delete";
		} else {
			return false;
		}
	}
</script>
</head>
<body> 
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<div id="wrap">
	<!-- canvas -->
	<div id="canvas">
		<!-- S T A R T :: headerArea-->
		<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/top.php" ?>
		<!-- E N D :: headerArea--> 
		
		<!-- S T A R T :: containerArea-->
		<div id="container">
			<div id="content">
				<div class="con_tit">
					<h2><?=$pageTitle?> - [<?=getMsg('th.detail')?>]</h2>
				</div>
				<!-- //con_tit -->
				<div class="con">
					<!-- 내용 : s -->
					<div id="bbs">
						<div id="bread">
							
							<div class="table_wrap">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리 기본내용입니다.">
								<colgroup>
									<col width="15%" />
									<col width="35%" />
									<col width="15%" />
									<col width="35%" />
								</colgroup>
								<tbody>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.name')?></label></th>
										<td colspan="3"><?=$data['name']?></td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.registdate')?></label></th>
										<td colspan="3" ><?=getDateTimeFormat($data['registdate'])?></td>
									</tr>
								</tbody>
							</table>
							</div>
							
							<div class="table_wrap">
								<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리 기본내용입니다.">
									<colgroup>
										<col width="15%" />
										<col width="85%" />
									</colgroup>
									<tbody>
										<tr>
											<th scope="row"><label for=""><?=getMsg('th.landing_type')?></label></th>
											<td><?=$data['state']==1?getMsg("th.normal_landing"):getMsg("th.image_landing") ?></td>
										</tr>
										<tr>
											<th scope="row"><label for=""><?=getMsg("th.order")?></label></th>
											<td colspan="3">
												<?=$data['ono'] ?>
											</td>
										</tr>
										<tr>
											<th scope="row"><label for=""><?=getMsg('th.title')?></label></th>
											<td>
												<? if ($data['secret'] == '0') { ?>
												<?=$data['title']?>
												<? } else { ?>
												<img src="<?=$uploadPath.$data['imagename1']?>" />
												<? } ?>
											</td>
										</tr>
										<tr>
											<td colspan="2">
												<? if ( $data['state'] == 1 ) { ?>
												<?=stripslashes($data['contents'])?>
												<? } else { ?>
												<?
													if ($data['filename']) {
														$filename = split(",", $data['filename']);
														$rel = split(",", $data['relation_url']);
														for ($i=0; $i<count($filename); $i++) {
												?>
												<? if ($rel[$i]) { ?>
												<a href="<?=$rel[$i] ?>" target="_blank">
												<? } ?>
												<img src="<?=$uploadPath.$filename[$i] ?>" />
												<? if ($rel[$i]) { ?>
												</a>
												<? } ?>
												<?
														}
													}
												?>
												<? } ?>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="btn">
								<? $_REQUEST[no] = $data[no]; ?>
								<div class="btnLeft">
									<a class="btns" href="<?=$objCommon->getQueryString('index.php', 0, $_REQUEST)?>"><strong><?=getMsg('btn.list')?></strong></a>
								</div>
								<div class="btnRight">
									<? if($useWriteAnswer){?>
									<a class="btns" href="<?=$objCommon->getQueryString('reply.php', $data['no'], $_REQUEST)?>"><strong><?=getMsg('th.answer')?></strong></a>
									<? } ?>
									<a class="btns" href="<?=$objCommon->getQueryString('edit.php', $data['no'], $_REQUEST)?>"><strong><?=getMsg('btn.edit')?></strong></a>
									<a class="btns" href="#" onclick="goDelete();"><strong><?=getMsg('btn.delete')?></strong></a>
								</div>
							</div>
							<!--//btn-->
						</div>
						<!-- //bread -->
					</div>
					<!-- //bbs --> 
					<!-- 내용 : e -->
				</div>
				<!--//con -->
			</div>
			<!--//content -->
		</div>
		<!--//container --> 
		<!-- E N D :: containerArea-->
	</div>
	<!--//canvas -->
</div>
<!--//wrap -->

</body>
</html>