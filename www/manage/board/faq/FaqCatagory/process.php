<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Common.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "../config.php";
$_REQUEST['category_tablename'] = $category_tablename;
$objCommon = new Common($pageRows, $tablename, $_REQUEST);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
</head>
<body>
<?
if (checkReferer($_SERVER["HTTP_REFERER"])) {

	if ($_REQUEST['cmd'] == 'write') {

		$r = $objCommon->insertCategory($_REQUEST);
		if ($r > 0) {
			echo returnURLMsg($objCommon->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.insert'));
		} else {
			echo returnURLMsg($objCommon->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.error'));
		}

	} else if ($_REQUEST['cmd'] == 'edit') {

		$r = $objCommon->updateCategory($_REQUEST);

		if ($r > 0) {
			echo returnURLMsg($objCommon->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.update'));
		} else {
			echo returnURLMsg($objCommon->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.error'));
		}
	} else if ($_REQUEST['cmd'] == 'groupDelete') {

		$no = $_REQUEST['no'];
		
		$r = 0;
		for ($i=0; $i<count($no); $i++) {
			$r += $objCommon->deleteCategory($no[$i]);
		}

		if ($r > 0) {
			echo returnURLMsg($objCommon->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.countdelete',$r));
		} else {
			echo returnURLMsg($objCommon->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.error'));
		}

	} else if ($_REQUEST['cmd'] == 'delete') {

		$no = $_REQUEST['no'];
		
		$r = $objCommon->deleteCategory($no);

		if ($r > 0) {
			echo returnURLMsg($objCommon->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.delete'));
		} else {
			echo returnURLMsg($objCommon->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.error'));
		}
	}


} else {
	echo returnURLMsg($objCommon->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), getMsg('result.text.error'));
}
?>
</body>
</html>