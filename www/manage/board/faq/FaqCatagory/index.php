<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Common.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "../config.php";

$objCommon = new Common($pageRows, $tablename, $_REQUEST);

$_REQUEST['category_tablename'] = $category_tablename;

$rowPageCount = $objCommon->getCategoryCount($_REQUEST);
$category_result = $objCommon->getCategoryList($_REQUEST);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
<script>
function goSave() {
	if($("#wri input[name=name]").val().trim() == ""){
		alert("<?=getMsg('alert.text.categoryname')?>");
		$("#wri input[name=name]").focus();
		return false;
	}
	return true;
}

function groupDelete() {	
	if ( $('[name^=no]:checked').length > 0 ) {
		document.frm.submit();
	} else {
		alert("<?=getMsg('alert.text.delete')?>");
	}
}

function goEdit(no, name) {
	
	if ($("#frm #"+name+"").val().length == 0) {
		alert("<?=getMsg('alert.text.categoryname')?>");
		$("#frm #"+name+"").focus();
		return false;
	}
	var tName = $("#frm #"+name+"").val();
	location.href="process.php?cmd=edit&no="+no+"&name="+encodeURI(tName);
}


</script>
</head>
<body> 
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<div id="wrap">
	<!-- canvas -->
	<div id="canvas">
		<!-- S T A R T :: headerArea-->
		<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/top.php" ?>
		<!-- E N D :: headerArea--> 
		
		<!-- S T A R T :: containerArea-->
		<div id="container">
			<div id="content">
				<div class="con_tit">
					<h2><?=$pageTitle?> - [<?=getMsg("th.list")?>]</h2>
				</div>
				<!-- //con_tit -->
				<div class="con">
					<!-- 내용 : s -->
					<div id="bbs">
						<div id="blist">
							<div class="faqlist">
								<form name="wri" id="wri" action="<?=getSslCheckUrl($_SERVER['REQUEST_URI'], 'process.php')?>" method="post" enctype="multipart/form-data" onsubmit="return goSave();" >
									<fieldset>
										<legend>FAQ분류등록</legend>
										<label>분류명</label>
										<input type="text" name="name" maxlength="20" class="faqcata" />
										<input value="저장" type="submit" class="blue_btn he34" />
									</fieldset>
									<input type="hidden" name="cmd" value="write"/>
								</form>
							</div>						
							
							<p><span><?=getMsg('th.total')?> <?=$rowPageCount[0]?><?=getMsg('th.amount')?></span></p>
							<form name="frm" id="frm" action="process.php" method="post">
							<div class="table_wrap">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리목록입니다.">
								<colgroup>
									<col class="w2" />
									<col class="w3" />
									<col class="" />
									<col class="w20" />
									<col class="w5" />
									<col class="w5" />
								</colgroup>
								<thead>
									<tr>
										<th scope="col" class="first">
											<label class="b_first_c"><input type="checkbox" name="allChk" id="allChk" onClick="check(this, document.getElementsByName('no[]'))"/>
											<i></i>
											</label>
										</th>
										<th scope="col"><?=getMsg("th.no")?></th>
										<th scope="col"><?=getMsg('th.category')?></th>
										<th scope="col"><?=getMsg("th.registdate")?></th> 
										<th scope="col"><?=getMsg('th.edit')?></th> 
										<th scope="col" class="last"><?=getMsg('th.delete')?></th>
									</tr>
								</thead>
								<tbody>
								<? if ($rowPageCount[0] == 0) { ?>
									<tr>
										<td class="first" colspan="11"><?=getMsg('message.tbody.category')?></td>
									</tr>
								<?
									 } else {
										 $i = 0;
										 while ($row=mysql_fetch_assoc($category_result)) {
								?>
									<tr class="faqcat">
										<td class="first">
											<label class="b_nor_c"><input type="checkbox" name="no[]" value="<?=$row[no]?>"/>
											<i></i>
											</label>
										</td>
									
										<td><?=$rowPageCount[0] - (($objCommon->reqPageNo-1)*$pageRows) - $i?></td>
										<td><input type="text" name="name<?=$i?>" id="name<?=$i?>" style="width:90%;" value="<?=$row[name]?>" class="faqcatt"/></td>
										<td><?=$row[registdate]?></td>
										<td><a class="blue_btn" href="#" onclick="goEdit('<?=$row[no]?>', 'name<?=$i?>');"><?=getMsg('btn.edit')?></a></td>
										<td class="last"><a class="red_btn" href="#" onclick="delConfirm('<?=getMsg('confirm.text.delete')?>','process.php?cmd=delete&no=<?=$row[no]?>')"><?=getMsg('btn.delete')?></a></td>


									</tr>
								<?
									$i++;
										}
									 }
								?>
								</tbody>
							</table>
								<input type="hidden" name="cmd" id="cmd" value="groupDelete"/>
							</div>
							</form>
							<div class="btn">
								<div class="btnLeft">
									<a class="btns" href="#" onclick="groupDelete();"><strong><?=getMsg('btn.delete')?></strong> </a>
								</div>
							</div>
							<!--//btn-->
							<!-- 페이징 처리 -->
							<?=pageList($objCommon->reqPageNo, $rowPageCount[1], $objCommon->getQueryString('index.php', 0, $_REQUEST))?>
						</div>
						<!-- //blist -->
					</div>
					<!-- //bbs --> 
					<!-- 내용 : e -->
				</div>
				<!--//con -->
			</div>
			<!--//content -->
		</div>
		<!--//container --> 
		<!-- E N D :: containerArea-->
	</div>
	<!--//canvas -->
</div>
<!--//wrap -->

</body>
</html>