<?
$pageTitle 	 	= "FAQ 게시판";				// 게시판 명
$tablename  		= "faq";					// DB 테이블 명
$category_tablename  	= "faq_category";					// 카테고리 테이블 명
$uploadPath 		= "/upload/faq/";			// 파일, 동영상 첨부 경로
$maxSaveSize	  		= 50*1024*1024;					// 50Mb
$pageRows	  		= 10;							// 보여질 게시글 수
$gradeno		  		= 2;							// 관리자 권한 기준 [지점 사용시]
$usePerfect 		= true; 						//조회수
		
/* 화면 필드 생성 여부 */
$unickNum	  	= false;							// 고유번호 사용유무
$branch	  		= false;							// 지점 사용유무  
$clinic	  		= false;							// 진료과목 지점별 사용유무	
$doctor	  		= false;							// 의료진 사용유무
$category 		= true;
$useMovie  	 	= false;						// 파일 첨부 [사용: true, 사용 안함 : true]

$useRelationurl	= false;						// 관련URL 첨부 [사용: true, 사용 안함 : true]
$useEmail 		= true;							// 이메일 사용유무
$isComment 		= false;							// 댓글		

$istop 			= false;
$ismain 			= false;
$isnew 			= false;

$timeDate	  	= true;						// 날짜 or 날짜 and 시간	
$userCon	  		= true;							// 조회수 카운트 [관리자 페이지:true, 사용자 페이지:true, 조회수:사용여부]
$registdateGubun = true;							//날짜 수정
$useFile  		= false;							// 파일 첨부 [사용: true, 사용 안함 : true]
$textAreaEditor	= true;							// 에디터 사용여부
$editorIgnore= "";								//에디터 미사용 배열 구분자 ex : '|0|1|'	

$useFileCount  	= 10;							// 파일 첨부 가능 갯수
$useFileDrag    = true;

$userAgent = $_SERVER["HTTP_USER_AGENT"];
if($useFileDrag){
    if ( preg_match("/MSIE 6.0[0-9]*/", $userAgent) ) {
        $useFileDrag    = false;
    }else if ( preg_match("/MSIE 7.0*/", $userAgent) ) {
        $useFileDrag    = false;
    }else if ( preg_match("/MSIE 8.0*/", $userAgent) ) {
        $useFileDrag    = false;
    }else if ( preg_match("/MSIE 9.0*/", $userAgent) ) {
        $useFileDrag    = false;
    }else if ( preg_match("/MSIE 10.0*/", $userAgent) ) {
        $useFileDrag    = true;
	}
}

$orderby = Array( "top desc","registdate desc" );					    //order by 배열
?>
