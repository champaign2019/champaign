<script src="https://spi.maps.daum.net/imap/map_js_init/postcode.v2.js"></script>
<script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=a3af3c34ec734976cafac7f23d4c9c6a&libraries=services"></script>
<script>
	var geocoder;
	(initializeMap = function() {
		geocoder = new kakao.maps.services.Geocoder();
	})();
	var result = new Object();
	$(window).load(function() {
		var frm = document.frm;
		getLatLng = function(address, callbackFunc) {
			var result = new Object();
			geocoder.addressSearch(address , function(results, status) {
//				console.log(address, status);
				if (status === kakao.maps.services.Status.OK) {
					result.success = true;
					result.lat = results[0].y;
					result.lng = results[0].x;
				}
				else {
					result.success = false;
					result.lat = '';
					result.lng = '';
				}
				callbackFunc(result);
			});
		};

		$('.go-save').click(function() {
			var address = $('#addr0').val();
			getLatLng(address, function(data) {
//				console.log(data);
				if ( !data.success ) {
					alert('['+address+'] 주소의 좌표를\nKakao 지도에서 가져오지 못했습니다.\n주소를 다시 검색해주세요.');
					return;
				}

				var loc = new function() {
					this.lat = data.lat;
					this.lng = data.lng;
				};
				loc = JSON.stringify(loc).replace(/"/gi, '');
				$('[name=relation_url]').val( loc );

				if ( !validation(frm) )
					return;

				$(frm).submit();
			});
		});
	});

	function zipcodeapi() {
		new daum.Postcode({
			oncomplete: function(data) {
				// 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

				// 각 주소의 노출 규칙에 따라 주소를 조합한다.
				// 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
				var fullAddr = ''; // 최종 주소 변수
				var extraAddr = ''; // 조합형 주소 변수

				// 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
				if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
					fullAddr = data.roadAddress;

				} else { // 사용자가 지번 주소를 선택했을 경우(J)
					fullAddr = data.jibunAddress;
				}

				// 사용자가 선택한 주소가 도로명 타입일때 조합한다.
				if(data.userSelectedType === 'R'){
					//법정동명이 있을 경우 추가한다.
					if(data.bname !== ''){
						extraAddr += data.bname;
					}
					// 건물명이 있을 경우 추가한다.
					if(data.buildingName !== ''){
						extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
					}
					// 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
					fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
				}

				// 우편번호와 주소 정보를 해당 필드에 넣는다.
				$('#zipcode').val(data.zonecode); //5자리 새우편번호 사용
				$('#addr0').val(data.roadAddress);
//				$('#addr0').val(fullAddr);

				$('#title').val(data.jibunAddress ? data.jibunAddress : data.autoJibunAddress);

				// 커서를 상세주소 필드로 이동한다.
				$('#addr1').focus();
			}
		}).open();
	};
</script>