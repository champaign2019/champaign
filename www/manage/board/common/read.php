<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Common.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

$objCommon = new Common($pageRows, $tablename, $_REQUEST);
$_REQUEST['orderby'] = $orderby; //정렬 배열 선언
$data = $objCommon->getData($_REQUEST, $userCon);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
<script type="text/javascript">
	function goDelete() {
		var del = confirm ('<?=getMsg('confirm.text.delete')?>');
		if (del){
			document.location.href = "process.php?no=<?=$data[no]?>&cmd=delete";
		} else {
			return false;
		}
	}
</script>
</head>
<body> 
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<div id="wrap">
	<!-- canvas -->
	<div id="canvas">
		<!-- S T A R T :: headerArea-->
		<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/top.php" ?>
		<!-- E N D :: headerArea--> 
		
		<!-- S T A R T :: containerArea-->
		<div id="container">
			<div id="content">
				<div class="con_tit">
					<h2><?=$pageTitle?> - [<?=getMsg('th.detail')?>]</h2>
				</div>
				<!-- //con_tit -->
				<div class="con">
					<!-- 내용 : s -->
					<div id="bbs">
						<div id="bread">
							
							<div class="table_wrap">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리 기본내용입니다.">
								<colgroup>
									<col width="15%" />
									<col width="35%" />
									<col width="15%" />
									<col width="35%" />
								</colgroup>
								<tbody>
								<? if($category_tablename) { ?>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.category')?></label></th>
										<td colspan="3"><?=$data['category_name']?></td>
									</tr>
								<? } ?>

								<?
									if ($branch) {
								?>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.hospital_fk')?></label></th>
										<td colspan="3"><?=$data['hospital_name']?></td>
									</tr>
								<?
									}
								?>
								<?
									if ($clinic) {
								?>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.clinic_fk')?></label></th>
										<td colspan="3"><?=$data['clinic_name']?></td>
									</tr>
								<?
									}
								?>
									<tr>
										<th scope="row"><label for="">지점명</label></th>
										<td colspan="1"><?=$data['name']?></td>
										<th scope="row"><label for="">도로주소</label></th>
										<td colspan="1"><?=$data['addr0']?></td>
									</tr>
									<tr>
										<th scope="row"><label for="">지번주소</label></th>
										<td colspan="1"><?=$data['title']?></td>
										<th scope="row"><label for="">전화번호</label></th>
										<td colspan="1"><?=$data['cell']?></td>
									</tr>
									<?
										if($useEmail || $useCell){
									?>
									<tr>
										<?
											if($useEmail){
										?>
										<th scope="row"><label for=""><?=getMsg('th.email')?></label></th>
										<td colspan="<?=$useCell ? 1 : 3?>"><a href="javascript:openPop('<?=$objCommon->getQueryStringAddParam('/manage/email/write_pop.php', 0, $_REQUEST, "receiveEmail". $data['email'])?>','메일보내기',2)"><?=$data['email']?></a></td>
										<?
											}
										?>
										<?
											if($useCell){
										?>
										<th scope="row"><label for=""><?=getMsg('th.cell')?></label></th>
										<td colspan="<?=$useEmail ? 1 : 3?>">
										<a href="javascript:openPop('<?=$objCommon->getQueryStringAddParam('/manage/sms/write_pop.php', 0, $_REQUEST, "receiver". $data['cell'])?>','문자보내기',1)"><?=$data['cell']?></a></td>
										<?
											}
										?>
									</tr>
									<?
										}
									?>
									
									
									<tr>
										<th scope="row"><label for="">노출여부</label></th>
										<td colspan="1"><?=getMain($data['main'])?></td>
										<th scope="row"><label for=""><?=getMsg('th.registdate')?></label></th>
										<td colspan="1" ><?=getDateTimeFormat($data['registdate'])?></td>
									</tr>

									<tr>
										<th scope="row"><label for="">고양이용 브라백토 판매점</label></th>
										<td colspan="3">
										<?$out_state=($data['state']==0)?"X":"O"?>
										<?=$out_state?>
										</td>	
									</tr>
									<?
										if($useSmsChk || $useEmailChk){
									?>
									<tr>
										<?
											if($useSmsChk){
										?>
										<th scope="row"><label for=""><?=getMsg('th.telconsult')?></label></th>
										<td colspan="<?=$useEmailChk ? 1 : 3?>"><?=getSmsName($data['iscall'])?></td>
										<?
											}
										?>
										<?
											if($useEmailChk){
										?>
										<th scope="row"><label for=""<?=getMsg('th.answeremail')?></label></th>
										<td colspan="<?=$useSmsChk ? 1 : 3?>"><?=getReceiveTypeName($data['ismail'])?></td>
										<?
											}
										?>
									</tr>
									<?
										}
									?>
									<?
										if($istop || $isnew){
									?>
									<tr>
										<?
											if($istop){
										?>
										<th scope="row"><label for=""><?=getMsg('th.topnotice')?></label></th>
										<td colspan="<?=$isnew ? 1 : 3?>"><?=getTop($data[top])?></td>
										<?
											}
										?>
										<?
											if($isnew){
										?>
										<th scope="row"><label for=""><?=getMsg('th.newicon')?></label></th>
										<td colspan="<?=$istop ? 1 : 3?>"><?=getNewIcon($data['newicon'])?>></td>
										<?
											}
										?>
									</tr>
									<?
										}
									?>
									
								</tbody>
							</table>
							</div>
							
							
							<?
								if($useAnswer && $data['answer']){
							?>
							<h3 class="minTitle"><?=getMsg('th.answer')?></h3>
							<div class="table_wrap">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리 기본내용입니다.">
								<colgroup>
									<col width="100%" />
								</colgroup>
								<tbody>
									<tr>
										<td><?=$data['answer']?></td>
									</tr>
								</tbody>
							</table>
							</div>
							<?
								}
							?>
							<?
								if($islog){
							?>
								<!--웹로그-->
							<?
							}
							?>
							<div class="btn">
								<? $_REQUEST[no] = $data[no]; ?>
								<div class="btnLeft">
									<a class="btns" href="<?=$objCommon->getQueryString('index.php', 0, $_REQUEST)?>"><strong><?=getMsg('btn.list')?></strong></a>
								</div>
								<div class="btnRight">
									<? if($useWriteAnswer){?>
									<a class="btns" href="<?=$objCommon->getQueryString('reply.php', $data['no'], $_REQUEST)?>"><strong><?=getMsg('th.answer')?></strong></a>
									<? } ?>
									<a class="btns" href="<?=$objCommon->getQueryString('edit.php', $data['no'], $_REQUEST)?>"><strong><?=getMsg('btn.edit')?></strong></a>
									<a class="btns" href="#" onclick="goDelete();"><strong><?=getMsg('btn.delete')?></strong></a>
								</div>
							</div>
							<!--//btn-->
							<?
								if ($isComment) {
							?>
								<? include $_SERVER['DOCUMENT_ROOT']."/manage/board/comment/comment.php" ?><!-- 댓글 -->
							<?
								}
							?>	
						</div>
						<!-- //bread -->
					</div>
					<!-- //bbs --> 
					<!-- 내용 : e -->
				</div>
				<!--//con -->
			</div>
			<!--//content -->
		</div>
		<!--//container --> 
		<!-- E N D :: containerArea-->
	</div>
	<!--//canvas -->
</div>
<!--//wrap -->

</body>
</html>