<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Hospital.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Clinic.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Common.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

$objCommon = new Common($pageRows, $tablename, $_REQUEST);
$data = $objCommon->getData($_REQUEST, false);
$category_result = $category_tablename ? $objCommon->getCategoryList($_REQUEST) : null;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/boardConfig/config.php" ?>
<? include "script.php" ?>
<script>
	$(window).load(function(){
		$('#registdate').datetimepicker({timepicker:false,format: "Y-m-d"});
		$("#button").click(function(){
			$('#registdate').datetimepicker('show');
		})				
		<?
		if($registdateGubun){
		?>
		// 달력
		//initCal({id:"registdate",type:"day",today:"y",timeYN:"y"});
		<?
		}
		?>
		fn_addDel_object(<?=$useFileCount?>); //파일 function 생성
	});
</script>
</head>
<body> 
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<div id="wrap">
	<!-- canvas -->
	<div id="canvas">
		<!-- S T A R T :: headerArea-->
		<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/top.php" ?>
		<!-- E N D :: headerArea--> 
		
		<!-- S T A R T :: containerArea-->
		<div id="container">
			<div id="content">
				<div class="con_tit">
					<h2><?=$pageTitle?> - [<? if($useAnswer){?> 답변&middot; <? } ?>수정]</h2>
				</div>
				<!-- //con_tit -->
				<div class="con">
					<!-- 내용 : s -->
					<div id="bbs">
						<div id="bwrite">
							
							<form method="post" name="frm" id="frm" action="<?=getSslCheckUrl($_SERVER['REQUEST_URI'], 'process.php')?>" enctype="multipart/form-data" >
							<div class="table_wrap">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리 기본내용입니다.">
								<colgroup>
									<col width="15%" />
									<col width="35%" />
									<col width="15%" />
									<col width="35%" />
								</colgroup>
								<tbody>
									<? if($category_tablename) { ?>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.category')?></label></th>
										<td colspan="3">
											<select name="category_fk" id="category_fk" data-value="<?=getMsg('alert.text.category')?>">
												<? while ($row=mysql_fetch_assoc($category_result)) { ?>
												<option value="<?=$row[no]?>" <?=getSelected($row[no], $data[category_fk])?>/><?=$row[name]?></option>
												<? } ?>
											</select>
										</td>
									</tr>
									<? } ?>

									<?
									if ($branch) {

										$hospital = new Hospital(999, $_REQUEST);
										$hResult = $hospital->branchSelect();
									?>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.hospital_fk')?></label></th>
										<td colspan="3">
											
											<select name="hospital_fk" id="hospital_fk" title="지점 선택" data-value="<?=getMsg('alert.text.shospital_fk')?>">
												<option value=""><?=getMsg('th.branch_sel')?></option>
												<? while ($row=mysql_fetch_assoc($hResult)) { ?>
												
												<option value="<?=$row['no']?>" <?=getSelected($data['hospital_fk'], $row['no']) ?>><?=$row['name']?></option>
												<? } ?>
											</select>
										
										</td>
									</tr>
									<?
									}
									?>
									<?
									if($clinic){
										
										$clinicObj = new Clinic(999, $_REQUEST);
										$clinicList = $clinicObj->selectBoxList(0,$data['hospital_fk'],$data['clinic_fk']);										
									?>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.clinic_fk')?></label></th>
										<td colspan="3">
											<select name="clinic_fk" id="clinic_fk" data-value="<?=getMsg('alert.text.shospital_fk')?>">
												<?= $clinicList ?>
											</select>
										</td>
									</tr>
									<?
									}
									?>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.name')?></label></th>
										<td colspan="<?=$usePassword ? 1 : 3?>">
											<input type="text" name="name" id="name" class="w100m25" value="<?=$data['name']?>" data-value="<?=getMsg('alert.text.name')?>"/>
										</td>
										<?
										if($usePassword){
										?>
										<th scope="row"><label for=""><?=getMsg('th.password')?></label></th>
										<td>
											<input type="password" id="password" name="password" class="w100m25" title="비밀번호를 입력해주세요" data-value="" />
										</td>
										<?
										}
										?>
									</tr>
									<tr>
										<th scope="row"><label for="">우편번호</label></th>
										<td colspan="1">
											<input type="text" id="zipcode" name="zipcode" value="<?=$data['zipcode']?>" data-value="우편번호를 선택하세요." maxlength="13" readonly onclick="zipcodeapi();" />
											<a class="blue_btn size02" href="javascript:;" onclick="zipcodeapi();">우편번호검색</a>
										</td>
										<th scope="row"><label for="">도로주소</label></th>
										<td colspan="1">
											<input type="text" id="addr0" name="addr0" value="<?=$data['addr0']?>" maxlength="13" class="w50" data-value="주소를 입력하세요." /><br/>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for="">지번주소</label></th>
										<td colspan="1">
											<input type="text" id="title" name="title" value="<?=$data['title']?>" class="w100m25" title="지번주소를 입력하세요." data-value="지번주소를 입력하세요."/>
										</td>
										<th scope="row"><label for="">전화번호</label></th>
										<td colspan="1">
											<input type="text" id="cell" name="cell" value="<?=$data['cell']?>" class="w100m25" title="휴대폰 번호를 입력해주세요." onkeyup="isOnlyNumberNotHypen(this);" data-value="전화번호를 입력하세요."/>
										</td>
										
									</tr>
									
									<tr>
										<th scope="row"><label for="">노출여부</label></th>
										<td colspan="<?=$ismain ? 1 : 3?>">
											<label for="main1" class="b_snor_r"><input type="radio" id="main1" name="main" value="1" style="width:14px" <?=getChecked("1", $data['main'])?> data-value="<?=getMsg('alert.text.maincontents')?>"/>
											<i></i><span class="blue_col vaM">
											<?=getMsg('lable.radio.main_y')?></span></label>
											<label for="main0" class="b_snor_r marl15"><input type="radio" id="main0" name="main" value="0" style="width:14px" <?=getChecked("0", $data['main'])?> data-value="<?=getMsg('alert.text.maincontents')?>"/>
											<i></i><span class="red_col vaM">
											<?=getMsg('lable.radio.main_n')?></span></label>
											<img src="/manage/img/question_btn.gif" class="helpComment" id="Main" alt="도움말 이미지"/>			
										</td>
										<th scope="row"><label for=""><?=getMsg('th.registdate')?></label></th>
										<td colspan="<?=$usePerfect ? 1 : 3?>">
											<input type="text" name="registdate" id="registdate" value="<?=$data['registdate']?>" class="w100m25"/>
											<span id="CalregistdateIcon">
											<button type="button" id="button"><img src="/manage/img/calendar_icon.png" id="CalregistdateIconImg" style="cursor:pointer;"/></button>
											</span>
										</td>
									</tr>

									<tr>
										<th scope="row"><label for="">고양이용 브라백토 판매점</label></th>
										<td colspan="3">
											<input type=checkbox value=1 name="state"> 
										</td>	
									</tr>
									<?
									if($useSmsChk || $useEmailChk){
									?>
									<tr>
										<?
										if($useSmsChk){
										?>
										<th scope="row"><label for=""><?=getMsg('th.telconsult')?></label></th>
										<td colspan="<?=$useSmsChk ? 1 : 3?>">
											<label for="iscall1" class="b_snor_r"><input type="radio" id="iscall1" name="iscall" value="1" <?=getChecked("1", $data['iscall'])?> data-value="<?=getMsg('alert.text.telconsult')?>"/><i></i>
											<span class="blue_col vaM"><?=getMsg('lable.radio.request_y')?></span></label>
											<label for="iscall0" class="b_snor_r marl15"><input type="radio" id="iscall0" name="iscall" value="0" <?=getChecked("0", $data['iscall'])?> data-value="<?=getMsg('alert.text.telconsult')?>"/><i></i><span class="red_col vaM">
											<?=getMsg('lable.radio.request_n')?></span></label>
										</td>
										<?
										}
										?>
										<?
										if($useEmailChk){
										?>
										<th scope="row"><label for=""<?=getMsg('th.answeremail')?></label></th>
										<td colspan="<?=$useEmailChk ? 1 : 3?>">
											<label for="isemail1"  class="b_snor_r"><input type="radio" id="isemail1" name="isemail" value="1" <?=getChecked("1", $data['ismail'])?> data-value="<?=getMsg('alert.text.emailconsult')?>"/><i></i>
											<span class="blue_col vaM"><?=getMsg('lable.radio.request_y')?></span></label>
											<label for="isemail0" class="b_snor_r marl15"><input type="radio" id="isemail0" name="isemail" value="0" <?=getChecked("0", $data['ismail'])?> data-value="<?=getMsg('alert.text.emailconsult')?>"/><i></i><span class="red_col vaM">
											<?=getMsg('lable.radio.request_n')?></span></label>
										</td>
										<?
										}
										?>
									</tr>
									<?
									}
									?>

									<?
									if($istop || $isnew){
									?>
									<tr>
										<?
										if($istop){
										?>
										<th scope="row"><label for=""><?=getMsg('th.topnotice')?></label></th>
										<td colspan="<?=$isnew ? 1 : 3?>">
											<label for="top1" class="b_snor_r"><input type="radio" id="top1" name="top" value="1" <?=getChecked("1", $data['top'])?> data-value="<?=getMsg('alert.text.topnotice')?>"/>
											<i></i>
											<span class="blue_col vaM"><?=getMsg('lable.radio.notice_y')?></span></label>
											<label for="top0" class="b_snor_r marl15"><input type="radio" id="top0" name="top" value="0" <?=getChecked("0", $data['top'])?> data-value="<?=getMsg('alert.text.topnotice')?>"/><i></i><span class="red_col vaM">
											<?=getMsg('lable.radio.notice_n')?></span></label>
											<img src="/manage/img/question_btn.gif" class="helpComment" id="Top" alt="도움말 이미지"/>
										</td>
										<?
										}
										?>
										<?
										if($isnew){
										?>
										<th scope="row"><label for=""><?=getMsg('th.newicon')?></label></th>
										<td colspan="<?=$istop ? 1 : 3?>">
											<label for="newicon1" class="b_snor_r"><input type="radio" id="newicon1" name="newicon" value="1" <?=getChecked("1", $data['newicon'])?> data-value="<?=getMsg('alert.text.newicon')?>"/><i></i><span class="blue_col vaM">
											<?=getMsg('lable.radio.always')?></span></label>
											<label for="newicon2" class="b_snor_r marl15"><input type="radio" id="newicon2" name="newicon" value="2" <?=getChecked("2", $data['newicon'])?> data-value="<?=getMsg('alert.text.newicon')?>"/><i></i> <span class="gre_col vaM">
											<?=getMsg('lable.radio.oneday')?></span></label>
											<label for="newicon0" class="b_snor_r marl15"><input type="radio" id="newicon0" name="newicon" value="0" <?=getChecked("0", $data['newicon'])?> data-value="<?=getMsg('alert.text.newicon')?>"/><i></i><span class="red_col vaM">
											<?=getMsg('lable.radio.noicon')?></span></label>
											<img src="/manage/img/question_btn.gif" class="helpComment" id="New" alt="도움말 이미지"/>
										</td>
										<?
										}
										?>
									</tr>
									<?
									}
									?>
									
									<?
									if($gallery){
									?>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.imagename')?></label></th>
										<td colspan="3">
											<? if (!$data['imagename1']) { ?>
											<input type="file" name="imagename1" id="imagename1" title="목록이미지를 업로드 해주세요."  class="input92p"/>
											<? } else { ?>
												<div class="weidtFile">
													<p class="pre_file"><img src="/manage/img/file_img.gif" /> <span class="blue_col vaM"><?=$data['imagename1_org']?></span>
														<label for="imagename1_chk" class="b_nor_c size02 marl15"><input type="checkbox" id="imagename1_chk" name="imagename1_chk" value="1" title="기존이미지를 삭제하시려면 체크해주세요" /><i></i>
														<?=getMsg('lable.checkbox.image_del')?></label>
													</p>
													<input type="file" name="imagename1" id="imagename1" title="목록이미지를 업로드 해주세요."  class="input92p"/>
												</div>
											<? } ?>											
										</td>
									</tr>									
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.image_alt')?></label></th>
										<td colspan="3"><input type="text" class="img_alt input92p" id="image1_alt" name="image1_alt" value="<?=$data['image1_alt']?>" title="목록이미지의 설명을 입력해주세요." /></td>
									</tr>
									<?
									}
									?>
									
									<? if ($useFile) { ?>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.filename')?></label></th>
										<td colspan="3" id="useFile">
											<? if (!$data['filename']) {				
											?>
												<input type="file" name="filenames[]" class="" title="첨부파일" style="display:inline-block;" />
												<a class="btns gr_btn" href="javascript:;" id="addFile"><?=getMsg('btn.add')?></a>
												<a class="btns gr_btn" href="javascript:;" id="delFile"><?=getMsg('btn.remove')?></a>
												<? } else { 
													$unos = split(",", $data['unos']);
													$filename = split(",", $data['filename']);
													$filename_org = split(",", $data['filename_org']);
													$filesize = split(",", $data['filesize']);
												?>
													<div class="weidtFile">
														<?for($i=0;$i<sizeof($filesize);$i++){?>
														<p><?=getMsg('lable.image_org')?> : <?=$filename_org[$i]?><br />
															<input type="checkbox" id="" name="filename_chk[]"  value="<?=$unos[$i]?>" title="첨부파일을 삭제하시려면 체크해주세요"  />
															<label for="filename_chk"><?=getMsg('lable.checkbox.image_del')?></label>
														</p>
														<input type="file" name="<?=$unos[$i]?>|filename" class="filename" id="" title="첨부파일을 업로드 해주세요." style="display:inline-block;" />
														<?}?>
														<a class="btns gr_btn" href="javascript:;" id="addFile"><?=getMsg('btn.add')?></a>
														<a class="btns gr_btn" href="javascript:;" id="delFile"><?=getMsg('btn.remove')?></a>
<!-- 														<p><?=getMsg('lable.image_org')?> : <?=$data['filename_org']?><br /> -->
<!-- 															<input type="checkbox" id="filename_chk" name="filename_chk" value="1" title="첨부파일을 삭제하시려면 체크해주세요" /> -->
<!-- 															<label for="filename_chk"><?=getMsg('lable.checkbox.image_del')?></label> -->
<!-- 														</p> -->
<!-- 														<input type="file" name="filename" id="filename" title="첨부파일을 업로드 해주세요." /> -->
													</div>
												<? } ?>											
											</td>
										</td>
									</tr>
									<? } ?>
									<? if ($useRelationurl) { ?>
									<tr style="display:none;">
										<th scope="row"></th>
										<td colspan="3">
											<input type="text" id="relation_url" name="relation_url" class="input92p" title="" value="<?=$data['relation_url']?>"/>
										</td>
									</tr>
									<? } ?>
									<? if ($useMovie) { ?>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.moviename')?></label></th>
										<td colspan="3">
											<p class="he_txt"><?=getMsg('message.tbody.movie')?></p>
											<? if (!$data['moviename']) { ?>
											<input type="file" name="moviename" id="moviename" class="input92p" title="동영상" />
											<? } else { ?>
											<p class="pre_file"><img src="/manage/img/file_img.gif" /> <span class="blue_col vaM"><?=$data['moviename_org']?></span>
											<label for="moviename_chk"  class="b_nor_c size02 marl15"><input type="checkbox" id="moviename_chk" name="moviename_chk" value="1" title="동영상을 삭제하시려면 체크해주세요" /><i></i>
											<?=getMsg('lable.checkbox.image_del')?></label></p>
											<input type="file" id="moviename" name="moviename" class="input92p" title="동영상을 업로드 해주세요." />	
											<? } ?>
										</td>
									</tr>
									<? } ?>
								</tbody>
							</table>
							</div>

							<?
							if($useAnswer){
							?>
							<h3 class="minTitle"><?=getMsg('th.answer')?></h3>
							<div class="table_wrap">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리 기본내용입니다.">
								<colgroup>
									<col width="15%" />
									<col width="35%" />
									<col width="15%" />
									<col width="35%" />
								</colgroup>
								<tbody>
									<tr>
										<td colspan="4">
											<textarea id="answer" name="answer" title="내용을 입력해주세요" style="width:100%;"><?=$data['answer']?></textarea>	
										</td>
									</tr>
								</tbody>
							</table>
							</div>
							<?
							}
							?>
							<?
							if($useAnswer && ($useSmsChk || $useEmailChk)){
							?>
							<div class="editfoot">
								<p>* 해당 항목을 체크하시면 상담요청자에게 <?=$useSmsChk ? "문자메세지" : ""?> <?=$useSmsChk && $useEmailChk ? "또는" : "" ?> <?=$useEmailChk ? "이메일이" : ""?> 발송됩니다.</p>
								<?
								if($useEmailChk){
								?>
								<label for="email_chk" class="b_nor_c size02"><input type="checkbox" id="email_chk" name="email_chk" value="1" <?=getChecked("1", $data['ismail'])?> /><i></i>
								<?=getMsg('lable.checkbox.email_chk')?></label>
								<?
								}
								?>
								<?
								if($useSmsChk){
								?>
								<label for="sms_chk" class="b_nor_c size02"><input type="checkbox" id="sms_chk" name="sms_chk" value="1"  <?=getChecked("1", $data['iscall'])?> /> <i></i>
								<?=getMsg('lable.checkbox.sms_chk')?></label>
								<?
								}
								?>
							</div>
							<?
							}
							?>

							<?
							if(!$registdateGubun){
							?>
							<input type="hidden" name="registdate" id="registdate" value="<?=$data['registdate']?>" />
							<?
							}
							?>
							<?
							if(!$userCon){
							?>
							<input type="hidden" name="readno" id="readno" value="<?=$data['readno']?>" />
							<?
							}
							?>
							<input type="hidden" name="cmd" id="cmd" value="edit"/>
							<input type="hidden" name="stype" id="stype" value="<?=$_REQUEST['stype']?>"/>
							<input type="hidden" name="sval" id="sval" value="<?=$_REQUEST['sval']?>"/>
							<input type="hidden" name="no" id="no" value="<?=$data['no']?>"/>
							<input type="hidden" name="reqPageNo" id="reqPageNo" value="<?=$data['reqPageNo']?>"/>
							<? if (!$branch) { ?>
							<input type="hidden" name="hospital_fk" id="hospital_fk" value="<?=$data['hospital_fk']?>"/>
							<?}?>
							<div class="btn">
								<div class="btnLeft">
									<a class="btns" href="<?=$objCommon->getQueryString('index.php', 0, $_REQUEST)?>"><strong><?=getMsg('btn.list')?></strong></a>
								</div>
								<div class="btnRight">
									<a class="btns gr_btn go-save"><?=getMsg('btn.save')?></a>
								</div>
							</div>
							<!--//btn-->
							</form>
						</div>
						<!-- //bread -->
					</div>
					<!-- //bbs --> 
					<!-- 내용 : e -->
				</div>
				<!--//con -->
			</div>
			<!--//content -->
		</div>
		<!--//container --> 
		<!-- E N D :: containerArea-->
	</div>
	<!--//canvas -->
</div>
<!--//wrap -->
<script>
<?if($data['state']==1){?>
$("[name='state']").attr("checked","checked");
<?}?>
</script>
</body>
</html>