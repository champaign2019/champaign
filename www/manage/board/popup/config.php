<?
$pageTitle 		= "팝업관리";
$tablename 		= "popup";
$branch	  		= false;							// 지점 사용유무  
$clinic	  		= false;							// 진료과목 지점별 사용유무	
$doctor	  		= false;							// 의료진 사용유무
$pageRows		= 5;									// 리스트에 보여질 로우 수
$gradeno		= 2;									// 관리자 권한 기준 [지점 사용시]
$uploadPath		= "/upload/popup/";						// 파일, 동영상 첨부 경로
$maxSaveSize	= 50*1024*1024;							// 50Mb
$textAreaEditor	= true;							// 에디터 사용여부
$editorIgnore= "";								//에디터 미사용 배열 구분자 ex : '|0|1|'	
$useFileCount  	= 10;							// 파일 첨부 가능 갯수
$useFileDrag    = false;

// castle적용
//include_once($_SERVER['DOCUMENT_ROOT']."/include/castle.php");
?>

