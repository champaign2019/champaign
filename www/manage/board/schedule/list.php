<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/schedule/Schedule.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

	$param = new Schedule($pageRows, $tablename, $_REQUEST);
	
	$rowPageCount = $param->getCount($_REQUEST);
	$list = $param->getList($_REQUEST);
	
	$notice;
	if($param->reqPageNo == 1){
		$notice = $param->getListTop($_REQUEST);
	}
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
<script>
function groupDelete() {	
	if ( $('[name^=no]:checked').length > 0 ){
		document.frm.submit();
	} else {
		alert("삭제할 항목을 하나 이상 선택해 주세요.");
	}
}

function goSearch() {
	$("#searchForm").submit();
}


</script>
</head>
<body> 
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<div id="wrap">
	<!-- canvas -->
	<div id="canvas">
		<!-- S T A R T :: headerArea-->
		<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/top.php" ?>
		<!-- E N D :: headerArea--> 
		
		<!-- S T A R T :: containerArea-->
		<div id="container">
			<div id="content">
				<div class="con_tit">
					<h2><?=$pageTitle?> - [목록]</h2>
				</div>
				<!-- //con_tit -->
				<div class="con">
					<!-- 내용 : s -->
					<div id="bbs">
						<div id="blist">
							<p><span><strong>총 <?=$rowPageCount[0]?>개</strong>  <span class="middle">|</span>  <?=$param->reqPageNo?>/<?=$rowPageCount[1]?>페이지</span></p>
							<table>
							<colgroup>
								<col width="8%" />
								<col width="14%" />
								<col width="14%" />
								<col width="14%" />
								<col width="14%" />
								<col width="14%" />
								<col width="14%" />
								<col width="8%" />
							</colgroup>
							<thead>
								<tr>
									<th>NO</th>
									<th>상태</th>
									<th>제목</th>
									<th>시작일</th>
									<th>종료일</th>
									<th>작성자</th>
									<th>작성일</th>
									<th>조회수</th>
								</tr>
							</thead>
							<tbody>
								<?
									if($rowPageCount[0] > 0){
										$targetUrl 	= "";
										$style 	= "";
										$i = 0;
										while ($row=mysql_fetch_assoc($list)) {
											$style	= "style=\"cursor:pointer;\"";
											$targetUrl = "onclick=\"document.location.href='".$param->getQueryString('read.php', $row['no'], $_REQUEST)."'\"";
								?>
								<tr <?=$targetUrl?><?=$style ?>>
									<td><?=($rowPageCount[0]-(($param->reqPageNo-1)*$pageRows) - $i)?></td>
									<td><?=getState( $row['state'] )?></td>
									<td><?=$row['title']?></a></td>
									<td><?=$row['startdate'] ?></td>
									<td><?=$row['enddate'] ?></td>
									<td><?=$row['user_name']?></td>
									<td><?= getYMD($row['registdate']) ?></td>
									<td><?=$row['readno']?></td>
								</tr>
								<?
									$i++;
										}
									} else {
								?>
								<tr><td class="first" colspan="7">등록된 일정이 없습니다.</td></tr>
								<? 	} ?>
							</tbody>
						</table>
								<input type="hidden" name="cmd" id="cmd" value="groupDelete"/>
								<input type="hidden" name="stype" id="stype" value="<?=$_REQUEST[stype]?>"/>
								<input type="hidden" name="sval" id="sval" value="<?=$_REQUEST[sval]?>"/>
							</form>
							<div class="btn">
								<div class="btnLeft">
									<a class="btns gr_btn" href="#" onclick="groupDelete();">삭제</a>
								</div>
								<div class="btnRight">
									<a class="wbtn" href="write.php">글쓰기</a>
								</div>
							</div>
							<!--//btn-->
							<!-- 페이징 처리 -->
							<?=pageList($param->reqPageNo, $rowPageCount[1], $param->getQueryString('list.php', 0, $_REQUEST))?>
							<!-- //페이징 처리 -->
							<form name="searchForm" id="searchForm" action="list.php" method="post">
								<div class="search">
									<select name="stype" title="검색을 선택해주세요">
										<option value="all" <?=getSelected($_REQUEST[stype], "all") ?>>전체</option>
										<option value="title" <?=getSelected($_REQUEST[stype], "title") ?>>제목</option>
										<option value="contents" <?=getSelected($_REQUEST[stype], "contents") ?>>내용</option>
										<option value="user_name" <?=getSelected($_REQUEST[stype], "user_name") ?>>작성자</option>
									</select>
									<input type="text" name="sval" id="sval" value="<?=$_REQUEST[sval]?>" />
									<a href="javascript:;" class="se_btn " onclick="$('#searchForm').submit()" >검색</a>
								</div>
							</form>
							<!-- //search --> 
						</div>
						<!-- //blist -->
					</div>
					<!-- //bbs --> 
					<!-- 내용 : e -->
				</div>
				<!--//con -->
			</div>
			<!--//content -->
		</div>
		<!--//container --> 
		<!-- E N D :: containerArea-->
	</div>
	<!--//canvas -->
</div>
<!--//wrap -->

</body>
</html>