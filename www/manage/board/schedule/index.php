<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/schedule/Schedule.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";


	$param = new Schedule($pageRows, $_REQUEST);
	
	$today = getReqParameter($_REQUEST['today'], substr(getToday(),0,8).'01');
	$start_day = getMonthDateAdd( -1, $today );
	$end_day = getMonthDateAdd( 2, $today );
	$today_year = substr($today,0,4);
	$today_month = substr($today,5,2);

	$start = $_REQUEST['start'];
	$end = $_REQUEST['end'];
	$color0 = $_REQUEST['color0'];
	$color1 = $_REQUEST['color1'];

	$_REQUEST['startday'] = $start_day;
	$_REQUEST['endday'] = $end_day;
	$_REQUEST['today_year'] = $today_year;
	$_REQUEST['today_month'] = $today_month;

	$schedulelist = $param->schedulelist($_REQUEST);
	$schedulelist2 = $param->schedulelist($_REQUEST);

	$scheduleObjectList = array();
	while ($row2=mysql_fetch_assoc($schedulelist2)) {
		array_push($scheduleObjectList, $row2);
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
<link rel="stylesheet" href="/css/schedule.css" type="text/css" media="all" />

<script type="text/javascript">

// 변수 선언부
var arr = new Array(12);
	arr[0] = 31;
	arr[1] = 28;
	arr[2] = 31;
	arr[3] = 30;
	arr[4] = 31;
	arr[5] = 30;
	arr[6] = 31;
	arr[7] = 31;
	arr[8] = 30;
	arr[9] = 31;
	arr[10] = 30;
	arr[11] = 31;

var monthArr = new Array(12);
	monthArr[0] = ("January");
	monthArr[1] = ("Feburary");
	monthArr[2] = ("March");
	monthArr[3] = ("April");
	monthArr[4] = ("May");
	monthArr[5] = ("June");
	monthArr[6] = ("July");
	monthArr[7] = ("August");
	monthArr[8] = ("September");
	monthArr[9] = ("October");
	monthArr[10] = ("November");
	monthArr[11] = ("December");

var colorArr = ["clean","clean"];
if( "<?=$color0?>" != "" ){
	colorArr[0] = "<?=$color0?>";
}
if( "<?=$color1?>" != "" ){
	colorArr[1] = "<?=$color1?>";
}

var arrIndex = 0;
var today = new Date();

if( (today.getFullYear()+"-"+(today.getMonth()+1)) != "<?=substr($today, 0, 8)?>" ){
	today.setFullYear( "<?=substr($today, 0, 4) ?>" );
	today.setMonth( "<?=(int)substr($today, 5, 2)-1 ?>" );
	today.setDate( "<?=substr($today, 8) ?>" );
}

/*
var year = today.getFullYear();
var month = (today.getMonth()+1);
var day = today.getDay();
*/
var yearOption = '';
var monthOption = '';
var yearVal = '<?=$today_year?>';
var monthVal = '<?=$today_month?>';
for(var i=-3;i<4;i++){
	yearOption += '<option value="'+(yearVal *1 + i)+'">'+(yearVal *1 + i)+'</option>';
}
for(var i=1;i<=12;i++){
	var sumi = i;
	if(sumi < 10){
		sumi = '0' + sumi;
	}
	monthOption += '<option value="'+sumi+'">'+sumi+'</option>';
}

function goWrite(day){
	if(day < 10){
		day = "0"+day;
	}
	var sdate = yearVal + "-" + monthVal + "-" + day;
	location.href="write.php?menu_fk=<?=$_REQUEST[menu_fk]?>&startdate1="+sdate;
}
// var caltitle = '<a onclick="getDateCycle(-1);" style="cursor:pointer;" class="c_left"><img src="/img/comm_left.png" /></a>'+
// 							'<span class="cal_title"></span>'+
// 							'<a onclick="getDateCycle(1);" style="cursor:pointer;" class="c_right"><img src="/img/comm_right.png" /></a>';
var caltitle = '<a onclick="getDateCycle(-1);" class="c_left"><img src="/img/comm_left.png" /></a>'+
							'<select id="calYear" >'+
							yearOption+
							'</select>'+
							'<select id="calMonth">'+
							monthOption+
							'</select>'+
							'<a onclick="getDateCycle(1);" class="c_right"><img src="/img/comm_right.png" /></a>';

var calform = '<? for( $i=0; $i<6; $i++ ){ ?>'+
							'<tr>'+
								'<? for( $j=0; $j<7; $j++ ){ ?>'+
										'<td id="td<?=$i*7+$j?>" onclick="" value="0" class="<?=dayColor($i*7+$j)?>">'+
											'<span id="schedule<?=$i*7+$j?>" class="date" style="cursor:pointer" onclick="goWrite(<?=$i*7+$j?>);"></span>'+
											'<ul>'+
												'<? for ($k=0; $k<count($scheduleObjectList); $k++) { ?>'+
													'<? if( $scheduleObjectList[$k]["state"] > -1 ){ ?>'+
														'<li class="lines"></li>'+
													'<? } ?>'+
												'<? } ?>'+
											'</ul>'+
										'</td>'+
								'<? } ?>'+
							'</tr>'+
							'<? } ?>';

function getMaxDay( mon ){
	
	if( today.getFullYear() % 400 == 0 ){
		arr[1] = 29;
	}
	else if( today.getFullYear() % 4 == 0 && today.getFullYear() % 100 != 0){
		arr[1] = 29;
	}
	return arr[mon];
}

function getTodayDate( value ){

	var result = today.getMonth()+value;	
	if( result == 12 ){ result = 0; }
	else if( result == -1 ){ result = 11; }
	var result2 = "";
	return '<?=$today_year?>'+"."+'<?=$today_month?>'+" "+monthArr[<?=$today_month?>*1-1];

}

function refresh(){
	var yy = 0;
	if( today.getMonth() == -1 ){ today.setMonth(11); yy -= 1; }
	else if( today.getMonth() == 12 ){ today.setMonth(0); yy += 1; }

	today.setFullYear( today.getFullYear() + yy );

	$('#cal_title').html( caltitle );
	
	$('#calYear option').each(function(){
		if($(this).val() == yearVal)
			$(this).prop('selected', true);
	});
	
	$('#calMonth option').each(function(){
		if($(this).val() == monthVal)
			$(this).prop('selected', true);
	});
	
	for( var i=-1; i<2; i++ ){
		$('.cal_title:eq('+(i+1)+')').html(getTodayDate(i));
	}
	$('tbody').html( calform );
	/*
	var firstday = new Date();
	firstday.setDate(1);
	firstday.setMonth(today.getMonth()-1);*/
	for( var k=0; k<getMaxDay(today.getMonth()); k++ ){
		$('#schedule'+(today.getDay()+k)).html(k+1);
	}
	makeScheduleLine();
	console.log( today.getFullYear()+"-"+(today.getMonth()+1)+"-"+today.getDate());

}

function dateChg(year, month){
	var today = year + "-" + month + "-01";
	location.href = "index.php?today="+today+"&menu_fk=<?=$_REQUEST[menu_fk]?>";
}

function getDateCycle( value ){
	today.setMonth( today.getMonth()+value );
	//refresh();
	var sendMonth = (today.getMonth()+1);
	var sendDate = today.getDate();
	if( (today.getMonth()+1) < 10 ){
		sendMonth = "0"+sendMonth;
	}
	if( today.getDate() < 10 ){
		sendDate = "0"+sendDate;
	}
	location.href="index.php?today="+today.getFullYear()+"-"+sendMonth+"-01&start="+$('#start_day').val()+"&end="+$('#end_day').val()+"&color0="+colorArr[0]+"&color1="+colorArr[1]+"&menu_fk=<?=$_REQUEST[menu_fk]?>";
}

function makeScheduleLine(){
	var color = new Array('#fce4e4','#cfe4fe','#c5f4b0','#fff0c4','#ffd9c4','#c4c8ff','#e6b5fd','#ffc4e4','#fcd56c','#a4a0fd');
	var index = 0;
	var idxChk = -1;
	<? while ($row=mysql_fetch_assoc($schedulelist)) {
			$title = strlen($row['title']) > 8 ? substr($row['title'], 0, 8) : $row['title'];
			$title2 = strlen($row['title']) > 8 ? substr($row['title'], 8, strlen($row['title'])) : "";
	?>
		if( '<?=$row["state"]?>' > '-1' ){

			var slday = '<?=$row[startdate]?>';
			var eslday = '<?=$row[enddate]?>';
			var lineCol = '<?=$row[line_color]?>';
			
			var s = new Date(slday);
			var e = new Date(eslday);
			var minus = (e.getTime() - s.getTime()) / 1000/60/60/24;
			var _height = 0;							
			var padd=  0;
			var addTd = '';
			for( var fd=0; fd<42; fd++ ){
				if( $('#schedule'+fd).html() != "" ){		//값이 있으면
					//시작일과 종료일의 월이 같을때
					if( slday.substring(5,7) == eslday.substring(5,7) ){
						if( $('#schedule'+fd).html() >= parseInt(slday.substring(8)) && $('#schedule'+fd).html() <= parseInt(eslday.substring(8)) ){	//범위 내의 li를
							if( $('#schedule'+fd).html().trim() == parseInt(slday.substring(8)) ){
//					console.log("index :: "+index);
//					console.log("fd :: "+fd);
								var state = "<?=getHtmlRemove( getState($row[state]))?>";
								if(state.trim() != ''){
									state = "["+state+"] ";
								}
								var cc= Math.round(minus);
									$('#td'+fd+' ul li:eq('+index+')').html('<strong><span>'+state+'</span><?= getHtmlRemove( $row[title])?></strong>');
									
							}
							if(index != idxChk){
								_height = $('#td'+fd+' ul li:eq('+index+')').css('height');
								padd = $('#td'+fd+' ul li:eq('+index+')').css('padding');
							}
							idxChk = index;
							
							$('#td'+fd+' ul li:eq('+index+')').parents('tr').find('td').each(function(){
								$(this).find('ul li:eq('+index+')').css('height', _height);
							});
							console.log(addTd);
							$('#td'+fd+' ul li:eq('+index+')').attr('class', 'lines text bg_type');
//							$('#td'+fd+' ul li:eq('+index+')').css('background-color',color[index%color.length]);		// 색칠
							$('#td'+fd+' ul li:eq('+index+')').css('background-color',lineCol);		// 색칠
							$('#td'+fd+' ul li:eq('+index+')').css('cursor','pointer');									// 마우스 모양
							$('#td'+fd+' ul li:eq('+index+')').click(function(){										// 링크
								location.href='read.php?menu_fk=<?=$row[menu_fk]?>&no=<?=$row[no]?>';
							});
						}
					}
					else{	//시작일과 종료일의 월이 다를때

						if( (today.getMonth()+1) == parseInt(slday.substring(5,7)) ){
							var diffNo = '<?=$row[no]?>';
							if( $('#schedule'+fd).html() >= parseInt(slday.substring(8)) ){
								var state = "<?=getHtmlRemove( getState( $row[state] ))?>";
								if(state.trim() != ''){
									state = "["+state+"] ";
								}
								if( $('#schedule'+fd).html() == parseInt(slday.substring(8)) ){
									$('#td'+fd+' ul li:eq('+index+')').html('<font color="black"><?=$row[title]?></font>');
									$('#td'+fd+' ul li:eq('+index+')').html('<strong><span>'+state+'</span><?=$row[title]?></strong>');
								}
								if( $('#schedule'+fd).html() == arr[today.getMonth()] ){
									$('#td'+fd+' ul li:eq('+index+')').css('text-align','right');
									$('#td'+fd+' ul li:eq('+index+')').html('<font color="black" style="padding-right:0px;">~ <?=substr($row[enddate], 0, 10)?></font>');
								}

								if(index != idxChk){
									_height = $('#td'+fd+' ul li:eq('+index+')').css('height');
									padd = $('#td'+fd+' ul li:eq('+index+')').css('padding');
								}
								idxChk = index;

								$('#td'+fd+' ul li:eq('+index+')').css('height', _height);
								$('#td'+fd+' ul li:eq('+index+')').css('padding', padd);
								$('#td'+fd+' ul li:eq('+index+')').attr('class', 'lines text bg_type');
//								$('#td'+fd+' ul li:eq('+index+')').css('background-color',color[index%color.length]);		// 색칠
								$('#td'+fd+' ul li:eq('+index+')').css('background-color',lineCol);		// 색칠
								$('#td'+fd+' ul li:eq('+index+')').css('cursor','pointer');									// 마우스 모양
								$('#td'+fd+' ul li:eq('+index+')').click(function(){										// 링크
									location.href='read.php?menu_fk=<?=$row[menu_fk]?>&no=<?=$row[no]?>';
								});
							}
						}
						else if( (today.getMonth()+1) == parseInt(eslday.substring(5,7)) ){
							var diffNo = '<?=$row[no]?>';
							if( $('#schedule'+fd).html() <= parseInt(eslday.substring(8)) ){
								var state = "<?=getHtmlRemove( getState( $row[state] ))?>";
								if(state.trim() != ''){
									state = "["+state+"] ";
								}
								if( $('#schedule'+fd).html() == parseInt(eslday.substring(8)) ){
									$('#td'+fd+' ul li:eq('+index+')').css('text-align','right');
								}
								if( $('#schedule'+fd).html() == '1' ){
									$('#td'+fd+' ul li:eq('+index+')').css('text-align','left');
									$('#td'+fd+' ul li:eq('+index+')').prepend('<strong><span>'+state+'</span><?=$row[title]?></strong>');
								}

								if(index != idxChk){
									_height = $('#td'+fd+' ul li:eq('+index+')').css('height');
									padd = $('#td'+fd+' ul li:eq('+index+')').css('padding');
								}
								idxChk = index;

								$('#td'+fd+' ul li:eq('+index+')').css('height', _height);
								$('#td'+fd+' ul li:eq('+index+')').css('padding', padd);
								$('#td'+fd+' ul li:eq('+index+')').attr('class', 'lines text bg_type');
//								$('#td'+fd+' ul li:eq('+index+')').css('background-color',color[index%color.length]);		// 색칠
								$('#td'+fd+' ul li:eq('+index+')').css('background-color',lineCol);		// 색칠
								$('#td'+fd+' ul li:eq('+index+')').css('cursor','pointer');									// 마우스 모양
								$('#td'+fd+' ul li:eq('+index+')').click(function(){										// 링크
									location.href='read.php?menu_fk=<?=$row[menu_fk]?>&no=<?=$row[no]?>';
								});
							}
						}

					}
				}	
			}
			index += 1;
		}
	<? } ?>
}

$(window).load( function(){
	refresh();
});


function setColor( obj ){

	if( $('#schedule'+$(obj).attr('id').substring($(obj).attr('id').indexOf('td')+2)).html() == "" ){
		alert('선택 할 수 없습니다.');
	}
	else{
		var theday = new Date();
		if( colorArr[0] != "clean" && colorArr[1] != "clean" ){
			removeColor();		
		}

		switch ($(obj).attr('value')){
			case '0':
				if( colorArr[0] == "clean" ){
					colorArr[0] = $(obj).attr('id');
					theday.setMonth( today.getMonth() );
					theday.setDate( $('#schedule'+$(obj).attr('id').substring($(obj).attr('id').indexOf('td')+2)).html() );
					var mon0 = (theday.getMonth()+1);
					var day0 = theday.getDate();
					if( (theday.getMonth()+1) < 10 ){ mon0 = "0"+mon0; }
					if( theday.getDate() < 10 ){ day0 = "0"+day0; }
					$('#start_day').val(theday.getFullYear()+"-"+mon0+"-"+day0);
				}
				else{
					colorArr[1] = $(obj).attr('id');
					theday.setMonth( today.getMonth() );
					theday.setDate( $('#schedule'+$(obj).attr('id').substring($(obj).attr('id').indexOf('td')+2)).html() );
					var mon0 = (theday.getMonth()+1);
					var day0 = theday.getDate();
					if( (theday.getMonth()+1) < 10 ){ mon0 = "0"+mon0; }
					if( theday.getDate() < 10 ){ day0 = "0"+day0; }
					$('#end_day').val(theday.getFullYear()+"-"+mon0+"-"+day0);
				}
				$(obj).css('background-color','#EEE');
				$(obj).attr('value','1');
				break;
			case '1':
				if( colorArr[0] == $(obj).attr('id') ){ colorArr[0] = "clean"; }
				else{ colorArr[1] = "clean"; }
				$(obj).css('background-color','#FFF');
				$(obj).attr('value','0');	
				$('#start_day').val("");
				$('#end_day').val("");
				break;
		}

	}
}

function removeColor(){
	if( colorArr[0] != "clean" && colorArr[1] != "clean" ){
		for(var c=0; c<42; c++){
			$('#td'+c).css('background-color','#FFF');
			$('#td'+c).attr('value','0');
		}
		colorArr[0] = "clean";
		colorArr[1] = "clean";
		$('#start_day').val("");
		$('#end_day').val("");
	}
}

$(window).load(function(){
	var leng = 0;
	var check = false;
	$('.schedule_table td ul').each(function(idx){
		
	});
	
	
	
	$('.schedule_table tbody tr').each(function(){
		if($(this).find('.bg_type').length == 0){
			$(this).find('.lines').css('min-height', 'auto');
		}
	});
	
	$('#calYear, #calMonth').change(function(){
		var day = $('#calYear').val() + "-" + $('#calMonth').val() + "-01";
		location.href = 'index.php?today='+day+'&menu_fk=<?=$_REQUEST["menu_fk"]?>';
	});
	
//	calResize();
	nullRemove();
	
});

function nullRemove(){
	$('.c_table tbody tr').each(function(_idx, item){
		var arr_delIdx = new Array();
		var arr_idx = new Array();
		$(item).find('td').each(function(idx2, item2){
			if( $(item2).find(".text").length > 0  ){

				$( item2).find("li").each(function(_liIdx ){
					var alreadyHas = false;
					if( $(this).hasClass("text")){
						for(var idx in arr_delIdx){
							if( arr_delIdx[idx] == _liIdx ){
								alreadyHas = true;
							}else{

							}
						}
						if( !alreadyHas ) arr_delIdx.push( _liIdx );
					}
				});
				
			}

		});		// for td end
		$(item).find('td').each(function(){
			$(this).find("li").each(function(__idx, _item){
				var willRemove = true;
				for(var idx in arr_delIdx){
			
					if( __idx != arr_delIdx[idx] && willRemove ) willRemove = true;
					else willRemove = false;

				}
				if( willRemove ){
//					$(_item).css("min-height", "auto");
					$(_item).remove();
				}
			});
		});
	});
}


function calResize(){
	var indexArr1 = new Array();
	var indexArr2 = new Array();
	$(".c_table.schedule_table tbody tr").each(function(idx,item){
		var tempMax = 100;
		var tempMin = 0;
		indexArr1[idx] = 0;
		indexArr2[idx] = 0;
		var maxSize = 0;
		var maxLine = 0;
		var firstVali = false;
		var lastVali = false;
		$(item).find("td").each(function(idx2, item2){
			maxLine = $(item2).find(".lines").size();
			var index1 = $(item2).find(".lines.bg_type").first().index();
			var index2 = $(item2).find(".lines.bg_type").last().index();
			var index3 = index2 - index1 + 1;
	        if ( tempMax > index1 && index1 != -1 ) {
		        indexArr1[idx] = index1;
	        }
			tempMax = index1;
	        if ( tempMin < index2 && index2 != -1 ) {
		        indexArr2[idx] = index2;
	        }
			tempMin = index2;
			if ( index3 > maxSize ) {
				maxSize = index3;
	        }
			if ( $(item2).find("ul li").first().attr("class").indexOf("bg_type") != -1 ) {
				firstVali = true;
	        }
			if ( $(item2).find("ul li").last().attr("class").indexOf("bg_type") != -1 ) {
				lastVali = true;
	        }
		});
	});

	$(".c_table.schedule_table tbody tr").each(function(idx,item){
		$(item).find("td").each(function(idx2, item2){
	        $(item2).find("li:lt("+indexArr1[idx]+")").css("min-height","auto");
	        $(item2).find("li:gt("+(indexArr2[idx])+")").css("min-height","auto");
		});
	});
}
</script>
</head>
<body> 
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<div id="wrap">
	<!-- canvas -->
	<div id="canvas">
		<!-- S T A R T :: headerArea-->
		<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/top.php" ?>
		<!-- E N D :: headerArea--> 
		
		<!-- S T A R T :: containerArea-->
		<div id="container">
			<div id="content">
				<div class="con_tit">
					<h2><?=$pageTitle?> - [목록]</h2>
				</div>
				<!-- //con_tit -->
				<div class="con">
					<!-- 내용 : s -->
					<div id="bbs">
						<div id="blist">
							<div class="commuting_wrap">
							<!-- 달력 폼 시작 -->
							<div class="div_05_calender mb30">
								<div id="cal_title" class="div_month mb22 up_bar"></div>
								<div class="c_table schedule_table">
								<table>
									<colgroup>
										<col width="100px" />
										<col width="100px" />
										<col width="100px" />
										<col width="100px" />
										<col width="100px" />
										<col width="100px" />
										<col width="100px" />
									</colgroup>
										<thead>
											<tr>
												<th>일</th>
												<th>월</th>
												<th>화</th>
												<th>수</th>
												<th>목</th>
												<th>금</th>
												<th>토</th>
											</tr>
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
							
							<div class="btn">
								<div class="btnLeft">
									<a class="btns gr_btn" href="list.php">리스트</a>
								</div>
								<div class="btnRight">
									<a class="wbtn" href="write.php">글쓰기</a>
								</div>
							</div>
						
							<form name="searchForm" id="searchForm" action="list.php" method="get">
							<div class="search">
								<select name="stype" title="검색을 선택해주세요">
									<option value="all" <?=getSelected($_REQUEST['stype'], "all") ?>>전체</option>
									<option value="title" <?=getSelected($_REQUEST['stype'], "title") ?>>제목</option>
									<option value="contents" <?=getSelected($_REQUEST['stype'], "contents") ?>>내용</option>
									<option value="user_name" <?=getSelected($_REQUEST['stype'], "user_name") ?>>작성자</option>
								</select>
							
								<span>
									<input type="text" name="sval" id="sval" value="<?=$_REQUEST['sval']?>" />
									<input type="submit" class="se_btn " value="검색" />
								</span>
							</div>
							<input type="hidden" name="menu_fk" value="<?=$_REQUEST['menu_fk'] ?>" />
							</form>
						     <!-- 달력 폼 끝 -->
						
							</div>
						</div>
						<!-- //blist -->
					</div>
					<!-- //bbs --> 
					<!-- 내용 : e -->
				</div>
				<!--//con -->
			</div>
			<!--//content -->
		</div>
		<!--//container --> 
		<!-- E N D :: containerArea-->
	</div>
	<!--//canvas -->
</div>
<!--//wrap -->

</body>
</html>