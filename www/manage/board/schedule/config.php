<?
$pageTitle 	 	= "일정표";				// 게시판 명
$tablename  		= "schedule";					// DB 테이블 명
$uploadPath 		= "/upload/file/";			// 파일, 동영상 첨부 경로
$fileLimit = "JSP|ASP|PHP|HTML|CAB|EXE|EXEC|JS";
$maxSaveSize	  	= 50*1024*1024;					// 50Mb
$pageRows	  		= 10;							// 보여질 게시글 수
$gradeno			= 2;							// 관리자 권한 기준 [지점 사용시]
$islog 			= false; 						//유입(방문자통계 사용여부)
$usePerfect 	= true; 						//조회수
		
/* 화면 필드 생성 여부 */
$unickNum		= true;							// 고유번호 사용유무  
$branch	  		= false;							// 지점 사용유무  
$type	  		= false;							// 분류 지점별 사용유무	
$doctor	  		= false;							// 의료진 사용유무
$useMovie  	 	= true;						// 파일 첨부 [사용: true, 사용 안함 : true]
$useRelationurl	= true;						// 관련URL 첨부 [사용: true, 사용 안함 : true]
$isComment 		= false;							// 댓글		
$likeUp			= true;
$istop 			= true;
$ismain 		= true;
$isnew 			= true;

$timeDate	  	= true;						// 날짜 or 날짜 and 시간	
$userCon	  	= true;							// 조회수 카운트 [관리자 페이지:true, 사용자 페이지:true, 조회수:사용여부]
$registdateGubun = true;							//날짜 수정
$useFile  		= true;							// 파일 첨부 [사용: true, 사용 안함 : true]
$textAreaEditor	= true;							// 에디터 사용여부
$editorIgnore	= "";								//에디터 미사용 배열 구분자 ex : '|0|1|'		
$orderby = Array( "top desc","registdate desc" );					    //order by 배열

$useFileCount = 0;

?>
