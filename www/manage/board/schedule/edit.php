<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/schedule/Schedule.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

	$param = new Schedule($pageRows, $_REQUEST);
	$data = $param->getData($_REQUEST, true);
	
	$startdate = explode(" ", $data['startdate']);
	$enddate = explode(" ", $data['enddate']);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="/css/schedule.css" type="text/css" media="all" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/boardConfig/config.php" ?>
<link rel="stylesheet" href="/css/schedule.css" type="text/css" media="all" />
<script type="text/javascript">
$(document).ready(function(){
	$('#startdate1').datetimepicker({timepicker:false,format: "Y-m-d"});
	$("#startbutton").click(function(){
		$('#startdate1').datetimepicker('show');
	})		
	$('#enddate1').datetimepicker({timepicker:false,format: "Y-m-d"});
	$("#endbutton").click(function(){
		$('#enddate1').datetimepicker('show');
	})				
	//initCal({id:"startdate1",type:"day",today:"y"});
	//initCal({id:"enddate1",type:"day",today:"y"});

});


function goSave(obj) {
	if(validation(obj)){
		$('[name=startdate]').val( $('#startdate1').val() +" "+ $('#startdate2').val()+":"+$('#startdate3').val() +":00" );
		$('[name=enddate]').val( $('#enddate1').val() +" "+ $('#enddate2').val()+":"+$('#enddate3').val() +":00" );
		if($('[name=startdate]').val() > $('[name=enddate]').val()){
			alert('시작일이 종료일보다 높습니다. 재설정 해주세요.');
			$('[name=startdate]').focus();
			return false;
		}
		var form = $("#frm");
		$(form).submit();	
	}else{
		return;
	}
}


function deleteFile(no){
	if(confirm("첨부된 파일을 삭제하시겠습니까?")){
		$.post("filePop.php", "cmd=fileDelete&no="+no, function(data){
			data = data.replace(/<[^>]+>/g, '').trim();
			if(data == 'success'){
				$("#file"+no).hide(500).remove();
			} else if (data == 'fail') {
				alert('파일 삭제에 실패했습니다.');
			}
		});
	}
}

</script>
</head>
<body> 
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<div id="wrap">
	<!-- canvas -->
	<div id="canvas">
		<!-- S T A R T :: headerArea-->
		<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/top.php" ?>
		<!-- E N D :: headerArea--> 
		
		<!-- S T A R T :: containerArea-->
		<div id="container">
			<div id="content">
				<div class="con_tit">
					<h2><?=$pageTitle?> - [수정]</h2>
				</div>
				<!-- //con_tit -->
				<div class="con">
					<!-- 내용 : s -->
					<div id="bbs">
						<div id="bwrite">
							<form action="process.php" method="post" id="frm" name="frm"  enctype="multipart/form-data">
								<table>
								<colgroup>
									<col width="19%" />
									<col width="*" />
								</colgroup>
								<tbody>
									<tr>
										<th>이름</th>
										<td><input type="text" value="<?=$data['user_name']?>" name="user_name" id="name" class="size02"/></td>
									</tr>
									<tr>
										<th><span class="col01"> </span>시작일</th>
										<td>
											<input type="text" value="<?=$startdate[0] ?>" name="startdate1" id="startdate1" class="size02" maxlength="13" onkeyup="isNumberOrHyphen(this);" readonly="readonly"/>
											<span id="Calstartdate1Icon">
											<button type="button" id="startbutton"><img src="/manage/img/calendar_icon.png" id="CalregistdateIconImg" style="cursor:pointer;"/></button>
											</span>
											<select name="startdate2" id="startdate2" data-value="시작일을 입력해 주세요.">
												<?
												$temp = explode(":", $startdate[1]);
												$stime = "";
												for($i=1;$i<=23;$i++){ 
													$stime = $i < 10 ? ("0".$i) : $i;
												?>
												<option value="<?=$stime?>" <?=getSelected($temp[0], $stime) ?> ><?=$stime?></option>
												<?} ?>
											</select><span>시</span>
											<select name="startdate3" id="startdate3" data-value="시작일을 입력해 주세요." >
												<?
												$stime2 = "";
												for($i=0;$i<=55;$i=$i+5){ 
													$stime2 = $i < 10 ? ("0".$i) : $i;
												?>
												<option value="<?=$stime2?>" <?=getSelected($temp[1], $stime2) ?> ><?=$stime2?></option>
												<?} ?>
											</select><span class="last">분</span>
										</td>
									</tr>
									<tr>
										<th><span class="col01"> </span>종료일</th>
										<td>
											<input type="text" value="<?=$enddate[0] ?>" name="enddate1" id="enddate1" class="size02" maxlength="13" onkeyup="isNumberOrHyphen(this);" readonly="readonly"/>
											<span id="Calenddate1Icon">
											<button type="button" id="endbutton"><img src="/manage/img/calendar_icon.png" id="CalregistdateIconImg" style="cursor:pointer;"/></button>
											</span>
											<select name="enddate2" id="enddate2" data-value="종료일을 입력해 주세요." >
												<?
												$temp = explode(":", $enddate[1]);
												$etime = "";
												for($i=1;$i<=23;$i++){ 
													$etime = $i < 10 ? ("0".$i) : $i;
												?>
												<option value="<?=$etime?>" <?=getSelected($temp[0], $etime) ?> ><?=$etime?></option>
												<?} ?>
											</select><span>시</span>
											<select name="enddate3" id="enddate3" data-value="종료일을 입력해 주세요.">
												<?
												$etime2 = "";
												for($i=0;$i<=55;$i=$i+5){ 
													$etime2 = $i < 10 ? ("0".$i) : $i;
												?>
												<option value="<?=$etime2?>" <?=getSelected($temp[1], $etime2) ?> ><?=$etime2?></option>
												<?} ?>
											</select><span class="last">분</span>
										</td>
									</tr>


									<tr>
										<th><span class="col01"> </span>상태</th>
										<td>
											<label><input type="radio" name="state" value="0" <?=getChecked($data['state'], '0') ?>/> 상태없음</label>
											<label><input type="radio" name="state" value="1" <?=getChecked($data['state'], '1') ?>/> 미진행</label>
											<label><input type="radio" name="state" value="2" <?=getChecked($data['state'], '2') ?>/> 진행중</label>
											<label><input type="radio" name="state" value="3" <?=getChecked($data['state'], '3') ?>/> 완료</label>
										</td>
									</tr>
									<tr>
										<th><span class="col01"> </span>라인 색상</th>
										<td class="lin_color">
											<label><input type="radio" name="line_color" value="#fce4e4" <?=getChecked($data['line_color'], "#fce4e4")?>/><i style="background-color:#fce4e4"></i></label>
											<label><input type="radio" name="line_color" value="#cfe4fe" <?=getChecked($data['line_color'], "#cfe4fe")?>/> <i style="background-color:#cfe4fe"></i></label>
											<label><input type="radio" name="line_color" value="#c5f4b0" <?=getChecked($data['line_color'], "#c5f4b0")?>/> <i style="background-color:#c5f4b0"></i></label>
											<label><input type="radio" name="line_color" value="#fff0c4" <?=getChecked($data['line_color'], "#fff0c4")?>/> <i style="background-color:#fff0c4"></i></label>
											<label><input type="radio" name="line_color" value="#ffd9c4" <?=getChecked($data['line_color'], "#ffd9c4")?>/> <i style="background-color:#ffd9c4"></i></label>
											<label><input type="radio" name="line_color" value="#c4c8ff" <?=getChecked($data['line_color'], "#c4c8ff")?>/> <i style="background-color:#c4c8ff"></i></label>
											<label><input type="radio" name="line_color" value="#e6b5fd" <?=getChecked($data['line_color'], "#e6b5fd")?>/> <i style="background-color:#e6b5fd"></i></label>
											<label><input type="radio" name="line_color" value="#ffc4e4" <?=getChecked($data['line_color'], "#ffc4e4")?>/> <i style="background-color:#ffc4e4"></i></label>
											<label><input type="radio" name="line_color" value="#fcd56c" <?=getChecked($data['line_color'], "#fcd56c")?>/> <i style="background-color:#fcd56c"></i></label>
											<label><input type="radio" name="line_color" value="#a4a0fd" <?=getChecked($data['line_color'], "#a4a0fd")?>/> <i style="background-color:#a4a0fd"></i></label>
										</td>
									</tr>
									<tr>
										<th>제목</th>
										<td><input type="text" value="<?=$data['title']?>" name="title" id="title" class="w50"/></td>
									</tr>
									<tr>
										<th>내용</th>
										<td>
											<textarea name="contents" id="contents"><?=stripslashes($data['contents'])?></textarea>
										</td>
									</tr>
									<tr>
										<th>동영상</th>
										<td>
											<p><label for="">확장명이 .MP4가 아닌 경우 재생이 안될 수도 있습니다.</label></p>
											<? if ($data['moviename'] == null || "" == $data['moviename']) { ?>
											<input type="file" name="moviename" id="moviename" class="input92p size03" title="동영상" />
											<? } else { ?>
											<span>기존파일 : <?=$data['moviename_org']?>&nbsp;&nbsp;</span>
											<input type="checkbox" id="moviename_chk" name="moviename_chk" value="1" title="동영상을 삭제하시려면 체크해주세요" />
											<label for="moviename_chk">기존파일삭제</label><br/>
											<input type="file" id="moviename" name="moviename" class="input92p size03" title="동영상을 업로드 해주세요." />	
											<? } ?>
										</td>
									</tr>
								</tbody>
								</table>
								<input type="hidden" name="startdate" />
								<input type="hidden" name="enddate" />
								<input type="hidden" name="cmd" value="edit" />
								<input type="hidden" name="user_fk" value="<?=$data['user_fk']?>" />
								<input type="hidden" name="no" value="<?=$data['no']?>" />
								<input type="hidden" name="menu_fk" value="<?=$data['menu_fk']?>" />
								<input type="hidden" name="board_type" value="<?=$data['board_type']?>" />
								<input type="hidden" name="temp_no" value="<?=$_REQUEST['createTemp_no']?>" />
							</form>
							<div class="btn">
								<? $_REQUEST['no'] = $data['no']; ?>
								<div class="btnLeft">
									<a class="btns gr_btn" href="<?=$param->getQueryString("index.php", $data['no'], $_REQUEST)?>">취소</a>
								</div>
								<div class="btnRight">
									<a class="btns gr_btn" href="javascript:;" onClick="goSave(this);">저장</a>
								</div>
							</div>
							<!--//btn-->
							<?
								if ($isComment) {
							?>
								<? include $_SERVER['DOCUMENT_ROOT']."/manage/board/comment/comment.php" ?><!-- 댓글 -->
							<?
								}
							?>	
						</div>
						<!-- //bread -->
					</div>
					<!-- //bbs --> 
					<!-- 내용 : e -->
				</div>
				<!--//con -->
			</div>
			<!--//content -->
		</div>
		<!--//container --> 
		<!-- E N D :: containerArea-->
	</div>
	<!--//canvas -->
</div>
<!--//wrap -->

</body>
</html>