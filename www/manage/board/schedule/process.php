<?session_start();
	include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

	include_once $_SERVER['DOCUMENT_ROOT']."/lib/schedule/Schedule.class.php";

	include "config.php";

	$param = new Schedule(0, request);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
</head>
<body>
<?	
if (checkReferer($_SERVER["HTTP_REFERER"])) {

	$_REQUEST['uploadPath'] = $_SERVER['DOCUMENT_ROOT'].$uploadPath;

	if ($_REQUEST['cmd'] == 'write') {

		$_REQUEST = fileupload('moviename', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 동영상

		$r = $param->insert($_REQUEST);
		if ($r > 0) {
			$_REQUEST['startdate'] = substr($_REQUEST['startdate'], 0, 7).'-01';
			echo returnURLMsg($param->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), '정상적으로 저장되었습니다.');
		} else {
			echo returnURLMsg($param->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), '요청처리중 장애가 발생하였습니다.');
		}

	} else if($_REQUEST['cmd'] == 'reply'){

		$r = $param->reply($_REQUEST);
		if($r > 0){
			echo returnURLMsg($param->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), '정상적으로 저장되었습니다.');
		} else {
			echo returnURLMsg($objCparamommon->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), '요청처리중 장애가 발생하였습니다.');
		}

	} else if($_REQUEST['cmd'] == 'edit') {

		$_REQUEST = fileupload('moviename', $_SERVER['DOCUMENT_ROOT'].$uploadPath, $_REQUEST, false, $maxSaveSize);	// 동영상

		$r = $param->update($_REQUEST);
		if($r > 0){
			$_REQUEST['startdate'] = substr($_REQUEST['startdate'], 0, 7).'-01';
			echo returnURLMsg($param->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), '정상적으로 저장되었습니다.');
		} else {
			echo returnURLMsg($param->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), '요청처리중 장애가 발생하였습니다.');
		}

	} else if($_REQUEST['cmd'] == 'delete') {

			$r = $param->delete($_REQUEST['no']);	
			if($r > 0){
				$param['startdate'] = substr($param['startdate'], 0, 7).'-01';				
				echo returnURLMsg($param->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), '정상적으로 저장되었습니다.');
			} else {
				echo returnURLMsg($param->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), '요청처리중 장애가 발생하였습니다.');
			}
	}

} else {
	echo returnURLMsg($param->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), '요청처리중 장애가 발생하였습니다.');
}
	
?>