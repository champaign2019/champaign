<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/schedule/Schedule.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

	$param = new Schedule($pageRows, $tablename, $_REQUEST);
	$_REQUEST['orderby'] = $orderby; //정렬 배열 선언
	$data = $param->getData($_REQUEST);
	$enddate = explode(' ',$data['enddate']);
	$day1 = null;
	$day2 = null;
	$day1 = date("Y-m-d", strtotime(getToday()));
	$day2 = date("Y-m-d", strtotime($enddate[0]));
	if($day1 > $day2){
		$param->stateUpdate($_REQUEST);
	}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
<script type="text/javascript">
function goDelete(){
	if(confirm("삭제 하시겠습니까?")){
		document.location.href = "process.php?menu_fk=<?=$data['menu_fk']?>&$_REQUEST['cmd']=delete&no=<?=$data['no']?>&startdate=<?=$data[startdate]?>";
	} else {
		return false;
	}
}
</script>
</head>
<body>
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<div id="wrap">
	<!-- canvas -->
	<div id="canvas">
		<!-- S T A R T :: headerArea-->
		<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/top.php" ?>
		<!-- E N D :: headerArea--> 
		
		<!-- S T A R T :: containerArea-->
		<div id="container">
			<div id="content">
				<div class="con_tit">
					<h2><?=$pageTitle?> - [상세]</h2>
				</div>
				<!-- //con_tit -->
				<div class="con">
					<!-- 내용 : s -->
					<div id="bbs">
						<div id="bread">
							<table>
							<colgroup>
								<col width="16%" />
								<col width="34%" />
								<col width="16%" />
								<col width="34%" />
							</colgroup>
							<tbody>
								<tr>
									<th>이름</th>
									<td><?=$data['user_name']?></td>
									<th>작성일</th>
									<td><?=$data['registdate']?></td>
								</tr>
								<tr>
									<th>시작일</th>
									<td><?=$data['startdate']?></td>
									<th>종료일</th>
									<td><?=$data['enddate']?></td>
								</tr>
								<tr>
									<th>상태</th>
									<td colspan="3"><?=getHtmlRemove(getState($data['state']))?></td>
								</tr>
								<tr>
									<th>제목</th>
									<td colspan="3"><?=$data['title']?></td>
								</tr>
								<tr>
									<th>내용</th>
									<td colspan="3">
										<div class="in_txt">
										<? if (!empty($data['moviename'])) { ?>
										<p>
											<script type="text/javascript">
											tv_adplay_autosize("<?=$uploadPath?><?=$data[moviename]?>", "MoviePlayer");
											</script>
										</p>
										
										<? } ?>
										 <?=stripslashes($data['contents'])?>	
										</div>
									</td>
								</tr>
							</tbody>
							</table>
							<div class="btn">
								<? $_REQUEST['no'] = $data['no']; ?>
								<div class="btnLeft">
									<a class="btns gr_btn" href="<?=$param->getQueryString("index.php", 0, $_REQUEST)?>">목록</a>
								</div>
								<div class="btnRight">
									<a class="btns gr_btn" href="<?=$param->getQueryString("edit.php", $data['no'], $_REQUEST)?>">수정</a>
									<a class="btns gr_btn" href="javascript:;" onClick="goDelete();">삭제</a>
								</div>
							</div>
							<!--//btn-->
							<?
								if ($isComment) {
							?>
								<? include $_SERVER['DOCUMENT_ROOT']."/manage/board/comment/comment.php" ?><!-- 댓글 -->
							<?
								}
							?>	
						</div>
						<!-- //bread -->
					</div>
					<!-- //bbs --> 
					<!-- 내용 : e -->
				</div>
				<!--//con -->
			</div>
			<!--//content -->
		</div>
		<!--//container --> 
		<!-- E N D :: containerArea-->
	</div>
	<!--//canvas -->
</div>
<!--//wrap -->

</body>
</html>