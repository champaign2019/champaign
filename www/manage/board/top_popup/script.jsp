<script>
	jQuery(window).load(function(){
		var calendar = ['start_day', 'end_day'];
		calendar.forEach(function(i){
			initCal({id:i,type:"day",today:"y"});
		});
		$('[name=border_color]').colorPicker({pickerDefault: $('[name=border_color]').val()});
		$('[name=border_color_m]').colorPicker({pickerDefault: $('[name=border_color_m]').val()});
		$('[name=bg_color]').colorPicker({pickerDefault: $('[name=bg_color]').val()});
		$('[name=bg_color_m]').colorPicker({pickerDefault: $('[name=bg_color_m]').val()});
		goType($('[name=type]:checked').val());
		$(".img_alt").focus(function(){
			focusAltRemove($(this));
		});
		$(".img_alt").blur(function(){
			blurAltInsert($(this));
		});

		var $popup_device_type = $('[name=popup_device_type]');
		$popup_device_type.change(function() {
			pdt(this.value);
		});
		pdt($('[name=popup_device_type]:checked').val());
	});
	
	function pdt(_popup_device_type) {
		$('h3.pc_area').text('PC 팝업 정보');
		if ( _popup_device_type == 0 ) {
			$('.pc_area').show();
			$('.mobile_area').hide();
		}
		else if ( _popup_device_type == 1 ) {
			$('.pc_area').hide();
			$('.mobile_area').show();
		}
		else if ( _popup_device_type == 2 ) {
			$('h3.pc_area').text('PC&Mobile 팝업 정보');
			$('.pc_area').show();
			$('.mobile_area').hide();
		}
		else if ( _popup_device_type == 3 ) {
			$('.pc_area').show();
			$('.mobile_area').show();
		}
		goType($('[name=type]:checked').val());
	}
	
	function getMsg(msg) {
		var r;
		$.ajax({
			method : 'post'
			, url : '/ajax/getMsg.jsp'
			, async : false
			, data : {msg : msg}
			, success : function(data) {
				r = data.trim();
			}
		});
		return r;
	}
	
	function goSave() {
		var obj = document.frm;

		if(validation(obj)){
			return true;
		}
		else {
			return false;
		}
	}

	function goType(type) {
		if (type == "0") {
			$(".normalPopup").show();
			$(".size").show();
			$(".imagePopup").hide();
			$(".color").show();
			$(".borderDl").show();
			$(".bgDl").hide();
		} else if (type == "1") {
			$(".normalPopup").hide();
			$(".size").hide();
			$(".imagePopup").show();
			$(".color").show();
			$(".borderDl").hide();
			$(".bgDl").show();
		} else if (type == "2") {
			$(".normalPopup").show();
			$(".size").show();
			$(".imagePopup").hide();
			$(".color").hide();
		} else if (type == "3") {
			$(".normalPopup").hide();
			$(".size").hide();
			$(".imagePopup").show();
			$(".color").hide();
		}
	}

	function isLim(obj){
		if( $(obj).is(':checked') ){
			$(obj).closest('td').find('.colorPicker-picker').hide();
			$(obj).closest('td').find('[name=bg_color]').val('');
		}else{
			$(obj).closest('td').find('.colorPicker-picker').show();
		}
	}
</script>



















