<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Popup.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

$popup = new Popup($pageRows, $tablename, $_REQUEST);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/boardConfig/config.php" ?>
<? include "./script.jsp" ?>
<style>
	.colorPicker-picker {display:inline-block; margin-right:10px;}
</style>
</head>
<body> 
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<div id="wrap">
	<!-- canvas -->
	<div id="canvas">
		<!-- S T A R T :: headerArea-->
		<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/top.php" ?>
		<!-- E N D :: headerArea--> 
		
		<!-- S T A R T :: containerArea-->
		<div id="container">
			<div id="content">
				<div class="con_tit">
					<h2><?=$pageTitle?> - [<?=getMsg('th.write')?>]</h2>
				</div>
				<!-- //con_tit -->
				<div class="con">
					<!-- 내용 : s -->
					<div id="bbs">
						<div id="bread">
							<h3 class="minTitle">팝업 공통 정보</h3>
							<form method="post" name="frm" id="frm" action="<?=getSslCheckUrl($_SERVER['REQUEST_URI'], 'process.php')?>" enctype="multipart/form-data" onsubmit="return goSave();">
							<div class="table_wrap">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리 기본내용입니다.">
								<colgroup>
									<col width="15%" />
									<col width="35%" />
									<col width="15%" />
									<col width="35%" />
								</colgroup>
								<tbody>
									<?php
										if (!DEFAULT_PDT_USE) {
									?>
									<tr>
										<th scope="row"><label for="">팝업 노출 종류</label></th>
										<td colspan="3">
											<?php
												$popup_device_type = eval('return '.POPUP_DEVICE_TYPE.';');
												for ($i=0; $i<count($popup_device_type); $i++) {
											?>
											<label class="b_snor_r">
												<input data-value="팝업종류를 선택해 주세요." type="radio" name="popup_device_type" value="<?=$i ?>" <?=getChecked($i, '0') ?> /><i></i>
												<?=$popup_device_type[$i] ?>
											</label>
											<?php
												}
											?>                                                                 
										</td>
									</tr>
									<?php
										}
										else {
									?>
									<input type="hidden" name="popup_device_type" value="<?=DEFAULT_PDT_INDEX ?>" />
									<?php
										}
									?>
									<tr>
										<th scope="row"><label for="">시작일</label></th>
										<td>
											<input type="text" id="start_day" name="start_day" maxlength="10" class="inputTitle" value="<?=getToday()?>" title="시작일을 입력해주세요" />&nbsp;
											<span id="Calstart_dayIcon">
												<img src="/manage/img/calendar_icon.png" id="Calstart_dayIconImg" style="cursor:pointer;"/>
											</span>
										</td>
										<th scope="row"><label for="">종료일</label></th>
										<td>
											<input type="text" id="end_day" name="end_day" maxlength="10" class="inputTitle" value="<?=getToday()?>" title="종료일을 입력해주세요" />&nbsp;
											<span id="Calend_dayIcon">
												<img src="/manage/img/calendar_icon.png" id="Calend_dayIconImg" style="cursor:pointer;"/>
											</span>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for="">제목</label></th>
										<td colspan="3">
											<input type="text" id="title" name="title" class="input92p" title="제목을 입력해주세요"  data-value="제목을 입력해 주세요."/>
											<p class="marT05"><span class="color1">* 제목은 팝업 상단에 노출됩니다 (단. 일반/이미지레이어 팝업에서는 제목은 나타지 않습니다.)</span></p>	
										</td>
									</tr>
								</tbody>
							</table>
							</div>
							
							<h3 class="pc_area">PC 팝업 정보</h3>
							<div class="table_wrap">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리 기본내용입니다." class="pc_area">
								<colgroup>
									<col width="15%" />
									<col width="35%" />
									<col width="15%" />
									<col width="35%" />
								</colgroup>
								<tbody>
									<tr class="imagePopup">
										<th scope="row"><label for="">이미지</label></th>
										<td colspan="3">
											<input type="file" id="imagename" name="imagename" class="input92p" title="이미지파일을 업로드 해주세요." />	
										</td>
									</tr>
									<tr class="imagePopup">
										<th scope="row"><label for="">이미지 설명</label></th>
										<td colspan="3">
											<input type="text" id="image_alt" name="image_alt" class="input92p" title="이미지 설명을 입력해주세요." />	
										</td>
									</tr>
									<tr class="size">
										<th scope="row"><label for="">가로사이즈</label></th>
										<td colspan="3">
											<input type="text" name="popup_width" id="popup_width"  maxlength="4" onkeydown="isOnlyNumber(this)" onkeyup="isOnlyNumber(this)" class="inputLong"/>px 
										</td>
									</tr>
									<tr>
										<th scope="row"><label for="">상세보기 URL</label></th>
										<td colspan="3">
											<input type="text" id="relation_url" name="relation_url" class="input92p" value=""/>	
										</td>
									</tr>									
								</tbody>
							</table>
							</div>
							
							<h3 class="mobile_area" style="display:none;">Mobile 팝업 정보</h3>
							<div class="table_wrap">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리 기본내용입니다." class="mobile_area" style="display:none;">
								<colgroup>
									<col width="15%" />
									<col width="35%" />
									<col width="15%" />
									<col width="35%" />
								</colgroup>
								<tbody>
									<tr class="imagePopup">
										<th scope="row"><label for="">이미지</label></th>
										<td colspan="3">
											<input type="file" id="imagename_m" name="imagename_m" class="input92p" title="이미지파일을 업로드 해주세요." />	
										</td>
									</tr>
									<tr class="imagePopup">
										<th scope="row"><label for="">이미지 설명</label></th>
										<td colspan="3">
											<input type="text" id="image_alt_m" name="image_alt_m" class="input92p" title="이미지 설명을 입력해주세요." />	
										</td>
									</tr>
									<tr class="size">
										<th scope="row"><label for="">가로사이즈</label></th>
										<td colspan="3">
											<input type="text" name="popup_width_m" id="popup_width_m"  maxlength="4" onkeydown="isOnlyNumber(this)" onkeyup="isOnlyNumber(this)" class="inputLong"/>px 
										</td>
									</tr>
									<tr>
										<th scope="row"><label for="">상세보기 URL</label></th>
										<td colspan="3">
											<input type="text" id="relation_url_m" name="relation_url_m" class="input92p" value=""/>	
										</td>
									</tr>									
								</tbody>
							</table>
							</div>

							<input type="hidden" name="cmd" value="write" />
							</form>
							<div class="btn">
								<div class="btnLeft">
									<a class="btns" href="<?=$popup->getQueryString('index.php', 0, $_REQUEST)?>"><strong><?=getMsg('btn.list')?></strong></a>
								</div>
								<div class="btnRight">
									<a class="btns" href="javascript:;" onclick="$('#frm').submit();"><strong><?=getMsg('btn.save')?></strong></a>
								</div>
							</div>
							<!--//btn-->
						</div>
						<!-- //bread -->
					</div>
					<!-- //bbs --> 
					<!-- 내용 : e -->
				</div>
				<!--//con -->
			</div>
			<!--//content -->
		</div>
		<!--//container --> 
		<!-- E N D :: containerArea-->
	</div>
	<!--//canvas -->
</div>
<!--//wrap -->

</body>
</html>