<?
$pageTitle 	 	= "랜딩 신청 관리";				// 게시판 명
$tablename  		= "landing_consult";					// DB 테이블 명
$category_tablename  	= "";					// 카테고리 테이블 명
$uploadPath 		= "/upload/landing/";			// 파일, 동영상 첨부 경로
$maxSaveSize	  		= 50*1024*1024;					// 50Mb
$pageRows	  		= 10;							// 보여질 게시글 수
$gradeno		  		= 2;							// 관리자 권한 기준 [지점 사용시]


$islog 			= false; 						//유입(방문자통계 사용여부)
$usePerfect 		= false; 						//조회수
		
/* 화면 필드 생성 여부 */
$unickNum		= false;							// 고유번호 사용유무  
$branch	  		= false;							// 지점 사용유무  
$clinic	  		= false;							// 진료과목 지점별 사용유무	
$doctor	  		= false;							// 의료진 사용유무	
$useMovie  	 	= false;						// 파일 첨부 [사용: true, 사용 안함 : true]
$useRelationurl	= false;						// 관련URL 첨부 [사용: true, 사용 안함 : true]
$useEmail 		= false;							// 이메일 사용유무
$useTel 			= false;							// 연락처 사용유무
$useCell 		= false;							// 핸드폰 사용유무
$useSmsChk 		= false;							// sms 수신 사용유무
$useEmailChk		= false;							// email 수신 사용유무
$usePassword		= false;							// 비밀번호 사용유무
$useBtn 			= false;							// 글쓰기, 수정페이지, 삭제 가능 유무		
$isComment 		= false;							// 댓글		
$isListImage		= false;							//상세에 목록이미지 사용유무
$listImageResize = false;						//목록이미지 리사이즈

$istop 			= false;
$ismain 			= false;
$isnew 			= false;

$timeDate	  	= false;						// 날짜 or 날짜 and 시간	
$userCon	  		= false;							// 조회수 카운트 [관리자 페이지:true, 사용자 페이지:true, 조회수:사용여부]
$registdateGubun = false;							//날짜 수정
$useFile  		= false;							// 파일 첨부 [사용: true, 사용 안함 : true]
$textAreaEditor	= true;							// 에디터 사용여부
$editorIgnore= "";								//에디터 미사용 배열 구분자 ex : '|0|1|'						
$gallery 		= false; 						//목록이미지
$editImgUse 		= false;							//목록이미지 없으면 에디터이미지 [사용 : true, 사용안함 : false]
$useSecret	  	= false;							// 비밀글 사용 여부	

$useAnswer		= false;							// 답변필드사용여부
$useWriteAnswer	= false;							// 답변글사용여부
$answer 				= useWriteAnswer ? 1 : 0;							

$searchPosition1 	= 0; 							//검색 위치 0 : 위 1 아래 
$searchPosition2 	= "center"; 					//검색 위치 center : 가운데 right : 오른쪽  left 왼쪽 


$useFileCount  	= 10;							// 파일 첨부 가능 갯수
$useFileDrag    =	false;

$userAgent = $_SERVER["HTTP_USER_AGENT"];
if($useFileDrag){
    if ( preg_match("/MSIE 6.0[0-9]*/", $userAgent) ) {
        $useFileDrag    = false;
    }else if ( preg_match("/MSIE 7.0*/", $userAgent) ) {
        $useFileDrag    = false;
    }else if ( preg_match("/MSIE 8.0*/", $userAgent) ) {
        $useFileDrag    = false;
    }else if ( preg_match("/MSIE 9.0*/", $userAgent) ) {
        $useFileDrag    = false;
    }else if ( preg_match("/MSIE 10.0*/", $userAgent) ) {
        $useFileDrag    = false;
	}
}


/* 문자메세지/이메일메세지 */
$getUserMsg = "상담요청이 정상적으로 요청되었습니다. 감사합니다.";			//사용자 보내는 메세지 - 빈값이면 문자 안나감
$getAdminMsg = "님의 상담이 요청되었습니다. 확인하시고 답변 부탁 드립니다.";	//관리자 보내는 메세지 - 빈값이면 문자 안나감

$getAnswerUserMsg = "";											//사용자 답변 보내는 메세지 - 빈값이면 문자 안나감
$getAnswerAdminMsg = "";											//관리자 답변 보내는 메세지 - 빈값이면 문자 안나감

$getUserEmail = "상담 요청이 정상적으로 요청되었습니다.";					//사용자 보내는 이메일 - 빈값이면 문자 안나감
$getAnswerUserEmail = "";											//관리자 보내는 이메일 - 빈값이면 문자 안나감

$orderby = Array( "top desc","registdate desc" );					    //order by 배열
if($answer > 0){
	$orderby = Array( "top desc","gno DESC","ono ASC","registdate desc" );					    //order by 배열
}
?>
