<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Hospital.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Clinic.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Common.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

$common = new Common($pageRows, $tablename, $_REQUEST);
$data = $common->getData($_REQUEST, false);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/boardConfig/config.php" ?>

<script>
	$(window).load(function(){
				
		<?if(registdateGubun){?>
		// 달력
		initCal({id:"registdate",type:"day",today:"y",timeYN:"y"});
		<?}?>
	});
	
	function goSave(obj) {
		
		
		if(validation(obj)){
			
			$("#frm").submit();	
			
		}
	}
	
</script>
</head>
<body> 
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<div id="wrap">
	<!-- canvas -->
	<div id="canvas">
		<!-- S T A R T :: headerArea-->
		<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/top.php" ?>
		<!-- E N D :: headerArea--> 
		
		<!-- S T A R T :: containerArea-->
		<div id="container">
			<div id="content">
				<div class="con_tit">
					<h2><?=$pageTitle?> - [<?=getMsg('th.write')?>]</h2>
				</div>
				<!-- //con_tit -->
				<div class="con">
					<!-- 내용 : s -->
					<div id="bbs">
						<div id="bread">
							<?
							if (branch) { 
							?>
							<h3><?=getMsg('th.branch_sel')?></h3>
							<?
							}
							?>
							<form method="post" name="frm" id="frm" action="<?=getSslCheckUrl($_SERVER['REQUEST_URI'], 'process.php')?>" enctype="multipart/form-data" >
							<div class="table_wrap">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리 기본내용입니다.">
								<colgroup>
									<col width="15%" />
									<col width="35%" />
									<col width="15%" />
									<col width="35%" />
								</colgroup>
								<tbody>
									<?
									if ($branch) {

										$hospital = new Hospital(999, $_REQUEST);
										$hResult = $hospital->branchSelect();
									?>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.hospital_fk')?></label></th>
										<td colspan="3">
											
											<select name="hospital_fk" id="hospital_fk" title="지점 선택" data-value="<?=getMsg('alert.text.shospital_fk')?>">
												<option value=""><?=getMsg('th.branch_sel')?></option>
												<? while ($row=mysql_fetch_assoc($hResult)) { ?>
												
												<option value="<?=$row['no']?>" <?=getSelected($data['hospital_fk'], $row['no']) ?>><?=$row['name']?></option>
												<? } ?>
											</select>
										
										</td>
									</tr>
									<?
									}
									?>
									<?
									if($clinic){
										
										$clinicObj = new Clinic(999, $_REQUEST);
										$clinicList = $clinicObj->selectBoxList(0,$data['hospital_fk'],$data['clinic_fk']);										
									?>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.clinic_fk')?></label></th>
										<td colspan="3">
											<select name="clinic_fk" id="clinic_fk" data-value="<?=getMsg('alert.text.shospital_fk')?>">
												<?= $clinicList ?>
											</select>
										</td>
									</tr>
									<?
									}
									?>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.name')?></label></th>
										<td colspan="<?=false ? 1 : 3?>">
											<input type="text" name="name" id="name" class="w100m25" value="" data-value="작성자를 선택해 주세요."/>
										</td>
										<?
										if(false){
										?>
										<th scope="row"><label for=""><?=getMsg('th.password')?></label></th>
										<td>
											<input type="password" id="password" name="password" value="" class="w100m25" title="비밀번호를 입력해주세요" data-value="비밀번호를 입력해 주세요." />
										</td>
										<?
										}
										?>
									</tr>
									<?
									if($useCell || $useEmail){
									?>
									<tr>
										<?
										if($useCell){
										?>
										<th scope="row"><label for=""><?=getMsg('th.cell')?></label></th>
										<td colspan="<?=$useCell ? 1 : 3?>">
											<input type="text" id="cell" name="cell" value="" class="w100m25" title="휴대폰 번호를 입력해주세요." onkeyup="isOnlyNumberNotHypen(this);"/>
										</td>
										<?
										}
										?>
										<?
										if($useEmail){
										?>
										<th scope="row"><label for=""><?=getMsg('th.email')?></label></th>
										<td colspan="<?=$useEmail ? 1 : 3?>">
											<input type="text" id="email" name="email" value="" title="이메일주소를 입력해주세요." onkeyup="isValidEmailing(this);" class="w100m25"/>
										</td>
										<?
										}
										?>
									</tr>
									<?
									}
									?>
									<? if($usePerfect || $registdateGubun) { ?>
									<tr>
										<?
										if($usePerfect){
										?>
										<th scope="row"><label for=""><?=getMsg('th.userCon')?></label></th>
										<td colspan="<?=$registdateGubun ? 1 : 3?>">
											<input type="text" id="readno" name="readno" value="" title="조회수를 입력해주세요" style="width:50px;" onkeyup="isOnlyNumber(this);"/>
										</td>
										<?
										}
										?>
										<?
										if($registdateGubun){
										?>
										<th scope="row"><label for=""><?=getMsg('th.registdate')?></label></th>
										<td colspan="<?=$usePerfect ? 1 : 3?>">
											<input type="text" id="registdate" name="registdate" class="w100m25" value="" title="등록일을 입력해주세요" />
										</td>
										<?
										}
										?>
									</tr>
									<? 
									}
									?>
									<?
									if($useSmsChk || $useEmailChk){
									?>
									<tr>
										<?
										if($useSmsChk){
										?>
										<th scope="row"><label for=""><?=getMsg('th.telconsult')?></label></th>
										<td colspan="<?=$useSmsChk ? 1 : 3?>">
											<label for="iscall1" class="b_snor_r"><input type="radio" id="iscall1" name="iscall" value="1" <?=getChecked("1", $data['iscall'])?> data-value="<?=getMsg('alert.text.telconsult')?>"/><i></i>
											<span class="blue_col vaM"><?=getMsg('lable.radio.request_y')?></span></label>
											<label for="iscall0" class="b_snor_r marl15"><input type="radio" id="iscall0" name="iscall" value="0" <?=getChecked("0", $data['iscall'])?> data-value="<?=getMsg('alert.text.telconsult')?>"/><i></i><span class="red_col vaM">
											<?=getMsg('lable.radio.request_n')?></span></label>
										</td>
										<?
										}
										?>
										<?
										if($useEmailChk){
										?>
										<th scope="row"><label for=""<?=getMsg('th.answeremail')?></label></th>
										<td colspan="<?=$useEmailChk ? 1 : 3?>">
											<label for="isemail1"  class="b_snor_r"><input type="radio" id="isemail1" name="isemail" value="1" <?=getChecked("1", $data['ismail'])?> data-value="<?=getMsg('alert.text.emailconsult')?>"/><i></i>
											<span class="blue_col vaM"><?=getMsg('lable.radio.request_y')?></span></label>
											<label for="isemail0" class="b_snor_r marl15"><input type="radio" id="isemail0" name="isemail" value="0" <?=getChecked("0", $data['ismail'])?> data-value="<?=getMsg('alert.text.emailconsult')?>"/><i></i><span class="red_col vaM">
											<?=getMsg('lable.radio.request_n')?></span></label>
										</td>
										<?
										}
										?>
									</tr>
									<?
									}
									?>

									<?
									
									if($istop || $isnew){
									?>
									<tr>
										<?
										if($istop){
										?>
										<th scope="row"><label for=""><?=getMsg('th.topnotice')?></label></th>
										<td colspan="<?=$isnew ? 1 : 3?>">
											<label for="top1" class="b_snor_r"><input type="radio" id="top1" name="top" value="1" <?=getChecked("1", $data['top'])?> data-value="<?=getMsg('alert.text.topnotice')?>"/>
											<i></i>
											<span class="blue_col vaM"><?=getMsg('lable.radio.notice_y')?></span></label>
											<label for="top0" class="b_snor_r marl15"><input type="radio" id="top0" name="top" value="0" <?=getChecked("0", $data['top'])?> data-value="<?=getMsg('alert.text.topnotice')?>"/><i></i><span class="red_col vaM">
											<?=getMsg('lable.radio.notice_n')?></span></label>
											<img src="/manage/img/question_btn.gif" class="helpComment" id="Top" alt="도움말 이미지"/>
										</td>
										<?
										}
										?>
										<?
										if($isnew){
										?>
										<th scope="row"><label for=""><?=getMsg('th.newicon')?></label></th>
										<td colspan="<?=$istop ? 1 : 3?>">
											<label for="newicon1" class="b_snor_r"><input type="radio" id="newicon1" name="newicon" value="1" <?=getChecked("1", $data['newicon'])?> data-value="NEW아이콘을 선택해 주세요."/><i></i><span class="blue_col vaM">
											<?=getMsg('lable.radio.always')?></span></label>
											<label for="newicon2" class="b_snor_r marl15"><input type="radio" id="newicon2" name="newicon" value="2" <?=getChecked("2", $data['newicon'])?> data-value="NEW아이콘을 선택해 주세요."/><i></i><span class="gre_col vaM">
											<?=getMsg('lable.radio.oneday')?></span></label>
											<label for="newicon0" class="b_snor_r marl15"><input type="radio" id="newicon0" name="newicon" value="0" <?=getChecked("0", $data['newicon'])?> data-value="NEW아이콘을 선택해 주세요."/><i></i><span class="red_col vaM">
											<?=getMsg('lable.radio.noicon')?></span></label>
											<img src="/manage/img/question_btn.gif" class="helpComment" id="New" alt="도움말 이미지"/>
										</td>
										<?
										}
										?>
									</tr>
									<?
									}
									?>
									<?
									if($ismain || $useSecret){
									?>
									<tr>
										<?
										if($ismain){
										?>
										<th scope="row"><label for=""><?=getMsg('th.maincontents')?></label></th>
										<td colspan="<?=$useSecret ? 1 : 3?>">
											<label for="main1" class="b_snor_r"><input type="radio" id="main1" name="main" value="1" style="width:14px" <?=getChecked("1", $data['main'])?> data-value="메인게시물을 선택해 주세요."/>
											<i></i><span class="blue_col vaM">
											<?=getMsg('lable.radio.main_y')?></span></label>
											<label for="main0" class="b_snor_r marl15"><input type="radio" id="main0" name="main" value="0" style="width:14px" <?=getChecked("0", $data['main'])?> data-value="메인게시물을 선택해 주세요."/>
											<i></i><span class="red_col vaM">
											<?=getMsg('lable.radio.main_n')?></span></label>
											<img src="/manage/img/question_btn.gif" class="helpComment" id="Main" alt="도움말 이미지"/>		
										</td>
										<?
										}
										?>
										<?
										if($useSecret){
										?>
										<th scope="row"><label for=""><?=getMsg('th.secret1')?></label></th>
										<td colspan="<?=$ismain ? 1 : 3?>">
											<label for="secret1" class="b_snor_r"><input type="radio" id="secret1" name="secret" value="0" style="width:14px" <?=getChecked("0", $data['secret'])?> data-value="<?=getMsg('alert.text.secret')?>"/><i></i>
											<span class="blue_col vaM">
											<?=getMsg('lable.td.public')?></span></label>
											<label for="secret0"  class="b_snor_r marl15"><input type="radio" id="secret0" name="secret" value="1" style="width:14px" <?=getChecked("1", $data['secret'])?> data-value="<?=getMsg('alert.text.secret')?>"/><i></i><span class="red_col vaM">
											<?=getMsg('lable.td.secret')?></span></label>
											<img src="/manage/img/question_btn.gif" class="helpComment" id="Secret" alt="도움말 이미지"/>
										</td>
										<?
										}
										?>
									</tr>
									<?
									}
									?>
									<tr>
										<td colspan="4" height="0" class="bline"></td>
									</tr>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.title')?></label></th>
										<td colspan="3">
											<input type="text" id="title" name="title" class="input92p" title="제목을 입력해주세요" data-value="<?=getMsg('alert.text.title')?>" />	
										</td>
									</tr>
									<tr>
										<td colspan="4">
											<textarea id="contents" name="contents" title="내용을 입력해주세요" style="width:100%;" data-value="<?=getMsg('alert.text.contents')?>"><?=stripslashes($data['contents'])?><br/><br/></textarea>	
										</td>
									</tr>
									<? if ($useFile) { ?>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.filename')?></label></th>
										<td colspan="3">
											<input type="file" id="filename" name="filename" class="input92p" title="첨부파일을 업로드 해주세요." />	
										</td>
									</tr>
									<? } ?>
									<? if ($useRelationurl) { ?>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.relation_url')?></label></th>
										<td colspan="3">
											<input type="text" id="relation_url" name="relation_url" class="input92p" title="" />
										</td>
									</tr>
									<? } ?>
									<? if ($useMovie) { ?>
									<tr>
										<th scope="row"><label for=""><?=getMsg('th.moviename')?></label></th>
										<td colspan="3">
											<p><?=getMsg('message.tbody.movie')?></p>
											<input type="file" id="moviename" name="moviename" class="input92p" title="첨부파일을 업로드 해주세요." />	
										</td>
									</tr>
									<? } ?>
								</tbody>
							</table>
							</div>
							
								<input type="hidden" name="cmd" id="cmd" value="reply"/>
								<input type="hidden" name="gno" id="gno" value="<?=$data['gno']?>"/>
								<input type="hidden" name="ono" id="ono" value="<?=$data['ono']?>"/>
								<input type="hidden" name="no" id="no" value="<?=$data['no']?>"/>
								<input type="hidden" name="nested" id="nested" value="<?=$data['nested']?>"/>
								<? if (!$branch) { ?>
								<input type="hidden" name="hospital_fk" id="hospital_fk" value="<?=$data['hospital_fk']?>"/>
								<?}?>
							<div class="btn">
								<div class="btnLeft">
									<a class="btns" href="<?=$common->getQueryString('index.php', 0, $_REQUEST)?>"><strong><?=getMsg('btn.list')?></strong></a>
								</div>
								<div class="btnRight">
									<a class="btns" href="#" onclick="goSave(this);"><strong><?=getMsg('btn.save')?></strong></a>
								</div>
							</div>
							<!--//btn-->
							</form>
						</div>
						<!-- //bread -->
					</div>
					<!-- //bbs --> 
					<!-- 내용 : e -->
				</div>
				<!--//con -->
			</div>
			<!--//content -->
		</div>
		<!--//container --> 
		<!-- E N D :: containerArea-->
	</div>
	<!--//canvas -->
</div>
<!--//wrap -->

</body>
</html>