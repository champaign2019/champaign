<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Popup.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

$popup = new Popup($pageRows, $tablename, $category_tablename, $_REQUEST);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/boardConfig/config.php" ?>

<style>
	.colorPicker-picker {display:inline-block; margin-right:10px;}
</style>

<script>
	jQuery(window).load(function(){
		initCal({id:"start_day",type:"day",today:"y"});
		initCal({id:"end_day",type:"day",today:"y"});

//		$('#bg_color').colorPicker({pickerDefault: "000000"});
	});
	
	$(document).ready(function(){
		$(".img_alt").focus(function(){
			focusAltRemove($(this));
		});
		
		$(".img_alt").blur(function(){
			blurAltInsert($(this));
		});
		
		$('#border_color').colorPicker();
		$('#bg_color').colorPicker({pickerDefault: "000000"});
		goType(2);		
	});
	
	function goSave() {
		var f= document.frm;
		
	 	if (getRadioValue(f.type) == "0" || getRadioValue(f.type) == "2") {
			if ($("#popup_width").val() == false || $("#popup_width").val() == '0') {
				alert('가로사이즈 입력하세요.(0 입력 불가)');
				$("#popup_width").focus();
				return false;
			} 
			
			if ($("#popup_height").val() == false || $("#popup_height").val() == '0') {
				alert('세로사이즈를 입력하세요.(0 입력 불가)');
				$("#popup_height").focus();
				return false;
			} 
		} 
		if ($("#area_left").val() == false) {
			alert('가로위치를 입력하세요.');
			$("#area_left").focus();
			return false;
		} 
		if ($("#area_top").val() == false) {
			alert('세로위치를 입력하세요.');
			$("#area_top").focus();
			return false;
		} 
		if ($("#start_day").val() == false || $("#start_day").val().length < 9) {
			alert('시작일을 입력하세요.');
			$("#start_day").focus();
			return false;
		}
		if ($("#end_day").val() == false || $("#end_day").val().length < 9) {
			 alert('종료일 입력하세요.');
			$("#end_day").focus();
			return false;
		}
		if ($("#title").val() == "") {
			alert('제목을 입력해 주세요.');
			$("#title").focus();
			return false;
		}
		if (getRadioValue(f.type) == "0" || getRadioValue(f.type) == "2") {
			if ($("#contents").val() == "") {
				alert('내용을 입력해 주세요.');
				$("#contents").focus();
				return false;
			}
		} else {
			if ($("#imagename").val() == "") {
				alert('이미지를 선택하세요.');
				return false;
			} 

			if (checkImgFormatPopup(document.getElementById("imagename"))) {
				$("#imagename").focus();
				return false;
			} 
		}
		return true;
	}

	function goType(type) {
		if (type == "0") {
			$(".normalPopup").show();
			$("#size").show();
			$(".imagePopup").hide();
			$("#fileMovie").hide();
			$(".color").show();
			$("#borderDl").show();
			$("#bgDl").hide();
		} else if (type == "1") {
			$(".normalPopup").hide();
			$("#size").hide();
			$(".imagePopup").show();
			$("#fileMovie").hide();
			$(".color").show();
			$("#borderDl").hide();
			$("#bgDl").show();
		} else if (type == "2") {
			$(".normalPopup").show();
			$("#size").show();
			$(".imagePopup").hide();
			$("#fileMovie").show();
			$(".color").hide();
		} else if (type == "3") {
			$(".normalPopup").hide();
			$("#size").hide();
			$(".imagePopup").show();
			$("#fileMovie").hide();
			$(".color").hide();
		}
	}

	function isLim(is){
		if(is.checked){
			$(".colorPicker-picker")[1].style.display = "none"; 
			$("#bg_color").val("");
		}else{
			$(".colorPicker-picker")[1].style.display = "block"; 
			$(".colorPicker-picker")[1].style.display = "inline-block";
		}
	}

</script>
</head>
<body> 
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<div id="wrap">
	<!-- canvas -->
	<div id="canvas">
		<!-- S T A R T :: headerArea-->
		<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/top.php" ?>
		<!-- E N D :: headerArea--> 
		
		<!-- S T A R T :: containerArea-->
		<div id="container">
			<div id="content">
				<div class="con_tit">
					<h2><?=$pageTitle?> - [<?=getMsg('th.write')?>]</h2>
				</div>
				<!-- //con_tit -->
				<div class="con">
					<!-- 내용 : s -->
					<div id="bbs">
						<div id="bread">
							<form method="post" name="frm" id="frm" action="<?=getSslCheckUrl($_SERVER['REQUEST_URI'], 'process.php')?>" enctype="multipart/form-data" onsubmit="return goSave();">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리 기본내용입니다.">
								<colgroup>
									<col width="15%" />
									<col width="35%" />
									<col width="15%" />
									<col width="35%" />
								</colgroup>
								<tbody>
									<tr>
										<th scope="row"><label for="">팝업종류</label></th>
										<td colspan="3">
											<label for="type2"  class="b_snor_r"><input name="type" type="radio" id="type2" value="2" onclick="goType('2');"onfocus="goType('2');" checked data-value="팝업종류를 선택해 주세요."/><i></i>
											일반 팝업</label>                                                                 
											<label for="type3" class="b_snor_r marl15"><input name="type" type="radio" id="type3" value="3" onclick="goType('3');"onfocus="goType('3');" data-value="팝업종류를 선택해 주세요."/><i></i>
											이미지 팝업</label>                                                                
											<label for="type0" class="b_snor_r marl15"><input name="type" type="radio" id="type0" value="0" onclick="goType('0');"onfocus="goType('0');" data-value="팝업종류를 선택해 주세요."/><i></i>
											일반레이어 팝업</label>                                                              
											<label for="type1" class="b_snor_r marl15"><input name="type" type="radio" id="type1" value="1" onclick="goType('1');"onfocus="goType('1');" data-value="팝업종류를 선택해 주세요."/><i></i>
											이미지레이어 팝업</label>										
										</td>
									</tr>
									<tr id="size">
										<th scope="row"><label for="">가로사이즈</label></th>
										<td>
											<input type="text" name="popup_width" id="popup_width"  maxlength="4" onkeydown="isOnlyNumber(this)" onkeyup="isOnlyNumber(this)" class="inputLong"/>px 
										</td>
										<th scope="row"><label for="">세로사이즈</label></th>
										<td>
											<input type="text" name="popup_height" id="popup_height"  maxlength="4" onkeydown="isOnlyNumber(this)" onkeyup="isOnlyNumber(this)" class="inputLong"/>px 
										</td>
									</tr>
									<tr id="borderDl" class="color">
										<th scope="row"><label for="">테두리 색</label></th>
										<td colspan="3"><input id="border_color" name="border_color" value=""/></td>
									</tr>
									<tr id="bgDl" class="color">
										<th scope="row"><label for="">배경색</label></th>
										<td colspan="3">
											<input id="bg_color" name="bg_color" value="" style="width:16px;"/><input type="checkbox" name="isLimpid" id="isLimpid"  onclick="isLim(this)"/> <label for="isLimpid">투명색</label>
<!-- 											<input id="bg_color" name="bg_color" value=""/> -->
										</td>
									</tr>
									<tr>
										<th scope="row"><label for="">가로위치</label></th>
										<td><input type="text" name="area_left" id="area_left"  maxlength="4"  onkeydown="isOnlyNumber(this)" onkeyup="isOnlyNumber(this)" class="inputLong"/>px</td>
										<th scope="row"><label for="">세로위치</label></th>
										<td><input type="text" name="area_top" id="area_top"  maxlength="4"  onkeydown="isOnlyNumber(this)" onkeyup="isOnlyNumber(this)" class="inputLong"/>px</td>
									</tr>
									<tr>
										<th scope="row"><label for="">시작일</label></th>
										<td>
											<input type="text" id="start_day" name="start_day" maxlength="10" class="inputTitle" value="<?=getToday()?>" title="시작일을 입력해주세요" />&nbsp;
											<span id="Calstart_dayIcon">
												<img src="/manage/img/calendar_icon.png" id="Calstart_dayIconImg" style="cursor:pointer;"/>
											</span>
										</td>
										<th scope="row"><label for="">종료일</label></th>
										<td>
											<input type="text" id="end_day" name="end_day" maxlength="10" class="inputTitle" value="<?=getToday()?>" title="종료일을 입력해주세요" />&nbsp;
											<span id="Calend_dayIcon">
												<img src="/manage/img/calendar_icon.png" id="Calend_dayIconImg" style="cursor:pointer;"/>
											</span>
										</td>
									</tr>
									<tr>
										<td colspan="4" height="0" class="bline"></td>
									</tr>
									<tr>
										<th scope="row"><label for="">제목</label></th>
										<td colspan="3">
											<input type="text" id="title" name="title" class="input92p" title="제목을 입력해주세요"  data-value="제목을 입력해 주세요."/>
											<p class="marT05"><span class="color1">* 제목은 팝업 상단에 노출됩니다 (단. 일반/이미지레이어 팝업에서는 제목은 나타지 않습니다.)</span></p>	
										</td>
									</tr>
									<tr class="normalPopup">
										<td colspan="4">
											<textarea id="contents" name="contents" title="내용을 입력해주세요" style="width:100%;"></textarea>	
										</td>
									</tr>
									<tr class="imagePopup" style="display:none;">
										<th scope="row"><label for="">이미지</label></th>
										<td colspan="3">
											<input type="file" id="imagename" name="imagename" class="input92p" title="이미지파일을 업로드 해주세요." />	
										</td>
									</tr>
									<tr class="imagePopup" style="display:none;">
										<th scope="row"><label for="">이미지 설명</label></th>
										<td colspan="3">
											<input type="text" id="img_alt" name="img_alt" class="input92p" title="이미지 설명을 입력해주세요." />	
										</td>
									</tr>
									<tr>
										<th scope="row"><label for="">상세보기 URL</label></th>
										<td colspan="3">
											<input type="text" id="relation_url" name="relation_url" class="input92p" value=""/>	
										</td>
									</tr>									
								</tbody>
							</table>
							<input type="hidden" name="cmd" value="write" />
							</form>
							<div class="btn">
								<div class="btnLeft">
									<a class="btns" href="<?=$popup->getQueryString('index.php', 0, $_REQUEST)?>"><strong><?=getMsg('btn.list')?></strong></a>
								</div>
								<div class="btnRight">
									<a class="btns" href="javascript:;" onclick="$('#frm').submit();"><strong><?=getMsg('btn.save')?></strong></a>
								</div>
							</div>
							<!--//btn-->
						</div>
						<!-- //bread -->
					</div>
					<!-- //bbs --> 
					<!-- 내용 : e -->
				</div>
				<!--//con -->
			</div>
			<!--//content -->
		</div>
		<!--//container --> 
		<!-- E N D :: containerArea-->
	</div>
	<!--//canvas -->
</div>
<!--//wrap -->

</body>
</html>