<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Popup.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";
include "config.php";

$popup = new Popup($pageRows, $tablename, $category_tablename, $_REQUEST);
$rowPageCount = $popup->getCount($_REQUEST);
$result = rstToArray($popup->getList($_REQUEST));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/headHtml.php" ?>
<script>
function groupDelete() {	
	if ( $('[name^=no]:checked').length > 0 ) {
		document.frm.submit();
	} else {
		alert("<?=getMsg('alert.text.delete')?>");
	}
}

function goSearch() {
	$("#searchForm").submit();
}
function ShowDiv(road) {
	$("#"+road).show();
}

var ns4=document.layers;
var ie4=document.all;
var ns6=document.getElementById&&!document.all;
var zCount = 9999;
</script>
</head>
<body> 
<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/common.php" ?>
<div id="wrap">
	<!-- canvas -->
	<div id="canvas">
		<!-- S T A R T :: headerArea-->
		<? include $_SERVER['DOCUMENT_ROOT']."/manage/include/top.php" ?>
		<!-- E N D :: headerArea--> 
		
		<!-- S T A R T :: containerArea-->
		<div id="container">
			<div id="content">
				<div class="con_tit">
					<h2><?=$pageTitle?> - [<?=getMsg("th.list")?>]</h2>
				</div>
				<!-- //con_tit -->
				<div class="con">
					<!-- 내용 : s -->
					<div id="bbs">
						<div id="blist">
							<p><span><strong><?=getMsg('th.total')?> <?=$rowPageCount[0]?><?=getMsg('th.amount')?></strong>  |  <?=$popup->reqPageNo?>/<?=$rowPageCount[1]?><?=getMsg('th.page')?></span></p>
							<form name="frm" id="frm" action="process.php" method="post">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="관리자 관리목록입니다.">
								<colgroup>
									<col class="w3" />
									<col class="w4" />
									<col class="" />
									<col class="w10" />
									<col class="w10" />
									<col class="w10" />
								</colgroup>
								<thead>
									<tr>
										<th scope="col" class="first">
										<label class="b_nor_c"><input type="checkbox" name="allChk" id="allChk" onClick="check(this, document.getElementsByName('no[]'))"/><i></i>
										</label>
										</th>
										<th scope="col"><?=getMsg("th.no")?></th>
										<th scope="col"><?=getMsg("th.title")?></th> 
										<th scope="col">상태</th> 
										<th scope="col"><?=getMsg("th.registdate")?></th> 
										<th scope="col" class="last">팝업확인</th>
									</tr>
								</thead>
								<tbody>
								<? if ($rowPageCount[0] == 0) { ?>
									<tr>
										<td class="first" colspan="6">등록된 팝업이 없습니다.</td>
									</tr>
								<?
									 } else {
										$targetUrl = "";
										$topClass = "";

										for($i = 0; $i < count($result); $i++) {
											$row = $result[$i];
											$targetUrl = "style='cursor:pointer;' onclick=\"location.href='".$popup->getQueryString('edit.php', $row[no], $_REQUEST)."'\"";
											
											$tempContents = $row[contents];
											$detailBtn = "";
											if ($row[relation_url]) {
												$detailBtn = "<img src=\"/img/btn_detail.gif\" align=\"absmiddle\" style=\"border:0; margin:2 0 0 0;  text-align:right; cursor:pointer;\" onclick=\"window.open('http://".$row[relation_url]."','_blank','height=600,width=800,top=50,left=50,toolbar=1, directories=1, status=1, menubar=1, scrollbars=1, resizable=1,location=1')\">&nbsp;";
											}
											if ($row[type] == '1') {
												$tempContents = "<img src='".$uploadPath.$row[imagename]."' style='border:0; cursor:pointer;' ";
												if ($row[relation_url]) {
													$tempContents += "onclick=\"window.open('http://".$row[relation_url]."','_blank','height=600,width=800,top=50,left=50,toolbar=1, directories=1, status=1, menubar=1, scrollbars=1, resizable=1,location=1')\"";
												}
												$tempContents .= ">";
												$detailBtn = "";
											}												
								?>
									<tr>
										<td class="first">
										<label class="b_nor_c"><input type="checkbox" name="no[]" id="no" value="<?=$row[no]?>"/><i></i>
										</label>
										</td>
										<td <?=$targetUrl?>><?=$rowPageCount[0] - (($popup->reqPageNo-1)*$pageRows) - $i?></td>
										<td <?=$targetUrl?> class="title" >
											<a href="<?=$popup->getQueryString('edit.php', $row[no], $_REQUEST)?>">
											<?=$row[title]?>
											</a>
										</td>
										<td <?=$targetUrl?>><?=getPopupState($row[state])?></td>
										<td <?=$targetUrl?>><?=$row[registdate]?></td>
										<td class="last">
											<? if ($row[type] == '0' || $row[type] == '1') { ?>
											<p style="cursor:pointer" class="blue_btn consl" onclick="startTime('divPop<?=$row[no]?>', 'popMain<?=$row[no]?>', '<?=$row[area_top]?>', '<?=$row[area_left]?>', '<?=$row[popup_width]+12?>', '<?=$row[popup_height]?>', '0');ShowDiv('showimage<?=$i?>');">팝업확인</p>
											<? } else { ?>
											<p style="cursor:pointer" class="blue_btn consl" onclick="window.open('/include/popup/popup.php?no=<?=$row[no]?>','divPop<?=$row[no]?>','width=<?=$row[popup_width]?>, height=<?=$row[popup_height]+20?>, top=<?=$row[area_top]?>, left=<?=$row[area_left]?>,toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no');">팝업확인</p>
											<? } ?>
											
										</td>
									</tr>
									
								<?
										}
									 }
								?>
								</tbody>
							</table>
								<input type="hidden" name="cmd" id="cmd" value="groupDelete"/>
								<input type="hidden" name="stype" id="stype" value="<?=$_REQUEST[stype]?>"/>
								<input type="hidden" name="sval" id="sval" value="<?=$_REQUEST[sval]?>"/>
							</form>
							<div class="btn">
								<div class="btnLeft">
									<a class="btns" href="#" onclick="groupDelete();"><strong><?=getMsg('btn.delete')?></strong> </a>
								</div>
								<div class="btnRight">
									<a class="wbtn" href="write.php"><strong><?=getMsg('btn.write')?></strong> </a>
								</div>
							</div>
							<!--//btn-->
							<!-- 페이징 처리 -->
							<?=pageList($popup->reqPageNo, $rowPageCount[1], $popup->getQueryString('index.php', 0, $_REQUEST))?>
							<!-- //페이징 처리 -->
							<form name="searchForm" id="searchForm" action="index.php" method="post">
								<div class="search">
									<select name="stype" title="검색을 선택해주세요">
										<option value="all" <?=getSelected($_REQUEST[stype], "all") ?>><?=getMsg('th.all')?></option>
										<option value="title" <?=getSelected($_REQUEST[stype], "title") ?>>제목</option>
										<option value="contents" <?=getSelected($_REQUEST[stype], "contents") ?>><?=getMsg('lable.option.contents')?></option>
									</select>
									<input type="text" name="sval" value="<?=$_REQUEST[sval]?>" title="검색할 내용을 입력해주세요" />
									<input type="submit" class="se_btn " value="<?=getMsg('btn.search')?>" />
								</div>
							</form>
							<!-- //search --> 
						</div>
						<!-- //blist -->
					</div>
					<!-- //bbs --> 
					<!-- 내용 : e -->
				</div>
				<!--//con -->
			</div>
			<!--//content -->
		</div>
		<!--//container --> 
		<!-- E N D :: containerArea-->
	</div>
	<!--//canvas -->
</div>
<!--//wrap -->
<?
for($i = 0; $i < count($result); $i++) {
	$row = $result[$i];

	$targetUrl = "style='cursor:pointer;' onclick=\"location.href='".$popup->getQueryString('edit.php', $row[no], $_REQUEST)."'\"";
											
	$tempContents = $row[contents];
	$detailBtn = "";
	if ($row[relation_url]) {
		$detailBtn = "<img src=\"/img/btn_detail.gif\" align=\"absmiddle\" style=\"border:0; margin:2 0 0 0;  text-align:right; cursor:pointer;\" onclick=\"window.open('".cvtHttp($row[relation_url])."','_blank','height=600,width=800,top=50,left=50,toolbar=1, directories=1, status=1, menubar=1, scrollbars=1, resizable=1,location=1')\">&nbsp;";
	}
	if ($row[type] == '1') {
		$tempContents = "<img src='".$uploadPath.$row[imagename]."' style='border:0; cursor:pointer;' ";
		if ($row[relation_url]) {
			$tempContents .= "onclick=\"window.open('".cvtHttp($row[relation_url])."','_blank','height=600,width=800,top=50,left=50,toolbar=1, directories=1, status=1, menubar=1, scrollbars=1, resizable=1,location=1')\"";
		}
		$tempContents .= ">";
		$detailBtn = "";
	}								
	
	if ($row[type] == '0' || $row[type] == '1') {
		$border = "";
		if ($row[type] != '1') {
			$border = "border:".$row[border_color]." 5px solid;";
		}
?>
	<div id="showimage<?=$i?>" style="position:absolute;left:<?=$row[area_left]?>px;top:<?=$row[area_top]?>px;z-index:999999; display:none;">
		<div id="divPop<?=$row[no]?>">
			<div id="dragbar<?=$i?>" onmousedown="initDrags<?=$i?>();" style="<?=$border?> background:#fff; overflow:hidden;">
				<div id="popMain<?=$row[no]?>">
					<p><?=$tempContents?></p>
					<p><?=$detailBtn?></p>
				</div>
			</div>
			<div style="background:<?=$row[type] == '1' ? $row[bg_color] : $row[border_color] ?>; color:<?=$row[bg_color] == '#fff' ? "#000" : "#fff"?>; vertical-align:middle; text-align:right; padding:3px 10px;">
				<input type="checkbox" id="chkbox<?=$row[no]?>" onclick="closeLayer('divPop<?=$row[no]?>', this, '1', 'showimage<?=$i?>');"/>오늘 하루 이 창을 열지 않음
				<a onclick="closeLayer('divPop<?=$row[no]?>', 'chkbox<?=$row[no]?>', '1', 'showimage<?=$i?>');" style="color:<?=$row[bg_color] == '#fff' ? "#000" : "#fff"?>; cursor:pointer;">[닫기]</a>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		var	crossobj<?=$i?>;
		var	dragapproved<?=$i?>;
		
		function drag_drop<?=$i?>(e){
			if (ie4&&dragapproved<?=$i?>){
				crossobj<?=$i?>.style.left = tempx+event.clientX-offsetx+"px";
				crossobj<?=$i?>.style.top = tempy+event.clientY-offsety+"px";
				return false;
			} else if (ns6&&dragapproved<?=$i?>){
				crossobj<?=$i?>.style.left = tempx+e.clientX-offsetx+"px";
				crossobj<?=$i?>.style.top = tempy+e.clientY-offsety+"px";
				return false;
			}
		}
		
		 function initializedrag<?=$i?>(e){
			crossobj<?=$i?> = $("#showimage<?=$i?>")[0];
		
			var firedobj=ns6? e.target : event.srcElement;
			var topelement=ns6? "HTML" : "BODY";
			while (firedobj.tagName!=topelement&&firedobj.id!="dragbar<?=$i?>"){
			  firedobj=ns6? firedobj.parentNode : firedobj.parentElement;
			}
		
			if (firedobj.id=="dragbar<?=$i?>"){
			  offsetx=ie4? event.clientX : e.clientX;
			  offsety=ie4? event.clientY : e.clientY;
		
			  tempx=parseInt(crossobj<?=$i?>.style.left);
			  tempy=parseInt(crossobj<?=$i?>.style.top);
		
			  dragapproved<?=$i?>=true;
			  document.onmousemove=drag_drop<?=$i?>;
			}
		  }    
		
		  function initDrags<?=$i?>() {
			  zCount++;
			  document.onmousedown=initializedrag<?=$i?>;
			  document.onmouseup=new Function("dragapproved<?=$i?>=false");
			  document.getElementById("showimage<?=$i?>").style.zIndex = zCount;
		  }
		</script>							
<?
	}
}
?>
</body>
</html>