<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Comment.class.php";

$comment = new Comment(9999, $tablename, $_REQUEST);
$result = $comment->getList($_REQUEST);

?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/boardConfig/config.php" ?>
<script>
	function goComment() {
		
		if($("#name").val()=="" |$("#name").val()=="이름") {
			alert("<?=getMsg('alert.text.name2')?>");
			$("#name").focus();
			return false;
		}
		
		if($("#password").length > 0) {
			if($("#password").val()=="") {
				alert("<?=getMsg('alert.text.password')?>");
				$("#password").focus();
				return false;
			}
		}else{
			if($("#password_temp").val()=="<?=getMsg('th.password')?>") {
				alert("<?=getMsg('alert.text.password')?>");
				$("#password_temp").focus();
				return false;
			}
		}
		
		if($("#contents").val()=="") {
			alert("<?=getMsg('alert.text.contents')?>");
			$("#contents").focus();
			return false;
		}
		return true;
	}
	
	function goDeleteRe(obj) {
		var str = "<?=getMsg('confirm.text.commentdelete')?>";
		if(confirm(str)) {
			$("#no").val($(obj).attr("id"));
			$("#delete_frm").submit();
//			return true;
		}else{
			return false;
		}
	}
	
	
$(document).ready(function(){
	
	$(".r_delete").click(function(){
		goDeleteRe($(this));	
	});
	
	$(".focus_zone").focus(function(){
		focusTextRemove($(this));
	});
	
	$(".focus_zone").blur(function(){
		blurTextInsert($(this));
	});
		
});
</script>

<div class="reple">
	
	<form name="delete_frm" id="delete_frm" action="/manage/board/comment/process.php" method="post">
		<? if (mysql_num_rows($result) == 0) { ?>
		<dl>
			<dd class="bbsno">
				<?=getMsg('message.tbody.reple')?>
			</dd>
		</dl>
		<? } else { ?>
		<? while ($co_row = mysql_fetch_assoc($result)) { ?>
		
		<dl>
			<dt><strong><?=$co_row['name']?></strong><i></i><?=getDateTimeFormat($co_row['registdate'])?></dt>
			<dd><?=$co_row['contents']?>
				<span class="reEdit">
					<input type="button" class="blue_btn r_delete" id="<?=$co_row['no']?>" value="<?=getMsg('th.delete')?>"/>
				</span>
			</dd>
		</dl>
		<? 
			} 
		}
		?>
		<input type="hidden" name="comment_cmd" id="comment_cmd" value="r_delete"/>
		<input type="hidden" name="parent_fk" id="parent_fk" value="<?=$comment->parent_fk?>"/>
		<input type="hidden" name="no" id="no" value=""/>
		<input type="hidden" name="tablename" id="tablename" value="<?=$tablename?>"/>
		<input type="hidden" name="url"	id="url" value="<?=$_SERVER["REQUEST_URI"]?>"/>
	</form>
	<div class="rego">
		<form name="comment_frm" id="comment_frm" action="/manage/board/comment/process.php" method="post" onsubmit="return goComment();">
			<dl>
				<dt>
					<input type="text" class="focus_zone" name="name" id="name" value="<?=getMsg('th.name3')?>" title="작성자 이름을 입력해주세요" />
					<input type="text" class="focus_zone" name="password_temp" id="password_temp" value="<?=getMsg('th.password')?>" title="비밀번호를 입력해주세요" />
				</dt>
				<dd>
					<textarea class="focus_zone" name="contents" id="contents" title="내용을 입력해주세요"></textarea>
					<div class="btn">
						<div class="btnLeft">
							<a class="blue_btn" style="cursor:pointer;" onclick="$('#comment_frm').submit();"><?=getMsg('btn.comment')?></a>
						</div>					
					</div>					
					<!--//btnAll--> 
				</dd>
			</dl>
			<input type="hidden" name="comment_cmd" id="comment_cmd" value="r_write"/>
			<input type="hidden" name="parent_fk" id="parent_fk" value="<?=$comment->parent_fk?>"/>
			<input type="hidden" name="tablename" id="tablename" value="<?=$tablename?>"/>
			<input type="hidden" name="url"	id="url" value="<?=$_SERVER["REQUEST_URI"]?>"/>
		</form>
	</div>
	<!-- //rego -->
</div>