<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";


include_once $_SERVER['DOCUMENT_ROOT']."/lib/global/Message_global.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Comment.class.php";

include $_SERVER['DOCUMENT_ROOT']."/manage/include/logingCheck.php";


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
<?
if (checkReferer($_SERVER["HTTP_REFERER"])) {
	$comment = new Comment(9999, $_REQUEST);
	$comment_cmd = $_REQUEST['comment_cmd'];

	if ($comment_cmd == 'r_write') {

		$r = $comment->insert($_REQUEST);
		if ($r > 0) {
			echo returnURLMsg($comment->getQueryString($_REQUEST['url'], $_REQUEST['parent_fk'], $_REQUEST), getMsg('result.text.insert'));
		} else {
			echo returnURLMsg($comment->getQueryString($_REQUEST['url'], $_REQUEST['parent_fk'], $_REQUEST), getMsg('result.text.error'));
		}
	} else if ($comment_cmd == 'r_delete') {
		$no = $_REQUEST['no'];
		$r = $comment->delete($no);
		if ($r > 0) {
			echo returnURLMsg($comment->getQueryString($_REQUEST['url'], $_REQUEST['parent_fk'], $_REQUEST), getMsg('result.text.delete'));
		} else {
			echo returnURLMsg($comment->getQueryString($_REQUEST['url'], $_REQUEST['parent_fk'], $_REQUEST), getMsg('result.text.error'));
		}
	} 

} else {
	echo returnURLMsg($intranet->getQueryString($_REQUEST['url'], $_REQUEST['parent_fk'], $_REQUEST), getMsg('result.text.error'));
}


?>
</body>
</html>