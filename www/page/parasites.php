<?session_start();?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/logingCheck.php" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/themeHtml.php" ?>
</head>
<body>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
	<section class="inner-header">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-12  col-lg-4 m_ver">
					<figure class="d-flex"><img src="/images/parasites/parasites_con01_img01.png" alt="Meet the bad guys" class=" img-fluid wow fadeInUp"></figure>
				</div>
				<div class="col-12  col-lg-8">
					<div class="header-text-inner">
						<h1 class="anim color_yellow type02"><span class="titleanimation-bottom"><span class="mobile_small">산책길의 불청객,</span><br/> 참진드기</span></h1>
						<p class="wow fadeInUp">참진드기는 반려동물의 피부에서 흡혈을 하며, 털 사이사이 위치하여 눈에 잘 띠지 않습니다. 참진드기는 흡혈 활동으로 다양한 전염병을 옮길 수 있으며, 한 번에 2000개 이상의 알을 낳아 번식할 수 있습니다.</p>
					</div>
				</div>
				<div class="col-12  col-lg-4 pc_ver">
					<figure class="d-flex"><img src="/images/parasites/parasites_con01_img01.png" alt="Meet the bad guys" class=" img-fluid wow fadeInUp"></figure>
				</div>
			</div>
		</div>
	</section>
	
	<section class="col-3-section">
		<div class="container-1">
			<h2 class="text-center" >참진드기는 어떤 질병을 옮길까요?</h2>
			<p class="text-center" style="padding-bottom:60px">참진드기는 여러 위험한 질병의 매개체가 될 수 있으며, 흡혈로 인해 이 질병들이 전염됩니다.<br/> 특히 동물과 사람 간의 전파가 가능한 인수공통 질병을 주의해야 합니다.</p>
			<div class="row row-2">
				<div class="col-md-12 col-lg-4">
					<div class="glid-list">
						<figure><img src="/images/icons/parasites_icon01.png" alt="effective and long-lasting" width="200" class="wow fadeInUp"></figure>
						<h5 class="anim"><span class="titleanimation-bottom">중증열성혈소판감소증후군(SFTS)</span></h5>
						<p class="wow fadeInUp">‘살인진드기’라고도 불리는 작은소참진드기(Heamaphysalis longicornis)의 흡혈 의해 감염되는 신종 바이러스성 전염병입니다. 사람에서 약 20%의 치사율을 보이는 이 무서운 질환은 강아지 및 고양이에서도 발생하는 것으로 보고되었습니다.</p>
					</div>
				</div>
				<div class="col-md-12 col-lg-4">
					<div class="glid-list">
						<figure><img src="/images/icons/parasites_icon02.png" alt="peace of mind" width="200" class="wow fadeInUp"></figure>
						<h5 class="anim"><span class="titleanimation-bottom">바베시아증(Babesiosis)</span></h5>
						<p class="wow fadeInUp">바베시아증은 진드기내의 기생충이 흡혈 시 감염되어 발생합니다. 주로 41°C 이상의 고열 증상으로 시작하여 
						식욕감소, 호흡수 증가, 근육 떨림, 빈혈, 황달, 체중감소 등의 임상증상을 보입니다. 심한 경우 사망에까지 이르게 하는
						이 질병은 사람에게도 전염될 수 있어 무엇보다도 예방이 중요합니다.</p>
					</div>
				</div>
				<div class="col-md-12 col-lg-4">
					<div class="glid-list">
						<figure><img src="/images/icons/parasites_icon03.png" alt="simple and convenient" width="200" class="wow fadeInUp"></figure>
						<h5 class="anim"><span class="titleanimation-bottom">라임병(Lyme disease)</span></h5>
						<p class="wow fadeInUp">라임병은 진드기의 전 생애에 걸쳐 감염 진드기의 흡혈에 의해 전염될 수 있습니다. 라임병에 걸린 동물은 고열, 식욕부진, 관절 종창으로 인한 파행, 림프절 종창 및 무기력함 등의 증상을 보이고, 치료가 늦어지면 신장, 심장 및 신경계에 손상을 입혀 만성 질환을 유발시킵니다.</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-md-12 col-lg-8 wow fadeInUp">
					<div class="benefits mb-more type02">
						<div class="benefits-list mb-0  wow fadeInUp">
							<h3 class="bigheading anim"><span class="titleanimation-bottom">작은소참진드기의 생활환</span></h3>
							<p class="wow fadeInUp">클릭하여 작은소참진드기의 각 단계별 특징을 알아보세요</p>
							<div class="steps-car owl-carousel owl-theme wow fadeInUp">
								<div class="item" data-dot="1. 충란">
									<h4>충란</h4>
									<p>성충은 2-3주간 에 2000개 이상의 충란을 산란하며, 60-90일 후 부화합니다.</p>
								</div>
								<div class="item" data-dot="2. 유충">
									<h4>유충</h4>
									<p>
										유충은 숙주 피부에 올라간지 1시간내 밀착하여 5일가량 흡혈합니다.
									</p>
								</div>
								<div class="item" data-dot="3. 약충">
									<h4>약충</h4>
									<p>
										약충은 변태를 거쳐 간간해진 외피를 가지며, 7일간 흡혈을 하며 성충으로의 변태를 준비합니다.
									</p>
								</div>
								<div class="item" data-dot="4. 성충">
									<h4>성충</h4>
									<p>
										성충은 약 1주일간 흡혈 후 산란을 준비하며, 한 번에 1000개 이상의 알을 낳습니다.
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			   
				<div class="col-md-12 col-lg-4 mob-pedding wow fadeIn">
					<div class="video-block img_width">
						<div class="step-images-slider video-slider owl-carousel owl-theme ">
							<div class="video-item"><img src="/images/parasites/ticks/steps/step@2x.png" class="w-100" alt="Paralysis Tick"> </div>
							<div class="video-item"><img src="/images/parasites/ticks/steps/step2@2x.png" class="w-100" alt="Paralysis Tick"> </div>
							<div class="video-item"><img src="/images/parasites/ticks/steps/step3and4@2x.png" class="w-100" alt="Paralysis Tick"> </div>
							<div class="video-item"><img src="/images/parasites/ticks/steps/step3and4@2x2.png" class="w-100" alt="Paralysis Tick"> </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="alt-color chew_con">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-md-12 col-lg-8">
					<div class="benefits benefits-noimg">
						<div class="benefits-list extrta-mb">
							<h4 class="anim"><span class="titleanimation-bottom">참진드기 관리 요령</span></h4>
							<ul>
								<li>
									<h6 class="anim"><span class="titleanimation-bottom">1단계</span></h6>
									<p class="wow fadeInUp">흡혈을 시작하기 이전의 진드기는 매우 작기 때문에 신체검사 시 놓칠 확률이 높습니다. 흡혈을 시작한 이후의 진드기는 좀 더 쉽게 눈에 띕니다.</p>
								</li>
								<li>
									<h6 class="anim"><span class="titleanimation-bottom">2단계</span></h6>
									<p class="wow fadeInUp">손가락으로 반려동물의 털 속을 미끄러지듯 쓸어봅니다. 평소와 다르게 이물 혹은 혹같이 느껴진다면 해당 부위를 꼼꼼히 살피세요. 귓속, 눈 주위, 발가락 사이 등 놓치기 쉬운 부분도 체크하세요.</p>
								</li>
								<li>
									<h6 class="anim"><span class="titleanimation-bottom">3단계</span></h6>
									<p class="wow fadeInUp">산책 후 4-6시간 이내에 꼼꼼히 빗질해 주시면 도움이 될 수 있습니다.</p>
								</li>
								<li>
									<h6 class="anim"><span class="titleanimation-bottom">4단계</span></h6>
									<p class="wow fadeInUp">진드기 제거시에는 족집게를 사용하여 반려동물의 피부에 최대한 가깝게 잡고 천천히 피부로 부터 떼어내세요. 확 떼려 하면 진드기의 몸통과 머리가 분리될 수 있습니다.</p>
								</li>
								<li>
									<h6 class="anim"><span class="titleanimation-bottom">5단계</span></h6>
									<p class="wow fadeInUp">진드기 제거시에는 족집게를 사용하여 반려동물의 피부에 최대한 가깝게 잡고 천천히 피부로 부터 떼어내세요. 너무 급하게 확 떼려 하면 진드기의 몸통과 머리가 분리되어 진드기의 주둥이가 피부 속에 박혀있을 수 있습니다.</p>
								</li>
							</ul>
						</div>
					</div>
				</div>

				<div class="col-md-12 col-lg-4 mob-pedding wow fadeIn">
					<div class="video-block top-minouse img_width">
						<a href="https://youtu.be/UjsRpaDJH50" data-toggle="lightbox" data-gallery="youtubevideos">
							<img src="/images/videos/flea_thumb.jpg" class="img-fluid" alt="Ticks Video">
						</a>
						<a href="#" class="playbutton" title="Watch Video"></a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--
	<section class="col-3-section">
		<div class="container-1">

			<div class="alternet-list alternet-list-last">
				<div class="row no-gutters">
					<div class="col-md-12 col-lg-6"><img src="/images/parasites/parasites_img.jpg" class="w-100" alt="Dirt on Ticks"></div>
					<div class="col-md-12 col-lg-6 align-self-end">
						<div class="text-block alternet-text-block alternet-text-block-bottom">
							<h2 class="anim full-visible"><span class="titleanimation-bottom">진드기의 위험성</span></h2>
							<p class="wow fadeInUp">
								사람에서 치명적일 수 있는 작은소참진드기와 같은 참진드기들은 소중한 반려동물에서는 
								더욱 치명적일 수 있습니다. 이러한 진드기는 반려동물의 털 속 깊숙이 숨어 보이지 않게 
								피를 빨고 번식하며 각종 질병을 퍼트립니다.
								또한 이보다 더 작아 눈으로는 보이지 않는 귀진드기, 옴진드기, 모낭충 등은 반려동물의 피부에 
								각종 질병을 유발하며, 치료가 쉽지 않습니다.


							</p>
						</div>
					</div>
				</div>
			</div>

		</div>
	</section>
	
	<section>
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-md-12 col-lg-8 wow fadeInUp">
					<div class="benefits benefits-noimg">
						<div class="benefits-list mb-0  wow fadeInUp">
							<h3 class="bigheading">참진드기가 옮기는 질병들</h3>
							<div class=" steps-car owl-carousel owl-theme">
								<div class="item" data-dot="옴진드기">
									<p>옴진드기(Sarcoptes scabiei)</p>
									<p>옴진드기는 전 세계에서 발견되는 감염성이 높은 진드기 입니다. 맨눈에는 보이지 않을 정도로 작은 이 진드기는
									사람 및 감염된 다른 동물과의 직,간접적인 접촉을 통해 전염되며, 진드기는 피부층을 파고들어가
									알을 낳고 번식을 합니다. 감염된 개는 극심한 가려움증으로 삶의 질이 현저히 저하됩니다.</p>
									<figure><img src="/images/steps/step-1.jpg" class="img-fluid" alt="How to use Bravecto Spot-on for Cats"></figure>
								</div>
								<div class="item" data-dot="모낭충">
									<p>모낭충(Demodex)</p>
									<p>건강한 개의 피부에서도 정상적으로 발견되는 모낭충은, 어떠한 원인에 의해 비정상적으로 숫자가 늘어나면 문제가 됩니다. 
									매우 작은 이 진드기는 개의 모낭 및 땀샘에 서식하며, 정확한 원인은 아직 밝혀지지 않았으나, 다른 동물로의
									전염은 일어나지 않습니다. 매우 극심한 가려움증으로 삶의 질이 현저히 저하됩니다.</p>
									<figure><img src="/images/steps/step-2.jpg" class="img-fluid" alt="How to use Bravecto Spot-on for Cats"></figure>
								</div>
								<div class="item" data-dot="귀진드기">
									<p>귀진드기(Otodectes cynotis)</p>
									<p>귀진드기는 개와 고양이의 귀에 문제를 일으키는 진드기 입니다. 매우 작아 맨눈으로는 보이지 않는 이 진드기는
									주로 외이도에서 가려움증을 유발하지만 심한 경우 고막을 뚫고 중이까지 감염되기도 합니다. 감염된 동물은
									머리를 너무 심하게 흔들거나, 과도하게 긁어 상처가 날 수 있습니다.</p>
									<figure><img src="/images/steps/step-3.jpg" class="img-fluid" alt="How to use Bravecto Spot-on for Cats"></figure>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12 col-lg-4 mob-pedding wow fadeIn">
					<div class="video-block top-minouse">
						<a href="/video/spot-on-cats-video.mp4" data-toggle="lightbox" data-gallery="youtubevideos">
							<img src="/images/videos/spot-on-cats-video.jpg" class="img-fluid" alt="Spot On Cats Video">
						</a>
						<a href="#" class="playbutton" title="Watch Video"></a>
					</div>
				</div>
			</div>
		</div>
	</section>
	-->
	<? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
</body>
</html>
		
