<?session_start();?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/logingCheck.php" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/themeHtml.php" ?>
</head>
<body>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
	<div>
		<div id="sync1" class="owl-carousel owl-theme big-product-carousel">
			<div class="item">
				<div class="desktop-only"><img src="/images/products/chew-for-dogs/header-1.jpg" class="w-100" alt="Bravecto Chew for Dogs"></div>
				<div class="mobile-only"><img src="/images/products/chew-for-dogs/header-1-mobile.jpg" class="w-100" alt="Bravecto Chew for Dogs"></div>
			</div>
			<div class="item">
				<div class="desktop-only"><img src="/images/products/chew-for-dogs/header-2.jpg" class="w-100" alt="Bravecto Chew for Dogs"></div>
				<div class="mobile-only"><img src="/images/products/chew-for-dogs/header-2-mobile.jpg" class="w-100" alt="Bravecto Chew for Dogs"></div>
			</div>
			<div class="item">
				<div class="desktop-only"><img src="/images/products/chew-for-dogs/header-3.jpg" class="w-100" alt="Bravecto Chew for Dogs"></div>
				<div class="mobile-only"><img src="/images/products/chew-for-dogs/header-3-mobile.jpg" class="w-100" alt="Bravecto Chew for Dogs"></div>
			</div>
			<div class="item">
				<div class="desktop-only"><img src="/images/products/chew-for-dogs/header-4.jpg" class="w-100" alt="Bravecto Chew for Dogs"></div>
				<div class="mobile-only"><img src="/images/products/chew-for-dogs/header-4-mobile.jpg" class="w-100" alt="Bravecto Chew for Dogs"></div>
			</div>
		</div>
	</div>

	<div class="alt-color">
		<div class="container-1">
			<div id="sync2" class="owl-carousel owl-theme dogs">
				<div class="item yellow-active"><span><i></i>2 - 4.5 kg</span></div>
				<div class="item orange-active"><span><i></i>4.5 - 10 kg</span></div>
				<div class="item green-active"><span><i></i>10 - 20 kg</span></div>
				<div class="item blue-active"><span><i></i>20 - 40 kg</span></div>
			</div>
		</div>
	</div>

	<section class="product-section">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-md-12 col-lg-10">
					<div class="product-details-top">
						<h1 class="wow fadeInUp">강아지용 브라벡토 츄어블</h1>
						<h4 class="wow fadeInUp">맛있는 츄어블로 당신의 반려견을 보호하세요</h4>
						<p class="wow fadeInUp">우리 반려견들의 애교를 부르는 간식. 그래서 저희는 벼룩 및 진드기로부터 반려견을 보호할 수 있는
						간식을 만들었습니다.</p>
						<ul class="wow fadeInUp">
							<li>진드기(tick and mite) 및 벼룩으로 부터 12주간 보호합니다</li>
							<li>귀진드기, 옴진드기, 모낭충에도 효과가 있습니다</li>
							<li>8주령 이상 및 2kg 이상의 강아지 뿐만 아니라 임신 및 수유중인 강아지에도 안심하고 사용할 수 있습니다</li>
						</ul>
					</div>
				</div>
				<div class="col-md-12 col-lg-8 wow fadeInUp">
					<div class="product-details-top pt-0 pb-180">
						<ul class="nav nav-tabs" id="proTab" role="tablist">
							<li class="nav-item"> <a class="nav-link active" id="longlastng-tab" data-toggle="tab" href="#loglasting" role="tab" aria-selected="true" title="Long Lating">편리한</a> </li>
							<li class="nav-item"> <a class="nav-link" id="effective-tab" data-toggle="tab" href="#effective" role="tab" aria-selected="false" title="Efective">작용</a> </li>
							<li class="nav-item"> <a class="nav-link" id="hasslefree-tab" data-toggle="tab" href="#hasslefree" role="tab" aria-selected="false" title="Easy">맛있는</a> </li>
							<li class="nav-item"> <a class="nav-link" id="global-tab" data-toggle="tab" href="#global" role="tab" aria-selected="false" title="Global">세계적인</a> </li>
						</ul>
						<div class="tab-content" id="proTabContent">
							<div class="tab-pane fade show active" id="loglasting" role="tabpanel">
								<h4>편리한</h4>
								<p>브라벡토는 1회 사용만으로 12주간 외부기생충으로 부터 반려견을 보호합니다. 
								매 계절 1회 사용으로 기억하기 쉽고, 외부기생충 예방을 놓칠 일이 적어집니다.</p>
							</div>
							<div class="tab-pane fade" id="effective" role="tabpanel" aria-labelledby="effective-tab">
								<h4>빠르고 오래가는</h4>
								<p>브라벡토의 주성분 플루랄라너는 사용 4시간 이내에 작용하기 시작하여, 12시간 이내에
								진드기의 100%를 사멸합니다. 또한 플루랄라너의 대부분이 비활성 상태로 저장되어 있다가 12주간 서서히 방출됩니다.</p>
							</div>
							<div class="tab-pane fade" id="hasslefree" role="tabpanel" aria-labelledby="hasslefree-tab">
								<h4>안전한</h4>
								<p>브라벡토는 8주령이 갓 지났거나 2kg가 조금 넘는 작은 체구의 강아지, 임신중 혹은 수유중인 반려견에서
								안심하고 사용할 수 있습니다. 또한 많은 종류의 의약품 사용이 불가능한 콜리종에서의 안전성도 검증되었습니다.</p>
							</div>
							<div class="tab-pane fade" id="global" role="tabpanel" aria-labelledby="global-tab">
								<h4>세계적인</h4>
								<p>브라벡토는 전 세계 85여개 국가에서 판매되며, 2019년 1억 도스 판매를 돌파하였습니다.
								엄청난 판매량으로 그 안전성이 검증된 브라벡토의 놀라운 효능을 경험해 보세요.</p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12 col-lg-4 mob-pedding wow fadeIn">
					<div class="video-block wow fadeInUp"> <img src="/images/products/chew-for-dogs/section-1.jpg" alt="little girl feeding a cat" class="w-100"></div>
				</div>
			</div>
		</div>
	</section>

	<section class="alt-color">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-md-12 col-lg-8">
					<div class="benefits benefits-noimg">
						<div class="benefits-list wow fadeInUp"> <a href="#" class="redbg-link">플루랄라너</a> </div>
						<div class="benefits-list extrta-mb">
							<h3 class="wow fadeInUp">브라벡토만의 혁신적인 성분, 플루랄라너</h3>
							<p class="wow fadeInUp">2014년 미국 FDA를 시작으로, 전 세계 85여개 국가에서 허가를 받은 브라벡토의 성분 플루랄라너는 
							혈액에 흡수됨과 동시에 대부분이 비활성화 상태로 저장되어 서서히 방출됩니다.
							12주나 되는 기간동안 그 효능의 대부분을 유지하여 반려견에게 문제가 되는 외부기생충을 
							더욱 간편하고 안전하게 예방할 수 있게 되었죠. </p>
						</div>
						<div class="benefits-list extrta-mb ">
							<h3 class="wow fadeInUp">방대한 임상 연구와 모니터링</h3>
							<p class="wow fadeInUp">브라벡토(플루랄라너)는 저희 MSD동물약품의 비전 ‘The Science of Healthier Animals’ 의
							놀라운 결과물입니다. 방대한 임상 연구와 모니터링을 바탕으로 반려동물들이 브라벡토(플루랄라너)의 
							강력한 효능과 지속성 및 높은 안전성을 경험할 수 있게 되었습니다.</p>
						</div>
					</div>
				</div>
				<div class="col-md-12 col-lg-4 mob-pedding wow fadeIn">
					<div class="video-block top-minouse">
						<a href="https://youtu.be/p5d_E-VsdUw?autoplay=1&rel=0" data-toggle="lightbox" data-gallery="youtubevideos">
							<img src="/images/videos/chew-for-dogs-video.jpg" class="img-fluid" alt="Chew For Dogs Video">
						</a>
						<a href="#" class="playbutton" title="Watch Video"></a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-md-12 col-lg-8 wow fadeInUp">
					<div class="benefits benefits-noimg">
						<div class="benefits-list mb-0  wow fadeInUp">
							<h3 class="bigheading">브라벡토 츄어블 투여 방법</h3>

							<p>반려견들에게 약을 먹이는 것이 쉽지 만은 않습니다. 
							하지만 브라벡토 츄어블은 손쉽게 급여가 가능하죠.브라벡토 포장을 뜯어 간식처럼 급여를 하시거나, 밥을 먹을 때 사료에 섞어 주세요.
							강아지용 브라벡토 츄어블은 반려견들이 좋아하는 돼지간 맛일 뿐만 아니라 가수분해된 원료를 사용하여 피부알러지 걱정도 없습니다.
							강아지가 먹은 후에는 칭찬을 잊지 마세요!</p>
							<figure><img src="/images/steps/step-chew.png" class="img-fluid" alt="How to use Bravecto Chew for Dogs"></figure>
						</div>
					</div>
				</div>
				<div class="col-md-12 col-lg-4 mob-pedding wow fadeIn">
					<div class="video-block top-minouse">
						<a href="https://youtu.be/4OgB9MYgNs4?autoplay=1&rel=0" data-toggle="lightbox" data-gallery="youtubevideos">
							<img src="/images/videos/spot-on-dogs-video.jpg" class="img-fluid" alt="Chew For Dogs Video 2">
							<a href="#" class="playbutton" title="Watch Video"></a>
						</a>
					</div>
				</div>
			</div>
	</section>

	<section class="alt-color">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-md-12 col-lg-8 moile-last">
					<div class="benefits benefits-noimg">
						<div class="benefits-list wow fadeInUp">
							<h2>더 궁금하신 점이 있으신가요?</h2>
							<div class="accordion" id="faqs">
								<div class="faq-list wow fadeInUp">
									<a data-toggle="collapse" data-target="#f1" aria-expanded="true" href="#"> Where can I buy Bravecto? </a>
									<div id="f1" class="collapse show " data-parent="#faqs">
										<div class="answer">Bravecto is available from veterinarians and leading pet shops.</div>
									</div>
								</div>
								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f6" aria-expanded="true" href="#">What will Bravecto protect my pet against?</a>
									<div id="f6" class="collapse" data-parent="#faqs">
										<div class="answer">
											Bravecto is available in a range of products that provide long-lasting protection against common
											parasites in dogs and cats with just one dose*


											<table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered">
												<tr>
													<th scope="row">Product* </th>
													<td><strong>Bravecto Chew  for Dogs</strong></td>
												</tr>
												<tr>
													<th scope="row">Fleas</th>
													<td>
														<span class="correct">&nbsp;</span>
														3 months
													</td>
												</tr>
												<tr>
													<th scope="row">Paralysis Ticks</th>
													<td>
														<span class="correct">&nbsp;</span>
														4 months
													</td>
												</tr>
												<tr>
													<th scope="row">Brown Dog Ticks</th>
													<td>
														<span class="correct">&nbsp;</span>
														8 weeks
													</td>
												</tr>
												<tr>
													<th scope="row">Demodex Mites</th>
													<td><span class="correct">&nbsp;</span></td>
												</tr>
												<tr>
													<th scope="row">Sarcoptes Mites</th>
													<td><span class="correct">&nbsp;</span></td>
												</tr>
												<tr>
													<th scope="row">Ear mites</th>
													<td><span class="correct">&nbsp;</span></td>
												</tr>
											</table>
											<small>*Refer to product label for full claim details.</small>
										</div>
									</div>
								</div>
								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f2" aria-expanded="true" href="#">What should Bravecto chewable tablet look like when they come out of the package?</a>
									<div id="f2" class="collapse" data-parent="#faqs">
										<div class="answer">Bravecto is a light to dark brown tablet with a smooth or slightly rough surface and circular shape. Some marbling, speckles or both may be visible.</div>
									</div>
								</div>
								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f3" aria-expanded="true" aria-controls="f3" href="#">When is it safe to pet my dog after administering Bravecto?</a>
									<div id="f3" class="collapse" data-parent="#faqs">
										<div class="answer">There is no reason to stop touching your dog when treating with Bravecto.</div>
									</div>
								</div>
								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f4" aria-expanded="true" aria-controls="f4" href="#"> Can I use Bravecto all year-round? </a>
									<div id="f4" class="collapse" data-parent="#faqs">
										<div class="answer">
											Yes, it is safe to use Bravecto all year-round. Fleas can be a year-round problem indoors and
											outdoors. Administering Bravecto Chew for Dogs every 3 months ensures your dog will be protected from
											fleas throughout the year.
										</div>
									</div>
								</div>
								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f5" aria-expanded="true" aria-controls="f5" href="#"> My dog is taking some medications for another condition as well right now. Is it safe to give Bravecto?</a>
									<div id="f5" class="collapse" data-parent="#faqs">
										<div class="answer">
											It is always best to discuss all of your dog's treatments with your veterinarian as they are your pet's
											health care expert.
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f7" aria-expanded="true" aria-controls="f7" href="#">What weight ranges does Bravecto Chew for Dogs treat?</a>
									<div id="f7" class="collapse" data-parent="#faqs">
										<div class="answer" style="letter-spacing: -0.5px">
											Bravecto is available in single dose packs for dogs ranging from 2–56 kg. One dose of Bravecto Chew for Dogs provides 3 months protection against fleas and 4 months protection against paralysis ticks.
											<img class="my-2" src="/images/products/chew-for-dogs/packshot.png" style="max-width: 500px; width:100%; position: relative;">
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f8" aria-expanded="true" aria-controls="f8" href="#">How does Bravecto Chew for Dogs kill fleas and ticks?</a>
									<div id="f8" class="collapse" data-parent="#faqs">
										<div class="answer">
											After you give your dog Bravecto, the active ingredient quickly reaches tissue fluids just under your
											dog's skin. When fleas and ticks feed, they take in Bravecto and die. Bravecto Chew for Dogs starts to
											kill fleas after 2 hours and provides effective control within 8 hours for fleas and within 24 hours of
											attachment for ticks. Bravecto Chew for Dogs controls flea infestations for 3 months and paralysis
											ticks for 4 months.
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f9" aria-expanded="true" aria-controls="f9" href="#">Can I give the Bravecto Chew for Dogs to my cat?</a>
									<div id="f9" class="collapse" data-parent="#faqs">
										<div class="answer">
											No. Bravecto chew is not approved for use in cats. However, we recommend Bravecto Spot-on for Cats, which lasts up to 3 months and provides effective protection against fleas and paralysis ticks. It also reduces the incidence of flea allergy dermatitis (FAD).
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f10" aria-expanded="true" aria-controls="f10" href="#">How tasty is Bravecto Chew for Dogs?</a>
									<div id="f10" class="collapse" data-parent="#faqs">
										<div class="answer">In a study^ which included 144 healthy dogs, 91.7% of dogs ate the chew voluntarily.</div>
										<div class="answer"><small>^ - Meadowsm Geurino et al.  A randomized, blinded, controlled USA field study to assess the use of fluralaner tablets in controlling canine flea infestations Cheyney <em>Parasites & Vectors</em> (2014) 7:375</small></div>
									</div>
								</div>


								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f11" aria-expanded="true" aria-controls="f11" href="#">
										Is Bravecto Chew for Dogs waterproof?
										Can my dog swim after I give him Bravecto
										Chew For Dogs? Can I shampoo my dog? Do I need to bathe my dog before
										treatment? Does Bravecto Chew for Dogs work differently on long and short
										haired pets? Can I brush my dog after administering Bravecto Chew for Dogs?
									</a>
									<div id="f11" class="collapse" data-parent="#faqs">
										<div class="answer">
											Bathing, shampooing or swimming should not affect how well Bravecto Chew for Dogs works. Coat
											length and grooming or brushing also have no impact on how well it works.
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f12" aria-expanded="true" aria-controls="f12" href="#">
										What time of day should I give my dog Bravecto Chew For Dogs?
									</a>
									<div id="f12" class="collapse" data-parent="#faqs">
										<div class="answer">
											Bravecto Chew for Dogs can be given to your dog any time of the day; however it is recommended
											that you administer the chew at or around time of feeding.
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f13" aria-expanded="true" aria-controls="f13" href="#">Will Bravecto kill ticks and fleas that are already on my dog?</a>
									<div id="f13" class="collapse" data-parent="#faqs">
										<div class="answer">Yes</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f14" aria-expanded="true" aria-controls="f14" href="#">Does Bravecto kill fleas before they can lay eggs?</a>
									<div id="f14" class="collapse" data-parent="#faqs">
										<div class="answer">Yes. Bravecto kills newly emerged fleas before they lay eggs and breaks the flea life cycle.</div>
									</div>
								</div>


								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f15" aria-expanded="true" aria-controls="f15" href="#">Can dead and attached ticks be found on treated dogs?</a>
									<div id="f15" class="collapse" data-parent="#faqs">
										<div class="answer">
											Yes. Due to the way Bravecto works, dead attached ticks can be still seen on animals following
											treatment. Dead ticks can be easily removed as opposed to live ticks which take some force to
											dislodge from the skin of the animals.
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f16" aria-expanded="true" aria-controls="f16" href="#">Is it necessary to use additional insecticides to control flea stages in the environment?</a>
									<div id="f16" class="collapse" data-parent="#faqs">
										<div class="answer">
											No. Bravecto Chew for Dogs kills fleas and prevents flea infestations for 3 months, a time period that
											covers the entire flea life cycle. Bravecto also kills newly emerged fleas before they can lay eggs.
											Therefore it is not necessary to use additional insecticides to control flea stages in the environment.
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f17" aria-expanded="true" aria-controls="f17" href="#">Can Bravecto be used in breeding/pregnant/lactating dogs?</a>
									<div id="f17" class="collapse" data-parent="#faqs">
										<div class="answer">Yes. Bravecto is approved for use in breeding, pregnant and lactating dogs.</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f18" aria-expanded="true" aria-controls="f18" href="#">When should dogs start treatment with Bravecto?</a>
									<div id="f18" class="collapse" data-parent="#faqs">
										<div class="answer">Bravecto can be administered as early as 8 weeks of age. Dogs should weigh at least 2 kg.</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f19" aria-expanded="true" aria-controls="f19" href="#">Do I really need to treat for fleas all year round?</a>
									<div id="f19" class="collapse" data-parent="#faqs">
										<div class="answer">
											There are very few places in the world where there are no fleas. In warmer climates, fleas are
											present all year round while in more mild climates fleas are present mainly from late spring through to late autumn, and even longer thanks to central heating. To be on the safe side Bravecto Chew for
											Dogs should be administered every 3 months all year round to avoid giving flea populations a chance
											to build up in your home.
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f20" aria-expanded="true" aria-controls="f20" href="#">Is there any difference in efficacy if a dog chews the tablet, or swallows without chewing?</a>
									<div id="f20" class="collapse" data-parent="#faqs">
										<div class="answer">
											No, there is no difference in efficacy between dogs that swallow a Bravecto chew compared to those
											that chew the tablet before swallowing.
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f21" aria-expanded="true" aria-controls="f21" href="#">Why does my dog scratch even more on the first day of Bravecto treatment?</a>
									<div id="f21" class="collapse" data-parent="#faqs">
										<div class="answer">
											When fleas are in the process of dying their movements become uncoordinated. This may cause a
											skin sensation which can result in increased scratching by the dog. However, this phenomenon is
											quickly resolved once the fleas are dead (Bravecto Chew for Dogs provides effective control within 8
											hours for fleas).
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f22" aria-expanded="true" aria-controls="f22" href="#">Why do I see more fleas after I have administered Bravecto?</a>
									<div id="f22" class="collapse" data-parent="#faqs">
										<div class="answer">
											Fleas can continually re-infest treated dogs – either from juvenile flea life stages that have just
											matured to adults in the household or from fleas that jump onto the dog when outside or visiting
											other homes. Bravecto will quickly kill these fleas. Also, as fleas die, they can move from the skin
											surface where they feed, to the tips of the hairs in the coat, making them more visible to the human
											eye.
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f23" aria-expanded="true" aria-controls="f23" href="#">Why can I still see fleas after I have treated my dog with Bravecto?</a>
									<div id="f23" class="collapse" data-parent="#faqs">
										<div class="answer">
											The fleas you see on your pet are fleas which have newly emerged from their cocoons in the home
											or outdoor environment and jumped on your dog. These fleas will die quickly. This is because only a
											small percentage of the flea population is on your dog. Most of the flea population exists as
											immature stages (eggs, larvae and pupae) in your dog’s surroundings. It takes some time to clean
											out a flea population in the house. To help speed this process up, you should clean your house
											thoroughly (for example, by washing your dog’s bedding and vacuuming their favorite
											spots in the house) to help reduce the number of immature fleas in carpets, cracks and crevices. In
											case of a serious infestation, it may take some time to control the infestation. But any fleas you see
											on your dog a few days to weeks after treatment probably represent re-infestation from fleas
											emerging from cocoons in the environment. Bravecto Chew for Dogs’ flea protection lasts for 3
											months - during this period, there is no need to re-treat your dog.
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f24" aria-expanded="true" aria-controls="f24" href="#">Where can I get more information about Bravecto’s claims?</a>
									<div id="f24" class="collapse" data-parent="#faqs">
										<div class="answer">
											All <a class="inlinelink" href="https://portal.apvma.gov.au/pubcris;jsessionid=AjAebiai95var7phRx1L2QBS?p_auth=RBZ2QvsL&p_p_id=pubcrisportlet_WAR_pubcrisportlet&p_p_lifecycle=1&p_p_state=normal&p_p_mode=view&p_p_col_id=column-1&p_p_col_pos=2&p_p_col_count=4&_pubcrisportlet_WAR_pubcrisportlet_javax.portlet.action=search" title="product labels for Bravecto" target="_blank">product labels for Bravecto</a> can be found on the APVMA (Australian Pesticides and Veterinary
											Medicines Authority) Pubcris database.

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12 col-lg-4 mobile-first">
					<div class="where-to-get top-minouse wow fadeInUp">
						<h2>브라벡토 구매를 원하시나요?</h2>
						<p>당신의 반려견에게 가장 알맞은 제품에 대해 수의사와 상담하세요. 
						구입을 원하신다면 거주지역을 아래 검색창에 입력하세요.</p>
						<form action="/page/stockists.php">
							<input name="zipcode" type="text" placeholder="suburb">
							<button class="arror-btn" type="submit"></button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
</body>
</html>
		
