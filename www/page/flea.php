<?session_start();?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/logingCheck.php" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/themeHtml.php" ?>
</head>
<body>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
	<section class="inner-header">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-12  col-lg-4 m_ver">
					<figure class="d-flex"><img src="/images/parasites/flea_con01_img01.png" alt="Meet the bad guys" class=" img-fluid wow fadeInUp"></figure>
				</div>
				<div class="col-12  col-lg-8">
					<div class="header-text-inner">
						<h1 class="anim color_red type02"><span class="titleanimation-bottom"><span class="mobile_small">높이뛰기 선수,</span><br/> 번식왕 벼룩</span></h1>
						<p class="wow fadeInUp">벼룩은 한 번 감염되어 번식을 시작하면 근절하기가 굉장히 힘들어, 전 세계 수많은 반려동물 가족들을 괴롭히는 녀석입니다. 하지만 12주간 지속되어 생활환을 완전히 끊을 수 있는 브라벡토 앞에서는 뛰어야 벼룩이지요.</p>
					</div>
				</div>
				<div class="col-12  col-lg-4 pc_ver">
					<figure class="d-flex"><img src="/images/parasites/flea_con01_img01.png" alt="Meet the bad guys" class=" img-fluid wow fadeInUp"></figure>
				</div>
			</div>
		</div>
	</section>

	<section class="col-3-section">
		<div class="container-1">

			<div class="alternet-list alternet-list-last">
				<div class="row no-gutters">
					<div class="col-md-12 col-lg-6"><img src="/images/parasites/parasites_img_3.jpg" class="w-100" alt="Dirt on Ticks"></div>
					<div class="col-md-12 col-lg-6 align-self-end">
						<div class="text-block alternet-text-block alternet-text-block-bottom">
							<h2 class="anim full-visible"><span class="titleanimation-bottom">벼룩의 위험성</span></h2>
							<p class="wow fadeInUp">
								벼룩은 전 세계적으로 약 2000종이 존재하며, 큰 문제가 되고 있습니다. 외부에서 감염된 벼룩은 집안 곳곳에 퍼져 번식하며, 반려동물 뿐만 아니라 사람에게도 문제를 일으킵니다. 벼룩의 번식력은 굉장히 뛰어나 최대 3개월에 이르는 생활환을 끊지 않으면 순식간에 다시 발생합니다. 12주간 지속되는 브라벡토는 벼룩의 생활환을 완전히 끝낼 수 있습니다.
							</p>
						</div>
					</div>
				</div>
			</div>

		</div>
	</section>
	

	<section>
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-md-12 col-lg-8 wow fadeInUp">
					<div class="benefits mb-more type02">
						<div class="benefits-list mb-0  wow fadeInUp">
							<h3 class="bigheading anim"><span class="titleanimation-bottom">벼룩의 생활환</span></h3>
							<p class="wow fadeInUp">클릭하여 벼룩의 각 단계별 특징을 알아보세요</p>
							<div class="steps-car owl-carousel owl-theme wow fadeInUp">
								<div class="item" data-dot="1. 충란">
									<h4>충란(10일 이내)</h4>
									<p>벼룩은 반려동물에 붙어 하루 40-50여개의 알을 낳으며, 반려동물이 움직임에 따라 집안 구석구석 알이 퍼지게 됩니다.</p>
								</div>
								<div class="item" data-dot="2. 유충">
									<h4>유충(10일)</h4>
									<p>
										알에서 부화한 유충은 침구류, 옷 등에서 성충의 분변 및 각종 유기물을 섭취하며 성장합니다.
									</p>
								</div>
								<div class="item" data-dot="3. 번데기">
									<h4>번데기(11일 이상)</h4>
									<p>
										번데기 속에서 벼룩은 성충으로 자라며 오랫동안 살아남을 수 있습니다. 번데기는 온기, 이산화탄소, 움직임 등에 반응하여 성충으로 변태할 시기를 기다립니다.
									</p>
								</div>
								<div class="item" data-dot="4. 성충">
									<h4>성충(3개월 이내)</h4>
									<p>
										한 번 반려동물에 붙은 벼룩 성충은 일생동안 수많은 알을 낳으며 굉장히 빠른 속도로 번식합니다.
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			   
				<div class="col-md-12 col-lg-4 mob-pedding wow fadeIn">
					<div class="video-block img_width">
						<div class="step-images-slider video-slider owl-carousel owl-theme ">
							<div class="video-item"><img src="/images/parasites/ticks/steps/step_05.png" class="w-100" alt="Paralysis Tick"> </div>
							<div class="video-item"><img src="/images/parasites/ticks/steps/step_06.png" class="w-100" alt="Paralysis Tick"> </div>
							<div class="video-item"><img src="/images/parasites/ticks/steps/step_07.png" class="w-100" alt="Paralysis Tick"> </div>
							<div class="video-item"><img src="/images/parasites/ticks/steps/step_08.png" class="w-100" alt="Paralysis Tick"> </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="alt-color chew_con">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-md-12 col-lg-8">
					<div class="benefits benefits-noimg">
						<div class="benefits-list extrta-mb">
							<h4 class="anim"><span class="titleanimation-bottom">벼룩 관리 요령</span></h4>
							<ul>
								<li>
									<h6 class="anim"><span class="titleanimation-bottom">1단계</span></h6>
									<p class="wow fadeInUp">주기적인 벼룩 검사를 일상화 하세요.</p>
								</li>
								<li>
									<h6 class="anim"><span class="titleanimation-bottom">2단계</span></h6>
									<p class="wow fadeInUp">꼬리에서부터 손으로 반려동물의 털을 쓸어 올리며 움직임을 관찰하세요. 벼룩은 방해를 받으면 뛰어 오르며, 약 2-4mm의 크기입니다.</p>
								</li>
								<li>
									<h6 class="anim"><span class="titleanimation-bottom">3단계</span></h6>
									<p class="wow fadeInUp">위에서 벼룩을 찾지 못하여도 반려동물의 털에 검은 후춧가루같은 점이 있는지 확인하세요. 이는 벼룩의 분변일 수 있습니다.</p>
								</li>
								<li>
									<h6 class="anim"><span class="titleanimation-bottom">4단계</span></h6>
									<p class="wow fadeInUp">이러한 점을 젖은 헝겊 혹은 화장지로 닦아보세요. 검붉은색 혹은 주황색이 묻어나오면 이는 벼룩의 분변이며 벼룩이 존재함을 의미합니다. 이러한 경우 동물병원에 방문하여 수의사 선생님과 브라벡토 사용에 대해 상담하세요.</p>
								</li>
							</ul>
						</div>
					</div>
				</div>

				<div class="col-md-12 col-lg-4 mob-pedding wow fadeIn">
					<div class="video-block top-minouse img_width">
						<a href="https://youtu.be/UjsRpaDJH50" data-toggle="lightbox" data-gallery="youtubevideos">
							<img src="/images/videos/flea_thumb.jpg" class="img-fluid" alt="Ticks Video">
						</a>
						<a href="#" class="playbutton" title="Watch Video"></a>
					</div>
				</div>
			</div>
		</div>
	</section>



	<? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
</body>
</html>
		
