<?session_start();?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/logingCheck.php" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/themeHtml.php" ?>
</head>
<body>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
	<div>
		<div id="sync1" class="owl-carousel owl-theme big-product-carousel">
			<div class="item">
				<div class="desktop-only"><img src="/images/products/spot-on-cats/header-small-cat.jpg" class="w-100" alt="Bravecto Spot On For Cats 1.2 - 2.8 kg"></div>
				<div class="mobile-only"><img src="/images/products/spot-on-cats/header-small-cat-mobile.jpg" class="w-100" alt="Bravecto Spot On For Cats 1.2 - 2.8 kg"></div>
			</div>
			<div class="item">
				<div class="desktop-only"><img src="/images/products/spot-on-cats/header-medium-cat.jpg" class="w-100" alt="Bravecto Spot On For Cats 2.8 - 6.25 kg"></div>
				<div class="mobile-only"><img src="/images/products/spot-on-cats/header-medium-cat-mobile.jpg" class="w-100" alt="Bravecto Spot On For Cats 2.8 - 6.25 kg"></div>
			</div>
			<div class="item">
				<div class="desktop-only"><img src="/images/products/spot-on-cats/header-large-cat.jpg" class="w-100" alt="Bravecto Spot On For Cats 6.25 - 12.5 kg"></div>
				<div class="mobile-only"><img src="/images/products/spot-on-cats/header-large-cat-mobile.jpg" class="w-100" alt="Bravecto Spot On For Cats 6.25 - 12.5 kg"></div>
			</div>
		</div>
	</div>

	<div class="alt-color">
		<div class="container-1">
			<div id="sync2" class="owl-carousel owl-theme dogs cats">
				<div class="item green-cat-active"><span><i></i>1.2 - 2.8 kg</span></div>
				<div class="item blue-cat-active"><span><i></i>> 2.8 - 6.25 kg</span></div>
				<div class="item purpal-cat-active"><span><i></i>> 6.25 - 12.5 kg</span></div>
			</div>
		</div>
	</div>

	<section class="product-section">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-md-12 col-lg-10">
					<div class="product-details-top">
						<h1 class="wow fadeInUp">고양이용 브라벡토 스팟온</h1>
						<h4 class="wow fadeInUp">더욱 쉬워진 브라벡토 스팟온으로 당신의 반려묘를 보호하세요</h4>
						<p class="wow fadeInUp">브라벡토 스팟온만의 TWIST’N’USE 기술로 더욱 손쉽게 투여가 가능합니다.</p>
						<ul class="wow fadeInUp">
							<li>귀진드기로 부터 12주간 보호합니다</li>
							<li>진드기(tick and mite) 및 벼룩에 12주간 효과가 있습니다.</li>
							<li>더욱 쉬워졌습니다. 뚜껑을 뗄 필요도 없이 비틀고, 바르면 됩니다.</li>
							<li>11주령이 갓 지났거나 1.2kg가 조금 넘는 작은 체구의 새끼고양이에서 사용이 가능합니다.</li>
						</ul>
					</div>
				</div>
				<div class="col-md-12 col-lg-8 wow fadeInUp">
					<div class="product-details-top pt-0 pb-180">
						<ul class="nav nav-tabs" id="proTab" role="tablist">
							<li class="nav-item"><a class="nav-link active" id="longlastng-tab" data-toggle="tab" href="#loglasting" role="tab" aria-selected="true" title="Long Lating">편리한 </a></li>
							<li class="nav-item"><a class="nav-link" id="effective-tab" data-toggle="tab" href="#effective" role="tab" aria-selected="false" title="Efective">빠르고 오래가는</a></li>
							<li class="nav-item"><a class="nav-link" id="hasslefree-tab" data-toggle="tab" href="#hasslefree" role="tab" aria-selected="false" title="Hassle Free">안전한</a></li>
							<li class="nav-item"> <a class="nav-link" id="global-tab" data-toggle="tab" href="#global" role="tab" aria-selected="false" title="Global">세계적인</a> </li>
						</ul>
						<div class="tab-content" id="proTabContent">
							<div class="tab-pane fade show active" id="loglasting" role="tabpanel">
								<h4>편리한</h4>
								<p>브라벡토는 1회 사용만으로 12주간 외부기생충으로 부터 반려묘를 보호합니다. 
								매 계절 1회 사용으로 기억하기 쉽고, 외부기생충 예방을 놓칠 일이 적어집니다.</p>
								<!--<a href="benefits" target="_blank" class="findoutmore" title="Find out more"><span>Find out more</span></a>-->
							</div>
							<div class="tab-pane fade" id="effective" role="tabpanel" aria-labelledby="effective-tab">
								<h4>빠르고 오래가는</h4>
								<p>
									브라벡토의 주성분 플루랄라너는 사용 4시간 이내에 작용하기 시작하여, 12시간 이내에
									진드기의 100%를 사멸합니다. 또한 플루랄라너의 대부분이 비활성 상태로 저장되어 있다가 12주간 서서히 방출됩니다.
								</p>
								<!--<a href="benefits" target="_blank" class="findoutmore" title="Find out more"><span>Find out more</span></a>-->
							</div>
							<div class="tab-pane fade" id="hasslefree" role="tabpanel" aria-labelledby="hasslefree-tab">
								<h4>안전한</h4>
								<p>
									브라벡토는 11주령이 갓 지났거나 1.2kg가 조금 넘는 작은 체구의 새끼고양이에서 안심하고 사용할 수 있습니다.
									미국 FDA, 유럽 EMEA 등 수많은 국가에서 그 안전성을 검증받았습니다.  
								</p>
								<!--<a href="benefits" target="_blank" class="findoutmore" title="Find out more"><span>Find out more</span></a>-->
							</div>
							<div class="tab-pane fade" id="global" role="tabpanel" aria-labelledby="global-tab">
								<h4>세계적인</h4>
								<p>브라벡토는 전 세계 85여개 국가에서 판매되며, 2019년 1억 도스 판매를 돌파하였습니다.
								엄청난 판매량으로 그 안전성이 검증된 브라벡토의 놀라운 효능을 경험해 보세요.</p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12 col-lg-4 mob-pedding wow fadeIn">
					<div class="video-block wow fadeInUp"> <img src="/images/products/spot-on-cats/section-1.jpg" alt="Bravecto Spot-on for Cats" class="w-100"></div>
				</div>
			</div>
		</div>
	</section>

	<section class="alt-color">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-md-12 col-lg-8">
					<div class="benefits benefits-noimg">
						<div class="benefits-list wow fadeInUp"> <a href="#" class="redbg-link">플루랄라너</a> </div>
						<div class="benefits-list extrta-mb">
							<h3 class="wow fadeInUp">브라벡토만의 혁신적인 성분, 플루랄라너</h3>
							<p class="wow fadeInUp">2014년 미국 FDA를 시작으로, 전 세계 85여개 국가에서 허가를 받은 브라벡토의 성분 플루랄라너는 
							혈액에 흡수됨과 동시에 대부분이 비활성화 상태로 저장되어 서서히 방출됩니다.
							12주나 되는 기간동안 그 효능의 대부분을 유지하여 반려묘에게 문제가 되는 외부기생충을 
							더욱 간편하고 안전하게 예방할 수 있게 되었죠. </p>
						</div>
						<div class="benefits-list extrta-mb wow fadeInUp">
							<h3>방대한 임상 연구와 모니터링</h3>
							<p>브라벡토(플루랄라너)는 저희 MSD동물약품의 비전 ‘The Science of Healthier Animals’ 의
							놀라운 결과물입니다. 방대한 임상 연구와 모니터링을 바탕으로 반려동물들이 브라벡토(플루랄라너)의 
							강력한 효능과 지속성 및 높은 안전성을 경험할 수 있게 되었습니다.</p>
						</div>
					</div>
				</div>
				<div class="col-md-12 col-lg-4 mob-pedding wow fadeIn">
					<div class="video-block top-minouse">
						<img src="/images/products/spot-on-cats/section-2.jpg" class="w-100" alt="little girl feeding a cat">

					</div>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-md-12 col-lg-8 wow fadeInUp">
					<div class="benefits benefits-noimg">
						<div class="benefits-list mb-0  wow fadeInUp">
							<h3 class="bigheading">브라벡토 스팟온 투여 방법</h3>
							<div class=" steps-car owl-carousel owl-theme">
								<div class="item" data-dot="step 1">
									<p>브라벡토 스팟온의 안전포장지를 뜯습니다.</p>
									<figure><img src="/images/steps/step-1.jpg" class="img-fluid" alt="How to use Bravecto Spot-on for Cats"></figure>
								</div>
								<div class="item" data-dot="step 2">
									<p>브라벡토 스팟온의 뚜껑을 위로 향하게 잡고, 비틀어 줍니다.<p>
									<p><b>뚜껑은 떨어지지 않으니 무리하게 떼지 마십시오.</b></p>
									<figure><img src="/images/steps/step-2.jpg" class="img-fluid" alt="How to use Bravecto Spot-on for Cats"></figure>
								</div>
								<div class="item" data-dot="step 3">
									<p>반려묘의 털을 갈라 피부가 노출되게 한 후 목덜미 쪽 피부선을 따라 브라벡토 스팟온을 바릅니다.</p>
									<figure><img src="/images/steps/step-3.jpg" class="img-fluid" alt="How to use Bravecto Spot-on for Cats"></figure>
								</div>
								<div class="item" data-dot="step 4">
									<p>6.25 kg 미만의 고양이는 한 군데, 6.25 kg 이상의 고양이는 두 군데에 나누어 발라주세요.</p>
									<figure><img src="/images/steps/step-cat-4.png" class="img-fluid" alt="How to use Bravecto Spot-on for Cats"></figure>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12 col-lg-4 mob-pedding wow fadeIn">
					<div class="video-block top-minouse">
						<a href="https://youtu.be/iz7_X4AO9CQ?autoplay=1&rel=0" data-toggle="lightbox" data-gallery="youtubevideos">
							<img src="/images/videos/spot-on-cats-video.jpg" class="img-fluid" alt="Spot On Cats Video">
						</a>
						<a href="#" class="playbutton" title="Watch Video"></a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="alt-color">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-md-12 col-lg-8 moile-last">
					<div class="benefits benefits-noimg">
						<div class="benefits-list wow fadeInUp">
							<h2>더 궁금하신 점이 있으신가요?</h2>
							<div class="accordion" id="faqs">
								<div class="faq-list wow fadeInUp">
									<a data-toggle="collapse" data-target="#f1" aria-expanded="true" aria-controls="f1" href="#" title="Where can I buy Bravecto?"> Where can I buy Bravecto? </a>
									<div id="f1" class="collapse show " data-parent="#faqs">
										<div class="answer">Bravecto is available from veterinarians and leading pet shops. </div>
									</div>
								</div>
								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f6" aria-expanded="true" aria-controls="f6" href="#"> What will Bravecto protect my pet against? </a>
									<div id="f6" class="collapse" data-parent="#faqs">
										<div class="answer">
											Bravecto is available in a range of products that provide long-lasting protection against common
											parasites in dogs and cats with just one dose*
											:<br>
											<table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered">
												<tr>
													<th scope="row">Product* </th>
													<td><strong>Bravecto Spot-on for Cats</strong></td>
												</tr>
												<tr>
													<th scope="row">Fleas</th>
													<td>
														<span class="correct">&nbsp;</span>
														Up to 3 months
													</td>
												</tr>
												<tr>
													<th scope="row">Paralysis Ticks</th>
													<td>
														<span class="correct">&nbsp;</span>
														3 months
													</td>
												</tr>
											</table>

											<small>*Refer to product label for full claim details.</small>
										</div>
									</div>
								</div>
								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f2" aria-expanded="true" aria-controls="f2" href="#"> How do I apply Bravecto Spot-on</a>
									<div id="f2" class="collapse" data-parent="#faqs">
										<div class="answer">
											Bravecto Spot-on comes in a clever <strong>TWIST&acute;N&acute;USE</strong> tube with a non-removable cap, it is simple and
											convenient to apply:
											<img src="/images/products/how-to-apply.png" alt="How to apply Bravecto Spot-on Cats" style="max-width: 500px; width: 100%; position: relative;" class="my-3" />

											Apply contents of the appropriate tube for your cat’s body weight to the application sites.

										</div>
									</div>
								</div>
								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f3" aria-expanded="true" aria-controls="f3" href="#"> What pack size does Bravecto Spot-on for Cats come in? </a>
									<div id="f3" class="collapse" data-parent="#faqs">
										<div class="answer">
											Bravecto Spot-on for Cats is available in three pack sizes, ranging from 1.2 kg to 12.5 kg. One dose provides up to 3 months protection against fleas and paralysis ticks. Each pack contains 2 doses.

											<img class="my-2" src="/images/products/cat-pack-sizes.png" style="max-width: 500px;  width: 100%; position: relative;" />
										</div>
									</div>
								</div>
								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f4" aria-expanded="true" aria-controls="f4" href="#"> Why might long-lasting protection be better than monthly treatments? </a>
									<div id="f4" class="collapse" data-parent="#faqs">
										<div class="answer">Gaps in protection can occur when flea and tick treatments are not administered at the correct treatment interval. Bravecto assists with treatment compliance for an extended period with just one dose, offering you peace of mind.</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f7" aria-expanded="true" aria-controls="f7" href="#"> When should cats start treatment with Bravecto Spot-on for Cats?</a>
									<div id="f7" class="collapse" data-parent="#faqs">
										<div class="answer">
											Bravecto Spot-on for Cats can be administered to cats as early as 11 weeks of age. Cats should weigh
											at least 1.2 kg.
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f8" aria-expanded="true" aria-controls="f8" href="#">My cat weighs over 12.5 kgs – how do I treat him/her?</a>
									<div id="f8" class="collapse" data-parent="#faqs">
										<div class="answer">
											For cats above 12.5 kg body weight, use a combination of two pipettes that most closely matches
											the body weight.
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f9" aria-expanded="true" aria-controls="f9" href="#">When is it safe for me to pat/cuddle my cat after Bravecto spot-on application?</a>
									<div id="f9" class="collapse" data-parent="#faqs">
										<div class="answer">You should wait until the application area is dry before touching the area.</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f10" aria-expanded="true" aria-controls="f10" href="#">My cat is on medication for another condition right now. Is it safe to give Bravecto?</a>
									<div id="f10" class="collapse" data-parent="#faqs">
										<div class="answer">
											It is always best to discuss all of your cat's treatments with your veterinarian as they are your cat's
											health care expert. Studies with Bravecto Spot-on for Cats have shown no interactions with routinely
											used veterinary medicinal products.
										</div>
									</div>
								</div>


								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f11" aria-expanded="true" aria-controls="f11" href="#">How does Bravecto kill fleas and ticks?</a>
									<div id="f11" class="collapse" data-parent="#faqs">
										<div class="answer">
											After you treat your pet with Bravecto Spot-on, the active ingredient fluralaner is absorbed into the
											bloodstream and quickly reaches tissue fluids just under your pet’s skin. When fleas and ticks feed,
											they take in fluralaner and die. Bravecto Spot-on for Cats kills pre-existing and new infestations of
											paralysis ticks for 3 months. Fleas are killed in up to 12 hours after treatment or after
											reinfestation during the 3 month treatment period. The flea life cycle can be broken by this rapid
											onset of action and long-lasting efficacy.
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f12" aria-expanded="true" aria-controls="f12" href="#">Will Bravecto kill ticks and fleas that are already on my cat?</a>
									<div id="f12" class="collapse" data-parent="#faqs">
										<div class="answer">
											Yes, Bravecto Spot-on for Cats kills pre-existing paralysis ticks and fleas as well as newly arrived
											parasites on your cat.
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f13" aria-expanded="true" aria-controls="f13" href="#">Can dead and attached ticks be found on treated cats?</a>
									<div id="f13" class="collapse" data-parent="#faqs">
										<div class="answer">
											Yes. Due to the way Bravecto works, dead attached ticks can be still seen on cats following
											treatment. Dead ticks can be easily removed as opposed to live ticks which take some force to
											dislodge from the skin of the cat.
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f14" aria-expanded="true" aria-controls="f14" href="#">Can I use Bravecto Spot-on for Cats all year-round?</a>
									<div id="f14" class="collapse" data-parent="#faqs">
										<div class="answer">
											Yes, it is safe to use Bravecto all year-round. Fleas can be a year-round problem indoors and
											outdoors. Applying Bravecto Spot-on for Cats every 3 months ensures your cat will be protected
											from fleas throughout the year with just 4 doses.
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f15" aria-expanded="true" aria-controls="f15" href="#">Do I really need to treat for fleas all year round?</a>
									<div id="f15" class="collapse" data-parent="#faqs">
										<div class="answer">
											There are very few places in Australia where there are no fleas. In warmer climates, fleas are
											present all year round while in more mild climates fleas are present mainly from late spring through
											to late autumn, and even longer thanks to central heating. Bravecto Spot-on for Cats should be
											administered every 3 months, all year round to avoid giving flea populations a chance to build up in
											your home.
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f16" aria-expanded="true" aria-controls="f16" href="#">
										Is it necessary to use additional insecticides to control flea stages in the
										environment?
									</a>
									<div id="f16" class="collapse" data-parent="#faqs">
										<div class="answer">
											No. Bravecto Spot-on for Cats kills fleas and prevents new flea infestations for up to 3 months. As
											the majority of a flea population developing in the environment will emerge as adult fleas over this
											time, Bravecto will effectively control the entire flea population.
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f17" aria-expanded="true" aria-controls="f17" href="#">Can Bravecto Spot-on for Cats be used in breeding/pregnant/lactating cats? </a>
									<div id="f17" class="collapse" data-parent="#faqs">
										<div class="answer">
											The safety of Bravecto Spot-on for Cats has not been established during breeding, pregnancy or
											lactation.
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f18" aria-expanded="true" aria-controls="f18" href="#">
										Why does my cat scratch even more on the first day of Bravecto treatment?
									</a>
									<div id="f18" class="collapse" data-parent="#faqs">
										<div class="answer">
											When fleas are in the process of dying their movements become uncoordinated. This may cause a
											skin sensation which can result in increased scratching of the cat. However, this phenomenon is
											quickly resolved once the fleas are dead.
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f19" aria-expanded="true" aria-controls="f19" href="#">Why do I see fleas after I have administered Bravecto?</a>
									<div id="f19" class="collapse" data-parent="#faqs">
										<div class="answer">
											Fleas can continually re-infest treated cats – either from juvenile flea life stages that have just
											matured to adults in the household or from fleas that jump onto the pet when outside or visiting
											other homes. Bravecto will quickly kill these fleas. Also, as fleas die, they can move from the skin
											surface where they feed, to the tips of the hairs in the coat, making them more visible to the human
											eye.
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f24" aria-expanded="true" aria-controls="f24" href="#">Where can I get more information about Bravecto’s claims?</a>
									<div id="f24" class="collapse" data-parent="#faqs">
										<div class="answer">
											All <a class="inlinelink" href="https://portal.apvma.gov.au/pubcris;jsessionid=AjAebiai95var7phRx1L2QBS?p_auth=RBZ2QvsL&p_p_id=pubcrisportlet_WAR_pubcrisportlet&p_p_lifecycle=1&p_p_state=normal&p_p_mode=view&p_p_col_id=column-1&p_p_col_pos=2&p_p_col_count=4&_pubcrisportlet_WAR_pubcrisportlet_javax.portlet.action=search" title="product labels for Bravecto" target="_blank">product labels for Bravecto</a> can be found on the APVMA (Australian Pesticides and Veterinary
											Medicines Authority) Pubcris database.

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12 col-lg-4 mobile-first">
					<div class="where-to-get top-minouse wow fadeInUp">
						<h2>브라벡토 구매를 원하시나요?</h2>
						<p>당신의 반려묘에게 가장 알맞은 제품에 대해 수의사와 상담하세요. 
						구입을 원하신다면 거주지역을 아래 검색창에 입력하세요.</p>
						<form action="/page/stockists.php">
							<input name="zipcode" type="text" placeholder="suburb">
							<button class="arror-btn" type="submit"></button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
</body>
</html>
		
