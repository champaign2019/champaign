<?session_start();?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/logingCheck.php" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/themeHtml.php" ?>
</head>
<body>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
	<section class="inner-header">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-12  col-lg-6">
					<div class="header-text-inner">
						<h1 class="anim"><span class="titleanimation-bottom">수의사를 위한 정보</span></h1>
						<p class="wow fadeInUp">동물병원을 방문하는 보호자들을 위해 더욱<br/>전문적인 자료를 확인하세요</p>
					</div>
				</div>
				<div class="col-12  col-lg-6 ">
					<figure class="d-flex"><img src="/images/header-vet-information.png" alt="Information for vets" class=" img-fluid wow fadeInUp"></figure>
				</div>
			</div>
		</div>
	</section>

	<section class="col-3-section less-pading">
		<div class="container-1">
			<div class="alternet-list ">
				<div class="row no-gutters">
					<div class="col-md-12 col-lg-6 moile-last">
						<div class="text-block alternet-text-block ">
							<h2 class="anim full-visible"><span class="titleanimation-bottom">브라벡토와 함께하세요!</span></h2>
							<p class="wow fadeInUp">진드기(tick and mite) 및 벼룩 예방에 독보적인 브라벡토 판매를 희망하시나요?</p>
							<a href="/page/contact-us.php" class="findoutmore wow fadeInUp" title="Become a stockist"><span>브라벡토 구입 문의 : ㈜에스틴 / 전화번호</span></a>
						</div>
					</div>
					<div class="col-md-12 col-lg-6"><img src="/images/stockist.jpg" class="w-100" alt="Become a stockist"></div>
				</div>
			</div>
		</div>
	</section>

	<section class="col-3-section less-pading">
		<div class="container-1">
			<div class="alternet-list">
				<div class="row no-gutters">
					<div class="col-md-12 col-lg-6">
						<img src="/images/cat-vet-information.jpg" class="w-100" alt="Vet resources">
					</div>
					<div class="col-md-12 col-lg-6 align-self-end">
						<div class="text-block alternet-text-block alternet-text-block-bottom">
							<h2 class="anim full-visible"><span class="titleanimation-bottom">Vet resources</span></h2>
							<p class="wow fadeInUp">An online centre for vets providing easy access to downloadable Bravecto product information, clinical papers and marketing resources. </p>
							<a href="/page/vets-resources.php" class="findoutmore wow fadeInUp" title="Checkout all Vet vesources"><span>See all</span></a>
						</div>
					</div>

				</div>
			</div>
		</div>
	</section>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
</body>
</html>
		
