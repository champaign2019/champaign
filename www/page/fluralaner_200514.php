<?session_start();?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/logingCheck.php" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/themeHtml.php" ?>
</head>
<body>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
	<section class="inner-header">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-12  col-lg-6 m_ver">
					<figure class="d-flex"><img src="/images/parasites/dog-meet-the-bad-guys.png" alt="Meet the bad guys" class=" img-fluid wow fadeInUp"></figure>
				</div>
				<div class="col-12  col-lg-6">
					<div class="header-text-inner">
						<h1 class="anim" style="color:#9f2793; "><span class="titleanimation-bottom">혁신의 비밀, 플루랄라너</span></h1>
						<p class="wow fadeInUp">브라벡토의 특허 성분인 플루랄라너는 전신적으로 진드기(tick and mite)및 벼룩을 구제하며, 안전하게 투여가 가능한 제제입니다. 플루랄라너는 절지동물의 신경계만을 선택적으로 교란시키기 때문에¹ 반려동물 외부기생충 예방약으로 안전하게 사용할 수 있습니다.</p>
					</div>
				</div>
				<div class="col-12  col-lg-6 pc_ver">
					<figure class="d-flex"><img src="/images/parasites/dog-meet-the-bad-guys.png" alt="Meet the bad guys" class=" img-fluid wow fadeInUp"></figure>
				</div>
			</div>
		</div>
	</section>

	<section class="col-3-section chew_con">
		<div class="container-1">

			<div class="alternet-list">
				<div class="row no-gutters">
					<div class="col-md-12 col-lg-6 moile-last">
						<div class="text-block alternet-text-block">
							<h2 class="anim full-visible"><span class="titleanimation-bottom">빠르고 오래갑니다</span></h2>
							<p class="wow fadeInUp">플루랄라너는 반려동물에 투여한 이후 빠르게 작용하여 흡혈로 인해 질병이 전파될 수 있는 '골든타임' 이내에 진드기(tick and mite) 및 벼룩을 구제합니다.²
브라벡토는 벼룩에서 투여 후 2시간 이내에 작용하기 시작하여 12시간 이내에 100%가 사멸되며³. 진드기에서 투여 후 4시간 이내에 작용하기 시작하여 12시간 이내에 100%가 사멸됩니다⁴.
브라벡토의 주성분 플루랄라너는 반려동물의 체내에서 12주간 작용합니다. 덕분에 투여일자 계산이 더욱 쉽고, 그만큼 반려동물이 더 빈틈 없이 안전하게 생활할 수 있습니다.</p>
						</div>
					</div>
					<div class="col-md-12 col-lg-6 mobile-first">
						<div class="video-block top-minouse img_width"> 
						<a class="img_height_media" href="https://youtu.be/eJ7Zd0Hjj7s" data-toggle="lightbox" data-gallery="youtubevideos">
							<img src="/images/sum04.jpg" class="w-100" alt="Facts About Fleas">
						</a>
						<a href="#" class="playbutton" title="Watch Video" style="left:auto; right:0;"></a>
					</div>
					
					</div>
				</div>
			</div>

			<div class="alternet-list alternet-list-last">
				<div class="row no-gutters">
					<div class="col-md-12 col-lg-6"><img src="/images/parasites/dog-ticks.jpg" class="w-100" alt="Dirt on Ticks"></div>
					<div class="col-md-12 col-lg-6 align-self-end">
						<div class="text-block alternet-text-block alternet-text-block-bottom">
							<h2 class="anim full-visible"><span class="titleanimation-bottom">더 안전하게, 더 행복하게</span></h2>
							<p class="wow fadeInUp">
								강아지용 브라벡토는 8주령 및 체중 2kg 이상, 임신 또는 수유중인 강아지에서 안전하게 투여가 가능하며, 콜리종에서 안심하고 먹일 수 있습니다⁵. 고양이용 브라벡토는 9주령 및 체중 1.2kg 이상의 아기고양이에서도 안전하게 투여가 가능합니다.
							</p>
						</div>
					</div>
				</div>
			</div>

		</div>
	</section>

	<section class="mitemite_con02">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-md-12 col-lg-12 wow fadeInUp">
					<div class="benefits benefits-noimg">
						<div class="benefits-list mb-0  wow fadeInUp">
							<h3 class="bigheading">플루랄라너의 혁신, 어떻게 작용할까요?</h3>
							<div class=" steps-car owl-carousel owl-theme">
								<div class="item" data-dot="1">
									<div class="txt_box">
										<h4></h4>
										<p>츄어블 또는 스팟온으로 투여된 브라벡토는 주성분인 플루랄라너 형태로 혈액에 흡수됩니다.</p>
									</div>
									<figure><img src="/images/flu_img01.png" class="img-fluid" alt="How to use Bravecto Spot-on for Cats"></figure>
								</div>
								<div class="item" data-dot="2">
									<div class="txt_box">
										<h4></h4>
										<p>흡수된 플루랄라너는 대부분이 혈액 내의 혈장단백질과 결합하여 저장됩니다.</p>
									</div>
									<figure><img src="/images/flu_img02.png" class="img-fluid" alt="How to use Bravecto Spot-on for Cats"></figure>
								</div>
								<div class="item" data-dot="3">
									<div class="txt_box">
										<h4></h4>
										<p>저장된 플루랄라너는 서서히 혈액으로 방출되며, 피부 아래 층에 위치하게 됩니다.</p>
									</div>
									<figure><img src="/images/flu_img03.png" class="img-fluid" alt="How to use Bravecto Spot-on for Cats"></figure>
								</div>
								<div class="item" data-dot="4">
									<div class="txt_box">
										<h4></h4>
										<p>이로 인해 진드기 및 벼룩이 흡혈을 시도하게 되면 플루랄라너에 의해 신경이 마비됩니다.</p>
									</div>
									<figure><img src="/images/flu_img04.png" class="img-fluid" alt="How to use Bravecto Spot-on for Cats"></figure>
								</div>
								<div class="item" data-dot="5">
									<div class="txt_box">
										<h4></h4>
										<p>이러한 작용은 12주간 유지되며, 외부기생충의 신경계에 선택적으로 작용하여 안전합니다.</p>
									</div>
									<figure><img src="/images/flu_img05.png" class="img-fluid" alt="How to use Bravecto Spot-on for Cats"></figure>
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

<!-- 	<section class="alt-color chew_con"> -->
<!-- 		<div class="container-1"> -->
<!-- 			<div class="row no-gutters"> -->
<!-- 				<div class="col-md-12 col-lg-8"> -->
<!-- 					<div class="benefits benefits-noimg"> -->
<!-- 						<div class="benefits-list extrta-mb"> -->
<!-- 							<h4 class="anim"><span class="titleanimation-bottom">플루랄라너의 혁신, 어떻게 작용할까요?</span></h4> -->
<!-- 							<ul> -->
<!-- 								<li class="inline_box"> -->
<!-- 									<h6 class="anim"><span class="titleanimation-bottom">1</span></h6> -->
<!-- 									<p class="wow fadeInUp">츄어블 또는 스팟온으로 투여된 브라벡토는 주성분인 플루랄라너 형태로 혈액에 흡수됩니다.</p> -->
<!-- 								</li> -->
<!-- 								<li class="inline_box"> -->
<!-- 									<h6 class="anim"><span class="titleanimation-bottom">2</span></h6> -->
<!-- 									<p class="wow fadeInUp">흡수된 플루랄라너는 대부분이 혈액 내의 혈장단백질과 결합하여 저장됩니다.</p> -->
<!-- 								</li> -->
<!-- 								<li class="inline_box"> -->
<!-- 									<h6 class="anim"><span class="titleanimation-bottom">3</span></h6> -->
<!-- 									<p class="wow fadeInUp">저장된 플루랄라너는 서서히 혈액으로 방출되며, 피부 아래 층에 위치하게 됩니다.</p> -->
<!-- 								</li> -->
<!-- 								<li class="inline_box"> -->
<!-- 									<h6 class="anim"><span class="titleanimation-bottom">4</span></h6> -->
<!-- 									<p class="wow fadeInUp">이로 인해 진드기 및 벼룩이 흡혈을 시도하게 되면 플루랄라너에 의해 신경이 마비됩니다.</p> -->
<!-- 								</li> -->
<!-- 								<li class="inline_box"> -->
<!-- 									<h6 class="anim"><span class="titleanimation-bottom">5</span></h6> -->
<!-- 									<p class="wow fadeInUp">이러한 작용은 12주간 유지되며, 외부기생충의 신경계에 선택적으로 작용하여 안전합니다.</p> -->
<!-- 								</li> -->
<!-- 							</ul> -->
<!-- 						</div> -->
<!-- 					</div> -->
<!-- 				</div> -->
<!--  -->
<!-- 				<div class="col-md-12 col-lg-4 mob-pedding wow fadeIn"> -->
<!-- 					<div class="video-block top-minouse img_width mid_img"> -->
<!-- 						<img src="/images/videos/ticks-video.png" class="img-fluid" > -->
<!-- 					</div> -->
<!-- 				</div> -->
<!-- 			</div> -->
<!-- 		</div> -->
<!-- 	</section> -->

	<div class="footer_top_txt_box">
		<div class="container-1">
			<p>
				1. Ozoe Y, Asahi M, Ozoe F, Nakahira K, Mita T: The antiparasitic isoxazoline A1443 is a potent blocker of insect ligand-gated chloride channels. Biochem Biophys Res Commun. 2010, 391: 744-749. 10.1016/j.bbrc.2009.11.131.<br/>
				2. Little SE. Changing paradigms in understanding transmission of canine tick-borne disease: the role of interrupted feeding and intrastadial transmission. Proceedings of the 2nd Canine Vector-Borne Disease (CVBD) Symposium. April 25-28, 2007. Mazaro del Vallo, Sicily, Italy.<br/>
				3. Taenzler, Janina & Wengenmayer, Christina & Williams, Heike & Fourie, Josephus & Zschiesche, Eva & Roepke, Rainer & Heckeroth, Anja. (2014). Onset of activity of fluralaner (BRAVECTO (TM)) against Ctenocephalides felis on dogs. Parasites & vectors. 7. 567. 10.1186/PREACCEPT-1563099569137345.<br/>
				4. Wengenmayer, Christina & Williams, Heike & Zschiesche, Eva & Moritz, Andreas & Langenstein, Judith & Roepke, Rainer & Heckeroth, Anja. (2014). The speed of kill of fluralaner (Bravecto (TM)) against Ixodes ricinus ticks on dogs. Parasites & Vectors. 7. 10.1186/s13071-014-0525-3. <br/>
				5. Walther, Feli & Paul, Allan & Allan, Mark & Roepke, Rainer & Nuernberger, Martin. (2014). Safety of fluralaner, a novel systemic antiparasitic drug, in MDR1(-/-) Collies after oral administration. Parasites & vectors. 7. 86. 10.1186/1756-3305-7-86.

			</p>
		</div>
	</div>

	<? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
</body>
</html>
		
