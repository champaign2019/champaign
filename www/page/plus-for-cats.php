<?session_start();?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/logingCheck.php" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/themeHtml.php" ?>
</head>
<body>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
	<div>
		<div id="sync1" class="owl-carousel owl-theme big-product-carousel">
			<div class="item">
				<div class="desktop-only"><img src="/images/products/plus-for-cats/headerimages_catspotonplus_desktop_S.jpg" class="w-100" alt="Bravecto Plus For Cats 1.2 - 2.8 kg"></div>
				<div class="mobile-only"><img src="/images/products/plus-for-cats/headerimages_catspotonplus_mobile_S.jpg" class="w-100" alt="Bravecto Plus For Cats 1.2 - 2.8 kg"></div>
			</div>
			<div class="item">
				<div class="desktop-only"><img src="/images/products/plus-for-cats/headerimages_catspotonplus_desktop_M.jpg" class="w-100" alt="Bravecto Plus For Cats 2.8 - 6.25 kg"></div>
				<div class="mobile-only"><img src="/images/products/plus-for-cats/headerimages_catspotonplus_mobile_M.jpg" class="w-100" alt="Bravecto Plus For Cats 2.8 - 6.25 kg"></div>
			</div>
			<div class="item">
				<div class="desktop-only"><img src="/images/products/plus-for-cats/headerimages_catspotonplus_desktop_L.jpg" class="w-100" alt="Bravecto Plus For Cats 6.25 - 12.5 kg"></div>
				<div class="mobile-only"><img src="/images/products/plus-for-cats/headerimages_catspotonplus_mobile_L.jpg" class="w-100" alt="Bravecto Plus For Cats 6.25 - 12.5 kg"></div>
			</div>
		</div>
	</div>

	<div class="alt-color">
		<div class="container-1">
			<div id="sync2" class="owl-carousel owl-theme dogs cats">
				<div class="item green-cat-active"><span><i></i>1.2 - 2.8 kg</span></div>
				<div class="item blue-cat-active"><span><i></i>> 2.8 - 6.25 kg</span></div>
				<div class="item purpal-cat-active"><span><i></i>> 6.25 - 12.5 kg</span></div>
			</div>
		</div>
	</div>

	<section class="product-section">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-md-12 col-lg-10">
					<div class="product-details-top">
						<h1 class="wow fadeInUp">Bravecto <span class="plus">Plus</span> for Cats</h1>
						<h4 class="wow fadeInUp">
							Monthly treatments can be easy to lose track of, so we've made them last twice as long. Our spot-on treatment will guard your cat against fleas and paralysis ticks plus heartworm for 2 months and treat intestinal worms<sup>+</sup> and ear mites - all in one dose.
						</h4>

						<ul class="wow fadeInUp">
							<li>The first spot-on treatment to give your cat effective defence against fleas, paralysis ticks and heartworm with intestinal worm<sup>+</sup> and ear mite treatment - all in one</li>
							<li>Extended protection from a single dose keeps your furry friend guarded for twice as long as current monthly treatments</li>
							<li>Application is quick and easy with our <strong>TWIST&acute;N&acute;USE</strong> tube</li>
						</ul>
					</div>
				</div>
				<div class="col-md-12 col-lg-8 wow fadeInUp">
					<div class="product-details-top pt-0 pb-180">
						<ul class="nav nav-tabs" id="proTab" role="tablist">
							<li class="nav-item"><a class="nav-link active" id="boarddefence-tab" data-toggle="tab" href="#boarddefence" role="tab" aria-selected="true" title="Board Defence">BROAD DEFENCE</a></li>
							<li class="nav-item"><a class="nav-link" id="longerlasting-tab" data-toggle="tab" href="#longerlasting" role="tab" aria-selected="false" title="Efective">LONGER LASTING</a></li>
							<li class="nav-item"><a class="nav-link" id="easy-tab" data-toggle="tab" href="#easy" role="tab" aria-selected="false" title="Hassle Free">EASY</a></li>
						</ul>
						<div class="tab-content" id="proTabContent">
							<div class="tab-pane fade show active" id="boarddefence" role="tabpanel" aria-labelledby="boarddefence-tab">
								<h4>
									Convenient and reliable protection against fleas, paralysis ticks and heartworm plus intestinal worm<sup>+</sup> and ear mites treatment
								</h4>
								<p>
									Life is simpler without the annoyance of fleas and the deadly threat of ticks and worms. Bravecto works fast and continues to protect against fleas for 3 months, paralysis ticks for 10 weeks and heartworm for 2 months, plus treats intestinal worms<sup>+</sup> and ear mites. With one application it’s easy to keep your cat purring happily all year round.
								</p>
								<p>
									<small><sup>+</sup>Applies to roundworm and hookworm</small>
								</p>
								<!--<a href="benefits" target="_blank" class="findoutmore" title="Find out more"><span>Find out more</span></a>-->
							</div>
							<div class="tab-pane fade" id="longerlasting" role="tabpanel" aria-labelledby="longerlasting-tab">
								<h4>Treat every 2 months - longer lasting parasite protection from one dose</h4>
								<p>
									Bravecto <span class="plus">Plus</span> for Cats now defends against deadly heartworm for 2 months and effectively treats roundworm, hookworm and ear mites. With long-lasting parasite protection from a single dose, your cat is free to explore and enjoy life without the threat of nasty little parasites.
								</p>
								
								<!--<a href="benefits" target="_blank" class="findoutmore" title="Find out more"><span>Find out more</span></a>-->
							</div>
							<div class="tab-pane fade" id="easy" role="tabpanel" aria-labelledby="easy-tab">
								<h4>Life is simpler with our convenient spot-on application</h4>
								<p>
									Our <strong>TWIST&acute;N&acute;USE</strong> tube makes Bravecto <span class="plus">Plus</span> easy and convenient to apply, giving you confidence in successful protection even with pets who have Houdini-like abilitiies in making swallowed pills reappear outside their mouth moments later.
								</p>
							  
								<!--<a href="benefits" target="_blank" class="findoutmore" title="Find out more"><span>Find out more</span></a>-->
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12 col-lg-4 mob-pedding wow fadeIn">
					<div class="video-block wow fadeInUp"> <img src="/images/products/plus-for-cats/section-1.png" alt="Bravecto Plus for Cats" class="w-100"></div>
				</div>
			</div>
		</div>
	</section>

	<section class="alt-color">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-md-12 col-lg-8">
					<div class="benefits benefits-noimg">
						<div class="benefits-list wow fadeInUp"> <a href="#" class="redbg-link">SAFETY</a> </div>
						<div class="benefits-list extrta-mb">
							<h3 class="wow fadeInUp">
								Bravecto <span class="plus">Plus</span> is safe for kittens over 9 weeks of age, weighing at least 1.2 kg
							</h3>
							<p class="wow fadeInUp">
								Once your kitten is old enough to enjoy an active outdoor lifestyle, you'll have peace of mind that they’re protected from the risks of fleas, paralysis ticks and heartworm plus treated for intestinal worms<sup>+</sup> and ear mites.
								<br /><br />
								<small><i>+ Applies to roundworm and hookworm</i></small>
							</p>

						</div>
						<div class="benefits-list extrta-mb wow fadeInUp">
							<h3>Over 80 million doses dispensed worldwide</h3>
							<p>Bravecto is proud to deliver a safe, well-tested solution for dogs and cats worldwide against ticks and fleas. Our products have undergone rigorous regulatory approval processes and are sold with confidence in over 70 countries and are determined to be safe when used according to the label.</p>
						</div>
						<div class="benefits-list extrta-mb">
							<h3 class="wow fadeInUp">Extensively tested and continually monitored</h3>
							<p class="wow fadeInUp">With comprehensive clinical research, Bravecto <span class="plus">Plus</span> has been proven safe for use in kittens over 9 weeks of age weighing at least 1.2 kg</p>
							<p class="wow fadeInUp">Global safety monitoring of Bravecto use continues to provide additional compelling evidence of the safety of the product.</p>
						</div>
					</div>
				</div>
				<div class="col-md-12 col-lg-4 mob-pedding wow fadeIn">
					<div class="video-block top-minouse">
						<img src="/images/products/plus-for-cats/section-2.png" class="w-100" alt="little girl feeding a cat">

					</div>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-md-12 col-lg-8 wow fadeInUp">
					<div class="benefits benefits-noimg">
						<div class="benefits-list mb-0  wow fadeInUp">
							<h3 class="bigheading">How to use Bravecto <span class="plus">Plus</span> for Cats</h3>
							<div class="steps-car owl-carousel owl-theme">
								<div class="item" data-dot="step 1">
									<p>Remove Bravecto from its child-safe packaging.</p>
									<figure><img src="/images/steps/step-1.jpg" class="img-fluid" alt="How to use Bravecto Spot-on for Cats"></figure>
								</div>
								<div class="item" data-dot="step 2">
									<p>Hold the tube straight up and give the cap one twist to break the seal.<p>
									<p><b>PLEASE DO NOT ATTEMPT TO REMOVE THE CAP AS IT DOES NOT COME OFF</b></p>
									<figure><img src="/images/steps/step-2.jpg" class="img-fluid" alt="How to use Bravecto Spot-on for Cats"></figure>
								</div>
								<div class="item" data-dot="step 3">
									<p>Ensure the animal's back is horizontal. <b> Part the hair and apply directly to the skin</b>.</p>
									<figure><img src="/images/steps/step-3.jpg" class="img-fluid" alt="How to use Bravecto Spot-on for Cats"></figure>
								</div>
								<div class="item" data-dot="step 4">
									<p>Apply at the base of the skull, to ensure wet product can't be licked. For cats under 6.25 kg, apply in one spot. For cats over 6.25 kg, apply in 2 spots.</p>
									<figure><img src="/images/steps/step-cat-4.png" class="img-fluid" alt="How to use Bravecto Spot-on for Cats"></figure>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12 col-lg-4 mob-pedding wow fadeIn">
					<div class="video-block top-minouse">
						<a href="https://youtu.be/iz7_X4AO9CQ?autoplay=1&rel=0" data-toggle="lightbox" data-gallery="youtubevideos">
							<img src="/images/videos/spot-on-cats-video.jpg" class="img-fluid" alt="Spot On Cats Video">
						</a>
						<a href="#" class="playbutton" title="Watch Video"></a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="alt-color">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-md-12 col-lg-8 moile-last">
					<div class="benefits benefits-noimg">
						<div class="benefits-list wow fadeInUp">
							<h2>FAQs for Bravecto <span class="plus">Plus</span> for Cats</h2>
							<div class="accordion" id="faqs">

								<div class="faq-list wow fa deInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f00" aria-expanded="true" aria-controls="f00" href="#">
										What will Bravecto protect my pet against?
									</a>
									<div id="f00" class="collapse" data-parent="#faqs">
										<div class="answer">
											Bravecto is available in a range of products that provide long-lasting protection against common parasites in dogs and cats with just one dose*:
											<br />
											<br />
											<br />
										</div>

										<table class="w-100 table table-bordered table-striped table-responsive-md">
											<tr>
												<th>Product</th>
												<th>Fleas</th>
												<th>Paralysis Ticks</th>
												<th>Heartworm</th>
												<th>Gastro-intestinal worms (Roundworm and Hookworm)</th>
												<th>Brown Dog Ticks</th>
												<th>Demodex Mites</th>
												<th>Sarcoptes Mites</th>
												<th>Ear Mites</th>
											</tr>
											<tr class="protect-table-row">
												<th>Bravecto <span class="plus">Plus</span> for Cats</th>
												<td>✓<br />3 months</td>
												<td>✓<br />10 weeks</td>
												<td>✓<br />2 months</td>
												<td>✓<br />Treatment and control</td>
												<td></td>
												<td></td>
												<td></td>
												<td>✓<br />Treatment and control</td>
											</tr>
											<tr class="protect-table-row">
												<th>Bravecto Spot-on for Cats</th>
												<td>✓<br />Up to 3 months</td>
												<td>✓<br />3 months</td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
											</tr>
											<tr class="protect-table-row">
												<th>Bravecto Chew for Dogs</th>
												<td>✓<br />3 months</td>
												<td>✓<br />3 months </td>
												<td></td>
												<td></td>
												<td>✓<br />8 weeks</td>
												<td>✓<br />Treatment and control</td>
												<td>✓<br />Treatment and control</td>
												<td>✓<br />Treatment and control</td>
											</tr>
											<tr class="protect-table-row">
												<th>Bravecto Spot-on for Dogs</th>
												<td>✓<br />6 months</td>
												<td>✓<br />6 months </td>
												<td></td>
												<td></td>
												<td>✓<br />12 weeks</td>
												<td></td>
												<td></td>
												<td></td>
											</tr>
										</table>
									</div>
								</div>

								<div class="faq-list wow fa deInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f0" aria-expanded="true" aria-controls="f0" href="#">
										How do I apply Bravecto <span class="plus">Plus</span> for Cats?
									</a>
									<div id="f0" class="collapse" data-parent="#faqs">
										<div class="answer">
											Bravecto <span class="plus">Plus</span> comes in a clever <strong>TWIST&acute;N&acute;USE</strong> tube with a non-removable cap, it is simple and
											convenient to apply:
											<img src="/images/products/how-to-apply.png" alt="How to apply Bravecto Spot-on Cats" style="max-width: 500px; width: 100%; position: relative;" class="my-3" />

											<div class="faq-how-to-use-desc clearfix">
												<div><strong>Remove Bravecto</strong> from its child-safe packaging.</div>
												<div>
													Hold the tubestraight up, and <strong>give the cap one twist to break the seal</strong>.
													<br /><br />
													<strong>PLEASE DO NOT ATTEMPT TO REMOVE THE CAP AS IT DOES NOT COME OFF.</strong>
												</div>
												<div>Ensure the animal's back is <strong>horizontal</strong>. Part the hair and apply directly to the skin.</div>
											</div>

											Apply contents of the appropriate tube for your cat’s body weight to the application sites.

										</div>
									</div>
								</div>

								<div class="faq-list wow fa deInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f1" aria-expanded="true" aria-controls="f1" href="#">
										What pack sizes does Bravecto <span class="plus">Plus</span> for Cats come in?
									</a>
									<div id="f1" class="collapse" data-parent="#faqs">
										<div class="answer clearfix">
											Bravecto <span class="plus">Plus</span> for Cats is available for cats ranging from 1.2 – 12.5 kg. One dose provides 2 months protection against fleas, paralysis ticks and heartworm and treats roundworm, hookworm and ear mites. For cats above 12.5 kg body weight, use a combination of two pipettes that most closely matches the body weight.
											<img class="my-2" src="/images/products/plus-for-cats/pack-sizes.png" style="max-width: 500px; width:100%; position: relative;" />
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f2" aria-expanded="true" aria-controls="f2" href="#">
										Why is long-lasting protection better than monthly treatments?
									</a>
									<div id="f2" class="collapse " data-parent="#faqs">
										<div class="answer">Gaps in protection can occur when parasite treatments are not administered at the correct treatment interval. Bravecto guarantees compliance for an extended period with just one dose, offering you peace of mind.</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f3" aria-expanded="true" aria-controls="f3" href="#">
										Where can I buy Bravecto?
									</a>
									<div id="f3" class="collapse" data-parent="#faqs">
										<div class="answer">
											Bravecto is available from your veterinarian and leading pet retail outlets. Veterinarians are experts in pet health and can advise on the best and most appropriate treatments for your cat.
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f4" aria-expanded="true" aria-controls="f4" href="#">
										When should cats start treatment with Bravecto <span class="plus">Plus</span> for Cats?
									</a>
									<div id="f4" class="collapse" data-parent="#faqs">
										<div class="answer">
											Bravecto <span class="plus">Plus</span> for Cats can be administered to cats as early as 9 weeks of age. Cats should weigh at least 1.2 kg.
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f5" aria-expanded="true" aria-controls="f5" href="#">
										My cat weighs over 12.5 kgs – how do I treat him/her?
									</a>
									<div id="f5" class="collapse" data-parent="#faqs">
										<div class="answer">
											For cats above 12.5 kg body weight, use a combination of two pipettes that most closely matches the body weight.
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f6" aria-expanded="true" aria-controls="f6" href="#">
										When is it safe for me to pat/cuddle my cat after Bravecto <span class="plus">Plus</span> for Cats application?
									</a>
									<div id="f6" class="collapse" data-parent="#faqs">
										<div class="answer">
											You should wait until the application area is dry before touching the area.
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f7" aria-expanded="true" aria-controls="f7" href="#">
										My cat is on medication for another condition right now. Is it safe to give Bravecto <span class="plus">Plus</span> for Cats?
									</a>
									<div id="f7" class="collapse" data-parent="#faqs">
										<div class="answer">
											It is always best to discuss all of your cat's treatments with your veterinarian as they are your cat's health care expert. Studies with Bravecto <span class="plus">Plus</span> for Cats have shown no interactions with routinely used veterinary medicinal products.
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f8" aria-expanded="true" aria-controls="f8" href="#">
										How does Bravecto <span class="plus">Plus</span> for Cats kill fleas and ticks?
									</a>
									<div id="f8" class="collapse" data-parent="#faqs">
										<div class="answer">
											After you treat your pet with Bravecto <span class="plus">Plus</span> for Cats, the active ingredient fluralaner is absorbed into the bloodstream and quickly reaches tissue fluids just under your pet’s skin. When fleas and ticks feed, they take in fluralaner and die. Bravecto <span class="plus">Plus</span> for Cats kills pre-existing and new infestations of paralysis ticks for 10 weeks. Fleas are killed in up to 12 hours after treatment or after reinfestation during the 3 month treatment period. The flea life cycle can be broken by this rapid onset of action and long-lasting efficacy.
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f9" aria-expanded="true" aria-controls="f9" href="#">
										Will Bravecto <span class="plus">Plus</span> for Cats kill ticks and fleas that are already on my cat?
									</a>
									<div id="f9" class="collapse" data-parent="#faqs">
										<div class="answer">Yes.</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f10" aria-expanded="true" aria-controls="f10" href="#">
										Can dead and attached ticks be found on treated cats?
									</a>
									<div id="f10" class="collapse" data-parent="#faqs">
										<div class="answer">
											Yes. Due to the way Bravecto works, dead attached ticks can be still seen on cats following treatment. Dead ticks can be easily removed as opposed to live ticks which take some force to dislodge from the skin of the cat.
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f11" aria-expanded="true" aria-controls="f11" href="#">
										How can my cat get heartworm?
									</a>
									<div id="f11" class="collapse" data-parent="#faqs">
										<div class="answer">
											Heartworm larvae can be transmitted by feeding mosquitoes through to your cat’s bloodstream and eventually migrate to the cat’s heart or blood vessels of the lungs. If clinical signs occur these are often due disease of the lungs approximately 3 to 4 months post infection. Treatment is risky and can be fatal, therefore prevention is the easiest solution to the problem.
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f12" aria-expanded="true" aria-controls="f12" href="#">
										Can Bravecto <span class="plus">Plus</span> for Cats prevent heartworm?
									</a>
									<div id="f12" class="collapse" data-parent="#faqs">
										<div class="answer">
											Yes, Bravecto <span class="plus">Plus</span> for Cats provides continuous prevention against heartworm disease if the spot-on is applied every 2 months.
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f13" aria-expanded="true" aria-controls="f13" href="#">
										What are roundworms and hookworms and how can they affect my cat?
									</a>
									<div id="f13" class="collapse" data-parent="#faqs">
										<div class="answer">
											<p>Roundworm is a common internal parasite that lives in the gastrointestinal tract of cats and kittens. Roundworm eggs can be picked up by eating infected rodents or from the kittens' mother. The most common affects are vomiting, diarrohea, weight loss and a pot-bellied appearance.</p>

											<p>Hookworm is another worm that lives in the small intestine of cats.  Cats can become infected by ingesting the larvae in the environment (passed in the faeces of an infected animal), by eating infected rodents, or by larval penetration of the skin. Infection is more common in kittens.</p>

											<p>Most infected cats show no signs. Anaemia occasionally occurs and is the result of bloodsucking by the worms in the small intestine. Faeces may become loose and have a tarry consistency. Loss of appetite and weight loss  may also occur with long-term infections.</p>

											<p>Bravecto <span class="plus">Plus</span> for Cats treats cats for roundworm and hookworms.</p>
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f14" aria-expanded="true" aria-controls="f14" href="#">
										What are ear mites and how can they affect my cat?
									</a>
									<div id="f14" class="collapse" data-parent="#faqs">
										<div class="answer">
											Ear mites infest the external ear, causing inflammation of the ear canal. It is commonly seen in cats. The infested cat may shake its head and scratch its ear(s). The external ear may be inflamed and the ear drum can tear. Bravecto <span class="plus">Plus</span> for Cats treats cats for eat mites. All cats that have contact with infested cats should also be treated.
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f15" aria-expanded="true" aria-controls="f15" href="#">
										Can I use Bravecto <span class="plus">Plus</span> for Cats all year-round?
									</a>
									<div id="f15" class="collapse" data-parent="#faqs">
										<div class="answer">
											Yes, it is safe to use Bravecto <span class="plus">Plus</span> for Catss all year-round.  Applying Bravecto <span class="plus">Plus</span> for Cats every 2 months ensures your cat will be protected from fleas, paralysis ticks and heartworm  throughout the year with just 6 doses.  One dose  also treats roundworm, hookworm and ear mites.
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f16" aria-expanded="true" aria-controls="f16" href="#">
										Do I really need to treat for fleas all year round?
									</a>
									<div id="f16" class="collapse" data-parent="#faqs">
										<div class="answer">
											There are very few places in the Australia where there are no fleas. In warmer climates, fleas are present all year round while in more temperate climates fleas are present mainly from late spring through to late autumn, and even longer thanks to central heating. Bravecto <span class="plus">Plus</span> for Cats should be administered every 2 months, all year round to avoid giving flea populations a chance to build up in your home.
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f17" aria-expanded="true" aria-controls="f17" href="#">
										Is it necessary to use additional insecticides to control flea stages in the environment?
									</a>
									<div id="f17" class="collapse" data-parent="#faqs">
										<div class="answer">
											No. Bravecto <span class="plus">Plus</span> for Cats kills fleas and prevents new flea infestations for 2 months. As the majority of a flea population developing in the environment will emerge as adult fleas over this time, Bravecto will effectively control the entire flea population.
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f18" aria-expanded="true" aria-controls="f18" href="#">
										Can Bravecto <span class="plus">Plus</span> for Cats be used in breeding/pregnant/lactating cats?
									</a>
									<div id="f18" class="collapse" data-parent="#faqs">
										<div class="answer">
											The safety of Bravecto <span class="plus">Plus</span> for Cats has not been established during breeding, pregnancy or lactation.
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f19" aria-expanded="true" aria-controls="f19" href="#">
										Why does my cat scratch even more on the first day of Bravecto <span class="plus">Plus</span> for Cats treatment?
									</a>
									<div id="f19" class="collapse" data-parent="#faqs">
										<div class="answer">
											When fleas are in the process of dying their movements become uncoordinated. This may cause a skin sensation which can result in increased scratching of the cat. However, this phenomenon is quickly resolved once the fleas are dead.
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f20" aria-expanded="true" aria-controls="f20" href="#">
										Why do I see fleas after I have administered Bravecto <span class="plus">Plus</span> for Cats?
									</a>
									<div id="f20" class="collapse" data-parent="#faqs">
										<div class="answer">
											Fleas can continually re-infest treated cats – either from juvenile flea life stages that have just matured to adults in the household or from fleas that jump onto the pet when outside or visiting other homes. Bravecto will quickly kill these fleas. Also, as fleas die, they can move from the skin surface where they feed, to the tips of the hairs in the coat, making them more visible to the human eye.
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-12 col-lg-4 mobile-first">
					<div class="where-to-get top-minouse wow fadeInUp">
						<h2>Where can I get Bravecto?</h2>
						<p>You’ll find Bravecto at leading veterinarians and pet shops where pet health experts can advise on the best and most appropriate treatments for your dog or cat. Find your local stockist by entering your suburb below.</p>
						<form action="stockists">
							<input name="zipcode" type="text" placeholder="suburb">
							<button class="arror-btn" type="submit"></button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
</body>
</html>
		
