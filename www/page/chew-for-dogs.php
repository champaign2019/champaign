<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Common.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Hospital.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Clinic.class.php";

include "config.php";

$_REQUEST['orderby'] = $orderby;
$_REQUEST['scategory_fk'] = 67;
$objCommon = new Common($pageRows, $tablename, $_REQUEST);
$rowPageCount = $objCommon->getCount($_REQUEST);
$result = $objCommon->getList($_REQUEST);

$category_result = $category_tablename ? $objCommon->getCategoryList($_REQUEST) : null;
?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/logingCheck.php" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/themeHtml.php" ?>
</head>
<body>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
	<div>
		<div id="sync1" class="owl-carousel owl-theme big-product-carousel">
			<div class="item">
				<div class=""><img src="/images/products/chew-for-dogs/header-1.jpg" class="w-100" alt="Bravecto Chew for Dogs"></div>
			</div>
			<div class="item">
				<div class=""><img src="/images/products/chew-for-dogs/header-2.jpg" class="w-100" alt="Bravecto Chew for Dogs"></div>
			</div>
			<div class="item">
				<div class=""><img src="/images/products/chew-for-dogs/header-3.jpg" class="w-100" alt="Bravecto Chew for Dogs"></div>
			</div>
			<div class="item">
				<div class=""><img src="/images/products/chew-for-dogs/header-4.jpg" class="w-100" alt="Bravecto Chew for Dogs"></div>
			</div>
		</div>
	</div>

	<div class="alt-color">
		<div class="container-1">
			<div id="sync2" class="owl-carousel owl-theme dogs">
				<div class="item yellow-active"><span><i></i>2 - 4.5 kg</span></div>
				<div class="item orange-active"><span><i></i>4.5 - 10 kg</span></div>
				<div class="item green-active"><span><i></i>10 - 20 kg</span></div>
				<div class="item blue-active"><span><i></i>20 - 40 kg</span></div>
			</div>
		</div>
	</div>

	<section class="product-section">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-md-12 col-lg-10">
					<div class="product-details-top">
						<h1 class="wow fadeInUp">강아지용 브라벡토 츄어블</h1>
						<h4 class="wow fadeInUp">맛있는 츄어블로 당신의 반려견을 보호하세요</h4>
						<p class="wow fadeInUp">우리 반려견들의 애교를 부르는 간식. 그래서 저희는 간식처럼 맛있게 먹일 수 있는 외부기생충 예방약을 만들었습니다.</p>
						<ul class="wow fadeInUp">
							<li>진드기(tick and mite) 및 벼룩으로 부터 12주간 보호합니다</li>
							<li>특허성분인 플루랄라너의 작용은 귀진드기¹, 옴진드기², 모낭충³에도 효과를 발휘합니다.</li>
							<li>8주령 이상 및 체중 2kg 이상의 강아지 뿐만 아니라 임신 및 수유중인 강아지에도 안심하고 사용할 수 있습니다</li>
						</ul>
					</div>
				</div>
				<div class="col-md-12 col-lg-8 wow fadeInUp">
					<div class="product-details-top pt-0 pb-180">
						<ul class="nav nav-tabs" id="proTab" role="tablist">
							<li class="nav-item"> <a class="nav-link active" id="longlastng-tab" data-toggle="tab" href="#loglasting" role="tab" aria-selected="true" title="Long Lating">편리한</a> </li>
							<li class="nav-item"> <a class="nav-link" id="effective-tab" data-toggle="tab" href="#effective" role="tab" aria-selected="false" title="Efective">빠르고 오래가는</a> </li>
							<li class="nav-item"> <a class="nav-link" id="hasslefree-tab" data-toggle="tab" href="#hasslefree" role="tab" aria-selected="false" title="Easy">안전한</a> </li>
							<li class="nav-item"> <a class="nav-link" id="global-tab" data-toggle="tab" href="#global" role="tab" aria-selected="false" title="Global">세계적인</a> </li>
						</ul>
						<div class="tab-content" id="proTabContent">
							<div class="tab-pane fade show active" id="loglasting" role="tabpanel">
								<h4>편리한</h4>
								<p>브라벡토는 1회 사용만으로 12주간 외부기생충으로부터 반려견을 보호합니다. 바쁜 일상 속 1년에 딱 4번 투여로 기억하기 쉽고, 예방 타이밍을 놓칠 일이 적어집니다.</p>
							</div>
							<div class="tab-pane fade" id="effective" role="tabpanel" aria-labelledby="effective-tab">
								<h4>빠르고 오래가는</h4>
								<p>브라벡토의 특허성분 플루랄라너는 투여 후 4시간 이내에 작용하기 시작하여, 12시간 이내 흡혈 진드기가 100% 사멸합니다⁴. 또한 플루랄라너의 대부분이 비활성 상태로 저장되어 있다가 12주간 서서히 방출⁵되기 때문에 더욱 오랜 기간 예방이 가능합니다.</p>
							</div>
							<div class="tab-pane fade" id="hasslefree" role="tabpanel" aria-labelledby="hasslefree-tab">
								<h4>안전한</h4>
								<p>브라벡토는 8주령이 갓 지났거나 체중 2kg가 조금 넘는 작은 체구의 강아지, 임신중 혹은 수유중인 반려견에서 안심하고 사용할 수 있습니다. 또한 많은 종류의 의약품 사용이 불가능한 콜리종에서의 안전성⁶도 검증되었습니다.</p>
							</div>
							<div class="tab-pane fade" id="global" role="tabpanel" aria-labelledby="global-tab">
								<h4>세계적인</h4>
								<p>2014년 미국 FDA를 시작으로, 전 세계 85여개 국가에서 허가를 받은 브라벡토의 특허성분 플루랄라너는 12주나 되는 기간동안 그 효능의 대부분을 유지⁵하여 반려견에게 문제가 되는 외부기생충을 더욱 간편하고 안전하게 예방할 수 있게 되었습니다.</p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12 col-lg-4 mob-pedding wow fadeIn">
					<div class="video-block wow fadeInUp img_width"> <img src="/images/products/chew-for-dogs/section-1.jpg" alt="little girl feeding a cat" class="w-100"></div>
				</div>
			</div>
		</div>
	</section>

	<section class="alt-color chew_con">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-md-12 col-lg-8">
					<div class="benefits benefits-noimg">
						<div class="benefits-list wow fadeInUp"> <a href="/page/fluralaner.php" class="redbg-link">특허성분</a> </div>
						<div class="benefits-list extrta-mb">
							<h3 class="wow fadeInUp">브라벡토만의 혁신적인 성분, 플루랄라너</h3>
							<p class="wow fadeInUp">2014년 미국 FDA를 시작으로, 전 세계 85여개 국가에서 허가를 받은 브라벡토의 성분 플루랄라너는 
							혈액에 흡수됨과 동시에 대부분이 비활성화 상태로 저장되어 서서히 방출됩니다.
							12주나 되는 기간동안 그 효능의 대부분을 유지하여 반려견에게 문제가 되는 외부기생충을 
							더욱 간편하고 안전하게 예방할 수 있게 되었죠. </p>
						</div>
						<div class="benefits-list extrta-mb ">
							<h3 class="wow fadeInUp">방대한 임상 연구와 모니터링</h3>
							<p class="wow fadeInUp">브라벡토(플루랄라너)는 저희 MSD동물약품의 비전 ‘The Science of Healthier Animals’ 의 혁신적인 결과물입니다. 방대한 임상 연구와 모니터링을 바탕으로 반려동물들이 브라벡토(플루랄라너)의 강력한 효능과 지속성 및 높은 안전성을 경험할 수 있게 되었습니다.</p>
						</div>
					</div>
				</div>
				<div class="col-md-12 col-lg-4 mob-pedding wow fadeIn img_width">
					<div class="video-block top-minouse img_width">
						<a href="https://youtu.be/cW-v3q-BGh4" data-toggle="lightbox" data-gallery="youtubevideos">
							<img src="/images/sum01.jpg" class="img-fluid" alt="Chew For Dogs Video">
						</a>
						<a href="#" class="playbutton" title="Watch Video"></a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="col-3-section custom_section">
		<div class="container-1">
			<div class="alternet-list">
				<div class="row no-gutters">
					<div class="col-md-12 col-lg-6 moile-last">
						<div class="text-block alternet-text-block">
							<h2 class="anim full-visible anims"><span class="titleanimation-bottom">브라벡토 츄어블 투여 방법</span></h2>
							<p>반려견들에게 약을 먹이는 것이 쉽지 만은 않습니다. 
							하지만 브라벡토 츄어블은 손쉽게 급여가 가능하죠.브라벡토 포장을 뜯어 간식처럼 급여를 하시거나, 밥을 먹을 때 사료에 섞어 주세요.
							강아지용 브라벡토 츄어블은 반려견들이 좋아하는 돼지간 맛일 뿐만 아니라 가수분해된 원료를 사용하여 피부알러지 걱정도 없습니다.
							강아지가 먹은 후에는 칭찬을 잊지 마세요!</p>
						</div>
					</div>
					<div class="col-md-12 col-lg-6 mobile-first"><img src="/images/chew_info_img01.jpg" class="img-fluid" alt="브라벡토 츄어블 투여 방법"></div>
				</div>
			</div>
		</div>
	</section>

	<section class="alt-color chew_con">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-md-12 col-lg-8 moile-last">
					<div class="benefits benefits-noimg">
						<div class="benefits-list wow fadeInUp">
							<h2 style="cursor:pointer; padding-top:80px; padding-bottom:80px;text-align:center; " onclick="location.href='/faq/index.php'">더 궁금하신 점이 있으신가요? <button class="arror-btn type02" type="submit"></button></h2>
							<!--
							<div class="accordion" id="faqs">
								<?
									$i = 0;
									if($rowPageCount[0] > 0){
									while ($row=mysql_fetch_assoc($result)) {
								?>
								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f<?=$row['no']?>" aria-expanded="true" href="#"> <?=$row['title']?> </a>
									<div id="f<?=$row['no']?>" class="collapse" data-parent="#faqs">
										<div class="answer"><?=$row['contents']?></div>
									</div>
								</div>
								<?
										}
									}else{
								?>

								<div class="faq-list wow fadeInUp">
									<a data-toggle="collapse" data-target="#f1" aria-expanded="true" href="#"> 등록된 글이 없습니다. </a>
								</div>
								<?
									}
								?>


							</div>
							-->
						</div>
					</div>
				</div>
				<div class="col-md-12 col-lg-4 mobile-first">
					<div class="where-to-get top-minouse wow fadeInUp">
						<h2>브라벡토 구매를 원하시나요? </h2>
						<p>당신의 반려견에게 가장 알맞은 제품에 대해 수의사와 상담하세요. 
						구입을 원하신다면 거주지역을 아래 검색창에 입력하세요.</p>
						<form action="/page/stockists.php">
							<input name="zipcode" type="text" placeholder="시/군/구 검색">
							<button class="arror-btn" type="submit"></button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>

	<div class="footer_top_txt_box">
		<div class="container-1">
			<p>
				1. Taenzler J, de Vos C, Roepke RK, Frénais R, Heckeroth AR. Efficacy of fluralaner against Otodectes cynotis infestations in dogs and cats. Parasit Vectors. 2017;10:30. doi: 10.1186/s13071-016-1954-y.<br/>
				2. Taenzler J, Liebenberg J, Roepke RK, Frénais R, Heckeroth AR. Efficacy of fluralaner administered either orally or topically for the treatment of naturally acquired Sarcoptes scabiei var. canis infestation in dogs. Parasit Vectors. 2016;9:392. doi: 10.1186/s13071-016-1670-7.<br/>
				3. Djuric, Milos & Matic, Natalija & Davitkov, Darko & Glavinic, Uros & Davitkov, Dajana & Vejnović, Branislav & Stanimirovic, Zoran. (2019). Efficacy of oral fluralaner for the treatment of canine generalized demodicosis: a molecular-level confirmation. Parasites & Vectors. 12. 10.1186/s13071-019-3521-9.<br/>
				4. Wengenmayer, Christina & Williams, Heike & Zschiesche, Eva & Moritz, Andreas & Langenstein, Judith & Roepke, Rainer & Heckeroth, Anja. (2014). The speed of kill of fluralaner (Bravecto (TM)) against Ixodes ricinus ticks on dogs. Parasites & Vectors. 7. 10.1186/s13071-014-0525-3.<br/>
				5. Kilp, Susanne & Ramirez, Diana & Allan, Mark & Roepke, Rainer & Nuernberger, Martin. (2014). Pharmacokinetics of fluralaner in dogs following a single oral or intravenous administration. Parasites & vectors. 
				7. 85. 10.1186/1756-3305-7-85.<br/>
				6. Walther, Feli & Paul, Allan & Allan, Mark & Roepke, Rainer & Nuernberger, Martin. (2014). Safety of fluralaner, a novel systemic antiparasitic drug, in MDR1(-/-) Collies after oral administration. Parasites & vectors. 7. 86. 10.1186/1756-3305-7-86.

			</p>
		</div>
	</div>

	<? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
</body>
</html>
		
