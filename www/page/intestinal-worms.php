<?session_start();?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/logingCheck.php" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/themeHtml.php" ?>
</head>
<body>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
	<section class="inner-header">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-12  col-lg-6">
					<div class="header-text-inner">
						<h1 class="anim">
							<span class="titleanimation-bottom">Inside story on intestinal worms</span>
						</h1>
						<p class="wow fadeInUp">
							Roundworm and hookworm are easily contracted and transferred, showing few symptoms while causing a range of health issues.
						</p>
					</div>
				</div>
				<div class="col-12  col-lg-6 ">
					<figure class="d-flex"><img src="/images/parasites/intestinal-worms-header.png" alt="Inside Story on intestinal worms" class=" img-fluid wow fadeInUp"></figure>
				</div>
			</div>
		</div>
	</section>

	<section class="facts-ticks">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-12 col-lg-6">
					<div class="facts-list facts-light">
						<h4 class="anim">
							<span class="titleanimation-bottom">
								The facts about roundworm and hookworm
							</span>
						</h4>
						<ul>
							<li>
								<h6 class="anim">
									<span class="titleanimation-bottom">
										They're easier to pick up than you think
									</span>
								</h6>
								<p class="wow fadeInUp">
									Transmission can be as simple as your cat grooming their feet, sniffing infected ground or sniffing the faeces of other infected animals, alongside interaction with infected animals and rodents. Once infected, a mother cat will often pass these worms through the milk.
								</p>
							</li>
							<li>
								<h6 class="anim">
									<span class="titleanimation-bottom">
										Roundworm and hookworm are difficult to spot
									</span>
								</h6>
								<p class="wow fadeInUp">
									Once these intestinal parasites set up home in your cat, they tend to be quiet house guests. Without detection, they can stunt growth in kittens, cause anaemia, diarrhoea, vomiting and more extreme health issues like pneumonia.
								</p>
							</li>
							<li>
								<h6 class="anim">
									<span class="titleanimation-bottom">
										They can be transmitted to humans
									</span>
								</h6>
								<p class="wow fadeInUp">
									Roundworm and hookworm eggs can be transmitted to humans through interaction with infected cat and their environment.
								</p>
							</li>
						</ul>
					</div>
				</div>

				<div class="col-12 col-lg-6">
					<div class="facts-list extrta-pad">
						<h4 class="anim">
							<span class="titleanimation-bottom">
								Tips for preventing intestinal worms
							</span>
						</h4>
						<ul>
							<li>
								<h6 class="anim">
									<span class="titleanimation-bottom">
										Dispose of cat faeces appropriately
									</span>
								</h6>
								<p class="wow fadeInUp">
									It's extremely important to dispose of cat faeces appropriately, from litter trays, yards and playgrounds. Once contaminated, an environment can sustain eggs for long periods.
								</p>
							</li>
							<li>
								<h6 class="anim">
									<span class="titleanimation-bottom">
										Use Bravecto <span class="plus">Plus</span> for Cats
									</span>
								</h6>
								<p class="wow fadeInUp">
									Bravecto <span class="plus">Plus</span> for Cats provides long-lasting protection against heartworm, fleas and paralysis ticks as well as treatment of roundworm, hookworm and ear mites.
								</p>
							</li>
							<li>
								<h6 class="anim">
									<span class="titleanimation-bottom">
										Maintain strict personal hygiene in adults and children
									</span>
								</h6>
								<p class="wow fadeInUp">
									Even if faeces are not visible, microscopic eggs can still be present. Avoid directly contacting potentially contaminated environments, and ensure all family members wash their hands thoroughly after playing outdoors or with pets.
								</p>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="alt-color identify">
		<div class="row no-gutters">
			<div class="col-12 col-md-6">
				<div class="identify-list-right-border d-none d-md-block"></div>
				<div class="identify-list">
					<span class="red-lable wow fadeInUp">ROUNDWORMS</span>
					<h5 class="wow fadeInUp"><em>Toxascaris leonina</em> and <em>Toxocara cati</em></h5>
					<img src="/images/parasites/intestinal-worms/roundworms.png" class="img-fluid wow fadeInUp" alt="roundworms">
					<p class="wow fadeInUp">
						These unwelcome guests are the most common intestinal parasites to take up residence inside our furry friends. It's believed almost all cats will be infected with them at some point, often as kittens, and because they're easy to catch in many different ways, they're harder to control without a strong defence. Ungracious as they are, these little squirmers can grow up to 12.5 cms long as adults, staying alive by stealing food from their host's intestines.
					</p>
				</div>
			</div>
			<div class="col-12 col-md-6">
				<div class="identify-list">
					<span class="red-lable wow fadeInUp">HOOKWORMS</span>
					<h5 class="wow fadeInUp"><em>Ancylostoma tubaeforme</em> and <em>Uncinaria stenocephala</em></h5>
					<img src="/images/parasites/intestinal-worms/hookworms.png" class="img-fluid wow fadeInUp" alt="hookworms">
					<p class="wow fadeInUp">
						These thread-like worms are excellent at hide and seek, with their small size often making them hard to spot in animal waste. Known to live as long as cats do, the slender, slithery parasites attach to the lining of intestine walls where they feast on their host's blood, which doesn't just sound unpleasant - it also poses a real threat to the health and happiness of your cat, especially in kittens. Once they've made a home in a new host, they're ready to spread to other cats within 2-4 weeks.
					</p>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-md-12 col-lg-8 wow fadeInUp">
					<div class="benefits mb-more">
						<div class="benefits-list mb-0  wow fadeInUp">
							<h3 class="bigheading anim"><span class="titleanimation-bottom">Life cycle of a gastrointestinal worm<sup>+</sup></span></h3>
							<p class="wow fadeInUp">Click through all stages of a grastro-intestinal worm life cycle to learn more.</p>
							<div class="steps-car owl-carousel owl-theme wow fadeInUp">
								<div class="item" data-dot="1. EGGS">
									<p>
										A female worm deposits eggs into the intestines of a host, which are then carried through the faeces of an infected animal or, in the case of roundworm they may also be transmitted from mum to kitten through the milk.
									</p>
									<p>
										<small><sup>+</sup>Applies to roundworm and hookworm</small>
									</p>
								</div>
								<div class="item" data-dot="2. LARVAE">
									<p>Within a fertile environment, eggs develop into infective larvae.</p>
									<p>
										<small><sup>+</sup>Applies to roundworm and hookworm</small>
									</p>
								</div>
								<div class="item" data-dot="3. INFECTION">
									<h5>Hookworm</h5>
									<p>
										The cat swallows larvae, often by grooming its feet, and picks up the  hookworm infection. The larvae may also burrow through the cat's skin and migrate to the intestine, where they mature.
									</p>
									<h5>Roundworm</h5>
									<p>
										Cats become infected with roundworm when they ingest infective eggs from the environment, or infective roundworm larvae transfer to intermediate hosts, such as small rodents, which may then be eaten by cats. Roundworms can also be transmitted to kittens via their mother's milk.
									</p>
									<p>
										<small><sup>+</sup>Applies to roundworm and hookworm</small>
									</p>
								</div>
								<div class="item" data-dot="4. MATURATION">
									<p>
										Once infective larvae transfer to a suitable host, such as a pet cat, they mature and continue the cycle.
									</p>
									<p>
										<small><sup>+</sup>Applies to roundworm and hookworm</small>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12 col-lg-4 mob-pedding wow fadeIn">
					<div class="video-block">
						<div class="step-images-slider video-slider owl-carousel owl-theme ">
							<div class="video-item"><img src="/images/parasites/intestinal-worms/steps/step-1.svg" class="w-100" alt=Iintestinal Worms Step 1"> </div>
							<div class="video-item"><img src="/images/parasites/intestinal-worms/steps/step-2.svg" class="w-100" alt="Intestinal Worms Step 2"> </div>
							<div class="video-item"><img src="/images/parasites/intestinal-worms/steps/step-3.svg" class="w-100" alt="Intestinal Worms Step 3"> </div>
							<div class="video-item"><img src="/images/parasites/intestinal-worms/steps/step-4.svg" class="w-100" alt="Intestinal Worms Step 4"> </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="alt-color">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-md-12 col-lg-8">
					<div class="benefits benefits-noimg">
						<div class="extrta-mb">
							<h4 class="anim"><span class="titleanimation-bottom">The risks of leaving your cat untreated</span></h4>
							<p>
								Often playing the role of quiet house guests within a host, intestinal worms can be hard to spot. Keep a watchful eye out for tell-tale signs, and contact your local vet at the first sign of any symptoms .
							</p>

							<ul>
								<li>Your cat isn't gaining weight, suffers from weightloss or diarrhoea</li>
								<li>Their coat is dull or in poor condiiton</li>
								<li>They appear pot-bellied, or have decreased appetite</li>
								<li>They suffer from vomiting or lethargy</li>
								<li>They have a cough</li>
							</ul>

							<p>
								To ensure the ongoing health and happiness of your cat, the simplest and easiest way to treat against intestinal worms is a regular routine of Bravecto <span class="plus">Plus</span> for Cats.
							</p>
						</div>
					</div>
				</div>

				<div class="col-md-12 col-lg-4 mob-pedding wow fadeIn">
					<div class="video-block top-minouse">
						<img src="/images/parasites/intestinal-worms/intestinal-worms-bottom.png" class="img-fluid" alt="Intestinal Worms Cat Eyes ">
					</div>
				</div>
			</div>
		</div>
	</section>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
</body>
</html>
		
