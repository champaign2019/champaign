<?session_start();?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/logingCheck.php" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/themeHtml.php" ?>
</head>
<body>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
	<section class="inner-header">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-12  col-lg-4 m_ver">
					<figure class="d-flex"><img src="/images/parasites/mitemite_con01_img01.png" alt="Meet the bad guys" class=" img-fluid wow fadeInUp"></figure>
				</div>
				<div class="col-12  col-lg-8">
					<div class="header-text-inner">
						<h1 class="anim color_green type02" ><span class="titleanimation-bottom"><span class="mobile_small">피부 트러블메이커,</span><br/> 응애류 진드기</span></h1>
						<p class="wow fadeInUp">외부에서 흡혈하는 참진드기와 다르게 귀진드기, 옴진드기, 모낭충 등 응애류 진드기는 반려동물의 피부속에서 문제를 일으킵니다. 보통 크기가 매우 작아 눈으로는 관찰할 수 없으며, 진단 및 치료가 쉽지 않습니다.</p>
					</div>
				</div>
				<div class="col-12  col-lg-4 pc_ver ">
					<figure class="d-flex"><img src="/images/parasites/mitemite_con01_img01.png" alt="Meet the bad guys" class=" img-fluid wow fadeInUp"></figure>
				</div>
			</div>
		</div>
	</section>

	<section class="mitemite_con02">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-md-12 col-lg-12 wow fadeInUp">
					<div class="benefits benefits-noimg">
						<div class="benefits-list mb-0  wow fadeInUp">
							<h3 class="bigheading">응애류 진드기의 정체</h3>
							<div class=" steps-car owl-carousel owl-theme">
								<div class="item" data-dot="옴진드기">
									<div class="txt_box">
										<h4>옴진드기(<em>Sarcoptes scabiei</em>)</h4>
										<p>옴진드기는 전 세계에서 발견되는 감염성이 높은 진드기 입니다. 맨눈에는 보이지 않을 정도로 작은 이 진드기는<br/>
										사람 및 감염된 다른 동물과의 직,간접적인 접촉을 통해 전염되며, 진드기는 피부층을 파고들어가
										알을 낳고<br/> 번식을 합니다. 감염된 개는 극심한 가려움증으로 삶의 질이 현저히 저하됩니다.</p>
									</div>
									<figure><img src="/images/steps/step-1.png" class="img-fluid" alt="How to use Bravecto Spot-on for Cats"></figure>
								</div>
								<div class="item" data-dot="모낭충">
									<div class="txt_box">
										<h4>모낭충(<em>Demodex</em> Spp.)</h4>
										<p>건강한 반려동물의 피부에서도 정상적으로 발견되는 모낭충은, 면역력저하 등 다양한 원인에 의해 비정상적으로 숫자가 늘어나면 피부건강에 문제를 일으킵니다. 매우 작은 모낭충은 반려동물의 모낭 및 땀샘에 서식하며, 정확한 원인은 아직 밝혀지지 않았으나, 다른 동물로의 전염은 일어나지 않습니다. 매우 극심한 가려움증으로 삶의 질이 현저히 저하됩니다.</p>
									</div>	
									<figure><img src="/images/steps/step-2.png" class="img-fluid" alt="How to use Bravecto Spot-on for Cats"></figure>
								</div>
								<div class="item" data-dot="귀진드기">
									<div class="txt_box">
										<h4>귀진드기(<em>Otodectes cynotis</em>)</h4>
										<p>귀진드기는 개와 고양이의 귀건강에 문제를 일으키는 진드기 입니다. 매우 작아 맨눈으로는 보이지 않는 귀진드기는 주로 외이도에서 가려움증을 유발하지만 심한 경우 고막을 뚫고 중이까지 감염되기도 합니다. 감염된 동물은 머리를 심하게 흔들거나, 과도하게 긁어 상처를 통해 이차감염이 일어날 수 있습니다.</p>
									</div>
									<figure><img src="/images/steps/step-3.png" class="img-fluid" alt="How to use Bravecto Spot-on for Cats"></figure>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="alt-color resources-papers">
		<div class="container">
			<div class="paper-title">
				<h1 class="anim"><span class="titleanimation-bottom">응애류 진드기에 대한 임상연구</span></h1>
				<p class="wow fadeInUp">브라벡토의 특허성분 플루랄라너로 응애류 진드기로 인한 다양한 질병을 예방 및 치료할 수 있습니다. 아래 학술정보를 확인하여 보호자에게 더욱 정확한 정보를 제공하세요!</p>
				<br><br>
			</div>

			<div class="row">
				<div class="col-12 col-md-6 wow fadeInUp">
					<a href="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5240227" class="paper-link" target="_blank">Taenzler, Janina & Vos, Christa & Roepke, Rainer & Frenais, Régis & Heckeroth, Anja. (2017). Efficacy of fluralaner against Otodectes cynotis infestations in dogs and cats. Parasites & Vectors. 10. 10.1186/s13071-016-1954-y.</a>
				</div>
				<div class="col-12 col-md-6 wow fadeInUp">
					<a href="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6335790" class="paper-link" target="_blank">Bosco, Antonio & Leone, Federico & Vascone, Rosachiara & Pennacchio, Saverio & Ciuca, Lavinia & Cringoli, Giuseppe & Rinaldi, Laura. (2019). Efficacy of fluralaner spot-on solution for the treatment of Ctenocephalides felis and Otodectes cynotis mixed infestation in naturally infested cats. BMC Veterinary Research. 15. 10.1186/s12917-019-1775-2.</a>
				</div>
				<div class="col-12 col-md-6 wow fadeInUp">
					<a href="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4937584" class="paper-link" target="_blank">Taenzler, Janina & Liebenberg, Julian & Roepke, Rainer & Frenais, Régis & Heckeroth, Anja. (2016). Efficacy of fluralaner administered either orally or topically for the treatment of naturally acquired Sarcoptes scabiei var. canis infestation in dogs. Parasites & Vectors. 9. 10.1186/s13071-016-1670-7.</a>
				</div>
				<div class="col-12 col-md-6 wow fadeInUp">
					<a href="https://vetrecordcasereports.bmj.com/content/7/1/e000772" class="paper-link" target="_blank">Curtis, Cathy & Bourdeau, Patrick & Barr, Philip & Mukherjee, Rajat. (2019). Use of the novel ectoparasiticide fluralaner in the treatment of feline sarcoptic mange. Veterinary Record Case Reports. 7. e000772. 10.1136/vetreccr-2018-000772.</a>
				</div>
				<div class="col-12 col-md-6 wow fadeInUp">
					<a href="https://www.researchgate.net/publication/338463268_Efficacy_of_fluralaner_against_canine_generalized_demodicosis" class="paper-link" target="_blank">Lôres Lopes, Natália & Carvalho, Flávia & Berman, Renata & Machado, Marília & Medeiros, Carolina & Fernandes, Julio. (2019). Efficacy of fluralaner against canine generalized demodicosis. Brazilian Journal of Veterinary Medicine. 41. 10.29374/2527-2179.bjvm101719.</a>
				</div>
				<div class="col-12 col-md-6 wow fadeInUp">
					<a href="https://www.ncbi.nlm.nih.gov/pubmed/28466558" target="_blank" class="paper-link">Matricoti, I & Maina, Enock. (2017). The use of oral fluralaner for the treatment of feline generalised demodicosis: A case report. The Journal of small animal practice. 58. 10.1111/jsap.12682.</a>
				</div>
			</div>

		</div>
	</section>

	<? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
</body>
</html>
		
