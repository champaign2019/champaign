<?session_start();?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/logingCheck.php" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/themeHtml.php" ?>
</head>
<body>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
	<section class="inner-header">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-12  col-lg-6">
					<div class="header-text-inner">
						<h1 class="anim"><span class="titleanimation-bottom">Meet the bad guys</span></h1>
						<p class="wow fadeInUp">Learn more about the bad guys, and how best to protect your companions from their devastating effects.</p>
					</div>
				</div>
				<div class="col-12  col-lg-6 ">
					<figure class="d-flex"><img src="/images/parasites/dog-meet-the-bad-guys.png" alt="Meet the bad guys" class=" img-fluid wow fadeInUp"></figure>
				</div>
			</div>
		</div>
	</section>
	
	<section class="col-3-section">
		<div class="container-1">

			<div class="alternet-list alternet-list-last">
				<div class="row no-gutters">
					<div class="col-md-12 col-lg-6"><img src="/images/parasites/parasites_img.jpg" class="w-100" alt="Dirt on Ticks"></div>
					<div class="col-md-12 col-lg-6 align-self-end">
						<div class="text-block alternet-text-block alternet-text-block-bottom">
							<h2 class="anim full-visible"><span class="titleanimation-bottom">보일 듯 말 듯, 응애류 진드기</span></h2>
							<p class="wow fadeInUp">
								외부에서 흡혈하는 참진드기와 다르게 귀진드기, 옴진드기, 모낭충 등 응애류 진드기는 반려동물의 피부속에서 문제를 일으킵니다. 보통 크기가 매우 작아 눈으로는 관찰할 수 없으며, 진단 및 치료가 쉽지 않습니다.
							</p>
						</div>
					</div>
				</div>
			</div>

		</div>
		
		<div class="container-1">
			<div class="alternet-list alternet-list-last">
				<div class="row no-gutters">
					<div class="col-md-12 col-lg-6 align-self-end">
						<div class="text-block alternet-text-block alternet-text-block-bottom">
							<h2 class="anim full-visible"><span class="titleanimation-bottom">응애류 진드기의 정체</span></h2>
							<p class="wow fadeInUp">
								옴진드기는 전 세계에서 발견되는 감염성이 높은 진드기 입니다. 맨눈에는 보이지 않을 정도로 작은 이 진드기는
									사람 및 감염된 다른 동물과의 직,간접적인 접촉을 통해 전염되며, 진드기는 피부층을 파고들어가
									알을 낳고 번식을 합니다. 감염된 개는 극심한 가려움증으로 삶의 질이 현저히 저하됩니다.
							</p>
						</div>
					</div>
					<div class="col-md-12 col-lg-6"><img src="/images/parasites/parasites_img.jpg" class="w-100" alt="Dirt on Ticks"></div>
				</div>
			</div>

		</div>
	</section>

	<? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
</body>
</html>
		
