<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Common.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Hospital.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Clinic.class.php";

include "config.php";

$_REQUEST['orderby'] = $orderby;
$_REQUEST['scategory_fk'] = 68;
$objCommon = new Common($pageRows, $tablename, $_REQUEST);
$rowPageCount = $objCommon->getCount($_REQUEST);
$result = $objCommon->getList($_REQUEST);

$category_result = $category_tablename ? $objCommon->getCategoryList($_REQUEST) : null;
?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/logingCheck.php" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/themeHtml.php" ?>
</head>
<body>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
	<div>
		<div id="sync1" class="owl-carousel owl-theme big-product-carousel">
			<div class="item">
				<div class="desktop-only"><img src="/images/products/spot-on-dogs/header-1.jpg" class="w-100" alt="Bravecto Spot On For Dogs"></div>
				<div class="mobile-only"><img src="/images/products/spot-on-dogs/header-1-mobile.jpg" class="w-100" alt="Bravecto Spot On For Dogs"></div>
			</div>
			<div class="item">
				<div class="desktop-only"><img src="/images/products/spot-on-dogs/header-2.jpg" class="w-100" alt="Bravecto Spot On For Dogs"></div>
				<div class="mobile-only"><img src="/images/products/spot-on-dogs/header-2-mobile.jpg" class="w-100" alt="Bravecto Spot On For Dogs"></div>
			</div>
			<div class="item">
				<div class="desktop-only"><img src="/images/products/spot-on-dogs/header-3.jpg" class="w-100" alt="Bravecto Spot On For Dogs"></div>
				<div class="mobile-only"><img src="/images/products/spot-on-dogs/header-3-mobile.jpg" class="w-100" alt="Bravecto Spot On For Dogs"></div>
			</div>
			<div class="item">
				<div class="desktop-only"><img src="/images/products/spot-on-dogs/header-4.jpg" class="w-100" alt="Bravecto Spot On For Dogs"></div>
				<div class="mobile-only"><img src="/images/products/spot-on-dogs/header-4-mobile.jpg" class="w-100" alt="Bravecto Spot On For Dogs"></div>
			</div>
			<div class="item">
				<div class="desktop-only"><img src="/images/products/spot-on-dogs/header-5.jpg" class="w-100" alt="Bravecto Spot On For Dogs"></div>
				<div class="mobile-only"><img src="/images/products/spot-on-dogs/header-5-mobile.jpg" class="w-100" alt="Bravecto Spot On For Dogs"></div>
			</div>
		</div>
	</div>

	<div class="alt-color">
		<div class="container-1">
			<div id="sync2" class="owl-carousel owl-theme dogs">
				<div class="item yellow-active"><span><i></i>2 - 4.5 kg</span></div>
				<div class="item orange-active"><span><i></i>> 4.5 - 10 kg</span></div>
				<div class="item green-active"><span><i></i>> 10 - 20 kg</span></div>
				<div class="item blue-active"><span><i></i>> 20 - 40 kg</span></div>
				<div class="item purpal-active"><span><i></i>> 40 - 56 kg</span></div>
			</div>
		</div>
	</div>

	<section class="product-section">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-md-12 col-lg-10">
					<div class="product-details-top">
						<h1 class="wow fadeInUp">강아지용 브라벡토 스팟온</h1>
						<h4 class="wow fadeInUp">더욱 쉬워진 브라벡토 스팟온으로 당신의 반려견을 보호하세요</h4>
						<p class="wow fadeInUp">브라벡토 스팟온만의 TWIST’N’USE 기술로 더욱 손쉽게 투여가 가능합니다.</p>
						<ul class="wow fadeInUp">
							<li>진드기(tick and mite) 및 벼룩으로 부터 12주간 보호합니다</li>
							<li>귀진드기, 옴진드기, 모낭충에도 효과가 있습니다</li>
							<li>더욱 쉬워졌습니다. 뚜껑을 뗄 필요도 없이, 비틀고 바르면 됩니다.</li>
							<li>8주령 이상 및 2kg 이상의 강아지 뿐만 아니라 임신 및 수유중인 강아지에도 안심하고 사용할 수 있습니다</li>
						</ul>
					</div>
				</div>
				<div class="col-md-12 col-lg-8 wow fadeInUp">
					<div class="product-details-top pt-0 pb-180">
						<ul class="nav nav-tabs" id="proTab" role="tablist">
							<li class="nav-item"> <a class="nav-link active" id="longlastng-tab" data-toggle="tab" href="#loglasting" role="tab" aria-selected="true" title="Long Lating">편리한 </a> </li>
							<li class="nav-item"> <a class="nav-link" id="effective-tab" data-toggle="tab" href="#effective" role="tab" aria-selected="false" title="Efective">작용</a> </li>
							<li class="nav-item"> <a class="nav-link" id="hasslefree-tab" data-toggle="tab" href="#hasslefree" role="tab" aria-selected="false" title="Easy">맛있는 </a> </li>
							<li class="nav-item"> <a class="nav-link" id="global-tab" data-toggle="tab" href="#global" role="tab" aria-selected="false" title="Global">세계적인</a> </li>
						</ul>
						<div class="tab-content" id="proTabContent">
							<div class="tab-pane fade show active" id="loglasting" role="tabpanel">
								<h4>편리한</h4>
								<p>브라벡토는 1회 사용만으로 12주간 외부기생충으로 부터 반려견을 보호합니다. 
								매 계절 1회 사용으로 기억하기 쉽고, 외부기생충 예방을 놓칠 일이 적어집니다.</p>

							</div>
							<div class="tab-pane fade" id="effective" role="tabpanel" aria-labelledby="effective-tab">
								<h4>빠르고 오래가는</h4>
								<p>
									브라벡토의 주성분 플루랄라너는 사용 4시간 이내에 작용하기 시작하여, 12시간 이내에
									진드기의 100%를 사멸합니다. 또한 플루랄라너의 대부분이 비활성 상태로 저장되어 있다가 12주간 서서히 방출됩니다.
								</p>
							</div>
							<div class="tab-pane fade" id="hasslefree" role="tabpanel" aria-labelledby="hasslefree-tab">
								<h4>안전한</h4>
								<p>
									브라벡토는 8주령이 갓 지났거나 2kg가 조금 넘는 작은 체구의 강아지, 임신중 혹은 수유중인 반려견에서
									안심하고 사용할 수 있습니다. 또한 많은 종류의 의약품 사용이 불가능한 콜리종에서의 안전성도 검증되었습니다.
								</p>
							</div>
							<div class="tab-pane fade" id="global" role="tabpanel" aria-labelledby="global-tab">
								<h4>세계적인</h4>
								<p>브라벡토는 전 세계 85여개 국가에서 판매되며, 2019년 1억 도스 판매를 돌파하였습니다.
								엄청난 판매량으로 그 안전성이 검증된 브라벡토의 놀라운 효능을 경험해 보세요.</p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12 col-lg-4 mob-pedding wow fadeIn">
					<div class="video-block wow fadeInUp"> <img src="/images/products/spot-on-dogs/section-1.jpg" alt="Dog" class="w-100"></div>
				</div>
			</div>
		</div>
	</section>

	<section class="alt-color">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-md-12 col-lg-8">
					<div class="benefits benefits-noimg">
						<div class="benefits-list wow fadeInUp"> <a href="#" class="redbg-link">플루랄라너</a> </div>
						<div class="benefits-list extrta-mb">
							<h3 class="wow fadeInUp">브라벡토만의 혁신적인 성분, 플루랄라너</h3>
							<p class="wow fadeInUp">2014년 미국 FDA를 시작으로, 전 세계 85여개 국가에서 허가를 받은 브라벡토의 성분 플루랄라너는 
							혈액에 흡수됨과 동시에 대부분이 비활성화 상태로 저장되어 서서히 방출됩니다.
							12주나 되는 기간동안 그 효능의 대부분을 유지하여 반려견에게 문제가 되는 외부기생충을 
							더욱 간편하고 안전하게 예방할 수 있게 되었죠. </p>
						</div>
						<div class="benefits-list extrta-mb ">
							<h3 class="wow fadeInUp">방대한 임상 연구와 모니터링</h3>
							<p class="wow fadeInUp">브라벡토(플루랄라너)는 저희 MSD동물약품의 비전 ‘The Science of Healthier Animals’ 의
							놀라운 결과물입니다. 방대한 임상 연구와 모니터링을 바탕으로 반려동물들이 브라벡토(플루랄라너)의 
							강력한 효능과 지속성 및 높은 안전성을 경험할 수 있게 되었습니다.
						</div>
					</div>
				</div>
				<div class="col-md-12 col-lg-4 mob-pedding wow fadeIn">
					<div class="video-block top-minouse">
						<a href="https://youtu.be/4JHV4jE7pX4?autoplay=1&rel=0" data-toggle="lightbox" data-gallery="youtubevideos">
							<img src="/images/videos/spot-on-dogs-video.jpg" class="img-fluid" alt="Spot On Dogs Video">
						</a>
						<a href="#" class="playbutton" title="Watch Video"></a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-md-12 col-lg-8 wow fadeInUp">
					<div class="benefits benefits-noimg">
						<div class="benefits-list mb-0  wow fadeInUp">
							<h3 class="bigheading">브라벡토 스팟온 투여 방법</h3>
							<div class="steps-car owl-carousel owl-theme">
								<div class="item" data-dot="step 1">
									<p>브라벡토 스팟온의 안전포장지를 뜯습니다.</p>
									<figure><img src="/images/steps/step-1.jpg" class="img-fluid" alt="How to use Bravecto Spot-on for Dogs"></figure>
								</div>
								<div class="item" data-dot="step 2">
									<p>브라벡토 스팟온의 뚜껑을 위로 향하게 잡고, 비틀어 줍니다.</p>
									<p><strong>뚜껑은 떨어지지 않으니 무리하게 떼지 마십시오.</strong></p>
									<figure><img src="/images/steps/step-2.jpg" class="img-fluid" alt="How to use Bravecto Spot-on for Dogs"></figure>
								</div>
								<div class="item" data-dot="step 3">
									<p>반려견의 털을 갈라 피부가 노출되게 한 후 등쪽 피부선을 따라 브라벡토 스팟온을 바릅니다.</b></p>
									<figure><img src="/images/steps/step-3.jpg" class="img-fluid" alt="How to use Bravecto Spot-on for Dogs"></figure>
								</div>
								<div class="item" data-dot="step 4">
									<p>소형견은 두 군데, 중형견은 세 군데, 대형견은 네 군데에 나누어 발라주세요.</p>
									<figure><img id="for-dogs-step-4-image" src="/images/steps/step-dog-4.png" class="img-fluid" alt="How to use Bravecto Spot-on for Dogs"></figure>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12 col-lg-4 mob-pedding wow fadeIn">
					<div class="video-block top-minouse">
						<a href="https://youtu.be/iz7_X4AO9CQ?autoplay=1&rel=0" data-toggle="lightbox" data-gallery="youtubevideos">
							<img src="/images/videos/spot-on-dog-alt-video.jpg" class="img-fluid">
						</a>
						<a href="#" class="playbutton" title="Watch Video" onclick=""></a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="alt-color">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-md-12 col-lg-8 moile-last">
					<div class="benefits benefits-noimg">
						<div class="benefits-list wow fadeInUp">
							<h2>더 궁금하신 점이 있으신가요?</h2>
							<div class="accordion" id="faqs">
								<?
									$i = 0;
									if($rowPageCount[0] > 0){
									while ($row=mysql_fetch_assoc($result)) {
								?>
								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f<?=$row['no']?>" aria-expanded="true" href="#"> <?=$row['title']?> </a>
									<div id="f<?=$row['no']?>" class="collapse" data-parent="#faqs">
										<div class="answer"><?=$row['contents']?></div>
									</div>
								</div>
								<?
										}
									}else{
								?>

								<div class="faq-list wow fadeInUp">
									<a data-toggle="collapse" data-target="#f1" aria-expanded="true" href="#"> 등록된 글이 없습니다. </a>
								</div>
								<?
									}
								?>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12 col-lg-4 mobile-first">
					<div class="where-to-get top-minouse wow fadeInUp">
						<h2>브라벡토 구매를 원하시나요?</h2>
						<p>당신의 반려견에게 가장 알맞은 제품에 대해 수의사와 상담하세요. 
						구입을 원하신다면 거주지역을 아래 검색창에 입력하세요.</p>
						<form action="/page/stockists.php">
							<input name="zipcode" type="text" placeholder="suburb">
							<button class="arror-btn" type="submit"></button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
</body>
</html>
		
