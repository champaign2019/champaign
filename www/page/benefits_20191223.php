<?session_start();?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/logingCheck.php" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/themeHtml.php" ?>
</head>
<body>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
	<section class="inner-header">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-12  col-lg-6">
					<div class="header-text-inner">
						<h1 class="anim"><span class="titleanimation-bottom">브라벡토 장점</span></h1>
						<p class="wow fadeInUp">맛있는 츄어블 한 정으로 무엇보다도 쉽고 오랫동안 
						당신의 반려견을 외부기생충으로부터 보호해 줍니다.</p>
					</div>
				</div>
				<div class="col-12  col-lg-6">
					<figure class="d-flex"><img src="/images//benefits/bravecto-benefits.png" alt="effective and long-lasting" class=" img-fluid wow fadeInUp"></figure>
				</div>
			</div>
		</div>
	</section>

	<section class="col-3-section">
		<div class="container-1">
			<div class="row row-2">
				<div class="col-md-12 col-lg-4">
					<div class="glid-list">
						<figure><img src="/images/icons/longlasting.svg" alt="effective and long-lasting" width="200" class="wow fadeInUp"></figure>
						<h5 class="anim"><span class="titleanimation-bottom">빠르고 오래갑니다</span></h5>
						<p class="wow fadeInUp">브라벡토는 섭취 후 4시간 이내에 작용하기 시작하여 12시간 이내에 진드기 및 벼룩을 100% 사멸합니다. 
						이는 진드기 및 벼룩의 매개질병 예방 골든타임인 24-48시간 보다 훨씬 빠르며, 당신의 반려동물을 12주간
						외부기생충으로 부터 안전하게 보호합니다.</p>
					</div>
				</div>
				<div class="col-md-12 col-lg-4">
					<div class="glid-list">
						<figure><img src="/images/icons/mind.svg" alt="peace of mind" width="200" class="wow fadeInUp"></figure>
						<h5 class="anim"><span class="titleanimation-bottom">쉽고 간편합니다</span></h5>
						<p class="wow fadeInUp">브라벡토 츄어블은 가수분해 원료를 사용한 돼지 간 맛으로, 맛있을 뿐만 아니라 단백질 유래 피부 알러지도 예방을 해줍니다. 또한 브라벡토 스팟온은 입맛이 까다로운 고양이에게 손쉽게 바르기만 하면 됩니다.</p>
					</div>
				</div>
				<div class="col-md-12 col-lg-4">
					<div class="glid-list">
						<figure><img src="/images/icons/simple.svg" alt="simple and convenient" width="200" class="wow fadeInUp"></figure>
						<h5 class="anim"><span class="titleanimation-bottom">12주간 안심하세요</span></h5>
						<p class="wow fadeInUp">우리 집 장난꾸러기 강아지와 고양이, 산책시 풀숲에서 또는 피부에 살고있던 진드기 및 벼룩에 감염되면 보이는 것보다 더욱 괴로울 뿐만 아니라 치명적인 질환에 감염될 수도 있습니다. 브라벡토로 손쉽게 12주간 이러한 위험으로 부터 반려동물을 보호하세요.</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="alt-color">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-md-12 col-lg-8">
					<div class="benefits benefits-noimg">
						<div class="benefits-list wow fadeInUp"> <a href="#" class="redbg-link">SAFETY</a> </div>
						<div class="benefits-list extrta-mb">
							<h3 class="anim"><span class="titleanimation-bottom">강아지용 브라벡토</span></h3>
							<p class="wow fadeInUp">브라벡토는 8주령, 2kg 이상의 어린 강아지에 안전하게 사용이 가능하며, 임신 및 수유중인 강아지에서도
							사용할 수 있을 만큼 안전성을 검증 받았습니다.</p>
						</div>
						<div class="benefits-list extrta-mb ">
							<h3 class="anim"><span class="titleanimation-bottom">고양이용 브라벡토</span></h3>
							<p class="wow fadeInUp">브라벡토 스팟온은 11주령, 1.2kg이 갓 넘는 아주 작은 새끼고양이에서도 안전하게 사용할 수 있습니다.</p>
						   
						</div>
						<div class="benefits-list extrta-mb">
							<h3 class="anim"><span class="titleanimation-bottom">전 세계 1억 도스 이상 판매</span></h3>
							<p class="wow fadeInUp">브라벡토는 글로벌 출시 5주년인 2019년 전 세계 1억 도스 판매를 달성하였습니다. 전 세계 구석구석 수많은
							강아지, 고양이 및 보호자들이 브라벡토만의 독보적인 효능을 경험하였습니다. </p>
						</div>
					</div>
				</div>
				<div class="col-md-12 col-lg-4 mob-pedding wow fadeIn">
					<div class="video-block top-minouse"> 
						<a href="https://youtu.be/CuI_EfYnZHU" data-toggle="lightbox" data-gallery="youtubevideos">
							<img src="/images/videos/benefits-video.jpg" class="img-fluid" alt="Benefits Video">
						</a>
						<a href="#" class="playbutton" title="Watch Video"></a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
</body>
</html>
		
