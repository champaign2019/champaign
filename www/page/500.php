<?session_start();?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/logingCheck.php" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/themeHtml.php" ?>
</head>
<body>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
	<section class="hero-content hero-content__points backgrounds backgrounds__solid-color__purple typography__text-white-color text-center">
		<div class="row">
			<div class="small-12 columns">
				<div class="hero-content__header hero-content__points__header">
					<h1>500: Internal Server Error</h1>
				</div>
			</div>
		</div>
	</section>
	<section class="hero-content reminder-sign-up-form">
		<div class="row">
			<div class="small-12 medium-12 columns text-center">
				<div class="text-center">
					<p>Unfortunately there seems to be a problem trying to load your page. Please <a href="contact">contact us</a> to notify us of this issue.</p>
				</div>
			</div>
		</div>
	</section>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
</body>
</html>
		
