<?session_start();?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/logingCheck.php" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/themeHtml.php" ?>
</head>
<body>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
	<section class="inner-header">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-12  col-lg-6 m_ver">
					<figure class="d-flex"><img src="/images//benefits/bravecto-benefits.png" alt="effective and long-lasting" class=" img-fluid wow fadeInUp"></figure>
				</div>
				<div class="col-12  col-lg-6">
					<div class="header-text-inner">
						<h1 class="anim" style="color:#c0d22e;"><span class="titleanimation-bottom">브라벡토 장점</span></h1>
						<p class="wow fadeInUp">반려견과 반려묘 각각의 특성에 맞는 제형 선택으로 무엇보다도 쉽고 오랫동안 당신의 반려동물을 외부기생충으로부터 보호할 수 있습니다.</p>
					</div>
				</div>
				<div class="col-12  col-lg-6 pc_ver">
					<figure class="d-flex"><img src="/images//benefits/bravecto-benefits.png" alt="effective and long-lasting" class=" img-fluid wow fadeInUp"></figure>
				</div>
			</div>
		</div>
	</section>

	<section class="video_con type02">
		<div class="container-1">
			<div class="video_box">
				<iframe width="820" height="470" src="https://www.youtube.com/embed/sBUbSFkz3Vs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
		</div>
	</section>

	<section class="col-3-section">
		<div class="container-1">
			<div class="row row-2">
				<div class="col-md-12 col-lg-4">
					<div class="glid-list">
						<figure><img src="/images/icons/bene_icon01.png" alt="effective and long-lasting" width="200" class="wow fadeInUp"></figure>
						<h5 class="anim"><span class="titleanimation-bottom">빠르고 오래갑니다</span></h5>
						<p class="wow fadeInUp">브라벡토는 투여 후 4시간 이내에 작용하기 시작하여 12시간 이내에 흡혈 진드기 및 벼룩이 100% 사멸됩니다. 이는 진드기 및 벼룩의 매개질병 예방의 골든타임인 24-48시간 보다 훨씬 빠르며, 12주간 지속으로 당신의 반려동물을 오랫동안 외부기생충으로부터 보호합니다.</p>
					</div>
				</div>
				<div class="col-md-12 col-lg-4">
					<div class="glid-list">
						<figure><img src="/images/icons/bene_icon02.png" alt="peace of mind" width="200" class="wow fadeInUp"></figure>
						<h5 class="anim"><span class="titleanimation-bottom">쉽고 간편합니다</span></h5>
						<p class="wow fadeInUp">반려견에는 맛있게 먹일 수 있는 츄어블 제형으로, 반려묘에는 손쉽게 바를 수 있는 스팟온 제형으로. 스트레스 없이 외부기생충을 쉽고 간편하게 예방하세요.</p>
					</div>
				</div>
				<div class="col-md-12 col-lg-4">
					<div class="glid-list">
						<figure><img src="/images/icons/bene_icon03.png" alt="simple and convenient" width="200" class="wow fadeInUp"></figure>
						<h5 class="anim"><span class="titleanimation-bottom">12주간 안심하세요</span></h5>
						<p class="wow fadeInUp">1년에 딱 4번, 빈틈 없이! / 우리 집 장난꾸러기 강아지와 고양이, 산책시 풀숲에서 또는 피부에 살고있던 진드기 및 벼룩에 감염되면 보이는 것보다 더욱 괴로울 뿐만 아니라 치명적인 질환에 감염될 수도 있습니다. 12주간 예방되는 브라벡토로 빈틈 없는 사랑을 전하세요.</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="alt-color chew_con">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-md-12 col-lg-8">
					<div class="benefits benefits-noimg">
						<div class="benefits-list wow fadeInUp"> <a href="#" class="redbg-link">안전성</a> </div>
						<div class="benefits-list extrta-mb">
							<h3 class="anim"><span class="titleanimation-bottom">강아지용 브라벡토 츄어블</span></h3>
							<p class="wow fadeInUp">브라벡토는 8주령, 2kg 이상의 어린 강아지에 안전하게 사용이 가능하며, 임신 및 수유중인 강아지에서도
							사용할 수 있을 만큼 안전성을 검증 받았습니다.</p>
						</div>
						<div class="benefits-list extrta-mb ">
							<h3 class="anim"><span class="titleanimation-bottom">고양이용 브라벡토 스팟온</span></h3>
							<p class="wow fadeInUp">브라벡토 스팟온은 11주령, 1.2kg이 갓 넘는 아주 작은 새끼고양이에서도 안전하게 사용할 수 있습니다.</p>
						   
						</div>
						<div class="benefits-list extrta-mb">
							<h3 class="anim"><span class="titleanimation-bottom">전 세계 1억 도스 이상 판매</span></h3>
							<p class="wow fadeInUp">브라벡토는 글로벌 출시 5주년인 2019년 전 세계 1억 도스 판매를 달성하였습니다. 전 세계 구석구석 수많은
							강아지, 고양이 및 보호자들이 브라벡토만의 독보적인 효능을 경험하였습니다. </p>
						</div>
					</div>
				</div>
				<div class="col-md-12 col-lg-4 mob-pedding wow fadeIn">
					<div class="video-block top-minouse img_width"> 
						<a href="https://www.youtube.com/watch?v=3duEqxS2-wo" data-toggle="lightbox" data-gallery="youtubevideos">
							<img src="/images/sum03.jpg" class="img-fluid" alt="Benefits Video">
						</a>
						<a href="#" class="playbutton" title="Watch Video"></a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>

	<script>
		var link = $('.video_con .video_box iframe').attr('src');
		var header_h = $('header').height();
		var nav_h = $('.nav').height();
		var divOffset = $('.video_con').offset().top - header_h;
		var oneStart = 'true';
		$(window).scroll(function(){
			var position = $(window).scrollTop();
			
			if (oneStart === 'true'){
				if(position - divOffset > 0){
					$('.video_con .video_box iframe').attr('src', link + '?autoplay=1');
					oneStart = 'false';
				}
			}
		});
	</script>
</body>
</html>
		
