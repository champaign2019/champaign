<?session_start();?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/logingCheck.php" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/themeHtml.php" ?>
</head>
<body>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
	<section class="inner-header">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-12  col-lg-6">
					<div class="header-text-inner">
						<h1 class="anim"><span class="titleanimation-bottom">About us</span></h1>
						<p class="wow fadeInUp">Bravecto is a brand of MSD Animal Health, which develops, manufactures and markets a broad range of veterinary medicines and services globally.</p>
					</div>
				</div>
				<div class="col-12  col-lg-6 ">
					<figure class="d-flex"><img src="/images/about/about-header.png" alt="About Bravecto" class=" img-fluid wow fadeInUp"></figure>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-md-12 col-lg-8">
					<div class="product-details-top">
						<h1 class="anim"><span class="titleanimation-bottom">Our vision</span></h1>
						<h4 class="wow fadeInUp">At MSD Animal Health, the makers of Bravecto, our vision is to make a difference in the lives of people globally through our innovative medicines, vaccines and animal health products. </h4>
						<p class="wow fadeInUp">We are committed to being the premier, research-intensive biopharmaceutical company and are dedicated to providing leading innovations and solutions for today and the future.</p>
						<p class="wow fadeInUp">Our company is driven by simple core values. We strive to be visionary, collaborative, dynamic and responsible. Whether working with veterinarians or their patients around the world, healthy pets mean happy families.</p>
						<p class="wow fadeInUp">Since our early days, we have grown to become a leader whose work in animal health is unparalleled. For more than 60 years, our mission to protect the health and well-being of pets has seen us develop vaccines that protect pets from dangerous diseases, medicines to treat chronic diseases and effective parasite controls.</p>
						<br>
						<br>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="alt-color ">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-md-12 col-lg-8 ">
					<div class="benefits benefits-noimg">
						<div class="benefits-list wow fadeInUp">
							<div class="accordion" id="faqs">
								<div class="faq-list wow fadeInUp">
									<a data-toggle="collapse" data-target="#f1" aria-expanded="true" aria-controls="f1" href="#">
										<h4>Testing and safety</h4>
									</a>
									<div id="f1" class="collapse show " data-parent="#faqs">
										<div class="answer">
											Our top priority is the health of your pet. In fact, our employees share the common goal of working together to ensure your pets are there to comfort you and be an important part of families for years to come.<br>
											<br>
											We are serious about improving the health of animals around the world. This is why we are committed to investing in the research, development and manufacturing of some of the most innovative veterinary medicines in the world, improving the lives of animals in more than 150 countries and counting.
										</div>
									</div>
								</div>
								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f6" aria-expanded="true" aria-controls="f6" href="#">
										<h4>MSD Animal Health’s mission </h4>
									</a>
									<div id="f6" class="collapse" data-parent="#faqs">
										<div class="answer">

											<p>
												Bravecto is a brand of MSD Animal Health, which is a global healthcare company committed to their goal of making a difference - a goal that’s already 125 years and running.
											</p>

											<p>
												MSD Animal Health understands the strong bonds between people and their pets. Our goal is to support those bonds by providing small-animal veterinarians and pet owners with state-of-the-art products and services that help pets live longer, healthier lives.
											</p>

											<p>
												At MSD Animal Health we believe our responsibilities to wider society extend beyond our primary goals as a business. We continue to develop innovative solutions to find new and better ways to keep pets safe and healthy, with our veterinarians, scientists and industry experts working together in teams centered around entire diseases affecting pets, not just single products.
											</p>

											<p>
												In addition to all the great work our employees are doing to support you and your veterinarian, MSD Animal Health is involved with global initiatives such as Afya and Mission Rabies – both organisations working in Africa and India to eradicate the globe of rabies in the next 15 years. Locally in Australia, we are working with Assistance Dogs Australia who provide people who are living with physical disabilities with trained assistance dogs to help them move around and conduct everyday activities.
											</p>

											<p>MSD Animal Health. ABN 79 008 467 034.Toll free: 1800 230 833.</p>

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12 col-lg-4 mob-pedding wow fadeIn">
					<div class="video-block top-minouse"> <img src="/images/about/about-bottom.jpg" class="w-100" alt="Cat Video"> </div>
				</div>
			</div>
		</div>
	</section>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
</body>
</html>
		
