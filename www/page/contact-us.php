<?session_start();?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/logingCheck.php" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/themeHtml.php" ?>
</head>
<body>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
	<section class="contactus contactus_con01 inner-header">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-12 col-lg-4 m_ver">
					<figure class="d-flex">
						<img src="/images/contact-us/contact_us.png" class="img-fluid" alt="Contact us">
					</figure>
				</div>
				<div class="col-12 col-lg-8 align-self-center">
					<div class="contact-wrap type02 header-text-inner">
						<h1 class="anim type02" style="color:#dd168b;"><span class="titleanimation-bottom mobile_small">떠나가는 기억,</span><br/><span class="titleanimation-bottom">스마트한 브라벡토</span></h1>
						<p class="wow fadeInUp text" style="font-size:20px; ">
							우리집 막내둥이의 행복을 위해 절대 깜빡해서는 안될 브라벡토 투여일. <br />
							12주 동안이나 깜빡하지 않기에는 너무 바쁘게 살고 계시지는 않나요? <br />
							브라벡토 리마인더로 더 이상 떠나가는 기억과 씨름하지 마세요! <br />
							iOS 및 안드로이드 모두 사용 가능합니다.
						</p>	
						<p class="mt40 fadeInUp wow">
							<a href="https://apps.apple.com/kr/app/bravecto-dose-reminder/id1104271591?l=en" title="appstore" target="_blank"><img src="/images/footer/appstore.png" alt=""></a>
							<a href="https://play.google.com/store/apps/details?id=au.com.leafcutter.bravecto&hl=ko" title="googleplay" target="_blank"><img src="/images/footer/googleplay.png" alt=""></a>
						</p>
					</div>
				</div>
				<div class="col-12 col-lg-4 pc_ver">
					<figure class="d-flex">
						<img src="/images/contact-us/contact_us.png" class="img-fluid" alt="Contact us">
					</figure>
				</div>
			</div>
		</div>
	</section>

	<section class="contactus contactus_con02">
		<div class="container-1">
			<ul>
				<li class="wow fadeInUp">
					<p><b>STEP 1</b></p>
					<div class="img_box"><img src="/images/contact-us/contactus_con02_img01.png" alt="폰이미지" /></div>
					<p>우리집 막내둥이의 사진을 <br />예쁘게 찍어 등록하세요</p>
				</li>
				<li class="wow fadeInUp">
					<p><b>STEP 2</b></p>
					<div class="img_box"><img src="/images/contact-us/contactus_con02_img02.png" alt="폰이미지" /></div>
					<p>브라벡토를 투여한 <br />날짜를 선택하세요</p>
				</li>
				<li class="wow fadeInUp">
					<p><b>STEP 3</b></p>
					<div class="img_box"><img src="/images/contact-us/contactus_con02_img03.png" alt="폰이미지" /></div>
					<p>투여일이 가까워지면 <br />자동으로 알림이 옵니다</p>
				</li>
			</ul>
		</div>
	</section>

	<section class="contactus contactus_con03">
		<div class="container-1">
			<h4 class="wow fadeInUp">지금 <b>다운로드</b>하세요</h4>
			<p class="mt40 wow fadeInUp">아래 버튼을 클릭하여 <b>브라벡토 스마트앱</b>을 손쉽게 설치하세요. <br />
			또한 애플 앱스토어, 구글 플레이스토어에서 <b>[브라벡토]</b>를 검색하여 어플을 다운로드 받을 수 있습니다.</p>
			<ul class="wow fadeInUp">
				<li><a href="https://apps.apple.com/kr/app/bravecto-dose-reminder/id1104271591?l=en"><img src="/images/contact-us/contactus_con03_btn01.png" alt="앱스토어" /></a></li>
				<li><a href="https://play.google.com/store/apps/details?id=au.com.leafcutter.bravecto&hl=ko"><img src="/images/contact-us/contactus_con03_btn02.png" alt="앱스토어" /></a></li>
			</ul>
		</div>
	</section>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
</body>
</html>
		
