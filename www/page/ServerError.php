<?session_start();?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/logingCheck.php" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/themeHtml.php" ?>
</head>
<body>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
	<section class="inner-header">
		<div class="container-1">
			<div class="error-page">

				<h1 class="anim"><span class="titleanimation-bottom">Oops... 500</span></h1>



				<div class="errorimg"><img src="images/error-dog.png" class="img-fluid" alt="Bravecto"></div>

				<p class="wow fadeInUp">
					Our highly trained dog encountered and error while processing your request.
					<br>

					<a href="/" class="findoutmore" title="Find out more"><span>Go to Home</span></a>
				</p>

			</div>
		</div>
	</section>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
</body>
</html>
		
