<?session_start();?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/logingCheck.php" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/themeHtml.php" ?>
</head>
<body>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
	<section class="inner-header">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-12  col-lg-6">
					<div class="header-text-inner">
						<h1 class="anim"><span class="titleanimation-bottom">Hard truths about heartworms</span></h1>
						<p class="wow fadeInUp">
							Heartworm can be transferred with a single mosquito bite, posing serious risks for indoor and outdoor pets.
						</p>
					</div>
				</div>
				<div class="col-12  col-lg-6 ">
					<figure class="d-flex"><img src="/images/parasites/heartworms-header.png" alt="Dirt on Ticks" class=" img-fluid wow fadeInUp"></figure>
				</div>
			</div>
		</div>
	</section>

	<section class="facts-ticks">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-12 col-lg-6">
					<div class="facts-list facts-light">
						<h4 class="anim"><span class="titleanimation-bottom">The facts about heartworms</span></h4>
						<ul>
							<li>
								<h6 class="anim">
									<span class="titleanimation-bottom">
										Heartworm is a blood-borne parasite spread by mosquitoes
									</span>
								</h6>
								<p class="wow fadeInUp">
									Carried in the blood, heartworm transmission can be as simple as a single bite from a mosquito having previously fed on another infected animal.
								</p>
							</li>
							<li>
								<h6 class="anim">
									<span class="titleanimation-bottom">
										The threat across Australia is highest in tropical and coastal regions
									</span>
								</h6>
								<p class="wow fadeInUp">
									With our warm Australian climate, mosquitoes can plague us all year round. Known to breed in greater numbers around areas with stagnant water - from ponds and streams to yards and household plants - mosquitoes can also travel great distances when blown by strong winds.
								</p>
							</li>
							<li>
								<h6 class="anim">
								<span class="titleanimation-bottom">
									Both indoor and outdoor cats are at risk
								</span>
								</h6>
								<p class="wow fadeInUp">
									While flyscreens can help limit the entry of mosquitoes into our homes, those persistant little pests invariably find a way inside, leaving indoor cats at risk.
								</p>
							</li>
							<li>
								<h6 class="anim">
									<span class="titleanimation-bottom">
										Heartworm is a silent killer with few symptoms
									</span>
								</h6>
								<p class="wow fadeInUp">
									Taking up to 6 months to mature into adults, and living up to 2-3 years within a cat, heartworms may cause very few symptoms until it's too late.
								</p>
							</li>
							<li>
								<h6 class="anim">
									<span class="titleanimation-bottom">
										There is no approved treatment, only preventatives
									</span>
								</h6>
								<p class="wow fadeInUp">
									Currently there are no approved treatments for eliminating heartworm from infected cats, making prevention the best.
								</p>
							</li>
						</ul>
					</div>
				</div>

				<div class="col-12 col-lg-6">
					<div class="facts-list extrta-pad">
						<h4 class="anim"><span class="titleanimation-bottom">Tips for preventing heartworms</span></h4>
						<ul>
							<li>
								<h6 class="anim"><span class="titleanimation-bottom">Seek expert advice</span></h6>
								<p class="wow fadeInUp">
									The best place to start is by visiting your local vet who can discuss prevention with you.
								</p>
							</li>
							<li>
								<h6 class="anim">
									<span class="titleanimation-bottom">
										Use Bravecto <span class="plus">Plus</span> for Cats
									</span>
								</h6>
								<p class="wow fadeInUp">
									Bravecto <span class="plus">Plus</span> for Cats provides long-lasting protection against heartworm, fleas and paralysis ticks as well as treatment of roundworm, hookworm and ear mites.
								</p>
							</li>
							<li>
								<h6 class="anim">
									<span class="titleanimation-bottom">
										Reduce mosquitoes around your home
									</span>
								</h6>
								<p class="wow fadeInUp">
									Limiting mosquitoes around the house is an easy way to make life better for you and your pets. Remove stagnant water from pot plant dishes, make it routine to clean water bowls, and keep flyscreens closed on windows and doors as often as possible - particularly from dusk to dawn, when mosquito activity is highest.
								</p>
							</li>
							<li>
								<h6 class="anim">
									<span class="titleanimation-bottom">
										Keep your cat happy and healthy.
									</span>
								</h6>
								<p class="wow fadeInUp">
									Remember that your cat's immune system is it's main natural defence against disease. Providing a fun, playful home with less stress and more exercise, combined with optimal nutrition and Bravecto <span class="plus">Plus</span> for Cats is an easy way to make sure you're giving them the support they need.
								</p>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="alt-color identify">
		<div class="row no-gutters">
			<div class="col-12">
				<div class="fleestext">
					<span class="red-lable wow fadeInUp">HEARTWORM</span>
					<h5 class="wow fadeInUp text-center mt-4"><em>Dirofilaria immitis</em></h5>
					<img src="/images/parasites/heartworms/heartworms.png" class="img-fluid wow fadeInUp my-3" alt="australia map">
					<p class="wow fadeInUp">
						While heartworm are typically found in dogs, they can make a home in cats and cause a lot of damage in the process. Spread with a single bite from a mosquito carrying blood with infective larvae, the transfer is simple yet devastatingly effective. These nasty invaders are often hard to spot until it's too late, and can result in life-threatening respiratory diseases even at an immature stage of development.
					</p>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-md-12 col-lg-8 wow fadeInUp">
					<div class="benefits mb-more">
						<div class="benefits-list mb-0  wow fadeInUp">
							<h3 class="bigheading anim"><span class="titleanimation-bottom">Life Cycle of a Heartworm</span></h3>
							<p class="wow fadeInUp">Click through all stages of the heartworm life cycle to learn more.</p>
							<div class="steps-car owl-carousel owl-theme wow fadeInUp">
								<div class="item" data-dot="1. MICROFILARIA">
									<p>
										When a mosquito bites an infected host, any premature heartworm larvae (otherwise known as microfilaria) are taken on as part of the blood meal - leaving that satisfied mosquito as an infected carrier.
									</p>
								</div>
								<div class="item" data-dot="2. LARVAE">
									<p>
										Once inside the mosquito, microfilaria mature into into stage 3 heartworm larvae ready to be transmitted.
									</p>
								</div>
								<div class="item" data-dot="3. MIGRATION">
									<p>
										The next time the infected mosquito feeds, matured heartworm larvae enter the bloodstream of a new host.
									</p>
								</div>
								<div class="item" data-dot="4. MATURATION">
									<p>
										Once in the bloodstream, heartworm larvae migrate to the heart, lungs and surrounding blood vessels, maturing into heartworms that can grow up to 30 cm and live for up to two to three years.
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12 col-lg-4 mob-pedding wow fadeIn">
					<div class="video-block">
						<div class="step-images-slider video-slider owl-carousel owl-theme ">
							<div class="video-item"><img src="/images/parasites/heartworms/steps/step-1.svg" class="w-100" alt="Heratworm Step 1"> </div>
							<div class="video-item"><img src="/images/parasites/heartworms/steps/step-2.svg" class="w-100" alt="Heartworm Step 2"> </div>
							<div class="video-item"><img src="/images/parasites/heartworms/steps/step-3.svg" class="w-100" alt="Heartworm Step 3"> </div>
							<div class="video-item"><img src="/images/parasites/heartworms/steps/step-4.svg" class="w-100" alt="Heartworm Step 4"> </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="alt-color">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-md-12 col-lg-8">
					<div class="benefits benefits-noimg">
						<div class="extrta-mb">
							<h4 class="anim">
								<span class="titleanimation-bottom">
									The risks of leaving your cat untreated
								</span>
							</h4>
							<p>
								With very few visible symptoms until they pose a real threat, heartworm can be hard to spot. Watch out for these tell-tale signs, and contact your local vet at the first sign of any symptom.
							</p>

							<ul>
								<li>Coughing</li>
								<li>Vomiting and / or diarrhoea</li>
								<li>Rapid heart rate, seizures or difficulty in breathing</li>
								<li>Lethargy or collapsing</li>
								<li>Decreased appetite</li>
								<li>Sudden death in certain cats</li>
							</ul>
							<p>
								Effective preventative treatments are the best way to protect your cat. Heartworm larvae continue to grow inside the heart and lungs of your pet, leading to heart failure, lung disease and potential damage to other organs in the pet's body. To defend your cat against heartworm, make Bravecto <span class="plus">Plus</span> for Cats part of your routine.
							</p>                       
						</div>
					</div>
				</div>

				<div class="col-md-12 col-lg-4 mob-pedding wow fadeIn">
					<div class="video-block top-minouse">
						<img src="/images/parasites/heartworms/heartworms-bottom.png"class="img-fluid" alt="Heartworms Gold Cat">
					</div>
				</div>
			</div>
		</div>
	</section>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
</body>
</html>
		
