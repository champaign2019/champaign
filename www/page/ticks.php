<?session_start();?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/logingCheck.php" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/themeHtml.php" ?>
</head>
<body>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
	<section class="inner-header">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-12  col-lg-6">
					<div class="header-text-inner">
						<h1 class="anim"><span class="titleanimation-bottom">보일 듯 말 듯, 진드기</span></h1>
						<p class="wow fadeInUp">잔디밭, 도심, 침구류, 다른 동물의 피부 등 진드기는 보일 듯 말듯 우리 주변 곳곳에 숨어 있습니다. 
						일부 종은 피부에 달라붙어 피를 빨아먹고, 피부를 파고들어가 번식 하며 다양한 질병을 유발합니다. </p>
					</div>
				</div>
				<div class="col-12  col-lg-6 ">
					<figure class="d-flex"><img src="/images/parasites/ticks-header.png" alt="Dirt on Ticks" class=" img-fluid wow fadeInUp"></figure>
				</div>
			</div>
		</div>
	</section>

	<section class="facts-ticks">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-12 col-lg-6">
					<div class="facts-list facts-light">
						<h4 class="anim"><span class="titleanimation-bottom">The facts about ticks</span></h4>
						<ul>
							<li>
								<h6 class="anim"><span class="titleanimation-bottom">Paralysis tick - most dangerous tick</span></h6>
								<p class="wow fadeInUp">The single most dangerous tick for dogs and cats in Australia is the 
	paralysis tick.</p>
							</li>
							<li>
								<h6 class="anim"><span class="titleanimation-bottom">3,000 eggs at a time</span></h6>
								<p class="wow fadeInUp">A single female paralysis tick can lay up to 3,000 eggs at a time.</p>
							</li>
							<li>
								<h6 class="anim"><span class="titleanimation-bottom">They are related to spiders</span></h6>
								<p class="wow fadeInUp">
									A tick is a small blood-sucking parasite that is related to spiders.
								</p>
							</li>
							<li>
								<h6 class="anim"><span class="titleanimation-bottom">They can potentially kill dogs and cats</span></h6>
								<p class="wow fadeInUp">After attaching and feeding, a paralysis tick starts producing a potent toxin that affects the animals' central nervous system and causes progressive paralysis and possible death.</p>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-12 col-lg-6">
					<div class="facts-list extrta-pad">
						<h4 class="anim"><span class="titleanimation-bottom">Tips for preventing ticks</span></h4>
						<ul>
							<li>
								<h6 class="anim"><span class="titleanimation-bottom">Use Bravecto</span></h6>
								<p class="wow fadeInUp">Bravecto provides long-lasting protection against paralysis ticks for dogs and cats.</p>
							</li>
							<li>
								<h6 class="anim"><span class="titleanimation-bottom">Ticks like bush and shrub areas</span></h6>
								<p class="wow fadeInUp">Avoid bushy areas and long grass.</p>
							</li>
							<li>
								<h6 class="anim"><span class="titleanimation-bottom">Create a barrier between natural areas</span></h6>
								<p class="wow fadeInUp">If there are ticks around your home, create barriers by cutting bands of vegetation short between your lawn and surrounding natural areas, 
	or use mulch and wood chips to create vegetation-free bands at least a few metres wide.</p>
							</li>
							<li>
								<h6 class="anim"><span class="titleanimation-bottom">Remember to check for ticks after returning</span></h6>
								<p class="wow fadeInUp">Always check your dog and cat, as well as yourself, for ticks when you return from areas where you may have been exposed, and dress appropriately in long pants and boots if possible. In tick season daily searching for and removal of any ticks found on dogs and cats is recommended.</p>
							</li>
							<li>
								<h6 class="anim"><span class="titleanimation-bottom">Don't risk it - be proactive</span></h6>
								<p class="wow fadeInUp">Always use preventative treatment, such as Bravecto, during tick season.</p>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--
	<section class="alt-color identify">
		<div class="row no-gutters">
			<div class="col-12">
				<h3 class="wow fadeInUp text-center">Identifying ticks</h3>
			</div>
			<div class="col-12 col-md-6">
				<div class="identify-list-right-border d-none d-md-block"></div>
				<div class="identify-list">
					<span class="red-lable wow fadeInUp">PARALYSIS TICK</span>
					<h5 class="wow fadeInUp"><em>Ixodes holocyclus</em></h5>
					<img src="/images/parasites/ticks/paralysis-tick/map-aus-1.png" class="img-fluid wow fadeInUp" alt="australia map">
					<p class="wow fadeInUp">Paralysis ticks need humidity and the right temperature to develop. They are mainly found on the east coast of Australia. They will not survive or breed in cold and dry climates.</p>
					<img src="/images/parasites/ticks/paralysis-tick/adult-female-no-engorgement.png" class="img-fluid wow fadeInUp" alt="Adult female - no engorgement">
					<h6 class="wow fadeInUp">Adult female - no engorgement</h6>
					<img src="/images/parasites/ticks/paralysis-tick/adult-female-engorgement.png" class="img-fluid wow fadeInUp" alt="Adult female - moderate engorgement">
					<h6 class="wow fadeInUp">Adult female - moderate engorgement</h6>
					<img src="/images/parasites/ticks/paralysis-tick/adult-female-full-engorgement.png" class="img-fluid wow fadeInUp" alt="Adult female - full engorgement">
					<h6 class="wow fadeInUp">Adult female - full engorgement</h6>
				</div>
			</div>
			<div class="col-12 col-md-6">
				<div class="identify-list">
					<span class="red-lable wow fadeInUp">BROWN DOG TICK</span>
					<h5 class="wow fadeInUp"><em>Rhipicephalus sanguineus</em></h5>
					<img src="/images/parasites/ticks/brown-dog-tick/map-aus-2.png" class="img-fluid wow fadeInUp" alt="australia map">
					<p class="wow fadeInUp">Brown dog ticks thrive in temperate regions of the country however are capable of overwintering within kennels and homes in colder climates. The brown dog tick can thrive indoors throughout its entire life cycle, unlike other tick species.</p>
					<img src="/images/parasites/ticks/brown-dog-tick/adult-male.png" class="img-fluid wow fadeInUp" alt="Adult male">
					<h6 class="wow fadeInUp">Adult male</h6>
					<img src="/images/parasites/ticks/brown-dog-tick/adult-female.png" class="img-fluid wow fadeInUp" alt="Adult female ">
					<h6 class="wow fadeInUp">Adult female</h6>
					<img src="/images/parasites/ticks/brown-dog-tick/adult-female-Engorged.png" class="img-fluid wow fadeInUp" alt="Adult female - Engorged">
					<h6 class="wow fadeInUp">Adult female - engorged</h6>
				</div>
			</div>
		</div>
	</section>
	-->
	<section>
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-md-12 col-lg-8 wow fadeInUp">
					<div class="benefits mb-more">
						<div class="benefits-list mb-0  wow fadeInUp">
							<h3 class="bigheading anim"><span class="titleanimation-bottom">작은소참진드기의 생활환</span></h3>
							<p class="wow fadeInUp">클릭하여 작은소참진드기의 각 단계별 특징을 알아보세요</p>
							<div class="steps-car owl-carousel owl-theme wow fadeInUp">
								<div class="item" data-dot="1. 충란">
									<p>성충은 2-3주간 에 2000개 이상의 충란을 산란하며, 60-90일 후 부화합니다.</p>
								</div>
								<div class="item" data-dot="2. 유충">
									<p>
										유충은 숙주 피부에 올라간지 1시간내 밀착하여 5일가량 흡혈합니다.
									</p>
								</div>
								<div class="item" data-dot="3. 약충">
									<p>
										약충은 변태를 거쳐 간간해진 외피를 가지며, 7일간 흡혈을 하며 성충으로의 변태를 준비합니다.
									</p>
								</div>
								<div class="item" data-dot="4. 성충">
									<p>
										성충은 약 1주일간 흡혈 후 산란을 준비하며, 한 번에 1000개 이상의 알을 낳습니다.
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			   
				<div class="col-md-12 col-lg-4 mob-pedding wow fadeIn">
					<div class="video-block">
						<div class="step-images-slider video-slider owl-carousel owl-theme ">
							<div class="video-item"><img src="/images/parasites/ticks/steps/step@2x.png" class="w-100" alt="Paralysis Tick"> </div>
							<div class="video-item"><img src="/images/parasites/ticks/steps/step2@2x.png" class="w-100" alt="Paralysis Tick"> </div>
							<div class="video-item"><img src="/images/parasites/ticks/steps/step3and4@2x.png" class="w-100" alt="Paralysis Tick"> </div>
							<div class="video-item"><img src="/images/parasites/ticks/steps/step3and4@2x.png" class="w-100" alt="Paralysis Tick"> </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="alt-color">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-md-12 col-lg-8">
					<div class="benefits benefits-noimg">
						<div class="benefits-list extrta-mb">
							<h4 class="anim"><span class="titleanimation-bottom">진드기 확인하기</span></h4>
							<ul>
								<li>
									<h6 class="anim"><span class="titleanimation-bottom">1단계</span></h6>
									<p class="wow fadeInUp">흡혈을 시작하기 이전의 진드기는 매우 작기 때문에 신체검사 시 놓칠 확률이 높습니다. 흡혈을 시작한 이후의 진드기는 좀 더 쉽게 눈에 띱니다.</p>
								</li>
								<li>
									<h6 class="anim"><span class="titleanimation-bottom">2단계</span></h6>
									<p class="wow fadeInUp">손가락으로 반려동물의 털 속을 미끄러지듯 쓸어봅니다. 평소와 다르게 이물 혹은 혹같이 느껴진다면 해당 부위를 꼼꼼히 살피세요. 귓속, 눈 주위, 발가락 사이 등 놓치기 쉬운 부분도 체크하세요.</p>
								</li>
								<li>
									<h6 class="anim"><span class="titleanimation-bottom">3단계</span></h6>
									<p class="wow fadeInUp">산책 후 4-6시간 이내에 꼼꼼히 빗질해 주시면 도움이 될 수 있습니다.</p>
								</li>
								<li>
									<h6 class="anim"><span class="titleanimation-bottom">4단계</span></h6>
									<p class="wow fadeInUp">진드기 제거시에는 족집게를 사용하여 반려동물의 피부에 최대한 가깝게 잡고 천천히 피부로 부터 떼어내세요. 확 떼려 하면 진드기의 몸통과 머리가 분리될 수 있습니다.</p>
								</li>
								<li>
									<h6 class="anim"><span class="titleanimation-bottom">5단계</span></h6>
									<p class="wow fadeInUp">진드기의 머리가 분리되어 피부에 박혔다면, 국소적인 피부 반응 및 감염이 일어날 수 있습니다. 이런 경우 가까운 동물병원을 방문하시는 것을 권장 드립니다.</p>
								</li>
							</ul>
						</div>
					</div>
				</div>

				<div class="col-md-12 col-lg-4 mob-pedding wow fadeIn">
					<div class="video-block top-minouse">
						<a href="https://youtu.be/5dBdlACQVxg?autoplay=1&rel=0" data-toggle="lightbox" data-gallery="youtubevideos">
							<img src="/images/videos/ticks-video.jpg" class="img-fluid" alt="Ticks Video">
						</a>
						<a href="#" class="playbutton" title="Watch Video"></a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
</body>
</html>
		
