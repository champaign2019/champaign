<?session_start();?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/logingCheck.php" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/themeHtml.php" ?>
</head>
<body>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
	<section class="inner-header">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-12  col-lg-6">
					<div class="header-text-inner">
						<h1 class="anim"><span class="titleanimation-bottom">Get Bravecto</span></h1>
						<p class="wow fadeInUp">Enter a suburb to find a stockist</p>
						<form class="find-stockist-form" onsubmit="searchStores(); return false;">
							<input name="zipcode" placeholder="suburb" type="text" id="input_stockist_search">
							<button class="arror-btn" type="submit"></button>
						</form>
					</div>
				</div>
				<div class="col-12  col-lg-6 getbravecto-header">
					<figure class="d-flex">
						<img src="/images/stockists/get_bravecto.png" alt="Facts About Ticks"
							 class=" img-fluid wow fadeInUp">
					</figure>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div id="map" class="donly"></div>
	</section>

	<section id="section_results">
		<div class="container-1">
			<div class="map-search-loading text-center p-4"><h3>Searching...</h3></div>
			<div class="map-search">
				<h1 class="anim"><span class="titleanimation-bottom">Results for</span></h1>
				<h5><span class="search-keyword titleanimation-bottom search-text"></span></h5>
				<div class="row">
					<div class="col-12">
						<p class="hero-text wow fadeInUp title"></p>
					</div>
				</div>
				<div class="row results"></div>

			</div>
		</div>
	</section>

	<section class="alt-color">
		<div class="contact-cta">
			<h1 class="anim"><span class="titleanimation-bottom">Still have questions?</span></h1>
			<div class="contact-wrap">
				<ul>
					<li class="wow fadeInUp">
						<a href="tel:1800230833" title="call us">
							<small>BRAVECTO HOTLINE</small>
							<b>1800 230 833</b>
						</a>
					</li>
					<li class="maillink wow fadeInUp">
						<a href="mailto:bravectoaustralia@merck.com"
						   title="send us email"><span>bravectoaustralia@merck.com</span></a>
					</li>
				</ul>

				<div class="clearfix"></div>
			</div>

		</div>
	</section>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>

	@section scripts {
		<script src="/js/stockists.js"></script>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD_QVI2hMlj_pWD9KeOE1NsSF1U2K-79iQ&libraries=places,geometry&callback=initMap"></script>
	}
</body>
</html>
		
