<?session_start();?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/logingCheck.php" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/themeHtml.php" ?>
</head>
<body>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
	<section class="inner-header">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-12  col-lg-6 getbravecto-header m_ver">
					<figure class="d-flex">
						<img src="/images/stockists/get_bravecto.png" alt="Facts About Ticks" class=" img-fluid wow fadeInUp">
					</figure>
				</div>
				<div class="col-12  col-lg-6">
					<div class="header-text-inner">
						<h1 class="anim"><span class="titleanimation-bottom">브라벡토 처방을 원하시나요?</span></h1>
						<p class="wow fadeInUp">거주지역(시/군/구)을 검색하여 브라벡토를 처방하고 있는 인근 동물병원을 찾아보세요</p>
						<form class="find-stockist-form" onsubmit="getStockList(); return false;">
							<input name="zipcode" placeholder="시/군/구 검색" type="text" id="input_stockist_search">
							<button class="arror-btn" type="submit"></button>
						</form>
					</div>
				</div>
				<div class="col-12  col-lg-6 getbravecto-header pc_ver">
					<figure class="d-flex">
						<img src="/images/stockists/get_bravecto.png" alt="Facts About Ticks" class=" img-fluid wow fadeInUp">
					</figure>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div id="map" class="donly"></div>
	</section>

	<section id="section_results" style="display:none;">
		<div class="container-1">
			<div class="map-search-loading text-center p-4"><h3>Searching...</h3></div>
			<div class="map-search">
				<h1 class="anim"><span class="titleanimation-bottom">Results for</span></h1>
				<h5><span class="search-keyword titleanimation-bottom search-text"></span></h5>
				<div class="row">
					<div class="col-12">
						<p class="hero-text wow fadeInUp title"></p>
					</div>
				</div>
				<div class="row results"></div>

			</div>
		</div>
	</section>

	<section class="alt-color">
		<div class="contact-cta">
			<h1 class="anim"><span class="titleanimation-bottom">궁금하신 점이 있으신가요?</span></h1>
			<div class="contact-wrap">
				<ul>
					<li class="maillink wow fadeInUp">
						<a href="mailto:bravectoaustralia@merck.com"
						   title="send us email">
						   <small>이메일 문의 </small>
						   <span>bravectokorea@merck.com</span></a>
					</li>
				</ul>

				<div class="clearfix"></div>
			</div>

		</div>
	</section>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>

	@section scripts {
		<script src="/js/stockists.js"></script>
		<!--
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD_QVI2hMlj_pWD9KeOE1NsSF1U2K-79iQ&libraries=places,geometry&callback=initMap"></script>
		-->
		<script type="text/javascript" src="/js/jQ.js"></script>
		<script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=a3af3c34ec734976cafac7f23d4c9c6a"></script>
		<script>
			var infowindows=[];
			var myKakaoMap;
			var markers = new Object();
			var stockListData;
			var initializeMap;
			(function() {
				$.ajax({
					method : 'post'
					, url : 'ajax_stockListData.php'
					, dataType : 'json'
					, async : false
					, success : function(result) {
						stockListData = result;
					}
				});
			})();
			var initializeMap;
			(initializeMap = function() {
				var loc = eval('('+stockListData[0].relation_url+')');
				loc = new kakao.maps.LatLng(loc.lat, loc.lng);

				var mapContainer = document.getElementById('map');
				var mapOption = { 
					center: loc
					, level: 7
				};

				 myKakaoMap = new kakao.maps.Map(mapContainer, mapOption);

				 stockListData.forEach(function(el) {
					var loc = eval('('+el.relation_url+')');
					loc = new kakao.maps.LatLng(loc.lat, loc.lng);

					var markerImageOptions = new kakao.maps.MarkerImage(
						'/images/icons/map-marker.svg'
						, new kakao.maps.Size(64, 69)
						, {offset: new kakao.maps.Point(27, 69)}
					);
					var marker = new kakao.maps.Marker({
						position: loc
						, image : markerImageOptions
					});
					marker.setMap(myKakaoMap);
					var contentString = 
						'<div class="infowindow-content" id="popupName"><div class="top"><img src="/images/logo.svg" /></div>' +
						'<dl><dt>업체명</dt><dd>비젠소프트</dd></dl>' +	
						'<dl class="bg01"><dt>주소</dt><dd>'+ el.addr0 +'</dd></dl>' +
						'<dl class="bg02"><dt>연락처</dt><dd><a href="tel:02-338-4610">02-338-4610</a></dd></dl>' +	
						'<p class="img_box"> <img class="dog" src="/images/dog_icon.png" /> <img class="cat" src="/images/cat_icon.png" /></p>' +	
						//'<dl class="bg01"><dt>지번</dt><dd>'+ el.title +'</dd></dl>' +
						// '<ul><li class="bg01"><a href="https://www.google.com/maps/search/?api=1&query='+loc.getLat()+','+loc.getLng()+'" target="_blank"><i><img src="/images/map_icon.png" /></i>지도보기</a></li><li><a href="#" data-class="mapConfirm">확인</a></li></ul>'
						'<ul><li class="bg01"><a href="https://map.kakao.com/link/search/'+el.addr0+'" target="_blank"><i><img src="/images/map_icon.png" /></i>지도보기</a></li><li><a href="javascript:;" data-class="mapConfirm">확인</a></li></ul>'
					var iwPosition = loc;
					var infowindow = new kakao.maps.InfoWindow({
						position : iwPosition
						,	content : contentString
						,	removable : true
					});
					infowindows.push(infowindow);

					infowindow.close();
					
					
					kakao.maps.event.addListener(marker, 'click', function() {
						//$(".infowindow-content").parent().parent().hide();
						
							
						infowindows.forEach(function(el){
							el.close();
						});
						infowindow.open(myKakaoMap, marker);
						$("[data-class=mapConfirm]").off().click(function(){
							infowindow.close();
						})
					});

					marker['infowindow'] = infowindow;

					markers[el.no] = marker;
				});
			})();

			var moveMap = function(loc, move) {
				move = typeof move === 'undefined' ? true : false;
				loc = new kakao.maps.LatLng(loc.getLat(), loc.getLng());
				myKakaoMap.setCenter(loc);
				if (move) {
					$('html, body').animate({
						'scrollTop': $('#map').offset().top
					}, 500);
				}
			};

			var getStockList = function() {
				var search_keyword = $('#input_stockist_search').val();
				$.ajax({
					method : 'post'
					, url : 'ajax_stockList.php'
					, beforeSend : function() {
						$('.map-search-loading').show();
					}
					, data : {
						sval : search_keyword
					}
					, success : function(result) {
						result = result.trim();
						$('#section_results').show();
						$('.search-keyword').text( search_keyword );
						if ( jQ.isEmpty(result) ) {
							$('.hero-text').text( 'Sorry. We\'re unable to find any stockists matching your search.' );
							$('.row.results').html( '' );
						}
						else {
							result = $(result);
							var no = $(result).filter('.col-12.col-md-6.col-lg-4.col-xl-3').eq(0).data('no');
							moveMap(markers[no].getPosition(), false);
							$('.hero-text').text( 'Showing ' + $(result).filter('.col-12.col-md-6.col-lg-4.col-xl-3').length + ' stockists' );
							$('.row.results').html( result );
						}
						scrollToResults(150);
						$('.map-search-loading').hide();
					}
				});
			};
		</script>
	}


	<script>
	function close_infowin(){
		infowindows.forEach(function(el){
							el.close();
		});
	}
	</script>
</body>
</html>
		
