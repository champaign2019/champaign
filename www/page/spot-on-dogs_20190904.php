<?session_start();?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/logingCheck.php" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/themeHtml.php" ?>
</head>
<body>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
	<div>
		<div id="sync1" class="owl-carousel owl-theme big-product-carousel">
			<div class="item">
				<div class="desktop-only"><img src="/images/products/spot-on-dogs/header-1.jpg" class="w-100" alt="Bravecto Spot On For Dogs"></div>
				<div class="mobile-only"><img src="/images/products/spot-on-dogs/header-1-mobile.jpg" class="w-100" alt="Bravecto Spot On For Dogs"></div>
			</div>
			<div class="item">
				<div class="desktop-only"><img src="/images/products/spot-on-dogs/header-2.jpg" class="w-100" alt="Bravecto Spot On For Dogs"></div>
				<div class="mobile-only"><img src="/images/products/spot-on-dogs/header-2-mobile.jpg" class="w-100" alt="Bravecto Spot On For Dogs"></div>
			</div>
			<div class="item">
				<div class="desktop-only"><img src="/images/products/spot-on-dogs/header-3.jpg" class="w-100" alt="Bravecto Spot On For Dogs"></div>
				<div class="mobile-only"><img src="/images/products/spot-on-dogs/header-3-mobile.jpg" class="w-100" alt="Bravecto Spot On For Dogs"></div>
			</div>
			<div class="item">
				<div class="desktop-only"><img src="/images/products/spot-on-dogs/header-4.jpg" class="w-100" alt="Bravecto Spot On For Dogs"></div>
				<div class="mobile-only"><img src="/images/products/spot-on-dogs/header-4-mobile.jpg" class="w-100" alt="Bravecto Spot On For Dogs"></div>
			</div>
			<div class="item">
				<div class="desktop-only"><img src="/images/products/spot-on-dogs/header-5.jpg" class="w-100" alt="Bravecto Spot On For Dogs"></div>
				<div class="mobile-only"><img src="/images/products/spot-on-dogs/header-5-mobile.jpg" class="w-100" alt="Bravecto Spot On For Dogs"></div>
			</div>
		</div>
	</div>

	<div class="alt-color">
		<div class="container-1">
			<div id="sync2" class="owl-carousel owl-theme dogs">
				<div class="item yellow-active"><span><i></i>2 - 4.5 kg</span></div>
				<div class="item orange-active"><span><i></i>> 4.5 - 10 kg</span></div>
				<div class="item green-active"><span><i></i>> 10 - 20 kg</span></div>
				<div class="item blue-active"><span><i></i>> 20 - 40 kg</span></div>
				<div class="item purpal-active"><span><i></i>> 40 - 56 kg</span></div>
			</div>
		</div>
	</div>

	<section class="product-section">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-md-12 col-lg-10">
					<div class="product-details-top">
						<h1 class="wow fadeInUp">강아지용 브라벡토 스팟온</h1>
						<h4 class="wow fadeInUp">더욱 쉬워진 브라벡토 스팟온으로 당신의 반려견을 보호하세요</h4>
						<p class="wow fadeInUp">브라벡토 스팟온만의 TWIST’N’USE 기술로 더욱 손쉽게 투여가 가능합니다.</p>
						<ul class="wow fadeInUp">
							<li>진드기(tick and mite) 및 벼룩으로 부터 12주간 보호합니다</li>
							<li>귀진드기, 옴진드기, 모낭충에도 효과가 있습니다</li>
							<li>더욱 쉬워졌습니다. 뚜껑을 뗄 필요도 없이, 비틀고 바르면 됩니다.</li>
							<li>8주령 이상 및 2kg 이상의 강아지 뿐만 아니라 임신 및 수유중인 강아지에도 안심하고 사용할 수 있습니다</li>
						</ul>
					</div>
				</div>
				<div class="col-md-12 col-lg-8 wow fadeInUp">
					<div class="product-details-top pt-0 pb-180">
						<ul class="nav nav-tabs" id="proTab" role="tablist">
							<li class="nav-item"> <a class="nav-link active" id="longlastng-tab" data-toggle="tab" href="#loglasting" role="tab" aria-selected="true" title="Long Lating">편리한 </a> </li>
							<li class="nav-item"> <a class="nav-link" id="effective-tab" data-toggle="tab" href="#effective" role="tab" aria-selected="false" title="Efective">작용</a> </li>
							<li class="nav-item"> <a class="nav-link" id="hasslefree-tab" data-toggle="tab" href="#hasslefree" role="tab" aria-selected="false" title="Easy">맛있는 </a> </li>
							<li class="nav-item"> <a class="nav-link" id="global-tab" data-toggle="tab" href="#global" role="tab" aria-selected="false" title="Global">세계적인</a> </li>
						</ul>
						<div class="tab-content" id="proTabContent">
							<div class="tab-pane fade show active" id="loglasting" role="tabpanel">
								<h4>편리한</h4>
								<p>브라벡토는 1회 사용만으로 12주간 외부기생충으로 부터 반려견을 보호합니다. 
								매 계절 1회 사용으로 기억하기 쉽고, 외부기생충 예방을 놓칠 일이 적어집니다.</p>

							</div>
							<div class="tab-pane fade" id="effective" role="tabpanel" aria-labelledby="effective-tab">
								<h4>빠르고 오래가는</h4>
								<p>
									브라벡토의 주성분 플루랄라너는 사용 4시간 이내에 작용하기 시작하여, 12시간 이내에
									진드기의 100%를 사멸합니다. 또한 플루랄라너의 대부분이 비활성 상태로 저장되어 있다가 12주간 서서히 방출됩니다.
								</p>
							</div>
							<div class="tab-pane fade" id="hasslefree" role="tabpanel" aria-labelledby="hasslefree-tab">
								<h4>안전한</h4>
								<p>
									브라벡토는 8주령이 갓 지났거나 2kg가 조금 넘는 작은 체구의 강아지, 임신중 혹은 수유중인 반려견에서
									안심하고 사용할 수 있습니다. 또한 많은 종류의 의약품 사용이 불가능한 콜리종에서의 안전성도 검증되었습니다.
								</p>
							</div>
							<div class="tab-pane fade" id="global" role="tabpanel" aria-labelledby="global-tab">
								<h4>세계적인</h4>
								<p>브라벡토는 전 세계 85여개 국가에서 판매되며, 2019년 1억 도스 판매를 돌파하였습니다.
								엄청난 판매량으로 그 안전성이 검증된 브라벡토의 놀라운 효능을 경험해 보세요.</p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12 col-lg-4 mob-pedding wow fadeIn">
					<div class="video-block wow fadeInUp"> <img src="/images/products/spot-on-dogs/section-1.jpg" alt="Dog" class="w-100"></div>
				</div>
			</div>
		</div>
	</section>

	<section class="alt-color">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-md-12 col-lg-8">
					<div class="benefits benefits-noimg">
						<div class="benefits-list wow fadeInUp"> <a href="#" class="redbg-link">플루랄라너</a> </div>
						<div class="benefits-list extrta-mb">
							<h3 class="wow fadeInUp">브라벡토만의 혁신적인 성분, 플루랄라너</h3>
							<p class="wow fadeInUp">2014년 미국 FDA를 시작으로, 전 세계 85여개 국가에서 허가를 받은 브라벡토의 성분 플루랄라너는 
							혈액에 흡수됨과 동시에 대부분이 비활성화 상태로 저장되어 서서히 방출됩니다.
							12주나 되는 기간동안 그 효능의 대부분을 유지하여 반려견에게 문제가 되는 외부기생충을 
							더욱 간편하고 안전하게 예방할 수 있게 되었죠. </p>
						</div>
						<div class="benefits-list extrta-mb ">
							<h3 class="wow fadeInUp">방대한 임상 연구와 모니터링</h3>
							<p class="wow fadeInUp">브라벡토(플루랄라너)는 저희 MSD동물약품의 비전 ‘The Science of Healthier Animals’ 의
							놀라운 결과물입니다. 방대한 임상 연구와 모니터링을 바탕으로 반려동물들이 브라벡토(플루랄라너)의 
							강력한 효능과 지속성 및 높은 안전성을 경험할 수 있게 되었습니다.
						</div>
					</div>
				</div>
				<div class="col-md-12 col-lg-4 mob-pedding wow fadeIn">
					<div class="video-block top-minouse">
						<a href="https://youtu.be/4JHV4jE7pX4?autoplay=1&rel=0" data-toggle="lightbox" data-gallery="youtubevideos">
							<img src="/images/videos/spot-on-dogs-video.jpg" class="img-fluid" alt="Spot On Dogs Video">
						</a>
						<a href="#" class="playbutton" title="Watch Video"></a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-md-12 col-lg-8 wow fadeInUp">
					<div class="benefits benefits-noimg">
						<div class="benefits-list mb-0  wow fadeInUp">
							<h3 class="bigheading">브라벡토 스팟온 투여 방법</h3>
							<div class="steps-car owl-carousel owl-theme">
								<div class="item" data-dot="step 1">
									<p>브라벡토 스팟온의 안전포장지를 뜯습니다.</p>
									<figure><img src="/images/steps/step-1.jpg" class="img-fluid" alt="How to use Bravecto Spot-on for Dogs"></figure>
								</div>
								<div class="item" data-dot="step 2">
									<p>브라벡토 스팟온의 뚜껑을 위로 향하게 잡고, 비틀어 줍니다.</p>
									<p><strong>뚜껑은 떨어지지 않으니 무리하게 떼지 마십시오.</strong></p>
									<figure><img src="/images/steps/step-2.jpg" class="img-fluid" alt="How to use Bravecto Spot-on for Dogs"></figure>
								</div>
								<div class="item" data-dot="step 3">
									<p>반려견의 털을 갈라 피부가 노출되게 한 후 등쪽 피부선을 따라 브라벡토 스팟온을 바릅니다.</b></p>
									<figure><img src="/images/steps/step-3.jpg" class="img-fluid" alt="How to use Bravecto Spot-on for Dogs"></figure>
								</div>
								<div class="item" data-dot="step 4">
									<p>소형견은 두 군데, 중형견은 세 군데, 대형견은 네 군데에 나누어 발라주세요.</p>
									<figure><img id="for-dogs-step-4-image" src="/images/steps/step-dog-4.png" class="img-fluid" alt="How to use Bravecto Spot-on for Dogs"></figure>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12 col-lg-4 mob-pedding wow fadeIn">
					<div class="video-block top-minouse">
						<a href="https://youtu.be/iz7_X4AO9CQ?autoplay=1&rel=0" data-toggle="lightbox" data-gallery="youtubevideos">
							<img src="/images/videos/spot-on-dog-alt-video.jpg" class="img-fluid">
						</a>
						<a href="#" class="playbutton" title="Watch Video" onclick=""></a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="alt-color">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-md-12 col-lg-8 moile-last">
					<div class="benefits benefits-noimg">
						<div class="benefits-list wow fadeInUp">
							<h2>더 궁금하신 점이 있으신가요?</h2>
							<div class="accordion" id="faqs">
								<div class="faq-list wow fadeInUp">
									<a data-toggle="collapse" data-target="#f1" aria-expanded="true" aria-controls="f1" href="#">Where can I buy Bravecto?</a>
									<div id="f1" class="collapse show " data-parent="#faqs">
										<div class="answer">Bravecto is available from veterinarians and leading pet shops. </div>
									</div>
								</div>
								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f6" aria-expanded="true" aria-controls="f6" href="#">What will Bravecto protect my pet against?</a>
									<div id="f6" class="collapse" data-parent="#faqs">
										<div class="answer">
											Bravecto is available in a range of products that provide long-lasting protection against common
											parasites in dogs and cats with just one dose*: <br>

											<table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered">
												<tr>
													<th scope="row">Product* </th>
													<td><strong>Bravecto Spot-on for Dogs</strong></td>
												</tr>
												<tr>
													<th scope="row">Fleas</th>
													<td>
														<span class="correct">&nbsp;</span>
														6 months
													</td>
												</tr>
												<tr>
													<th scope="row">Paralysis Ticks</th>
													<td>
														<span class="correct">&nbsp;</span>
														6 months
													</td>
												</tr>
												<tr>
													<th scope="row">Brown Dog Ticks</th>
													<td>
														<span class="correct">&nbsp;</span>
														12 weeks
													</td>
												</tr>
											</table>
											<small>*Refer to product label for full claim details.</small>
										</div>
									</div>
								</div>
								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f2" aria-expanded="true" aria-controls="f2" href="#">How do I apply Bravecto Spot-on?</a>
									<div id="f2" class="collapse" data-parent="#faqs">
										<div class="answer clearfix">
											Bravecto Spot-on comes in a clever <strong>TWIST&acute;N&acute;USE</strong> tube with a non-removable cap, it is simple andconvenient to apply:<br>

											<img src="/images/products/how-to-apply.png" alt="How to apply Bravecto Spot-on Dogs" style="max-width: 500px; position: relative;" class="my-3" />

											Apply contents of the appropriate tube for your dog’s body weight to the application sites:

											<img src="/images/products/how-to-apply-dog.jpg" alt="How to apply Bravecto Spot-on Dogs" class="my-3" style="max-width: 500px; width:100%; position: relative;" />

										</div>
									</div>
								</div>
								<div class="faq-list wow fa deInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f3" aria-expanded="true" aria-controls="f3" href="#">What pack size does Bravecto Spot-on for Dogs come in?</a>
									<div id="f3" class="collapse" data-parent="#faqs">
										<div class="answer clearfix">
											Bravecto Spot-on for Dogs is available in single dose packs for dogs ranging from 2 – 56 kg. One dose
											provides 6 months protection against fleas and paralysis ticks and 12 weeks protection against
											brown dog ticks.
											<img class="my-2" src="/images/products/dog-pack-sizes.png" style="max-width: 500px; width:100%; position: relative;" />
										</div>
									</div>
								</div>
								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f4" aria-expanded="true" aria-controls="f4" href="#">Why does Bravecto Spot-on for Dogs last longer than Bravecto Chew? Is the dose very high/strong?</a>
									<div id="f4" class="collapse" data-parent="#faqs">
										<div class="answer">
											The reason Bravecto Spot-on for Dogs lasts longer is not due to the dose, since the dose in Bravecto
											Spot-on for Dogs and Bravecto Chew are identical. Bravecto Spot-on for Dogs has a unique
											formulation which leads to differences in absorption when Bravecto is applied onto the skin compared to
											when it is administered by mouth. It is this difference that accounts for the extended duration.
										</div>
									</div>
								</div>
								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f5" aria-expanded="true" aria-controls="f5" href="#">Can I use Bravecto Spot-on for Dogs in growing, large breed puppies?</a>
									<div id="f5" class="collapse" data-parent="#faqs">
										<div class="answer">MSD recommends the use of Bravecto Chew for rapidly growing, large breed puppies.</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f7" aria-expanded="true" aria-controls="f7" href="#">Why might long-lasting protection be better than monthly treatments?</a>
									<div id="f7" class="collapse" data-parent="#faqs">
										<div class="answer">
											86% of Australian pet owners say Bravecto is more convenient than a monthly treatment*. Gaps in protection can occur when flea and tick treatments are not administered at the correct treatment interval. Bravecto assists with treatment compliance for an extended period with just one dose, offering you peace of mind.
										</div>
										<small>*MSD Data on File. Outcomes Research Project 2017.</small>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f8" aria-expanded="true" aria-controls="f8" href="#">When should dogs start treatment with Bravecto Spot-on for Dogs?</a>
									<div id="f8" class="collapse" data-parent="#faqs">
										<div class="answer">
											Bravecto Spot-on for Dogs can be administered to dogs as early as 8 weeks of age. Dogs should
											weigh at least 2 kg.
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f9" aria-expanded="true" aria-controls="f9" href="#">My dog weighs over 56 kgs – how do I treat him/her?</a>
									<div id="f9" class="collapse" data-parent="#faqs">
										<div class="answer">
											For dogs above 56 kg body weight, use a combination of two pipettes that most closely matches the
											body weight.
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f10" aria-expanded="true" aria-controls="f10" href="#">When is it safe for me to pat/cuddle my dog after Bravecto Spot-on application?</a>
									<div id="f10" class="collapse" data-parent="#faqs">
										<div class="answer">You should wait until the application area is dry before touching the area.</div>
									</div>
								</div>


								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f11" aria-expanded="true" aria-controls="f11" href="#">How soon after using Bravecto Spot-on can my dog swim or be bathed?</a>
									<div id="f11" class="collapse" data-parent="#faqs">
										<div class="answer">
											While the product should be dry sooner, it is not recommended to wash or allow the dog to become
											immersed or swim in water within 3 days after treatment.
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f12" aria-expanded="true" aria-controls="f12" href="#">
										My dog is on medication for another condition right now. Is it safe to give
										Bravecto Spot-on for Dogs?
									</a>
									<div id="f12" class="collapse" data-parent="#faqs">
										<div class="answer">
											It is always best to discuss all of your dog's treatments with your veterinarian as they are your dog's
											health care expert. Studies with Bravecto Spot-on for Dogs have shown no interactions with
											routinely used veterinary medicinal products.
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f13" aria-expanded="true" aria-controls="f13" href="#">How does Bravecto Spot-on for Dogs kill fleas and ticks?</a>
									<div id="f13" class="collapse" data-parent="#faqs">
										<div class="answer">
											After you treat your pet with Bravecto Spot-on, the active ingredient fluralaner is absorbed into the
											bloodstream and quickly reaches tissue fluids just under your pet’s skin. When fleas and ticks feed,
											they take in fluralaner and die. Bravecto Spot-on for Dogs kills pre-existing and new infestations of
											paralysis ticks for 6 months and treats and controls brown dog ticks for 12 weeks. It also controls
											fleas on dogs within 8 hours of administration, and subsequently controls flea re-infestations for 6
											months. The flea life cycle can be broken by this rapid onset of action and long-lasting efficacy.
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f14" aria-expanded="true" aria-controls="f14" href="#">Will Bravecto kill ticks and fleas that are already on my dog?</a>
									<div id="f14" class="collapse" data-parent="#faqs">
										<div class="answer">Yes. </div>
									</div>
								</div>


								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f15" aria-expanded="true" aria-controls="f15" href="#">Can dead and attached ticks be found on treated dogs?</a>
									<div id="f15" class="collapse" data-parent="#faqs">
										<div class="answer">
											Yes. Due to the way Bravecto works, dead attached ticks can be still seen on dogs following
											treatment. Dead ticks can be easily removed as opposed to live ticks which take some force to
											dislodge from the skin of the dog.
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f16" aria-expanded="true" aria-controls="f16" href="#">
										Can I use Bravecto Spot-on for Dogs all year-round?
									</a>
									<div id="f16" class="collapse" data-parent="#faqs">
										<div class="answer">
											Yes, it is safe to use Bravecto all year-round. Fleas can be a year-round problem indoors and
											outdoors. Applying Bravecto Spot-on for Dogs every 6 months ensures your dog will be protected
											from fleas throughout the year with just two doses.
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f17" aria-expanded="true" aria-controls="f17" href="#">Do I really need to treat for fleas all year round? </a>
									<div id="f17" class="collapse" data-parent="#faqs">
										<div class="answer">
											There are very few places in Australia where there are no fleas. In warmer climates, fleas are
											present all year round while in more mild climates fleas are present mainly from late spring through
											to late autumn and even longer thanks to central heating. Bravecto Spot-on for Dogs should be
											administered every 6 months, all year round to avoid giving flea populations a chance to build up in
											your home.
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f18" aria-expanded="true" aria-controls="f18" href="#">
										Is it necessary to use additional insecticides to control flea stages in the
										environment?
									</a>
									<div id="f18" class="collapse" data-parent="#faqs">
										<div class="answer">
											No. Bravecto Spot-on for Dogs kills fleas and prevents new flea infestations for 6 months. As the
											majority of a flea population developing in the environment will emerge as adult fleas over this time,
											Bravecto will effectively control the entire flea population.
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f19" aria-expanded="true" aria-controls="f19" href="#">Can Bravecto Spot-on for Dogs be used in breeding/pregnant/lactating dogs? </a>
									<div id="f19" class="collapse" data-parent="#faqs">
										<div class="answer">Yes. Bravecto Spot-on for Dogs is approved for use in breeding, pregnant and lactating dogs. </div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f20" aria-expanded="true" aria-controls="f20" href="#">Why does my dog scratch even more on the first day of Bravecto treatment?</a>
									<div id="f20" class="collapse" data-parent="#faqs">
										<div class="answer">
											When fleas are in the process of dying their movements become uncoordinated. This may cause a
											skin sensation which can result in increased scratching of the dog. However, this phenomenon is
											quickly resolved once the fleas are dead.
										</div>
									</div>
								</div>

								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f21" aria-expanded="true" aria-controls="f21" href="#">Why do I see fleas after I have administered Bravecto?</a>
									<div id="f21" class="collapse" data-parent="#faqs">
										<div class="answer">
											Fleas can continually re-infest treated dogs – either from juvenile flea life stages that have just
											matured to adults in the household or from fleas that jump onto the pet when outside or visiting
											other homes. Bravecto will quickly kill these fleas. Also, as fleas die, they can move from the skin
											surface where they feed, to the tips of the hairs in the coat, making them more visible to the human
											eye.
										</div>
									</div>
								</div>
								<div class="faq-list wow fadeInUp">
									<a class="collapsed" data-toggle="collapse" data-target="#f24" aria-expanded="true" aria-controls="f24" href="#">Where can I get more information about Bravecto’s claims?</a>
									<div id="f24" class="collapse" data-parent="#faqs">
										<div class="answer">
											All <a class="inlinelink" href="https://portal.apvma.gov.au/pubcris;jsessionid=AjAebiai95var7phRx1L2QBS?p_auth=RBZ2QvsL&p_p_id=pubcrisportlet_WAR_pubcrisportlet&p_p_lifecycle=1&p_p_state=normal&p_p_mode=view&p_p_col_id=column-1&p_p_col_pos=2&p_p_col_count=4&_pubcrisportlet_WAR_pubcrisportlet_javax.portlet.action=search" title="product labels for Bravecto" target="_blank">product labels for Bravecto</a> can be found on the APVMA (Australian Pesticides and Veterinary
											Medicines Authority) Pubcris database.

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12 col-lg-4 mobile-first">
					<div class="where-to-get top-minouse wow fadeInUp">
						<h2>브라벡토 구매를 원하시나요?</h2>
						<p>당신의 반려견에게 가장 알맞은 제품에 대해 수의사와 상담하세요. 
						구입을 원하신다면 거주지역을 아래 검색창에 입력하세요.</p>
						<form action="/page/stockists.php">
							<input name="zipcode" type="text" placeholder="suburb">
							<button class="arror-btn" type="submit"></button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
</body>
</html>
		
