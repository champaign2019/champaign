<?session_start();?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/logingCheck.php" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/themeHtml.php" ?>
</head>
<body>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
	<section class="inner-header">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-12  col-lg-6">
					<div class="header-text-inner">
						<h1 class="anim"><span class="titleanimation-bottom">Facts about fleas</span></h1>
						<p class="wow fadeInUp">Learn more about fleas and how to prevent infestations.</p>
					</div>
				</div>
				<div class="col-12  col-lg-6 ">
					<figure class="d-flex"><img src="/images/parasites/fleas-header.png" alt="Facts About Fleas" class="img-fluid wow fadeInUp" /></figure>
				</div>
			</div>
		</div>
	</section>

	<section class="facts-ticks">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-12 col-lg-6">
					<div class="facts-list facts-light">
						<h4 class="anim"><span class="titleanimation-bottom">The facts about fleas</span></h4>
						<ul>
							<li>
								<h6 class="anim"><span class="titleanimation-bottom">A flea starts feeding on blood within 5 minutes</span></h6>
								<p class="wow fadeInUp">When a flea jumps onto a pet, it will start feeding within 5 minutes and may suck blood for up to 2&frac12; hours.</p>
							</li>
							<li>
								<h6 class="anim"><span class="titleanimation-bottom">A single flea lays 40 to 50 eggs a day</span></h6>
								<p class="wow fadeInUp">Each female flea can produce 40 to 50 eggs a day. That's up to 2,000 in a lifetime, infesting your home and your pet's environment.</p>
							</li>
							<li>
								<h6 class="anim"><span class="titleanimation-bottom">An adult flea can live for 2 months</span></h6>
								<p class="wow fadeInUp">A single flea can live on a pet for almost 2 months providing ongoing irritation.</p>
							</li>
							<li>
								<h6 class="anim"><span class="titleanimation-bottom">Fleas are stubborn and hard to remove</span></h6>
								<p class="wow fadeInUp">Flea larvae burrow deep into bedding and carpet - thorough, regular vacuuming and cleaning of pet’s bedding is recommended.</p>
							</li>
							<li>
								<h6 class="anim"><span class="titleanimation-bottom">8 weeks or more to remove flea populations in the environment</span></h6>
								<p class="wowo fadeInUp">Once a flea infestation has been established it can take up to 8 or more weeks to eradicate it.</p>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-12 col-lg-6">
					<div class="facts-list extrta-pad">
						<h4 class="anim"><span class="titleanimation-bottom">Tips for preventing fleas</span></h4>
						<ul>
							<li>
								<h6 class="anim"><span class="titleanimation-bottom">If you've had a flea infestation in your home, you'll never want to have one again.</span></h6>
								<p class="wow fadeInUp">To stop repeat infestations, you need to break the flea life cycle once 
	and for all.</p>
							</li>
							<li>
								<h6 class="anim"><span class="titleanimation-bottom">Use Bravecto</span></h6>
								<p class="wow fadeInUp">Bravecto provides long-lasting protection against fleas and kills newly emerged adult fleas before they lay eggs.</p>
							</li>
							<li>
								<h6 class="anim"><span class="titleanimation-bottom">Target flea hideouts</span></h6>
								<p class="wow fadeInUp">Wash all your pet’s bedding at a high temperature. Vacuum your carpets and furniture to remove eggs, larvae and pupae that may be present and discard the vacuum cleaner bag.</p>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="alt-color identify">
		<div class="row no-gutters">
			<div class="col-12">
				<h3 class="wow fadeInUp text-center">Identifying fleas</h3>
				<div class="fleestext">
					<p class="wow fadeInUp">
						The world is host to over 2,000 species of flea, and they’re a problem almost everywhere. Most common is <em>Ctenocephalides felis</em> the "cat flea". Despite its name, the cat flea affects both dogs and cats, and wildlife such as possums - and can be unpleasant for pet owners, too. Most pets will pick up fleas often throughout their lifetime. Even pets that never go outdoors are at risk from fleas that can find their way into their home.<br>
						<br>
						<br>
						<img src="/images/parasites/fleas/flees.png" alt="Fleas" class="wow fadeInUp">
					</p>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-md-12 col-lg-8 wow fadeInUp">
					<div class="benefits mb-more">
						<div class="benefits-list mb-0  wow fadeInUp">
							<h3 class="bigheading anim"><span class="titleanimation-bottom">Life cycle of a flea</span></h3>
							<p class="wow fadeInUp">Click through all stages of the Flea life cycle to learn more.</p>
							<div class="steps-car owl-carousel owl-theme">
								<div class="item" data-dot="1. EGGS">
									<p>
										<strong>STAGE 1  <small>&lt; 10 days</small></strong><br>
										<br>

										Female fleas lay eggs whilst on your dog or cat and can produce 40 to 50 eggs a day, leaving them to fall off your pet and all over your home.
									</p>
								</div>
								<div class="item" data-dot="2. LARVAE">
									<p>
										<strong>STAGE 2  <small> = 10 days</small></strong><br>
										<br>
										Each egg can hatch  into larvae and live in carpets, fabrics and  small crevices in your pet's enviroment. They feed on adult flea faeces and organic material they find until they're ready to develop into pupae.
									</p>
								</div>
								<div class="item" data-dot="3. PUPAE">
									<p>
										<strong>STAGE 3  <small>11 days (but can be months)</small></strong><br>
										<br>
										Pupae develop into adult fleas inside tiny cocoons. Here they can stay dormant for long periods and come out in response to heat, carbon dioxide and  movement.
									</p>
								</div>
								<div class="item" data-dot="4. ADULT">
									<p>
										<strong>STAGE 4 <small> &lt; 3 months</small></strong><br>
										<br>
										Once on your pet, adult fleas spend the rest of their lives there, feeding and laying eggs that  infest your home within a matter of days
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12 col-lg-4 mob-pedding wow fadeIn">
					<div class="video-block">
						<div class="step-images-slider video-slider owl-carousel owl-theme">
							<div class="video-item p-5"><img src="/images/parasites/fleas/steps/step1@2x.png" class="w-100" alt="Fleas Eggs"> </div>
							<div class="video-item p-5"><img src="/images/parasites/fleas/steps/step2@2x.png" class="w-100" alt="Fleas Eggs"> </div>
							<div class="video-item p-5"><img src="/images/parasites/fleas/steps/step3@2x.png" class="w-100" alt="Fleas Eggs"> </div>
							<div class="video-item p-5"><img src="/images/parasites/fleas/steps/step4@2x.png" class="w-100" alt="Fleas Eggs"> </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="alt-color">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-md-12 col-lg-8">
					<div class="benefits benefits-noimg">
						<div class="benefits-list extrta-mb">
							<h4 class="anim"><span class="titleanimation-bottom">How to check for fleas</span></h4>
							<ul>
								<li>
									<h6 class="anim"><span class="titleanimation-bottom">Step 1</span></h6>
									<p class="wow fadeInUp">Regularly checking for fleas should be part of routine pet care.</p>
								</li>
								<li>
									<h6 class="anim"><span class="titleanimation-bottom">Step 2</span></h6>
									<p class="wow fadeInUp">Try parting the coat near the base of the tail using your hands or a flea comb and look for movement. Most fleas grow to about the size of a pinhead and will move or jump when disturbed.</p>
								</li>
								<li>
									<h6 class="anim"><span class="titleanimation-bottom">Step 3</span></h6>
									<p class="wow fadeInUp">Even if you don’t see any fleas, have a look for dark, pepper-like particles on the surface of your pet’s skin and coat. These may be ‘flea dirt’, which are flea droppings.</p>
								</li>
								<li>
									<h6 class="anim"><span class="titleanimation-bottom">Step 4</span></h6>
									<p class="wow fadeInUp">You can easily confirm this by dabbing some of this material with a wet paper towel or cotton ball. If you see dark reddish brown or orange swirls, this is flea dirt and confirms that the pet has fleas. If you suspect fleas are present, talk to your vet about treatment with Bravecto.</p>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-12 col-lg-4 mob-pedding wow fadeIn">
					<div class="video-block top-minouse">
						<a href="https://youtu.be/jjft9jqHFPM?autoplay=1&rel=0" data-toggle="lightbox" data-gallery="youtubevideos">
							<img src="/images/videos/fleas-video.jpg" class="img-fluid" alt="Fleas Video">
						</a>
						<a href="#" class="playbutton" title="Watch Video"></a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
</body>
</html>
		
