<?session_start();?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/logingCheck.php" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/themeHtml.php" ?>
</head>
<body>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
	<section class="contactus">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-12 col-lg-6 align-self-center">
					<div class="contact-wrap">
						<h1 class="anim"><span class="titleanimation-bottom">문의하기</span></h1>
						<h6 class="wow fadeInUp">
							원하시는 정보를 찾기 어려우신가요?<br/>
							아래 이메일로 궁금하신 점을 문의해 주세요
						</h6>

						<ul>
							<li class="maillink wow fadeInUp">
								<a href="mailto:bravectoaustralia@merck.com" title="send us email">
								<small>이메일 문의</small>
								<span>bravectokorea@merck.com</span>
								</a>
							</li>
						</ul>

					</div>

				</div>
				<div class="col-12 col-lg-6 text-center">

					<img src="/images/contact-us/contact_us.png" class="img-fluid" alt="Contact us">
				</div>

			</div>

		</div>

	</section>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
</body>
</html>
		
