<?session_start();?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/logingCheck.php" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/themeHtml.php" ?>
</head>
<body>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
	<section class="inner-header">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-12  col-lg-6">
					<div class="header-text-inner">
						<h1 class="anim"><span class="titleanimation-bottom">뛰어야 벼룩</span></h1>
						<p class="wow fadeInUp">한 번 감염되면 끊기가 굉장히 힘들어 수많은 반려동물 및 가족들을 괴롭히는 벼룩. 12주간 지속되는 브라벡토 앞에서는 뛰어야 벼룩이죠</p>
					</div>
				</div>
				<div class="col-12  col-lg-6 ">
					<figure class="d-flex"><img src="/images/parasites/dog-meet-the-bad-guys.png" alt="Meet the bad guys" class=" img-fluid wow fadeInUp"></figure>
				</div>
			</div>
		</div>
	</section>

	<section class="col-3-section">
		<div class="container-1">

			<div class="alternet-list alternet-list-last">
				<div class="row no-gutters">
					<div class="col-md-12 col-lg-6"><img src="/images/parasites/parasites_img.jpg" class="w-100" alt="Dirt on Ticks"></div>
					<div class="col-md-12 col-lg-6 align-self-end">
						<div class="text-block alternet-text-block alternet-text-block-bottom">
							<h2 class="anim full-visible"><span class="titleanimation-bottom">벼룩의 위험성</span></h2>
							<p class="wow fadeInUp">
								벼룩은 전 세계적으로 약 2000종이 존재하며, 큰 문제가 되고 있습니다. 외부에서 감염된 벼룩은 집안 곳곳에 퍼져 번식하며, 반려동물 뿐만 아니라 사람에게도 문제를 일으킵니다. 벼룩의 번식력은 굉장히 뛰어나 최대 3개월에 이르는 생활환을 끊지 않으면 순식간에 다시 발생합니다. 12주간 지속되는 브라벡토는 벼룩의 생활환을 완전히 끝낼 수 있습니다.
							</p>
						</div>
					</div>
				</div>
			</div>

		</div>
	</section>
	

	<section>
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-md-12 col-lg-8 wow fadeInUp">
					<div class="benefits mb-more">
						<div class="benefits-list mb-0  wow fadeInUp">
							<h3 class="bigheading anim"><span class="titleanimation-bottom">벼룩의 생활환</span></h3>
							<p class="wow fadeInUp">클릭하여 작은소참진드기의 각 단계별 특징을 알아보세요</p>
							<div class="steps-car owl-carousel owl-theme wow fadeInUp">
								<div class="item" data-dot="1. 충란(10일 이내)">
									<p>벼룩은 반려동물에 붙어 하루 40-50여개의 알을 낳으며, 반려동물이 움직이며 알을 집안 구석구석 퍼트립니다.</p>
								</div>
								<div class="item" data-dot="2. 유충(10일)">
									<p>
										알에서 부화한 유충은 침구류, 옷 등에서 성충의 분변 및 각종 유기물을 섭취하며 성장합니다.
									</p>
								</div>
								<div class="item" data-dot="3. 번데기(11일 이상)">
									<p>
										번데기 속에서 벼룩은 성충으로 자라며 오랜 기간 살아남을 수 있습니다. 번데기는 온기, 이산화탄소, 움직임 등에 반응하여 밖으로 나옵니다.
									</p>
								</div>
								<div class="item" data-dot="4. 성충(3개월 이내)">
									<p>
										한 번 반려동물에 붙은 벼룩 성충은 일생동안 수많은 알을 낳으며 굉장히 빠른 속도로 번식합니다.
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			   
				<div class="col-md-12 col-lg-4 mob-pedding wow fadeIn">
					<div class="video-block">
						<div class="step-images-slider video-slider owl-carousel owl-theme ">
							<div class="video-item"><img src="/images/parasites/ticks/steps/step@2x.png" class="w-100" alt="Paralysis Tick"> </div>
							<div class="video-item"><img src="/images/parasites/ticks/steps/step2@2x.png" class="w-100" alt="Paralysis Tick"> </div>
							<div class="video-item"><img src="/images/parasites/ticks/steps/step3and4@2x.png" class="w-100" alt="Paralysis Tick"> </div>
							<div class="video-item"><img src="/images/parasites/ticks/steps/step3and4@2x.png" class="w-100" alt="Paralysis Tick"> </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="alt-color">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-md-12 col-lg-8">
					<div class="benefits benefits-noimg">
						<div class="benefits-list extrta-mb">
							<h4 class="anim"><span class="titleanimation-bottom">벼룩 확인법</span></h4>
							<ul>
								<li>
									<h6 class="anim"><span class="titleanimation-bottom">1단계</span></h6>
									<p class="wow fadeInUp">주기적인 벼룩 검사를 일상화 하세요.</p>
								</li>
								<li>
									<h6 class="anim"><span class="titleanimation-bottom">2단계</span></h6>
									<p class="wow fadeInUp">꼬리에서부터 손으로 반려동물의 털을 쓸어 올리며 움직임을 관찰하세요. 벼룩은 방해를 받으면 뛰어 오르며, 약 2-4mm의 크기입니다.</p>
								</li>
								<li>
									<h6 class="anim"><span class="titleanimation-bottom">3단계</span></h6>
									<p class="wow fadeInUp">위에서 벼룩을 찾지 못하여도 반려동물의 털에 검은 후춧가루같은 점이 있는지 확인하세요. 이는 벼룩의 분변일 수 있습니다.</p>
								</li>
								<li>
									<h6 class="anim"><span class="titleanimation-bottom">4단계</span></h6>
									<p class="wow fadeInUp">이러한 점을 젖은 헝겊 혹은 화장지로 닦아보세요. 검붉은색 혹은 주황색이 묻어나오면 이는 벼룩의 분변이며 벼룩이 존재함을 의미합니다. 이러한 경우 동물병원에 방문하여 수의사 선생님과 브라벡토 사용에 대해 상담하세요.</p>
								</li>
							</ul>
						</div>
					</div>
				</div>

				<div class="col-md-12 col-lg-4 mob-pedding wow fadeIn">
					<div class="video-block top-minouse">
						<a href="https://youtu.be/wVlZ2EPasD0" data-toggle="lightbox" data-gallery="youtubevideos">
							<img src="/images/videos/ticks-video.jpg" class="img-fluid" alt="Ticks Video">
						</a>
						<a href="#" class="playbutton" title="Watch Video"></a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="col-3-section">
		<div class="container-1">

			<div class="alternet-list alternet-list-last">
				<div class="row no-gutters">
					<div class="col-md-12 col-lg-6"><img src="/images/parasites/parasites_img.jpg" class="w-100" alt="Dirt on Ticks"></div>
					<div class="col-md-12 col-lg-6 align-self-end">
						<div class="text-block alternet-text-block alternet-text-block-bottom">
							<h2 class="anim full-visible"><span class="titleanimation-bottom">진드기의 위험성</span></h2>
							<p class="wow fadeInUp">
								사람에서 치명적일 수 있는 작은소참진드기와 같은 참진드기들은 소중한 반려동물에서는 
								더욱 치명적일 수 있습니다. 이러한 진드기는 반려동물의 털 속 깊숙이 숨어 보이지 않게 
								피를 빨고 번식하며 각종 질병을 퍼트립니다.
								또한 이보다 더 작아 눈으로는 보이지 않는 귀진드기, 옴진드기, 모낭충 등은 반려동물의 피부에 
								각종 질병을 유발하며, 치료가 쉽지 않습니다.


							</p>
						</div>
					</div>
				</div>
			</div>

		</div>
	</section>
	
	<section>
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-md-12 col-lg-8 wow fadeInUp">
					<div class="benefits benefits-noimg">
						<div class="benefits-list mb-0  wow fadeInUp">
							<h3 class="bigheading">참진드기가 옮기는 질병들</h3>
							<div class=" steps-car owl-carousel owl-theme">
								<div class="item" data-dot="옴진드기">
									<p>옴진드기(Sarcoptes scabiei)</p>
									<p>옴진드기는 전 세계에서 발견되는 감염성이 높은 진드기 입니다. 맨눈에는 보이지 않을 정도로 작은 이 진드기는
									사람 및 감염된 다른 동물과의 직,간접적인 접촉을 통해 전염되며, 진드기는 피부층을 파고들어가
									알을 낳고 번식을 합니다. 감염된 개는 극심한 가려움증으로 삶의 질이 현저히 저하됩니다.</p>
									<figure><img src="/images/steps/step-1.jpg" class="img-fluid" alt="How to use Bravecto Spot-on for Cats"></figure>
								</div>
								<div class="item" data-dot="모낭충">
									<p>모낭충(Demodex)</p>
									<p>건강한 개의 피부에서도 정상적으로 발견되는 모낭충은, 어떠한 원인에 의해 비정상적으로 숫자가 늘어나면 문제가 됩니다. 
									매우 작은 이 진드기는 개의 모낭 및 땀샘에 서식하며, 정확한 원인은 아직 밝혀지지 않았으나, 다른 동물로의
									전염은 일어나지 않습니다. 매우 극심한 가려움증으로 삶의 질이 현저히 저하됩니다.</p>
									<figure><img src="/images/steps/step-2.jpg" class="img-fluid" alt="How to use Bravecto Spot-on for Cats"></figure>
								</div>
								<div class="item" data-dot="귀진드기">
									<p>귀진드기(Otodectes cynotis)</p>
									<p>귀진드기는 개와 고양이의 귀에 문제를 일으키는 진드기 입니다. 매우 작아 맨눈으로는 보이지 않는 이 진드기는
									주로 외이도에서 가려움증을 유발하지만 심한 경우 고막을 뚫고 중이까지 감염되기도 합니다. 감염된 동물은
									머리를 너무 심하게 흔들거나, 과도하게 긁어 상처가 날 수 있습니다.</p>
									<figure><img src="/images/steps/step-3.jpg" class="img-fluid" alt="How to use Bravecto Spot-on for Cats"></figure>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12 col-lg-4 mob-pedding wow fadeIn">
					<div class="video-block top-minouse">
						<a href="/video/spot-on-cats-video.mp4" data-toggle="lightbox" data-gallery="youtubevideos">
							<img src="/images/videos/spot-on-cats-video.jpg" class="img-fluid" alt="Spot On Cats Video">
						</a>
						<a href="#" class="playbutton" title="Watch Video"></a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
</body>
</html>
		
