<?session_start();?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/logingCheck.php" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/themeHtml.php" ?>
</head>
<body>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
	<section class="inner-header">
		<div class="container-1">
			<div class="row no-gutters">
				<div class="col-12  col-lg-6">
					<div class="header-text-inner">
						<h1 class="anim"><span class="titleanimation-bottom">브라벡토 구입을 원하시나요?</span></h1>
						<p class="wow fadeInUp">거주 지역(시/군/구)을 검색하여 브라벡토 판매점을 찾아보세요.</p>
						<form class="find-stockist-form" onsubmit="getStockList(); return false;">
							<input name="zipcode" placeholder="판매점을 입력해주세요." type="text" id="input_stockist_search">
							<button class="arror-btn" type="submit"></button>
						</form>
					</div>
				</div>
				<div class="col-12  col-lg-6 getbravecto-header">
					<figure class="d-flex">
						<img src="/images/stockists/get_bravecto.png" alt="Facts About Ticks" class=" img-fluid wow fadeInUp">
					</figure>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div id="map" class="donly"></div>
	</section>

	<section id="section_results" style="display:none;">
		<div class="container-1">
			<div class="map-search-loading text-center p-4"><h3>Searching...</h3></div>
			<div class="map-search">
				<h1 class="anim"><span class="titleanimation-bottom">Results for</span></h1>
				<h5><span class="search-keyword titleanimation-bottom search-text"></span></h5>
				<div class="row">
					<div class="col-12">
						<p class="hero-text wow fadeInUp title"></p>
					</div>
				</div>
				<div class="row results"></div>

			</div>
		</div>
	</section>

	<section class="alt-color">
		<div class="contact-cta">
			<h1 class="anim"><span class="titleanimation-bottom">궁금하신 점이 있으신가요?</span></h1>
			<div class="contact-wrap">
				<ul>
					<li class="maillink wow fadeInUp">
						<a href="mailto:bravectoaustralia@merck.com"
						   title="send us email">
						   <small>이메일 문의 </small>
						   <span>bravectoaustralia@merck.com</span></a>
					</li>
				</ul>

				<div class="clearfix"></div>
			</div>

		</div>
	</section>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>

	@section scripts {
		<script src="/js/stockists.js"></script>
		<!--
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD_QVI2hMlj_pWD9KeOE1NsSF1U2K-79iQ&libraries=places,geometry&callback=initMap"></script>
		-->
		<script type="text/javascript" src="/js/jQ.js"></script>
		<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDQKYbdqgIltMM1a_WDob_U1T6ZXNRLg48&callback=initializeMap" type="text/javascript"></script>
		<script>
			var myGoogleMap;
			var geocoder;
			var markers = new Object();
			var stockListData;
			(function() {
				$.ajax({
					method : 'post'
					, url : 'ajax_stockListData.php'
					, dataType : 'json'
					, async : false
					, success : function(result) {
						stockListData = result;
					}
				});
			})();
			var initializeMap = function() {
				var loc = eval('('+stockListData[0].relation_url+')');
				myGoogleMap = new google.maps.Map(
					document.getElementById('map')
					, {
						zoom: 12
						, center: loc
					}
				);
				
				stockListData.forEach(function(el) {
					var loc = eval('('+el.relation_url+')');

					var marker = new google.maps.Marker({
						position: loc
						, map: myGoogleMap
						, icon: '/images/icons/map-marker.svg'
					});

					var contentString = '<div class="infowindow-content"><p><strong>' + el.name + '</strong><br/>'
						+ el.addr0 + '<br/>'
						+ el.title + '</p>' +
						'<p><a href="https://www.google.com/maps/search/?api=1&query='+loc.lat+','+loc.lng+'" target="_blank">Get directions</a></p><div>';

					var infowindow = new google.maps.InfoWindow({
						content: contentString
					});

					infowindow.addListener('closeclick', function () {
						activeInfoWindow = null;
					});

					marker.addListener('click', function () {
						if (activeInfoWindow !== null) {
							activeInfoWindow.close();
						}
						infowindow.open(map, marker);
						activeInfoWindow = infowindow;
					});

					markers[el.no] = marker;
				});

				geocoder = new google.maps.Geocoder();
			};

			var moveMap = function(loc, move) {
				move = typeof move === 'undefined' ? true : false;
				myGoogleMap.panTo(loc);
				if (move) {
					$('html, body').animate({
						'scrollTop': $('#map').offset().top
					}, 500);
				}
			};

			var getStockList = function() {
				var search_keyword = $('#input_stockist_search').val();
				$.ajax({
					method : 'post'
					, url : 'ajax_stockList.php'
					, beforeSend : function() {
						$('.map-search-loading').show();
					}
					, data : {
						sval : search_keyword
					}
					, success : function(result) {
						result = result.trim();
						$('#section_results').show();
						$('.search-keyword').text( search_keyword );
						if ( jQ.isEmpty(result) ) {
							$('.hero-text').text( 'Sorry. We\'re unable to find any stockists matching your search.' );
							$('.row.results').html( '' );
						}
						else {
							result = $(result);
							var no = $(result).filter('.col-12.col-md-6.col-lg-4.col-xl-3').eq(0).data('no');
							moveMap(markers[no].getPosition(), false);
							$('.hero-text').text( 'Showing ' + $(result).filter('.col-12.col-md-6.col-lg-4.col-xl-3').length + ' stockists' );
							$('.row.results').html( result );
						}
						scrollToResults(150);
						$('.map-search-loading').hide();
					}
				});
			};
		</script>
	}
</body>
</html>
		
