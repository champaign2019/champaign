//Global variables
var mapStyles = [
	{
		"elementType":
		"geometry",
		"stylers": [{ "color": "#DFDFDF" }]
	},
	{
		"elementType":
		"labels.icon",
		"stylers": [{ "visibility": "off" }]
	},
	{
		"elementType":
		"labels.text.fill",
		"stylers": [{ "color": "#616161" }]
	},
	{
		"elementType":
		"labels.text.stroke",
		"stylers": [{ "color": "#f5f5f5" }]
	},
	{
		"featureType":
		"administrative.land_parcel",
		"elementType": "labels.text.fill",
		"stylers": [{ "color": "#ff0000" }]
	},
	{
		"featureType":
		"poi",
		"elementType": "geometry",
		"stylers": [{ "color": "#DCD4EA" }]
	},
	{
		"featureType": "poi",
		"elementType": "labels.text.fill",
		"stylers": [{ "color": "#757575" }]
	},
	{
		"featureType": "poi.park",
		"elementType": "geometry",
		"stylers": [{ "color": "#e5e5e5" }]
	},
	{
		"featureType": "poi.park",
		"elementType": "labels.text.fill",
		"stylers": [{ "color": "#9e9e9e" }]
	},
	{
		"featureType": "road",
		"elementType": "geometry",
		"stylers": [{ "color": "#ffffff" }]
	},
	{
		"featureType": "road.arterial",
		"elementType": "labels.text.fill",
		"stylers": [{ "color": "#757575" }]
	},
	{
		"featureType": "road.highway",
		"elementType": "geometry",
		"stylers": [{ "color": "#dadada" }]
	},
	{
		"featureType": "road.highway",
		"elementType": "labels.text.fill",
		"stylers": [{ "color": "#616161" }]
	},
	{
		"featureType": "road.local",
		"elementType": "labels.text.fill",
		"stylers": [{ "color": "#9e9e9e" }]
	},
	{
		"featureType": "transit.line",
		"elementType": "geometry",
		"stylers": [{ "color": "#e5e5e5" }]
	},
	{
		"featureType": "transit.station",
		"elementType": "geometry",
		"stylers": [{ "color": "#eeeeee" }]
	},
	{
		"featureType": "water",
		"elementType": "geometry",
		"stylers": [{ "color": "#C9C5E4" }]
	},
	{
		"featureType": "water",
		"elementType": "labels.text.fill",
		"stylers": [{ "color": "#9e9e9e" }]
	}
];
var mapJsLoaded = false;
var map;
var stores = null;
var results = [];
var activeInfoWindow = null;
var maxResults = 8;

/**
 * Loads the static json data for stockists, parses zip,state and suburb values of each store and saves in 'stored' var.
 */
function loadData() {

	$.getJSON('/js/stockists.json', function (data) {

		for (x in data) {
			var address = data[x]['addr'].split(',');

			//Don't parse if address is not complete (probably a web address is given)
			if (address.length < 3)
				continue;

			var zip = address[address.length - 1];
			var state = address[address.length - 2];
			var suburb = address[address.length - 3];
			data[x]['zip'] = zip.trim();
			data[x]['state'] = state.trim();
			data[x]['suburb'] = suburb.trim();
			data[x]['street'] = address.splice(0, address.length - 3).join(',').trim()
		}

		stores = data;

		if (mapJsLoaded) {
			initMap();
		}
	});
}

/**
 * Initializes google maps, create markers and setup autocomplete
 */
function initMap() {

    //Set this to true so that loadData will call initMap after data is loaded
	mapJsLoaded = true;

	//if stores are empty, don't init map, initMap will be called after data is load
	if (stores === null) {
		return;
	}

	//Create Map
	map = new google.maps.Map(document.getElementById('map'), {
		zoom: 17,
		scrollwheel: false,
		center: new google.maps.LatLng(stores[0].lat, stores[0].lon),
		mapTypeId: 'roadmap',
		styles: mapStyles
	});

	// Create markers.
	stores.forEach(function (store) {
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(store.lat, store.lon),
			icon: '/images/icons/map-marker.svg',
			map: map,
		});

		var url = 'https://www.google.com/maps/search/?api=1&query=' + store.lat + ',' + store.lon;
		var contentString = '<div class="infowindow-content"><p><strong>' + store.name + '</strong><br>'
			+ store.street + '<br>'
			+ store.suburb + ' ' + store.state + ' ' + store.zip + '</p>' +
			'<p><a href="' + url + '" target="_blank">Get directions</a></p><div>';

		var infowindow = new google.maps.InfoWindow({
			content: contentString
		});

		infowindow.addListener('closeclick', function () {
			activeInfoWindow = null;
		});

		marker.addListener('click', function () {
			if (activeInfoWindow !== null) {
				activeInfoWindow.close();
			}
			infowindow.open(map, marker);
			activeInfoWindow = infowindow;
		});

		store.marker = marker;
		store.infowindow = infowindow;
	});

	//Setup autocomplete
	var input = document.getElementById('input_stockist_search');
	var options = { componentRestrictions: { country: 'AU' } };
	var autocomplete = new google.maps.places.Autocomplete(input, options);

	// autocomplete.setTypes(['address']);
	autocomplete.setTypes([]);
	autocomplete.addListener('place_changed', function () {
		var place = autocomplete.getPlace();
		if (place.geometry) {
			searchByPlace(place, "");
		}
	});

	handleQueryString();
}

function handleQueryString() {
	var queryParams = new URLSearchParams(window.location.search);
	if (queryParams.has("zipcode")) {
		$("#input_stockist_search").val(queryParams.get("zipcode"));
		searchStores();
	}
}

/**
 * Searches stores on form submit, using the entered search string
 * @returns {boolean}
 */
function searchStores() {

	var str = $("#input_stockist_search").val().trim();

	if (str.length === 0)
		return false;

	searchByText(str);
	return false;
}

/**
 *
 * Searches stores closest to the given google maps place and displays results
 * @param place: google.maps.place
 * @param str: optional title which can be displayed of place.formatted_address
 */
function searchByPlace(place, str) {

	var storesByDistance = stores.concat();
	storesByDistance.forEach(function (store) {
		store.distance = getDistance(store, place);
	});

	storesByDistance.sort(function (a, b) {
		return a.distance - b.distance;
	});

	results = storesByDistance.slice(0, maxResults);
	displayResults(str == '' ? place.formatted_address : str);
}

/**
 * Performs a search on zip|suburb|state values of stores matching given string.
 * If no results are matched, it picks up the first result suggested by google maps auto complete service and displays stores closes to that location.
 *
 * @param str
 */
function searchByText(str) {

	results = [];

	stores.forEach(function (store) {
		if (store.zip == str || store.suburb == str || store.state == str) {
			results.push(store);
		}
	});

	// uncomment this line if limit the number of matching results displayed.
	// results = results.slice(0, maxResults);

	//if manual search in array doesn't match anything, use google places autocomplete
	if (results.length === 0) {

		//if search string is a 4 digit number
		if ((!isNaN(parseInt(str)) && str.length == 4)) {
			searchByZip(str);
		}

		else {
			searchInPlaces(str);
		}
	}
	//Otherwise display the results
	else {
		displayResults(str);
	}
}

/**
 * Reverse geocodes given zip from geocode.xyz api and calls searchByPlace using
 * the lat|long of the resolved zip.
 * @param str: zip code
 */
function searchByZip(str) {

	$(".find-stockist-form input, .find-stockist-form button").attr('disabled', 'disabled');

	//Scroll to results section and show loading text
	scrollToResults(160);
	$(".map-search-loading").show();


	$.getJSON('https://geocode.xyz/' + str + '?region=AU&json=1', function (data) {
		//enable form input
		$(".find-stockist-form input, .find-stockist-form button").attr('disabled', null);

		//hide searching text
		$(".map-search-loading").hide();

		if ('latt' in data && 'longt' in data) {

			//create dummy place
			var place = {
				geometry: {
					location: new google.maps.LatLng(data.latt, data.longt)
				}
			};

			//perform search based on the place, override the title
			searchByPlace(place, str);
		} else {
			//Display empty results
			displayResults(str);
		}
	});
}


function searchInPlaces(str) {
	var geocoder = new google.maps.Geocoder;
	var service = new google.maps.places.AutocompleteService();
	var request = {
		componentRestrictions: { country: 'au' },
		input: str,
		types: []
	};
	service.getPlacePredictions(request, function (predictionResults, status) {
		//if there are predictions
		if (status === 'OK' && predictionResults.length > 0) {
			//use geocoder to get the place of first prediction
			geocoder.geocode({ 'placeId': predictionResults[0].place_id }, function (placeResults, status) {
				if (status === 'OK' && placeResults.length > 0) {
					//perform search based on the place, override the title
					searchByPlace(placeResults[0], str)
				}
				else {
					//Display empty results
					displayResults(str);
				}
			});
		} else {
			//Display empty results
			displayResults(str);
		}
	});
}

/**
 * If there are less then [maxResults] items in result array, this method extends the array with stores
 * which are 50kms within the existing results. 
 */
function extendResults() {

	for (x in results) {

		var store = results[x];
		//create dummy place
		var place = {
			geometry: {
				location: new google.maps.LatLng(store.lat, store.lon)
			}
		};

		//create a copy
		var storesByDistance = stores.concat();
		var nearByStores = [];

		//Push the stores close than 50km to the new array
		storesByDistance.forEach(function (s) {
			s.distance = getDistance(s, place);
			if (s.distance <= 50000) {
				nearByStores.push(s);
			}
		});

		//Sort nearbys
		nearByStores.sort(function (a, b) {
			return a.distance - b.distance;
		});

		for (y in nearByStores) {

			//Stop if maxResults are reached
			if (results.length === maxResults) {
				break;
			}

			//Check if nearby store exists in results array
			var exists = false;
			for (z in results) {
				if (results[z].clinicid == nearByStores[y].clinicid) {
					exists = true;
					break;
				}
			}

			//Push if not exists
			if(!exists)
				results.push(nearByStores[y]);
		}

		//Stop if maxResults are reached
		if (results.length === maxResults) {
			break;
		}
	}
}

/**
 * Renders the results stored in global results variable.
 * @param str
 */
function displayResults(str) {

	//Set html content (titles
	$('.map-search .search-text').html(str);
	$('.map-search .results').html('');

	//navigate map to first result
	if (results.length > 0) {

		if (results.length < maxResults) {
			extendResults();
		}
		//Display search result information
		$('.map-search .title').html('Showing ' + results.length + ' stockists');

		//Render the html of each resulting store
		results.forEach(function (store) {
			$('.map-search .results').append(getStoreHtml(store));
		});

		//navigate map to first resulti but don't scroll to map,
		navigateToStore(results[0].clinicid, false);
	} else {
		//Display no results warning.
		$('.map-search .title').html("Sorry. We're unable to find any stockists matching your search.");
	}

	//Display map search section (hidden on page load)
	$('.map-search').show();

	//scroll page to the results section
	scrollToResults(150);
}

/**
 * Pans map to the store with given clinic id and opens the infowindow.
 * @param clinicid
 * @param scroll
 */
function navigateToStore(clinicid, scroll) {

	results.forEach(function (store) {
		if (store.clinicid == clinicid) {

			if (scroll) {
				$("html, body").animate({
					'scrollTop': $('#map').offset().top
				}, 500);
			}

			map.panTo(store.marker.getPosition());
			map.setZoom(17);
			if (activeInfoWindow !== null) {
				activeInfoWindow.close();
			}
			store.infowindow.open(map, store.marker);

		}
	})
}

/**
 * Scrolls the the results section of the page,showing a small portion of map's bottom
 */
function scrollToResults(offset) {
	$("html, body").animate({
		'scrollTop': $('#section_results').offset().top - offset
	}, 500);
}

/**
 * Returns the geo-distance between a given store and a google maps place.
 * @param store
 * @param place
 * @returns {*}
 */
function getDistance(store, place) {
	return google.maps.geometry.spherical.computeDistanceBetween(place.geometry.location, store.marker.position);
}

/**
 * Generates the result div html for given store
 *
 * @param store
 * @returns {string}
 */
function getStoreHtml(store) {
	return '<div class="col-12 col-md-6 col-lg-4 col-xl-3" onclick="navigateToStore(' + store.clinicid + ', true)">\n' +
		'          <div class="stokist-address">\n' +
		'            <h5 class="wow fadeInUp anim anims"><span class="titleanimation-bottom">' + store.name + '</span></h5>\n' +
		'            <p class="wow fadeInUp">' + store.addr + '</p>\n' +
		'          </div>\n' +
		'        </div>';
}

$(document).ready(function () {
	loadData();
});