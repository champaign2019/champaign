function initCarousels() {
    $('.home-slider').owlCarousel({
        loop: true,
        autoplay: true,
        autoWidth: true,
        items: 3,
        margin: 0,
        dots: false,
        responsiveClass: true,
        autoplayHoverPause: true,   
        nav: false,
    });

    //Fix homepage slider width (full window width)
    $(".home-slider .owl-item > div").width($(window).width())

    initStepsCarousels();
    initProductHeaderCarousels();
}

function initStepsCarousels() {

    var steps_text_slider = $('.steps-car');

    steps_text_slider.owlCarousel({
        loop: true,
        autoplay: false,
        items: 1,
        margin: 0,
        dots: true,
        nav: false,
        autoHeight: true,
        dotsData: true
    });

    var step_images_slider = $('.step-images-slider');

    step_images_slider.owlCarousel({
        loop: true,
        autoplay: false,
        items: 1,
        margin: 0,
        dots: false,
        nav: true,
        autoHeight: true,
        navText: ["<i class='btn-arrow btn-right'></i>", "<i class='btn-arrow btn-left'></i>"],
    });
    
    steps_text_slider.on('changed.owl.carousel', function (event) {
        var index = event.item.index - 2;
        step_images_slider.trigger('to.owl.carousel', index);
    });

    step_images_slider.on('changed.owl.carousel', function (event) {
        var index = event.item.index - 2;
        steps_text_slider.trigger('to.owl.carousel', index);
    });
}

function initProductHeaderCarousels() {
    $('.product-sdlier-mobile').owlCarousel({
        loop: true,
        autoplay: false,
        items: 1,
        margin: 0,
        dots: true,
        nav: false,
        autoHeight: true
    });


    // -- Main Product (Shop Detail)
    // ==================================================
    var sync1 = $("#sync1");
    var sync2 = $("#sync2");
    var syncedSecondary = true;

    sync1
        .owlCarousel({
            items: 1,
            nav: false,
            autoplay: false,
            dots: false,
            loop: true,
        })
        .on("changed.owl.carousel", syncPosition);

    sync2
        .on("initialized.owl.carousel", function () {
            sync2
                .find(".owl-item")
                .eq(0)
                .addClass("current");
        })
        .owlCarousel({
            dots: false,
            nav: false,
            autoWidth: true,
            margin: 14,
            mouseDrag: false,
            responsive: {
                0: {
                    items: 1,
                    autoWidth: false,
                    nav: true,
                    navText: ["<i class='btn-ar btn-right1'></i>", "<i class='btn-ar btn-left1'></i>"],
                },

                992: {
                    autoWidth: true,
                    nav: false,
                    navText: ["<i class='btn-ar btn-right1'></i>", "<i class='btn-ar btn-left1'></i>"],
                },
            }
        })
        .on("changed.owl.carousel", syncPosition2);

    function syncPosition(el) {
        //if you set loop to false, you have to restore this next line
        //var current = el.item.index;

        //if you disable loop you have to comment this block
        var count = el.item.count - 1;
        var current = Math.round(el.item.index - el.item.count / 2 - 0.5);

        if (current < 0) {
            current = count;
        }
        if (current > count) {
            current = 0;
        }

        //end block

        sync2
            .find(".owl-item")
            .removeClass("current")
            .eq(current)
            .addClass("current");
        var onscreen = sync2.find(".owl-item.active").length - 1;
        var start = sync2
            .find(".owl-item.active")
            .first()
            .index();
        var end = sync2
            .find(".owl-item.active")
            .last()
            .index();

        if (current > end) {
            sync2.data("owl.carousel").to(current, 100, true);
        }
        if (current < start) {
            sync2.data("owl.carousel").to(current - onscreen, 100, true);
        }
    }

    function syncPosition2(el) {
        if (syncedSecondary) {
            var number = el.item.index;
            sync1.data("owl.carousel").to(number, 100, true);
        }
    }

    sync2.on("click", ".owl-item", function (e) {
        e.preventDefault();
        var number = $(this).index();
        sync1.data("owl.carousel").to(number, 300, true);
    });
}

//Setup dom event listeners
jQuery(document).ready(function () {

    initCarousels();
    $(window).resize(function () {
        initCarousels();
    });

    $(window).scroll(function () {
        var scroll = $(window).scrollTop();

        if (scroll >= 175) {
            $("header").addClass("darkHeader");
        } else {
            $("header").removeClass("darkHeader");
        }
    });

    $(document).click(function (e) {
        if (!$(e.target).is('.panel-body')) {
            $('.collapse1').collapse('hide');
        }
    });

    $(document).on('click', '[data-toggle="lightbox"]', function (event) {
        event.preventDefault();
        $(this).ekkoLightbox({
            alwaysShowClose: false
        });
    });

    $(document).on('click', '.playbutton', function (event) {
        event.preventDefault();
        $(this).prev().trigger('click');
    });

    $('.anim').viewportChecker({
        classToAdd: 'anims', // Class to add to the elements when they are visible
        offset: 100
    });

    $('.nav a.dropdown-toggle').click(function (e) {
        e.preventDefault()
        $(this).tab('show')
    })

    $('.tab-pane-first').viewportChecker({
        classToAdd: 'show', // Class to add to the elements when they are visible
        offset: 250
    });

    $('.home-product-tabs a').click(function (e) {
        e.preventDefault()
        $(this).tab('show')
    })

    $('a.scroll').on('click', function (e) {
        var href = $(this).attr('href');
        $('html, body').animate({
            scrollTop: $(href).offset().top
        }, 'slow');

        e.preventDefault();
    });

    $(".open-menu").click(function () {
        $("#mobilemenu").toggleClass("mobilemenu-active");
    });


    $("#producttab").stick_in_parent({
        offset_top: 66
    });

    $(".closmenu").click(function () {
        $("#mobilemenu").toggleClass("mobilemenu-active");
    });

    $(".menumobile .dropdown-toggle").click(function () {
        $("#mobilemenu").toggleClass("mobile-active");
    });

    $('.collapse').on('shown.bs.collapse', function (e) {
        var $card = $(this).closest('.card');

        $('html,body').animate({
            scrollTop: $card.offset.top
        }, 500);
    });

});