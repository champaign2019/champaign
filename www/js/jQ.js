(function(fn) {
	fn();
})(function() {
	var jQ = function( arg ) {
		return jQ.extends(arg);
	};

	(jQ.extends = function( arg ) {
		var _this = this;
		
		try {
			arg.$ = jQuery;
		}
		catch (e) {
			setTimeout(function() {
				arg.$ = jQuery;
			}, 3000);
		}

		arg.enterSubmit = function( obj ) {
			if ( event.keyCode === 13 ) {
				this.$(obj).closest('form').submit();
			}
		};

		arg.getParameter = function( param_name ) {
			return this.getParameterValues(param_name)[0];
		};

		arg.getParameterValues = function( param_name ) {
			param_name = typeof param_name !== 'undefined' ? param_name : '';
			
			var path = location.search;

			if (param_name) {
				var param_values = new Array();
				var split_values = path.split('&');
				for (var i=0; i<split_values.length; i++) {
					if ( split_values[i].indexOf(param_name+'=') >= 0 ) {
						param_values.push(split_values[i].split('=')[1]);
					}
				}

				return param_values;
			}
			else return '';
		};

		arg.transUniText = function( text ) {
			text = typeof text !== 'undefined' ? text : '';
			var uni_text = '';
			for (var i=0; i<text.length; i++) {
				if ( text[i].trim() == '' ) {
					uni_text += ' ';
				}
				else {
					uni_text += escape(text[i]).replace('%', '\\');
				}
			}
			return uni_text;
		};

		arg.transHexCode = function( text, isScript, transLoop ) {
			isScript = typeof isScript !== 'undefined' ? isScript : false;
			text = typeof text !== 'undefined' ? text : '';
			transLoop = typeof transLoop !== 'undefined' ? transLoop : 1;

			function getHexCode(str) {
				var result = '';
				for (var i=0; i<str.length; i++) {
					if(i!=0)result+=',';
					result+='0x'+((str.substring(i,(i+1))).charCodeAt()).toString(16);
				}
				return 'String.fromCharCode('+result+')';
			}

			for (var i=0; i<transLoop; i++) {
				if (isScript) {
					text = 'eval('+getHexCode(text)+')';
				}
				else {
					if (i!=0)
					text = 'eval('+getHexCode(text)+')';
					else
					text = getHexCode(text);
				}
			}

			return text;
		};

		arg.isChecked = function( obj, msg ) {
			var obj = typeof obj !== 'undefined' ? obj : null;
			var msg = typeof msg !== 'undefined' ? msg : '';
			if (this.$(obj).is(':checked')) {
				return true;
			}
			else {
				alert(msg);
				return false;
			}
		};

		arg.getDepth = function( selector, compare_filename, exception_path ) {
		    var path_name = location.pathname.substring(0, location.pathname.lastIndexOf('/')+1);
		    var file_name = location.pathname.replace(path_name, '');
		    if (typeof compare_filename === 'undefined') {
		        compare_filename = !(file_name.split('.')[0] == 'write' || file_name.split('.')[0] == 'read' || file_name.split('.')[0] == 'edit');
		    }
		    if ( compare_filename ) {
		        path_name = location.pathname;
		    }
		    var query_string = location.search;
		    var same_path = new Array();

		    this.$(selector).each(function(i, o) {
		        var this_href = jQ.$(this).attr('href').split('?')[0];
		        var this_path_name = this_href.substring(0, this_href.lastIndexOf('/')+1);
		        if ( compare_filename ) {
		            this_path_name = this_href.split('?')[0];
		        }
		        if ( typeof exception_path !== 'undefined' ) {
		            path_name = path_name.replace(exception_path, '');
		            this_path_name = this_path_name.replace(exception_path, '');
		        }
		        if ( path_name == this_path_name ) {
		            same_path.push({
		                cnt : 0
		                , obj : this
		                , index : jQ.$(selector).index( jQ.$(o) )
		            });
		        }
		    });

		    if ( same_path.length <= 0 ) {
		        return;
		    }
		    else {
		        for (var i=0; i<same_path.length; i++) {
		            var this_query_string = this.$(same_path[i].obj).attr('href').split('?')[1];
		            if ( this_query_string ) {
		                var this_params = this_query_string.split('&');
		                this_params.forEach(function(el) {
		                    if ( query_string.indexOf(el) >= 0 ) {
		                        same_path[i].cnt++;
		                    }
		                });
		            }
		        }
		        same_path.sort(function(a, b) {
		            return b['cnt'] - a['cnt'];
		        });
		        return new function() {
		            this.text = jQ.$(same_path[0].obj).text();
		            this.html = jQ.$(same_path[0].obj).closest('ul').html();
		            this.obj = same_path[0].obj;
		            this.index = same_path[0].index;
		        };
		    }
		};
		
		arg.hiddenDataAttributes = function( dataAttributes ) {
			dataAttributes.forEach(function(el) {
				jQ.$('*').filter(function() {
					if ( typeof jQ.$(this).data(el) !== 'undefined' )
						return this;
				}).each(function() {
					jQ.$(this).data(el, jQ.$(this).attr('data-'+el));
					jQ.$(this).removeAttr('data-'+el);
				});
			});
		};
		
		arg.addFile = function( fileWrap ) {
			jQ.$(this).click(function() {
				var size = jQ.$(fileWrap).size();
				var name = jQ.$(fileWrap).find('[type=file]').attr('name').split('|')[0].replace(/[0-9]/gi, '') + (size+1);
				var fileClone = jQ.$(fileWrap).eq(0).clone();
				jQ.$(fileClone).removeClass('not-removed').find('[type=file]').val('').removeAttr('style').attr('name', name).siblings().remove();
				jQ.$(fileWrap).parent().append(fileClone);
			});
		};

		arg.removeFile = function( fileWrap ) {
			jQ.$(this).click(function() {
				jQ.$(fileWrap).filter(':gt(0):last').not('.not-removed').remove();
			});
		};
		
		arg.lpad = function( str, padLen, padStr ) {
			if (padStr.length > padLen) {
				console.log('오류 : 채우고자 하는 문자열이 요청 길이보다 큽니다');
				return str;
			}
			str += '';
			padStr += '';
			while (str.length < padLen)
				str = padStr + str;
			str = str.length >= padLen ? str.substring(0, padLen) : str;
			return str;
		}
		
		arg.isEmpty = function( arg ) {
			if (typeof arg === 'undefined' || arg == null || arg == 'null' || arg == '') {
				return true;
			}
			return false;
		};
		
		arg.checkNull = function( arg ) {
			return !this.isEmpty(arg) ? arg : '';
		};
		
	})(jQ);

	this.jQ = jQ;
});