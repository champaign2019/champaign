<?session_start();
header("Content-Type:application/json"); ?>
<?
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/member/Member.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/email/SendMail.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/sms/Sms.class.php";

include $_SERVER['DOCUMENT_ROOT']."/include/humanCheck.php";

$url = $_REQUEST['url'] == '' ? "/" : substr($_REQUEST['url'], 0, strrpos($_REQUEST['url'], '?'));
$param = $_REQUEST[param];

$msg = 0;
$member = new Member($pageRows, "member", $_REQUEST);
$data = $member->dormantInfo($_REQUEST['id']);
$r = 0;
//코드생성
$code = rand(0000,9999);


if ($_REQUEST['type'] == 'cell') {

	if($_REQUEST['cell'] != $data['cell']) {
		$r = -1;
		$msg = "회원가입시 기입하신 휴대번호를 입력해주세요.";
	} else {
		$sms = new Sms(999, $_REQUEST);
		$_REQUEST['tran_id'] = SMS_KEYNO;
		$_REQUEST['tran_etc1'] = SMS_CNAME;
		$_REQUEST['tran_etc2'] = SMS_USERID;
		$_REQUEST['tran_etc3'] = SMS_USERNAME;
		$_REQUEST['sendtype'] = 'cell';
		$_REQUEST['tran_status'] = '1';
		$_REQUEST['tran_date'] = getFullToday();

		// 받을 사람(사용자)
		$msg = "[".COMPANY_NAME."] 인증번호 : ".$code." 를 사이트에 입력해주세요.";
		$_REQUEST['tran_callback'] = $hdata['reser_admin_tel'];
		$_REQUEST['receiver'] = $data['cell'];
		$_REQUEST['tran_msg'] = $msg;
		
		if($sms->sendSMS($_REQUEST)) {
			$r = 1;
			$msg = "발송이 완료되었습니다. 인증번호를 입력해주세요.";
		} else {
			$r = 0;
			$msg = "인증번호 발송을 다시 시도해주세요.";
		}
	}

} else {

	if($_REQUEST['email'] != $data['email']) {
		$r = -1;
		$msg = "회원가입시 기입하신 이메일주소를 입력해주세요.";
	} else {
		//1. 이메일 보내기
			$title = "[".COMPANY_NAME."] 휴면계정 인증번호"; //메일제목
			$contents = "<table border='0' cellpadding='0' cellspacing='0' width='100%' style='font-family:굴림; font-size: 12px; color: #4b4b4b; border:#CCCCCC 1px solid;'> ";
			$contents .= "
					<tr height='30'>									
						<td width=90 style='font-weight:bold; padding-left:5px; border-right:#CCCCCC 1px solid; border-bottom:#CCCCCC 1px solid; background-color: #f4f8fd;'>인증번호</td>
						<td style='padding-left:5px; border-bottom:#CCCCCC 1px solid;'> 인증번호 ".$code."를 입력해주세요. </td>
					</tr>
					</table>";
			$mailForm = getURLMakeMailForm(COMPANY_URL, EMAIL_FORM);
			$mailForm = str_replace(":SUBJECT", $title, $mailForm);
			$mailForm = str_replace(":CONTENT", $contents, $mailForm);
			$sendmail = new SendMail();
			
		//2. 이메일 완료시 상태 업데이트
		if($sendmail->send(COMPANY_EMAIL, COMPANY_NAME, $data['email'], $title, $mailForm)) {
			$r = 1;
			$msg = "발송이 완료되었습니다. 인증번호를 입력해주세요.";
		} else {
			$r = 0;
			$msg = "인증번호 발송을 다시 시도해주세요.";
		}
	}

}
?>
<?
$dataArray = array("result" => $r, "msg" => urlencode($msg), "code" => $code );
$last = json_encode($dataArray);
echo urldecode($last);
?>