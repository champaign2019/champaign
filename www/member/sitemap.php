<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<? include $_SERVER['DOCUMENT_ROOT']."/include/logingCheck.php" ?>
<? $pageTitle = "사이트 맵"; ?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="/css/layout.css" rel="stylesheet" type="text/css"/>
<link href="/css/board.css" rel="stylesheet" type="text/css"/>
<? include $_SERVER['DOCUMENT_ROOT']."/include/headHtml.php" ?>
<script type="text/javascript" src="/include/Function.js"></script>
<script type="text/javascript" src="/include/menu.js"></script>
<script type="text/javascript" src="/include/common.js"></script>
<script type="text/javascript" src="/include/prototype.js"></script>
</head>

<body>

<div id="wrap">
    <? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
    <? include "sub_visual/sub_visual.php" ?>
    <!-- 프로그램시작 -->
<div id="sitemap">
	<ul>
		<li class="sitemap01"><img src="/img/sitemap_01.jpg" alt="장학회소개" />
			<ul>
				<li><a href="#">인사말</a></li>
				<li><a href="#">연혁</a></li>
				<li><a href="#">정관&bull;조례</a>
					<ul>
						<li><a href="#">- 정관</a></li>
						<li><a href="#">- 조례</a></li>
					</ul>
				</li>
				<li><a href="#">CI소개</a></li>
				<li><a href="#">조직도</a></li>
				<li><a href="#">찾아오시는길</a></li>
			</ul>
		</li>
		<li class="sitemap02"><img src="/img/sitemap_02.jpg" alt="장학사업" />
			<ul>
				<li><a href="#">INDEX</a></li>
				<li><a href="#">장학금</a>
					<ul>
						<li><a href="#">- 글로벌인재육성장학금</a></li>
						<li><a href="#">- 인천인장학금</a></li>
						<li><a href="#">- 드림장학금</a></li>
						<li><a href="#">- 특별장학금</a></li>
					</ul>
				</li>
				<li><a href="#">선발장학생명단</a></li>
				<li><a href="#">학자금이자지원신청</a></li>
			</ul>
		</li>
		<li class="sitemap03"><img src="/img/sitemap_03.jpg" alt="기부나눔터" />
			<ul>
				<li><a href="#">기부안내</a>
					<ul>
						<li><a href="#">- 기부절차</a></li>
						<li><a href="#">- 기부약정</a></li>
					</ul>
				</li>
				<li><a href="#">기부현황</a>
					<ul>
						<li><a href="#">- 기부현황</a></li>
						<li><a href="#">- 명예의 전당</a></li>
						<li><a href="#">- 나의 기부내역</a></li>
					</ul>
				</li>
				<li><a href="#">멘토링</a>
					<ul>
						<li><a href="#">- 멘토링이란</a></li>
						<li><a href="#">- 멘토신청</a></li>
						<li><a href="#">- 멘티신청</a></li>
					</ul>
				</li>
			</ul>
		</li>
		<li class="sitemap04"><img src="/img/sitemap_04.jpg" alt="커뮤니티" />
			<ul>
				<li><a href="#">감사편지</a></li>
				<li><a href="#">공지사항</a></li>
				<li><a href="#">자주묻는질문</a></li>
				<li><a href="#">질문과답변 </a></li>
				<li><a href="#">포토갤러리</a></li>
				<li><a href="#">언론보도</a></li>
				<li><a href="#">자료실</a></li>
			</ul>
		</li>
		<li class="sitemap05"><img src="/img/sitemap_05.jpg" alt="회원가입" />
			<ul>
				<li><a href="/member/write.php">회원가입</a></li>
				<li><a href="/member/agree.php">회원약관</a></li>
				<li><a href="/member/login.php">로그인/로그아웃</a></li>
				<li><a href="/member/IdpwFind.php">ID/PW찾기</a></li>
				<li><a href="/member/edit.php">회원정보변경</a></li>
				<li><a href="/member/policy.php">개인정보취급방침</a></li>
			</ul>
		</li>
		<li class="sitemap06"><img src="/img/sitemap_06.jpg" alt="마이페이지" />
			<ul>
				<li><a href="#">나의 장학금</a>
					<ul>
						<li><a href="#">- 장학금신청결과</a></li>
						<li><a href="#">- 자기성장기록</a></li>
					</ul>
				</li>
				<li><a href="#">나의 나눔터</a>
					<ul>
						<li><a href="#">- 나의 기부내역</a></li>
						<li><a href="#">- 나의 멘토/멘티</a></li>
					</ul>
				</li>
				<li><a href="#">나의 커뮤니티</a>
					<ul>
						<li><a href="#">- 나의 질문과답변</a></li>
						<li><a href="#">- 나의 정보변경</a></li>
						<li><a href="#">- 회원탈퇴신청</a></li>
					</ul>
				</li>
			</ul>
		</li>
	</ul>
</div>
<!-- //sitemap --> 
<!-- 내용끝 --> 
<!-- 프로그램끝 -->
    <? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
    <? include "sub_footer/sub_footer.php" ?>
</div>
</body>
</html>
