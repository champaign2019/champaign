<?session_start();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<?
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/member/Member.class.php";
include $_SERVER['DOCUMENT_ROOT']."/include/humanCheck.php";

$url = $_REQUEST['url'] == '' ? "/" : substr($_REQUEST['url'], 0, strrpos($_REQUEST['url'], '?'));
$param = $_REQUEST[param];

$member = new Member($pageRows, "member", $_REQUEST);
$data = $member->loginMember($_REQUEST['id']);

?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/include/headHtml.php" ?>
<script>
function goSave(obj) {
	if($("#nowpassword").val() == '') {
		alert("사용하시던 비밀번호를 입력해주세요.");
		$("#nowpassword").focus();
		return;
	}
	if(validation(obj)){

		var form = $("#wform");
		fn_formSpanCheck(form,key);	
		
		$(obj).submit();
	}else{
		return;
	}
}
</script>
</head>
<body>

	<div id="wrap">
    <? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
    <? include "sub_visual/sub_visual.php" ?>
    <!-- 프로그램시작 -->
	<div class="programCon">
		<div class="login_wrap">
			<form name="wform" id="wform" method="post" action="process.php">
				<fieldset class="login">
					<p class="login_tit"><span>LOGIN</span> 휴면계정로그인</p>
					<p class="login_subtit"><span>(<?=$data['id']?>)</span>의 재사용을 위해 새로운 비밀번호를 입력해주세요.</p>
					<p class="login_txt"></p>
					<div class="text_box">
						<div class="re_move">
							<p>기존 비밀번호</p>
							<input type="password" name="nowpassword" id="nowpassword"/>
						</div>
						<div class="re_move">
							<p>변경할 비밀번호</p>
							<input type="password" name="password" id="password" class="inputPass" data-value="비밀번호를 입력하세요."/>
						</div>	
						<div class="re_move">
							<p>비밀번호 재확인</p>
							<input type="password" name="password2" id="password2" class="inputPass" data-value="비밀번호를 정확히 입력하세요."/>
						</div>	
						<div class="btn_box">
							<a href="javascript:;" onclick="goSave($('#wform'))" class="human_btn2">확인</a>
							<a href="javascript:;" onclick="$('#next').submit()" class="human_btn2" style="background:#5b5b5b">나중에 변경하기</a>
						</div>
					</div>
				</fieldset>
				<input type="hidden" name="cmd" id="cmd" value="changepw"/>
				<input type="hidden" name="no"  value="<?=$data['no']?>"/>
				<input type="hidden" name="id"  value="<?=$data['id']?>"/>
				<input type="hidden" name="url" id="url" value="<?=$url?>"/>
				<input type="hidden" name="param" id="param" value="<?=$param?>"/>
			</form>
			<form name="next" id="next" method="post" action="/member/login_human04.php">
				<input type="hidden" name="no"  value="<?=$data['no']?>"/>
				<input type="hidden" name="url" id="url" value="<?=$url?>"/>
				<input type="hidden" name="param" id="param" value="<?=$param?>"/>
			</form>
		</div>
	</div>	
	<!-- 프로그램끝 -->
    <? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
    <? include "sub_footer/sub_footer.php" ?>
</div>
</body>
</html>