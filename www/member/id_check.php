<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/member/Member.class.php";

include $_SERVER['DOCUMENT_ROOT']."/include/logingCheck.php";
include "config.php";

$member = new Member($pageRows, "member", $_REQUEST);
$use;
$cnt = 0;

if ($_REQUEST['id']) {
	$cnt = $member->checkId($_REQUEST['id']);
	$use = $cnt == 0 ? "<span>사용 가능</span>" : "<span style='color:#d32b2b;'>사용 불가능</span>";
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0, user-scalable=no" />
<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" />
<link rel="stylesheet" href="/css/basic.css" type="text/css" media="all" />
<!-- <link rel="stylesheet" href="/css/board.css" type="text/css" media="all" />
<link rel="stylesheet" href="/css/member.css" type="text/css" media="all" /> -->
<link rel="stylesheet" href="/css/board.css" type="text/css" media="all" />
<link rel="stylesheet" href="/css/layout.css" type="text/css" media="all" />
<link rel="stylesheet" href="/css/content.css" type="text/css" media="all" />

<script src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script type="text/javascript" src="/js/jquery-1.8.0.min.js"></script>
<script type="text/javascript" src="/js/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="/smarteditor/js/HuskyEZCreator.js"></script>


<script type="text/javascript" src="/js/function.js"></script>
<script type="text/javascript" src="/js/function_jquery.js"></script> 


<script type="text/javascript" src="/js/dev.js"></script> 
<script type="text/javascript" src="/js/validationBind.js"></script> 



<script type="text/javascript">
	var reg = /^[A-z0-9]{5,12}$/; //id : 5~12자 이내의 영대소문/숫자

	$(window).load(function(){	
		if(!reg.test($("#id").val())) {	//opener에서 보낸 id가 한글일 경우 확인
			$('.btnok').remove();
		}
	})

	function chkId() { //추가로 검색할 경우
		if(!reg.test($("#id").val())) {
			alert("아이디를 영문/숫자 5~12자 이내로 입력해주세요.");
			return false;
		} else {
			return true;
		}
	}

	function goSave() {
		$(opener.document).find("#id").val($("#id").val());
		$(opener.document).find("#id").attr("readonly","readonly");
		$(opener.document).find("#loginChecked").val('true');
		window.close();
	}
</script>
</head>
<body onload="document.board.id.focus();">


<div id="member" class="idCheck_wrap">
	<div id="mpop">
		<form name="board" method="post" action="id_check.php" onsubmit="return chkId();">
			<fieldset id="popcheck">
				<legend></legend>
				<h2>ID CHECK <span>아이디 중복확인</span></h2>
				<p>사용하고자 하는 아이디를 입력해주세요. <br />아이디 중복확인 후 사용 가능한 아이디로 선택하시면 됩니다.</p>
				<div class="bgBox">
					<dl class="conBox">
						<dd>
							<input type="text" id="id" name="id" maxlength="50" class="inw195" onkeyup="$('.btnok').remove();" title="사용하실 아이디를 입력해 주십시요." value="<? if ($_REQUEST['id']) echo $_REQUEST['id']; ?>" maxlength="20" />
						</dd>
						<dd>
							<input type="submit" class="popcheckBtn" value="중복확인" />
						</dd>
					</dl>
				</div>
				<!-- //bgBox -->
				<? if ($_REQUEST['id']) { ?>
				<div class="btnok">
					<strong>사용하실 아이디</strong>는(은) <?=$use?>한 아이디 입니다.
					<? if ( $cnt == 0 ){ ?>
					<div class="btnAll">
						<a class="btns" href="#" onclick="goSave();"><strong>사용</strong></a>
					</div>
					<? } ?>
					<!--//btnAll-->
				</div>
				<!-- //btnok -->
				<? } ?>
				
			</fieldset>
			<input type="hidden" id="loginChecked" value="true" />
		</form>
	</div>
	<!-- //mpop -->
</div>
<!-- //member -->

</body>
</html>