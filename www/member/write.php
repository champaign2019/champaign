<?session_start();

include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/environment/Stipulation.class.php";

include $_SERVER['DOCUMENT_ROOT']."/include/logingCheck.php";

include "config.php";

$st = new Stipulation();
$data = $st->getData();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<? include $_SERVER['DOCUMENT_ROOT']."/include/headHtml.php" ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript">
$(window).load(function(){
	inputMaxLength();

	checkIdKr =  function() {
		if($("#id").val() == "") {
			alert("아이디를 영문/숫자 5~12자 이내로 입력해주세요.");
			return false;
		} else {
			return true;
		}		
	}

})

function checkSubmit() {
	var reg = /^[A-z0-9]{5,12}$/;
	if(!reg.test($("#id").val())) {
		return false;
	} else {
		return true;
	}
}

function goSave(obj) {
	if(checkSubmit()) {
		if(validation(obj)){
			try{
	//			if($("[name=tel1]").val() != "" && $("input[name=tel2]").val() != "" && $("input[name=tel3]").val() != ""){
	//				$("input[name=tel]").val($("[name=tel1]").val()+"-"+$("input[name=tel2]").val()+"-"+$("input[name=tel3]").val())
	//			}
			}catch(e){
			}
			try{
	//			if($("[name=cell1]").val() != "" && $("input[name=cell2]").val() != "" && $("input[name=cell3]").val() != ""){
	//				$("input[name=cell]").val($("[name=cell1]").val()+"-"+$("input[name=cell2]").val()+"-"+$("input[name=cell3]").val())
	//			}
			}catch(e){
			}
			try{
				if($("input[name=email1]").val() != "" && $("[name=email2]").val() != ""){
					$("input[name=email]").val($("input[name=email1]").val()+"@"+$("[name=email2]").val());
				}
			}catch(e){
			}
			
			var form = $("#wform");
			fn_formSpanCheck(form,key);	
			$(obj).submit();
		}else{
			return;
		}
	} else {
		alert("아이디를 영문/숫자 5~12자 이내로 입력해주세요.");
	}
}


function checkId(){
	if(checkIdKr()){
		var id = $("#id").val();
		window.open ('/member/id_check.php?id='+id, 'newwin' , 'toolbar=0, location=0, status=0, menubar=0, scrollbars=0, resizable=0, top=0, left=0, width=504, height=317');
	}
}

function searchzipcode(){
	var urlname = "/zipcode/search.php";
	window.open(urlname,"browse_org","height=369,width=506,menubar=no,directories=no,resizable=no,status=no,scrollbars=no");
}
</script>
<?
if(SSL_USE){ 
?>
<!-- 다음우편번호API -->
<script src="https://spi.maps.daum.net/imap/map_js_init/postcode.v2.js"></script>
<?}else{?>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<?}?>
<script>
    function zipcodeapi() {
        new daum.Postcode({
            oncomplete: function(data) {
                // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

                // 각 주소의 노출 규칙에 따라 주소를 조합한다.
                // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
                var fullAddr = ''; // 최종 주소 변수
                var extraAddr = ''; // 조합형 주소 변수

                // 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
                if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
                    fullAddr = data.roadAddress;

                } else { // 사용자가 지번 주소를 선택했을 경우(J)
                    fullAddr = data.jibunAddress;
                }

                // 사용자가 선택한 주소가 도로명 타입일때 조합한다.
                if(data.userSelectedType === 'R'){
                    //법정동명이 있을 경우 추가한다.
                    if(data.bname !== ''){
                        extraAddr += data.bname;
                    }
                    // 건물명이 있을 경우 추가한다.
                    if(data.buildingName !== ''){
                        extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                    }
                    // 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
                    fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
                }

                // 우편번호와 주소 정보를 해당 필드에 넣는다.
                $('#zipcode').val(data.zonecode); //5자리 새우편번호 사용
                $('#addr0').val(fullAddr);

                // 커서를 상세주소 필드로 이동한다.
                $('#addr1').focus();
            }
        }).open();
    }
</script>
</head>
<body>


	<div id="wrap">
    <? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
    <? include "sub_visual/sub_visual.php" ?>
    <!-- 프로그램시작 -->
	<div class="programCon" data-aos="fade-down" data-aos-delay="400">
		<form name="wform" id="wform" method="post" action="<?=getSslCheckUrl($_SERVER['REQUEST_URI'], 'process.php')?>">
			<div class="join">
				<div class="joinTop">
					<div class="ScrollLy"><?=str_replace("#company_name#", COMPANY_NAME, $data[join_text])?></div>
					<div class="jcheck">
						<input type="checkbox" data-value="회원가입약관에 동의하세요." id="agree00" name="agree00" value="" title="회원가입약관에 동의합니다 체크해주세요." checked="checekd"/>
						<label for="agree00"> 회원가입약관에 동의합니다.</label>
					</div>
					<!-- //jcheck -->
				</div>
				<div class="joinTop">
					<div class="ScrollLy"><?=str_replace("#company_name#", COMPANY_NAME, $data[privacy_mini_text])?></div>
					<div class="jcheck">
						<input type="checkbox" data-value="개인정보취급방침에 동의하세요." id="agree02" name="agree02" value="" title="개인정보취급방침에 동의합니다 체크해주세요." checked="checked" />
						<label for="agree02"> 개인정보취급방침에 동의합니다.</label>
					</div>
					<!-- //jcheck -->
				</div>
			</div>
			<!-- //join -->
			<div class="writeForm">
				<table>
					<colgroup>
						<col class="writeForm_col01" />
						<col width="*" />
					</colgroup>
					<tr>
						<th><label for="id">아이디</label></th>
						<td>
							<input data-value="아이디를 입력하세요." name="id" id="id" class="inputTxt inputIdtype" type="text" maxlength="20" /><a class="id_ch" href="javascript:checkId();">ID 중복확인</a>
						</td>
					</tr>
					<tr>
						<th><label for="password">비밀번호</label></th>
						<td>
							<input data-value="비밀번호를 입력하세요." name="password" id="password" class="inputPass" type="password" />
							<span class="password_ch"><label for="password2">비밀번호 확인</label></span>
							<input data-value="비밀번호를 정확히 입력하세요." name="password2" id="password2" class="inputPass size02 mmarT10" type="password" />
						</td>
					</tr>
					<tr>
						<th><label for="name">이름</label></th>
						<td>
							<input data-value="이름을 입력하세요." id="name" name="name" class="inputTxt inputName" type="text" maxlength="20"/>
						</td>
					</tr>
					<tr>
						<th>연락처</th>
						<td>
							<input type="text" name="tel" id="tel" value=""  data-value="연락처를 입력하세요." />
						</td>
					</tr>
					<tr>
						<th>휴대폰</th>
						<td>
							
							<?
							if(!$isCellSum){
							?>
							<select class="selectNum" name="cell1" id="cell1">
								<?=getOptsCell("")?>
							</select>
							<input data-value="휴대폰을 입력하세요." name="cell2" id="cell2" class="inputNum" type="text" value="" maxlength="4" onkeyup="isNumberOrHyphen2(this);" />
							<input data-value="휴대폰을 입력하세요." name="cell3" id="cell3" class="inputNum" type="text" value="" maxlength="4" onkeyup="isNumberOrHyphen2(this);" />
							<input type="hidden" name="cell" id="cell" value=""/>
							<?}else{ ?>
							<input type="text" name="cell" id="cell" value="" onkeyup="isNumberOrHyphen(this);" onblur="cvtUserPhoneNumber(this)" data-value="휴대폰을 입력하세요." />
							<?}?>
							
							<span class="label_wrap"><input type="checkbox" id="Num_check" name="issms" value="1" /><label for="Num_check">수신</label></span>
						</td>
					</tr>
					
					<tr>
						<th><label for="email">이메일</label></th>
						<td class="mail_type">
							<input data-value="이메일을 입력하세요." name="email1" id="email1" class="inputEmail" type="text" maxlength="100"  /><span class="email_txt">@</span>
							<select class="selecEmail" name="email2" id="email2" data-value="이메일을 선택하세요">
								<?=getOptsDomain("", 0)?>
							</select>
							<span class="label_wrap"><input type="checkbox" id="Email_check" name="ismail" value="1" /><label for="Email_check">수신</label></span>
						</td>
					</tr>
					<tr>
						<th><label for="zipcode">우편번호</label></th>
						<td>
							<input data-value="우편번호를 입력하세요." type="text" class="zipcode" id="zipcode" name="zipcode" value="" readonly="readonly" onclick="zipcodeapi();" /><a class="zip_ch" href="javascript:zipcodeapi();" >우편번호검색</a>
						</td>
					</tr>
					<tr>
						<th><label for="addr0">주소</label></th>
						<td class="addr_td">
							<input data-value="주소를 입력하세요." type="text" id="addr0" class="addr" name="addr0" value="" readonly="readonly" /><br />
							<input data-value="상세주소를 입력하세요." type="text" id="addr1" class="addr" name="addr1" value="" style="margin-top:7px;" />
						</td>
					</tr>
				</table>
			</div>
			<div class="writeForm_btn">
				<a href="javascript:;" id="w_btn" onclick="goSave($('#wform'))">확인</a>
				<a href="#" onClick="reset();">취소</a>
			</div>
			<input type="hidden" name="email" id="email" value="" />
			<input type="hidden" name="cmd" id="cmd" value="write" />
			<input type="hidden" name="secession" id="secession" value="0" />
		</form>
	</div>
	<!-- 프로그램끝 -->
    <? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
    <? include "sub_footer/sub_footer.php" ?>
</div>


</body>
</html>