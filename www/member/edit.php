<?session_start();
Header("Content-type: text/html; charset=utf-8");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<? include $_SERVER['DOCUMENT_ROOT']."/include/headHtml.php" ?>
<?
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/member/Member.class.php";

include $_SERVER['DOCUMENT_ROOT']."/include/logingCheck.php";
include "config.php";

$member = new Member($pageRows, "member", $_REQUEST);

?>
<title><?=COMPANY_NAME?> | 회원정보 변경</title>
<?
	if (!$loginCheck) {
?>
	<script type="text/javascript">
		var yesno = confirm("로그인이 필요합니다.\r\n로그인 하시겠습니까?");
		if(yesno) { document.location = "/member/login.php"; }
		else { history.go(-1); }
	</script>
</head>
</html>
<?
	} else {
	$_REQUEST[no] = $_SESSION[member_no];							// 로그인한 사용자.
	
	if("snsLogin" == $_SESSION[member_sns]) {
	
?>
	<script type="text/javascript">
		alert("회원정보를 수정 할 수 없습니다.");
		history.go(-1);
	</script>
<?
	} else {
	$data = $member->getData($_REQUEST);
?>
<script type="text/javascript">
	function goSave(obj) {
		if(validation(obj)){
			<?if(!$isCellSum){?>
			if($("[name=tel1]").val() != "" && $("input[name=tel2]").val() != "" && $("input[name=tel3]").val() != ""){
			$("input[name=tel]").val($("[name=tel1]").val()+"-"+$("input[name=tel2]").val()+"-"+$("input[name=tel3]").val())
			}
			
			if($("[name=cell1]").val() != "" && $("input[name=cell2]").val() != "" && $("input[name=cell3]").val() != ""){
			$("input[name=cell]").val($("[name=cell1]").val()+"-"+$("input[name=cell2]").val()+"-"+$("input[name=cell3]").val())
			}
			
			<?}?>
			
			
			if($("input[name=email1]").val() != "" && $("[name=email2]").val() != ""){
			$("input[name=email]").val($("input[name=email1]").val()+"@"+$("[name=email2]").val());
			}
			
			var form = $("#wform");

			fn_formSpanCheck(form,key);	
			$(form).submit();

		}else{
			return;
		}
	}

	function searchzipcode(){
		var urlname = "/zipcode/search.php";
		window.open(urlname,"browse_org","height=369,width=506,menubar=no,directories=no,resizable=no,status=no,scrollbars=no");
	}
</script>
<!-- 다음우편번호API -->
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script>
    function zipcodeapi() {
        new daum.Postcode({
            oncomplete: function(data) {
                // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

                // 각 주소의 노출 규칙에 따라 주소를 조합한다.
                // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
                var fullAddr = ''; // 최종 주소 변수
                var extraAddr = ''; // 조합형 주소 변수

                // 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
                if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
                    fullAddr = data.roadAddress;

                } else { // 사용자가 지번 주소를 선택했을 경우(J)
                    fullAddr = data.jibunAddress;
                }

                // 사용자가 선택한 주소가 도로명 타입일때 조합한다.
                if(data.userSelectedType === 'R'){
                    //법정동명이 있을 경우 추가한다.
                    if(data.bname !== ''){
                        extraAddr += data.bname;
                    }
                    // 건물명이 있을 경우 추가한다.
                    if(data.buildingName !== ''){
                        extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                    }
                    // 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
                    fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
                }

                // 우편번호와 주소 정보를 해당 필드에 넣는다.
                $('#zipcode').val(data.zonecode); //5자리 새우편번호 사용
                $('#addr0').val(fullAddr);

                // 커서를 상세주소 필드로 이동한다.
                $('#addr1').focus();
            }
        }).open();
    }
</script>
</head>
<body>

<div id="wrap">
    <? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
    <? include "sub_visual/sub_visual.php" ?>
    <!-- 프로그램시작 -->
	<div class="programCon">
		<form name="wform" id="wform" method="post" action="<?=getSslCheckUrl($_SERVER['REQUEST_URI'], 'process.php')?>">
	<!-- 		<form name="wform" id="wform" method="post" action="http://hospital.vizensoft.com:92/member/process.jsp" > -->
			<!-- //join -->
			<div class="writeForm">
				<table>
					<colgroup>
						<col class="writeForm_col01" />
						<col width="*" />
					</colgroup>
					<tr>
						<th><label for="id">아이디</label></th>
						<td>
							<label><?=$_SESSION["member_id"]?></label>
						</td>
					</tr>
					<tr>
						<th><label for="password">비밀번호</label></th>
						<td>
							<input name="password" id="password" class="inputPass" type="password" />
							<span class="password_ch"><label for="password2">비밀번호 확인</label></span>
							<input name="password2" id="password2" class="inputPass" type="password" />
						</td>
					</tr>
					<tr>
						<th><label for="name">이름</label></th>
						<td>
							<input data-value="이름을 입력하세요." id="name" name="name" class="inputTxt inputName" type="text" value="<?=$data[name]?>" />
						</td>
					</tr>
					<tr>
						<th>연락처</th>
						<td>
							
							<input type="text" name="tel" id="tel" value="<?=$data[tel]?>" onkeyup="isOnlyNumberNotHypen(this);" data-value="연락처를 입력하세요."/>
							
							
						</td>
					</tr>
					<tr>
						<th>휴대폰</th>
						<td>
							
							<?
							if(!$isCellSum){
							?>
							<select class="selectNum" name="cell1" id="cell1">
								<?=getOptsCell(getSplitIdx($data['cell'], "-", 0))?>
							</select>
							<input data-value="휴대폰을 입력하세요." name="cell2" id="cell2" class="inputNum" type="text" value="<?=getSplitIdx($data['cell'], "-", 1)?>" maxlength="4" onkeyup="isNumberOrHyphen2(this);" />
							<input data-value="휴대폰을 입력하세요." name="cell3" id="cell3" class="inputNum" type="text" value="<?=getSplitIdx($data['cell'], "-", 2)?>" maxlength="4" onkeyup="isNumberOrHyphen2(this);" />
							<input type="hidden" name="cell" id="cell" value=""/>
							<?}else{ ?>
							<input type="text" name="cell" id="cell" value="<?=$data['cell']?>" onkeyup="isNumberOrHyphen(this);" onblur="cvtUserPhoneNumber(this)" data-value="휴대폰을 입력하세요." />
							<?}?>
							
							<span class="label_wrap"><input type="checkbox" id="Num_check" name="issms" value="1" <?=getChecked($data[issms], 1)?> /><label for="Num_check">수신</label></span>
						</td>
					</tr>
					<tr>
						<th>이메일</th>
						<td>
							<input type="hidden" name="directEmail" value=""/>
							<input data-value="이메일을 입력하세요." name="email1" id="email1" class="inputEmail" type="text" value="<?=getSplitIdx( $data[email], "@", 0)?>" maxlength="70"/><span class="email_txt">@</span>
							<select class="selecEmail" name="email2" id="email2" data-value="이메일을 선택하세요">
								<?=getOptsDomain(getSplitIdx( $data[email], "@", 1))?>
							</select>
							<span class="label_wrap"><input type="checkbox" id="Email_check" name="ismail" value="1"  <?=getChecked($data[ismail], 1)?> /><label for="Email_check">수신</label></span>
						</td>
					</tr>

					<tr>
						<th><label for="zipcode">우편번호</label></th>
						<td>
							<input data-value="우편번호를 입력하세요." type="text" class="zipcode" id="zipcode" name="zipcode" value="<?=$data[zipcode]?>" readonly="readonly" onclick="zipcodeapi();" /><a class="zip_ch" href="javascript:zipcodeapi();" >우편번호검색</a>
						</td>
					</tr>
					<tr>
						<th><label for="addr0">주소</label></th>
						<td class="addr_td">
							<input data-value="주소를 입력하세요." type="text" id="addr0" class="addr" name="addr0" value="<?=$data[addr0]?>" readonly="readonly" /><br />
							<input data-value="상세주소를 입력하세요." type="text" id="addr1" class="addr" name="addr1" value="<?=$data[addr1]?>" style="margin-top:7px;" />
						</td>
					</tr>
				</table>
			</div>
			<div class="writeForm_btn">
				<a href="javascript:;" id="w_btn" onclick="goSave(this);">확인</a>
	<!-- 			<a href="javascript:;" id="w_btn" onclick="goSave($('#wform'))">확인</a> -->
				<a href="#" onClick="reset();">취소</a>
			</div>
			<input type="hidden" name="email" id="email" value="<?=$data[email]?>" />
			<input type="hidden" name="no" id="no" value="<?=$data[no]?>" />
			<input type="hidden" name="cmd" id="cmd" value="edit" />
			<input type="submit" style="display:none;"/>
		</form>
	</div>
	<!-- 프로그램끝 -->
    <? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
    <? include "sub_footer/sub_footer.php" ?>
</div>


</body>
</html>
<? }
}
?>