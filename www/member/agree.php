<?session_start();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<?
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/environment/Stipulation.class.php";

$st = new Stipulation();
$row = $st->getData();
?>
<head>
<? include $_SERVER['DOCUMENT_ROOT']."/include/headHtml.php" ?>
<title><?=COMPANY_NAME?> | 회원가입약관</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
<div id="wrap">
    <? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
    <? include "sub_visual/sub_visual.php" ?>
    <!-- 프로그램시작 -->
<div id="member" data-aos="fade-down" data-aos-delay="400">
	<div class="mem_wrap">
	<h4>회원약관</h4>
	<div id="agree">
		<?=str_replace("#company_name#", COMPANY_NAME, $row[join_text])?>
	</div>
	<!-- //agree -->
	</div>
</div>
<!-- //member -->
<!-- 프로그램끝 -->
    <? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
    <? include "sub_footer/sub_footer.php" ?>
</div>
</body>
</html>