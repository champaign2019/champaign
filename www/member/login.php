<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include 'config.php';
$tempUrl = getReqParameter($_REQUEST['url'], '/');
if ("/" != $tempUrl && strpos($tempUrl, 'http') !== false) {
	$tempUrl = str_replace('http://', '', $tempUrl);
	$tempUrl = str_replace('https://', '', $tempUrl);
	$tempUrl = substr($tempUrl, strpos($tempUrl, '/'), strlen($tempUrl));
}
//$firsturl = $_REQUEST['url'] == '' ? "/" : substr($_REQUEST['url'], 0, strrpos($_REQUEST['url'], '?'));
$firsturl = getReqParameter($tempUrl, '/');
if($firsturl == '/') {
	if($_SERVER['HTTP_REFERER'] != COMPANY_URL) {
		if(checkReferer($_SERVER["HTTP_REFERER"])) {
			$firsturl = getReqParameter($_SERVER['HTTP_REFERER'],'/');
		}
	}
}
$param = $_REQUEST[param];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<? include $_SERVER['DOCUMENT_ROOT']."/include/headHtml.php" ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?if($login_naver){?>
<script type="text/javascript" src="/js/naverLogin-1.0.1.js"></script> <!-- 네이버로그인 -->
<?}?>
<?if($login_kakao){?>
<script src="/js/kakao.js"></script> <!-- 카카오로그인 -->
<?}?>
<?if($login_facebook){?>
<? include $_SERVER['DOCUMENT_ROOT']."/sns/facebook.php" ?>
<?}?>
<script type="text/javascript">
//카카오 로그인
//<![CDATA[
// 사용할 앱의 JavaScript 키를 설정해 주세요.
Kakao.init('<?=LOGIN_KAKAO?>');
function loginWithKakao() {
		// 로그인 창을 띄웁니다.
		Kakao.Auth.login({
		  success: function(authObj) {
				Kakao.API.request({
					url:'/v1/user/me',
						success: function(res) {
						$("#id").val(JSON.stringify(res.id))
						$('#password').val('snspass');
						$('#from').val('kakao');
						$("#login").submit();
					},
						fail: function(error) {
							alert(JSON.stringify(error));
					}
				});
		  },
		  fail: function(err) {
			alert(JSON.stringify(err));
		  }
		});
	  };
//]]>

function loginCheck(){
	if ( getObject("id").value.length < 1 ) {
		alert("아이디를 입력해주세요.");
		getObject("id").focus();
		return false;
	}

	if ( getObject("password").value.length < 1 ) {
		alert("비밀번호를 입력해주세요.");
		getObject("password").focus();
		return false;
	}
	return true;
}

</script>
</head>
<body onload="getObject('id').focus();">

<div id="wrap">
    <? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
    <? include "sub_visual/sub_visual.php" ?>
    <!-- 프로그램시작 -->
	<div class="programCon">
		<div class="login_wrap">
			<form name="login" id="login" method="post" action="<?=getMemberSslCheckUrl('include/login.php')?>" onsubmit="return loginCheck($('#loginBtn'));">
				<fieldset class="login">
					<legend></legend>
					<p class="login_tit"><span>LOGIN</span>; 회원전용서비스</p>
					<p class="login_subtit">로그인 후 확인하실 수 있습니다.</p>
					<p class="login_txt">아이디와 비밀번호를 입력해주세요.<br/>아직 회원이 아니신 분들은 회원가입 후 서비스를 이용하실 수 있습니다.</p>
					<ul class="loginCon">
						<li>
							<div class="linp_wrap">
								<dl>
									<dt><label for="id"><img src="/img/id_icon.png" alt="" /></label></dt>
									<dd><input data-value="아이디를 입력하세요." type="text" name="id" id="id" placeholder="ID" /></dd>
								</dl>
								<dl>
									<dt><label for="password"><img class="password_icon" src="/img/password_icon.png" alt="" /></label></dt>
									<dd><input data-value="비밀번호를 입력하세요." type="password" name="password" id="password" placeholder="Password" /></dd>
								</dl>
							</div>
							<a class="login_btn" href="javascript:;" id="loginBtn" onclick="$('#login').submit()">로그인</a>
						</li>
						<li class="last">
							<div>
								<span>아직 회원이 아니신가요?</span>
								<a href="/member/write.php">회원가입</a>
							</div>
							<div>
								<span>아이디/비밀번호를 분실하셨나요?</span>
								<a href="/member/idpwFind.php">아이디 / 비밀번호 찾기</a>
							</div>
						</li>
					</ul>
				</fieldset>
				<input type="hidden" name="from" id="from" value=""/>
				<input type="hidden" name="name" id="name" value=""/>
				<input type="hidden" name="email" id="email" value=""/>
				<input type="hidden" name="url" id="url" value="<?=$firsturl?>"/>
				<input type="hidden" name="param" id="param" value="<?=$param?>"/>
				<input type="submit" style="display:none;" />
			</form>
			<div class="member_con">
				<div class="member_box02">
					<div class="member_box02_In">
						 <? if($login_google && ("" <> LOGIN_GOOGLE)) { ?>
							<a href="javascript:;" class="a_bg1" id="google_login">구글 계정으로 로그인 하기</a>
						<? } ?>
						<?  if($login_kakao && ("" <> LOGIN_KAKAO)) {  ?>
							<a href="javascript:;" class="a_bg2" onclick="loginWithKakao()">카카오 계정으로 로그인 하기</a>
						<? } ?>
						<?  if($login_facebook && ("" <> LOGIN_FACEBOOK)) {  ?>
							<a href="javascript:;" class="a_bg3" onclick="facebook_login()">페이스북 계정으로 로그인 하기</a>
						<? } ?>
						<? if($login_naver && ("" <> LOGIN_NAVER)) {  ?>
							<a href="javascript:;" class="a_bg4" id="naver_login"> 네이버 계정으로 로그인하기</a>
						<? } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- 프로그램끝 -->
    <? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
    <? include "sub_footer/sub_footer.php" ?>
</div>

	<!-- sns로그인 시작 -->
	<script type="text/javascript">
		var naver_id_login = new naver_id_login("<?=LOGIN_NAVER?>", "<?=COMPANY_URL?>/sns/naver.php");
		var state = naver_id_login.getUniqState();
		$('#forState').val(state);
		naver_id_login.setState(state);
		naver_id_login.setPopup();
		naver_id_login.setCustomButton(false, "naver_login");
		naver_id_login.init_naver_id_login();
	</script>

	<script src="https://apis.google.com/js/api:client.js"></script>
	<script>
		var googleUser = {};
		  var startApp = function() {
		    gapi.load('auth2', function(){
		      // Retrieve the singleton for the GoogleAuth library and set up the client.
		      auth2 = gapi.auth2.init({
		        client_id: '<?=LOGIN_GOOGLE?>',
		        cookiepolicy: 'single_host_origin',
		        // Request scopes in addition to 'profile' and 'email'
		        scope: 'profile email'
		      });
		      attachSignin(document.getElementById('google_login'));
		    });
		  };
		  function attachSignin(element) {   
			  auth2.attachClickHandler(element, {},
			  function(googleUser) {
				var profile = googleUser.getBasicProfile();
				$("#id").val(profile.getId());
				$('#password').val('snspass');
				$('#name').val(profile.getName());
				$('#email').val(profile.getEmail());
				$('#from').val('google');
				$("#login").submit();
				}, function(error) {
					//alert(JSON.stringify(error, undefined, 2));
				});
		   }
	</script>
	<script>startApp();</script>
	<!-- sns로그인 끝 -->

</body>
</html>