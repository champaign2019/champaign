<?session_start();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<?
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/member/Member.class.php";
include $_SERVER['DOCUMENT_ROOT']."/include/humanCheck.php";

$url = $_REQUEST['url'] == '' ? "/" : substr($_REQUEST['url'], 0, strrpos($_REQUEST['url'], '?'));
$param = $_REQUEST[param];

$result = 0;
$member = new Member($pageRows, "member", $_REQUEST);
$data = $member->dormantRead($_REQUEST);
$id = $data['id'];
$data['dormant_state'] = 0;
$mr = $member->resetDormantStateId($data);
if($mr > 0) {
	$result = $member->deleteDormant($_REQUEST['no']);
}

if($result > 0) {
	unset($_SESSION['human_no']);
	unset($_SESSION['human_id']);
	unset($_SESSION['human_name']);
	unset($_SESSION['human_email']);
	unset($_SESSION['human_tel']);
	unset($_SESSION['human_cell']);
?>
<script src="/js/jquery-1.8.0.min.js"></script>
<form name="next" id="next" action="login_human03.php" method="post">
	<input type="hidden" name="id" value="<?=$id?>">
	<input type="hidden" name="url" value="<?=$url?>">	
	<input type="hidden" name="param" value="<?=$param?>">	
</form>
<script>
	$("#next").submit();
</script>
<?
} else {
?>
<script>
	alert("다시 시도하여 주세요.");
	location.href=history.back(-1);
</script>
<? } ?>