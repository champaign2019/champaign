<?session_start();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<?
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/member/Member.class.php";
include $_SERVER['DOCUMENT_ROOT']."/include/humanCheck.php";

$url = $_REQUEST['url'] == '' ? "/" : substr($_REQUEST['url'], 0, strrpos($_REQUEST['url'], '?'));
$param = $_REQUEST[param];

$member = new Member($pageRows, "member", $_REQUEST);
$data = $member->dormantRead($_REQUEST);

?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/include/headHtml.php" ?>
<script>
var thistype;
var code;
var timer;
var SetTime;
function send(type) {
	clearInterval(timer);
	timer = null;
	SetTime = 300;
	if(type == 'cell') {
		thistype = 'cell';
		
		if($("#cell").val() == '') {
			alert("휴대번호를 입력하세요.");
			$("#cell").focus();
			return false;
		} else {
		
		$.ajax({
			url:"ajaxSend.php",
			type:"POST",
			dataType:"json",
			data: {
				type:'cell', 
				id:'<?=$data[id]?>',
				cell:$("#cell").val()
			},
			success : function(data) {
				if(data.result > 0) {
					alert(data.msg);
					code = data.code;
					delay(type); //카운트시작
				} else {
					alert(data.msg);
					console.log(data.result);
					return;
				}
			}, 
			error : function(err) {
				console.log(err);
				alert("인증번호 발송을 다시 시도해주세요.");
				return;
			}
		});
		
		}
	} else if (type="email") {
		thistype = 'email';
		
		if($("#email").val() == '') {
			alert("이메일을 입력하세요.");
			$("#email").focus();
			return false;
		} else {
			
		$.ajax({
			url:"ajaxSend.php",
			type:"POST",
			dataType:"json",
			data: {
				type:'email', 
				id:'<?=$data[id]?>',
				email:$("#email").val()
			},
			success : function(data) {
				if(data.result > 0) {
					alert(data.msg);
					code = data.code;
					delay(); //카운트시작
				} else {
					alert(data.msg);
					console.log(data.result);
					return;
				}
			}, 
			error : function(err) {
				alert("인증번호 발송을 다시 시도해주세요.");
				return;
			}
		});
		
		}
	} else {
		alert("인증번호를 발송할 유형을 선택하세요.");
		return false;
	}
	
}
function delay() {
	$("#btn_"+thistype).text("다시받기");
 	timer = setInterval('timechk()', 1000);
}

function timechk() {
	var m = Math.floor(SetTime / 60) + "분 " + (SetTime % 60) + "초";
	var msg = "["+m+"]";
 	$("#time_"+thistype).text(msg);
	SetTime--;
	if(SetTime < 0) {
		code = "";
		clearInterval(timer);
		timer = null;
	}
}

function next() {
var thiscode = "";
	if(thistype == 'cell') {
		thiscode = $("#cellNo").val();
	} else if(thistype == 'email') {
		thiscode = $("#emailNo").val();
	} else {
		alert("인증번호를 발송받고 입력하세요.");
		return false;
	}
	
	// 인증번호 확인
	if( thiscode == '') {
		alert("인증번호를 입력해주세요.");
		return;
	} else if ( (thiscode != '') && (thiscode != code) ) {
		alert("인증번호를 확인해주세요.");
		return;
	} else {
		$("#login").submit();
	}
}
</script>
</head>
<body>

	<div id="wrap">
    <? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
    <? include "sub_visual/sub_visual.php" ?>
    <!-- 프로그램시작 -->
	<div class="programCon">
		<div class="login_wrap">
			<form name="login" id="login" method="post" action="ajaxHuman.php">
				<fieldset class="login">
					<p class="login_tit"><span>LOGIN</span> 휴면계정로그인</p>
					<p class="login_subtit"><span>(<?=$data['id']?>)</span>의 재사용을 위해 휴면 계정의 확인이 필요합니다.</p>
					<p class="login_txt">아래 인증 가능한 방법을 선택해 주세요.</p>
					<div class="text_box">
						<div class="phone">
							<div class="input_box">
								<label>
									<input type="radio" name="a1" onclick="display1()" checked/>
									내 정보에 등록된 휴대폰 번호 입력
								</label>
							</div>
							<div id="phone_box">
								<div class="after">
									<input type="text" id="cell" placeholder="휴대폰 번호 전체를 입력해주세요."  maxlength="13"  onkeyup="isNumberOrHyphen(this);" onblur="cvtUserPhoneNumber(this);"/>
									<a onclick="send('cell')" href="javascript:;" id="btn_cell">인증번호 발송</a>
								</div>
								<div class="after">
									<input type="text" placeholder="인증번호 입력" id="cellNo"/>
									<span class="time" id="time_cell"></span>
								</div>
							</div>
						</div>
						
						<div class="email">
							<div class="input_box">
								<label>
									<input type="radio" name="a1"  onclick="display2()" />
									내 정보에 등록된 이메일 주소 입력
								</label>
							</div>
							<div id="email_box">
								<div class="after">
									<input type="text" id="email" placeholder="이메일 주소 전체를 입력해주세요." onblur="emailCheck(this);" />
									<a onclick="send('email')" href="javascript:;" id="btn_email">인증번호 발송</a>
								</div>
								<div class="after">
									<input type="text" placeholder="인증번호 입력" id="emailNo"/>
									<span class="time" id="time_email"></span>
								</div>
							</div>
						</div>
						<a class="human_btn" onclick="next()" href="javascript:;">확인</a>
					</div>
				</fieldset>
				<input type="hidden" name="no" id="no" value="<?=$data['no']?>"/>
				<input type="hidden" name="url" id="url" value="<?=$url?>"/>
				<input type="hidden" name="param" id="param" value="<?=$param?>"/>
			</form>
			
		</div>
	</div>
	<!-- 프로그램끝 -->
    <? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
    <? include "sub_footer/sub_footer.php" ?>
</div>
	<script type="text/javascript">
		function display1(box){
			phone_box.style.display = 'block';
			email_box.style.display = 'none';
		}

		function display2(box){
			phone_box.style.display = 'none';
			email_box.style.display = 'block';
		}

		window.onload=display1
	</script>	
</body>
</html>