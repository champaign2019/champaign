<?session_start();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/member/Member.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/email/SendMail.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/log/Log.class.php";

include $_SERVER['DOCUMENT_ROOT']."/include/logingCheck.php";
include "config.php";

$member = new Member($pageRows, "member", $_REQUEST);

?>
</head>
<body>
<?
if (checkReferer($_SERVER["HTTP_REFERER"])) {
	
	if ($_REQUEST['cmd'] == 'write') {
		$r = $member->insert($_REQUEST);
		if ($r > 0) {

			// 방문자 통계 서비스가 유료일때 통계데이터 update
			if (IS_LOG && LOG_TYPE == 1) {
				$log = new Log($tablename);
				$log->setLog($_COOKIE["CONNECTID_".WEBLOG_NUMBER], $r);
			}

			$data = $member->loginMember($_REQUEST['id']);

			$_SESSION['member_no'] = $data['no'];
			$_SESSION['member_id'] = $data['id'];
			$_SESSION['member_name'] = $data['name'];
			$_SESSION['member_email'] = $data['email'];
			$_SESSION['member_cell'] = $data['cell'];

			//echo returnURLMsg(COMPANY_URL.LOGIN_AFTER_PAGE, '정상적으로 회원가입되었습니다.');
			echo returnURLMsg('login.php', '정상적으로 회원가입되었습니다.');
		} else {
			echo returnURLMsg('write.php', '요청처리중 장애가 발생하였습니다.');
		}

	} else if ($_REQUEST['cmd'] == 'edit') {
		$r = $member->update($_REQUEST);
		if ($r > 0) {
			echo returnURLMsg('edit.php', '정상적으로 수정되었습니다.');
		} else {
			echo returnURLMsg('edit.php', '요청처리중 장애가 발생하였습니다.');
		}

	} else if ($_REQUEST['cmd'] == 'secede') {	// 탈퇴처리
		if ($member->checkId($_REQUEST[id]) > 0) {
			$data = $member->loginMember($_REQUEST[id]);
			$r = $member->updateSecession($data[no]);
			
			if ($r > 0) {
				echo returnURLMsg('secede.php', '정상적으로 탈퇴되었습니다.');
			} else {
				echo returnURLMsg('secede.php', '탈퇴처리를 실패하였습니다.');
			}
		}
	} else if ($_REQUEST[cmd] == 'searchid') {	// 아이디 찾기
		$data = $member->searchId($_REQUEST[name], $_REQUEST[email]);
		if ($data) {
			$title = "[".COMPANY_NAME."]".$data[name]."님의 아이디입니다.";
			foreach ( $data as $row){
				$contents .= "<br/>아이디 : ".$row[id];
			}
			$mailForm = getURLMakeMailForm(COMPANY_URL, EMAIL_FORM);
			$mailForm = str_replace(":SUBJECT", $title, $mailForm);
			$mailForm = str_replace(":CONTENT", $contents, $mailForm);

			$sendmail = new SendMail();
			$sendmail->send(COMPANY_EMAIL, COMPANY_NAME, $_REQUEST['email'], $title, $mailForm);

			echo returnURLMsg('idpwFind.php', '메일 발송이 정상적으로 처리되었습니다.');
		} else {
			echo returnURLMsg('idpwFind.php', '입력하신 정보는 존재 하지 않습니다.');
		}

	} else if ($_REQUEST[cmd] == 'searchpw') {	// 패스워드 찾기
		$data = $member->searchPw($_REQUEST[name], $_REQUEST[email], $_REQUEST[id]);
		$temppass = getRandomStr(10);


		if ($temppass && $member->updateTempPass($data[no], $temppass)) {
			$title = "[".COMPANY_NAME."]".$data[name]."님의 임시비밀번호입니다.";
			$contents = "<br/>임시비밀번호 : ".$temppass;

			$mailForm = getURLMakeMailForm(COMPANY_URL, EMAIL_FORM);
			$mailForm = str_replace(":SUBJECT", $title, $mailForm);
			$mailForm = str_replace(":CONTENT", $contents, $mailForm);

			$sendmail = new SendMail();
			$sendmail->send(COMPANY_EMAIL, COMPANY_NAME, $_REQUEST['email'], $title, $mailForm);

			echo returnURLMsg('idpwFind.php', '임시비밀번호가 정상적으로 메일발송되었습니다.');
		}
		else {
			echo returnURLMsg('idpwFind.php', '임시비밀번호 생성 실패');
		}
	} else if ($_REQUEST[cmd] == 'changepw') {	// 휴면계정 패스워드 변경
		
		$chk = $member->checkPw($_REQUEST);

		if ($chk > 0) {

			$chk = $member->beforePw($_REQUEST);
			if ($chk > 0) {
				echo returnURLMsg($member->getQueryStringAddParam(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'login_human03.php'), 0, $_REQUEST, 'id',  $_REQUEST['id']), '기존 비밀번호와 다른 비밀번호를 입력해주세요.');
			} else {
				$r = $member->updatePw($_REQUEST);
				if($r > 0) {
					echo returnURLMsg($member->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'login_human04.php'), $_REQUEST['no'], $_REQUEST), '정상적으로 수정되었습니다.');
				} else {
					echo returnURLMsg($member->getQueryStringAddParam(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'login_human03.php'), 0, $_REQUEST, 'id',  $_REQUEST['id']), '비밀번호 변경을 다시 시도하여주세요.');
				}
			}
		} else {
			echo returnURLMsg($member->getQueryStringAddParam(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'login_human03.php'), 0, $_REQUEST, 'id',  $_REQUEST['id']), '사용하시던 비밀번호를 정확히 입력해주세요.');
		}
	}

} else {
	echo returnURLMsg($admin->getQueryString(getRemoviSslUrl($_SERVER["REQUEST_URI"], 'index.php'), 0, $_REQUEST), '요청처리중 장애가 발생하였습니다.1');
}
?>
</body>
</html>