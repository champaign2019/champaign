<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<? include $_SERVER['DOCUMENT_ROOT']."/include/headHtml.php" ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?
	$pageTitle = "아이디 찾기";
?>
<script type='text/javascript'>
/*$(function(){
	$(".findTab li").click(function(){
		
		$(".findTab li").removeClass("on");
		$(this).addClass("on");
		if ( $(this).index() == 0 ){
			$(".findCon li dl").eq(1).addClass("off");
			$(".findCon li dl").eq(1).find("input[name=id]").prop("type","hidden");
			$("#cmd").val("searchid");
			$(".login_wrap .login_txt").html("회원님의 이름과 이메일을 입력해주세요.<br />입력하신 이메일주소로 아이디를 보내드립니다");
		} else {
			$(".findCon li dl").eq(1).removeClass("off");
			$(".findCon li dl").eq(1).find("input[name=id]").prop("type","text");
			$("#cmd").val("searchpw");
			$(".login_wrap .login_txt").html("회원님의 이름과 아이디, 이메일을 입력해주세요.<br />입력하신 이메일주소로 임시 비밀번호를 보내드립니다");
		}
	});

	/*$("#idFind").click(function(){
		$(".bgBox input[type=image]").removeClass("okBtn");
		$(".infoBox dl:eq(1)").css("display","none");
		$("#idFind").removeClass("unpick");
		$("#pwFind").addClass("unpick");
		$("#member p:eq(1)").html("회원님의 이름과 이메일을 입력해주세요. <br />입력하신 이메일주소로 아이디를 보내드립니다");
		$("#cmd").val("searchid");
	});
	
	$("#pwFind").click(function(){
		$(".bgBox input[type=image]").addClass("okBtn");
		$(".infoBox dl:eq(1)").css("display","block");
		$("#pwFind").removeClass("unpick");
		$("#idFind").addClass("unpick");
		$("#member p:eq(1)").html("회원님의 이름과 아이디, 이메일을 입력해주세요. <br />입력하신 이메일주소로 임시 비밀번호를 보내드립니다");
		$("#cmd").val("searchpw");
	});
});
*/
function loginCheck(obj){
	
	if(validation(obj)){
		
		//if(!fn_spamCheck()){return false;}
		

		fn_formSpanCheck(obj,key);	
		return true;	
	}else{
		return false;
	}
}
</script>
</head>
<body >

	<div id="wrap">
    <? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
    <? include "sub_visual/sub_visual.php" ?>
    <!-- 프로그램시작 -->
	<div class="programCon">
		<div class="login_wrap pd640">
			
				<fieldset class="login">
					<p class="login_tit"><span>SEARCH</span>; 아이디/비밀번호 찾기</p>
					<p class="login_subtit">잊어버린 아이디/비밀번호를 찾아드립니다.</p>

					<div class="findCon_wrap no01">
						
						<div>
							<form name="board1" id="board1" method="post" action="process.php" onsubmit="return loginCheck(this);">
							<div class="in_hei">
								<div class="up_txt">
									<p>아이디 찾기</p>
									<span>회원님의 이름과 이메일을 입력해주세요. 입력하신 이메일주소로 아이디를 보내드립니다.</span>
								</div>
								<ul class="findCon">
									<li>
										<dl>
											<dt><label for="name"><img class="name_icon" src="/img/name_icon.png" alt="" /></label></dt>
											<dd><input data-value="이름을 입력하세요" type="text" id="name" name="name" placeholder="이름" onkeyup="javascript:if(event.keyCode == 13){$('#board1').submit();}" /></dd>
										</dl>
										<dl>
											<dt><label for="email"><img class="email_icon" src="/img/email_icon.png" alt="" /></label></dt>
											<dd><input data-value="이메일을 입력하세요." type="text" id="email" name="email" placeholder="이메일" onkeyup="javascript:if(event.keyCode == 13){$('#board1').submit();}" /></dd>
										</dl>
									</li>
								</ul>
							</div>
							<div class="under_btn">
								<a class="login_btn" style="cursor:pointer;" onclick="$('#board1').submit();" >확인</a>
							</div>
							<input type="hidden" name="cmd" id="cmd" value="searchid"/>
						</form>
						</div>
						
						
						<div>
							<form name="board2" id="board2" method="post" action="process.php" onsubmit="return loginCheck(this);">
							<div class="in_hei">
								<div class="up_txt">
									<p>비밀번호 찾기</p>
									<span>회원님의 이름과 아이디, 이메일을 입력해주세요. 입력하신 이메일주소로 임시 비밀번호를 보내드립니다</span>
								</div>
								<ul class="findCon">
									<li>
										<dl>
											<dt><label for="name"><img class="name_icon" src="/img/name_icon.png" alt="" /></label></dt>
											<dd><input data-value="이름을 입력하세요" type="text" id="name" name="name" placeholder="이름" onkeyup="javascript:if(event.keyCode == 13){$('#board2').submit();}" /></dd>
										</dl>
										<dl>
											<dt><label for="id"><img src="/img/id_icon.png" alt="" /></label></dt>
											<dd><input data-value="아이디를 입력하세요." type="text" id="id" name="id" placeholder="아이디" onkeyup="javascript:if(event.keyCode == 13){$('#board2').submit();}" /></dd>
										</dl>
										<dl>
											<dt><label for="email"><img class="email_icon" src="/img/email_icon.png" alt="" /></label></dt>
											<dd><input data-value="이메일을 입력하세요." type="text" id="email" name="email" placeholder="이메일" onkeyup="javascript:if(event.keyCode == 13){$('#board2').submit();}" /></dd>
										</dl>
									</li>
								</ul>
							</div>
							<div class="under_btn">
								<a class="login_btn" style="cursor:pointer;" onclick="$('#board2').submit();" >확인</a>
							</div>
							<input type="hidden" name="cmd" id="cmd" value="searchpw"/>
						</form>
						</div>
						
					</div>
				</fieldset>
				
		</div>
	</div>
	<!-- 프로그램끝 -->
    <? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
    <? include "sub_footer/sub_footer.php" ?>
</div>

</body>
</html>