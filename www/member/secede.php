<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";
include $_SERVER['DOCUMENT_ROOT']."/include/logingCheck.php";

	$pageTitle = "회원 탈퇴";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<? include $_SERVER['DOCUMENT_ROOT']."/include/headHtml.php" ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type='text/javascript'>

function loginCheck(obj){

	if(validation(obj)){
		var form = $("#board");
		fn_formSpanCheck(form,key);	
		
		return true;
	}else{
		return false;
	}
}

</script>
</head>

<body onload="getObject('id').focus();">
	<div id="wrap">
    <? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
    <? include "sub_visual/sub_visual.php" ?>
    <!-- 프로그램시작 -->
	<div class="programCon">
		<div class="login_wrap">
			<form name="board" id="board" method="post" action="<?=getSslCheckUrl($_SERVER['REQUEST_URI'], 'process.php')?>" onsubmit="return loginCheck(this);">
				<fieldset class="login">					
					<p class="login_tit"><span>MEMBER</span>; 회원탈퇴</p>
					<p class="login_subtit">탈퇴를하시면 회원의 개인정보가 삭제됩니다.</p>
					<p class="login_txt">고객님께서 회원가입이 되어있으신지 확인합니다.<br/>아이디와 비밀번호를 정확하게 기입하시고 탈퇴신청을 해주시기 바랍니다.</p>

					<ul class="secedeCon">
						<li>
							<dl>
								<dt><label for="id"><img src="/img/id_icon.png" alt="" /></label></dt>
								<dd><input type="text" id="id" name="id" placeholder="ID" data-value="아이디를 입력하세요." /></dd>
							</dl>
							<dl>
								<dt><label for="password"><img class="password_icon" src="/img/password_icon.png" alt="" /></label></dt>
								<dd><input data-value="비밀번호를 입력하세요." type="password" id="password" name="password" placeholder="Password" /></dd>
							</dl>
							
						</li>
					</ul>
					<div class="sec_ucon">
						<a class="login_btn" href="javascript:;" onclick="$('#board').submit()">회원탈퇴</a>
					</div>

				</fieldset>
				<input type="hidden" name="cmd" id="cmd" value="secede" />
			</form>
		</div>
	</div>
	<!-- 프로그램끝 -->
    <? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
    <? include "sub_footer/sub_footer.php" ?>
</div>
</body>
</html>