<?session_start();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<?
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/member/Member.class.php";
include $_SERVER['DOCUMENT_ROOT']."/include/humanCheck.php";

$url = $_REQUEST['url'] == '' ? "/" : substr($_REQUEST['url'], 0, strrpos($_REQUEST['url'], '?'));
$param = $_REQUEST[param];

$member = new Member($pageRows, "member", $_REQUEST);
$_REQUEST['sdormanttype'] = DORMANT_TYPE;
$_REQUEST['no'] = $human_no;
$data = $member->dormantRead($_REQUEST);

?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/include/headHtml.php" ?>
</head>
<body>

	<div id="wrap">
    <? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
    <? include "sub_visual/sub_visual.php" ?>
    <!-- 프로그램시작 -->
	<div class="programCon">
		<div class="login_wrap human_wrap">
			<form name="login" id="login" method="post" action="/member/login_human02.php" >
				<fieldset class="login">
					<p class="login_tit"><span>LOGIN</span> 휴면계정로그인</p>
					<p class="login_subtit">어서오세요 <span>(<?=$data['name']?>)</span> 님</p>
					<p class="login_txt"><span>(<?=COMPANY_NAME?>)</span>을 오랫동안 이용하지 않으셔서</p>
					<p class="login_txt">회원님의 계정이 휴면 상태로 전환되었습니다.</p>
					<p class="login_txt"><span>(<?=COMPANY_NAME?>)</span> 전체 서비스를 다시 이용하고 싶은 경우에는,<br class="none640"/>아래 '휴면 계정 재사용' 을 선택해주시길 바랍니다.</p>
					<a class="human_btn" onclick="$('#login').submit();" href="javascript:;">휴면 계정 재사용</a>
				</fieldset>
				<input type="hidden" name="no" id="no" value="<?=$data['no']?>"/>
				<input type="hidden" name="url" id="url" value="<?=$url?>"/>
				<input type="hidden" name="param" id="param" value="<?=$param?>"/>
			</form>
			
		</div>
	</div>
	<!-- 프로그램끝 -->
    <? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
    <? include "sub_footer/sub_footer.php" ?>
</div>
</body>
</html>