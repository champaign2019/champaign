<?session_start();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<?
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/member/Member.class.php";

include $_SERVER['DOCUMENT_ROOT']."/include/logingCheck.php";
include "config.php";

$url = $_REQUEST['url'] == '' ? "/" : substr($_REQUEST['url'], 0, strrpos($_REQUEST['url'], '?'));
$param = $_REQUEST[param];

$member = new Member($pageRows, "member", $_REQUEST);
$data = $member->getData($_REQUEST);
?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/include/headHtml.php" ?>
</head>
<body>

	<div id="wrap">
    <? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
    <? include "sub_visual/sub_visual.php" ?>
    <!-- 프로그램시작 -->
	<div class="programCon">
		<div class="login_wrap">
			<form name="login" id="login" method="post" action="/member/login.php" >
				<fieldset class="login">
					<p class="login_tit"><span>LOGIN</span> 휴면계정로그인</p>
					<p class="login_subtit"></p>
					<p class="login_txt"></p>
					<div class="text_box">
						<p><span>(<?=$data['id']?>)</span>의 휴면 상태가 해제되었습니다.</p>
						<p>다시 로그인 해주세요</p>
						<a href="javascript:;" onclick="$('#login').submit();" class="human_btn">로그인 화면</a>
					</div>
				</fieldset>
				<input type="hidden" name="url" id="url" value="<?=$url?>"/>
				<input type="hidden" name="param" id="param" value="<?=$param?>"/>
			</form>
			
		</div>
	</div>	
	<!-- 프로그램끝 -->
    <? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
    <? include "sub_footer/sub_footer.php" ?>
</div>
</body>
</html>