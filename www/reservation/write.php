<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/member/Member.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/reservation/Reservation.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Hospital.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Clinic.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Doctor.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/environment/Stipulation.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/include/logingCheck.php";

include "config.php";

if ($boardgrade == 3 || $boardgrade == 2)  { // 게시판 접근 권한 처리

include $_SERVER['DOCUMENT_ROOT']."/include/gradeCheck.php";

}


	$reser = new Reservation($pageRows, $tablename, $_REQUEST);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/include/headHtml.php" ?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/boardConfig/config.php" ?>
<script>

function goSave(obj) {
	
	
	if(validation(obj)){
		<?if(!$isCellSum){?>
		if($("[name=tel1]").val() != "" && $("input[name=tel2]").val() != "" && $("input[name=tel3]").val() != ""){
		$("input[name=tel]").val($("[name=tel1]").val()+"-"+$("input[name=tel2]").val()+"-"+$("input[name=tel3]").val())
		}
		
		if($("[name=cell1]").val() != "" && $("input[name=cell2]").val() != "" && $("input[name=cell3]").val() != ""){
		$("input[name=cell]").val($("[name=cell1]").val()+"-"+$("input[name=cell2]").val()+"-"+$("input[name=cell3]").val())
		}
		
		<?}?>
		
		if($("input[name=email1]").val() != "" && $("[name=email2]").val() != ""){
			$("input[name=email]").val($("input[name=email1]").val().trim()+"@"+$("[name=email2]").val().trim());
		}
	
		if(!fn_spamCheck()){return false;}
		
		var form = $("#frm");

		fn_formSpanCheck(form,key);	
		return true;	
	}else{
		return false;
	}
}
</script>
</head>
<body>

	<div id="wrap">
    <? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
    <? include "sub_visual/sub_visual.php" ?>
    <!-- 프로그램시작 -->
	<form method="post" name="frm" id="frm" action="<?=getSslCheckUrl($_SERVER['REQUEST_URI'], 'process.php')?>" onsubmit="return goSave(this);">
	<div class="programCon">
		<?
			if (!$loginCheck) { 
			
			$st = new Stipulation();
			$sv = $st->getData();
			
		?>
		<div class="join" data-aos="fade-down" data-aos-delay="400">
			<div class="joinTop">
				<div class="ScrollLy">
					<?=str_replace("#company_name#", COMPANY_NAME, $sv['privacy_mini_text'])?>	
					<!-- //policy -->
				</div>
				<div class="jcheck">
					<input type="checkbox" data-value="<?=getMsg('alt.checkbox.agree') ?>" id="agree02" name="agree02" value="" title="<?=getMsg('message.tbody.agree_ch') ?>" checked="checekd"/>
					<label for="agree02"> <?=getMsg('message.tbody.agree') ?></label>
				</div>
				<!-- //jcheck -->
			</div>
		</div>
		<?}?>
		<!-- //join -->
		<? 
		if($branch || $clinic) { 
		?>
		<div class="writeForm writeForm_top" data-aos="fade-down" data-aos-delay="400">
			<table>
				<colgroup>
					<?
					if($branch){
					?>
					<col class="writeForm_col01" />
					<col width="*" />
					<?
					}
					?>
					<?
					if($clinic){
					?>
					<col class="writeForm_col01" />
					<col width="*" />
					<?
					}
					?>
				</colgroup>			
				<tr>
					<?
					if($branch){
						$hospital = new Hospital(999, $_REQUEST);
						$hResult = $hospital->branchSelect();
					?>
					<th>지점정보</th>
					<td>
						
						<select name="hospital_fk" id="hospital_fk" onchange="" data-value="<?=getMsg('alert.text.shospital_fk') ?>">
							<option value=""><?=getMsg('lable.option.branch') ?></option>
							<? while ($row=mysql_fetch_assoc($hResult)) { ?>				
							<option value="<?=$row['no']?>"><?=$row['name']?></option>
							<? } ?>
						</select>	
					</td>
					<?
					}
					?>
					<?
					if($clinic){
						$clinicObj = new Clinic(999, $_REQUEST);
						$clinicList = $clinicObj->selectBoxList(0,($branch ? 0 : DEFAULT_BRANCH_NO),$_REQUEST['sclinic_fk']);
						
					?>
					<th><?=getMsg('th.clinic_fk') ?></th>
					<td>
						<select name="clinic_fk" id="clinic_fk" data-value="<?=getMsg('alt.select.sclinic_fk') ?>" >
							<?= $clinicList ?>
						</select>
					</td>
					<?
					}
					?>
				</tr>
				<?
					if($doctor){
						$doctorObj = new Doctor(999, $_REQUEST);
						$DoctorList = $doctorObj->selectBoxList(0,0,($branch ? 0 : DEFAULT_BRANCH_NO),0,$_REQUEST['sdoctor_fk']);
				?>
				<tr>
					<th scope="row"><label for="">의료진</label></th>
					<td>
					<select name="doctor_fk" id="doctor_fk" class="w100m25" data-value="의료진을 선택해 주세요.">
						<?= $DoctorList ?>
					</select>
					</td>
				</tr>
				<?
				}
				else {
				?>
				<input type="hidden" id="doctor_fk" value="1"/>
				<?
				}
				?>
			</table>
		</div>
		<?
		}
		?>
		<div class="schedule_wrap" data-aos="fade-down" data-aos-delay="400">
			<div class="schedule"></div>
			<div class="schedule_time"></div>
		</div>
		<div class="writeForm" data-aos="fade-down" data-aos-delay="400">
			<table>
				<colgroup>
					<col class="writeForm_col01" />
					<col width="*" />
				</colgroup>
				<tr>
					<th>이름</th>
					<td>
						<input data-value="작성자을 입력하세요." id="name" name="name" class="inputTxt inputName" type="text" value="<?=$_SESSION['member_name']?>" />
					</td>
				</tr>
				
				<tr>
					<th>비밀번호</th>
					<td>
						<input data-value="비밀번호를 입력하세요." name="password" id="password" class="inputPass" type="password"  maxlength="100" />
					</td>
				</tr>
				<tr>
					<th><?=getMsg('th.tel') ?></th>
					<td>
						<?
						if(!$isCellSum){
						?>
						<select class="selectNum" name="tel1" id="tel1">
							<?=getOptsTel(getSplitIdx($_SESSION['member_tel'], "-", 0))?>
						</select>
						<input data-value="연락처를 입력하세요." name="tel2" id="tel2" class="inputNum" type="text" value="<?=getSplitIdx($_SESSION['member_tel'], "-", 1)?>" maxlength="4" onkeyup="isNumberOrHyphen2(this);" />
						<input data-value="연락처를 입력하세요." name="tel3" id="tel3" class="inputNum" type="text" value="<?=getSplitIdx($_SESSION['member_tel'], "-", 2)?>" maxlength="4" onkeyup="isNumberOrHyphen2(this);" />
						<input type="hidden" name="tel" id="tel" value=""/>
						<?}else{?>
						<input type="text" name="tel" id="tel" value="<?=$_SESSION['member_tel']?>" onkeyup="isNumberOrHyphen(this);" onblur="cvtUserPhoneNumber(this);" data-value="연락처를 입력하세요." />
						
						<?}?>
					</td>
				</tr>
				<tr>
					<th><?=getMsg('th.cell') ?></th>
					<td>
						<?
						if(!$isCellSum){
						?>
						<select class="selectNum" name="cell1" id="cell1">
							<?=getOptsCell(getSplitIdx($_SESSION['member_cell'], "", 0))?>
						</select>
						<input data-value="<?=getMsg('alert.text.cell') ?>" name="cell2" id="cell2" class="inputNum" type="text" value="<?=getSplitIdx($_SESSION['member_cell'], "-", 1)?>" maxlength="4" onkeyup="isNumberOrHyphen2(this);" />
						<input data-value="<?=getMsg('alert.text.cell') ?>" name="cell3" id="cell3" class="inputNum" type="text" value="<?=getSplitIdx($_SESSION['member_cell'], "-", 2)?>" maxlength="4" onkeyup="isNumberOrHyphen2(this);" />
						<input type="hidden" name="cell" id="cell" value=""/>
						<?}else{ ?>
						<input type="text" name="cell" id="cell" value="<?=$_SESSION['member_cell']?>" onkeyup="isNumberOrHyphen(this);" onblur="cvtUserPhoneNumber(this);" data-value="<?=getMsg('alert.text.cell') ?>" />
						<?}?>
						<?
						if($useSmsChk){
						?>
						<span class="label_wrap"><input type="checkbox" id="Num_check" name="telconsult" value="1" /><label for="Num_check"><?=getMsg('th.telconsult') ?></label></span>
						<?
						}
						?>
					</td>
				</tr>
				<tr>
					<th><?=getMsg('th.email') ?></th>
					<td class="mail_type">
						<input data-value="이메일을 입력하세요." name="email1" id="email1" class="inputEmail" type="text" value="<?=getSplitIdx($_SESSION['member_email'], "@", 0)?>" maxlength="70" /><span class="email_txt">@</span>
						<select class="selecEmail" name="email2" id="email2" data-value="이메일을 선택하세요">
							<option value="">선택하세요</option>
							<?=getOptsEmail(getSplitIdx($_SESSION['member_email'], "@", 1))?>
						</select>
					</td>
				</tr>
				
				<tr>
					<th><label for="gender">초/재진 구분</label></th>
					<td>
						<input type="radio" id="newold1" name="newold" value="1" checked="checked" title="초진" data-value="초진/재진 구분을 선택하세요." />	<label for="treat01">초진</label>
						<input type="radio" id="newold2" name="newold" value="2" title="재진" data-value="초진/재진 구분을 선택하세요." /><label for="treat02">재진</label>
					</td>
				</tr>
				<tr>
					<th>내원경로</th>
					<td>
						<select id="route" name="route" title="내원경로를 선택해주세요." data-value="내원경로를 선택해주세요.">
							<?=getRouteNameType(0)?>
						</select>
					</td>
				</tr>
				<tr>
					<th><?=getMsg('th.contents') ?></th>
					<td><textarea name="etc" id="etc" cols="30" rows="10"></textarea></td>
				</tr>
				
			</table>
		</div>
		<div class="writeForm_btn" data-aos="fade-down" data-aos-delay="400">
			<a href="javascript:;" id="w_btn" onclick="$('#frm').submit()"><?=getMsg('btn.submit') ?></a>
			<a href="<?=$reser->getQueryString('write.php', 0, $_REQUEST)?>"><?=getMsg('btn.cancel') ?></a>
		</div>
		<input type="hidden" name="cmd" id="cmd" value="write"/>
		<input type="hidden" name="email" id="email" value=""/>
		<input type="hidden" name="curMonth" id="curMonth" value=""/>
		<? if($loginCheck) { ?>
		<input type="hidden" name="member_fk" value="<?= $_SESSION['member_no'] ?>"/>
		<? } ?>
		<? if (!$branch) { ?>
			<input type="hidden" name="hospital_fk" id="hospital_fk" value="<?=DEFAULT_BRANCH_NO?>"/>
		<?}?>
		<? if (!$doctor) { ?>
		<input type="hidden" name="doctor_fk" id="doctor_fk" value="<?=DEFAULT_DOCTOR_NO?>"/>
		<?
		}
		?>
	</div>
	</form>
	<!-- 프로그램끝 -->
    <? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
    <? include "sub_footer/sub_footer.php" ?>
</div>
    <? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
    <? include "sub_footer/sub_footer.php" ?>
</div>
</body>
</html>