<?session_start();
include "config.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<? include $_SERVER['DOCUMENT_ROOT']."/include/headHtml.php" ?>
<? include $_SERVER['DOCUMENT_ROOT']."/include/boardConfig/config.php" ?>
<script>
<? if ($loginCheck) { ?>
document.location.href = "index.php";
<? }else{ ?>
var str = "회원가입 후 예약하셨다면 '예' 버튼을 \n비회원으로 예약하셨다면 '취소' 버튼을 클릭해 주세요!";
if(confirm(str)) {
<?
	unset($_SESSION['nomember_name']);
	unset($_SESSION['nomember_password']);
?>
	document.location.href = "/member/login.php?url=/reservation/index.php";
}
<? 
}
?>


function loginCheck(obj) {
	
	
	if(validation(obj)){
		var form = $("#login");

		fn_formSpanCheck(form,key);
		$("#login").submit();
		
	}else{
		return;
	}
}
</script>
</head>
<body onLoad="$('#name').focus();">


	<div id="wrap">
    <? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
    <? include "sub_visual/sub_visual.php" ?>
    <!-- 프로그램시작 -->
	<div class="programCon">
		<div class="login_wrap">
			<form name="login" id="login" method="post" action="/include/nonMemberLogin.php" >
				<fieldset class="login">
					
					<p class="login_tit"><span>RESERVATION</span></p>
					<p class="login_subtit">비회원으로 예약하신 경우</p>
					<p class="login_txt">예약시 입력하신 이름과 비밀번호를 입력해주세요.<br />예약하신 내용을 확인하실 수 있습니다.</p>

					<ul class="secedeCon">
						<li>
							<dl>
								<dt><label for="id"><img class="name_icon" src="/img/name_icon.png" alt="" /></label></dt>
								<dd><input type="text" id="name" name="name" placeholder="name" data-value="<?=getMsg('alert.text.name2') ?>" onkeyup="if(event.keyCode == 13){loginCheck(this)}" /></dd>
							</dl>
							<dl>
								<dt><label for="password"><img class="password_icon" src="/img/password_icon.png" alt="" /></label></dt>
								<dd><input type="password" name="password" id="password" placeholder="Password" data-value="패스워드을 입력하세요." onkeyup="if(event.keyCode == 13){loginCheck(this)}" /></dd>
							</dl>
						</li>						
					</ul>
					<div class="sec_ucon">
						<a class="login_btn" href="javascript:;" onclick="loginCheck(this);">예약확인</a>
					</div>
					<input type="hidden" name="url" value="/reservation/index.php" />
				</fieldset>
			</form>
		</div>
	</div>
	<!-- 프로그램끝 -->
    <? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
    <? include "sub_footer/sub_footer.php" ?>
</div>


</body>
</html>