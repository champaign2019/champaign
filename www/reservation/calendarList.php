<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/ReserCalendar.class.php";


include "config.php";

$rc = new ReserCalendar();

$smonth = $_REQUEST['smonth'];

$subSmonth;
if (strlen($_REQUEST['smonth']) > 7) {
	$subSmonth = $smonth;
	$smonth = substr($smonth, 0, 7);
}

//달력의 년월 표기 및 이전달 다음달 처리 시작
$checkToday = getToday();
$preMonth = getYearMonth($smonth, -1);
$curMonth = getYearMonth($smonth, 0);
$nexMonth = getYearMonth($smonth, 1);

$hospital_fk = 0;

$result = $rc->reservationCalendar($curMonth, $_REQUEST['shospital_fk']); // 달력 불러오기
$listSize = mysql_num_rows($result);




?>
<div class="calender_wrap">
<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="" class="calendar_top_table">
	<thead>
		<tr>
			<th>
				<img src="/manage/img/calend_left.png" alt="이전달" onclick="calendarSelectMonth('<?=$preMonth?>');calendarNewList(<?= chkIsset($_REQUEST['shospital_fk']) ?>,'<?=$preMonth?>');">
				<span class="now"><?=substr($curMonth, 0, 4)?>년 <?=substr($curMonth, 5)?>월</span>
				<img src="/manage/img/calend_right.png" alt="다음달" onclick="calendarSelectMonth('<?=$nexMonth?>');calendarNewList(<?= chkIsset($_REQUEST['shospital_fk']) ?>,'<?=$nexMonth?>');">
			</th>
		</tr>
	</thead>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="메모시간선택표입니다" class="calendar_table">
	<colgroup>
		<col width="16%" />
		<col width="14%" />
		<col width="14%" />
		<col width="14%" />
		<col width="14%" />
		<col width="14%" />
		<col width="14%" />
	</colgroup>
	<thead>
		<tr class="day_name">
			<th scope="col" class="first sun">일</th>
			<th scope="col">월</th>
			<th scope="col">화</th>
			<th scope="col">수</th>
			<th scope="col">목</th>
			<th scope="col">금</th>
			<th scope="col" class="sat">토</th>
		</tr>
	</thead>
	<tbody>
		<?  if($listSize == 0) { ?>
		<tr height=20 align=center bgcolor=FFFFFF style="padding-top:2px">
			<td colspan=7>달력이 존재하지 않습니다.</td>
		</tr>
		<? } else { 
			$i = 0;
			while ($row=mysql_fetch_assoc($result)) {
				$name = $row['name'];				// 요일명(1 = 일요일, 2 = 월요일......)
				$possibleTerm = $row['possibleTerm'];				// 예약 가능
				$today = $row['today'];							// 오늘 날짜(yyyy-mm-dd))
				$holiday = $row['holiday'];	// 공휴일명 값이 없으면 공휴일
				$holidayName = "";										// 휴일 풍선 도움말
				$style = "ca1";											// 휴일에 대한 스타일 지정
				$styleMouse = "";
				$timeButton = "";
				$styleDay = substr($today,8);


				
				// 일요일과 토요일인 경우 날짜의 컬러를 변경한다.	
				if ($name == 1 || $name == 7) {
					$styleDay = ($name == 1 ? "<span class='sun'>" : "<span class='sat'>").substr($today,8)."</span></b>";
					$style = ($name == 1 ? "sun" : "sat");
				} else if($holiday != "") {
					$style = "sun";
				}
				
				if ($possibleTerm == 0 && !$holiday) {
					$style = "care";
					$styleMouse = " onmouseout=this.className='".$style."' onmouseover=this.className='ent' style='cursor:pointer;' onclick=\"calendarSelectDayConfirm('".$today."');\"";
					$holidayName = "진료일";
				} else {
					if ($holiday) {
						$styleDay = "<span class='color1'>".substr($today,8)."</span>";
						$holidayName = $holiday;
						$style = "first";
					}
				}

				if ($possibleTerm == 0 && $holiday == "") {
					$style = "onBg";
					$styleMouse = " onmouseout=\"this.className='".$style."'\" onmouseover=\"this.className='ent'\" style='cursor:pointer;' onclick=\"calendarSelectDayConfirm('".$today."');\"";
					$holidayName = "진료일";
				} else if ($possibleTerm == 2) {
					$styleMouse = " onclick=\"alert('지점과 의료진을 먼저 선택하세요.');\"";
					$holidayName = "지점과 의료진을 먼저 선택하세요!";
					$styleDay=substr($today,8);
				} else {
					if ($holiday != "") {
						$styleDay = substr($today,8);
						$holidayName = $holiday;
					}
				}

				
				if ($checkToday == $today) {
					
					$style = "on";
					$styleDay = "<p>".substr($today,8)."</p>";
					//오늘날짜도 예약가능할경우
					//styleMouse = " onmouseout=\"this.className='"+style+"'\" onmouseover=\"this.className='ent'\"  style='cursor:pointer;' onclick=\"calendarSelectDayConfirm('"+today+"');\"";
					$tyleMouse = " onmouseout=\"this.className='".$style."'\"";
				}
		
				if ($i == 0 || 1 == $name) { 
		?>	
			<tr height=20 align=center bgcolor=FFFFFF>
		<?		}
				if ( $i == 0) {
						for ($j = 0 ; $j < $name-1 ; $j++) {
		?>
							<td <?=$j == 0 ? "class='first'" : "" ?>></td>
		<?
					} 
				}
		?>
				<td class="<?=$style?>"<?=$styleMouse?> title="<?=$holidayName?>" <?=$name != 1 && $style == 'first' ? "" : ""?>>
					<span><?=$styleDay?></span>
				</td>
		<?
				if ($i == $listSize-1) {
					for ($k = 0 ; $k < 7-$name ; $k++) {
		?>
						<td></td>
		<?
					} 
				}
		?>
		<?
				if ($i == $listSize-1 || 7 == $name) {  
		?>
					</tr>
		<?
				}
		$i++;
			}
		}	
		?>
	</tbody>
</table>
	<div class="under_wrap">
		<p>
			<span class="cal_check"></span><label for="cal_check">상담가능날짜</label>
		</p>
		<div class="sr_wrap">
			<p>예약선택일 <input type="text" class="reserDate_input" id="reserdate" name="reserdate" value="<?=$subSmonth?>" /> 일</p>
			<i></i>
			<p>예약선택시간 <input type="text" class="no02" id="resertime" name="resertime" value="" />시</p>
		</div>
	</div>
</div>
