<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/member/Member.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/log/Log.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/reservation/Reservation.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Hospital.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Clinic.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Doctor.class.php";

include "config.php";

if ($boardgrade == 3) {
	include $_SERVER['DOCUMENT_ROOT']."/include/gradeCheck.php";
}

$reser = new Reservation($pageRows, $tablename, $_REQUEST);
$_REQUEST['orderby'] = $orderby; //정렬 배열 선언
$data = $reser->getData($_REQUEST['no']);

$member = new Member(999, 'member', $_REQUEST);
$hospital = new Hospital(999, $_REQUEST);
$clinicObj = new Clinic(999, $_REQUEST);
$doctor = new Doctor(999, $_REQUEST);

if ($loginCheck) {
	$_REQUEST['smember_fk'] = $_SESSION['member_no'];
} else if ($noMemberCheck) {
	$_REQUEST['name'] = $_SESSION['nomember_name'];
	$_REQUEST['password'] = $_SESSION['nomember_password'];
} else {
	echo returnURL("confirm.php");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/include/headHtml.php" ?>
<script>
$(document).ready(function(){
	$("#cancel").click(function(){
		var str = "해당 예약을 취소하시겠습니까?";
		if(confirm(str)){
			location.href="process.php?no=<?=$data['no']?>&cmd=cancel&password=<?=$_REQUEST['password']?>&hospital_fk=<?=$data['hospital_fk']?>&sms_no=<?=$data['sms_no']?>";
		}else{
			return false;
		}
	});
});
</script>
</head>
<body>

	<div id="wrap">
    <? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
    <? include "sub_visual/sub_visual.php" ?>
    <!-- 프로그램시작 -->
	<div class="programCon">
		<div class="writeForm bd_th">
			<table>
				<colgroup>
					<col class="writeForm_col02">
					<col class="writeForm_col03">
					<col class="writeForm_col02">
					<col class="writeForm_col03">
				</colgroup>
				<tr>
					<td colspan="4" class="step"><span>STEP1 예약 정보</span></td>
				</tr>
				<? if($branch) { ?>
				<tr>
					
					<th><span>지점</span></th>
					<td colspan="3"><span><?=$data['hospital_name']?></span></td>
				</tr>
				<? } ?>
				<? if($clinic) { ?>
				<tr>
					<th><span><?=getMsg('th.clinic_fk') ?></span></th>
					<td colspan="3"><span><?=$data['clinic_name']?></span></td>
				</tr>
				<? } ?>
				<? if($doctor) { ?>
				<tr>
					<th><span>의료진</span></th>
					<td colspan="3"><span><?=$data['doctor_name']?></span></td>
				</tr>
				<? } ?>

				<tr>
					<th><span>초/재진</span></th>
					<td colspan="3"><?=getIncipientMeetName($data['newold'])?></td>
				</tr>
				<tr>
					<th><span>예약일</span></th>
					<td><span><?=$data['reserdate']?> <?=$data['resertime']?></span></td>
					<th><span>예약상태</span></th>
					<td><span><?=getReserStateName($data['state'])?></span></td>
				</tr>
				<tr>
					<th><span>완료일</span></th>
					<td><span>
						<? if ($data['confirmdate']) { ?>
						<?=getYMD($data['confirmdate'])?>
						<? } ?>	
					</span></td>
					<th><span>취소일</span></th>
					<td><span>
						<? if ($data['canceldate']) { ?>
						<?=getYMD($data['canceldate'])?>
						<? } ?>
					</span></td>
				</tr>
				<tr>
					<th><span>변경일</span></th>
					<td colspan="3"><span>
						<? if ($data['editdate']) { ?>
						<?=getYMD($data['editdate'])?>
						<? } ?>
					</span></td>
				</tr>
				<tr>
					<td colspan="4" class="step"><span>STEP2 예약자 정보</span></td>
				</tr>
				<tr>
					<th><span>예약자</span></th>
					<td colspan="3"><span><?=$data['name']?></span></td>
				</tr>
				<tr>
					<th><span><?=getMsg('th.cell') ?></span></th>
					<td><span><?=$data['cell']?></span></td>
					<th><span>전화</span></th>
					<td><span><?=$data['tel']?></span></td>
				</tr>
				<tr>
					<th><span><?=getMsg('th.email') ?></span></th>
					<td colspan="3"><span><?=$data['email']?></span></td>
				</tr>
				<tr>
					<th><span>내원경로</span></th>
					<td colspan="<?=$useSmsChk?"1":"3" ?>"><span><?=getRouteTypeName($data['route'])?></span></td>
					<?php if($useSmsChk){ ?>
					<th><span><?=getMsg('th.telconsult') ?></span></th>
					<td><span><?=getTelIncipientName($data['telconsult'])?></span></td>
					<?php } ?>
				</tr>
				<tr>
					<th><span>등록일</span></th>
					<td colspan="3"><span><?=$data['registdate']?></span></td>
				</tr>
				<tr>
					<th><span><?=getMsg('th.contents') ?></span></th>
					<td colspan="3"><?=$data['etc']?></td>
				</tr>
			</table>
		</div>
		<dl class="readBottom_btn">
			<dt>
				<? if($data['state'] < 3) { ?>
				<a class="btns" href="<?=$reser->getQueryString('edit.php', $data['no'], $_REQUEST)?>"><strong>변경</strong></a>
				<?}?> 
				<? if ($data['state'] != 4) { ?>
				<a class="btns" href="#" id="cancel"><strong><?=getMsg('btn.cancel') ?></strong></a>
				<? } ?>
			</dt>
			<dd>
				<a class="btns" href="<?=$reser->getQueryString('index.php', 0, $_REQUEST)?>"><strong>목록</strong></a>
			</dd>
		</dl>
	</div>
	<!-- 프로그램끝 -->
    <? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
    <? include "sub_footer/sub_footer.php" ?>
</div>
</body>
</html>