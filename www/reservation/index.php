<?session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/member/Member.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/reservation/Reservation.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Hospital.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Clinic.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/management/hospital/Doctor.class.php";

include "config.php";

if ($boardgrade == 3) {
	include $_SERVER['DOCUMENT_ROOT']."/include/gradeCheck.php";
}

$reser = new Reservation($pageRows, $tablename, $_REQUEST);
if ($loginCheck) {
	// 로그인 했는데 member_no가 empty인 경우 - 회원인데 empty 인 경우 모든 리스트를 불러오므로 예외처리를 한다.
	if ( empty($_SESSION['member_no']) ) {
		echo returnURL("confirm.php");
	}
	else {
		$_REQUEST['smember_fk'] = $_SESSION['member_no'];
		$rowPageCount = $reser->getCountLogin($_REQUEST);
	}
} else if ($noMemberCheck) {
	$_REQUEST['name'] = $_SESSION['nomember_name'];
	$_REQUEST['password'] = $_SESSION['nomember_password'];
	$rowPageCount = $reser->getCountNonLogin($_REQUEST);
} else {
	echo returnURL("confirm.php");
}


// 로그인 상태가 아니면서 성명과 비밀번호를 입력 했지만 검색된 결과가 없을 경우 다시 돌려 보냈다.
if ($rowPageCount[0] == 0 && !$loginCheck) {
	echo returnURLMsg("confirm.php", "예약내역이 없습니다.");
}
else {
	$result = $reser->getList($_REQUEST);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/include/headHtml.php" ?>
</head>
<body>

<div id="wrap">
    <? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
    <? include "sub_visual/sub_visual.php" ?>
    <!-- 프로그램시작 -->
	<div class="programCon">
		<div class="program_table">
			<table summary="온라인예약 게시판입니다.">
				<colgroup>
					<col class="w80 none1000" />
					
					<? if($branch) { ?>
					<col class="w130" />
					<?}?>
					<? if($clinic) { ?>
					<col class="w130" />
					<?}?>
					<? if($doctor) { ?>
					<col class="w130" />
					<?}?>
					<col class="w130" />
					<col class="w130" />
					<col class="w100" />
					<col class="w80" />
				</colgroup>
				<thead>
					<tr>
						<th class="none1000"><?=getMsg('th.no') ?></th>
						
						<? if($branch) { ?>
						<th>지점</th>
						<? } ?>
						<? if($clinic) { ?>
						<th><?=getMsg('th.clinic_fk') ?></th>
						<? } ?>
						<? if($doctor) { ?>
						<th>의료진</th>
						<? } ?>
						<th>예약일시</th>
						<th>예약자명</th>
						<th>접수일</th>
						<th>예약상태</th>
					</tr>
				</thead>
				<tbody class="programBody">
					<? if ($rowPageCount[0] == 0) { ?>
					<tr class="bbsno">
						<td colspan="8">등록된 게시물이 없습니다</td>
					</tr>
					<?
						} else {
							$i=0;
							while ($row=mysql_fetch_assoc($result)) {
								$targetUrl = "style='cursor:pointer;' onclick=\"location.href='".$reser->getQueryString('read.php', $row['no'], $_REQUEST)."'\"";
					?>
					<tr>
						<td <?=$targetUrl?>><?=$rowPageCount[0] - (($reser->reqPageNo-1)*$pageRows) - $i?></td>
						<? if ($branch) { ?>
						<td <?=$targetUrl?>><span class="branch01"><?=$row['hospital_name']?></span></td>
						<? } ?>
						<? if ($clinic) { ?>
						<td <?=$targetUrl?>><span class="branch02"><?=$row['clinic_name']?></span></td>
						<? } ?>
						<? if ($doctor) { ?>
						<td <?=$targetUrl?>><?=$row['doctor_name']?></td>
						<? } ?>
						<td <?=$targetUrl?>><?=$row['reserdate']?></td>
						<td <?=$targetUrl?>><?=$row['name']?></td>
						<td <?=$targetUrl?>><?=$row['registdate']?></td>
						<td <?=$targetUrl?>><?=getReserStateName($row['state'])?></td>
					</tr>
					<?
							$i++;
							}
						}
					?>
				</tbody>
			</table>
		</div>
		<div class="mo_programPage"><a href="javascript:;" onclick="more()"><?=getMsg('btn.more') ?><span>+</span></a></div>
		<dl class="write_btn">
			<dd><a href="write.php">진료예약</a></dd>
		</dl>
		
		<?=pageListUser($reser->reqPageNo, $rowPageCount[1], $reser->getQueryString('index.php', 0, $_REQUEST))?>
	</div>
	<!-- 프로그램끝 -->
    <? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
    <? include "sub_footer/sub_footer.php" ?>
</div>

</body>
</html>
<? } ?>