<?
	session_start();

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<title>병원프로그램PHP 사용자 페이지</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0, user-scalable=no" />

<link href="/include/style.css" rel="stylesheet" type="text/css"/>
<script language="javascript" src="/include/Function.js"></script>
<script type="text/javascript" src="/js/jquery-1.8.0.min.js"></script>
<script>
	$(window).on("load resize", function(){
		if( $(window).width() < 1000 ){
			$('#frs').attr('cols', '0,*');
			$('#mframe').attr('rows','40,*');
		}else{
			$('#frs').attr('cols', '200,*');
			$('#mframe').attr('rows','0,*');
		}
	});
	$(window).load(function(){
	    $(".all_m_btn", frames[0].document).click(function(){
	    	$(".pro_m_wrap", frames[2].document).toggleClass("view");
	    });
	});
</script>

</head>
<frameset topmargin=0 leftmargin=0 border="0" rows="0,*" id="mframe">
	<frame src="/mobile_menu_btn.php"  />
	<frameset topmargin=0 leftmargin=0 cols="200,*" frameborder="NO" border="0" framespacing="0" onLoad="this.focus();" id="frs">
		<frame src="/leftmenu.php" name="headFrame" noresize title="좌측 메뉴"/>
		<frame src="/consult/consult/index.php" name="mainFrame" title="화면"/>
	</frameset>
</frameset>
</html>