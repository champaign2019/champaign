<?
/*


*/

include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/db/DBConnection.class.php";

class Doctor {

	// 검색 파라미터 (초기 개발시 검색조건 세팅필요)
	var $param = array (
					"stype",
					"sval",
					"shospital_fk",
					"sclinic_fk"
				);

	var $pageRows;			// 페이지 로우수
	var $startPageNo=0;		// limit 시작페이지
	public $reqPageNo=1;	// 요청페이지
	var $conn;

	// 생성자
	function Doctor($pageRows=0, $request='') {
		$this->pageRows = $pageRows;
		$this->reqPageNo = ($request['reqPageNo'] == 0) ? 1 : $request['reqPageNo'];	// 요청페이지값 없을시 1로 세팅
		if ($request['reqPageNo'] > 0) {
			$this->startPageNo = ($request['reqPageNo']-1) * $this->pageRows;
		}
	}

	// 검색 파라미터 queryString 생성
	function getQueryString($page="", $no=0, $request='') {	
		$str = '';
		
		for ($i=0; $i<count($this->param); $i++) {
			if ($request[$this->param[$i]]) {
				$str .= $this->param[$i]."=".urlencode($request[$this->param[$i]])."&";
			}
		}

		if ($no > 0) $str .= "no=".$no;			// no값이 있을 경우에만 파라미터 세팅 (페이지 이동시 no필요 없음)

		$return = '';
		if ($str) {
			$return = $page.'?'.$str;
		} else {
			$return = $page.'?';
		}

		return $return;
	}

	// sql WHERE절 생성
	function getWhereSql($p) {
		$whereSql = " WHERE 1=1";
		if ($p['sval']) {
			if ($p['stype'] == 'all') {
				$whereSql .= " AND ( (name like '%".$p['sval']."%' ) or (title like '%".$p['sval']."%' ) or (contents like '%".$p['sval']."%') ) ";
			} else {
				$whereSql .= " AND (".$p['stype']." LIKE '%".$p['sval']."%' )";
			}
		}
		if ($p['shospital_fk']) {
			$whereSql .= " AND hospital_fk = ".$p['shospital_fk'];
		}
		if ($p['sclinic_fk']) {
			$whereSql .= " AND clinic_fk = ".$p['sclinic_fk'];
		}
		return $whereSql;
	}


	// 전체로우수, 페이지카운트
	function getCount($param = "") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$whereSql = $this->getWhereSql($param);	// where절
		$sql = " SELECT COUNT(*) AS cnt FROM doctor ".$whereSql;

		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		$row=mysql_fetch_array($result);
		$totalCount = $row['cnt'];
		$pageCount = getPageCount($this->pageRows, $totalCount);

		$data[0] = $totalCount;
		$data[1] = $pageCount;

		return $data;
	}

	// 목록
	function getList($param='') {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$whereSql = $this->getWhereSql($param);	// where절

		$sql = "
			SELECT *,
				no, hospital_fk, clinic_fk, name, email, memo, imagename, imagename_org, image_alt,
				(SELECT name FROM hospital WHERE hospital.no = doctor.hospital_fk) AS hospital_name,
				(SELECT name FROM clinic WHERE clinic.no = doctor.clinic_fk) AS clinic_name 
			FROM doctor
			".$whereSql."
			ORDER BY no DESC LIMIT ".$this->startPageNo.", ".$this->pageRows." ";

		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		return $result;
	}

	// 관리자 등록
	function insert($req="") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		//$gno = $this->getMaxGno();
		$sql = "
			INSERT INTO doctor (
				hospital_fk, clinic_fk, name, email, ";
		if ($req[imagename]) {
			$sql .= "imagename, imagename_org, image_alt, ";
		}
		$sql .= "memo
			) VALUES (
			$req[hospital_fk],
			$req[clinic_fk],
			'$req[name]', 
			'$req[email]',";
		if ($req[imagename]) {
			$sql .= "'$req[imagename]', '$req[imagename_org]', '$req[image_alt]', ";
		}
		$sql .= "
			'".$req[memo]."'
			)";

		mysql_query($sql, $conn);

		$sql = "SELECT LAST_INSERT_ID() AS lastNo";
		$result = mysql_query($sql, $conn);
		$row = mysql_fetch_array($result);
		$lastNo = $row['lastNo'];
		mysql_close($conn);
		return $lastNo;
	}

	// 수정
	function update($req="") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		// 기존 목록이미지 삭제
		if ($req[listimage_chk] == "1") {
			mysql_query("UPDATE doctor SET imagename='', imagename_org='' WHERE no=".$req[no], $conn);
		}

		$sql = "
			UPDATE doctor SET 
				hospital_fk='$req[hospital_fk]', clinic_fk='$req[clinic_fk]', name='$req[name]', email='$req[email]', ";
		if ($req[imagename]) {
			$sql .= "imagename='$req[imagename]', imagename_org='$req[imagename_org]', image_alt='$req[image_alt]', ";
		}
		$sql .= " memo='$req[memo]'
			WHERE no = ".$req['no'];

		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		return $result;
	}

	// 삭제
	function delete($no=0) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = " DELETE FROM doctor WHERE no = ".$no;

		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		return $result;
	}

	// 목록
	function getData($no=0) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = "
			SELECT *,
				(SELECT name FROM hospital WHERE hospital.no = doctor.hospital_fk) AS hospital_name,
				(SELECT name FROM clinic WHERE clinic.no = doctor.clinic_fk) AS clinic_name 
			FROM doctor
			WHERE no = ".$no;
		
		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		$data = mysql_fetch_assoc($result);

		return $data;
	}

	/**
	 * 의료진목록을 selectbox형태로 가져온다.
	 * @param type 권한과 관계없이 병원목록 처리 (0:전체, 1:권한에따라처리, 2:회원글쓰기 및 수정용, 3:회원목록)
	 * @param grade 관리자권한 (1이하=전체, 2이이상=해당지점만)
	 * @param hospital_fk 해당관리자의 소속병원fk
	 * @param clinic_fk 해당지점의 진료과목fk
	 * @param reqHospital_fk 목록중 선택되어어야할 의료진fk
	 * @return selectbox HTML
	 */
	function selectBoxList($type, $grade, $hospital_fk, $clinic_fk, $reqDoctor_fk) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$option = "";
		if ($type == 0) {
			$option .= "<option value='0'>의료진선택</option>";
		} else if ($type == 3) {
			$option .= "<option value='0'>전체</option>";
		}

		$sql = "
			SELECT *
			FROM doctor WHERE 0=0";
		if ($hospital_fk > 0) {
			$sql .= " AND hospital_fk = $hospital_fk";
		}
		if ($clinic_fk > 0) {
			$sql .= " AND clinic_fk = $clinic_fk";
		}

		if ($hospital_fk != 1) {
			$sql .= " AND hospital_fk > ".DEFAULT_BRANCH_NO;
		}

		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		while ($row = mysql_fetch_assoc($result)) {
			$option .= "<option value='".$row[no]."' ".getSelected($row[no], $reqDoctor_fk).">".$row[name]."</option>";
		}

		return $option;
	}
	

}


?>