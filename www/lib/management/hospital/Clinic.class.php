<?
/*


*/

include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/db/DBConnection.class.php";

class Clinic {

	// 검색 파라미터 (초기 개발시 검색조건 세팅필요)
	var $param = array (
					"shospital_fk"
				);

	var $pageRows;			// 페이지 로우수
	var $startPageNo=0;		// limit 시작페이지
	public $reqPageNo=1;	// 요청페이지
	var $conn;

	// 생성자
	function Clinic($pageRows=0, $request='') {
		$this->pageRows = $pageRows;
		$this->reqPageNo = ($request['reqPageNo'] == 0) ? 1 : $request['reqPageNo'];	// 요청페이지값 없을시 1로 세팅
		if ($request['reqPageNo'] > 0) {
			$this->startPageNo = ($request['reqPageNo']-1) * $this->pageRows;
		}
	}

	// 검색 파라미터 queryString 생성
	function getQueryString($page="", $no=0, $request='') {	
		$str = '';
		
		for ($i=0; $i<count($this->param); $i++) {
			if ($request[$this->param[$i]]) {
				$str = $str.$this->param[$i]."=".urlencode($request[$this->param[$i]])."&";
			}
		}

		if ($no > 0) $str = $str."no=".$no;			// no값이 있을 경우에만 파라미터 세팅 (페이지 이동시 no필요 없음)

		$return = '';
		if ($str) {
			$return = $page.'?'.$str;
		} else {
			$return = $page;
		}

		return $return;
	}

	// sql WHERE절 생성
	function getWhereSql($p) {
		$whereSql = "";
		if ($p['shospital_fk']) {
			$whereSql .= " WHERE hospital_fk = ".$p['shospital_fk'];
		}
		return $whereSql;
	}


	// 전체로우수, 페이지카운트
	function getCount($param = "") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$whereSql = $this->getWhereSql($param);	// where절
		$sql = " SELECT COUNT(*) AS cnt FROM clinic ".$whereSql;

		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		$row=mysql_fetch_array($result);
		$totalCount = $row['cnt'];
		$pageCount = getPageCount($this->pageRows, $totalCount);

		$data[0] = $totalCount;
		$data[1] = $pageCount;

		return $data;
	}

	// 목록
	function getList($param='') {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$whereSql = $this->getWhereSql($param);	// where절

		$sql = "
			SELECT *, 
				(SELECT name FROM hospital WHERE hospital.no = clinic.hospital_fk) AS hospital_name 
			FROM clinic 
			".$whereSql."
			ORDER BY no DESC LIMIT ".$this->startPageNo.", ".$this->pageRows." ";
	
		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		return $result;
	}

	// 등록
	function insert($req="") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = "
			INSERT INTO clinic (
				hospital_fk, name
			) VALUES (
				'$req[hospital_fk]', '$req[name]'
			)";
		mysql_query($sql, $conn);

		$sql = "SELECT LAST_INSERT_ID() AS lastNo";
		$result = mysql_query($sql, $conn);
		$row = mysql_fetch_array($result);
		$lastNo = $row['lastNo'];
		mysql_close($conn);
		return $lastNo;
	}

	// 수정
	function update($req="") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = "
			UPDATE clinic SET
				hospital_fk = '$req[hospital_fk]', name = '$req[name]'
			WHERE no = ".$req['no'];

		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		return $result;
	}

	// 삭제
	function delete($no=0) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = " DELETE FROM clinic WHERE no = ".$no;

		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		return $result;
	}

	// 목록
	function readClinicName($req='') {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$whereSql = $this->getWhereSql($param);	// where절

		$sql = "
			SELECT 
				name
			FROM clinic
			WHERE no = ".$req['no'];
		
		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		$data = mysql_fetch_assoc($result);
		$clinicName = $data['name'];

		return $clinicName;
	}

	/**
	 * 목록을 selectbox형태로 가져온다.
	 * @param type 권한과 관계없이 병원목록 처리 (0:전체, 1:권한에따라처리, 3:목록)
	 * @param hospital_fk 해당관리자의 소속병원fk
	 * @param clinic_fk 목록중 선택되어어야할 진료과목fk
	 * @return selectbox HTML
	 */
	function selectBoxList($type, $hospital_fk, $clinic_fk) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$option = "";

		if ($type == 0) {
			$option .= "<option value=''>진료과목 선택</option>";
		} else if ($type == 3) {
			$option .= "<option value=''>전체</option>";
		}

		$sql = "
			SELECT * FROM clinic WHERE 1 = 1
			";
		if ($hospital_fk != 0) {
			$sql .= " and hospital_fk = $hospital_fk ";
		}
		
		if ($hospital_fk != 1) {
			$sql .= " and hospital_fk > ".DEFAULT_BRANCH_NO."  ";
		}

		$sql .= " ORDER BY no ASC ";
		

		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		

		while ($row = mysql_fetch_assoc($result)) {
			$option .= "<option value='".$row[no]."' ".getSelected($row[no], $clinic_fk)." data-hospital='".$row['hospital_fk']."'>".$row[name]."</option>";
		}

		return $option;
	}

	// 진료과목 목록
	function getClinicSelect() {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = "
			SELECT * FROM clinic ORDER BY no ASC";
	
		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		return $result;
	}

	
	function selectBoxListProduct($type, $gubun, $branch_fk, $type_fk) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$option = "";

		if ($type == 0) {
			$option .= "<option value=''>소분류 선택</option>";
		} else if ($type == 3) {
			$option .= "<option value=''>전체</option>";
		}

		$sql = "
			SELECT * FROM clinic WHERE 1 = 1
			";
		if ($branch_fk != 0) {
			$sql .= " and branch_fk = $branch_fk ";
		}
		
		$sql .= " ORDER BY no ASC ";
		

		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		

		while ($row = mysql_fetch_assoc($result)) {
			$option .= "<option value='".$row[no]."' ".getSelected($row[no], $type_fk)." data-branch='".$row['branch_fk']."'>".$row[name]."</option>";
		}

		return $option;
	}

	function radioList($type, $branch_fk, $type_fk) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$option = "";

	
		$sql = "
			SELECT * FROM clinic WHERE 1 = 1
			";
		if ($branch_fk != 0) {
			$sql .= " and hospital_fk = $branch_fk ";
		}
		
		$sql .= " ORDER BY no ASC ";
		
		

		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		

		while ($row = mysql_fetch_assoc($result)) {
			$option .= "<label><input type='radio' name='clinic_fk' id='clinic_fk' value='".$row[no]."' ".getChecked($row[no], $type_fk)." data-branch='".$row['branch_fk']."' /> ".$row[name]."</label>";
		}

		return $option;
	}

}


?>