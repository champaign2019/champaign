<?
/*


*/

include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/db/DBConnection.class.php";

class Hospital {

	// 검색 파라미터 (초기 개발시 검색조건 세팅필요)
	var $param = array (
					"stype",
					"sval"
				);

	var $pageRows;			// 페이지 로우수
	var $startPageNo=0;		// limit 시작페이지
	public $reqPageNo=1;	// 요청페이지
	var $conn;

	// 생성자
	function Hospital($pageRows=0, $request='') {
		$this->pageRows = $pageRows;
		$this->reqPageNo = ($request['reqPageNo'] == 0) ? 1 : $request['reqPageNo'];	// 요청페이지값 없을시 1로 세팅
		if ($request['reqPageNo'] > 0) {
			$this->startPageNo = ($request['reqPageNo']-1) * $this->pageRows;
		}
	}

	// 검색 파라미터 queryString 생성
	function getQueryString($page="", $no=0, $request='') {	
		$str = '';
		
		for ($i=0; $i<count($this->param); $i++) {
			if ($request[$this->param[$i]]) {
				$str = $str.$this->param[$i]."=".urlencode($request[$this->param[$i]])."&";
			}
		}

		if ($no > 0) $str = $str."no=".$no;			// no값이 있을 경우에만 파라미터 세팅 (페이지 이동시 no필요 없음)

		$return = '';
		if ($str) {
			$return = $page.'?'.$str;
		} else {
			$return = $page;
		}

		return $return;
	}

	// sql WHERE절 생성
	function getWhereSql($p) {
		$whereSql = " WHERE 0=0 ";
		if ($p['sval']) {
			if ($p['stype'] == 'all') {
				$whereSql = $whereSql." AND ( (name like '%".$p['sval']."%' ) or (id like '%".$p['sval']."%' ) or (memo like '%".$p['sval']."%') ) ";
			} else {
				$whereSql = $whereSql." AND (".$p['stype']." LIKE '%".$p['sval']."%' )";
			}
		}
		return $whereSql;
	}


	// 전체로우수, 페이지카운트
	function getCount($param = "") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$whereSql = $this->getWhereSql($param);	// where절
		$sql = " SELECT COUNT(*) AS cnt FROM hospital ".$whereSql;

		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		$row=mysql_fetch_array($result);
		$totalCount = $row['cnt'];
		$pageCount = getPageCount($this->pageRows, $totalCount);

		$data[0] = $totalCount;
		$data[1] = $pageCount;

		return $data;
	}

	// 목록
	function getList($param='') {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$whereSql = $this->getWhereSql($param);	// where절

		$sql = "
			SELECT * FROM hospital 
			".$whereSql."
			ORDER BY no DESC LIMIT ".$this->startPageNo.", ".$this->pageRows." ";
		
		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		return $result;
	}

	// 등록
	function insert($req="") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = "
			INSERT INTO hospital (
				name, consult_write_user, consult_write_admin, consult_reply_user, consult_admin_tel,
				reser_write_user, reser_write_admin, reser_edit_user, reser_edit_admin, reser_cancel_user,
				reser_cancel_admin, reser_end_user, reser_admin_tel, reser_possible_day, reser_possible_term,
				reser_start_time, reser_end_time, consult_sms_timechk, consult_sms_starttime, consult_sms_endtime,
				consult_sms_msg,  reser_sms_timechk, reser_sms_starttime, reser_sms_endtime, reser_sms_msg, 
				reser_sms_editmsg, reser_sms_cancelmsg, reser_sms_confirmmsg
			) VALUES (
				'$req[name]', '$req[consult_write_user]', '$req[consult_write_admin]', '$req[consult_reply_user]', '$req[consult_admin_tel]',
				'$req[reser_write_user]', '$req[reser_write_admin]', '$req[reser_edit_user]', '$req[reser_edit_admin]', '$req[reser_cancel_user]',
				'$req[reser_cancel_admin]', '$req[reser_end_user]', '$req[reser_admin_tel]', ".chkIsset($req[reser_possible_day]).", ".chkIsset($req[reser_possible_term]).",
				'$req[reser_start_time]', '$req[reser_end_time]', '$req[consult_sms_timechk]', '$req[consult_sms_starttime]', '$req[consult_sms_endtime]',
				'$req[consult_sms_msg]',  '$req[reser_sms_timechk]', '$req[reser_sms_starttime]', '$req[reser_sms_endtime]', '$req[reser_sms_msg]', 
				'$req[reser_sms_editmsg]', '$req[reser_sms_cancelmsg]', '$req[reser_sms_confirmmsg]'
			)";
		mysql_query($sql, $conn);
		$sql = "SELECT LAST_INSERT_ID() AS lastNo";
		$result = mysql_query($sql, $conn);
		$row = mysql_fetch_array($result);
		$lastNo = $row['lastNo'];
		mysql_close($conn);
		return $lastNo;
	}

	// 수정
	function update($req="") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = "
			UPDATE hospital SET
				name = '$req[name]', consult_write_user = '$req[consult_write_user]', consult_write_admin = '$req[consult_write_admin]', consult_reply_user = '$req[consult_reply_user]', consult_admin_tel = '$req[consult_admin_tel]',	
				reser_write_user = '$req[reser_write_user]', reser_write_admin = '$req[reser_write_admin]', reser_edit_user = '$req[reser_edit_user]', reser_edit_admin = '$req[reser_edit_admin]', reser_cancel_user = '$req[reser_cancel_user]',
				reser_cancel_admin = '$req[reser_cancel_admin]', reser_end_user = '$req[reser_end_user]', reser_admin_tel = '$req[reser_admin_tel]', reser_possible_day = ".chkIsset($req[reser_possible_day]).", reser_possible_term = ".chkIsset($req[reser_possible_term]).",	
				reser_start_time = '$req[reser_start_time]',  reser_end_time = '$req[reser_end_time]', consult_sms_timechk = '$req[consult_sms_timechk]', consult_sms_starttime = '$req[consult_sms_starttime]',
				consult_sms_endtime = '$req[consult_sms_endtime]', consult_sms_msg = '$req[consult_sms_msg]', reser_sms_timechk = '$req[reser_sms_timechk]', reser_sms_starttime = '$req[reser_sms_starttime]',
				reser_sms_endtime = '$req[reser_sms_endtime]', reser_sms_msg = '$req[reser_sms_msg]' , reser_sms_editmsg = '$req[reser_sms_editmsg]', reser_sms_cancelmsg = '$req[reser_sms_cancelmsg]', reser_sms_confirmmsg = '$req[reser_sms_confirmmsg]'
			WHERE no = ".$req['no'];

		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		return $result;
	}

	// 삭제
	function delete($no=0) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = " DELETE FROM hospital WHERE no = ".$no;

		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		return $result;
	}

	// 목록
	function getData($no) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$whereSql = $this->getWhereSql($param);	// where절

		$sql = "
			SELECT 
				*
			FROM hospital
			WHERE no = ".$no;

		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		$data = mysql_fetch_assoc($result);

		return $data;
	}

	/**
	 * 병원목록을 selectbox형태로 가져온다.
	 * @param type 권한과 관계없이 병원목록 처리 (0:전체, 1:권한에따라처리, 3:목록)
	 * @param grade 관리자권한 (1이하=전체, 2이이상=해당지점만)
	 * @param hospital_fk 해당관리자의 소속병원fk
	 * @param reqHospital_fk 목록중 선택되어어야할 병원fk
	 * @return selectbox HTML
	 */
	function selectBoxList($type, $grade, $hospital_fk, $reqHospital_fk) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$option = "";

		if ($type == 0) {
			$option .= "<option value=''>지점선택</option>";
		} else if ($type == 3) {
			$option .= "<option value=''>전체</option>";
		}

		$sql = "
			SELECT * FROM hospital 
			";
		if ($hospital_fk != 0) {
			$sql .= " WHERE no=1 OR no = $hospital_fk ";
		}
		
		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		while ($row = mysql_fetch_assoc($result)) {
			$option .= "<option value='".$row[no]."' ".getSelected($row[no], $reqHospital_fk).">".$row[name]."</option>";
		}

		return $option;
	}

	// 목록
	function branchSelect() {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = " SELECT * FROM hospital where 1 = 1 and no > ".DEFAULT_BRANCH_NO;
		
		$sql .= " ORDER BY no ASC ";
		


		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		return $result;
	}

	
	function branchSelectProduct($gubun) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = " SELECT * FROM hospital where 1 = 1 and gubun = ".$gubun;
		
		$sql .= " ORDER BY no ASC ";
		


		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		return $result;
	}

}


?>