<?
/*


*/

include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/db/DBConnection.class.php";

class ReserCalendar {

	var $conn;

	// 생성자
	function ReserCalendar() {
	}

	// 검색 파라미터 queryString 생성
	function getQueryString($page="", $no=0, $request='') {	
		$str = '';
		
		for ($i=0; $i<count($this->param); $i++) {
			if (isset($request[$this->param[$i]])) {
				$str = $str.$this->param[$i]."=".urlencode($request[$this->param[$i]])."&";
			}
		}

		if ($no > 0) $str = $str."no=".$no;			// no값이 있을 경우에만 파라미터 세팅 (페이지 이동시 no필요 없음)

		$return = '';
		if ($str) {
			$return = $page.'?'.$str;
		} else {
			$return = $page;
		}

		return $return;
	}

	/**
	 * 정기/법정/병원지정 휴일을 지정
	 * 기존 insertOffHoliday와 insertLegalHoliday를 holiday_type값을 구분으로 같이 사용
	 * @param hospital_fk
	 * @param name(배열)
	 * @param cmd (1:정기휴일, 2:법정휴일, 3:병원지정휴일)
	 * @return
	 */
	function insertHoliday($req) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		// 기존 데이터 삭제
		$holiday_type = 1;	// 정기휴일인 경우
		$cmd = $req['cmd'];
		if ($cmd == 'offHoliday') {
			$holiday_type = 1;
			mysql_query("DELETE FROM holiday WHERE hospital_fk=$req[hospital_fk] and holiday_type='$holiday_type'", $conn);
		} else if ($cmd == 'legalHoliday') {	// 법정휴일인 경우
			$holiday_type = 2;
			mysql_query("DELETE FROM holiday WHERE hospital_fk=$req[hospital_fk] and holiday_type='$holiday_type'", $conn);
		} else if ($cmd == 'exceptionHoliday') {	// 병원지정휴일인 경우
			$holiday_type = 3;
			$sql = "
				DELETE FROM holiday 
				WHERE 
					hospital_fk = $rq[hospital_fk]
					AND holiday_type='$holiday_type'
					AND holiday LIKE '$req[curmonth]%' 
					AND holiday IN (
						SELECT today FROM calendar 
						WHERE 
							today LIKE '$req[curmonth]%'
							AND today NOT IN ( ";
					for ($i=0; $i<count($name); $i++) {
						$sql .= "'".$name[$i]."'";
						if ($i < count($name)-1) {
							$sql .= ", ";
						}
					}
					$sql .= ") ";
			if ($req[name]) {
				mysql_query($sql, $conn);
			}
		}

		
		// 데이터 저장
		if ($req[name]) {
			$name = $req[name];
			$sql = " INSERT INTO holiday (hospital_fk, holiday_type, holiday) ";
			if ($holiday_type == 1) {	// 정기휴일인 경우
				$sql .= " SELECT $req[hospital_fk], '1', today FROM calendar ";
				if (count($name) > 0) {
					$sql .= " WHERE name IN (";
					for ($i=0; $i<count($name); $i++) {
						$sql .= "'".$name[$i]."'";
						if ($i < count($name)-1) {
							$sql .= ", ";
						}
					}
					$sql .= ") ";
				}
			} else if ($holiday_type == 2) {	// 법정휴일인 경우
				$sql .= " SELECT $req[hospital_fk], '2', holiday FROM legalholiday ";
				if (count($name) > 0) {
					$sql .= " WHERE holiday_type IN (";
					for ($i=0; $i<count($name); $i++) {
						$sql .= "'".$name[$i]."'";
						if ($i < count($name)-1) {
							$sql .= ", ";
						}
					}
					$sql .= ") ";
				}
			} else if ($holiday_type == 3) {	// 병원지정휴일인 경우
				$sql .= " SELECT $req[hospital_fk], '3', today FROM calendar ";
				if (count($name) > 0) {
					$sql .= " WHERE today IN (";
					for ($i=0; $i<count($name); $i++) {
						$sql .= "'".$name[$i]."'";
						if ($i < count($name)-1) {
							$sql .= ", ";
						}
					}
					$sql .= ") ";
				}
			}
		}

		echo $sql;
	
		mysql_query($sql, $conn);

	}



	// offHoliday 요일일 지정휴일로 지정되어 있는지 확인
	function getOffHolidayChecked($hospital_fk) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$sql = " 
			SELECT CASE WHEN a.name = b.name THEN 0																											
					ELSE 1 END AS holidayCheck																																
			FROM																																										
				(SELECT name FROM calendar WHERE today  BETWEEN '2020-01-01' AND '2020-12-31' GROUP BY name) AS a	
				LEFT OUTER JOIN																																					
				(SELECT	DAYOFWEEK(holiday) AS name																												
				FROM	holiday 																																						
				WHERE	hospital_fk = $hospital_fk AND holiday_type = '1' AND holiday BETWEEN '2020-01-01' AND '2020-12-31'					
				GROUP BY DAYOFWEEK(holiday)) AS b																													
			ON a.name = b.name																																				
			ORDER BY a.name ";

		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		$holidayCheck;
		
		$i=0;
		while ($row=mysql_fetch_array($result)) {
			$holidayCheck[$i] = $row[holidayCheck];
			$i++;
		}

		return $holidayCheck;
	}

	// offHoliday 요일이 법정휴일로 지정되어 있는지 확인
	function getLegalHolidayChecked($hospital_fk) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$sql = " 
			SELECT  SUM(CASE WHEN a.holiday = b.holiday THEN 1 ELSE 0 END) AS holidayCheck	
			FROM																															
				(SELECT holiday_type, holiday FROM legalholiday) AS a												
				LEFT OUTER JOIN																										
				(SELECT	holiday																										
				FROM	holiday 																											
				WHERE	hospital_fk = $hospital_fk AND holiday_type = '2') AS b													
			ON a.holiday = b.holiday																								
			GROUP BY a.holiday_type
			ORDER BY holiday_type ASC ";

		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		$holidayCheck;
		
		$i=0;
		while ($row=mysql_fetch_array($result)) {
			$holidayCheck[$i] = $row[holidayCheck];
			$i++;
		}

		return $holidayCheck;
	}


	// month달의 hospital_fk인 병원의 휴일 정보 달력을 가져온다
	// holiday가 0이면 근무일
	function readCalendar($month, $hospital_fk) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$sql = "
			SELECT 	a.today, a.name, IFNULL(b.holidayName, 0) AS holiday
			FROM
				(SELECT today, name	FROM calendar WHERE today LIKE '%$month%') AS a
				LEFT OUTER JOIN
				(SELECT	holiday,
						CONVERT(CASE	WHEN MAX(holiday_type) = '1' THEN '정기휴일'
								WHEN MAX(holiday_type) = '2' THEN  (	SELECT CASE WHEN holiday_type ='01' THEN '설날(신정)'
																				WHEN holiday_type ='02' THEN '설날(구정)'
																				WHEN holiday_type ='03' THEN '삼일절'
																				WHEN holiday_type ='04' THEN '어린이날'
																				WHEN holiday_type ='05' THEN '석가탄신일'
																				WHEN holiday_type ='06' THEN '현충일'
																				WHEN holiday_type ='07' THEN '광복절'
																				WHEN holiday_type ='08' THEN '추석'
																				WHEN holiday_type ='09' THEN '개천절'
																				WHEN holiday_type ='10' THEN '한글날'
																				ELSE '성탄절' END
																	FROM legalholiday
																	WHERE holiday = c.holiday)
								ELSE '기타휴일' END USING utf8) AS holidayName
				FROM holiday AS c WHERE hospital_fk = $hospital_fk AND holiday LIKE '%$month%' GROUP BY holiday) as b
			ON a.today = b.holiday
			ORDER BY today ";
		
		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		return $result;
	}

	// 온라인예약에서 month달의 hospital_fk 병원의 휴일정보 달력을 가져옴
	// holiday가 null이 아니면 possibleTerm 0인 날이 예약 가능일
	function reservationCalendar($month, $hospital_fk) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$sql = "";
		if ($hospital_fk == 0) {
			$sql = "SELECT today, name, '' as holiday, 2 as possibleTerm FROM calendar WHERE today LIKE '$month%'  order by today ASC";
		} else {
			$sql = "
				SELECT c.today, c.name,  IFNULL(c.holiday,'') AS holiday,																																
					CASE WHEN c.today BETWEEN d.startDay AND d.endDay THEN 0																				
							ELSE 1 END AS possibleTerm																																
				FROM																																											
					(SELECT 	a.today, a.name, b.holidayName AS holiday																								
					FROM																																										
						(SELECT today, name	FROM calendar WHERE today LIKE '$month%') AS a																			
						LEFT OUTER JOIN																																					
						(SELECT	holiday,																																					
								CONVERT(CASE	WHEN MAX(holiday_type) = '1' THEN '정기휴일'																	
												WHEN MAX(holiday_type) = '2' THEN  (	SELECT CASE WHEN holiday_type ='01' THEN '설날(신정)'	
																								WHEN holiday_type ='02' THEN '설날(구정)'									
																								WHEN holiday_type ='03' THEN '삼일절'											
																								WHEN holiday_type ='04' THEN '어린이날'										
																								WHEN holiday_type ='05' THEN '석가탄신일'									
																								WHEN holiday_type ='06' THEN '현충일'											
																								WHEN holiday_type ='07' THEN '광복절'											
																								WHEN holiday_type ='08' THEN '추석'												
																								WHEN holiday_type ='09' THEN '개천절'											
																								WHEN holiday_type ='10' THEN '한글날'											
																								ELSE '성탄절' END
																					FROM legalholiday																					
																					WHERE holiday = c.holiday)																		
												ELSE '기타휴일' END USING utf8) AS holidayName																			
						FROM holiday AS c WHERE hospital_fk = $hospital_fk AND holiday LIKE '$month%' GROUP BY holiday) AS b											
					ON a.today = b.holiday) AS c,																																	
					(SELECT	DATE_ADD(CURDATE(), INTERVAL reser_possible_day DAY) as startDay, 														
							DATE_ADD(CURDATE(), INTERVAL reser_possible_day+reser_possible_term DAY) AS endDay									
					FROM hospital WHERE no = $hospital_fk) AS d																															
				ORDER BY today ";
		}
//		echo $sql;
		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		return $result;
	}

	function reservationCalendarMo($month, $hospital_fk) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$sql = "";
		if ($hospital_fk == 0) {
			$sql = "SELECT today, name, '' as holiday, 2 as possibleTerm FROM calendar WHERE today > '$month'  order by today ASC";
		} else {
			$sql = "
				SELECT c.today, c.name,  c.holiday,																																
					CASE WHEN c.today BETWEEN d.startDay AND d.endDay THEN 0																				
							ELSE 1 END AS possibleTerm																																
				FROM																																											
					(SELECT 	a.today, a.name, b.holidayName AS holiday																								
					FROM																																										
						(SELECT today, name	FROM calendar WHERE today >= '$month') AS a																			
						LEFT OUTER JOIN																																					
						(SELECT	holiday,																																					
								CONVERT(CASE	WHEN MAX(holiday_type) = '1' THEN '정기휴일'																	
												WHEN MAX(holiday_type) = '2' THEN  (	SELECT CASE WHEN holiday_type ='01' THEN '설날(신정)'	
																								WHEN holiday_type ='02' THEN '설날(구정)'									
																								WHEN holiday_type ='03' THEN '삼일절'											
																								WHEN holiday_type ='04' THEN '어린이날'										
																								WHEN holiday_type ='05' THEN '석가탄신일'									
																								WHEN holiday_type ='06' THEN '현충일'											
																								WHEN holiday_type ='07' THEN '광복절'											
																								WHEN holiday_type ='08' THEN '추석'												
																								WHEN holiday_type ='09' THEN '개천절'											
																								WHEN holiday_type ='10' THEN '한글날'											
																								ELSE '성탄절' END
																					FROM legalholiday																					
																					WHERE holiday = c.holiday)																		
												ELSE '기타휴일' END USING utf8) AS holidayName																			
						FROM holiday AS c WHERE hospital_fk = $hospital_fk AND holiday >= '$month' GROUP BY holiday) AS b											
					ON a.today = b.holiday) AS c,																																	
					(SELECT	DATE_ADD(CURDATE(), INTERVAL reser_possible_day DAY) as startDay, 														
							DATE_ADD(CURDATE(), INTERVAL reser_possible_day+reser_possible_term DAY) AS endDay									
					FROM hospital WHERE no = $hospital_fk) AS d																															
				ORDER BY today ";
		}
		
		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		return $result;
	}

	function isSelectHoliday($hospital_fk, $holiday) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$sql = " SELECT COUNT(*) AS cnt FROM holiday WHERE holiday = '$holiday'	and hospital_fk='$hospital_fk' ";

		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		$row=mysql_fetch_array($result);
		$cnt = $row[cnt];
		$isHoly = false;
		if ($cnt > 0) {
			$isHoly = true;
		}

		return $isHoly;
	}

}


?>