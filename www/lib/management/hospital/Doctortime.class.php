<?
/*


*/

include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/db/DBConnection.class.php";

class Doctortime {

	// 검색 파라미터 (초기 개발시 검색조건 세팅필요)
	var $param = array (
					"shospital_fk",
					"sdoctor_fk"
				);

	var $pageRows;			// 페이지 로우수
	var $startPageNo=0;		// limit 시작페이지
	public $reqPageNo=1;	// 요청페이지
	var $conn;

	// 생성자
	function Doctortime($pageRows=0, $request='') {
		$this->pageRows = $pageRows;
		$this->reqPageNo = ($request['reqPageNo'] == 0) ? 1 : $request['reqPageNo'];	// 요청페이지값 없을시 1로 세팅
		if ($request['reqPageNo'] > 0) {
			$this->startPageNo = ($request['reqPageNo']-1) * $this->pageRows;
		}
	}

	// 검색 파라미터 queryString 생성
	function getQueryString($page="", $no=0, $request='') {	
		$str = '';
		
		for ($i=0; $i<count($this->param); $i++) {
			if ($request[$this->param[$i]]) {
				$str = $str.$this->param[$i]."=".urlencode($request[$this->param[$i]])."&";
			}
		}

		if ($no > 0) $str = $str."no=".$no;			// no값이 있을 경우에만 파라미터 세팅 (페이지 이동시 no필요 없음)

		$return = '';
		if ($str) {
			$return = $page.'?'.$str;
		} else {
			$return = $page;
		}

		return $return;
	}

	// sql WHERE절 생성
	function getWhereSql($p) {
		$whereSql = " WHERE 0=0 ";
		if ($p['shospital_fk']) {
			$whereSql .= " AND hospital_fk = ".$p['shospital_fk'];
		}
		if ($p['sdoctor_fk']) {
			$whereSql .= " AND doctor_fk = ".$p['sdoctor_fk'];
		}
		return $whereSql;
	}


	// 전체로우수, 페이지카운트
	function getCount($param = "") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$whereSql = $this->getWhereSql($param);	// where절
		$sql = " SELECT COUNT(*) AS cnt FROM doctortime ".$whereSql;

		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		$row=mysql_fetch_array($result);
		$totalCount = $row['cnt'];
		$pageCount = getPageCount($this->pageRows, $totalCount);

		$data[0] = $totalCount;
		$data[1] = $pageCount;

		return $data;
	}

	// 목록
	function getList($param='') {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$whereSql = $this->getWhereSql($param);	// where절

		$sql = "
			SELECT *,
				(SELECT name FROM hospital WHERE no = doctortime.hospital_fk) AS hospital_name,
				(SELECT name FROM doctor WHERE no = doctortime.doctor_fk) AS doctor_name
			FROM doctortime 
			".$whereSql."
			ORDER BY no DESC LIMIT ".$this->startPageNo.", ".$this->pageRows." ";
		
		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		return $result;
	}

	// 등록
	function insert($req="") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = "
			INSERT INTO doctortime (
				hospital_fk, doctor_fk, startday, mon_start_time, mon_end_time, tue_start_time, tue_end_time,
				wed_start_time, wed_end_time, thu_start_time, thu_end_time, fri_start_time, fri_end_time,
				sat_start_time, sat_end_time, sun_start_time, sun_end_time, spe_start_time, spe_end_time,
				lunch_start_time, lunch_end_time, dinner_start_time, dinner_end_time, time_interval
			) VALUES (
				".chkIsset($req[hospital_fk]).", ".chkIsset($req[doctor_fk]).", '$req[startday]', '$req[mon_start_time]', '$req[mon_end_time]', '$req[tue_start_time]', '$req[tue_end_time]',
				'$req[wed_start_time]', '$req[wed_end_time]', '$req[thu_start_time]', '$req[thu_end_time]', '$req[fri_start_time]', '$req[fri_end_time]',
				'$req[sat_start_time]', '$req[sat_end_time]', '$req[sun_start_time]', '$req[sun_end_time]', '$req[spe_start_time]', '$req[spe_end_time]',
				'$req[lunch_start_time]', '$req[lunch_end_time]', '$req[dinner_start_time]', '$req[dinner_end_time]', '$req[time_interval]'
			)";
		mysql_query($sql, $conn);

		$sql = "SELECT LAST_INSERT_ID() AS lastNo";
		$result = mysql_query($sql, $conn);
		$row = mysql_fetch_array($result);
		$lastNo = $row['lastNo'];
		mysql_close($conn);
		return $lastNo;
	}

	// 등록시 날짜 체크
	function checkInsert($req) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$sql = " SELECT COUNT(*) AS cnt FROM doctortime WHERE doctor_fk=$req[doctor_fk] AND startday >= '$req[startday]'";

		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		$row=mysql_fetch_array($result);
		$cnt = $row['cnt'];

		return $cnt;
	}

	// 수정
	function update($req="") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = "
			UPDATE doctortime SET
				hospital_fk=$req[hospital_fk], doctor_fk=$req[doctor_fk], startday='$req[startday]', mon_start_time='$req[mon_start_time]', mon_end_time='$req[mon_end_time]', tue_start_time='$req[tue_start_time]', tue_end_time='$req[tue_end_time]',
				wed_start_time='$req[wed_start_time]', wed_end_time='$req[wed_end_time]', thu_start_time='$req[thu_start_time]', thu_end_time='$req[thu_end_time]', fri_start_time='$req[fri_start_time]', fri_end_time='$req[fri_end_time]',
				sat_start_time='$req[sat_start_time]', sat_end_time='$req[sat_end_time]', sun_start_time='$req[sun_start_time]', sun_end_time='$req[sun_end_time]', spe_start_time='$req[spe_start_time]', spe_end_time='$req[spe_end_time]',
				lunch_start_time='$req[lunch_start_time]', lunch_end_time='$req[lunch_end_time]', dinner_start_time='$req[dinner_start_time]', dinner_end_time='$req[dinner_end_time]', time_interval='$req[time_interval]'
			WHERE no = ".$req['no'];

		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		return $result;
	}

	// 수정시 날짜 체크
	function checkUpdate($req) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$sql = " SELECT COUNT(*) AS cnt FROM doctortime WHERE no!=$req[no] AND doctor_fk=$req[doctor_fk] AND startday >= '$req[startday]'";

		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		$row=mysql_fetch_array($result);
		$cnt = $row['cnt'];

		return $cnt;
	}

	// 삭제
	function delete($no=0) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = " DELETE FROM doctortime WHERE no = ".$no;

		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		return $result;
	}

	// 상세
	function getData($req='') {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = "
			SELECT 
				*,
				(SELECT name FROM hospital WHERE no = doctortime.hospital_fk) AS hospital_name,
	        	(SELECT name FROM doctor WHERE no = doctortime.doctor_fk) AS doctor_name    
			FROM doctortime
			WHERE no = ".$req['no'];
		
		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		$data = mysql_fetch_assoc($result);

		return $data;
	}

	// 온라인예약에서 지점별 예약
	function selectHospitalDayData($hospital_fk, $doctorno, $searchDay, $yoil, $isHoly) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = "
			SELECT *,
				(SELECT name FROM hospital WHERE no = doctortime.hospital_fk) AS hospital_name,
				(SELECT name FROM doctor WHERE no = doctortime.doctor_fk) AS doctor_name
			FROM doctortime
			WHERE 0 = 0 and startday <= '$searchDay' and hospital_fk = $hospital_fk and doctor_fk = $doctorno
			ORDER BY no DESC LIMIT 0, 1";

		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		$data = mysql_fetch_assoc($result);

		if ($data) {
			if ($yoil == 0) {
				$data['yoil_start_time'] = substr($data['sun_start_time'],0,2).substr($data['sun_start_time'],3,2);
				$data['yoil_end_time'] = substr($data['sun_end_time'],0,2).substr($data['sun_end_time'],3,2);
			} else if ($yoil == 1) {
				$data['yoil_start_time'] = substr($data['mon_start_time'],0,2).substr($data['mon_start_time'],3,2);
				$data['yoil_end_time'] = substr($data['mon_end_time'],0,2).substr($data['mon_end_time'],3,2);
			} else if ($yoil == 2) {
				$data['yoil_start_time'] = substr($data['tue_start_time'],0,2).substr($data['tue_start_time'],3,2);
				$data['yoil_end_time'] = substr($data['tue_end_time'],0,2).substr($data['tue_end_time'],3,2);
			} else if ($yoil == 3) {
				$data['yoil_start_time'] = substr($data['wed_start_time'],0,2).substr($data['wed_start_time'],3,2);
				$data['yoil_end_time'] = substr($data['wed_end_time'],0,2).substr($data['wed_end_time'],3,2);
			} else if ($yoil == 4) {
				$data['yoil_start_time'] = substr($data['thu_start_time'],0,2).substr($data['thu_start_time'],3,2);
				$data['yoil_end_time'] = substr($data['thu_end_time'],0,2).substr($data['thu_end_time'],3,2);
			} else if ($yoil == 5) {
				$data['yoil_start_time'] = substr($data['fri_start_time'],0,2).substr($data['fri_start_time'],3,2);
				$data['yoil_end_time'] = substr($data['fri_end_time'],0,2).substr($data['fri_end_time'],3,2);
			} else if ($yoil == 6) {
				$data['yoil_start_time'] = substr($data['sat_start_time'],0,2).substr($data['sat_start_time'],3,2);
				$data['yoil_end_time'] = substr($data['sat_end_time'],0,2).substr($data['sat_end_time'],3,2);
			}

			if ($isHoly) {
				$data['yoil_start_time'] = substr($data['spe_start_time'],0,2).substr($data['spe_start_time'],3,2);
				$data['yoil_end_time'] = substr($data['spe_end_time'],0,2).substr($data['spe_end_time'],3,2);
			}

			$data['lunch_start_time'] = substr($data['lunch_start_time'],0,2).substr($data['lunch_start_time'],3,2);
			$data['lunch_end_time'] = substr($data['lunch_end_time'],0,2).substr($data['lunch_end_time'],3,2);
			$data['dinner_start_time'] = substr($data['dinner_start_time'],0,2).substr($data['dinner_start_time'],3,2);
			$data['dinner_end_time'] = substr($data['dinner_end_time'],0,2).substr($data['dinner_end_time'],3,2);
		} else {
			$data = '';
		}
		return $data;
	}


}


?>