<? session_start(); 
/*


*/

include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/db/DBConnection.class.php";

class Category {

	static function getList() {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = "";
		$sql .= "
			select * from category
			where 1 = 1
			order by pcsorts asc, sorts asc
		";
		
		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		return $result;
	}

	static function insertDeptManage() {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		$req = $_REQUEST;

		$req['code'] = getKey();
		$code = $req['code'];
		$no = 1;
		try {
			$sql = "
				insert into category
					   ( code
						,orgname
						,orgname_eng
						,contents
						,rank
						,delstate
						,rel_code
						,rel_first_code
						,sorts
						,pcsorts
						)
				values ( '".$req['code']."'
						,'".$req['orgname']."'
						,'".$req['orgname_eng']."'
						,'".$req['contents']."'
						,'".$req['rank']."'
						,'".$req['delstate']."'
						,'".$req['rel_code']."'
			";
			if ( $req['rel_first_code'] == '0' ) {
				$sql .= "
						, '".$req['code']."'
						,(select ifnull(max(sorts),0)+1 from category og where rel_code = '0')
				";
			}
			if ( $req['rel_first_code'] != '0' ) {
				$sql .= "
						, '".$req['rel_first_code']."'
						,(select ifnull(max(sorts),0)+1 from category og where rel_code = '".$req['rel_code']."' and og.rel_first_code = '".$req['rel_first_code']."')
				";
			}
			$sql .= "
						,'".$req['pcsorts']."'
						)   
			";
			
			mysql_query($sql, $conn);
			$sql = "SELECT LAST_INSERT_ID() AS lastNo";
			$result = mysql_query($sql, $conn);


			$row = mysql_fetch_array($result);
			$lastNo = $row['lastNo'];
			$no = $lastNo;

			mysql_close($conn);
		}
		catch ( Exception $e ) {
			$no = 0;
			$code = "";
		}
		

		return $code;
	}
	
	static function changeDeptUpdate() {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		$req = $_REQUEST;

		if ( empty($req['firstParent']) ) {
			$req['firstParent'] = $req['code'];
		}

		$r = 0;
		$sql = "";

		if ( !empty($req['children']) ) {
			$sql .= "
					select  code
				,		orgname
				,		orgname_eng
				,		contents
				,		rank
				,		delstate
				,		rel_code
				,		rel_first_code
				,		pcsorts
				,		sorts
				from category
				where code = '".$req['code']."'
			";
			
			$result = mysql_query($sql, $conn);
			$data = mysql_fetch_assoc($result);

			if ( $data ) {
				$req['number'] = $req['pcsorts'] = $data['pcsorts'];
				$req['curentChilds'] = explode(',', $req['children']);

				$sql = "";
				$sql .= "
					update category                
					set rel_first_code = '".$req['firstParent']."'
					,	pcsorts = pcsorts + '".$req['number']."'
					where 1 = 1
					and code in (
				";
				for ($i=0; $i<count($req['curentChilds']); $i++) {
					if ($i > 0) $sql .= ",";
					$sql .= "
						'". $req['curentChilds'][$i] ."'
					";
				}
				$sql .= "
					)
				";
				mysql_query($sql, $conn);
			}
		}

		$sql = "";
		$sql .= "
			update category                
            set rel_code = '".$req['currentParent']."'
            ,	rel_first_code = '".$req['firstParent']."'
            ,	pcsorts = '".$req['pcsorts']."'       
            where code = '".$req['code']."'
		";
		mysql_query($sql, $conn);

		$codes = explode(',', $req['frsParents']);
		for ( $i=0; $i<count($codes); $i++ ) {
			$req['sorts'] = ($i+1);
			$req['code'] = $codes[$i];
			$sql = "
				update category                
				set sorts = '".$req['sorts']."'       
				where code = '".$req['code']."'
			";
			mysql_query($sql, $conn);
		}

		mysql_close($conn);

		return $r;
	}
	
	static function readDeptManage() {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		$req = $_REQUEST;

		$result = Category::selectDeptManage($req, $conn);

		mysql_close($conn);

		return $result;
	}

	static function selectDeptManage($req, $conn) {
		$sql = "";
		$sql .= "
			select  `code`
			,		orgname
			,		orgname_eng
			,		contents
			,		rank
			,		delstate
			,		rel_code
			,		rel_first_code
			,		pcsorts
			,		sorts
            from category
            where `code` = '".$req['code']."'
		";

//		echo $sql;
		
		$result = mysql_query($sql, $conn);
		$data = mysql_fetch_assoc($result);

		return $data;
	}

	static function deleteDeptManage() {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		$req = $_REQUEST;

		$data = Category::selectDeptManage($req, $conn);
		
		$sql = "";
		$sql .= "
			update category                
            set sorts = sorts-1       
            where 1 = 1
            and pcsorts = '".$req['pcsorts']."' and rel_first_code = '".$req['rel_first_code']."' and sorts > '".$req['sorts']."'
		";
		$result = mysql_query($sql, $conn);
		
		$parents = explode(',', $req['frsParents']);
		for ($i=0; $i<count($parents); $i++) {
			$req['code'] = $parents[$i];
    		$sql = "
				delete from category
	             where code = '".$req['code']."'
			";
			mysql_query($sql, $conn);
		}
		
		mysql_close($conn);
	}

	static function updtDeptManage() {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		$req = $_REQUEST;

		$no = 0;
		try {
			$sql = "
				update category                
               set 	 orgname        = '".$req['orgname']."'  
					,orgname_eng    = '".$req['orgname_eng']."'    
					,contents       = '".$req['contents']."'       
             where code = '".$req['code']."'
			";
			$no = mysql_query($sql, $conn);
		}
		catch(Exception $e){
			$no = 0;	
		}
		
		return $no;
	}

	static function categoryList() {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		$req = $_REQUEST;

		$sql = "
			select * from category 
			where pcsorts = '".chkIsset($req['pcsorts'])."'
			and rel_code = '".chkIsset($req['rel_code'])."'
			order by sorts asc , pcsorts asc
		";

//		echo $sql;

		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		return $result;
	}
	
	static function getCategoryGroupData($value = '', $unit = ',') {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$result = "";

		if( $value ){
			$values = explode($unit, $value);

			$sql = "";
			$sql .= "
				select GROUP_CONCAT(orgname order by pcsorts asc SEPARATOR ',') as orgname
				from category 
				where code in (
			";

			for ($i=0; $i<count($values); $i++) {
				if ($i > 0) $sql .= ",";
				$sql .= "
					'". $values[$i] ."'
				";
			}

			$sql .= "
				)
			";

			$result = mysql_query($sql, $conn);
			$result = mysql_fetch_assoc($result);
			
			mysql_close($conn);
		}

		return $result['orgname'];
	}

}


?>