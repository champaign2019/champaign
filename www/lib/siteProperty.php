<?
include_once $_SERVER['DOCUMENT_ROOT']."/lib/config/SiteProperty.class.php";
	
	$siteVO = SiteProperty::getInstance();
/*
사이트 기본정보 상수값 정의

by withsky 2014.11.17
*/
define("VERSION", $siteVO['version']);							// PHP 버전

define("COMPANY_TYPE", $siteVO['company_type']);							// 1:병원, 2:기업

define("MANAGER_OPEN", $siteVO['manager_open']); 

define("COMPANY_NAME", $siteVO['company_name']);							// 업체명
define("SSL_USE", $siteVO['ssl_use']);										// SSL 사용여부
define("COMPANY_URL", $siteVO['company_url']);		// URL
define("COMPANY_URL_MO", $siteVO['company_url_mo']);	// MOBILE URL
define("SSL_PORT", $siteVO['ssl_port']);	// SSL URL
define("DB_ENCRYPTION", $siteVO['db_encryption']);							// db 암호화방식
define("COMPANY_EMAIL", $siteVO['company_email']);				// 대표 이메일
define("COMPANY_TEL", $siteVO['company_tel']);							// 대표 전화번호
define("COMPANY_FAX", $siteVO['company_fax']);							// 대표 팩스번호
define("COMPANY_ADDR", $siteVO['company_addr']);	// 대표 주소
define("CHARSET", "utf-8");										// 캐릭터셋
define("CREATE_YEAR", $siteVO['create_year']);									// copyright
define("CREATE_MONTH", $siteVO['create_month']);									// copyright

define("CLINIC_DOCTOR_REL", $siteVO['clinic_doctor_rel']);									// 병원관리 사용유무

// SMS관련
define("SMS_TRANSMITTER", $siteVO['sms_transmitter']);						// SMS 발신번호
define("SMS_CALLBACK", "");						// SMS 수신번호
define("SMS_KEYNO", $siteVO['sms_keyno']);										// SMS 키값 (세팅필요)
define("SMS_USERID", $siteVO['sms_userid']);								// SMS 유저아이디 (세팅필요)
define("SMS_USERNAME", $siteVO['sms_username']);							// SMS 사용자명
define("SMS_CNAME",	$siteVO['sms_cname']);						// SMS 업체명
define("SMS_CONSULT_SEND",	true);								// 상담등록시 SMS발송여부

// email 관련 
define("SMTP_HOST", $siteVO['smtp_host']);						// 메일서버
define("SMTP_USER", $siteVO['smtp_user']);								// 메일계정 아이디
//define("SMTP_PASSWORD", $siteVO['smtp_password']);								// 메일계정 패스워드
define("SMTP_PASSWORD", SiteProperty::decrypt($siteVO['smtp_password'], 'vizen0502'));
define("EMAIL_FORM", $siteVO['email_form']);					// 이메일 기본 폼

// 웹로그 관련
define("WEBLOG_NUMBER", $siteVO['weblog_number']);									// 웹로그 사용자 키값
define("WEBLOG_NUMBER_MO", $siteVO['weblog_number_mo']);									// 웹로그 사용자 키값(모바일)
define("WEBLOG_NUMBER_NEW", $siteVO['weblog_number_new']);									// 웹로그 사용자 키값(모바일)
define("IS_LOG", $siteVO['is_log']);											// 웹로그 사용여부
define("LOG_VER", $siteVO['log_ver']);											// 웹로그 버전 (1:기존, 2:리뉴얼)
define("LOG_TYPE", $siteVO['log_type']);											// 웹로그 버전 (0:무료, 1:유료)

define("LOGIN_AFTER_PAGE", "/member/edit.php");					// 로그인 후 페이지
define("START_PAGE", $siteVO['start_page']);	// 관리자 로그인 후 첫페이지
define("CUSTOMER_FK", $siteVO['customer_fk']);										// 유지보수관리 거래처PK
define("PROJECT_FK", $siteVO['project_fk']);										// 유지보수관리 프로젝트PK
define("EDITOR_UPLOAD_PATH", $siteVO['editor_upload_path']);				// editor 이미지 업로드 경로
define("EDITOR_MAXSIZE", $siteVO['editor_maxsize']);							// editor 이미지 업로드 최대사이트 (50MB)
define("ZIPCODE_URL", $siteVO['zipcode_url']);	// 우편번호 조회 URL

define("CHECK_REFERER", $siteVO['check_referer']);									// 레퍼러값 체크여부
define("REFERER_URL", $siteVO['referer_url']);								// 레퍼러 비교 도메인(www 제외)



// 로그관련
$autoDenyLog = strpos($_SERVER['HTTP_HOST'], 'vizensoft') > -1 ? true : false; // 개발사이트가 아닐 경우 로그파일 자동 false
define("IS_LOGFILE", $autoDenyLog);								// 로그 사용여부 (www/log 생성필요, 파일은 날짜별로 자동생성)
define("LOG_PATH", $_SERVER['DOCUMENT_ROOT']."/log");			// 로그파일 경로
define("LOG_FILENAME", "_log.txt");								// 로그파일명 (날짜_log.txt)
define("LOG_PAGE_CHAR", "UTF-8");								// 로그작성시 페이지의 캐릭터셋
define("LOG_SERVER_CHAR", "EUC-KR");							// 로그작성시 서버의 캐릭터셋

// 병원기본설정
define("DEFAULT_BRANCH_NO", $siteVO['default_branch_no']);									// 기본 지점
define("DEFAULT_DOCTOR_NO", $siteVO['default_doctor_no']);									// 기본 의료진 정보
define("DEFAULT_CLINIC_NO", 1);									// 기본 진료과목 정보
define("RESER_JUNGBOK", $siteVO['reser_jungbok']);									// 사용자 중복예약 가능여부 (true:가능, false:불가)
define("RESER_BUNDLE", $siteVO['reser_bundle']);									// 사용자 중복예약 가능여부 (true:가능, false:불가)

//SNS 로그인 관련
define("LOGIN_NAVER",$siteVO['login_naver']);					//로그인_네이버 Client ID : 1EL7Z6P09J8QkkImIfZz
define("LOGIN_NAVER_USE",$siteVO['login_naver_use']);			
define("LOGIN_GOOGLE",$siteVO['login_google']);		// 로그인_구글 Client ID : 903997996012-ju9fi8bp0nca0taqqbss2mbvgi6os0kn.apps.googleusercontent.com
define("LOGIN_GOOGLE_USE",$siteVO['login_google_use']);			
define("LOGIN_KAKAO",$siteVO['login_kakao']);		// 로그인_카카오 Javascript 키 :96c141d38f3b78cb79360c0829af2a87
define("LOGIN_KAKAO_USE",$siteVO['login_kakao_use']);			
define("LOGIN_FACEBOOK",$siteVO['login_facebook']);						// 로그인_페이스북 Client ID :134396777254894
define("LOGIN_FACEBOOK_USE",$siteVO['login_facebook_use']);			

/**
 * 디폴트 PDT(POPUP_DEVICE_TYPE) 사용 유무
 * true : POPUP_DEVICE_TYPE 중, 무조건 한 개만 사용
 * false : POPUP_DEVICE_TYPE 전부 사용
 */
define("DEFAULT_PDT_USE", $siteVO['default_pdt_use']);

/**
 * 디폴트 PDT(POPUP_DEVICE_TYPE) index 값
 */
define("DEFAULT_PDT_INDEX", $siteVO['default_pdt_index']);

/**
 * 사이트 다국어 지원 설정
 */
define("DEFAULT_LANG", $siteVO['default_lang']);

/**
 * 변경하면 안되는 값
 * properties 파일명
 */
define("LANG_KOR", $siteVO['lang_kor']); //kor-국문 
define("LANG_ENG", $siteVO['lang_eng']); //eng-영문
define("LANG_CHI", $siteVO['lang_chi']); //chi-중문

/**
 * [0] : PC Device에만 노출 될 팝업
 * [1] : Mobile Device에만 노출 될 팝업
 * [2] : 하나의 데이터(팝업이미지, 각 설정값 등...)로 PC, Mobile 한번에 관리 
 * [3] : 각각의 데이터(팝업이미지, 각 설정값 등...)로 PC, Mobile 따로 관리
 */
define("POPUP_DEVICE_TYPE", var_export(
		 array(
			"PC 전용"
			, "Mobile 전용"
			, "PC&Mobile 공통 관리"
			, "PC&Mobile 각각 관리"
		)
		, true
	)
);

/**
 * 휴면계정전환 기준날짜
 */
define("DORMANT_DATE",$siteVO['dormant_date']);
/**
 * 휴면계정전환 전 알림날짜
 */
define("DORMANT_NOTICE_DATE",$siteVO['dormant_notice_date']);
/**
 * 휴면계정 관리
 * 0-일반(default), 1-휴면관리계정
 */
define("DORMANT_TYPE",$siteVO['dormant_type']);

//관리자 페이지 세션유지 계정 ID
define("KEEP_USER",$siteVO['keep_user']);

define("ALLOWED_IP", $siteVO['allowed_ip']);
?>