<?
/*
사이트 기본정보 상수값 정의

by withsky 2014.11.17
*/
define("VERSION", "Ver 1.0.1");							// PHP 버전

define("COMPANY_NAME", "병원프로그램PHP");							// 업체명
define("SSL_USE", false);										// SSL 사용여부
define("COMPANY_URL", "http://hospital-php.vizensoft.com:92");		// URL
define("COMPANY_URL_MO", "http://hospital-php.vizensoft.com:92");	// MOBILE URL
define("COMPANY_SSL_URL", "https://hospital-php.vizensoft.com:92");	// SSL URL
define("DB_ENCRYPTION", "password");							// db 암호화방식
define("COMPANY_EMAIL", "zero3618@naver.com");				// 대표 이메일
define("COMPANY_TEL", "");							// 대표 전화번호
define("COMPANY_FAX", "");							// 대표 팩스번호
define("COMPANY_ADDR", "");	// 대표 주소
define("CHARSET", "utf-8");										// 캐릭터셋
define("CREATE_YEAR", 2015);									// copyright

define("CLINIC_DOCTOR_REL", false);									// 병원관리 사용유무

// SMS관련
define("SMS_TRANSMITTER", "");						// SMS 발신번호
define("SMS_CALLBACK", "");						// SMS 수신번호
define("SMS_KEYNO", "1");										// SMS 키값 (세팅필요)
define("SMS_USERID", "program");								// SMS 유저아이디 (세팅필요)
define("SMS_USERNAME", "비젠소프트");							// SMS 사용자명
define("SMS_CNAME",	"비젠병원프로그램PHP");						// SMS 업체명
define("SMS_CONSULT_SEND",	true);								// 상담등록시 SMS발송여부

// email 관련
define("SMTP_HOST", "mail.vizen.co.kr");						// 메일서버
define("SMTP_USER", "vizenmail");								// 메일계정 아이디
define("SMTP_PASSWORD", "vizen");								// 메일계정 패스워드
define("EMAIL_FORM", "/include/emailForm.php");					// 이메일 기본 폼

// 웹로그 관련
define("WEBLOG_NUMBER", 1);									// 웹로그 사용자 키값
define("WEBLOG_NUMBER_MO", 2);									// 웹로그 사용자 키값(모바일)
define("IS_LOG", true);											// 웹로그 사용여부
define("LOG_VER", 1);											// 웹로그 버전 (1:기존, 2:리뉴얼)
define("LOG_TYPE", 0);											// 웹로그 버전 (0:무료, 1:유료)

define("LOGIN_AFTER_PAGE", "/member/edit.php");					// 로그인 후 페이지
define("START_PAGE", "/manage/environment/admin/index.php");	// 관리자 로그인 후 첫페이지
define("CUSTOMER_FK", 488);										// 유지보수관리 거래처PK
define("PROJECT_FK", 848);										// 유지보수관리 프로젝트PK
define("EDITOR_UPLOAD_PATH", "/upload/editor/");				// editor 이미지 업로드 경로
define("EDITOR_MAXSIZE", 50*1024*1024);							// editor 이미지 업로드 최대사이트 (50MB)
define("ZIPCODE_URL", "http://zipcode.vizensoft.com/api2.php");	// 우편번호 조회 URL

define("CHECK_REFERER", true);									// 레퍼러값 체크여부
define("REFERER_URL", "hospital-php");								// 레퍼러 비교 도메인(www 제외)



// 로그관련
$autoDenyLog = strpos($_SERVER['HTTP_HOST'], 'vizensoft') > -1 ? true : false; // 개발사이트가 아닐 경우 로그파일 자동 false
define("IS_LOGFILE", $autoDenyLog);								// 로그 사용여부 (www/log 생성필요, 파일은 날짜별로 자동생성)
define("LOG_PATH", $_SERVER['DOCUMENT_ROOT']."/log");			// 로그파일 경로
define("LOG_FILENAME", "_log.txt");								// 로그파일명 (날짜_log.txt)
define("LOG_PAGE_CHAR", "UTF-8");								// 로그작성시 페이지의 캐릭터셋
define("LOG_SERVER_CHAR", "EUC-KR");							// 로그작성시 서버의 캐릭터셋

// 병원기본설정
define("DEFAULT_BRANCH_NO", 1);									// 기본 지점
define("DEFAULT_DOCTOR_NO", 1);									// 기본 의료진 정보
define("DEFAULT_CLINIC_NO", 1);									// 기본 진료과목 정보
define("RESER_JUNGBOK", false);									// 사용자 중복예약 가능여부 (true:가능, false:불가)


//SNS 로그인 관련
define("LOGIN_NAVER","");					//로그인_네이버 Client ID : 1EL7Z6P09J8QkkImIfZz
define("LOGIN_GOOGLE","");		// 로그인_구글 Client ID : 903997996012-ju9fi8bp0nca0taqqbss2mbvgi6os0kn.apps.googleusercontent.com
define("LOGIN_KAKAO","");		// 로그인_카카오 Javascript 키 :96c141d38f3b78cb79360c0829af2a87
define("LOGIN_FACEBOOK","");						// 로그인_페이스북 Client ID :134396777254894

/**
 * 디폴트 PDT(POPUP_DEVICE_TYPE) 사용 유무
 * true : POPUP_DEVICE_TYPE 중, 무조건 한 개만 사용
 * false : POPUP_DEVICE_TYPE 전부 사용
 */
define("DEFAULT_PDT_USE", false);

/**
 * 디폴트 PDT(POPUP_DEVICE_TYPE) index 값
 */
define("DEFAULT_PDT_INDEX", 0);

/**
 * 사이트 다국어 지원 설정
 */
define("DEFAULT_LANG", "kor");

/**
 * 변경하면 안되는 값
 * properties 파일명
 */
define("LANG_KOR","kor"); //kor-국문 
define("LANG_ENG","eng"); //eng-영문
define("LANG_CHI","chi"); //chi-중문

/**
 * [0] : PC Device에만 노출 될 팝업
 * [1] : Mobile Device에만 노출 될 팝업
 * [2] : 하나의 데이터(팝업이미지, 각 설정값 등...)로 PC, Mobile 한번에 관리 
 * [3] : 각각의 데이터(팝업이미지, 각 설정값 등...)로 PC, Mobile 따로 관리
 */
define("POPUP_DEVICE_TYPE", var_export(
		 array(
			"PC 전용"
			, "Mobile 전용"
			, "PC&Mobile 공통 관리"
			, "PC&Mobile 각각 관리"
		)
		, true
	)
);

/**
 * 휴면계정전환 기준날짜
 */
define("DORMANT_DATE",365);
/**
 * 휴면계정전환 전 알림날짜
 */
define("DORMANT_NOTICE_DATE",335);
/**
 * 휴면계정 관리
 * 0-일반(default), 1-휴면관리계정
 */
define("DORMANT_TYPE",0);

//관리자 페이지 세션유지 계정 ID
define("KEEP_USER","vizensoft");

?>