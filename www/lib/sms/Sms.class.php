<?
/*


*/

include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/db/DBConnection.class.php";

class Sms {

	// 검색 파라미터 (초기 개발시 검색조건 세팅필요)
	var $param = array (
					"stype",
					"sval",
					"sday",
					"eday",
					"stran_id",
					"stran_status",
					"isreser"
				);

	var $pageRows;			// 페이지 로우수
	var $startPageNo=0;		// limit 시작페이지
	public $reqPageNo=1;	// 요청페이지
	var $conn;

	// 생성자
	function Sms($pageRows=0, $request='') {
		$this->pageRows = $pageRows;
		$this->reqPageNo = ($request['reqPageNo'] == 0) ? 1 : $request['reqPageNo'];	// 요청페이지값 없을시 1로 세팅
		if ($request['reqPageNo'] > 0) {
			$this->startPageNo = ($request['reqPageNo']-1) * $this->pageRows;
		}
	}

	// 검색 파라미터 queryString 생성
	function getQueryString($page="", $no=0, $request='') {	
		$str = '';
		
		for ($i=0; $i<count($this->param); $i++) {
			if ($request[$this->param[$i]]) {
				$str = $str.$this->param[$i]."=".urlencode($request[$this->param[$i]])."&";
			}
		}

		if ($no > 0) $str = $str."no=".$no;			// no값이 있을 경우에만 파라미터 세팅 (페이지 이동시 no필요 없음)

		$return = '';
		if ($str) {
			$return = $page.'?'.$str;
		} else {
			$return = $page.'?';
		}

		return $return;
	}

	// sql WHERE절 생성
	function getWhereSql($p) {
		$whereSql = " WHERE 1=1 ";
		if ($p['stran_id']) {
			$whereSql .= " AND tran_id = '$p[stran_id]' ";
		}
		if ($p['stran_status']) {
			$whereSql .= " AND tran_status = '$p[stran_status]' ";
		}
		if ($p['isreser'] == 0) {
			$whereSql .= " AND tran_date < NOW() ";
		} else {
			$whereSql .= " AND tran_date > NOW() ";
		}

		if ($p['sday']) {
			if ($p['eday']) {
				$whereSql .= " AND tran_date BETWEEN '".$p['sday']." 00:00:00' AND '".$p['eday']." 23:59:59' ";
			}
		}

		if ($p['sval']) {
			if ($p['stype'] == 'all') {
				$whereSql .= " AND ( (tran_msg LIKE '%".$p['sval']."%' ) OR (tran_callback LIKE '%".$p['sval']."%' ) OR (tran_phone LIKE '%".$p['sval']."%' ) ) ";
			} else {
				$whereSql .= " AND ".$p['stype']." LIKE '%".$p['sval']."%' ";
			}
		}

		return $whereSql;
	}


	// 전체로우수, 페이지카운트
	function getCount($param = "") {
		$dbconn = new DBConnection('sms');
		$conn = $dbconn->getConnection();
		
		$whereSql = $this->getWhereSql($param);	// where절
		$sql = " SELECT COUNT(*) AS cnt FROM sms ".$whereSql;

		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		$row=mysql_fetch_array($result);
		$totalCount = $row['cnt'];
		$pageCount = getPageCount($this->pageRows, $totalCount);

		$data[0] = $totalCount;
		$data[1] = $pageCount;

		return $data;
	}

	// 목록
	function getList($param='') {
		$dbconn = new DBConnection('sms');
		$conn = $dbconn->getConnection();
		
		$whereSql = $this->getWhereSql($param);	// where절

		$sql = "
			SELECT 
				no, tran_pr, tran_phone, tran_callback, tran_date, 
				IFNULL((SELECT statusName FROM em_status_category b WHERE b.statusFK = sms.tran_status), sms.tran_status) AS tran_status,
				IFNULL(tran_net, '') AS tran_net, tran_msg, tran_etc1, tran_etc2, tran_etc3, tran_id, tran_rslt, tran_type, 
				(Case tran_type WHEN 0 THEN 'SMS' WHEN 5 THEN 'MMS' WHEN 6 THEN 'LMS' END) AS tran_typeName,  tran_rsltdate
			FROM sms 
			".$whereSql."
			ORDER BY no DESC LIMIT ".$this->startPageNo.", ".$this->pageRows." ";
		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		return $result;
	}

	// 키값 및 최대 발송량 체크
	function chkMax($tran_id='') {
		$tran_ymd = date('Y-m-d H:i:s');
		$rst = false;

		$keyChk = $this->keyCheck($tran_id);
		$todayChk = 0;
		$maxChk = 0;
		if ($keyChk > 0) {
			$todayChk = $this->todayCount($tran_id, $tran_ymd);
			$maxChk = $this->maxCount($tran_id);
			if ($maxChk >= $todayChk) {
				$rst = true;
			} else {
				$rst = false;
			}
		} else {
			$rst = false;
		}
		return $rst;
	}

	// SMS 발공 공통
	function smsSendPpurio($req='') {
		$smsList = split("\r\n", $req['receiver']);
		if (sizeof($smsList) > 0) {
			for ($i=0; $i<sizeof($smsList); $i++) {
				$req['tran_pr'] = "P".(microtime(true)*100).$i."_".$req['tran_id'];
				$req['tran_phone'] = $smsList[$i];
				$no = $this->insert($req);
			}
		}
		return $no;
	}

	function send($req='') {
		$rst = false;
		if (getStrBytes($req['tran_msg']) <= 90) {
			$rst = $this->sendSMS($_REQUEST);
		}
		else if (getStrBytes($req['tran_msg']) > 90) {
			$_REQUEST['mms_subject'] = COMAPNY_NAME;
			$_REQUEST['mms_body'] = $_REQUEST['tran_msg'];
			$rst = $this->sendLMS($_REQUEST);
		}
		return $rst;
	}


	// SMS발송
	function sendSMS($req='') {
		$rst = $this->chkMax($req['tran_id']);
		if ($rst) {
			$req['tran_type'] = 0;
			$req['tran_etc4'] = '';
			$rst = $this->chkAmounts($req);
			if($rst) {
				$rst = $this->smsSendPpurio($req);
			}
		}
		return $rst;
	}

	// LMS발송
	function sendLMS($req='') {
		$rst = $this->chkMax($req['tran_id']);
		if ($rst) {
			$req['tran_type'] = 5;
			$maxMms_seq = $this->insertLMS($req);
			$req['tran_etc4'] = $maxMms_seq;

			$rst = $this->chkAmounts($req);
			if($rst) {
				$rst = $this->smsSendPpurio($req);
			}
		}
		return $rst;
	}

	// MMS발송
	function sendMMS($req='') {
		$rst = $this->chkMax($req['tran_id']);
		if ($rst) {
			$req['tran_type'] = 6;
			$req['file_cnt'] = 2;
			$maxMms_seq = $this->insertMMS($req);
			$req['tran_etc4'] = $maxMms_seq;
	
			$rst = $this->chkAmounts($req);
			if($rst) {
				$rst = $this->smsSendPpurio($req);
			}
		}
		return $rst;
	}

	// insert
	function insert($req="") {
		$dbconn = new DBConnection('sms');
		$conn = $dbconn->getConnection();

		$sql = "
			INSERT INTO sms 
				(
				tran_phone, tran_callback, tran_status, tran_date, tran_msg,
        		tran_etc1, tran_etc2, tran_etc3, tran_etc4, tran_id, tran_type, tran_pr, tran_ymd, selcheck
				)
			VALUES
				(
				'$req[tran_phone]', '$req[tran_callback]', '$req[tran_status]', '$req[tran_date]', '$req[tran_msg]',
        		'$req[tran_etc1]', '$req[tran_etc2]', '$req[tran_etc3]', '$req[tran_etc4]', '$req[tran_id]', $req[tran_type], '$req[tran_pr]', '$req[tran_ymd]', 2
				)";
		mysql_query($sql, $conn);

		$sql = "SELECT LAST_INSERT_ID() AS lastNo";
		$result = mysql_query($sql, $conn);
		$row = mysql_fetch_array($result);
		$lastNo = $row['lastNo'];
		mysql_close($conn);
		return $lastNo;
	}

	// SMS KEY 체크
	function keyCheck($tran_id='') {
		$dbconn = new DBConnection('sms');
		$conn = $dbconn->getConnection();

		$sql = "
			SELECT COUNT(sms_key) AS sms_key FROM em_site WHERE sms_key = '$tran_id' ";

		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		$row = mysql_fetch_assoc($result);
		$cnt = $row['sms_key'];

		return $cnt;
	}

	//선불충전관련 추가 2019-07-04
	function chkAmounts($req="") {
		$dbconn = new DBConnection('sms');
		$conn = $dbconn->getConnection();

		$sql = "SELECT ifnull( total_amounts - (
					CASE WHEN $req[tran_type]= 0 THEN 27.5 
						 WHEN $req[tran_type] = 5 THEN 66 
						 WHEN $req[tran_type] = 6 THEN 275
						END
					), 0) as total
		    	FROM em_site WHERE sms_key = '$req[tran_id]' ";

		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		$row = mysql_fetch_assoc($result);
		$total = $row['total'];
		if($total <= 0) {
			return false;
		}
		return true;
	}

	// 삭제
	function delete($tran_pr='') {
		$dbconn = new DBConnection('sms');
		$conn = $dbconn->getConnection();

		$sql = " DELETE FROM sms WHERE tran_pr = '".$tran_pr."' AND tran_status=1 ";

		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		return $result;
	}
	// 삭제
	function deleteNo($sms_no='') {
		$dbconn = new DBConnection('sms');
		$conn = $dbconn->getConnection();

		$sql = " DELETE FROM sms WHERE no = '".$sms_no."' AND tran_status=1 ";

		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		return $result;
	}

	// 오늘 발송 수
	function todayCount($tran_id='', $tran_ymd='') {
		$dbconn = new DBConnection('sms');
		$conn = $dbconn->getConnection();

		$sql = "
			SELECT COUNT(*) AS cnt FROM sms WHERE tran_id = '$tran_id' AND tran_ymd='$tran_ymd' ";

		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		$row = mysql_fetch_assoc($result);
		$cnt = $row['cnt'];

		return $cnt;
	}

	// 일일 최대 발송 수
	function maxCount($tran_id='') {
		$dbconn = new DBConnection('sms');
		$conn = $dbconn->getConnection();

		$sql = "
			SELECT sms_max FROM em_site WHERE sms_key = '$tran_id' ";

		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		$row = mysql_fetch_assoc($result);
		$sms_max = $row['sms_max'];

		return $sms_max;
	}

	// LMS인경우 em_tran테이블에 insert
	function insertLMS($req) {
		$dbconn = new DBConnection('sms');
		$conn = $dbconn->getConnection();

		$sql = "
			INSERT INTO em_tran_mms 
				(
				mms_subject, mms_body, file_cnt
				)
			VALUES
				(
				'$req[mms_subject]', '$req[mms_body]', $req[file_cnt]
				)";
		mysql_query($sql, $conn);

		$sql = "SELECT LAST_INSERT_ID() AS lastNo";
		$result = mysql_query($sql, $conn);
		$row = mysql_fetch_array($result);
		$lastNo = $row['lastNo'];
		mysql_close($conn);
		return $lastNo;
	}

	// MMS인경우 em_tran테이블에 insert
	function insertMMS($req) {
		$dbconn = new DBConnection('sms');
		$conn = $dbconn->getConnection();

		$sql = "
			INSERT INTO em_tran_mms 
				(
				mms_subject, mms_body, file_cnt, file_type1, file_name1, service_dep1
				)
			VALUES
				(
				'$req[mms_subject]', '$req[mms_body]', $req[file_cnt], 'IMG', '$req[file_name1]', 'ALL'
				)";
		mysql_query($sql, $conn);

		$sql = "SELECT LAST_INSERT_ID() AS lastNo";
		$result = mysql_query($sql, $conn);
		$row = mysql_fetch_array($result);
		$lastNo = $row['lastNo'];
		mysql_close($conn);
		return $lastNo;
	}

	// MMS테이블 마지막 시쿼스값
	function maxMms_seq($mms_seq='') {
		$dbconn = new DBConnection('sms');
		$conn = $dbconn->getConnection();

		$sql = "
			SELECT MAX(mms_seq) AS maxMms_seq FROM em_tran_mms WHERE mms_seq = $mms_seq ";

		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		$row = mysql_fetch_assoc($result);
		$maxMms_seq = $row['maxMms_seq'];

		return $maxMms_seq;
	}

	

}


?>