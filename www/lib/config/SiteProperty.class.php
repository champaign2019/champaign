<?
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/db/DBConnection.class.php";

class SiteProperty {
	protected static $instance;

	// 검색 파라미터 (초기 개발시 검색조건 세팅필요)
	var $param = array ();
	var $conn;

	// 생성자
	function SiteProperty() {}

	public static function getInstance() {
		if(is_null(self::$instance)){
//			self::$instance = new self();
			self::$instance = self::SitePropertyVO();
		}
		return self::$instance;
	}

	function SitePropertyVO() {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

//	static $siteVO;
//		if(!isset($siteVO)) {


		$sql = "SELECT * FROM site_info";
//echo $sql; 
		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		$data = mysql_fetch_assoc($result);
			$siteVO = $data;
//		}

		return $siteVO;
		
	}

	function SitePropertyParam($request='') {
		$this->tableName = "site_config";
	}

	function chkBoolean($request='') {
		$result = false;
		if($request==1) {
			$result = true;
		}
		return $result;
	}

	// 수정
	function update($req="") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = "
			UPDATE `site_info` SET
			`version`='".$req['version']."',
			`company_type`=".$req['company_type'].",
			`ssl_use`=".$req['ssl_use'].",
			`default_branch_no`=".$req['default_branch_no'].",
			`default_doctor_no`=".$req['default_doctor_no'].",
			
			`reser_jungbok`=".$req['reser_jungbok'].",

			`start_page`='".$req['start_page']."',
			`check_referer`=".$req['check_referer'].",
			`referer_url`='".$req['referer_url']."',
			`customer_fk`=".$req['customer_fk'].",
			`project_fk`=".$req['project_fk'].",
			`editor_upload_path`='".$req['editor_upload_path']."',
			`editor_maxsize`=".$req['editor_maxsize'].",

			`company_name`='".$req['company_name']."',
			`company_url`='".$req['company_url']."',
			`ssl_port`='".$req['ssl_port']."',
			`company_email`='".$req['company_email']."',
			`company_tel`='".$req['company_tel']."',
			`company_fax`='".$req['company_fax']."',
			`company_addr`='".$req['company_addr']."',

			`create_year`=".$req['create_year'].",
			`create_month`=".$req['create_month'].",

			`login_naver`='".$req['login_naver']."',
			`login_naver_use`=".chkIsset($req['login_naver_use']).",
			`login_google`='".$req['login_google']."',
			`login_google_use`=".chkIsset($req['login_google_use']).",
			`login_kakao`='".$req['login_kakao']."',
			`login_kakao_use`=".chkIsset($req['login_kakao_use']).",
			`login_facebook`='".$req['login_facebook']."',
			`login_facebook_use`=".chkIsset($req['login_facebook_use']).",
			
			`default_lang`='".$req['default_lang']."',

			`dormant_date`=".$req['dormant_date'].",
			`dormant_notice_date`=".$req['dormant_notice_date'].",
			`dormant_type`=".$req['dormant_type'].",
			`keep_user`='".$req['keep_user']."',

			`sms_transmitter`='".$req['sms_transmitter']."',
			`sms_keyno`='".$req['sms_keyno']."',

			`sms_username`='".$req['sms_username']."',
			`sms_cname`='".$req['sms_cname']."',
			
			`smtp_host`='".$req['smtp_host']."',
			`smtp_user`='".$req['smtp_user']."',
			`smtp_password`='".$this->encrypt($req['smtp_password'], 'vizen0502')."',
			`email_form`='".$req['email_form']."',
			
			`weblog_number`='".$req['weblog_number']."',
			`weblog_number_mo`='".$req['weblog_number_mo']."',
			`weblog_number_new`='".$req['weblog_number_new']."',
			`is_log`=".$req['is_log'].",
			`log_ver`=".$req['log_ver'].",
			`log_type`=".$req['log_type']."";
//echo $sql;

		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		return $result;
	}

	function encrypt($string, $key) {
		$result = '';
		for($i=0; $i<strlen($string); $i++) {
			$char = substr($string, $i, 1);
			$keychar = substr($key, ($i % strlen($key))-1, 1);
			$char = chr(ord($char)+ord($keychar));
			$result .= $char;
		}
		return base64_encode($result);
	}
	 
	function decrypt($string, $key) {
		$result = '';
		$string = base64_decode($string);
		for($i=0; $i<strlen($string); $i++) {
			$char = substr($string, $i, 1);
			$keychar = substr($key, ($i % strlen($key))-1, 1);
			$char = chr(ord($char)-ord($keychar));
			$result .= $char;
		}
		return $result;
	}

}


?>