<?
/*
온라인예약

예약은 지점 여부에 따라 목록 검색조건 수정 필요.

*/

include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/db/DBConnection.class.php";

class Reservation {

	// 검색 파라미터 (초기 개발시 검색조건 세팅필요)
	var $param = array (
					"stype",
					"sval",
					"shospital_fk",
					"sdoctor_fk",
					"sclinic_fk",
					"smember_fk",
					"ssite",
					"sresertype",
					"snewold",
					"sroute",
					"sstate",
					"datetype",
					"sreserdate",
					"sresertime",
					"startday",
					"endday"
				);

	var $tableName;			// 테이블명
	var $pageRows;			// 페이지 로우수
	var $startPageNo=0;		// limit 시작페이지
	public $reqPageNo=1;	// 요청페이지
	var $conn;

	// 생성자
	function Reservation($pageRows=0, $tableName='', $request='') {
		$this->pageRows = $pageRows;
		$this->tableName = $tableName;
		$this->reqPageNo = ($request['reqPageNo'] == 0) ? 1 : $request['reqPageNo'];	// 요청페이지값 없을시 1로 세팅
		if ($request['reqPageNo'] > 0) {
			$this->startPageNo = ($request['reqPageNo']-1) * $this->pageRows;
		}
	}

	// 검색 파라미터 queryString 생성
	function getQueryString($page="", $no=0, $request='') {	
		$str = '';
		
		for ($i=0; $i<count($this->param); $i++) {
			if (isset($request[$this->param[$i]])) {
				$str .= $this->param[$i]."=".urlencode($request[$this->param[$i]])."&";
			}
		}

		if ($no > 0) $str .= "no=".$no;			// no값이 있을 경우에만 파라미터 세팅 (페이지 이동시 no필요 없음)

		$return = '';
		if ($str) {
			$return = $page.'?'.$str;
		} else {
			$return = $page;
		}

		return $return;
	}

	// sql WHERE절 생성
	function getWhereSql($p) {
		$whereSql = " WHERE 1=1";
		if ($p['shospital_fk'] > 0) {
			$whereSql .= " AND hospital_fk = ".$p['shospital_fk'];
		}
		if ($p['sclinic_fk'] > 0) {
			$whereSql .= " AND clinic_fk = ".$p['sclinic_fk'];
		}
		if ($p['sdoctor_fk'] > 0) {
			$whereSql .= " AND doctor_fk = ".$p['sdoctor_fk'];
		}
		if ($p['smember_fk'] > 0) {
			$whereSql .= " AND member_Fk = ".$p['smember_fk'];
		}
		if ($p['name']) {
			$whereSql .= " AND name='".$p['name']."' AND password=".DB_ENCRYPTION."('".$p[password]."')";
		}
		if ($p['sresertype'] > 0) {
			$whereSql .= " AND resertype = ".$p['sresertype'];
		}
		if ($p['snewold'] > 0) {
			$whereSql .= " AND newold = ".$p['snewold'];
		}
		if ($p['sonoff'] > 0) {
			$whereSql .= " AND onoff = ".$p['sonoff'];
		}
		if ($p['sroute'] > 0) {
			$whereSql .= " AND route = ".$p['sroute'];
		}
		if ($p['sstate'] > 0) {
			if ($p['sstate'] == 5) {
				$whereSql .= " AND state <= 2";
			}
			if ($p['sstate'] == 6) {
				$whereSql .= " AND state <= 3";
			}
			if ($p['sstate'] != 5 && $p['sstate'] != 6) {
				$whereSql .= " AND state = ".$p['sstate'];
			}
		}
		if ($p['sval']) {
			if ($p['stype'] == 'all') {
				$whereSql .= " AND ( (name like '%".$p['sval']."%' ) or (offchgname like '%".$p['sval']."%' ) or (cell like '%".$p['sval']."%' ) or (email like '%".$p['sval']."%' ) or (etc like '%".$p['sval']."%') ) ";
			} else {
				$whereSql .= " AND (".$p['stype']." LIKE '%".$p['sval']."%' )";
			}
		}
		if ($p['startday']) {
			if ($p['endday']) {
				if ($p['datetype'] == 'all') {
					$whereSql .= " AND ((registdate BETWEEN '".$p['startday']." 00:00:00' AND '".$p['endday']." 23:59:59') OR (reserdate BETWEEN '".$p['startday']." 00:00:00' AND '".$p['endday']." 23:59:59')) ";
				} else {
					$whereSql .= " AND ".$p['datetype']." BETWEEN '".$p['startday']." 00:00:00' AND '".$p['endday']." 23:59:59' ";
				}
			}
		}
		if ($p['smain']) {
			$whereSql .= " AND main = ".$p['smain'];
		}
		if ($p['sreserdate']) {
			$whereSql .= " AND reserdate BETWEEN '".$p['sreserdate']." 00:00:00' AND '".$p['sreserdate']." 23:59:59'";
		}
		return $whereSql;
	}


	// 전체로우수, 페이지카운트
	function getCount($param = "") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$whereSql = $this->getWhereSql($param);	// where절
		$sql = " SELECT COUNT(*) AS cnt FROM ".$this->tableName.$whereSql;
		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		$row=mysql_fetch_array($result);
		$totalCount = $row['cnt'];
		$pageCount = getPageCount($this->pageRows, $totalCount);

		$data[0] = $totalCount;
		$data[1] = $pageCount;

		return $data;
	}
	
	function getCountLogin($param = "") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$whereSql = $this->getWhereSql($param);	// where절
		$sql = " SELECT COUNT(*) AS cnt FROM ".$this->tableName." WHERE member_Fk = ".$param['smember_fk'];
		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		$row=mysql_fetch_array($result);
		$totalCount = $row['cnt'];
		$pageCount = getPageCount($this->pageRows, $totalCount);

		$data[0] = $totalCount;
		$data[1] = $pageCount;

		return $data;
	}
	
	function getCountNonLogin($param = "") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$whereSql = $this->getWhereSql($param);	// where절
		$sql = " SELECT COUNT(*) AS cnt FROM ".$this->tableName." WHERE name='".$param['name']."' AND password=".DB_ENCRYPTION."('".$param[password]."')";
		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		$row=mysql_fetch_array($result);
		$totalCount = $row['cnt'];
		$pageCount = getPageCount($this->pageRows, $totalCount);

		$data[0] = $totalCount;
		$data[1] = $pageCount;

		return $data;
	}

	// 목록
	function getList($param='') {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$whereSql = $this->getWhereSql($param);	// where절

		$sql = "
			SELECT
				no, member_fk, hospital_fk, doctor_fk, clinic_fk,
				(SELECT name FROM hospital WHERE hospital.no = ".$this->tableName.".hospital_fk) AS hospital_name,
				(SELECT name FROM doctor WHERE doctor.no = ".$this->tableName.".doctor_fk) AS doctor_name,
				(SELECT name FROM clinic WHERE clinic.no = ".$this->tableName.".clinic_fk) AS clinic_name,
				state, reserdate, DATE_FORMAT(resertime, '%H:%i') AS resertime,
				name, password, cell, tel, email, etc,
				registdate, editdate, confirmdate, canceldate,
				onoff, newold, route, lastchange, telconsult,
				resertype, offname, offchgname
			FROM ".$this->tableName."
			".$whereSql."
			ORDER BY no DESC LIMIT ".$this->startPageNo.", ".$this->pageRows." ";

		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		return $result;
	}

	// 예약
	// return -1인 경우 중복예약
	function insert($req) {
		$result=0;

		if (RESER_JUNGBOK) {
			$result = $this->reserInsert($req);
		} else {
			$check = $this->checkReservationForInsert($req);

			if ($check > 0) {
				$result = -1;
			} else {
				$result = $this->reserInsert($req);
			}
		}
		return $result;
	}

	// 저장
	function reserInsert($req="") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = "
			INSERT INTO ".$this->tableName." (
				member_fk, hospital_fk, doctor_fk, clinic_fk, state, reserdate, resertime, 
				name, password, cell, tel, email, registdate, onoff, newold, route, lastchange, telconsult,
				resertype, offname, offchgname, etc
			) VALUES (
				".chkIsset($req[member_fk]).", ".chkIsset($req[hospital_fk]).", ".chkIsset($req[doctor_fk]).", ".chkIsset($req[clinic_fk]).", ".chkIsset($req[state]).", '$req[reserdate]', '$req[resertime]',
				'$req[name]', ".DB_ENCRYPTION."('$req[password]'), '$req[cell]', '$req[tel]', '$req[email]', NOW(), ".chkIsset($req[onoff]).", $req[newold], $req[route], '$req[lastchange]', ".chkIsset($req[telconsult]).",
				".chkIsset($req[resertype]).", '$req[offname]', '$req[offchgname]', '$req[etc]' 
			)";

		mysql_query($sql, $conn);

		$sql = "SELECT LAST_INSERT_ID() AS lastNo";
		$result = mysql_query($sql, $conn);
		$row = mysql_fetch_array($result);
		$lastNo = $row['lastNo'];
		mysql_close($conn);
		return $lastNo;
		
	}

	// 예약 중복 체크 insert시
	function checkReservationForInsert($param = "") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$sql = " SELECT COUNT(*) AS cnt FROM ".$this->tableName."
			WHERE doctor_fk=$param[doctor_fk]
			AND hospital_fk=$param[hospital_fk]
			AND reserdate='$param[reserdate]'
			AND resertime='$param[resertime]'
			AND state != 4";

		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		$row=mysql_fetch_array($result);
		$cnt = $row['cnt'];

		return $cnt;
	}

	// 수정
	function reserUpdate($req="") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = "
			UPDATE ".$this->tableName." SET 
			hospital_fk=".chkIsset($req['hospital_fk']).", doctor_fk=".chkIsset($req['doctor_fk']).", clinic_fk=".chkIsset($req['clinic_fk']).", state=".chkIsset($req['state']).", reserdate='".$req['reserdate']."', resertime='".$req['resertime']."',
			name='$req[name]', ";
		if ($req['password']) $sql = $sql." password = ".DB_ENCRYPTION."('".$req['password']."'),";		// password 값이 있는경우에만 update
		
		$sql .= " cell='".$req['cell']."', tel='".$req['tel']."', email='".$req['email']."', etc='".$req['etc']."', editdate=now(),
			onoff=".chkIsset($req[onoff]).", newold=".chkIsset($req[newold]).", route=".$req[route].", lastchange='".$req[lastchange]."', telconsult='".$req[telconsult]."',
			resertype=".chkIsset($req[resertype]).", offchgname='$req[offchgname]'
			WHERE no = ".$req['no'];
		

		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		return $result;
	}

	// 예약 중복 체크 update시
	function checkReservationForUpdate($param = "") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$sql = " SELECT no, resertime FROM ".$this->tableName."
			WHERE doctor_fk=$param[doctor_fk]
			AND hospital_fk=$param[hospital_fk]
			AND reserdate='$param[reserdate]'
			AND resertime='$param[resertime]'
			AND state != 4";

		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		$row=mysql_fetch_array($result);
		$cnt = $row['cnt'];

		return $cnt;
	}

	// 수정 - 비밀번호 체크 후 중복예약체크
	function update($req, $userCon) {
		$result=0;

		if ($userCon) {	// 사용자일때
			if ($this->checkPassword($req)) {	// 비밀번호 체크
				if (RESER_JUNGBOK) {	// 중복체크
					$result = $this->reserUpdate($req);
				} else {
					$check = $this->checkReservationForUpdate($req);
					if ($check) {
						if ($req['no'] != $check['no'] && $req['resertime'] != $check['resertime']) {
							$result = -2;	// 중복인 경우
						} else {
							$result = $this->reserUpdate($req);
						}
					} else {
						$result = $this->reserUpdate($req);
					}
				}
			} else {	// 비밀번호 틀린 경우
				$result = -1;
			}
		} else {	// 관리자일때
			if (RESER_JUNGBOK) {	// 중복체크
				$result = $this->reserUpdate($req);
			} else {
				$check = $this->checkReservationForUpdate($req);
				if ($check) {
					if ($req['no'] != $check['no'] && $req['resertime'] != $check['resertime']) {
						$result = -2;
					} else {
						$result = $this->reserUpdate($req);
					}
				} else {
					$result = $this->reserUpdate($req);
				}
			}
		}

		return $result;
	}

	// 삭제
	function delete($no=0) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = " DELETE FROM ".$this->tableName." WHERE no = ".$no;

		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		return $result;
	}

	// 목록
	function getData($no=0) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = "
			SELECT *,
				(SELECT name FROM hospital WHERE hospital.no = ".$this->tableName.".hospital_fk) AS hospital_name,
				(SELECT name FROM doctor WHERE doctor.no = ".$this->tableName.".doctor_fk) AS doctor_name,
				(SELECT name FROM clinic WHERE clinic.no = ".$this->tableName.".clinic_fk) AS clinic_name
			FROM ".$this->tableName."
			WHERE no = ".$no;

		// 조회수 증가
		if ($userCon) {
			mysql_query("UPDATE ".$this->tableName." SET readno=readno+1 WHERE no=".$no, $conn);
		}
		
		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		$data = mysql_fetch_assoc($result);

		return $data;
	}

	// 비밀번호확인
	function checkPassword($req='') {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = "
			SELECT 
				count(*) AS cnt
			FROM ".$this->tableName."
			WHERE no=".$req['no']." and password=".DB_ENCRYPTION."('".$req['password']."')";

		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		$data = mysql_fetch_assoc($result);
		$cnt = $data[cnt];

		$boolResult = false;
		if ($cnt > 0) {
			$boolResult = true;
		}
		return $boolResult;
	}

	// 완료처리
	// return > 0 이면 정상
	function confirmReservation($req="") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = "
			UPDATE ".$this->tableName." SET 
				state=3, lastchange='$req[lastchange]', confirmdate=NOW()
			WHERE no = ".$req['no'];

		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		return $result;
	}

	function updateSmsNo($req="") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = "
			UPDATE reservation SET 
				sms_no=".$req['smsNo']."
			WHERE no = ".$req['userNo'];

		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		return $result;
	}

	// 취소처리
	// 비밀번호 체크 후 사용자 접속여부에 따라 수정자 id값 저장
	function cancelReservation($req="", $userCon, $id) {

		$r = 0;
		$lastchange = '';
		if ($userCon) {
			if ($this->checkPassword($req) > 0) {
				if ($id) {
					$lastchange = '회원('.$id.')';
				} else {
					$lastchange = '비회원(직접변경)';
				}
			} else {
				$r = -1;
			}
		} else {
			$lastchange = '관리자('.$id.')';
		}

		if ($r > -1) {
			$dbconn = new DBConnection();
			$conn = $dbconn->getConnection();

			$sql = "
				UPDATE ".$this->tableName." SET 
					state=4, lastchange='$lastchange', canceldate=NOW()
				WHERE no = ".$req['no'];

			$result = mysql_query($sql, $conn);
			mysql_close($conn);
			return $result;

		}
	}

	// 검색일의 예약 상황 조회
	function checkHoliday($req) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = "
			SELECT 
				count(*) AS cnt
			FROM holiday
			WHERE hospital_fk=".$req[hospital_fk]." AND holiday='".$req[reserdate]."' ";
		



		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		$data = mysql_fetch_assoc($result);
		$cnt = $data[cnt];

		return $cnt;
	}

	// 검색일의 예약 상황 조회
	function resertimeList($req) {

		$cnt = $this->checkHoliday($req);

		$reserholytype = 0;
		if ($cnt > 0) $reserholytype = 1;

		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = "
			SELECT	c.resertime, IFNULL(d.cnt, 0) AS resercount, IFNULL(d.resertime, 0) AS reseryesno
			FROM (	
				SELECT a.resertime
					FROM
						(SELECT intervaltype, resertime FROM resertimeinterval) AS a,
						(SELECT ";
		if ($reserholytype == 1) {
			$sql .= " spe_start_time AS starttime, spe_end_time AS endtime, ";
		} else {
			$sql .= " CASE DAYOFWEEK('$req[reserdate]') 
								WHEN 1 THEN sun_start_time
								WHEN 2 THEN mon_start_time
								WHEN 3 THEN tue_start_time
								WHEN 4 THEN wed_start_time
								WHEN 5 THEN thu_start_time
								WHEN 6 THEN fri_start_time
								ELSE sat_start_time
							END AS starttime,
							CASE DAYOFWEEK('$req[reserdate]') 
								WHEN 1 THEN sun_end_time
								WHEN 2 THEN mon_end_time
								WHEN 3 THEN tue_end_time
								WHEN 4 THEN wed_end_time
								WHEN 5 THEN thu_end_time
								WHEN 6 THEN fri_end_time
								ELSE sat_end_time
							END AS endtime, ";
		}
		$sql .= "
							lunch_start_time, lunch_end_time, dinner_start_time, dinner_end_time, time_interval
						FROM doctortime
						WHERE doctor_fk=$req[doctor_fk] AND startday <= '$req[reserdate]'
						ORDER BY startday DESC LIMIT 0,1) AS b
					WHERE
						a.intervaltype = b.time_interval
						AND a.resertime >= b.starttime
						AND a.resertime < b.endtime
						AND NOT (a.resertime >= b.lunch_start_time AND a.resertime < b.lunch_end_time)
						AND NOT (a.resertime >= b.dinner_start_time AND a.resertime < b.dinner_end_time)
					) AS c
				LEFT OUTER JOIN (
					SELECT resertime, COUNT(*) AS cnt FROM reservation
					WHERE 
						hospital_fk=$req[hospital_fk]
						AND doctor_fk = $req[doctor_fk]
						AND state != 4
						AND reserdate = '$req[reserdate]'
					GROUP BY resertime
				) AS d
				ON c.resertime = d.resertime
			";


		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		return $result;
	}

	// holiday테이블에 해당 병원pk와 날짜가 존재하는지 여부
	function chkHolidayHospital($hospital_fk, $sholiday) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = "
			SELECT 
				count(*) AS cnt
			FROM holiday
			WHERE hospital_fk=".$hospital_fk." AND holiday='".$sholiday."' ";

		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		$data = mysql_fetch_assoc($result);
		$cnt = $data[cnt];

		return $cnt;
	}

	// 지점별 예약 내역
	function hospitalday($hospital_fk, $searchDay, $startTime, $endTime) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$whereSql = $this->getWhereSql($param);	// where절

		$sql = "
			SELECT
				 a.resertime AS realtime, b.resertime, b.name, IFNULL(b.state,0) AS state, b.name, ifnull(b.no, 0) AS no, ifnull(b.doctor_fk, 0) AS doctor_fk,
				 b.etc, b.clinic_name, ifnull(b.onoff, 0) AS onoff																																			
			FROM
				(SELECT
					resertime, endtime				
				FROM
					resertimeinterval WHERE intervalType = 2  AND (resertime >= '$startTime' AND resertime <= '$endTime')  order by resertime) AS a
			LEFT OUTER JOIN
				(SELECT
					   no, member_fk, resertime, state, name, doctor_fk, etc, 
					   (SELECT name FROM clinic WHERE no = clinic_fk) AS clinic_name, onoff																			
				FROM
					reservation
				WHERE
					 hospital_fk = $hospital_fk AND doctor_fk IN (SELECT no FROM doctor WHERE hospital_fk = $hospital_fk ) AND reserdate = '$searchDay') AS b
			ON  a.resertime <= b.resertime AND a.endtime > b.resertime
			ORDER BY realtime ASC, doctor_fk ASC, resertime ASC	";
		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		return $result;
	}

	
	

}


?>