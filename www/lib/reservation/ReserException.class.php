<?
/*
온라인예약 예외처리

*/

include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/db/DBConnection.class.php";

class ReserException {

	// 검색 파라미터 (초기 개발시 검색조건 세팅필요)
	var $param = array (
					
				);

	var $conn;

	// 생성자
	function ReserException() {
	}

	// 전체로우수, 페이지카운트
	function getCount($param = "") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$whereSql = $this->getWhereSql($param);	// where절
		$sql = " SELECT COUNT(*) AS cnt FROM ".$this->tableName.$whereSql;

		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		$row=mysql_fetch_array($result);
		$totalCount = $row['cnt'];
		$pageCount = getPageCount($this->pageRows, $totalCount);

		$data[0] = $totalCount;
		$data[1] = $pageCount;

		return $data;
	}

	// 예약 예외 저장
	function insert($req="") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = "
			INSERT INTO reserexception 
    			(doctor_fk, reserdate, resertime, registdate) 
			VALUES 
				($req[doctor_fk], '$req[reserdate]', '$req[resertime], NOW())
			";

		mysql_query($sql, $conn);

		$sql = "SELECT LAST_INSERT_ID() AS lastNo";
		$result = mysql_query($sql, $conn);
		$row = mysql_fetch_array($result);
		$lastNo = $row['lastNo'];
		mysql_close($conn);
		return $lastNo;
		
	}

	// 예약 예외 번호로 불러오기
	function getData($no=0) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = "
			SELECT no, doctor_fk, reserdate, resertime, registdate 
			FROM reserexception
			WHERE no = ".$no;

		// 조회수 증가
		if ($userCon) {
			mysql_query("UPDATE ".$this->tableName." SET readno=readno+1 WHERE no=".$no, $conn);
		}
		
		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		$data = mysql_fetch_assoc($result);

		return $data;
	}

	// 의료진, 예약일, 예약시간으로 검색. 있으면 true
	function isReserException($req = "") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$sql = " SELECT COUNT(*) AS cnt FROM reserexception
			WHERE 0=0";
		if ($req['sdoctor_fk'] > 0) {
			$sql .= " AND doctor_fk=".$req['sdoctor_fk'];
		}
		if ($req['sreserdate']) {
			$sql .= " AND reserdate = '$req[reserdate]'";
		}
		if ($req['sresertime']) {
			$sql .= " AND resertime = '$req[resertime]'";
		}

		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		$row=mysql_fetch_array($result);
		$cnt = $row['cnt'];
		$r = false;
		if ($cnt > 0) {
			$r = true;
		}
		return $r;
	}

	// 예약 예외 리스트 [의료진이나 예약 날짜로 검색]
	function getList($req='') {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = "
			SELECT
				nno, doctor_fk, reserdate, resertime, registdate 
			FROM reserexception
			WHERE 0=0 ";
		if ($req['sdoctor_fk'] > 0) {
			$sql .= " AND doctor_fk=".$req['sdoctor_fk'];
		}
		if ($req['reserdate']) {
			$sql .= " AND reserdate='$req[reserdate]'";
		}
		$sql .= " ORDER BY reserdate ASC, resertime ASC ";

		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		return $result;
	}

	// 삭제
	function delete($req) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = " DELETE FROM reserexception WHERE doctor_fk=$req[sdoctor_fk] AND reserdate = '$req[sreserdate]' AND resertime = '$req[sresertime]'";

		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		return $result;
	}

}


?>