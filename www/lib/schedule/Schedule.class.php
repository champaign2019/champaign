<? session_start(); 

include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/db/DBConnection.class.php";

class Schedule {
	
	//검색 파라미터 (초기 개발시 검색조건 셋팅필요)
	var $param = array (
					"sval",
					"stype"
		);

	var $tableName;
	var $pageRows; //페이지로우수
	var $startPageNo=0;
	public $regPageNo=1;
	var $conn;
	var $parent_fk;

	//생성자
	function Schedule($pageRows=0, $tableName='', $request='') {
		$this->pageRows = $pageRows;
		$this->tableName = $tableName;
		$this->reqPageNo = ($request['reqPageNo'] == 0) ? 1 : $request['reqPageNo'];	// 요청페이지값 없을시 1로 세팅
		if ($request['reqPageNo'] > 0) {
			$this->startPageNo = ($request['reqPageNo']-1) * $this->pageRows;
		}
		$this->parent_fk = $request['no'];
//		$this->tablename = $tablename;
	}

	// 검색 파라미터 queryString 생성 
	function getQueryString($page="", $no=0, $request='') {	
		$str = '';
		
		for ($i=0; $i<count($this->param); $i++) {
			if (isset( $request[$this->param[$i]] )) {
				$str = $str.$this->param[$i]."=".urlencode($request[$this->param[$i]])."&";
			}
		}

		if ($no > 0) $str = $str."no=".$no;			// no값이 있을 경우에만 파라미터 세팅 (페이지 이동시 no필요 없음)

		$return = '';
		if ($str) {
			$return = $page.'?'.$str;
		} else {
			$return = $page;
		}

		return $return;
	}

	//sql WHERE절 생성
	function getWhereSql($p) {
		$whereSql = " WHERE del_state=0 AND menu_fk = ";
		if($p['menu_fk']) {
			$whereSql.=$p['menu_fk'];
		} else {
			$whereSql.="0";
		}

		if ($p['sval']) {
			if ($p['stype'] == 'all') {
				$whereSql = $whereSql." AND ( (user_name LIKE '%".$p['sval']."%' ) OR (contents LIKE '%".$p['sval']."%' ) OR (title LIKE '%".$p['sval']."%' ) )";
			} else {
				$whereSql = $whereSql. " AND (".$p['sval']." LIKE '%".$p['sval']."%' )";
			}
		}

		if ($p['sboard_type']) {
			$whereSql = $whereSql. " AND board_type=".$p['sboard_type'];
		}

		if ($p['viewReply'] == '0') {
			$whereSql = $whereSql. " AND ono = 0";
		}
		return $whereSql;
	}

	//sql 정렬절 생성
	function getOrderby($p) {
		$orderbySql = " ORDER BY ";
		if (!$p['orderby']) {
			if ($p['board_type'] == "2") {
				"gno DESC, ono ASC";
			} else {
				"registdate DESC";
			}
		} else {
			$orderbySql .= $p['orderby']." ".$p['ordertype'];
		}
	}

	//목록
	function schedulelist($req='') {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$whereSql = $this->getWhereSql($req);

		$sql = "
			SELECT * FROM schedule
				WHERE ((startdate LIKE concat('%','".$req['today_year']."','-','%')
				AND startdate LIKE concat('%','-','".$req['today_month']."','-','%'))
				OR
				(enddate LIKE concat('%','".$req['today_year']."','-','%')
				AND enddate LIKE concat('%','-','".$req['today_month']."','-','%')))
				AND del_state = 0 AND menu_fk = '".$req['menu_fk']."'"; 
		
		if($req['sval']) {
			if($req['stype'] == 'all') {
				$sql .=" AND ( (user_name LIKE '%'".$req['sval']."'%' ) OR (contents LIKE '%'".$req['sval']."'%' ) OR (title LIKE '%'".$req['sval']."'%' ) )";
			} else {
				$sql .="('".$req['stype']."' LIKE '%'".$req['sval']."'%' )";
			}
		}
		$sql .=" ORDER BY startdate ASC ";
		
//	echo $sql;	
		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		return $result;
	}


	//전체로우수, 페이지카운트
	function getCount($param = "") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$whereSql = $this->getWhereSql($param);	// where절

		$sql = " SELECT COUNT(*) AS cnt FROM schedule ".$whereSql;
		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		$row=mysql_fetch_array($result);

		$totalCount = $row['cnt'];
		$pageCount = getPageCount($this->pageRows, $totalCount);

		$data[0] = $totalCount;
		$data[1] = $pageCount;
		//echo($sql);
		return $data;
	}

	function getListTop($param="") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$whereSql = $this->getWhereSql($param);	// where절

		$sql = "SELECT *, (SELECT COUNT(*) FROM comment WHERE b.no = comment.parent_fk and comment.tablename = 'schedule') AS c_cnt FROM schedule AS b ";
        $sql .= $whereSql." AND top = 1";
        $sql .= $this->getOrderby($param);

        $sql .= " LIMIT ".$this->startPageNo.", ".$this->pageRows." ";		

		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		return $result;
	}


	function getData($req='') {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$sql = "SELECT *, (SELECT COUNT(*) FROM comment WHERE b.no = comment.parent_fk and comment.tablename = 'schedule') AS c_cnt FROM schedule AS b
    	WHERE no = ".$req['no'];

//echo $sql;
		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		$data = mysql_fetch_assoc($result);

		return $data;

	}

	function getList($req='') {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$whereSql = $this->getWhereSql($req);
		
		$sql = "SELECT *, (SELECT COUNT(*) FROM comment WHERE b.no = comment.parent_fk and comment.tablename = 'schedule')  AS c_cnt FROM schedule AS b";
		$sql .= $whereSql;
        $sql .= $this->getOrderby($param);

        $sql .= " LIMIT ".$this->startPageNo.", ".$this->pageRows." ";	

//echo $sql;

		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		return $result;

	}

	function stateUpdate($req='') {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = "UPDATE schedule SET state = 3 WHERE no = ".$req['no'];

		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		return $result;

	}

	function insert($req="") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = "INSERT INTO schedule (";
		$sql.= "menu_fk, board_type, user_fk, title, contents,
    			readno, top, gno, ono, indent,
    			registdate, user_name, list_image,
    			startdate, enddate, state, moviename, moviename_org, line_color";
		$sql.=") VALUES (";
		$sql.= "'".$req['menu_fk']."','".$req['board_type']."','".$req['user_fk']."','".$req['title']."','".$req['contents']."','"
				.$req['readno']."','".$req['top']."','".$req['gno']."','".$req['ono']."','".$req['indent'].
				"',NOW(),'".$req['user_name']."','".$req['list_image']."','"
				.$req['startdate']."','".$req['enddate']."','".$req['state']."','".$req['moviename']."','".$req['moviename_org']."','".$req['line_color']."' )";
//echo $sql;
		mysql_query($sql,$conn);
		
		$sql = "SELECT LAST_INSERT_ID() AS lastNo";
		$result = mysql_query($sql, $conn);


		$row = mysql_fetch_array($result);
		$lastNo = $row['lastNo'];
		$req['no'] = $lastNo;

		mysql_close($conn);
		return $lastNo;

	}

	function update($req="") {		
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql="UPDATE schedule SET ";
		$sql.="title='".chkIsset($req['title'])."', contents='".chkIsset($req['contents'])."', list_image='".chkIsset($req['list_image'])."', startdate='".chkIsset($req['startdate'])."', enddate='".chkIsset($req['enddate'])."', state='".chkIsset($req['state'])."', line_color='".chkIsset($req['line_color'])."'";

		if($req['moviename_chk'] == "1"){		
	       	$sql.=", moviename='', moviename_org=''";
		}	
		if($req['moviename']){ 
	        $sql.=", moviename='".$req['moviename']."', moviename_org='".$req['moviename_org']."'";    
        }
		if($req['registdate']){		  
			$sql.=", registdate='".$req['registdate']."'";        
        }
		$sql.=" WHERE no =".$req['no'];

		$r = mysql_query($sql, $conn);

		mysql_close($conn);
		return $r;

	}

	function delete($no) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
	
		$sql="UPDATE schedule SET del_state=1 WHERE no=".$no;
		
		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		return $result;
	}


	// 조회수 +1
	function updateReadno($no=0) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = "UPDATE schedule SET readno = readno+1 WHERE no = $no";

		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		return $result;
	}
	
	// gno 최대값+1
	function getMaxGno() {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$sql = " SELECT IFNULL(MAX(gno), 0)+1 AS maxgno FROM ".$this->tableName;

		$result = mysql_query($sql, $conn);

		$row = mysql_fetch_array($result);
		$maxgno = $row['maxgno'];

		return $maxgno;
	}

	// ono 최대값
	function getMaxOno($gno) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$sql = " SELECT IFNULL(MAX(ono), 0) AS maxono FROM schedule WHERE gno=".$gno;

		$result = mysql_query($sql, $conn);

		$row=mysql_fetch_array($result);
		$maxono = $row['maxono'];

		return $maxono;
	}

	// ono 최소값
	function getMinOno($gno=0, $ono=0, $nested=0) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$sql = " SELECT IFNULL(MIN(ono), 0) AS minono FROM schedule WHERE gno=".$gno." AND ono > ".$ono." AND nested <= ".$nested;

		$result = mysql_query($sql, $conn);

		$row=mysql_fetch_array($result);
		$minono = $row['minono'];

		return $minono;
	}

	// ono가 0이 아닌경우 같은 그룹내 minOno보다 크거나 같은 ono +1
	function updateOno($gno=0, $minOno=0) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = "UPDATE schedule SET ono = ono+1 WHERE gno = $gno AND ono >= $minOno";

		$result = mysql_query($sql, $conn);
		return $result;

	}

	// rownum 구하기
	function getRowNum($req='') {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$whereSql = $this->getWhereSql($req);	// where절

		$sql = "
			SELECT rownum FROM (
				SELECT @rownum:=@rownum+1 AS rownum, no, title FROM schedule, (SELECT @rownum:=0) AS r
				".$whereSql."
				".$this->getOrderby($param)."
			) AS r2
			WHERE r2.no = ".$req['no'];
		
		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		$result=mysql_fetch_array($result);

		return $result[rownum];
	}

	// 다음글 가져오기
	function getNextRowNum($req='', $rownum=0) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$whereSql = $this->getWhereSql($req);	// where절

		$sql = "
			SELECT
				ifnull(rownum,0), ifnull(no,0) AS next_no, title AS next_title
			FROM (
				SELECT @rownum:=@rownum+1 AS rownum, no, title from schedule, (SELECT @rownum:=0) AS r
				".$whereSql."
				ORDER BY top DESC, gno DESC, ono ASC
			) AS r2
			WHERE r2.rownum = $req[rownum]+1";
		
		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		return $result;
	}

	// 이전글 가져오기
	function getPrevRowNum($req='', $rownum=0) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$whereSql = $this->getWhereSql($req);	// where절

		$sql = "
			SELECT
				ifnull(rownum,0), ifnull(no,0) AS next_no, title AS next_title
			FROM ( SELECT @rownum:=@rownum+1 AS rownum, no, title from schedule, (SELECT @rownum:=0) AS r
				".$whereSql."
				ORDER BY top DESC, gno DESC, ono ASC ) AS r2 WHERE r2.rownum = $req[rownum]-1";
		
		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		return $result;
	}


}

?>