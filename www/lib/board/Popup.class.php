<?
/*


*/

include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/db/DBConnection.class.php";

class Popup {

	// 검색 파라미터 (초기 개발시 검색조건 세팅필요)
	var $param = array (
					"stype",
					"sval"
				);

	var $tableName;			// 테이블명
	var $pageRows;			// 페이지 로우수
	var $startPageNo=0;		// limit 시작페이지
	public $reqPageNo=1;	// 요청페이지
	var $conn;

	// 생성자
	function Popup($pageRows=0, $tableName='', $request='') {
		$this->pageRows = $pageRows;
		$this->tableName = $tableName;
		$this->reqPageNo = ($request['reqPageNo'] == 0) ? 1 : $request['reqPageNo'];	// 요청페이지값 없을시 1로 세팅
		if ($request['reqPageNo'] > 0) {
			$this->startPageNo = ($request['reqPageNo']-1) * $this->pageRows;
		}
	}

	// 검색 파라미터 queryString 생성
	function getQueryString($page="", $no=0, $request='') {	
		$str = '';
		
		for ($i=0; $i<count($this->param); $i++) {
			if ($request[$this->param[$i]]) {
				$str .= $this->param[$i]."=".urlencode($request[$this->param[$i]])."&";
			}
		}

		if ($no > 0) $str .= "no=".$no;			// no값이 있을 경우에만 파라미터 세팅 (페이지 이동시 no필요 없음)

		$return = '';
		if ($str) {
			$return = $page.'?'.$str;
		} else {
			$return = $page;
		}

		return $return;
	}

	// sql WHERE절 생성
	function getWhereSql($p) {
		$whereSql = " WHERE 1=1";
		if ($p['sval']) {
			if ($p['stype'] == 'all') {
				$whereSql .= " AND ((title like '%".$p['sval']."%' ) or (contents like '%".$p['sval']."%') ) ";
			} else {
				$whereSql .= " AND (".$p['stype']." LIKE '%".$p['sval']."%' )";
			}
		}
		if ($p['snot_popup_device_type'] > -1) {
			$whereSql .= "
				AND popup_device_type != ".$p['snot_popup_device_type']."
			";
		}
		if ($p['sstate'] > -1) {
			$whereSql .= "
				AND if (start_day <= now() and end_day > now(), 1, 0) = '".$p['sstate']."'
			";
		}
		return $whereSql;
	}


	// 전체로우수, 페이지카운트
	function getCount($param = "") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$whereSql = $this->getWhereSql($param);	// where절
		$sql = " SELECT COUNT(*) AS cnt FROM ".$this->tableName.$whereSql;

		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		$row=mysql_fetch_array($result);
		$totalCount = $row['cnt'];
		$pageCount = getPageCount($this->pageRows, $totalCount);

		$data[0] = $totalCount;
		$data[1] = $pageCount;

		return $data;
	}

	// 목록
	function getList($param='') {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$whereSql = $this->getWhereSql($param);	// where절

		$sql = "
			SELECT *, IF (start_day <= left(NOW(), 10) AND end_day >= left(NOW(), 10), 1, 0) AS state
			FROM ".$this->tableName."
			".$whereSql."
			ORDER BY registdate DESC LIMIT ".$this->startPageNo.", ".$this->pageRows." ";

//		echo $sql;

		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		return $result;
	}

	// 관리자 등록
	function insert($req="") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		//$gno = $this->getMaxGno();
		$sql = "
			INSERT INTO ".$this->tableName." (
				type, title, contents, relation_url, ";
		if ($req[imagename]) {
			$sql .= "imagename, imagename_org, image_alt, ";
		}
		if ($req[filename]) {
			$sql .= "filename, filename_org, filesize, ";
		}
		if ($req[moviename]) {
			$sql .= "moviename, moviename_org, ";
		}
		$sql .= "area_top, area_left, popup_width, popup_height, start_day, end_day, border_color, bg_color";
		/* 모바일버전 추가 */
		$sql .= "
        	, popup_device_type	
        	, popup_width_m	
			, popup_height_m	
			, border_color_m	
			, bg_color_m	
			, area_left_m	
			, area_top_m	
			, contents_m	
			, relation_url_m	
			, imagename_m	
			, imagename_m_org	
			, image_alt_m	
		";
		/* 모바일버전 추가 */
		$sql .= "
			) VALUES (
			'$req[type]', 
			'$req[title]',
			'$req[contents]',
			'$req[relation_url]', ";
		if ($req[imagename]) {
			$sql .= "'$req[imagename]', '$req[imagename_org]', '$req[image_alt]', ";
		}
		if ($req[filename]) {
			$sql .= "'$req[filename]', '$req[filename_org]', $req[filesize], ";
		}
		if ($req[moviename]) {
			$sql .= "'$req[moviename]', '$req[moviename_org]', ";
		}
		$sql .= "
			".chkIsset($req[area_top]).",
			".chkIsset($req[area_left]).", 
			".chkIsset($req[popup_width]).",
			".chkIsset($req[popup_height]).",
			DATE_FORMAT('$req[start_day]', '%Y-%m-%d 00:00:00'),
			DATE_FORMAT('$req[end_day]', '%Y-%m-%d 23:59:59'),
			'$req[border_color]',
			'$req[bg_color]'";
		/* 모바일버전 추가 */
		$sql .= "
        	, '$req[popup_device_type]'	
        	, '$req[popup_width_m]'
			, '$req[popup_height_m]'
			, '$req[border_color_m]'
			, '$req[bg_color_m]'
			, '$req[area_left_m]'
			, '$req[area_top_m]'
			, '$req[contents_m]'
			, '$req[relation_url_m]'
			, '$req[imagename_m]'
			, '$req[imagename_m_org]'
			, '$req[image_alt_m]'
		";
		/* 모바일버전 추가 */
		$sql .= "
			)
		";

		mysql_query($sql, $conn);

		$sql = "SELECT LAST_INSERT_ID() AS lastNo";
		$result = mysql_query($sql, $conn);
		$row = mysql_fetch_array($result);
		$lastNo = $row['lastNo'];
		mysql_close($conn);
		return $lastNo;
	}

	// 수정
	function update($req="") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		// 기존 첨부파일 삭제
		if ($req[filename_chk] == "1") {
			mysql_query("UPDATE ".$this->tableName." SET filename='', filename_org='', filesize=0 WHERE no=".$req[no], $conn);
		}
		// 기존 동영상파일 삭제
		if ($req[moviename_chk] == "1") {
			mysql_query("UPDATE ".$this->tableName." SET moviename='', moviename_org='' WHERE no=".$req[no], $conn);
		}
		// 기존 목록이미지 삭제
		if ($req[imagename_chk] == "1") {
			mysql_query("UPDATE ".$this->tableName." SET imagename='', imagename_org='', image_alt='' WHERE no=".$req[no], $conn);
		}
		// 기존 목록이미지 삭제
		if ($req['imagename_chk_m'] == "1") {
			mysql_query("UPDATE ".$this->tableName." SET imagename_m='', imagename_m_org='', image_alt_m='' WHERE no=".$req[no], $conn);
		}

		$sql = "
			UPDATE ".$this->tableName." SET 
				type='$req[type]', title='$req[title]', contents='$req[contents]',
        		relation_url='$req[relation_url]', ";
		if ($req[filename]) {
			$sql .= "filename='$req[filename]', filename_org='$req[filename]', filesize=$req[filesize], ";
		}
		if ($req[moviename]) {
			$sql .= "moviename='$req[moviename]', moviename_org='$req[moviename_org]', ";
		}
		if ($req[imagename]) {
			$sql .= "imagename='$req[imagename]', imagename_org='$req[imagename_org]', ";
		}
		$sql .= " image_alt='$req[image_alt]', area_top=".chkIsset($req[area_top]).", area_left=".chkIsset($req[area_left]).", popup_width=".chkIsset($req[popup_width]).", popup_height=".chkIsset($req[popup_height]).",
			start_day = DATE_FORMAT('$req[start_day]', '%Y-%m-%d 00:00:00'), end_day=DATE_FORMAT('$req[end_day]', '%Y-%m-%d 23:59:59'), border_color='$req[border_color]', bg_color='$req[bg_color]'
		";
		/* 모바일버전 추가 */
		$sql .= "
        	, popup_device_type = '$req[popup_device_type]'
        	, popup_width_m = '$req[popup_width_m]'
			, popup_height_m	 = '$req[popup_height_m]'
			, border_color_m = '$req[border_color_m]'
			, bg_color_m = '$req[bg_color_m]'
			, area_left_m = '$req[area_left_m]'
			, area_top_m = '$req[area_top_m]'
			, contents_m = '$req[contents_m]'
			, relation_url_m = '$req[relation_url_m]'
		";
		if ($req['imagename_m']) {
			$sql .= ", imagename_m='$req[imagename_m]', imagename_m_org='$req[imagename_m_org]'";
		}
		/* 모바일버전 추가 */
		$sql .= "
			, image_alt_m='$req[image_alt_m]'
			WHERE no = ".$req['no'];

		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		return $result;
	}

	// 삭제
	function delete($no=0) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = " DELETE FROM ".$this->tableName." WHERE no = ".$no;

		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		return $result;
	}

	// 목록
	function getData($no=0) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = "
			SELECT *, IF (start_day <= NOW() AND end_day > NOW(), 0, 1) AS state
			FROM ".$this->tableName."
			WHERE no = ".$no;
		
		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		$data = mysql_fetch_assoc($result);

		return $data;
	}

	// 메인목록 조회
	function getMainList() {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$whereSql = $this->getWhereSql($param);	// where절

		$sql = "
			SELECT *
			FROM ".$this->tableName."
			WHERE start_day <= NOW() AND end_day > NOW()
			ORDER BY no DESC ";

		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		

		return $result;
	}
	

	// 관리자 등록
	function insertTopPopup($req="") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		//$gno = $this->getMaxGno();
		$sql = "
			INSERT INTO ".$this->tableName." 
			(
				title
				, start_day
				, end_day
				, popup_device_type	
				, relation_url
				, relation_url_m	

				, popup_width
				, popup_height
				, popup_width_m	
				, popup_height_m	
		";
		if ($req[imagename]) {
			$sql .= " , imagename, imagename_org, image_alt ";
		}
		if ($req[imagename_m]) {
			$sql .= " , imagename_m	, imagename_m_org, image_alt_m	 ";
		}
		/* 모바일버전 추가 */
		$sql .= "
			)
			VALUES
			(
				'$req[title]'
				, DATE_FORMAT('$req[start_day]', '%Y-%m-%d 00:00:00')
				, DATE_FORMAT('$req[end_day]', '%Y-%m-%d 23:59:59')
				, '$req[popup_device_type]'	
				, '$req[relation_url]'
				, '$req[relation_url_m]'

				, ".chkIsset($req[popup_width])."
				, ".chkIsset($req[popup_height])."
				, ".chkIsset($req[popup_width_m])."
				, ".chkIsset($req[popup_height_m])."
		";
		if ($req[imagename]) {
			$sql .= ", '$req[imagename]', '$req[imagename_org]', '$req[image_alt]'";
		}
		if ($req[imagename_m]) {
			$sql .= ", '$req[imagename_m]', '$req[imagename_m_org]', '$req[image_alt_m]'";
		}
		/* 모바일버전 추가 */
		$sql .= "
			)
		";

//		echo $sql;

		mysql_query($sql, $conn);

		$sql = "SELECT LAST_INSERT_ID() AS lastNo";
		$result = mysql_query($sql, $conn);
		$row = mysql_fetch_array($result);
		$lastNo = $row['lastNo'];
		mysql_close($conn);
		return $lastNo;
	}

	// 수정
	function updateTopPopup($req="") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		// 기존 첨부파일 삭제
		// 기존 목록이미지 삭제
		if ($req[imagename_chk] == "1") {
			mysql_query("UPDATE ".$this->tableName." SET imagename='', imagename_org='', image_alt='' WHERE no=".$req[no], $conn);
		}
		// 기존 목록이미지 삭제
		if ($req['imagename_chk_m'] == "1") {
			mysql_query("UPDATE ".$this->tableName." SET imagename_m='', imagename_m_org='', image_alt_m='' WHERE no=".$req[no], $conn);
		}

		$sql = "
			UPDATE ".$this->tableName." SET 
				title='$req[title]'
				, start_day = DATE_FORMAT('$req[start_day]', '%Y-%m-%d 00:00:00')
				, end_day=DATE_FORMAT('$req[end_day]', '%Y-%m-%d 23:59:59')
				, popup_device_type = '$req[popup_device_type]'
				, relation_url='$req[relation_url]'
				, relation_url_m='$req[relation_url_m]'

				, popup_width = '$req[popup_width]'
				, popup_height	 = '$req[popup_height]'
				, popup_width_m = '$req[popup_width_m]'
				, popup_height_m	 = '$req[popup_height_m]'
		";

		if ( $req['imagename'] ) {
			$sql .= ", imagename='$req[imagename]', imagename_org='$req[imagename_org]' ";
		}
		$sql .= ", image_alt='$req[image_alt]' ";

		if ($req['imagename_m']) {
			$sql .= ", imagename_m='$req[imagename_m]', imagename_m_org='$req[imagename_m_org]' ";
		}
		$sql .= ", image_alt_m='$req[image_alt_m]' ";

		$sql .= "
			WHERE
				no = ".$req['no']."
		";

		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		return $result;
	}
	
	function readLimit($req = '') {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = "";
		$sql .= "
			SELECT
				*
				, IF (start_day <= NOW() AND end_day > NOW(), 0, 1) AS state
			FROM
				".$this->tableName."
			WHERE
				1=1
		";
		if ($req['snot_popup_device_type'] > -1) {
			$sql .= "
				AND popup_device_type != ".$req['snot_popup_device_type']."
			";
		}
		$sql .= "
			ORDER BY
				registdate DESC
			LIMIT 1
		";

//		echo $sql;
		
		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		$data = mysql_fetch_assoc($result);

		return $data;
	}

}


?>