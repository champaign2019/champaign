<? session_start(); 
/*


*/

include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/db/DBConnection.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/attachfile/Attachfile.class.php";

class Common {

	// 검색 파라미터 (초기 개발시 검색조건 세팅필요)
	var $param = array (
					"stype",
					"sval",
					"smain",
					"shospital_fk",
					"sclinic_fk"
				);

	var $paramCmd = array (
					"stype",
					"sval",
					"smain",
					"shospital_fk",
					"sclinic_fk",
					"cmd"
				);

	var $board_fk;			// 게시판 구분
	var $tableName;			// 테이블명
	var $category_tablename;// 테이블명
	var $pageRows;			// 페이지 로우수
	var $vq;				// echo로 쿼리 출력  view query;
	var $startPageNo=0;		// limit 시작페이지
	public $reqPageNo=1;	// 요청페이지
	var $conn;




	// 생성자
	function Common($pageRows=0, $tableName='', $request='') {
		$this->pageRows = $pageRows;
		$this->tableName = $tableName;
		if($request['category_tablename'] == null)
			$this->category_tablename = 'faq_category';
		else
			$this->category_tablename = $request['category_tablename'];


		$this->reqPageNo = ($request['reqPageNo'] == 0) ? 1 : $request['reqPageNo'];	// 요청페이지값 없을시 1로 세팅
		if ($request['reqPageNo'] > 0) {
			$this->startPageNo = ($request['reqPageNo']-1) * $this->pageRows;
		}
		if ($request['board_fk']) { $this->board_fk = $request['board_fk'];
		}
		if ($request['vq']) {
			$this->vq = $request['vq'];
		}
		if( !getSpamCheck($request) ) $request["cmd"] = "error";		// /include/headHtml.jsp 에서 스팸확인
		$this->attachfile = new Attachfile($tableName);

	}


	function getOrderby($param){
		$sql = '';
		
		if($param['orderby']){
			$sql = ' ORDER BY ';
			$i = 0;
			foreach($param['orderby'] as $item){
				if($i > 0){ $sql .= " ,";}
				$sql .= $item;
				$i++;
			}

		}

		return $sql;
	}

	// 검색 파라미터 queryString 생성
	function getQueryString($page="", $no=0, $request='') {	
		$str = '';
		
		for ($i=0; $i<count($this->param); $i++) {
			if (isset($request[$this->param[$i]])) {
				$str .= $this->param[$i]."=".urlencode($request[$this->param[$i]])."&";
			}
		}

		if ($no > 0) $str .= "no=".$no;			// no값이 있을 경우에만 파라미터 세팅 (페이지 이동시 no필요 없음)

		$return = '';
		if ($str) {
			$return = $page.'?'.$str;
		} else {
			$return = $page.'?';
		}
		return $return;
	}

	function getQueryStringCmd($page="", $no=0, $request='') {	
		$str = '';
		
		for ($i=0; $i<count($this->paramCmd); $i++) {
			if (isset($request[$this->paramCmd[$i]])) {
				$str .= $this->paramCmd[$i]."=".urlencode($request[$this->paramCmd[$i]])."&";
			}
		}

		if ($no > 0) $str .= "no=".$no;			// no값이 있을 경우에만 파라미터 세팅 (페이지 이동시 no필요 없음)

		$return = '';
		if ($str) {
			$return = $page.'?'.$str;
		} else {
			$return = $page.'?';
		}

		return $return;
	}

	// 검색 파라미터 queryString 생성, 추가파라미터 지정
	function getQueryStringAddParam($page="", $no=0, $request='', $paramName='', $paramValue='') {	
		$str = '';
		
		for ($i=0; $i<count($this->param); $i++) {
			if ($request[$this->param[$i]]) {
				$str = $str.$this->param[$i]."=".urlencode($request[$this->param[$i]])."&";
			}
		}

		if ($no > 0) $str = $str."no=".$no;			// no값이 있을 경우에만 파라미터 세팅 (페이지 이동시 no필요 없음)

		if ($paramValue) $str .= "&".$paramName."=".urlencode($paramValue);

		$return = '';
		if ($str) {
			$return = $page.'?'.$str;
		} else {
			$return = $page;
		}

		return $return;
	}

	// sql WHERE절 생성
	function getWhereSql($p) {
		$whereSql = " WHERE 1=1";
		if ($p['sval']) {
			if ($p['stype'] == 'all') {
				$whereSql .= " AND ( (name like '%".mysql_real_escape_string($p['sval'])."%' ) or (title like '%".mysql_real_escape_string($p['sval'])."%' ) or (contents like '%".mysql_real_escape_string($p['sval'])."%') ) ";
			}
			else if ($p['stype'] == 'addr0ANDtitle') {
				$whereSql .= "
					AND (
						(
							(addr0 like '%".mysql_real_escape_string($p['sval'])."%' )
							or (name like '%".mysql_real_escape_string($p['sval'])."%' )
						)
					)
				";
			}
			else {
				$whereSql .= " AND (".$p['stype']." LIKE '%".$p['sval']."%' )";
			}
		}
		if ($p['board_fk']) { $whereSql .= " AND board_fk = '".$p['board_fk']."'"; }
		if ($p['smain']) {
			$whereSql .= " AND main = ".$p['smain'];
		}
		if ($p['shospital_fk'] && $p['shospital_fk'] > -1) {
			$whereSql .= " AND hospital_fk = ".$p['shospital_fk'];
		}
		if ($p['sclinic_fk'] && $p['sclinic_fk'] > -1) {
			$whereSql .= " AND clinic_fk = ".$p['sclinic_fk'];
		}
		if ($p['scategory_fk']) {
			$whereSql .= " AND category_fk = ".$p['scategory_fk'];
		}
		if ($p['category']) {
			$whereSql .= "
				AND category like concat('%','".$p['category']."','%')
			";
		}
		return $whereSql;
	}
	// 전체로우수, 페이지카운트
	function getCount($param = "") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$whereSql = $this->getWhereSql($param);	// where절
		$sql = " SELECT COUNT(*) AS cnt FROM ".$this->tableName.$whereSql;

		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		$row=mysql_fetch_array($result);
		$totalCount = $row['cnt'];
		$pageCount = getPageCount($this->pageRows, $totalCount);

		$data[0] = $totalCount;
		$data[1] = $pageCount;

		return $data;
	}
	// 목록
	function getList($param='') {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$whereSql = $this->getWhereSql($param);	// where절
		
		$sql="select *,";

		if($param['category_tablename']){
        $sql.="(select name from ".$param['category_tablename']." where ".$param['category_tablename'].".no = ".$this->tableName.".category_fk) as category_name,";
		}
        
        $sql.="(select name from hospital where hospital.no = ".$this->tableName.".hospital_fk) as hospital_name,";
		
		$sql.="(select count(*) from comment where comment.parent_fk = ".$this->tableName.".no and comment.tablename = '".$this->tableName."') as comment_count,";
        $sql.="(select name from clinic where clinic.no = ".$this->tableName.".clinic_fk) as clinic_name ";
        $sql.="from ".$this->tableName." ";
        $sql.="left outer join imagefiles ";
        $sql.="on ".$this->tableName.".no = imagefiles.common_fk ";
        $sql.="and imagefiles.tablename = '".$this->tableName."'";

		$sql.=$whereSql;
      
		$sql.=$this->getOrderby($param);
		
		$sql .= " LIMIT ".$this->startPageNo.", ".$this->pageRows." ";
		
		//echo $sql;

		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		return $result;
	}



	// 관리자 등록
	function insert($req="") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
//		if($req['whereConsult'] == '1'){
		if(true){
		$gno = $this->getMaxGno();
		$req['gno'] = $gno;
		}

		$sql="INSERT INTO ".$this->tableName." (";
        $sql.="hospital_fk, member_fk, clinic_fk, category_fk, secret, password, name, email, tel, cell, title, contents, answer,";
        $sql.="relation_url, filename, filename_org, filesize, moviename, moviename_org, reserdate, resertime, state, registdate,";
        if($req['readno']){
		$sql.="readno,";
		}

        $sql.="manage_fk, top, main, newicon, iscall, ismail, gno, ono, nested";
		if ( $this->tableName == 'product' ) {
			$sql .= " , category ";
		}
		if ( $this->tableName != 'faq' ) {
			$sql .= " , zipcode, addr0 ";
		}
        $sql.=") VALUES (";
        $sql.=chkIsset($req['hospital_fk']).", ".chkIsset($req['member_fk']).", ".chkIsset($req['clinic_fk']).", ".chkIsset($req['category_fk'])." , ".chkIsset($req['secret']).", ".DB_ENCRYPTION."('".$req['password']."'),";
		$sql.="'".$req['name']."','". $req['email']."','". $req['tel']."','". $req['cell']."','". $req['title']."','".addslashes($req['contents'])."','".addslashes($req['answer'])."',";
        $sql.="'".$req['relation_url']."','". $req['filename']."','". $req['filename_org']."',".chkIsset($req['filesize']).",'".$req['moviename']."','". $req['moviename_org']."','". $req['reserdate']."','". $req['resertime']."',".chkIsset($req['state']).",";
	    if($req['registdateGubun'] == '0'){
		$sql.="'".$req['registdate']."',";	
		}else{
		$sql.="NOW(),";
		}
		if($req['readno']){
		$sql.=chkIsset($req['readno']).", ";
		}
       	$sql.=chkIsset($req['manage_fk']).",".chkIsset($req['top']).",".chkIsset($req['main']).",".chkIsset($req['newicon']).",".chkIsset($req['iscall']).",".chkIsset($req['ismail']).",".chkIsset($req['gno']).", ".chkIsset($req['ono']).", ".chkIsset($req['nested']);
		if ( $this->tableName == 'product' ) {
			$sql .= " , '".$req['category']."' ";
		}
		if ( $this->tableName != 'faq' ) {
			$sql .= " , '".$req['zipcode']."', '".$req['addr0']."' ";
		}
        $sql.=")";

echo $sql;

		mysql_query($sql, $conn);


		$sql = "SELECT LAST_INSERT_ID() AS lastNo";
		$result = mysql_query($sql, $conn);


		$row = mysql_fetch_array($result);
		$lastNo = $row['lastNo'];
		$req['no'] = $lastNo;
		
		/**
		다중파일 업로드
		**/
		$attachParam = array(
			'common_fk' => $lastNo
			, 'uploadPath' => $req['uploadPath']
		);
		$this->attachfile->insert($attachParam);
		/**
		다중파일 업로드
		**/



		
//		if( !$req['answer_depth']) $answer_depth = $req[no];
//		else $answer_depth = $req['answer_depth'].$req[no];
//		$sqlDepth = "update $this->tableName set ";
//		$sqlDepth .= " answer_depth = '".$answer_depth."@' ";
//		$sqlDepth .= " where no = ".$req[no];
//		mysql_query($sqlDepth, $conn);

		/***
			동적 필드
		***/
		if($lastNo > 0 && ( $req[imagename1] | $req[imagename2] | $req[imagename3] | $req[imagename4] | $req[imagename5] | $req[imagename6]| $req[imagename7] | $req[imagename8] | $req[imagename9])){
			$this->insertSubRows($req);
		}
		
		/**
		다중파일 업로드
		**/
		if($lastNo > 0 && $req['filenames']){
			foreach($_REQUEST['filenames'] as $key => $val){ 
				$req['filename'] = $_REQUEST['filenames'][$key];
				$req['filename_org'] = $_REQUEST['filenames_org'][$key];
				$req['filesize'] = $_REQUEST['filesizes'][$key];
				$this->insertMultiFile($req);
			}
		}

		mysql_close($conn);
		return $lastNo;
	}

	// 수정
	function update($req="") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$data = $this->getFilenames($req['no']);
		
		$sql="UPDATE ".$this->tableName." SET ";
		$sql.="hospital_fk=".chkIsset($req['hospital_fk']).", clinic_fk=".chkIsset($req['clinic_fk']).", category_fk=".chkIsset($req['category_fk']).", secret=".chkIsset($req['secret']).","; 
		$sql.="name='".$req['name']."', email='".$req['email']."', tel = '".$req['tel']."', cell='".$req['cell']."', title='".$req['title']."', contents='".addslashes($req['contents'])."', answer='".addslashes($req['answer'])."',";
		$sql.="relation_url='".$req['relation_url']."', reserdate = '".$req['reserdate']."', resertime = '".$req['resertime']."', state = ".chkIsset($req['state']).",";
        if($req['filename_chk'] == '1'){
		
       	$sql.="filename='', filename_org='', filesize=0,";
		
		}

		if($req['filename']){
		  
        $sql.="filename='".$req['filename']."', filename_org='".$req['filename_org']."', filesize=".chkIsset($req['filesize']).",";
        
        }
		
		if($req['password']){
		  
        $sql.="password=".DB_ENCRYPTION."('".$req['password']."'),";
        
        }

		if($req['moviename_chk'] == '1'){
		
       	$sql.="moviename='', moviename_org='',";
		
		}
		
		if($req['moviename']){
		  
        $sql.="moviename='".$req['moviename']."', moviename_org='".$req['moviename_org']."',";
        
        }

		if($req['registdate']){
		  
        $sql.="registdate='".$req['registdate']."',";
        
        }
		
		if($req['readno']){
		  
        $sql.="readno=".chkIsset($req['readno']).",";
        
        }

        $sql.="top=".chkIsset($req['top']).", main=".chkIsset($req['main'])." ,newicon=".chkIsset($req['newicon']).", iscall=".chkIsset($req['iscall']).", ismail=".chkIsset($req['ismail'])."";
		if ( $this->tableName == 'product' ) {
			$sql .= ", category = '".$req['category']."'";
		}
		if ( $this->tableName != 'faq' ) {
			$sql .= ", zipcode = '".$req['zipcode']."', addr0 = '".$req['addr0']."'";
		}

		if ($req['ono']) {
			$sql .= ", ono = '".$req['ono']."'";
		}
        
		$sql.=" WHERE no =".$req['no'];

//		echo $sql;
		
		$r = mysql_query($sql, $conn);

		if($r> 0){
			
			/**
			다중파일 업로드
			**/
			$attachParam = array(
				'common_fk' => $req['no']
				, 'uploadPath' => $req['uploadPath']
			);
			$this->attachfile->update($attachParam);
			/**
			다중파일 업로드
			**/
			
			$req['common_fk'] = $req['no'];

			if($req['filename_chk'] == "1" || $req['filename'] != ""){

				unlink($req['uploadPath'].$data['filename']);
			}
			
			if($req['moviename_chk'] == "1" || $req['moviename'] != ""){
				unlink($req['uploadPath'].$data['moviename']);
			}

			
			$sql = "select count(*) as cnt from imagefiles where tablename = '".$this->tableName."' and common_fk = ".$req['common_fk'];
			

			$result = mysql_query($sql, $conn);
			
			$isImage=mysql_fetch_array($result);
			
		
			
			if($isImage['cnt'] > 0){

				$sql="UPDATE imagefiles SET ";
				if($req['imagename1_chk'] == '1'){
				$sql.="imagename1='', imagename1_org='', listimage='', ";

				}
				
				if($req['imagename1']){
				$sql.="imagename1='".$req['imagename1']."', imagename1_org='".$req['imagename1_org']."', listimage='".$req['listimage']."',";
				}

				$sql.="image1_alt = '".$req['image1_alt']."',";

				if($req['imagename2_chk'] == '1'){
				$sql.="imagename2='', imagename2_org='', ";
				}
				
				if($req['imagename2']){
				$sql.="imagename2='".$req['imagename2']."', imagename2_org='".$req['imagename2_org']."',";
				}
				
				$sql.="image2_alt = '".$req['image2_alt']."',";

				if($req['imagename3_chk'] == '1'){
				$sql.="imagename3='', imagename3_org='', ";
				}
				
				if($req['imagename3']){
				$sql.="imagename3='".$req['imagename3']."', imagename3_org='".$req['imagename3_org']."',";
				}

				$sql.="image3_alt = '".$req['image3_alt']."',";

				if($req['imagename4_chk'] == '1'){
				$sql.="imagename4='', imagename4_org='', ";
				}
				
				if($req['imagename4']){
				$sql.="imagename4='".$req['imagename4']."', imagename4_org='".$req['imagename4_org']."',";
				}

				$sql.="image4_alt = '".$req['image4_alt']."',";

				if($req['imagename5_chk'] == '1'){
				$sql.="imagename5='', imagename5_org='', ";
				}
				
				if($req['imagename5']){
				$sql.="imagename5='".$req['imagename5']."', imagename5_org='".$req['imagename5_org']."',";
				}
				
				$sql.="image5_alt = '".$req['image5_alt']."',";

				if($req['imagename6_chk'] == '1'){
				$sql.="imagename6='', imagename6_org='', ";
				}
				
				if($req['imagename6']){
				$sql.="imagename6='".$req['imagename6']."', imagename6_org='".$req['imagename6_org']."',";
				}
				
				$sql.="image6_alt = '".$req['image6_alt']."',";

				if($req['imagename7_chk'] == '1'){
				$sql.="imagename7='', imagename7_org='', ";
				}
				
				if($req['imagename7']){
				$sql.="imagename7='".$req['imagename7']."', imagename7_org='".$req['imagename7_org']."',";
				}
				
				$sql.="image7_alt = '".$req['image7_alt']."',";

				if($req['imagename8_chk'] == '1'){
				$sql.="imagename8='', imagename8_org='', ";
				}
				
				if($req['imagename8']){
				$sql.="imagename8='".$req['imagename8']."', imagename8_org='".$req['imagename8_org']."',";
				}
				
				$sql.="image8_alt = '".$req['image8_alt']."'";

				
				$sql.="WHERE common_fk = '".$req['common_fk']."'";
				$sql.="and tablename = '".$this->tableName."'";
				

				mysql_query($sql, $conn);
			
			}else{
				$sql="
				INSERT INTO imagefiles (
					common_fk
				,	tablename
				,	listimage
				,	imagename1
				,	imagename1_org
				,	image1_alt
				,	imagename2
				,	imagename2_org
				,	image2_alt
				,	imagename3
				,	imagename3_org
				,	image3_alt
				,	imagename4
				,	imagename4_org
				,	image4_alt
				,	imagename5
				,	imagename5_org
				,	image5_alt
				,	imagename6
				,	imagename6_org
				,	image6_alt
				,	imagename7
				,	imagename7_org
				,	image7_alt
				,	imagename8
				,	imagename8_org
				,	image8_alt
				,	registerDate
					
				) VALUES (";
				$sql.=chkIsset($req['common_fk']);
				$sql.=",	'".$this->tableName."'";
				$sql.=",	'".$req['listimage']."'";
				$sql.=",	'".$req['imagename1']."'";
				$sql.=",	'".$req['imagename1_org']."'";
				$sql.=",	'".$req['image1_alt']."'";
				$sql.=",	'".$req['imagename2']."'";
				$sql.=",	'".$req['imagename2_org']."'";
				$sql.=",	'".$req['image2_alt']."'";
				$sql.=",	'".$req['imagename3']."'";
				$sql.=",	'".$req['imagename3_org']."'";
				$sql.=",	'".$req['image3_alt']."'";
				$sql.=",	'".$req['imagename4']."'";
				$sql.=",	'".$req['imagename4_org']."'";
				$sql.=",	'".$req['image4_alt']."'";
				$sql.=",	'".$req['imagename5']."'";
				$sql.=",	'".$req['imagename5_org']."'";
				$sql.=",	'".$req['image5_alt']."'";
				$sql.=",	'".$req['imagename6']."'";
				$sql.=",	'".$req['imagename6_org']."'";
				$sql.=",	'".$req['image6_alt']."'";
				$sql.=",	'".$req['imagename7']."'";
				$sql.=",	'".$req['imagename7_org']."'";
				$sql.=",	'".$req['image7_alt']."'";	
				$sql.=",	'".$req['imagename8']."'";
				$sql.=",	'".$req['imagename8_org']."'";
				$sql.=",	'".$req['image8_alt']."'";
				$sql.=",	now()";
				$sql.=")";
				
				//echo $sql;
				mysql_query($sql, $conn);

			}
		}

		/**
		다중파일 업로드
		**/
		if($r > 0 ){
			if($req['filename_chk']){
				foreach($req['filename_chk'] as $key => $val){ 
					$req['uno']=$val;
					$this->deleteUploadFiles($req);
				}
			}
			if ( $req['unos'] ) {
				foreach($req['unos'] as $key => $val){ 
					$req['uno']=$val;
					$this->deleteUploadFiles($req);
				}
			}
			if($req['filenames']){
				foreach($_REQUEST['filenames'] as $key => $val){ 
					$req['filename'] = $_REQUEST['filenames'][$key];
					$req['filename_org'] = $_REQUEST['filenames_org'][$key];
					$req['filesize'] = $_REQUEST['filesizes'][$key];
					$this->insertMultiFile($req);
				}
			}
		}
//		echo $sql;
//		mysql_close($conn);
		return $r;
	}

	// 삭제
	function delete($req) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$data = $this->getFilenames($req['no']);

		$sql = " DELETE FROM ".$this->tableName." WHERE no = ".$req['no'];

		$result = mysql_query($sql, $conn);

		if($result > 0){
			if($data['filename'] != ""){

				unlink($req['uploadPath'].$data['filename']);
			}
			
			if($data['moviename'] != ""){
				unlink($req['uploadPath'].$data['moviename']);
			}
		}
		mysql_close($conn);
		return $result;
	}

	function deleteUploadFiles($req) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$data = $this->getFilenames($req['no']);

		$sql = " DELETE FROM uploadfiles WHERE uno in(".$req['uno'].")";

		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		return $result;
	}


	function insertReply($req="") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$maxOno = $this->getMaxOno($req['gno']);
		$minOno = $this->getMinOno($req['gno'],$req['ono'],$req['nested']);

		if($minOno == 0){
			$req['ono'] = ($maxOno+1);
		}else{
			
			$this->updateOno($req['gno'],$minOno);
			$req['ono'] = $minOno;

		}
		
		$req['nested'] = ($req['nested']+1);

		$sql="INSERT INTO ".$this->tableName." (";
        $sql.="hospital_fk, member_fk, clinic_fk, category_fk, secret, password, name, email, tel, cell, title, contents, answer,";
        $sql.="relation_url, filename, filename_org, filesize, moviename, moviename_org, reserdate, resertime, state, registdate,";
        if($req['readno']){
		$sql.="readno,";
		}

        $sql.="manage_fk, top, main, newicon, iscall, ismail, gno, ono, nested";
        $sql.=") VALUES (";
        $sql.=chkIsset($req['hospital_fk']).", ".chkIsset($req['member_fk']).", ".chkIsset($req['clinic_fk']).", ".chkIsset($req['category_fk'])." , ".chkIsset($req['secret']).",";
		
		$sql.="(select password from (select ".$this->tableName.".password from ".$this->tableName." where ".$this->tableName.".no = ".chkIsset($req['no']).") tb),";

		$sql.="'".$req['name']."','". $req['email']."','". $req['tel']."','". $req['cell']."','". $req['title']."','". $req['contents']."','". $req['answer']."',";
        $sql.="'".$req['relation_url']."','". $req['filename']."','". $req['filename_org']."',".chkIsset($req['filesize']).",'".$req['moviename']."','". $req['moviename_org']."','". $req['reserdate']."','". $req['resertime']."',".chkIsset($req['state']).",";
	    if($req['registdateGubun'] == '0'){
		$sql.="'".$req['registdate']."',";	
		}else{
		$sql.="NOW(),";
		}
		if($req['readno']){
		$sql.=chkIsset($req['readno']).", ";
		}
       	$sql.=chkIsset($req['manage_fk']).",".chkIsset($req['top']).",".chkIsset($req['main']).",".chkIsset($req['newicon']).",".chkIsset($req['iscall']).",".chkIsset($req['ismail']).",".chkIsset($req['gno']).", ".chkIsset($req['ono']).", ".chkIsset($req['nested']);
        $sql.=")";

		//echo $sql;

		mysql_query($sql, $conn);


		$sql = "SELECT LAST_INSERT_ID() AS lastNo";
		$result = mysql_query($sql, $conn);


		$row = mysql_fetch_array($result);
		$lastNo = $row['lastNo'];
		$req['no'] = $lastNo;


		
//		if( !$req['answer_depth']) $answer_depth = $req[no];
//		else $answer_depth = $req['answer_depth'].$req[no];
//		$sqlDepth = "update $this->tableName set ";
//		$sqlDepth .= " answer_depth = '".$answer_depth."@' ";
//		$sqlDepth .= " where no = ".$req[no];
//		mysql_query($sqlDepth, $conn);

		/***
			동적 필드
		***/
		if($lastNo > 0 && ( $req[imagename1] | $req[imagename2] | $req[imagename3] | $req[imagename4] | $req[imagename5] | $req[imagename6]| $req[imagename7] | $req[imagename8] | $req[imagename9])){
			$this->insertSubRows($req);
		}

		/**
		다중파일 업로드
		**/
		if($lastNo > 0 && $req['filenames']){
			foreach($_REQUEST['filenames'] as $key => $val){ 
				$req['filename'] = $_REQUEST['filenames'][$key];
				$req['filename_org'] = $_REQUEST['filenames_org'][$key];
				$req['filesize'] = $_REQUEST['filesizes'][$key];
				$this->insertMultiFile($req);
			}
		}


		mysql_close($conn);
		return $lastNo;
	}

	// 목록
	function getData($param=0, $userCon) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		$whereSql = $this->getWhereSql($param);	// where절

		// 조회수 증가
		if ($userCon) {
			mysql_query("UPDATE ".$this->tableName." SET readno=readno+1 WHERE no=".$param[no], $conn);
		}
		
		$sql.="select t.no";
		$sql.=",t.hospital_fk";
		$sql.=",t.member_fk";
		$sql.=",t.clinic_fk";
		$sql.=",t.category_fk";
		$sql.=",t.state";
		$sql.=",t.secret";
		$sql.=",t.password";
		$sql.=",t.name";
		$sql.=",t.email";
		$sql.=",t.tel";
		$sql.=",t.cell";
		$sql.=",t.title";
		$sql.=",t.contents";
		$sql.=",t.answer";
		$sql.=",t.relation_url";
		$sql.=",t.moviename";
		$sql.=",t.moviename_org";
		$sql.=",t.registdate";
		$sql.=",t.readno";
		$sql.=",t.top";
		$sql.=",t.main";
		$sql.=",t.newicon";
		$sql.=",t.connectid";
		$sql.=",t.refname";
		$sql.=",t.refhost";
		$sql.=",t.refpage";
		$sql.=",t.refparam";
		$sql.=",t.campaigntext";
		$sql.=",t.refsearch";
		$sql.=",t.ip";
		$sql.=",t.sessionid";
		$sql.=",t.iscall";
		$sql.=",t.ismail";
		$sql.=",t.mobile";
		$sql.=",t.gno";
		$sql.=",t.ono";
		$sql.=",t.nested";
		$sql.=",t.admin_fk";
		$sql.=",t.reserdate";
		$sql.=",t.resertime";
		$sql.=",t.manage_fk";
		if ( $this->tableName != 'faq' ) {
			$sql.=",t.zipcode";
			$sql.=",t.addr0";
		}
		if ( $this->tableName == 'product' ) {
			$sql.=",t.category";
		}
		$sql.=",GROUP_CONCAT(u.uno order by uno) as unos";
		$sql.=",GROUP_CONCAT(u.filename order by uno) as filename";
		$sql.=",GROUP_CONCAT(u.filename_org order by uno) as filename_org";
		$sql.=",GROUP_CONCAT(u.filesize order by uno) as filesize ,";
		if($param['category_tablename']){
        $sql.="(select name from ".$param['category_tablename']." where ".$param['category_tablename'].".no = t.category_fk) as category_name,";
		}
        
        $sql.="(select name from hospital where hospital.no = t.hospital_fk) as hospital_name,";

        $sql.="(select name from clinic where clinic.no = t.clinic_fk) as clinic_name ";
        $sql.=",imagefiles.*"; // 2018-02-06 추가
        $sql.="from ".$this->tableName." t ";
        $sql.="left outer join imagefiles ";
        $sql.="on t.no = imagefiles.common_fk ";
        $sql.="and imagefiles.tablename = '".$this->tableName."' ";
        $sql.="left outer join uploadfiles u ";
        $sql.="on t.no = u.common_fk ";
        $sql.="and u.tablename = '".$this->tableName."' ";
		$sql.="WHERE t.no = ".$param[no];
		//echo $sql;
		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		$data = mysql_fetch_assoc($result);
		if($data[no] >0){
			$param[rownum] = $this->getRowNum($param);
			$obj = $this->getNextPrevData($param, 1);
			$data[prev_no] = $obj[no];
			$data[prev_title] = $obj[title];
			$obj = $this->getNextPrevData($param, -1);
			$data[next_no] = $obj[no];
			$data[next_title] = $obj[title];
		}
		
		/**
		다중 파일 리스트
		**/
		$attachParam = array(
			'common_fk' => $data['no']
		);
//		$data['fileList'] = $this->attachfile->fileList($attachParam);
		/**
		다중 파일 리스트
		**/

		return $data;
	}
	function getData_bak($param=0, $userCon) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		$whereSql = $this->getWhereSql($param);	// where절

		// 조회수 증가
		if ($userCon) {
			mysql_query("UPDATE ".$this->tableName." SET readno=readno+1 WHERE no=".$param[no], $conn);
		}
		
		$sql="select *,";

		if($param['category_tablename']){
        $sql.="(select name from ".$param['category_tablename']." where ".$param['category_tablename'].".no = ".$this->tableName.".category_fk) as category_name,";
		}
        
        $sql.="(select name from hospital where hospital.no = ".$this->tableName.".hospital_fk) as hospital_name,";

        $sql.="(select name from clinic where clinic.no = ".$this->tableName.".clinic_fk) as clinic_name ";
        $sql.="from ".$this->tableName." ";
        $sql.="left outer join imagefiles ";
        $sql.="on ".$this->tableName.".no = imagefiles.common_fk ";
        $sql.="and imagefiles.tablename = '".$this->tableName."' ";
		$sql.="WHERE ".$this->tableName.".no = ".$param[no];
		
		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		$data = mysql_fetch_assoc($result);
		if($data[no] >0){
			$param[rownum] = $this->getRowNum($param);
			$obj = $this->getNextPrevData($param, 1);
			$data[prev_no] = $obj[no];
			$data[prev_title] = $obj[title];
			$obj = $this->getNextPrevData($param, -1);
			$data[next_no] = $obj[no];
			$data[next_title] = $obj[title];
		}

		return $data;
	}


	function getFiles($param='') {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$sql = " SELECT * FROM uploadfiles where common_fk = ".$param['no'];
//		echo $sql;
		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		return $result;
	}

	// gno 최대값+1
	function getMaxGno() {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$sql = " SELECT IFNULL(MAX(gno), 0)+1 AS maxgno FROM ".$this->tableName;

		$result = mysql_query($sql, $conn);
		//mysql_close($conn);

		$row=mysql_fetch_array($result);
		$maxgno = $row['maxgno'];

		return $maxgno;
	}

	// ono 최대값
	function getMaxOno($gno) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$sql = " SELECT IFNULL(MAX(ono), 0) AS maxono FROM ".$this->tableName." WHERE gno=".$gno;

		$result = mysql_query($sql, $conn);
		//mysql_close($conn);

		$row=mysql_fetch_array($result);
		$maxono = $row['maxono'];

		return $maxono;
	}

	// ono 최소값
	function getMinOno($gno=0, $ono=0, $nested=0) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$sql = " SELECT IFNULL(MIN(ono), 0) AS minono FROM ".$this->tableName." WHERE gno=".$gno." AND ono > ".$ono." AND nested <= ".$nested;

		$result = mysql_query($sql, $conn);
		//mysql_close($conn);

		$row=mysql_fetch_array($result);
		$minono = $row['minono'];

		return $minono;
	}

	// ono가 0이 아닌경우 같은 그룹내 minOno보다 크거나 같은 ono +1
	function updateOno($gno=0, $minOno=0) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = "UPDATE ".$this->tableName." SET ono = ono+1 WHERE gno = $gno AND ono >= $minOno";

		$result = mysql_query($sql, $conn);
		//mysql_close($conn);
		return $result;
	}

	// 파일명 가져오기
	function getFilenames($no=0) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$sql="SELECT 	filename
				, 		moviename
				,		imagename1
				,		imagename2
				,		imagename3
				,		imagename4
				,		imagename5
				,		imagename6
				,		imagename7
				,		imagename8
        FROM ".$this->tableName;
        $sql.=" left outer join imagefiles ";
        $sql.=" on ".$this->tableName.".no = imagefiles.common_fk";
        $sql.=" and imagefiles.tablename = '".$this->tableName."'";
        $sql.=" where ".$this->tableName.".no = ".$no;
		//echo $sql;
		$result = mysql_query($sql, $conn);

		$data=mysql_fetch_array($result);
		//mysql_close($conn);

		return $data;
	}

	// rownum 구하기
	function getRowNum($req='') {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$whereSql = $this->getWhereSql($req);	// where절

		$sql = "
			SELECT rownum FROM (
				SELECT @rownum:=@rownum+1 AS rownum, no, title FROM ".$this->tableName.", (SELECT @rownum:=0) AS r
				".$whereSql."
				".$this->getOrderby($param)."
			) AS r2
			WHERE r2.no = ".$req['no'];
		
		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		$result=mysql_fetch_array($result);

		return $result[rownum];
	}

	/*** 이전/다음글 가져오기
			auth oy
	****/
	function getNextPrevData($req='', $diff=1) {
		if(!$diff)
			return null;
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		$whereSql = $this->getWhereSql($req);	// where절

		$sql = "
			SELECT
				ifnull(rownum,0), ifnull(no,0) AS no, title AS title
			FROM (
				SELECT @rownum:=@rownum+1 AS rownum, no, title from ".$this->tableName.", (SELECT @rownum:=0) AS r
				".$whereSql."
				".$this->getOrderby($param)."
			) AS r2
			WHERE r2.rownum = $req[rownum]+('".$diff."')";
//		echo $sql;
		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		$result=mysql_fetch_array($result);
		return $result;
	}









	// 다음글 가져오기
	function getNextRowNum($req='', $rownum=0) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$whereSql = $this->getWhereSql($req);	// where절

		$sql = "
			SELECT
				ifnull(rownum,0), ifnull(no,0) AS next_no, title AS next_title
			FROM (
				SELECT @rownum:=@rownum+1 AS rownum, no, title from ".$this->tableName.", (SELECT @rownum:=0) AS r
				".$whereSql."
				ORDER BY top DESC, gno DESC, ono ASC
			) AS r2
			WHERE r2.rownum = $req[rownum]+1";
		
		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		return $result;
	}

	// 이전글 가져오기
	function getPrevRowNum($req='', $rownum=0) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$whereSql = $this->getWhereSql($req);	// where절

		$sql = "
			SELECT
				ifnull(rownum,0), ifnull(no,0) AS next_no, title AS next_title
			FROM (
				SELECT @rownum:=@rownum+1 AS rownum, no, title from ".$this->tableName.", (SELECT @rownum:=0) AS r
				".$whereSql."
				ORDER BY top DESC, gno DESC, ono ASC
			) AS r2
			WHERE r2.rownum = $req[rownum]-1";
		
		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		return $result;
	}

	// 조회수 +1
	function updateReadno($no=0) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = "UPDATE ".$this->tableName." SET readno = readno+1 WHERE no = $no";

		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		return $result;
	}

	// 메인목록 조회
	function getMainList($number) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$whereSql = $this->getWhereSql($param);	// where절

		$sql = "
			SELECT *,
				(SELECT name FROM hospital AS h WHERE h.no=".$this->tableName.".hospital_fk) AS hospital_name,
				(SELECT name FROM clinic AS c WHERE c.no = ".$this->tableName.".clinic_fk) AS clinic_name
			FROM ".$this->tableName."
			ORDER BY main DESC, registdate DESC 
			LIMIT 0, ".$number." ";

		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		return $result;
	}

	// 비밀번호확인
	function checkPassword($req='') {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = "
			SELECT 
				count(*) as cnt
			FROM ".$this->tableName."
			WHERE no=".$req['no']." and password=".DB_ENCRYPTION."('".$req['cur_password']."')";
		//echo $sql;
		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		$data = mysql_fetch_assoc($result);
		$cnt = $data[cnt];

		return $cnt;
	}

	// 카테고리 전체로우수, 페이지카운트
	function getCategoryCount($param = "") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$whereSql = $this->getWhereSql($param);	// where절
		$sql = " SELECT COUNT(*) AS cnt FROM ".$this->category_tablename.$whereSql;

		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		$row=mysql_fetch_array($result);
		$totalCount = $row['cnt'];
		$pageCount = getPageCount($this->pageRows, $totalCount);

		$data[0] = $totalCount;
		$data[1] = $pageCount;

		return $data;
	}

	// 카테고리목록
	function getCategoryList($param='') {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$whereSql = $this->getWhereSql($param);	// where절

		$sql = "
			SELECT * FROM ".$this->category_tablename." order by registdate desc";

		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		return $result;
	}

	// 카테고리 등록
	function insertCategory($req="") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = "
			INSERT INTO ".$this->category_tablename." (
				name 
			) VALUES (
				'$req[name]'
			)";

		mysql_query($sql, $conn);

		$sql = "SELECT LAST_INSERT_ID() AS lastNo";
		$result = mysql_query($sql, $conn);
		$row = mysql_fetch_array($result);
		$lastNo = $row['lastNo'];
		mysql_close($conn);
		return $lastNo;
	}

	// 카테고리상세
	function getCategoryData($no=0) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = "
			SELECT no, name, registdate FROM ".$this->category_tablename." WHERE no = ".$no;
		
		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		$data = mysql_fetch_assoc($result);

		return $data;
	}

	// 삭제
	function deleteCategory($no=0) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = " DELETE FROM ".$this->category_tablename." WHERE no = ".$no;

		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		return $result;
	}

	// 카테고리수정
	function updateCategory($req="") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = "
			UPDATE ".$this->category_tablename." SET name = '$req[name]' WHERE no = ".$req['no'];

		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		return $result;
	}

	function insertSubRows($param){
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		$sql = '';
		$sql .= "insert into imagefiles ";
		$sql .= "(";

		$sql .= "common_fk";
		$sql .= ", tablename";
		$sql .= ",	listimage";
		$sql .= ",	imagename1";
		$sql .= ",	imagename1_org";
		$sql .= ",	image1_alt";
		$sql .= ",	imagename2";
		$sql .= ",	imagename2_org";
		$sql .= ",	image2_alt";
		$sql .= ",	imagename3";
		$sql .= ",	imagename3_org";
		$sql .= ",	image3_alt";
		$sql .= ",	imagename4";
		$sql .= ",	imagename4_org";
		$sql .= ",	image4_alt";
		$sql .= ",	imagename5";
		$sql .= ",	imagename5_org";
		$sql .= ",	image5_alt";
		$sql .= ",	imagename6";
		$sql .= ",	imagename6_org";
		$sql .= ",	image6_alt";
		$sql .= ",	imagename7";
		$sql .= ",	imagename7_org";
		$sql .= ",	image7_alt";
		$sql .= ",	imagename8";
		$sql .= ",	imagename8_org";
		$sql .= ",	image8_alt";
		$sql .= ",	registerDate";



		$sql .= ")";
		$sql .= "values";
		$sql .= "(";
		$sql .= "	$param[no]";
		$sql .= ", '".$this->tableName."' ";
		$sql .= ",	'$param[listimage]'";
		$sql .= ",	'$param[imagename1]'";
		$sql .= ",	'$param[imagename1_org]'";
		$sql .= ",	'$param[image1_alt]'";
		$sql .= ",	'$param[imagename2]'";
		$sql .= ",	'$param[imagename2_org]'";
		$sql .= ",	'$param[image2_alt]'";
		$sql .= ",	'$param[imagename3]'";
		$sql .= ",	'$param[imagename3_org]'";
		$sql .= ",	'$param[image3_alt]'";
		$sql .= ",	'$param[imagename4]'";
		$sql .= ",	'$param[imagename4_org]'";
		$sql .= ",	'$param[image4_alt]'";
		$sql .= ",	'$param[imagename5]'";
		$sql .= ",	'$param[imagename5_org]'";
		$sql .= ",	'$param[image5_alt]'";
		$sql .= ",	'$param[imagename6]'";
		$sql .= ",	'$param[imagename6_org]'";
		$sql .= ",	'$param[image6_alt]'";
		$sql .= ",	'$param[imagename7]'";
		$sql .= ",	'$param[imagename7_org]'";
		$sql .= ",	'$param[image7_alt]'";
		$sql .= ",	'$param[imagename8]'";
		$sql .= ",	'$param[imagename8_org]'";
		$sql .= ",	'$param[image8_alt]'";
		$sql .= ",	now()";
		$sql .= ")";
		mysql_query($sql, $conn);

		return $lastNo;


	}

	function insertMultiFile($param){
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		$sql = '';
		$sql .= "insert into uploadfiles ";
		$sql .= "(";

		$sql .= "common_fk";
		$sql .= ", tablename";
		$sql .= ",	filename";
		$sql .= ",	filename_org";
		$sql .= ",	filesize";
		$sql .= ",	registerDate";
		$sql .= ")";
		$sql .= "values";
		$sql .= "(";
		$sql .= "	$param[no]";
		$sql .= ", '".$this->tableName."' ";
		$sql .= ",	'$param[filename]'";
		$sql .= ",	'$param[filename_org]'";
		$sql .= ",	'$param[filesize]'";
		$sql .= ",	now()";
		$sql .= ")";
		mysql_query($sql, $conn);

		return $lastNo;
	}


	function insertDragMultiFile($param){
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		$sql = '';
		$sql .= "insert into uploadfiles ";
		$sql .= "(";

		$sql .= "common_fk";
		$sql .= ", tablename";
		$sql .= ",	filename";
		$sql .= ",	filename_org";
		$sql .= ",	filesize";
		$sql .= ",	registerDate";
		$sql .= ",  userInfo";
		$sql .= ")";
		$sql .= "values";
		$sql .= "(";
		$sql .= "	0";
		$sql .= ",  '$param[board]' ";
		$sql .= ",	'$param[filename]'";
		$sql .= ",	'$param[filename_org]'";
		$sql .= ",	'$param[filesize]'";
		$sql .= ",	 now()";
		$sql .= ",  '$param[session]'";
		$sql .= ")";
		mysql_query($sql, $conn);

		return $lastNo;
	}


	// 업로드파일 삭제
	function deleteFileList($req) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = " DELETE FROM uploadfiles WHERE userInfo = '".$req['session']."'";

		$result = mysql_query($sql, $conn);

		mysql_close($conn);
		return $result;
	}
	// 업로드파일 삭제
	function deleteFilename($req) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = " DELETE FROM uploadfiles WHERE filename = '".$req['filename']."'";

		$result = mysql_query($sql, $conn);

		mysql_close($conn);
		return $result;
	}
	
	// 업로드파일 리스트
	function uploadFileList($req) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = "SELECT * FROM uploadfiles WHERE userInfo = '".$req['session']."'";

		$result = mysql_query($sql, $conn);

		mysql_close($conn);
		return $result;
	}
	
	// 드래그 드롭한 파일 fk 업데이트
	function updateUploadFiles($req) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = "UPDATE uploadfiles SET common_fk = ".$req['common_fk'].", userInfo = null WHERE userInfo = '".$req['session']."'";

		$result = mysql_query($sql, $conn);

		mysql_close($conn);
		return $result;
	}

}


?>