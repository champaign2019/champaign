<?
class xssFilter {
	
	var $useIgnore =  array(
							"contents", 
							"answer", 
							"url",
							"param",
							"privacy_text",
							"privacy_mini_text", 
							"join_text",
							"uploadPath"
						);	
	
	//스크립트 공격 관련 태그 replace
	function cleanXSS($value) { 
		$value = str_replace("<","&lt;",$value);
		$value = str_replace(">","&gt;",$value);
		$value = str_replace("'","&#39;",$value);
		$value = str_replace("\"","&quot;",$value);
		$value = str_replace("eval\\((.*)\\)","",$value);
		$value = str_replace("[\\\"\\\'][\\s]*javascript:(.*)[\\\"\\\']","\"\"",$value);
		$value = str_replace("script","",$value);

		return $value;

	}
	
	function checkParameter($_r){
		//request key 값 :: form 태그에 담겨온 name 값들
		$param_arr = array_keys($_r);
		
		for($i = 0; $i < count($param_arr); $i++){		
			$paramName = $param_arr[$i];
			$isIgnore  = false;
			
			for($j = 0; $j < count($this->useIgnore); $j++){
				// name 값이 xss 필터가 필요 없을 경우 
				if($this->useIgnore[$j] == $paramName){
					$isIgnore = true;
				}			
			}

			if(!$isIgnore){
				// xss 필터 처리
				$_r[$paramName] = $this->cleanXSS($_r[$paramName]); 
			}
		}
		//$_REQUEST 리턴
		return $_r;
	}
}
?>
