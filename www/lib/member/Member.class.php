<? session_start(); 
/*


*/

include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/db/DBConnection.class.php";

class Member {

	public static function setMemberSession($session = '') {
		if ( is_null($GLOBALS['memberSession']) ) {
			if ( $session['member_no'] ) {
				$memberSession = $session;
				$memberSessionKey = array_keys($memberSession);

				$memberSessionArray = array();
				for ($i=0; $i<count($memberSession); $i++) {
					if ( strpos($memberSessionKey[$i], 'member_') !== false ) {
//						echo $memberSessionKey[$i].' : '.$session[$memberSessionKey[$i]].'<br/>';
						$memberSessionArray[$memberSessionKey[$i]] = $session[$memberSessionKey[$i]];
					}
				}
				$GLOBALS['memberSession'] = $memberSessionArray;
			}
		}
	}

	public static function getMemberSession() {
		return $GLOBALS['memberSession'];
	}
	
	public static function loginCheck() {
		$loginCheck = false;
		if ( is_null($GLOBALS['memberSession']) ) {
			$loginCheck = true;
		}
		return $loginCheck;
	}

	public static function memberSessionInvalidate() {
		$GLOBALS['memberSession'] = null;
	}

	// 검색 파라미터 (초기 개발시 검색조건 세팅필요)
	var $param = array (
					"stype",
					"sval",
					"sgender",
					"ssecession",
					"sissms",
					"sisemail",
					"sstartdate",
					"senddate",
					"sdatetype",
					"sendtype",
					"shospital_fk",
					"sdormantdays",
					"sdormantstate",
					"sdormantemail",
					"sdormanttype"
				);

	var $tableName;			// 테이블명
	var $pageRows;			// 페이지 로우수
	var $startPageNo=0;		// limit 시작페이지
	public $reqPageNo=1;	// 요청페이지
	var $conn;

	// 생성자
	function Member($pageRows=0, $tableName='', $request='') {
		$this->pageRows = $pageRows;
		$this->tableName = $tableName;
		$this->reqPageNo = ($request['reqPageNo'] == 0) ? 1 : $request['reqPageNo'];	// 요청페이지값 없을시 1로 세팅
		if ($request['reqPageNo'] > 0) {
			$this->startPageNo = ($request['reqPageNo']-1) * $this->pageRows;
		}
		$this->param['sissms'] = -1;
		$this->param['sisemail'] = -1;
		$this->param['sdormanttype'] = 0;
		$this->param['sdormantstate'] = -1;
		$this->param['sdormantdays'] = -1;
		$this->param['sdormantday'] = 0;
		$this->param['sdormantemail'] = -1;

		if( !getSpamCheck($request) ) $_REQUEST["cmd"] = "error";		// /include/headHtml.jsp 에서 스팸확인
	}

	// 검색 파라미터 queryString 생성
	function getQueryString($page="", $no=0, $request='') {	
		$str = '';
		
		for ($i=0; $i<count($this->param); $i++) {
			if ($request[$this->param[$i]] ) {
				$str = $str.$this->param[$i]."=".urlencode($request[$this->param[$i]])."&";
			}
		}

		if ($no > 0) $str = $str."no=".$no;			// no값이 있을 경우에만 파라미터 세팅 (페이지 이동시 no필요 없음)

		$return = '';
		if ($str) {
			$return = $page.'?'.$str;
		} else {
			$return = $page;
		}

		return $return;
	}

	// 검색 파라미터 queryString 생성, 추가파라미터 지정
	function getQueryStringAddParam($page="", $no=0, $request='', $paramName='', $paramValue='') {	
		$str = '';
		
		for ($i=0; $i<count($this->param); $i++) {
			if ($request[$this->param[$i]]) {
				$str = $str.$this->param[$i]."=".urlencode($request[$this->param[$i]])."&";
			}
		}

		if ($no > 0) $str = $str."no=".$no;			// no값이 있을 경우에만 파라미터 세팅 (페이지 이동시 no필요 없음)

		if ($paramValue) $str .= "&".$paramName."=".urlencode($paramValue);

		$return = '';
		if ($str) {
			$return = $page.'?'.$str;
		} else {
			$return = $page;
		}

		return $return;
	}

	// sql WHERE절 생성
	function getWhereSql($p) {
		$whereSql = " WHERE 1=1 ";
		if ($p['sval']) {
			if ($p['stype'] == 'all') {
				$whereSql = $whereSql." AND ( (name like '%".$p['sval']."%' ) OR (id like '%".$p['sval']."%' ) OR (tel like '%".$p['sval']."%' ) OR (cell like '%".$p['sval']."%' ) OR (email like '%".$p['sval']."%' ) OR (addr0 like '%".$p['sval']."%' ) OR (addr1 like '%".$p['sval']."%' ) ) ";
			} else if ($p['stype'] == 'address') {
				$whereSql .= " AND ( (addr0 LIKE '%".$p['sval']."%') OR (addr1 LIKE '%".$p['sval']."%') )";
			} else {
				$whereSql = $whereSql." AND (".$p['stype']." LIKE '%".$p['sval']."%' )";
			}
		}
		if ($p['sdormanttype'] == 1) {
			$whereSql .= " AND dormant_state=0 ";
		}
		if ($p['sdormantemail'] != null && $p['sdormantemail'] != -1) {
			$whereSql .= " AND dormant_email=".$p['sdormantemail'];
		}
		if ($p['sdormanttype'] != 0) {
			if ($p['sdormantdays'] && $p['sdormantdays'] != -1) {
				$whereSql .= " AND ( SELECT DATEDIFF(now(),logindate) >= ".$p['sdormantdays']." ) ";
			}
		}
		if ($p['ssecession'] > -1) {
			$whereSql .= " AND secession=".$p['ssecession'];
		}
		if ($p['sgender'] > -1) {
			$whereSql .= " AND gender=".$p['sgender'];
		}
		if ($p['ssecession'] > -1) {
			$whereSql .= " AND secession=".$p['ssecession'];
		}
		if ($p['sissms'] > -1) {
			$whereSql .= " AND issms=".$p['sissms'];
		}
		if ($p['sisemail'] > -1) {
			$whereSql .= " AND ismail=".$p['sisemail'];
		}
		if ($p['sstartdate']) {
			if ($p['senddate']) {
				if ($p['sdatetype'] == 'registdate') {
					$whereSql .= " AND registdate BETWEEN '".$p['sstartdate']." 00:00:00' AND '".$p['senddate']." 23:59:59' ";
				}
			}
		}
		if ($p['sendtype'] == 'email') {
			$whereSql .= " AND (email REGEXP '^[a-zA-Z0-9._-]+@[a-zA-Z0-9]+[a-zA-Z0-9\\-]+[a-zA-Z0-9]+\\.[a-zA-Z]+$') ";
		}
		if ($p['sendtype'] == 'cell') {
			$whereSql .= " AND (cell REGEXP '^01([016789]{1})+(-[0-9]{3,4})+(-[0-9]{4})$' OR CELL REGEXP '^01([016789]{1})+([0-9]{3,4})+([0-9]{4})$') ";
		}
		if ($p['shospital_fk'] > -1) {
			$whereSql .= " AND hospital_fk = ".$p['shospital_fk'];
		}
		return $whereSql;
	}

	// sql 정렬절 생성
	function getOrderbySql($p) {
		$orderbySql = " ORDER BY ";
		if (!$p['orderby']) {
			$orderbySql .= " no DESC ";
		} else {
			$orderbySql .= $p['orderby']." ".$p['ordertype'];
		}
		return $orderbySql;
	}


	// 전체로우수, 페이지카운트
	function getCount($param = "") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$whereSql = $this->getWhereSql($param);	// where절
		$sql = " SELECT COUNT(*) AS cnt FROM ".$this->tableName.$whereSql;
//echo $sql;
		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		$row=mysql_fetch_array($result);
		$totalCount = $row['cnt'];
		$pageCount = getPageCount($this->pageRows, $totalCount);

		$data[0] = $totalCount;
		$data[1] = $pageCount;

		return $data;
	}

	// 목록
	function getList($param='') {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$whereSql = $this->getWhereSql($param);	// where절
		$orderbySql = $this->getOrderbySql($param);	// where절

		$sql = "
			SELECT *,
				(SELECT name FROM hospital AS h WHERE h.no = ".$this->tableName.".hospital_fk) AS hospital_name";
	if($param['sdormanttype'] != 0) {
		$sql .=", ( SELECT DATEDIFF(now(),logindate) ) AS dormant_days";
	}
		$sql .=" FROM ".$this->tableName."
			".$whereSql."
			".$orderbySql."
			LIMIT ".$this->startPageNo.", ".$this->pageRows." ";
		
		$result = mysql_query($sql, $conn);
		mysql_close($conn);
//		echo $sql;

		return $result;
	}

	//휴면계정 리스트
	function dormantlist($param='') {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$whereSql = $this->getWhereSql($param);	// where절
		$orderbySql = $this->getOrderbySql($param);	// where절

		$sql = "
			SELECT *,
				(SELECT name FROM hospital AS h WHERE h.no = dormant.hospital_fk) AS hospital_name";
	if($param['sdormanttype'] != 0) {
		$sql .=", ( SELECT DATEDIFF(now(),logindate) ) AS dormant_days";
	}
		$sql .=" FROM dormant 
			".$whereSql."
			".$orderbySql."
			LIMIT ".$this->startPageNo.", ".$this->pageRows." ";
		
		$result = mysql_query($sql, $conn);
		mysql_close($conn);
//		echo $sql;

		return $result;
	}

	// 관리자 등록
	function insert($req="") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = "
			INSERT INTO ".$this->tableName."
				(hospital_fk, id, password, name, gender, tel, cell, email,
				zipcode, addr0, addr1, secession, issms, ismail, birthday, birthtype, registdate, logindate)
			VALUES
				(".chkIsset($req['hospital_fk']).", '$req[id]', ".DB_ENCRYPTION."('$req[password]'), '$req[name]', ".chkIsset($req[gender]).", '$req[tel]', '$req[cell]', '$req[email]',
				'$req[zipcode]', '$req[addr0]', '$req[addr1]', '$req[secession]', ".chkIsset($req[issms]).", ".chkIsset($req[ismail]).", '$req[birthday]', '$req[birthtype]', NOW(), NOW())";
		mysql_query($sql, $conn);

		$sql = "SELECT LAST_INSERT_ID() AS lastNo";
		$result = mysql_query($sql, $conn);
		$row = mysql_fetch_array($result);
		$lastNo = $row['lastNo'];
		mysql_close($conn);
		return $lastNo;
	}

	// 수정
	function update($req="") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = "
			UPDATE ".$this->tableName." SET ";
		if ($req['password']) {
			$sql .= "password=".DB_ENCRYPTION."('".$req['password']."'), ";
		}
		$sql .= " hospital_fk=".chkIsset($req['hospital_fk']).", name='$req[name]', tel='$req[tel]', cell='$req[cell]', email='$req[email]', secession='$req[secession]', gender = ".chkIsset($req[gender]).",
				zipcode='$req[zipcode]', addr0='$req[addr0]', addr1='$req[addr1]', issms=".chkIsset($req[issms]).", ismail=".chkIsset($req[ismail]).", birthday='$req[birthday]', birthtype='$req[birthtype]'			
				WHERE no = ".$req['no'];

		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		return $result;
	}

	// 임시 비밀번호로 변경
	function updateTempPass($no, $temppass) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = "
			UPDATE ".$this->tableName." SET password=".DB_ENCRYPTION."('".$temppass."') WHERE no = ".$no;

		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		return $result;
	}

	// 탈퇴처리
	function updateSecession($no=0) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = "
			UPDATE member SET secession=1 WHERE no = ".$no;

		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		return $result;
	}

	// 삭제
	function delete($no=0) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = " DELETE FROM ".$this->tableName." WHERE no = ".$no;

		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		return $result;
	}

	// 목록
	function getData($req='') {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$whereSql = $this->getWhereSql($param);	// where절

		$sql = "
			SELECT 
				*, (SELECT name FROM hospital AS h WHERE h.no = ".$this->tableName.".hospital_fk) AS hospital_name";
	if($req['sdormanttype'] != 0) {
		$sql .=" , ( SELECT DATEDIFF(now(),logindate) ) AS dormant_days
				 , ( SELECT DATE_ADD(logindate,INTERVAL 365 DAY) ) AS dormant_date";
	}
		$sql .=" FROM ".$this->tableName."
			WHERE no = ".$req['no'];
//echo $sql;
		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		$data = mysql_fetch_assoc($result);

		return $data;
	}

	// 아이디체크
	function checkId($id = "") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$sql = " SELECT COUNT(*) AS cnt FROM member WHERE id= '".$id."' ";

		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		$row=mysql_fetch_array($result);
		$count = $row['cnt'];

		return $count;
	}

	// 아이디 찾기
	function searchId($name='', $email='') {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$sql = "
			SELECT * FROM member
			WHERE name = '".$name."' AND email='".$email."' ";
		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		$data = rstToArray($result);

		return $data;
	}

	// 패스워드 찾기
	function searchPw($name='', $email='', $id='') {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$sql = "
			SELECT * FROM member
			WHERE name = '".$name."' AND email='".$email."' AND id='".$id."' ";
		
		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		$data = mysql_fetch_assoc($result);

		return $data;
	}

	// 회원 로그인체크
	function checkLogin($id = "", $password='') {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$sql = " SELECT IF(secession > 0, 2, count(1) ) AS cnt FROM member WHERE id= '".$id."' AND password=".DB_ENCRYPTION."('".$password."')";
		outlog($sql);

		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		$row=mysql_fetch_array($result);
		$count = $row['cnt'];

		return $count;
	}

	// 로그인후 세션저장을 위해 조회
	function loginMember($id='') {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$sql = "
			SELECT * FROM member
			WHERE id = '".$id."' ";
		
		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		$data = mysql_fetch_assoc($result);

		return $data;
	}

	// 탈퇴자 전체로우수, 페이지카운트
	function getSecedeCount($param = "") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$sql = " SELECT COUNT(*) AS cnt FROM secede WHERE 0=0 ";
		if ($p['sval']) {
			if ($p['stype'] == 'all') {
				$sql .= " AND (name LIKE '%".$p['sval']."%' OR id LIKE '%".$p['sval']."%') ";
			} else {
				$sql .= " AND $p[stype] LIKE '%%' ";
			}
		}

		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		$row=mysql_fetch_array($result);
		$totalCount = $row['cnt'];
		$pageCount = getPageCount($this->pageRows, $totalCount);

		$data[0] = $totalCount;
		$data[1] = $pageCount;

		return $data;
	}

	// 탈퇴자 목록
	function getSecedeList($param='') {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$sql = "
			SELECT * FROM secede WHERE 0=0 ";
		if ($p['sval']) {
			if ($p['stype'] == 'all') {
				$sql .= " AND (name LIKE '%".$p['sval']."%' OR id LIKE '%".$p['sval']."%') ";
			} else {
				$sql .= " AND $p[stype] LIKE '%%' ";
			}
		}
		$sql .= " ORDER BY secededate DESC LIMIT ".$this->startPageNo.", ".$this->pageRows." ";
		
		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		return $result;
	}

	// 발송대상
	function getCountForSend($param = "") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$whereSql = $this->getWhereSql($param);	// where절
		$sql = " SELECT COUNT(".$param['sendtype'].") AS cnt FROM ".$this->tableName.$whereSql;

		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		$row=mysql_fetch_array($result);
		$totalCount = $row['cnt'];
		$pageCount = getPageCount($this->pageRows, $totalCount);

		$data[0] = $totalCount;
		$data[1] = $pageCount;

		return $data;
	}

	// 발송대상자 목록
	function getListForSend($param='') {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$whereSql = $this->getWhereSql($param);	// where절
		$orderbySql = $this->getOrderbySql($param);	// where절

		$sql = "
			SELECT ".$param['sendtype']." FROM member ".$whereSql;
		$sql .= " GROUP BY ".$param['sendtype']." ";
		
		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		return $result;
	}

	// 탈퇴자 등록
	function insertSecede($req="") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = "
			INSERT INTO secede
				(name, registdate, secededate, id)
			VALUES
				('$req[name]', '$req[registdate]', NOW(), '$req[id]')";
		mysql_query($sql, $conn);

		$sql = "SELECT LAST_INSERT_ID() AS lastNo";
		$result = mysql_query($sql, $conn);
		$row = mysql_fetch_array($result);
		$lastNo = $row['lastNo'];
		mysql_close($conn);
		return $lastNo;
	}

	// 삭제
	function deleteSecede($no=0) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = " DELETE FROM secede WHERE no = ".$no;

		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		return $result;
	}

	// 목록
	function selectReserMemberData($param) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$whereSql = $this->getWhereSql($param);	// where절
		$orderbySql = $this->getOrderbySql($param);	// where절

		$sql = "
			SELECT *,
				m.*, h.name AS hospital_name
			FROM member AS m
			LEFT JOIN hospital AS h
			ON m.hospital_fk = h.no 
			WHERE m.name LIKE '%$param%' or m.cell like '%$param%'
			ORDER BY m.name ASC ";
		
		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		return $result;
	}

	//휴면READ
	function dormantRead($req = "") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$sql = "
			SELECT 
				*, (SELECT name FROM hospital AS h WHERE h.no = dormant.hospital_fk) AS hospital_name
				 , ( SELECT DATEDIFF(now(),logindate) ) AS dormant_days
			FROM dormant
			WHERE no = ".$req['no']." ";
		
		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		$data = mysql_fetch_assoc($result);

		return $data;
	}

	//휴면READ (id기준)
	function dormantInfo($id = "") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$sql = "
			SELECT 
				*, (SELECT name FROM hospital AS h WHERE h.no = dormant.hospital_fk) AS hospital_name
				 , ( SELECT DATEDIFF(now(),logindate) ) AS dormant_days
			FROM dormant
			WHERE id = '".$id."' ";
		
		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		$data = mysql_fetch_assoc($result);

		return $data;
	}

	//로그인 시 마지막 로그인 날짜 수정
	function updateLogin($req="") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = "
			UPDATE member SET logindate = NOW() WHERE no = ".$req['no'];
		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		return $result;
	}

	// 관리자페이지에서 휴면상태 변경 
	function changeDormantState($no=0) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = "
			INSERT INTO dormant
				( hospital_fk, id, password, name, gender, tel, cell, email,
				zipcode, addr0, addr1, secession, issms, ismail, birthday, birthtype, registdate, logindate, dormant_email )
			
			SELECT 
				hospital_fk, id, password, name, gender, tel, cell, email,
				zipcode, addr0, addr1, secession, issms, ismail, birthday, birthtype, registdate, logindate, dormant_email
			FROM member WHERE no = ".$no;
		mysql_query($sql, $conn);

		$sql = "SELECT LAST_INSERT_ID() AS lastNo";
		$result = mysql_query($sql, $conn);
		$row = mysql_fetch_array($result);
		$lastNo = $row['lastNo'];
		mysql_close($conn);
		return $lastNo;
	}

	//휴면테이블 -> 멤버테이블로 이동 
	function resetDormantState($req="") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = "
			UPDATE member SET 
			hospital_fk=".chkIsset($req['hospital_fk']).", name='$req[name]', tel='$req[tel]', cell='$req[cell]', email='$req[email]', secession='$req[secession]', gender = ".chkIsset($req[gender]).",
			zipcode='$req[zipcode]', addr0='$req[addr0]', addr1='$req[addr1]', issms=".chkIsset($req[issms]).", ismail=".chkIsset($req[ismail]).", birthday='$req[birthday]', birthtype='$req[birthtype]', logindate='$req[logindate]', dormant_email=".chkIsset($req[dormant_email]).", dormant_state=".chkIsset($req[dormant_state])." WHERE no = ".$req[no];
//echo $sql;
		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		return $result;
	}

	//휴면테이블 -> 멤버테이블로 이동 
	function resetDormantStateId($req="") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = "
			UPDATE member SET 
			hospital_fk=".chkIsset($req['hospital_fk']).", name='$req[name]', tel='$req[tel]', cell='$req[cell]', email='$req[email]', secession='$req[secession]', gender = ".chkIsset($req[gender]).",
			zipcode='$req[zipcode]', addr0='$req[addr0]', addr1='$req[addr1]', issms=".chkIsset($req[issms]).", ismail=".chkIsset($req[ismail]).", birthday='$req[birthday]', birthtype='$req[birthtype]', logindate='$req[logindate]', dormant_email=".chkIsset($req[dormant_email]).", dormant_state=".chkIsset($req[dormant_state])." WHERE id = '".$req[id]."'";

		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		return $result;
	}

	//
	function changeDormantEmail($no=0) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = "
			UPDATE member SET dormant_email = 1 WHERE no = ".$no;
		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		return $result;
	}

	//비밀번호만 변경
	function updatePw($req="") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = "
			UPDATE member SET  password=".DB_ENCRYPTION."('$req[password]') WHERE no = ".$req['no'];

		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		return $result;
	}

	// 로우수, 페이지카운트
	function dormantcount($param = "") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$whereSql = $this->getWhereSql($param);	// where절
		$sql = " SELECT COUNT(*) AS cnt FROM dormant ".$whereSql;

		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		$row=mysql_fetch_array($result);
		$totalCount = $row['cnt'];
		$pageCount = getPageCount($this->pageRows, $totalCount);

		$data[0] = $totalCount;
		$data[1] = $pageCount;

		return $data;
	}

	function deleteDormant($no=0) {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();

		$sql = " DELETE FROM dormant WHERE no = ".$no;

		$result = mysql_query($sql, $conn);
		mysql_close($conn);
		return $result;
	}

	function checkPw($req = "") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$sql = " SELECT COUNT(*) AS cnt FROM member WHERE password=".DB_ENCRYPTION."('$req[nowpassword]') AND no = ".$req['no'];
echo $sql;
		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		$row=mysql_fetch_array($result);
		$count = $row['cnt'];

		return $count;
	}

	function beforePw($req = "") {
		$dbconn = new DBConnection();
		$conn = $dbconn->getConnection();
		
		$sql = " SELECT COUNT(*) AS cnt FROM member WHERE password=".DB_ENCRYPTION."('$req[password]') AND no = ".$req['no'];

		$result = mysql_query($sql, $conn);
		mysql_close($conn);

		$row=mysql_fetch_array($result);
		$count = $row['cnt'];

		return $count;
	}
}


?>