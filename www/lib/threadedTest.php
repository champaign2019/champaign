<?

class Task extends Threaded {
    public $response;

    public function someWork()
    {
        $content = file_get_contents('http://google.com');
        preg_match('~<title>(.+)</title>~', $content, $matches);
        $this->response = $matches[1];
    }
}

class TaskThread extends Thread {
    private $task;

    public function __construct(Threaded $task)
    {
        $this->task = $task;
    }

    public function run()
    {
        $this->task->someWork();
    }
}

$task = new Task;
$taskThread = new TaskThread($task);

$taskThread->start() && $taskThread->join();

var_dump($task->response);

?>