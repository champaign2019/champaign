<? session_start(); 
	/*
	기존 com.vizensoft.util.Function.java
	by withsky 2014.11.18

	*/

	include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";

	// 메시지를 alert하고 rurl로 이동
	function returnURLMsg($rurl = "", $msg = "") {
		$return = 
			"<script>
				alert('".$msg."');
				document.location.href='".$rurl."';
			</script>";
		return $return;
	}

	// rurl로 이동
	function returnURL($rurl = "") {
		$return = 
			"<script>
				document.location.href='".$rurl."';
			</script>";
		return $return;
	}

	// rurl로 이동 후 창을 닫는다.
	function returnURLClose($rurl = "") {
		$return = 
			"<script>
				document.location.href='".$rurl."';
				window.close();
			</script>";
		return $return;
	} 

	// 메시지 alert, rurl로 이동 후 창을 닫는다.
	function returnURLCloseMsg($rurl = "", $msg = "") {
		$return = 
			"<script>
				alert('".$msg."');
				document.location.href='".$rurl."';
				window.close();
			</script>";
		return $return;
	}

	// 메시지 alert 이전페이지로 이동
	function returnHistory($msg = "") {
		$return = 
			"<script>
				alert('".$msg."');
				history.back();
			</script>";
		return $return;
	}

	// 메세지를 alert하고 opener를 reload한 후 창을 닫는다.
	function popupCloseRefresh($msg = "") {
		$return = 
			"<script>
				". ($msg ? "alert('".$msg."');" : "")."
				opener.location.reload();
				window.close();
			</script>";
		return $return;
	}

	// 메시지를 확인하면 rurl로 이동, 취소하면 이전페이지로 이동
	function returnConfirm($rurl="", $msg="") {
		$return = 
			"<script>
				sure=confirm('".$msg."');
				if (sure)
					location.href='".$rurl."';
				else
					history.back();
			</script>";
		return $return;
	}

	// 메시지를 확인하면 rurl로 이동, 취소하면 burl로 이동
	function returnUrlConfirm($rurl="", $burl="", $msg="") {
		$return = 
			"<script>
				sure=confirm('".$msg."');
				if (sure)
					location.href='".$rurl."';
				else
					location.href='".$burl."';
			</script>";
		return $return;
	}

	// 파일사이즈를 계산해서 알맞은 단위로 변경한다.
	function getFileSize($fileSize = 0) {
		$fSize = "";
		if ($fileSize > 1024 && ($fileSize < 1024*1024)) {
			$fSize = round(($fileSize/1024),2)."KB";
		} else if ($fileSize >= 1024*1024) {
			$fSize = round(($fileSize/(1024*1024)),2)."MB";
		} else {
			$fSize = $fileSize."Bytes";
		}
		return $fSize;
	}

	// checkbox 체크여부
	function getChecked($s, $w) {
		$r = "";
		if ($w == '') $w = -1;	// 빈문자열인 경우 0과 같으므로 검색조건 selectbox 문제로 강제로 -1로 변경
		if ($s == $w) $r = "checked";
		return $r;
	}

	function getCheckedi($s, $w) {
		$r = "";
		if ($w == '') $w = -1;	// 빈문자열인 경우 0과 같으므로 검색조건 selectbox 문제로 강제로 -1로 변경
		if ($s == $w) $r = "checked";
		return $r;
	}


	function getCheckedB($s, $w) {
		$r = "";
		$rr = 0;
		if ($s == true) $rr = 1;
		if ($rr == $w) $r = "checked";
		return $r;
	}

	// selectbox 체크여부
	function getSelected($s, $w) {
		$r = "";
		if ($w == '') $w = -1;	// 빈문자열인 경우 0과 같으므로 검색조건 selectbox 문제로 강제로 -1로 변경
		if ($s == $w) $r = "selected";
		return $r;
	}

	// 이미지사이즈 비교
	// 실제 이미지 width값과 비교해서 작을 경우 reWidth값으로 리턴
	function getImgReSize($img = "", $reWidth=0) {
		$r = 0;
		try {
			$info = getimagesize($_SERVER['DOCUMENT_ROOT'].$img);
			$size = substr($info[3], strpos($info[3], "\"")+1);
			$size = substr($size, 0, strpos($size, "\""));
			if ($size > $reWidth) {
				$r = $reWidth;
			} else {
				$r = $size;
			}
		} catch (Exception $e) {
			$r = 0;
		}

		return $r;
	}

	function getImgSizeCheck($img="") {
		$result = "";
		if($img) {
			$imginfo = getimagesize($uploadFullPath.$img);
			$width = $imginfo[0];
			$height = $imginfo[1];
			if ($width > $height) { //너비가 클 경우
//					$result = "width='"+resize+"'";
					$result = "width='100%'";
//					$result = "width='9.2em'";
			} else if ($width < $height){ //높이가 클 경우
//					$result = "height='"+resize+"'";
				$result = "height='100%'";
//					$result = "height=9.2em";
			} else {
				$result = "";
			}
		}
		return $result;
	}

	// jquery ajax로 form submit
	function scriptAjaxSubmit($target="", $formName="", $msg="", $href="") {
		$rHtml = " $.ajax({
					type: \"POST\",
					url: \"".$target."\",
					async:false,
					data:$(\"".$formName."\").serialize(),
					dataType:\"html\",
					success: function(html){
					result = html.trim();
					alert(\"".$msg."\");
					location.href = '".$href."';
					},
					error : function(request, status, error) {
						alert(\"code : \"+request.status+\"message : \"+request.responseText);
					}
					}); ";
		return $rHtml;
	}

	// SSL 사용여부 체크
	function getSslCheckUrl($request_uri="", $page="") {

		$request_uri = substr($_SERVER['REQUEST_URI'], 0, strrpos($_SERVER['REQUEST_URI'], '/'));

		$url = $_SERVER["HTTP_HOST"];
		$urltemp = explode(':', $url);
		$url = "http://".$url;
		if (SSL_USE) $url = "https://".$urltemp[0].SSL_PORT;

		$returnUrl = $url.$request_uri.'/'.$page;

		return $returnUrl;
	}

	// SSL 사용여부 체크
	function getMemberSslCheckUrl($page="") {

		$url = $_SERVER["HTTP_HOST"];
		$urltemp = explode(':', $url);
		$url = "http://".$url;
		if (SSL_USE) $url = "https://".$urltemp[0].SSL_PORT;

		$returnUrl = $url.'/'.$page;

		return $returnUrl;
	}

	// SSL 사용여부 체크 
	// 심볼릭 링크 lib 가 모바일과 데스크탑경로가 같을 경우
	// COMPANY_URL 을 
	function getSslCheckUrl2($request_uri="", $page="") {

		$request_uri = substr($request_uri, 0, strrpos($request_uri, '/'));

		if( strlen(stristr($_SERVER["HTTP_HOST"], "m.ndental") ) ){
	//	if( strlen(stristr($request_uri, "m.ndental") ) ){
			$url = COMPANY_M_URL;
		}else{
			$url = COMPANY_URL;
		}
		if (SSL_USE) $url = COMPANY_SSL_URL;

		$returnUrl = $url.$request_uri.'/'.$page;

		return $returnUrl;
	}

	// 페이지카운트
	function getPageCount($pageRows=0, $totalCount=0) {
		if( $pageRows ==0 )
			$pageRows = 1;
		$pageCount = $totalCount / $pageRows;
		if ($totalCount % $pageRows > 0) $pageCount++;
		return (int)$pageCount;
	}

	// 레퍼러 주소체크
	function checkReferer($referer) {
		$result = true;
		if (CHECK_REFERER) {
			if (!strpos($referer,REFERER_URL)) {
				$result = false;
			}
		}
		return $result;
	}

	// https 주소 제거
	function getRemoviSslUrl($request_uri='', $page='') {

		$request_uri = substr($request_uri, 0, strrpos($request_uri, '/'));
		
		$test = str_replace('https://', '', str_replace('http://', '', $_SERVER['HTTP_REFERER']));
		$url = 'http://'.substr($test, 0, strpos($test, '/'));
		
		$returnUrl = $url.$request_uri.'/'.$page;

		return $returnUrl;
	}

	// 파일명 변환 밀리세컨초_인코딩%제외
	function getRandFileName($fileName='') {
		
		// 확장자가 실행파일인 경우 확장자에 -x를 붙임(웹실행방지 보안을 위해)
		$fileName = preg_replace("/\.(php|phtm|htm|cgi|pl|exe|jsp|asp|inc|cab|js)/i", "$0-x", $fileName);
		
		
		// 랜덤문자 3개 생성
		$chars_array = array_merge(range(0,9), range('a','z'), range('A','Z'));
		$shuffle = implode("", $chars_array);
		$rand_char = "";
		for ($i=0; $i<3; $i++) {
			$rand_char .= $shuffle[mt_rand(0,strlen($shuffle))];
		}
		$result = (microtime(true)*100)."_".$rand_char."_".str_replace('%', '', urlencode(str_replace(' ', '_', $fileName)));
//		echo $result;
		return $result;
	}

	// newiconf 표기여부 체크
	function checkNewIcon($registdate='', $newicon='', $day=0) {
		$img = false;
		$r = (microtime(true)-strtotime($registdate))/60/60/24;
		if ($newicon == '1' || ($newicon == '2' && ($r < $day))) {
			$img = true;
		}
		return $img;
	}

	// 값체크 없으면 0리턴 (숫자인 경우)
	function chkIsset($val='') {
		$r;
		if (isset($val) && $val != '') {	// 값이 존재하고 빈문자열('')이 아닌 경우에만 파라미터 리턴
			$r = $val;
		} else {
			$r = 0;
		}
		return $r;
	}

	// DB insert, update 시, 빈문자열 0으로 치환
	function changeEmptyValue($req, $array) {

		for ($i=0; $i<sizeof($array); $i++) {
			$req[$array[$i]] = (int)$req[$array[$i]];
		}

		return $req;
	}

	// 이메일 폼 생성
	function getURLMakeMailForm($url, $email_form) {
		$request = $url.$email_form;
		$response = file_get_contents($request);

		return $response;
	}

	/* 
	로그 출력 
	/www/log 파일 생성
	권한필요
	파일명은 날짜별로 생성
	사용여부는 siteProperty.php의 IS_LOGFILE 변수 true시 적용
	한글깨질 시 웹사이트 페이지 캐릭터셋과 서버 캐릭터셋 확인 후 siteProperty.php 변경
	*/

	function outlog($message) {
		if(is_array($message)){
			$message = iconv(LOG_PAGE_CHAR, LOG_SERVER_CHAR, print_r($message, true));	// 인코딩 필요
		} else {
			$message = iconv(LOG_PAGE_CHAR, LOG_SERVER_CHAR, $message);	// 인코딩 필요
		}

		
		$filename = date('Y-m-d').LOG_FILENAME;
		$p = pathinfo($_SERVER['PHP_SELF']);
		$location = $p['dirname']."/".$p['basename'];
		$log = date('H:i:s')." ".$_SERVER['REMOTE_ADDR']." ".$location."\r\n".$message."\r\n";


	// additional
	//	echo phpInfo();
	//	echo '로그파일 권한 : ';
	//	echo LOG_PATH;
	//	echo file_exists(LOG_PATH);
	//	chmod(LOG_PATH, 0755);
	//	echo '`chmod 777 '.LOG_PATH.'`';
	//	if ( !file_exists(LOG_PATH) ) {
	//		 $oldmask = umask(1);  // helpful when used in linux server  
	//	}
		if (IS_LOGFILE) {
			$f = fopen(LOG_PATH."/".$filename, "a");
			fwrite($f, $log);
			fclose($f);
		}
	}

	/*
	파일 업로드 처리 함수
	해당 첨부파일을 실파일명, 원파일명, 파일사이즈를 구해 $_REQUEST에 세팅 후 리턴
	$filename : 첨부파일명, $req : $_REQUEST, $isSize : 사이즈 구하기, $maxSaveSize : 최대 파일사이즈(config.php)
	파일사이즈 명명 규칙 : 파일명이 filename3인 경우 filesize3
	*/
	function fileupload($filename, $uploadFullPath, $req, $isSize, $maxSaveSize) {
		$files = $_FILES[$filename];
		$tmp_name = $files['tmp_name'];
		if ($tmp_name) {

			if ($files['size'] <= $maxSaveSize) {
			
				$org_name = $files['name'];
				$file_name = getRandFileName($org_name);
				if (!$files['error']) {
					move_uploaded_file($tmp_name, $uploadFullPath.$file_name);
				}

				// db insert value
				$req[$filename] = $file_name;
				$req[$filename.'_org'] = $org_name;
				if ($isSize) {
					$tmp = substr($filename, -1);
					if ((int)$tmp > 0) {
						$req['filesize'.$tmp] = $files['size'];
					} else {
						$req['filesize']=$files['size'];
					}
				}
			} else {
				echo "
				<script>
					alert('파일첨부 최대용량을 초과했습니다.');
					window.location.replace('index.php');
				</script>";
			}
		}

		return $req;
	}

	/*
	목록이미지 리사이즈 - 파일 업로드 처리 함수
	해당 첨부파일을 실파일명, 원파일명, 파일사이즈를 구해 $_REQUEST에 세팅 후 리턴
	$filename : 첨부파일명, $req : $_REQUEST, $isSize : 사이즈 구하기, $maxSaveSize : 최대 파일사이즈(config.php)
	파일사이즈 명명 규칙 : 파일명이 filename3인 경우 filesize3
	*/
	function fileresizeupload($uploadFullPath, $req) {

		$o = array();
		$t = array();
		$width = 130;
		$height = 130;

		//원본이미지 정보
		$imginfo = getimagesize($uploadFullPath.$req['imagename1']);
		$o['mime'] = $imginfo['mime'];

//		$o_img = imagecreatefromJPEG($uploadFullPath.$req['imagename1']);
		switch($o['mime']){
			case 'image/jpeg' :	$o['img'] = imagecreatefromjpeg($uploadFullPath.$req['imagename1']);	break;
			case 'image/gif' :	$o['img'] = imagecreatefromgif($uploadFullPath.$req['imagename1']);	break;
			case 'image/png' :	$o['img'] = imagecreatefrompng($uploadFullPath.$req['imagename1']);	break;
			case 'image/bmp' :	$o['img'] = imagecreatefrombmp($uploadFullPath.$req['imagename1']);	break;

			default :		return array('bool' => false);						break;
		}

		// 원본 이미지 크기
		$o['size'] = array('w' => $imginfo[0], 'h' => $imginfo[1]);

		// 썸네일 이미지 가로, 세로 비율 계산
		$t['ratio']['w'] = $o['size']['w'] / $width;
		$t['ratio']['h'] = $o['size']['h'] / $height;

		// 썸네일 이미지의 비율계산 (가로 == 세로)
		if($t['ratio']['w'] == $t['ratio']['h']){
			$t['size']['w'] = $width;
			$t['size']['h'] = $height;
		}
		// 썸네일 이미지의 비율계산 (가로 > 세로)
		elseif($t['ratio']['w'] > $t['ratio']['h']){
			$t['size']['w'] = $width;
			$t['size']['h'] = round(($width * $o['size']['h']) / $o['size']['w']);
		}
		// 썸네일 이미지의 비율계산 (가로 < 세로)
		elseif($t['ratio']['w'] < $t['ratio']['h']){
			$t['size']['w'] = $width;
			$t['size']['h'] = round(($width * $o['size']['h']) / $o['size']['w']);
//			$t['size']['w'] = round(($height * $o['size']['w']) / $o['size']['h']);
//			$t['size']['h'] = $height;
		}


		$t['img'] = imagecreatetruecolor($t['size']['w'],$t['size']['h']);
//		$n_img = imagecreatetruecolor(130,130);
//		imagecopyresampled($n_img,$o_img,0,0,0,0,130,130,260,260);
//		ImageCopyResized($n_img,$o_img,0,0,0,0,130,130,260,260);		//원본

		ImageCopyResized($t['img'],$o['img'],0,0,0,0,$t['size']['w'],$t['size']['h'],$o['size']['w'],$o['size']['h']);
		$n_path = $uploadFullPath."mini_".$req['imagename1'];
		

//		imagejpeg($n_img, $n_path);
		switch($o['mime']){
			case 'image/jpeg' :	imagejpeg($t['img'], $n_path);	break;
			case 'image/gif' :	imagegif($t['img'], $n_path);	break;
			case 'image/png' :	imagepng($t['img'], $n_path);	break;
			case 'image/bmp' :	imagebmp($t['img'], $n_path);	break;
		}


//		imagedestroy($n_img);
		imagedestroy($t['img']);

		$req['listimage'] = "mini_".$req['imagename1'];

		return $req;

	}

	/*
	다중 파일 업로드 처리 함수
	해당 첨부파일을 실파일명, 원파일명, 파일사이즈를 구해 $_REQUEST에 세팅 후 리턴
	$filename : 첨부파일명, $req : $_REQUEST, $isSize : 사이즈 구하기, $maxSaveSize : 최대 파일사이즈(config.php)
	파일사이즈 명명 규칙 : 파일명이 filename3인 경우 filesize3
	*/
	function multifileupload($filenames, $uploadFullPath, $req, $isSize, $maxSaveSize) {
		$filename = array();
		$filename_org = array();
		$filesize = array();
		$files = $_FILES[$filenames];
		if($files) {
//		echo ($files);

		foreach($files['size'] as $key => $val){ 
//		echo $files['tmp_name'][$key];
			$tmp_name = $files['tmp_name'][$key];
			if ($tmp_name) {

				if ($files['size'][$key] <= $maxSaveSize) {
				
					$org_name = $files['name'][$key];
					$file_name = getRandFileName($org_name);
					if (!$files['error'][$key]) {
						move_uploaded_file($tmp_name, $uploadFullPath.$file_name);
					}

					// db insert value
//					$req[$filenames] = $file_name;
//					$req[$filenames.'_org'] = $org_name;
					array_push($filename, $file_name);
					array_push($filename_org, $org_name);
					if ($isSize) {
						array_push($filesize, $files['size'][$key]);
					}
				} else {
					echo "
					<script>
						alert('파일첨부 최대용량을 초과했습니다.');
						window.location.replace('index.php');
					</script>";
				}
			}
		}
//		echo $filename_org;
//		echo $filesize[0];
		$req[$filenames] = $filename;
		$req[$filenames.'_org'] = $filename_org;
		$req['filesizes'] = $filesize;
		}
		return $req;
	}

	function downloadUrl($uploadPath, $filename, $filename_org) {
		$url = "/lib/download.php?path=".$uploadPath."&vf=".$filename."&af=".$filename_org;
		return $url;
	}

	// utf8 문자열 길이구하기
	function utf8_length($str) {
		$len = strlen($str);
		for ($i = $length = 0; $i < $len; $length++) {
			$high = ord($str{$i});
			if ($high < 0x80)//0<= code <128 범위의 문자(ASCII 문자)는 인덱스 1칸이동
				$i += 1;
			else if ($high < 0xE0)//128 <= code < 224 범위의 문자(확장 ASCII 문자)는 인덱스 2칸이동
				$i += 2;
			else if ($high < 0xF0)//224 <= code < 240 범위의 문자(유니코드 확장문자)는 인덱스 3칸이동 
				$i += 3;
			else//그외 4칸이동 (미래에 나올문자)
				$i += 4;
		}
		return $length;
	}

	// utf8문자열 자르고 뒤에 ... 붙이기
	function utf8_strcut($str, $chars, $tail = '...') {  
		if (utf8_length($str) <= $chars)//전체 길이를 불러올 수 있으면 tail을 제거한다.
			$tail = '';
		else
			$chars -= utf8_length($tail);//글자가 잘리게 생겼다면 tail 문자열의 길이만큼 본문을 빼준다.
			$len = strlen($str);
		for ($i = $adapted = 0; $i < $len; $adapted = $i) {
			$high = ord($str{$i});
			if ($high < 0x80)
				$i += 1;
			else if ($high < 0xE0)
				$i += 2;
			else if ($high < 0xF0)
				$i += 3;
			else
				$i += 4;
			if (--$chars < 0)
				break;
		}
		return trim(substr($str, 0, $adapted)) . $tail;
	}

	// 랜덤문자열 생성
	function getRandomStr($length)	{  
		$characters  = "0123456789";  
		$characters .= "abcdefghijklmnopqrstuvwxyz";  
		$characters .= "ABCDEFGHIJKLMNOPQRSTUVWXYZ";  
		  
		$string_generated = "";  
		  
		$nmr_loops = $length;  
		while ($nmr_loops--)  
		{  
			$string_generated .= $characters[mt_rand(0, strlen($characters))];  
		}  
		  
		return $string_generated;  
	}

	function getDifferChecked($s, $w) {
		return ($s != $w) ? "checked" : "";
	}

	// 지점별 예약달력에서 가능여부 체크
	function reserPosibleTime($rt, $data, $lunch, $dinner) {
		$result = 0;

		if ($data) {
			// 예약시간 int형으로 바꾸기 ex) 0900
			$reser = ($rt) ? (int)(substr($rt, 0, 2).substr($rt, 3, 2)) : 0;

			if ($data['yoil_start_time'] && $data['yoil_end_time']) {
				$startT = (int)$data['yoil_start_time'];
				$endT = (int)$data['yoil_end_time'];

				// 시작, 끝시간에 포함되면 가능(0) 넘거나 모자르면 불가능(1)
				$result = $startT <= $reser && $reser <= $endT ? $result : $result+1;
			}

			// 점심시간
			if ($lunch) {
				if ($data['lunch_start_time'] && $data['lunch_end_time']) {
					$startT = (int)$data['lunch_start_time'];
					$endT = (int)$data['lunch_end_time'];

					// 점심시간 시작시간 보다 작고 끝시간 보다 크면 가능(0), 포함이면 불가능(10)
					$result = $startT <= $reser && $reser < $endT ? $result+10 : $result;
				}
			}

			// 저녁시간
			if ($dinner) {
				if ($data['dinner_start_time'] && $data['dinner_end_time']) {
					$startT = (int)$data['dinner_start_time'];
					$endT = (int)$data['dinner_end_time'];

					// 저녁시간 시작보다 작고 끝시간 보다 크면 가능(0), 포함이면 불가능(100)
					$result = $startT <= $reser && $reser < $endT ? $result+100 : $result;
				}
			}
		}
		return $result;
	}

	// row 중 특정배열의 필드값 가져오기
	function getRowValue($result, $i, $name) {
		mysql_data_seek($result, $i);
		$row = mysql_fetch_array($result);
		return $row[$name];
	}

	// 관리자 SMS발송여부 체크
	function timeLimit($chk, $start, $end) {
		$tf = false;

		if ($chk == "0") {
			$tf = true;
		} else {
			$time = time();
			$sTime = strtotime($start);
			$eTime = strtotime($end);

			if ($sTime <= $time && $time <= $eTime) {
				$tf = true;
			}
		}
		return $tf;
	}

	/**
	 * DB result 값 array 배열로 변경
	 * @param unknown $rst
	 * @return multitype:
	 */
	function rstToArray($rst){
		$arr = array();
		if($rst != null && mysql_num_rows($rst) > 0){
			while($row = mysql_fetch_assoc($rst)){
				array_push($arr, $row);
			}
		}
		return $arr;
	}

	/**
	 * json_decode 거치면 json_array list 객체들은 obj형식으로 변형됨.
	 * @param unknown $obj
	 * @return multitype:
	 */
	function objToArray($obj){
		if(is_object($obj)){
			$obj = get_object_vars($obj);
		}
		
		if(is_array($obj)){
			return array_map(__FUNCTION__, $obj);
		} else {
			return $obj;
		}
	}

	function MobileCheck() { 
		global $HTTP_USER_AGENT; 
		$MobileArray  = array("iphone","lgtelecom","skt","mobile","samsung","nokia","blackberry","android","android","sony","phone");

		$checkCount = 0; 
			for($i=0; $i<sizeof($MobileArray); $i++){ 
				if(preg_match("/$MobileArray[$i]/", strtolower($HTTP_USER_AGENT))){ $checkCount++; break; } 
			} 
	   return ($checkCount >= 1); 
	}

	function getHtmlRemoveTextReSize($html, $size) {
		$html = utf8_strcut(strip_tags($html),$size,'...');
		return $html;
	}


	function outRelyDepth($answer_depth, $img = '<img src="/img/answer_icon.gif" style="width:20px;" />'){
		if(!$answer_depth) return '';
		else{
			if(sizeof( preg_split('/@/', $answer_depth) ) > 1 ) {			
				foreach(preg_split('/@/', $answer_depth) as $i => $item){
					if($i<1) continue;
					echo $img;
				}
			}
		}
	}

	function getSplitIdx( $str, $splitStr, $idx){
		$rtnArr = explode( $splitStr, $str);
		return $rtnArr[$idx];
	}

	function cvtHttp( $url){
		if( strpos( $url, 'http://') > -1 || strpos( $url, 'https://') > -1){
			return $url;
		}else{
			return 'http://'.$url;
		}
	}

	//특정문자가 변수내에 존재할경우 true 값을 반환하는 함수
	function contains($str, $search) {
		$pos = strpos($str, $search);
		if ($pos > -1) {
			return true;
		}
		else return false;
	}


	/***
		request 바인딩 할때 스팸체크
	***/
	function getSpamCheck( $req ){
		$result		= true;
		$name		= $req['name'] ;
		$title		= $req['title'] ;
		$contents	= $req['contents'] ;
		if( $name || $title || $contents){
			$curAddr = $_SERVER['REQUEST_URI'];
			if( !contains( $curAddr, "/manage/") )
				if( $_SESSION["SESSIONKEY"] != $req["spamkey"]){
					$result	= false;
				}
		}
		if(!$result ){
			echo returnHistory("요청 중 장애가 발생했습니다.error[".$_SESSION["SESSIONKEY"]."/".$req["spamkey"]."]");
			return;
		}
		return $result;
	}

	/***
	 * 휴대폰번호거나 전화번호가 국내(대한민국) 에서 발급받은 번호 인지.
	 * @param cell
	 * @return
	 */
	function isKoreanCellNumber($cell){
	}


	/****
	 * 휴대폰에 하이픈 추가
	 * 01029598365 -> 010-2959-8365
	 * @param cell
	 * @return
	 */
	function getTelWithHypen($cell){
		$rtnVal = $cell;
		$cell = str_replace( '-', '', $cell );
		$strLen = strlen( $cell );		 // -없애고 나서의 길이
		if($strLen == 11)
			if( preg_match( '/(010)|(011)|(016)|(017)|(018)|(019)|(070)/', substr($cell, 0, 3) ) )
				$rtnVal = substr($cell, 0, 3) . "-" . substr($cell, 3, 4) . "-" . substr($cell, 7);

		return $rtnVal;
	}

	function getReqParameter($arg1, $arg2) {
		if ( is_null($arg1) || empty($arg1) ) {
			return $arg2;
		}
		else {
			return trim($arg1);
		}
	}

	function RegexImgIgnore($contents,$sizeStr=''){
		
		$result = "";
		preg_match_all("/<img[^>]*src=[\'\"]?([^>\'\"]+)[\'\"]?[^>]*>/", $contents, $matchs);
		$imagename = $matchs[1][0];
		if($imagename == ''){
		$result = "/img/no_image.png";
		}else{

		$file_ext = strtolower(substr(strrchr($imagename, "."), 1)); 
		$fileNameWithoutExt = substr($imagename, 0, strrpos($imagename, "."));
		
		$ext = end(explode('.', $imagename)); 
			if($sizeStr == ''){
				$result = $fileNameWithoutExt.".".$ext;
			}else{
				$result = $fileNameWithoutExt."-".$sizeStr.".".$ext;
			}
		}
		return $result;


	}

	function encrypt($data, $secret)
	{
		//Generate a key from a hash
		$key = md5(utf8_encode($secret), true);

		//Take first 8 bytes of $key and append them to the end of $key.
		$key .= substr($key, 0, 8);

		//Pad for PKCS7
		$blockSize = mcrypt_get_block_size('tripledes', 'ecb');
		$len = strlen($data);
		$pad = $blockSize - ($len % $blockSize);
		$data .= str_repeat(chr($pad), $pad);

		//Encrypt data
		$encData = mcrypt_encrypt('tripledes', $key, $data, 'ecb');

		return base64_encode($encData);
	}

	function decrypt($data, $secret)
	{
		//Generate a key from a hash
		$key = md5(utf8_encode($secret), true);

		//Take first 8 bytes of $key and append them to the end of $key.
		$key .= substr($key, 0, 8);

		$data = base64_decode($data);

		$data = mcrypt_decrypt('tripledes', $key, $data, 'ecb');

		$block = mcrypt_get_block_size('tripledes', 'ecb');
		$len = strlen($data);
		$pad = ord($data[$len-1]);

		return substr($data, 0, strlen($data) - $pad);
	}


	function dayColor($index) {
		if( $index % 7 == 0 ){
			return "txt_red";
		}
		else if( $index % 7 == 6 ){
			return "txt_blue";
		}
		else{
			return "";
		}
	}

	function getKey($length = 10) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyz';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return date('Ymd').$randomString;
	}

	function ipMatches($ar, $ip) {
		$result = false;

		$ar = preg_replace('/\r\n|\r|\n/', ',', $ar);
		$iplist = explode(',', $ar);

		for ($i=0; $i<count($iplist); $i++) {
			$isExist = preg_match('/'.$iplist[$i].'/i', $ip, $matches);
			if ($isExist > 0) {
				$result = true;
			}
		}

		return $result;
	}

	function getStrBytes($str) {
		return mb_strwidth($str, 'utf8');;
	}

?>