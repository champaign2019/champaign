<?
	session_start(); 

	include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/lib/db/DBConnection.class.php";

	class Attachfile {
		var $conn;
		var $userAgent;
		var $tablename;

		function Attachfile($tablename) {
			$this->userAgent = $_SERVER['HTTP_USER_AGENT'];
			$this->tablename = $tablename;
		}

		/*
			필수 parameters
			1. $param 배열에 common_fk라는 key값의 밸류 : 연관되는 부모 테이블에서 해당되는 데이터의 pk값이 되겠음
			2. $param 배열에 uploadPath라는 key값의 밸류 : 파일이 업로드 될 realPath가 되겠음.
			3. Attachfile 생성자로 객체 생성할 때 넘겨받은 tablename : 연관되는 부모 테이블의 테이블명이 되겠음.
		*/
		function insert($param) {
			$dbconn = new DBConnection();
			$conn = $dbconn->getConnection();

			$common_fk = $param['common_fk'];
			$uploadPath = $param['uploadPath'];

			$filename_index =  1;
			$prevFileKey = '';
			foreach ( $_FILES as $fileKey => $fileValues ) {
				if ($fileValues['error'] == 0) {

					$fileKey = preg_replace('/[0-9]/', '', $fileKey);
					$tmp_name = $fileValues['tmp_name'];
					$fileobject = $fileKey;
					$filename_org = $fileValues['name'];
					$filename = getRandFileName($filename_org);
					$filesize = $fileValues['size'];

					if (!empty($prevFileKey) && $prevFileKey != $fileKey) {
						$filename_index = 1;
					}

					$sql="
						INSERT INTO `attacheFiles`
						(
							common_fk
							, tablename
							, fileobject
							, filename_index
							, filename
							, filename_org
							, filesize
							, registdate
							, userInfo
							, filename_alt
						)
						VALUES
						(
							'".$common_fk."'
							, '".$this->tablename."'
							, '".$fileobject."'
							, '".$filename_index."'
							, '".$filename."'
							, '".$filename_org."'
							, '".$filesize."'
							, NOW()
							, '".$this->userAgent."'
							, ''
						)
					";

//					echo $fileKey . ' // ';
//					echo $fileKey . ' // ';
//					print_r($fileValues);
//					echo '<br/><br/>';
//					echo $sql;
//					echo '{'.$prevFileKey.'}';
//					echo '{'.$fileKey.'}';
//					echo '{'.(!empty($prevFileKey) && $prevFileKey != $fileKey ? 'true' : 'false').'}';
//					echo '<br/>';
//					echo '<br/>';

					mysql_query($sql, $conn);

					move_uploaded_file($tmp_name, $uploadPath.$filename);

					$filename_index++;

					$prevFileKey = $fileKey;
				}
			}
		}

		/*
			필수 parameters
			1. $param 배열에 common_fk라는 key값의 밸류 : 연관되는 부모 테이블에서 해당되는 데이터의 pk값이 되겠음
			2. Attachfile 생성자로 객체 생성할 때 넘겨받은 tablename
		*/
		function fileList($param) {
			$dbconn = new DBConnection();
			$conn = $dbconn->getConnection();
			
			$common_fk = $param['common_fk'];

			$result = array();

			// 먼저 해당 테이블 common_fk의 fileobject를 중복 제외(GROUP BY) 시켜서 리스트로써 가져옴
			$sql = "
				SELECT
					*
				FROM
					`attacheFiles`
				WHERE
					tablename = '".$this->tablename."'
					AND common_fk = '".$common_fk."'
				GROUP BY
					fileobject
			";
			$fileobjectList = mysql_query($sql, $conn);
			// 그 그룹당 리스트를 구해서 그룹을 키값으로 해서 리스트를 각 키마다 저장 후 리턴
			while ($row = mysql_fetch_assoc($fileobjectList)) {
				$fileobject = $row['fileobject'];

				$sql = "
					SELECT
						*
					FROM
						`attacheFiles`
					WHERE
						tablename = '".$this->tablename."'
						AND common_fk = '".$common_fk."'
						AND fileobject = '".$fileobject."'
					ORDER BY
						uno ASC, filename ASC, filename_index ASC
				";
				
				$list = mysql_query($sql, $conn);

				$result[$fileobject] = $list;
			}
			mysql_close($conn);

			return $result;
		}

		/*
			필수 parameters
			1. $param 배열에 common_fk라는 key값의 밸류 : 연관되는 부모 테이블에서 해당되는 데이터의 pk값이 되겠음
			2. $param 배열에 uploadPath라는 key값의 밸류 : 파일이 업로드 될 realPath가 되겠음.
			3. Attachfile 생성자로 객체 생성할 때 넘겨받은 tablename : 연관되는 부모 테이블의 테이블명이 되겠음.

			전체적인 프로세스 흐름
			먼저 [기존파일 삭제]를 체크한 데이터들 for문 돌려서
			임시적으로 filename컬럼과 filename_org 컬럼을 null로 세팅한다.
			그 이후에 첨부된 파일 list순으로 다시 for문 돌려서
			넘어온 file의 name속성값(ex filename1, filename2, ...)과
			db의 fileobject(ex filename, imagefile, ...)와 filename_index(ex 1, 2, ...)를 병합하여 file의 name 속성값(fileKey)와 비교 후
			일치하는 데이터가 있으면 해당 데이터에 파일을 다시 첨부하는것이므로 update,
			이 때 기존파일 삭제 체크가 되어 있어서 null로 세팅이 되어있던 데이터같은경우는
			새로운 파일이 첨부될 경우에 새로운 filename, filename_org로 대체되어서 미리 null로 세팅해두어도 문제가 없다.
			이후에 파일 list순으로 update가 전부 다 이루어지면
			기존파일 삭제만 체크하고 파일은 첨부하지 않아 filename, filename_org가 null인 상태로 남아있는 데이터가 존재할텐데
			그래서 filename, filename_org가 null 혹은 공백인 것 기준(null이나 공백이란 것 자체가 정상적이지 않은 데이터라고 판단하기에 충분)으로
			전부 delete해준다.
			그 이후 filename_index값을 재 정렬하는 update문 실행.
			끝.
		*/
		function update($param) {
			$dbconn = new DBConnection();
			$conn = $dbconn->getConnection();

			$removeCheckArray = $_REQUEST['remove_chk'];
			foreach ($removeCheckArray as $key => $value) {
				$sql = "UPDATE `attacheFiles` SET filename = null, filename_org = null WHERE uno = '".$value."'";
				mysql_query($sql, $conn);
			}

			$common_fk = $param['common_fk'];
			$uploadPath = $param['uploadPath'];

			foreach ( $_FILES as $fileKey => $fileValues ) {
				if ($fileValues['error'] == 0) {

					$tmp_name = $fileValues['tmp_name'];
					$fileobject = preg_replace('/[0-9]/', '', $fileKey);
					$filename_org = $fileValues['name'];
					$filename = getRandFileName($filename_org);
					$filesize = $fileValues['size'];

					$sql = "
						SELECT
							COUNT(*) as cnt
						FROM
							`attacheFiles`
						WHERE
							tablename = '".$this->tablename."'
							AND common_fk = '".$common_fk."'
							AND CONCAT(fileobject, filename_index) = '".$fileKey."'
					";

					$count = mysql_query($sql, $conn);
					$count=mysql_fetch_array($count);
					$count = $count['cnt'];

//					echo $sql;
//					echo '['.$count.']';
//					echo '<br/>';

					// 해당 첨부파일은 새로 첨부하는 파일이란 소리, 고로 insert
					if ($count == 0) {
						$sql="
							SELECT
								MAX(filename_index) as max_index
							FROM
								`attacheFiles`
							WHERE
								tablename = '".$this->tablename."'
								AND common_fk = '".$common_fk."'
								AND fileobject = '".$fileobject."'
						";
						$max_index = mysql_query($sql, $conn);
						$max_index=mysql_fetch_array($max_index);
						$max_index = $max_index['max_index'];
						
//						echo $sql;
//						echo '['.$max_index.']';
//						echo '<br/>';

						$sql="
							INSERT INTO `attacheFiles`
							(
								common_fk
								, tablename
								, fileobject
								, filename_index
								, filename
								, filename_org
								, filesize
								, registdate
								, userInfo
								, filename_alt
							)
							VALUES
							(
								'".$common_fk."'
								, '".$this->tablename."'
								, '".$fileobject."'
								, '".($max_index + 1)."'
								, '".$filename."'
								, '".$filename_org."'
								, '".$filesize."'
								, NOW()
								, '".$this->userAgent."'
								, ''
							)
						";
					}
					else {
						$sql="
							UPDATE
								`attacheFiles`
							SET
								filename = '".$filename."'
								, filename_org = '".$filename_org."'
								, filesize = '".$filesize."'
								, userInfo = '".$this->userAgent."'
							WHERE
								tablename = '".$this->tablename."'
								AND common_fk = '".$common_fk."'
								AND CONCAT(fileobject, filename_index) = '".$fileKey."'
						";
					}

//					echo $sql . '<br/>';
//					echo $fileKey . ' // ';
//					print_r($fileValues);
//					echo '<br/><br/>';

					mysql_query($sql, $conn);

					move_uploaded_file($tmp_name, $uploadPath.$filename);

					// 최종적으로 기존파일 삭제는 체크되었으나, 대체될 파일이 첨부되지 않은 친구들 전부 삭제
					$sql = "DELETE FROM `attacheFiles` WHERE filename is null OR filename = '' OR filename_org is null OR filename_org = ''";
					mysql_query($sql, $conn);
				}
			}

			/*seq 정렬하는 구문 START*/
			// 먼저 해당 테이블 common_fk의 fileobject를 중복 제외 시켜서 리스트로써 가져옴
			$sql = "
				SELECT
					*
				FROM
					`attacheFiles`
				WHERE
					tablename = '".$this->tablename."'
					AND common_fk = '".$common_fk."'
				GROUP BY
					fileobject
			";
			$fileobjectList = mysql_query($sql, $conn);
			// 그 그룹당 리스트를 구해서 그룹을 키값으로 해서 리스트를 구함
			while ($row = mysql_fetch_assoc($fileobjectList)) {
				$fileobject = $row['fileobject'];

				$sql = "
					SELECT
						*
					FROM
						`attacheFiles`
					WHERE
						tablename = '".$this->tablename."'
						AND common_fk = '".$common_fk."'
						AND fileobject = '".$fileobject."'
					ORDER BY
						uno ASC, filename ASC, filename_index ASC
				";
				
				$list = mysql_query($sql, $conn);

				$idx = 1;
				while ($row2 = mysql_fetch_assoc($list)) {
					$sql = "
						UPDATE
							`attacheFiles`
						SET
							filename_index = '".$idx."'
						WHERE
							uno = '".$row2['uno']."'
					";

//					echo $sql;
//					echo '<br/>';

					mysql_query($sql, $conn);
					$idx++;
				}
			}
			/*seq 정렬하는 구문 END*/

			mysql_close($conn);
		}

		/*
			필수 parameter
			1. $fileTagName : 생성될 input type file태그의 name 속성값

			선택 parameter
			1. $fileList : 기존에 저장되었던 파일 리스트를 완성시켜줄 fileList

			write, edit 페이지에 파일태그 생성 함수
			$fileTagName : 예를들어 filename1, filename2로 파일태그의 이름이 생성된다면,
			숫자를 제거한 filename만 입력해주면 된다.
			$fileList : 수정페이지 전용 argument.
			저장된 파일 리스트를 넣어주면 자동완성 해준다.
		*/
		function fileTag($fileTagName = 'filename', $fileList = '') {
			$tag = '';

			if (mysql_num_rows($fileList) == 0) {
				$tag .= '
					<div>
						<input type="file" name="'.$fileTagName.'1" class="input50p" style="display: inline-block;" title="첨부파일을 업로드 해주세요." />
						<a class="btns add-file">'.getMsg('btn.add').'</a>
					</div>
				';
			}
			else {
				$tag .= '
					<div class="weidtFile">
				';

				$i = 0;
				while ($fileRow = mysql_fetch_assoc($fileList)) {

					$tag .= '
						<div>
							<p>
								'.getMsg('lable.image_org').' : '.$fileRow['filename_org'].'<br/>
								<label for="'.($fileRow['fileobject'].$fileRow['filename_index']).'_chk" class="b_nor_c size02 marl15">
									<input type="checkbox" id="'.($fileRow['fileobject'].$fileRow['filename_index']).'_chk" name="remove_chk[]"  value="'.$fileRow['uno'].'" />
									<i></i>'.getMsg('lable.checkbox.image_del').'
								</label>
							</p>
							<input type="file" name="'.($fileRow['fileobject'].$fileRow['filename_index']).'" class="input50p" style="display: inline-block;" title="첨부파일을 업로드 해주세요." />
					';

					if ($i == 0) {
						$tag .= '
							<a class="btns add-file">'.getMsg('btn.add').'</a>
						';
					}

					$tag .= '
						</div>
					';

					$i++;
				}

				$tag .= '
					</div>
				';
			}

			$tag .= "
				<script id=\"scriptTag\">
					(function() {
						var selector = jQ.$('[name=".$fileTagName."1]').parent().find('.add-file');
						jQ.$(selector).addFile( jQ.$(selector).parent() );
						jQ.$('#scriptTag').remove();
					})();
				</script>
			";

			return $tag;
		}

		/*
			필수 parameter
			1. $fileList : 기존에 저장되었던 파일 리스트를 완성시켜줄 fileList
		*/
		function fileView($fileList = '') {
			$tag = '';

			while ($fileRow = mysql_fetch_assoc($fileList)) {
				$tag .= '
					<p>
						<img src="/manage/img/file_img.gif" alt="파일첨부" />&nbsp;&nbsp;
						<a href="/lib/download.php?path=<?=$uploadPath?>&vf='.$fileRow['filename'].'&af='.$fileRow['filename_org'].'" target="_blank">
							'.$fileRow['filename_org'].' ['.getFileSize($fileRow['filesize']).']
						</a>
					</p>
				';
			}

			return $tag;
		}
	}

?>














