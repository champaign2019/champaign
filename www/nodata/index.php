<link href="https://fonts.googleapis.com/css?family=Nanum+Gothic" rel="stylesheet">
<style>
	.nodata{border:1px solid #d6d6d6 !important; width:100% !important;text-align: center;padding: 100px 0 !important; height:auto !important; box-sizing:border-box; margin:0 auto !important}
	.nodata p{font-size:18px;font-weight: bold; letter-spacing: -0.05em; color:#444;padding-top: 20px;font-family: 'Nanum Gothic', sans-serif;-ms-word-break: keep-all;	word-break: keep-all;}

	@media all and (max-width:640px){
		.nodata{padding:45px 0 !important}
		.nodata p{font-size:14px;}
	}
</style>
<div>
	<ul>
		<li class="nodata">
			<img src="/img/sub_list_none_img.png" alt="느낌표 이미지">
			<p><?=$_REQUEST['nodata']?></p>
			<!-- property:데이터가 없습니다. -->
		</li>
	</ul>
</div>


