<?session_start();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?

include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Common.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/category/Category.class.php";

include "config.php";
if ($boardgrade == 3) { // 게시판접근 권한처리
	include $_SERVER['DOCUMENT_ROOT']."/include/gradeCheck.php";
}

$common = new Common($pageRows, $tablename, $_REQUEST);
$_REQUEST['orderby'] = $orderby;
$_REQUEST['gubun'] = $gubun;

$rowPageCount = $common->getCount($_REQUEST);
$result = $common->getList($_REQUEST);
	$_REQUEST['pcsorts'] = 1;
	$ctlist = Category::categoryList();
	if($_REQUEST['view_loc_type'] != 1){ //제거 하기 

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/include/headHtml.php" ?>
<script type="text/javascript">
var rowCount = <?=$rowPageCount[0]?>

$(window).load(function(){
	
	
	if($(".program_table tbody tr").size() == rowCount){
		$(".mo_programPage").hide();	
	}
	$(".program_table td.tit").mouseenter(function(){
		$(this).parent("tr").addClass("on_e");
	});
	$(".program_table td.tit").mouseleave(function(){
		$(this).parent("tr").removeClass("on_e");
	});
	
})

function goSearch() {
	$("#searchForm").submit();
}


function more(){
	
	//다음페이지 계산
	var reqPageNo = (Math.ceil($(".program_table tbody tr").size()/<?=$pageRows?>)+1);
	
	$.post("index.php",
	{
		reqPageNo : reqPageNo,
		stype : $("#stype").val(),
		sval : $("#sval").val(),
		view_loc_type : 1
		
	},function(data){
		
		$(".program_table tbody").append(data);
		if($(".program_table tbody tr").size() == rowCount){
			$(".mo_programPage").hide();
		}
		
		
	}).fail(function(error){
		
	})
	
}
</script>

</head>

<body>
	
	<div id="wrap">
		<? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
		<? include "sub_visual/sub_visual.php" ?>
		<!-- 프로그램 시작 -->
		<div class="programCon" data-aos="fade-down" data-aos-delay="400">
			<div class="com_imgg_wrap">
				<div class="program_search">
					<form name="searchForm" id="searchForm" action="index.php" method="get">
					<span id="groupCategory">
						<span>
							<select name="categoryFks" onchange="fn_select_category(this)" data-value="카테고리를 선택해 주세요.">
								<option value="">선택하세요.</option>
								<?
									$categorys = explode(',', $_REQUEST['category']);
									while($row = mysql_fetch_assoc($ctlist)) {
								?>
								<option value="<?=$row['code']?>" <?=getSelected($row['code'], $categorys[0]) ?>><?=$row['orgname']?></option>
								<?
									}
								?>
							</select>
						</span>
					</span>
					<input type="hidden" name="category" value="<?=$_REQUEST["category"] ?>" />
					<script>
						var paramCategoryFk = '<?=$_REQUEST["category"] ?>';
						
						if(paramCategoryFk != ""){
							fn_select_category_value(paramCategoryFk);
						}
					</script>
					<select name="stype" title="검색을 선택해주세요">
						<option value="all" <?=getSelected($_REQUEST[stype], "all") ?>>제목+내용</option>
						<option value="name" <?=getSelected($_REQUEST[stype], "name") ?>>작성자</option>
						<option value="email" <?=getSelected($_REQUEST[stype], "email") ?>>이메일</option>
						<option value="contents" <?=getSelected($_REQUEST[stype], "contents") ?>>내용</option>
					</select>			
					<span>
						<input type="text" name="sval" id="sval" value="<?=$_REQUEST[sval]?>" />
					<a href="javascript:;" onclick="$('#searchForm').submit()" >검색</a>
					</span>
					</form>
				</div>
			</div>
			<div class="comi_list">
				<ul>
					<?
					}
					if($rowPageCount[0] > 0){
						$readUrl = "";
						$i = 0;
						while ($row=mysql_fetch_assoc($result)) {
							$readUrl = "style='cursor:pointer;' onclick=\"location.href='".$common->getQueryString('read.php', $row[no], $_REQUEST)."'\"";
						
							//(지점 > 분류) 체크 로직
							$firstTitle = "";
							if($branch && $row['branch_name']){
							
								$firstTitle .= "<span class='branch01'>" .$row['branch_name']. "</span>"; 
							
							}
							if($type && $row['type_name']){
								if($row['firstTitle'] != "["){
									$firstTitle.="";
								}	
								$firstTitle.= "<span class='branch02'>" .$row['type_name']. "</span>";
							} 
							
							$firstTitle.="";
								
							if(!$firstTitle){
								$firstTitle = "";
							}	
					?>
					<li <?=$readUrl ?>>
						<div class="img_type">
							<p>
							<?
							if(is_file($_SERVER['DOCUMENT_ROOT'].$uploadPath.$row['imagename1'])){
							?>
							<img src="<?=$uploadPath?><?=$row['imagename1']?>" alt="<?=$row['image1_alt']?>" title="<?=$row['image1_alt']?>" />
							<?
							}else{
							?>
								<?if("/img/no_image.png" == RegexImgIgnore($row['contents'], $editImgUse)  && !$useNoImg ){ ?>
								<? }else { ?>
								<img src="<?=RegexImgIgnore($row['contents'],$editImgUse) ?>" alt=""  />
								<? } ?>
							<?
							}
							?>
							</p>
						</div>
						<div class="txt_type">
							<p>
								<?=$firstTitle?>
							</p>
							<h5><?=$row['title']?></h5>
							<div class="txt_w">
								<?=utf8_strcut(strip_tags($row['contents']),50,'...')?>
							</div>
							<div class="date">
								<?
								if($timeDate){
								?>
								<?= getDateTimeFormat($row['registdate']) ?>
								<?
								}else{
								?>
								<?= getYMD($row['registdate']) ?>
								<?
								}
								?>
							</div>
						</div>
					</li>
					<!-- <li>
						<div class="img_type">
							<p><img src="/img/comi_img01.png" /></p>
						</div>
						<div class="txt_type">
							<p>
								<span class="branch01">제품카테고리</span>
								<span class="branch02">제품카테고리 상세</span>
							</p>
							<h5>비젠소프트 제품01</h5>
							<div class="txt_w">
								vizen-prod0103444 <br />비젠소프트의 대표 상품입니다. 이 상품의 특징은 강하고 튼튼하며 견고하게 만들어졌습니다.<br /> 비젠소프트의 대표 상품입니다. 이 상품의 특징은 강하고 튼튼하며 견고하게 만들어졌습니다.
							</div>
							<div class="date">2017-10-13</div>
						</div>
					</li>
					<li>
						<div class="img_type">
							<p><img src="/img/comi_img01.png" /></p>
						</div>
						<div class="txt_type">
							<p>
								<span class="branch01">제품카테고리</span>
								<span class="branch02">제품카테고리 상세</span>
							</p>
							<h5>비젠소프트 제품01</h5>
							<div class="txt_w">
								vizen-prod0103444 <br />비젠소프트의 대표 상품입니다. 이 상품의 특징은 강하고 튼튼하며 견고하게 만들어졌습니다.<br /> 비젠소프트의 대표 상품입니다. 이 상품의 특징은 강하고 튼튼하며 견고하게 만들어졌습니다.
							</div>
							<div class="date">2017-10-13</div>
						</div>
					</li> -->
					<?
						}
					}else{ 
					?>
					<?
					$_REQUEST['nodata'] = "해당되는 데이터가 없습니다.";
					include $_SERVER['DOCUMENT_ROOT']."/nodata/index.php" 
					?>
					<?
					}
					
					if($_REQUEST['view_loc_type'] != 1){
					?>
				</ul>
			</div>
			<div class="mo_programPage"><a href="javascript:;" onclick="more()">더보기<span>+</span></a></div>
			<?=pageListUser($common->reqPageNo, $rowPageCount[1], $common->getQueryString('index.php', 0, $_REQUEST))?><!-- 페이징 처리 -->
		</div>
		<!-- 프로그램끝 -->	
		<? include "sub_footer/sub_footer.php" ?>
		<? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
	</div>
</body>
</html>
<?
}
?>		
