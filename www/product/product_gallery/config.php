<?
include $_SERVER['DOCUMENT_ROOT']."/include/logingCheck.php";
$pageTitle 	 	= "제품소개 - 갤러리";				// 게시판 명
$tablename  		= "product";					// DB 테이블 명
$uploadPath 		= "/upload/prod_gall/";			// 파일, 동영상 첨부 경로
$maxSaveSize	  		= 50*1024*1024;					// 50Mb
$pageRows	  		= 10;							// 보여질 게시글 수
$boardgrade	  		= 0;							// 사용자 게시판 권한 [0: 미로그인(읽기, 쓰기), 1: 미로그인 - 읽기, 로그인 - 쓰기, 2: 로그인(읽기, 쓰기), 3: 로그인만 접근]
$gubun 				= 1;							//
/* 화면 필드 생성 여부 */
$branch	  		= false;							// 지점 사용유무  
$type	  		= false;							// 분류 지점별 사용유무	
$doctor			= false;						// 의료진
$useMovie  	 	= false;						// 파일 첨부 [사용: true, 사용 안함 : true]
$useRelationurl	= false;						// 관련URL 첨부 [사용: true, 사용 안함 : true]
$isListImage		= true;							//상세에 목록이미지 사용유무	

$timeDate	  	= true;						// 날짜 or 날짜 and 시간	
$userCon	  		= true;							// 조회수 카운트 [관리자 페이지:true, 사용자 페이지:true, 조회수:사용여부]
$useFile  		= false;							// 파일 첨부 [사용: true, 사용 안함 : true]
$textAreaEditor	= true;							// 에디터 사용여부
$editorIgnore= "";								//에디터 미사용 배열 구분자 ex : '|0|1|'						

$orderby = Array( "top desc","registdate desc" );						    //order by 배열
?>

