<?session_start();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?

include_once $_SERVER['DOCUMENT_ROOT']."/lib/siteProperty.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dbConfig.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/function.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/codeUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/dateUtil.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/page.php";

include_once $_SERVER['DOCUMENT_ROOT']."/lib/board/Common.class.php";
include_once $_SERVER['DOCUMENT_ROOT']."/lib/category/Category.class.php";

include "config.php";
if ($boardgrade == 3) { // 게시판접근 권한처리
	include $_SERVER['DOCUMENT_ROOT']."/include/gradeCheck.php";
}

$common = new Common($pageRows, $tablename, $_REQUEST);
$_REQUEST['orderby'] = $orderby;
$rowPageCount = $common->getCount($_REQUEST);
$result = $common->getList($_REQUEST);
	$_REQUEST['pcsorts'] = 1;
	$ctlist = Category::categoryList();
	if($_REQUEST['view_loc_type'] != 1){ //제거 하기 

?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include $_SERVER['DOCUMENT_ROOT']."/include/headHtml.php" ?>
<script type="text/javascript">
$(window).load(function(){
	
	
	if($(".program_table tbody tr").size() == rowCount){
		$(".mo_programPage").hide();	
	}
	$(".program_table td.tit").mouseenter(function(){
		$(this).parent("tr").addClass("on_e");
	});
	$(".program_table td.tit").mouseleave(function(){
		$(this).parent("tr").removeClass("on_e");
	});
	
})

var rowCount = <?=$rowPageCount[0]?>

$(window).load(function(){
	
	
	if($(".program_table tbody tr").size() == rowCount){
		$(".mo_programPage").hide();	
	}
	
})

function goSearch() {
	$("#searchForm").submit();
}


function more(){
	
	//다음페이지 계산
	var reqPageNo = (Math.ceil($(".program_table tbody tr").size()/<?=$pageRows?>)+1);
	
	$.post("index.php",
	{
		reqPageNo : reqPageNo,
		stype : $("#stype").val(),
		view_loc_type : 1,
		sval : $("#sval").val()
		
	},function(data){
		
		$(".program_table tbody").append(data);
		if($(".program_table tbody tr").size() == rowCount){
			$(".mo_programPage").hide();
		}
		
		
	}).fail(function(error){
		
	})
	
}
</script>
</head>

<body>

<div id="wrap">
	<? include $_SERVER['DOCUMENT_ROOT']."/include/header.php" ?>
	<? include "sub_visual/sub_visual.php" ?>
	<!-- 프로그램시작 -->
	<div class="programCon" data-aos="fade-down" data-aos-delay="400">
		<div class="gal_wrap">
			<ul>
				<?
				}
				
				$topClass = "";
				$readUrl = "";
				$i = 0;
				 if ($rowPageCount[0] > 0) { 
							while ($row=mysql_fetch_assoc($result)) {
								$readUrl = "style='cursor:pointer;' onclick=\"location.href='".$common->getQueryString('read.php', $row[no], $_REQUEST)."'\"";
								
								//(지점 > 분류) 체크 로직
								$firstTitle = "";
								if($branch && $row['branch_name']){
								
									$firstTitle .= "<span class='branch01'>" .$row['branch_name']. "</span>"; 
								
								}
								if($type && $row['type_name']){
									if($row['firstTitle'] != "["){
										$firstTitle.="";
									}	
									$firstTitle.= "<span class='branch02'>" .$row['type_name']. "</span>";
								}
								
								$firstTitle.="";
							
								if("[]" == $firstTitle){
									$firstTitle = "";
								}
					?>
					<li>
					<a <?=$readUrl?>>
						<div class="part_img">
							<?
							if(is_file($_SERVER['DOCUMENT_ROOT'].$uploadPath.$row['imagename1'])){
							?>
							<img src="<?=$uploadPath?><?=$row['imagename1']?>" alt="<?=$row['image1_alt']?>" title="<?=$row['image1_alt']?>" />
							<?
							}else{
							?>
							<img src="/img/no_image.png" alt="noImage" class="noimg_ty" />
							<?
							}
							?>
						</div>
						<div class="txt">
							<p class="up_type">
								<? if ($row['top'] == "1") { ?>
								<span class="noti_icon">공지</span>
								<?}?>
								<? if (checkNewIcon($row['registdate'], $row['newicon'], 1)) { ?>
								<span class="new_icon gre">NEW</span>
								<? } ?>
								<?=$firstTitle?>
							</p>
							<p class="title"><?=$row['title']?></p>
							
							<span>
							<?
							//날짜만 보여지는지 시간까지 보여지는지에 체크

							if($timeDate){
							?>
							<?= getDateTimeFormat($row['registdate']) ?>
							<?
							}else{
							?>
							<?= getYMD($row['registdate']) ?>
							<?
							}
							?> 
							<i></i><img src="/manage/img/ucount_icon.png" class="eyeicon" /> <?=$row[readno]?>
							</span>
						</div>
					</a>
				</li>
				<?
						$i++;
					}
				}else{ 
				?>
				<?
				$_REQUEST['nodata'] = "해당되는 데이터가 없습니다.";
				include $_SERVER['DOCUMENT_ROOT']."/nodata/index.php" 
				?>
				<?
				}
				
				if($_REQUEST['view_loc_type'] == 0){
				?>
			</ul>
		</div>
		<?
		if($rowPageCount[0] > $pageRows){
		?>
		<div class="mo_programPage"><a href="javascript:;" onclick="more()">더보기<span>+</span></a></div>
		<?
		}
		?>
		
		<?=pageListUser($common->reqPageNo, $rowPageCount[1], $common->getQueryString('index.php', 0, $_REQUEST))?><!-- 페이징 처리 -->
		
		<form name="searchForm" id="searchForm" action="index.php" method="get">
		<div class="program_search">
			<span id="groupCategory">
				<span>
					<select name="categoryFks" onchange="fn_select_category(this)" data-value="카테고리를 선택해 주세요.">
						<option value="">선택하세요.</option>
						<?
							$categorys = explode(',', $_REQUEST['category']);
							while($row = mysql_fetch_assoc($ctlist)) {
						?>
						<option value="<?=$row['code']?>" <?=getSelected($row['code'], $categorys[0]) ?>><?=$row['orgname']?></option>
						<?
							}
						?>
					</select>
				</span>
			</span>
			<input type="hidden" name="category" value="<?=$_REQUEST["category"] ?>" />
			<script>
				var paramCategoryFk = '<?=$_REQUEST["category"] ?>';
				
				if(paramCategoryFk != ""){
					fn_select_category_value(paramCategoryFk);
				}
			</script>
			<select name="stype" title="검색을 선택해주세요">
				<option value="all" <?=getSelected($_REQUEST[stype], "all") ?>>제목+내용</option>
				<option value="title" <?=getSelected($_REQUEST[stype], "title") ?>>제목</option>
				<option value="name" <?=getSelected($_REQUEST[stype], "name") ?>>작성자</option>
				<option value="contents" <?=getSelected($_REQUEST[stype], "contents") ?>>내용</option>
			</select>
			<span>
				<input type="text" name="sval" value="<?=$_REQUEST[sval]?>" title="검색할 내용을 입력해주세요" />
				<a href="javascript:;" onclick="$('#searchForm').submit()" >검색</a>
			</span>
		</div>
		</form>
	</div>
	<!-- 프로그램끝 -->
	<? include "sub_footer/sub_footer.php" ?>
	<? include $_SERVER['DOCUMENT_ROOT']."/include/footer.php" ?>
</div>
</body>
</html>

<? } ?>