<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<? include $_SERVER['DOCUMENT_ROOT']."/include/headHtml.php" ?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0, user-scalable=no" />
</head>
<body>
	<div id="er_wrap">
		<div class="er_cont">
			<div class="er_404">
				<img src="/img/404_img.jpg" class="er_mark" />
				<p class="tit_er">이용에 불편을 드려 죄송합니다.</p>
				<p class="desc_er">
					페이지의 주소가 잘못 입력되었거나 변경 혹은 삭제되어 요청하신 페이지를 찾을 수 없습니다.<br/>
					<b>입력하신 주소가 정확한지 다시 한번 확인해주시기 바랍니다.</b><br/>
					관련 문의사항은 고객센터로 알려주시면 친절하게 안내해 드리겠습니다.
				</p>
				<a href="javascript:history.back(-1);">이전 페이지로 돌아가기 <img src="/img/common_arr.png" class="com_ar" /></a>
			</div>
		</div>
	</div>
</body>
</html>