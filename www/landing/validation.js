/*공통모듈 시작*/
//입력값에 스페이스 이외의 의미있는 값이 있는지 체크
function isEmpty(input) {
    if (input.value == null || input.value.replace(/ /gi,"") == "") {
		return true;
    }
    return false;
}

// 이메일 형식 체크
function isValidEmail(input) {
	var format = /\w+([-+.]\w+)*@\w+([-.]\w+)*\.[a-zA-Z]{2,4}$/;
	return isValidFormat(input,format);
}

/**
  * 입력값이 사용자가 정의한 포맷 형식인지 체크
  * 자세한 format 형식은 자바스크립트의 ''regular expression''을 참조
  */
 function isValidFormat(input,format) {
     if (input.value.search(format) != -1) {
         return true; //올바른 포맷 형식
     }
     return false;
 }

 function isSelected(selObj){
	if (selObj.options[selObj.selectedIndex].value != "" ){
		return true;
	} else {
		return false;
	}
}

// 오직 숫자만
function isOnlyNumber(obj){
	var exp = /[^0-9]/g;
	if ( exp.test(obj.value) ) {
		alert("숫자만 입력가능합니다.");
		obj.value = "";
		obj.focus();
	}
}

// select box의 option이 선택되어 있는지 확인
function selectBoxSelectedCheck(input) {
	var result = false;
	for (var i=0 ; i < input.options.length ; i++) {
		if (input.options[i].selected == true) {
			result = true;
		}
	}
	return result;
}

function chkLength(frm) {
	var f = frm;
	var tmpStr, nStrLen, reserve;

	sInputStr = f.message.value;
	nStrLen = calculate_byte(sInputStr);

	if ( nStrLen > 80 ) {
		tmpStr = Cut_Str(sInputStr,80);
		reserve = nStrLen - 80;

		alert("작성하신 문구는 " + reserve + "바이트가 초과되었습니다.(최대 80Bytes)"); 

		// 80byte에 맞게 입력내용 수정
		f.message.value = tmpStr;
		nStrLen = calculate_byte(tmpStr);
		f.msgLength.value = nStrLen;
	} else {
		f.msgLength.value = nStrLen;
	}

	return;
}

/*공통모듈 끝*/


function goSave() {
	var f = document.board;

	if (isEmpty(f.area)) {
		alert("지역을 선택해주세요.");
		f.area.focus();
		return false;
	}

	if (isEmpty(f.name)) {
		alert("이름을 입력하세요.");
		f.name.value = "";
		f.name.focus();
		return false;
	}

	if (isEmpty(f.cell1)) {
		alert("휴대폰 앞번호를 선택해 주세요.");
		f.cell1.focus();
		return false;
	}

	if (isEmpty(f.cell2)) {
		alert("휴대폰 번호를 입력하세요.");
		f.cell2.value = "";
		f.cell2.focus();
		return false;
	}

	if (isEmpty(f.cell3)) {
		alert("휴대폰 번호를 입력하세요.");
		f.cell3.value = "";
		f.cell3.focus();
		return false;
	}
	
	if (!isEmpty(f.email)) {
		if ( !isValidEmail(f.email) ){
			alert("이메일을 정확히 입력해 주십시요.");
			f.email.focus();
			return false;
		}
	} 

	if (isEmpty(f.contents)) {
		alert("상담내용을 입력하세요.");
		f.contents.value = "";
		f.contents.focus();
		return false;
	} 

	f.cell.value = f.cell1.value +"-"+ f.cell2.value+"-"+f.cell3.value;

	f.submit();
}




